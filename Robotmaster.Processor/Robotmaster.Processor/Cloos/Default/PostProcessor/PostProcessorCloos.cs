﻿// <copyright file="PostProcessorCloos.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Cloos.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Cloos.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     <see cref="PostProcessorCloos"/> inherits from <see cref="PostProcessor"/>.
    ///     <para>
    ///         Implements the Cloos dedicated properties and methods of the post-processor in order to output Cloos default robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorCloos : PostProcessor
    {
        /// <summary>
        ///     Gets the current Txt File <see cref="FileSection"/>.
        /// </summary>
        internal virtual FileSection TxtFile => this.CurrentPostFile.FileSection;

        /// <summary>
        ///     Gets the current Crf File <see cref="FileSection"/>.
        /// </summary>
        internal virtual FileSection CrfFile => this.CurrentPostFile.Children[0].FileSection;

        /// <summary>
        ///     Gets the current Variable Declarations <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter VariableDeclarations => this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter;

        /// <summary>
        ///     Gets the current Welding List <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter WeldingList => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets the current Dummy Welding List <see cref="FileSection.StreamWriter"/>.
        ///     A Dummy Welding List contains a set of welding parameters where the Arc Activation is Disabled.
        ///     Dummy Welding Lists are only used for setting the linear feedrate when the robot is NOT welding (plunge and retract movements).
        ///     When the robot is welding, the welding speed is determined by the selected item in Welding List (feedrate is one of the parameters defined for each item).
        /// </summary>
        internal virtual StreamWriter DummyWeldingList => this.CurrentPostFile.FileSection.SubFileSections[2].StreamWriter;

        /// <summary>
        ///     Gets the current Sensing Procedure <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter SensingProcedure => this.CurrentPostFile.FileSection.SubFileSections[3].StreamWriter;

        /// <summary>
        ///     Gets the current Moves Section <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[4].StreamWriter;

        /// <summary>
        ///     Gets the current Positions <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter Positions => this.CurrentPostFile.Children[0].FileSection.StreamWriter;

        /// <summary>
        ///     Gets or sets the Cached PTP Maximum Speed.
        /// </summary>
        internal virtual int CachedPtpMaximumSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the Cached PTP Smooth Transition Value.
        /// </summary>
        internal virtual int CachedPtpSmoothTransitionValue { get; set; }

        /// <summary>
        ///     Gets or sets the Cached Linear/Circular FeedRate.
        /// </summary>
        internal virtual int CachedFeedRate { get; set; }

        /// <summary>
        ///     Gets or sets the Cached Linear/Circular Interpolation Distance.
        /// </summary>
        internal virtual int CachedLinearCircularInterpolationDistance { get; set; }

        /// <summary>
        ///     Gets or sets the Cached Linear/Circular Acceleration Distance.
        /// </summary>
        internal virtual int CachedLinearCircularAccelerationDistance { get; set; }

        /// <summary>
        ///     Gets or sets the Starting Index for the Dummy Welding List.
        /// </summary>
        internal virtual int StartIndexForDummyWeldingList { get; set; }

        /// <summary>
        ///     Gets the Used Dummy Welding List.
        /// </summary>
        internal virtual List<string> UsedDummyWeldingList { get; } = new List<string>();

        /// <summary>
        ///     Gets or sets a value indicating whether the Force Dummy Welding List Output condition is used.
        /// </summary>
        internal virtual bool ForceDummyWeldingListOutput { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the Allow Dummy Welding List Output condition is used.
        /// </summary>
        internal virtual bool AllowDummyWeldingListOutput { get; set; }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            // Define the Current Post File (TXT)
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_").ToUpper();
            this.CurrentPostFile.FileExtension = ".txt";

            // Define the CRF file
            var crfFile = new PostFile();
            this.CurrentPostFile.AddChild(crfFile);
            crfFile.FileName = this.CurrentPostFile.FileName;
            crfFile.FileExtension = ".crf";

            // Define the Post File structure
            this.StartPostFiles(this.CurrentPostFile);

            // Reset variables
            this.CachedPtpMaximumSpeed =
                this.CachedPtpSmoothTransitionValue =
                    this.CachedLinearCircularInterpolationDistance =
                        this.CachedLinearCircularAccelerationDistance = -1;
            this.StartIndexForDummyWeldingList = 950;
            this.UsedDummyWeldingList.Clear();
            this.ForceDummyWeldingListOutput = false;
            this.AllowDummyWeldingListOutput = true;
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Cloos"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void StartPostFiles(PostFile postFile)
        {
            // Define File Sections
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Variable Declarations
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Welding List Output
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Dummy Welding List Output
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Sensing Procedure
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Main Move Section

            // Populate Headers and Footers
            this.TxtFile.Header = "RESTART" + "\r\n";
            this.TxtFile.Footer = "\r\n" + "END" + "\r\n";

            this.VariableDeclarations.WriteLine("\r\n" + "EXTERNAL PROC CLEAN,SYN8 FROM MASTER" + "\r\n" +
                                                "VAR STP,LN,H54203" + "\r\n" +
                                                "VAR SP,SSP,EEP,EP,I,SPD,IPOL,STAT,X,Y,Z,AL,BE,GA,E1,E2" + "\r\n" +
                                                "VAR X1,Y1,Z1,AL1,BE1,GA1,E11,E12" + "\r\n" +
                                                "VAR C,L,T");

            this.DummyWeldingList.WriteLine(string.Empty);

            this.Moves.WriteLine("\r\n" + "MAIN" + "\r\n" +
                                 "\r\n" +
                                 "! Program name: \"" + postFile.FileName + "\"" + "\r\n" +
                                 "! Programmed using Robotmaster" + "\r\n");

            this.CrfFile.Header = $"( Robot     : {CloosProcessorSwitches.GetCloosRobotNumber(this.CellSettingsNode)} )\r\n" +
                                  $"( Serial Nr : {CloosProcessorSwitches.GetCloosSerialNumber(this.CellSettingsNode)} )\r\n" +
                                  "( Achszahl  : 8 )\r\n" +
                                  "( Resolution: 2:2:2:2:2:2:2:2: Konfigend)\r\n";

            // Generate dummy list of 15 points at the end of the file (required by Cloos)
            for (var pointCount = 0; pointCount < 15; pointCount++)
            {
                this.CrfFile.Footer += "R,+000.00000,+000.00000,+000.00000,+000.00000,+000.00000,+000.00000" + "\r\n" +
                                       ($"{9000 + pointCount:00000},00100,00000,00001,+13475.00000,+000.00000,+14660.00000,+000.00000,+900.00000,+000.00000,+000.00000,+000.00000" + "\r\n");
            }
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);

            // Output Motion Settings
            this.OutputMotionSettings(point, operationNode);
        }

        /// <inheritdoc />
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(inlineEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <summary>
        ///     Outputs the motion settings.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputMotionSettings(PathNode point, CbtNode operationNode)
        {
            if (point.MoveType() == MoveType.Rapid)
            {
                this.OutputPtpMotionSettings(operationNode);
            }
            else
            {
                this.OutputLinearCircularMotionSettings(point, operationNode);
            }
        }

        /// <summary>
        ///     Outputs the Ptp motion settings.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputPtpMotionSettings(CbtNode operationNode)
        {
            // Output Speed
            this.OutputPtpSpeed(operationNode);

            // Output Termination
            this.OutputPtpTermination(operationNode);
        }

        /// <summary>
        ///     Outputs the Ptp Speed.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputPtpSpeed(CbtNode operationNode)
        {
            int ptpMaximumSpeed = CloosMotionSettings.GetPtpMaximumSpeed(operationNode);

            if (this.CachedPtpMaximumSpeed != ptpMaximumSpeed)
            {
                this.Moves.WriteLine($"PTPMAX ({ptpMaximumSpeed:0})");
                this.CachedPtpMaximumSpeed = ptpMaximumSpeed;
            }
        }

        /// <summary>
        ///     Outputs the Ptp Termination.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputPtpTermination(CbtNode operationNode)
        {
            int ptpTerminationType = CloosMotionSettings.GetPtpTerminationType(operationNode);

            if (ptpTerminationType == 0)
            {
                if (this.CachedPtpSmoothTransitionValue != 0)
                {
                    this.Moves.WriteLine($"STV (0)");
                    this.CachedPtpSmoothTransitionValue = 0;
                }
            }
            else
            {
                int ptpSmoothTransitionValue = CloosMotionSettings.GetPtpSmoothTransitionValue(operationNode);

                if (this.CachedPtpSmoothTransitionValue != ptpSmoothTransitionValue)
                {
                    this.Moves.WriteLine($"STV ({ptpSmoothTransitionValue:0})");
                    this.CachedPtpSmoothTransitionValue = ptpSmoothTransitionValue;
                }
            }
        }

        /// <summary>
        ///     Outputs the Linear/Circular motion settings.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputLinearCircularMotionSettings(PathNode point, CbtNode operationNode)
        {
            // Output Feedrate
            this.OutputLinearCircularFeedrate(point);

            // Output Termination
            this.OutputLinearCircularTermination(point, operationNode);
        }

        /// <summary>
        ///     Outputs the Linear/Circular Feedrate.
        /// </summary>
        /// <param name="point">Current point.</param>
        internal virtual void OutputLinearCircularFeedrate(PathNode point)
        {
            // Dummy Welding List allows defining Linear/Circular Motion Parameters when the Arc is not active.
            // Linear Feedrate units are [cm/min]
            if (this.AllowDummyWeldingListOutput &&
                (System.Math.Abs(this.CachedFeedRate - point.Feedrate().LinearFeedrate) > 0.5 || this.ForceDummyWeldingListOutput))
            {
                string testDummyWeldingListElement = $"(3211,0,0,{point.Feedrate().LinearFeedrate:0},0,0,700,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)";
                int testDummyWeldingListElementIndex = this.UsedDummyWeldingList.FindIndex(a => a == testDummyWeldingListElement);

                if (testDummyWeldingListElementIndex == -1)
                {
                    this.DummyWeldingList.WriteLine($"LIST {this.StartIndexForDummyWeldingList + this.UsedDummyWeldingList.Count} = {testDummyWeldingListElement}");
                    this.Moves.WriteLine($"$ ({this.StartIndexForDummyWeldingList + this.UsedDummyWeldingList.Count})");
                    this.UsedDummyWeldingList.Add(testDummyWeldingListElement);
                }
                else
                {
                    this.Moves.WriteLine($"$ ({this.StartIndexForDummyWeldingList + testDummyWeldingListElementIndex})");
                }

                this.CachedFeedRate = (int)point.Feedrate().LinearFeedrate; // Expected units are [cm/min]
                this.ForceDummyWeldingListOutput = false;
            }
        }

        /// <summary>
        ///     Outputs the Linear/Circular Termination.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputLinearCircularTermination(PathNode point, CbtNode operationNode)
        {
            if (CloosMotionSettings.GetLinearCircularTerminationType(operationNode) == 0)
            {
                // Reset values to zero
                if (this.CachedLinearCircularAccelerationDistance != 0 || this.CachedLinearCircularInterpolationDistance != 0)
                {
                    this.Moves.WriteLine("SETDD (0,0)");
                    this.CachedLinearCircularAccelerationDistance = 0;
                    this.CachedLinearCircularInterpolationDistance = 0;
                }
            }
            else
            {
                int linearCircularAccelerationDistance = CloosMotionSettings.GetLinearCircularAccelerationDistance(operationNode);
                int linearCircularInterpolationDistance = CloosMotionSettings.GetLinearCircularInterpolationDistance(operationNode);

                if (this.CachedLinearCircularAccelerationDistance != linearCircularAccelerationDistance ||
                    this.CachedLinearCircularInterpolationDistance != linearCircularInterpolationDistance)
                {
                    this.Moves.WriteLine($"SETDD ({linearCircularAccelerationDistance:0},{linearCircularInterpolationDistance:0})");
                    this.CachedLinearCircularAccelerationDistance = linearCircularAccelerationDistance;
                    this.CachedLinearCircularInterpolationDistance = linearCircularInterpolationDistance;
                }
            }
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine($"GC ({this.PointNumberInProgram})");

            this.Positions.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (point.IsArcMiddlePoint())
            {
                this.Moves.WriteLine($"ARC ({this.PointNumberInProgram - 1},{this.PointNumberInProgram},{this.PointNumberInProgram + 1})");
            }

            this.Positions.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine($"GP ({this.PointNumberInProgram})");

            this.Positions.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine($"GP ({this.PointNumberInProgram})");

            this.Positions.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <summary>
        ///     Formats the point output in the CRF file.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Cloos Point Output.</returns>
        internal virtual string FormatPointOutput(PathNode point, CbtNode operationNode)
        {
            string output = string.Empty;

            //// Cloos has no notion of User Frame. All points will be converted to be referenced from World Frame.
            //// Cloos has no notion of TCP Frame. All points will be referenced to robot Wrist Frame.
            //// Units are 1/10 mm and 1/10 deg.

            Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(
                OperationManager.GetUserFrame(operationNode),
                this.SceneCbtRoot,
                this.OperationCbtRoot,
                SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode);

            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);
            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
            }

            Matrix4X4 pointMatrix = userFrameMatrix * pointFrameInUserFrame * toolFrameMatrix.GetInverse();

            Vector3 pointPosition = pointMatrix.Position;
            Vector3 eulerAngles = this.RobotFormatter.MatrixToEuler(pointMatrix);

            if (this.PointNumberInProgram == 1) ////Repeated point required by Cloos
            {
                output += $"R,{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]):+000.00000;-000.00000}," +
                          $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]):+000.00000;-000.00000}," +
                          $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]):+000.00000;-000.00000}," +
                          $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]):+000.00000;-000.00000}," +
                          $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]):+000.00000;-000.00000}," +
                          $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]):+000.00000;-000.00000}\r\n";

                output += $"00000,00100,00000,00001," +
                          $"{10.0 * pointPosition.X:+000.00000;-000.00000}," +
                          $"{10.0 * pointPosition.Y:+000.00000;-000.00000}," +
                          $"{10.0 * pointPosition.Z:+000.00000;-000.00000}," +
                          $"{10.0 * eulerAngles.X:+000.00000;-000.00000}," +
                          $"{10.0 * eulerAngles.Y:+000.00000;-000.00000}," +
                          $"{10.0 * eulerAngles.Z:+000.00000;-000.00000}" +
                          this.FormatExternalAxes(point) + "\r\n";
            }

            output += $"R,{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]):+000.00000;-000.00000}," +
                     $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]):+000.00000;-000.00000}," +
                     $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]):+000.00000;-000.00000}," +
                     $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]):+000.00000;-000.00000}," +
                     $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]):+000.00000;-000.00000}," +
                     $"{10.0 * point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]):+000.00000;-000.00000}\r\n";

            output += $"{this.PointNumberInProgram:00000},00100,00000,00001," +
                      $"{10.0 * pointPosition.X:+000.00000;-000.00000}," +
                      $"{10.0 * pointPosition.Y:+000.00000;-000.00000}," +
                      $"{10.0 * pointPosition.Z:+000.00000;-000.00000}," +
                      $"{10.0 * eulerAngles.X:+000.00000;-000.00000}," +
                      $"{10.0 * eulerAngles.Y:+000.00000;-000.00000}," +
                      $"{10.0 * eulerAngles.Z:+000.00000;-000.00000}" +
                      this.FormatExternalAxes(point);

            return output;
        }

        /// <summary>
        ///     Formats the External Axes values for Joint, Linear and Circular moves.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Cloos External Axes Output.</returns>
        internal virtual string FormatExternalAxes(PathNode point)
        {
            string output = string.Empty;

            var rails = new[] { "E1", "E2", "E3" };
            var rotaries = new[] { "R2", "R1" };

            var railIndex = 0;
            var rotaryIndex = 0;

            foreach (Joint rail in this.Rails)
            {
                output += $",{(this.JointFormat.TryGetValue(rails[railIndex], out int i) ? $"{10 * point.JointValue(i):+000.00000;-000.00000}" : string.Empty)}";

                railIndex++;
            }

            foreach (Joint rotary in this.Rotaries)
            {
                output += $",{(this.JointFormat.TryGetValue(rotaries[rotaryIndex], out int i) ? $"{10 * point.JointValue(i):+000.00000;-000.00000}" : string.Empty)}";

                rotaryIndex++;
            }

            return output;
        }

        /// <inheritdoc />
        internal override bool IsProgramInputValid()
        {
            // Verifies if the device has the "label" for the rail and rotaries axes properly defined in the ROBX file.
            if (!this.IsExternalAxesNameValid())
            {
                this.NotifyUser("ERROR: The rail/rotary axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if any operation calls a dynamic User Frame
            foreach (CbtNode operationNode in ProgramManager.GetOperationNodes(this.ProgramNode))
            {
                if (UserFrameManager.IsDynamicUserFrame(OperationManager.GetUserFrame(operationNode), this.OperationCbtRoot))
                {
                    this.NotifyUser("ERROR: Cloos does not support dynamic User Frames. Please set the User Frame to Cell Fixed instead. Posting will be stopped.");
                    return false;
                }
            }

            // Verifies if the program contains tool changes
            if (this.IsMultiToolProgram)
            {
                this.NotifyUser("ERROR: Cloos does not support tool changes inside a program. Posting will be stopped.");
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the rail and rotary axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the rail and rotary axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsExternalAxesNameValid()
        {
            var externalAxes = new List<string>() { "R1", "R2", "E1", "E2", "E3" };
            return !(this.Rails.Any() && this.Rails.Any(x => !externalAxes.Contains(x.Name))) &&
                   !(this.Rotaries.Any() && this.Rotaries.Any(x => !externalAxes.Contains(x.Name)));
        }
    }
}
