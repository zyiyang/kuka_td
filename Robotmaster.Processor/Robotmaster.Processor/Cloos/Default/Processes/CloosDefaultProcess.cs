﻿// <copyright file="CloosDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Cloos default process.
    /// </summary>
    [DataContract(Name = "CloosDefaultProcess")]
    public class CloosDefaultProcess : PackageProcess, ICloosProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CloosDefaultProcess"/> class.
        /// </summary>
        public CloosDefaultProcess()
        {
            this.Name = "Cloos Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
