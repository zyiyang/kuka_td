﻿// <copyright file="SeamTracking.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Seam Tracking event.
    /// </summary>
    [DataContract(Name = "CloosSeamTracking")]
    public class SeamTracking : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SeamTracking"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SeamTracking()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the lateral correction speed.
        /// </summary>
        [DataMember(Name = "LateralCorrectionSpeed")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int LateralCorrectionSpeed { get; set; } = 0;

        /// <summary>
        ///     Gets or sets the vertical correction speed.
        /// </summary>
        [DataMember(Name = "VerticalCorrectionSpeed")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int VerticalCorrectionSpeed { get; set; } = 0;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"! Seam Tracking {((this.LateralCorrectionSpeed <= 0 && this.VerticalCorrectionSpeed <= 0) ? "Off" : "On")}: {this.LateralCorrectionSpeed},{this.VerticalCorrectionSpeed}";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return $"SSPD ({this.LateralCorrectionSpeed},{this.VerticalCorrectionSpeed})";
        }
    }
}
