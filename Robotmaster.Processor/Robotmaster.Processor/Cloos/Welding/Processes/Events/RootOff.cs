﻿// <copyright file="RootOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Root Off event.
    /// </summary>
    [DataContract(Name = "CloosRootOff")]
    public class RootOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RootOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public RootOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "ROOTOFF(1)";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return "ROOTOFF(1)";
        }
    }
}