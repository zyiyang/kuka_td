﻿// <copyright file="ArcOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Arc On event.
    /// </summary>
    [DataContract(Name = "CloosArcOn")]
    public class ArcOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ArcOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ArcOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the welding list number.
        /// </summary>
        [DataMember(Name = "WeldingList")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int WeldingList { get; set; } = 0;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"! Arc On: {this.WeldingList}";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return $"! Arc On: {this.WeldingList}" + "\r\n" + $"$ ({this.WeldingList})";
        }
    }
}