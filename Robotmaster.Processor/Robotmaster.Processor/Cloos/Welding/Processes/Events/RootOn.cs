﻿// <copyright file="RootOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Root On event.
    /// </summary>
    [DataContract(Name = "CloosRootOn")]
    public class RootOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RootOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public RootOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "ROOTON(1)";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return "ROOTON(1)";
        }
    }
}