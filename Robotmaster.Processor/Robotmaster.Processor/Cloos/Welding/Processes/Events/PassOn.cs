﻿// <copyright file="PassOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Pass On event.
    /// </summary>
    [DataContract(Name = "CloosPassOn")]
    public class PassOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PassOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public PassOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the number of filler seams.
        /// </summary>
        [DataMember(Name = "NumberOfFillerSeams")]
        public virtual int NumberOfFillerSeams { get; set; } = 0;

        /// <summary>
        ///     Gets or sets the index of the Reference Point.
        /// </summary>
        [DataMember(Name = "IndexOfReferencePoint")]
        public virtual int IndexOfReferencePoint { get; set; } = 0;

        /// <summary>
        ///     Gets or sets the index of Starting Point of the Filler Seam.
        /// </summary>
        [DataMember(Name = "IndexOfStartingPoint")]
        public virtual int IndexOfStartingPoint { get; set; } = 0;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"! PASS ON: {this.NumberOfFillerSeams}/{this.IndexOfReferencePoint}/{this.IndexOfStartingPoint}";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return $"! PASS ON: {this.NumberOfFillerSeams}/{this.IndexOfReferencePoint}/{this.IndexOfStartingPoint}";
        }
    }
}
