﻿// <copyright file="CloosWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Cloos.Welding.MainProcessor;
    using Robotmaster.Processor.Cloos.Welding.PostProcessor;
    using Robotmaster.Processor.Cloos.Welding.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Cloos welding process.
    /// </summary>
    [DataContract(Name = "CloosWeldingProcess")]
    public class CloosWeldingProcess : PackageProcess, ICloosProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CloosWeldingProcess"/> class.
        /// </summary>
        public CloosWeldingProcess()
        {
            this.Name = "Cloos Welding Process";
            this.PostProcessorType = typeof(PostProcessorCloosWelding);
            this.MainProcessorType = typeof(MainProcessorCloosWelding);
            this.AddEventToMet(typeof(ArcOn));
            this.AddEventToMet(typeof(ArcOff));
            this.ProcessMenuFileName = "CloosWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
