﻿// <copyright file="CloosWeldingWithTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.AuxiliaryMenus.CloosWeldingWithTouchSensing
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "CloosWeldingWithTouchSensingAuxiliary")]
    public class CloosWeldingWithTouchSensingAuxiliary : AuxiliaryMenu, ICloosProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CloosWeldingWithTouchSensingAuxiliary"/> class.
        /// </summary>
        public CloosWeldingWithTouchSensingAuxiliary()
        {
            this.Name = "Cloos Welding with Touch Sensing";
            this.ApplicationType = ApplicationType.WeldingWithTouchSensing;
            this.AuxiliaryMenuFileName = "CloosWeldingWithTouchSensingAuxiliaryMenu";
        }
    }
}
