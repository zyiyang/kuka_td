// <copyright file="CloosWeldingWithTouchSensingAuxiliaryMenu.cs" company="Hypertherm Robotic Software Inc.">
// Copyright 2002-2020 Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>
// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------

namespace Robotmaster.Processor.Cloos.Welding.AuxiliaryMenus.CloosWeldingWithTouchSensing
{
    using System;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Manages the data of the external menu Additional Cloos Welding with Touch Sensing Parameters.
    /// </summary>
    internal static class CloosWeldingWithTouchSensingAuxiliaryMenu
    {
        /// <summary>
        ///     The cache for retrieved SMS values.
        /// </summary>
        private static readonly SmsValueCache Cache = new SmsValueCache();

        /// <summary>
        ///     Checks if the external menu is loaded.
        /// </summary>
        /// <param name="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu exists; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsLoaded(object node)
        {
            return ExternalMenuManager.IsExternalMenuLoaded("AM_CloosWeldingWithTouchSensingAuxiliaryMenu_M3JF9J2V", node);
        }

        /// <summary>
        ///     Checks if the external menu is overridden.
        /// </summary>
        /// <param node="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu is overridden; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsOverridden(object node)
        {
            return ExternalMenuManager.IsExternalMenuOverridden("AM_CloosWeldingWithTouchSensingAuxiliaryMenu_M3JF9J2V", node);
        }

        /// <summary>
        ///     Gets The welding list number.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The welding list number.
        /// </returns>
        internal static int GetWeldingListNumber(object node, bool useCache = true)
        {
            const string settingUid = "{C0FDEFA2-4BB4-4235-BC3A-A50264D561AC}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The welding list number.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeldingListNumber(object node, int value, bool useCache = true)
        {
            const string settingUid = "{C0FDEFA2-4BB4-4235-BC3A-A50264D561AC}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }
        /// <summary>
        ///     Clears the SMS value cache.
        /// </summary>
        ///
        internal static void ClearCache()
        {
            Cache.Clear();
        }
    }
}
