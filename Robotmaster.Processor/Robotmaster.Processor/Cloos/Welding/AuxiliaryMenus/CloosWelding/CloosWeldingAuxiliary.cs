﻿// <copyright file="CloosWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.AuxiliaryMenus.CloosWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "CloosWeldingAuxiliary")]
    public class CloosWeldingAuxiliary : AuxiliaryMenu, ICloosProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CloosWeldingAuxiliary"/> class.
        /// </summary>
        public CloosWeldingAuxiliary()
        {
            this.Name = "Cloos Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "CloosWeldingAuxiliaryMenu";
        }
    }
}
