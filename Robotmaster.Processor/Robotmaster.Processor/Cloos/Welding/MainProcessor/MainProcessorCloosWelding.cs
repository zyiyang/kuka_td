﻿// <copyright file="MainProcessorCloosWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Cloos.Default.MainProcessor;
    using Robotmaster.Processor.Cloos.Welding.AuxiliaryMenus.CloosWelding;
    using Robotmaster.Processor.Cloos.Welding.AuxiliaryMenus.CloosWeldingWithTouchSensing;
    using Robotmaster.Processor.Cloos.Welding.Processes.Events;
    using Robotmaster.Processor.Cloos.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    internal class MainProcessorCloosWelding : MainProcessorCloos
    {
        /// <inheritdoc />
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                var weldingList = 0;

                if (weldingOperation)
                {
                    weldingList = CloosWeldingAuxiliaryMenu.GetWeldingListNumber(operationNode);
                }

                if (weldingWithTouchSensingOperation)
                {
                    weldingList = CloosWeldingWithTouchSensingAuxiliaryMenu.GetWeldingListNumber(operationNode);
                }

                point = this.AddArcOnEvent(operationNode, point, EventIndex.After, weldingList);

                if (CloosWeldingParameters.GetUseSeamTracking(operationNode) == 1)
                {
                    point = this.AddSeamTrackingEvent(
                        operationNode,
                        point,
                        EventIndex.After,
                        CloosWeldingParameters.GetLateralCorrectionSpeed(operationNode),
                        CloosWeldingParameters.GetVerticalCorrectionSpeed(operationNode));
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                point = this.AddArcOffEvent(operationNode, point, EventIndex.After);

                if (CloosWeldingParameters.GetUseSeamTracking(operationNode) == 1)
                {
                    point = this.AddSeamTrackingEvent(operationNode, point, EventIndex.After, 0, 0);
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            base.EditOperationBeforeAllPointEdits(operationNode);

            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;

            if (weldingOperation)
            {
                int numberOfFillerSeams = CloosWeldingAuxiliaryMenu.GetNumberOfFillerSeams(operationNode);

                if (numberOfFillerSeams > 0)
                {
                    int indexOfReferencePoint = CloosWeldingAuxiliaryMenu.GetIndexOfReferencePoint(operationNode);
                    int indexOfStartingPoint = CloosWeldingAuxiliaryMenu.GetIndexOfStartingPointOfFillerSeam(operationNode);

                    PathNode firstPointInOperation = OperationManager.GetFirstPoint(operationNode);
                    PathNode lastPointInOperation = OperationManager.GetLastPoint(operationNode);

                    firstPointInOperation = this.AddPassOnEvent(operationNode, firstPointInOperation, EventIndex.Before, numberOfFillerSeams, indexOfReferencePoint, indexOfStartingPoint);
                    lastPointInOperation = this.AddPassOffEvent(operationNode, lastPointInOperation, EventIndex.After);
                }
            }
        }

        /// <inheritdoc />
        internal override PathNode EditFirstNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;

            if (weldingOperation)
            {
                int numberOfFillerSeams = CloosWeldingAuxiliaryMenu.GetNumberOfFillerSeams(operationNode);

                if (numberOfFillerSeams > 0)
                {
                    point = this.AddRootOnEvent(operationNode, point, EventIndex.Before);
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;

            if (weldingOperation)
            {
                int numberOfFillerSeams = CloosWeldingAuxiliaryMenu.GetNumberOfFillerSeams(operationNode);

                if (numberOfFillerSeams > 0)
                {
                    point = this.AddRootOffEvent(operationNode, point, EventIndex.After);
                }
            }

            return point;
        }

        /// <summary>
        ///     Adds ArcOn Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <param name="weldingList">The welding list number.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddArcOnEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex, int weldingList)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOn()
                {
                    WeldingList = weldingList,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds ArcOff Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddArcOffEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOff()
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds SeamTracking Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <param name="lateralCorrectionSpeed">The lateral correction speed.</param>
        /// <param name="verticalCorrectionSpeed">The vertical correction speed.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddSeamTrackingEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex, int lateralCorrectionSpeed, int verticalCorrectionSpeed)
        {
            point = PointManager.AddEventToPathPoint(
                new SeamTracking
                {
                    LateralCorrectionSpeed = lateralCorrectionSpeed,
                    VerticalCorrectionSpeed = verticalCorrectionSpeed,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds PassOn Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <param name="numberOfFillerSeams">The number of filler seams.</param>
        /// <param name="indexOfReferencePoint">The index of the reference point.</param>
        /// <param name="indexOfStartingPoint">The index of the starting point.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddPassOnEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex, int numberOfFillerSeams, int indexOfReferencePoint, int indexOfStartingPoint)
        {
            point = PointManager.AddEventToPathPoint(
                new PassOn()
                {
                    NumberOfFillerSeams = numberOfFillerSeams,
                    IndexOfReferencePoint = indexOfReferencePoint,
                    IndexOfStartingPoint = indexOfStartingPoint,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds PassOff Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddPassOffEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new PassOff()
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds RoonOn Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddRootOnEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new RootOn
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds RootOff Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddRootOffEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new RootOff
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }
    }
}
