﻿// <copyright file="PostProcessorCloosWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Cloos.Welding.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Cloos.Default.PostProcessor;
    using Robotmaster.Processor.Cloos.Welding.Processes.Events;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Rise;
    using Robotmaster.Rise.Event;

    internal class PostProcessorCloosWelding : PostProcessorCloos
    {
        /// <inheritdoc />
        internal override void StartPostFiles(PostFile postFile)
        {
            base.StartPostFiles(postFile);

            // External Welding List
            this.ReadExternalWeldingList();

            //// Sensing procedure
            this.SensingProcedure.WriteLine(this.FormatSensingProcedure());
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            base.OutputBeforePointEvent(beforePointEvent, point, operationNode);

            if (beforePointEvent is ArcOn)
            {
                // Code will use the actual welding list based on ArcOn event
                this.AllowDummyWeldingListOutput = false;
            }
            else if (beforePointEvent is ArcOff)
            {
                // Feedrate for subsequent points will be obtained from the dummy welding list
                // We force the output of the feedrate for the retraction move
                this.AllowDummyWeldingListOutput = true;
                this.ForceDummyWeldingListOutput = true;
            }
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            base.OutputAfterPointEvent(afterPointEvent, point, operationNode);

            if (afterPointEvent is ArcOn)
            {
                // Code will use the actual welding list based on ArcOn event
                this.AllowDummyWeldingListOutput = false;
            }
            else if (afterPointEvent is ArcOff)
            {
                // Feedrate for subsequent points will be obtained from the dummy welding list
                // We force the output of the feedrate for the retraction move
                this.AllowDummyWeldingListOutput = true;
                this.ForceDummyWeldingListOutput = true;
            }
        }

        /// <inheritdoc />
        internal override void Run(CbtNode programNode)
        {
            base.Run(programNode);

            // Run Cloos multi-pass
            this.ProcessCloosMultipass();
        }

        /// <summary>
        ///     Reads the Cloos Welding List from an external TXT file located in the Shared Folder.
        ///     The contents of the TXT file are copied directly in the Welding List StreamWriter.
        /// </summary>
        internal virtual void ReadExternalWeldingList()
        {
            string filePath = Path.Combine(SharedFilesSettings.Default.SharedDir, SharedFilesSettings.Default.CatalogueDirName, "Cloos Welding List", "CLOOS Welding List.txt");

            if (File.Exists(filePath))
            {
                this.WeldingList.WriteLine(string.Empty);

                List<string> weldingList = File.ReadAllLines(filePath).ToList();

                foreach (string listItem in weldingList)
                {
                    this.WeldingList.WriteLine(listItem);
                }
            }
        }

        /// <summary>
        ///     Formats the Sensing Procedure.
        /// </summary>
        /// <returns>Formatted Cloos Sensing Procedure.</returns>
        internal virtual string FormatSensingProcedure()
        {
            return "\r\n" + "PROC SENS" + "\r\n" +
                   "SET (1)" + "\r\n" +
                   "$ (99)" + "\r\n" +
                   "GP (STP)" + "\r\n" +
                   "SET (1)" + "\r\n" +
                   "WAITM (100)" + "\r\n" +
                   "L002: IF IN(1) THEN JUMP L002" + "\r\n" +
                   "WHEN IN (1) DURING GC (STP+2) THEN JUMP L001" + "\r\n" +
                   "RESET (1)" + "\r\n" +
                   "PAUSE" + "\r\n" +
                   "L001:" + "\r\n" +
                   "RESET (1)" + "\r\n" +
                   "DECHANGE" + "\r\n" +
                   "CHANGE (STP+1)" + "\r\n" +
                   "GP (STP)" + "\r\n" +
                   "STORPOS (STP+1000,100,1,0)" + "\r\n" +
                   "ENDP";
        }

        /// <summary>
        ///     Post-process the generated code for adding Multi-Layer Welding.
        /// </summary>
        internal virtual void ProcessCloosMultipass()
        {
            if (this.IsProgramInputValid())
            {
                // Look for posted files
                string txtFilePath = Path.Combine(this.ProgramFolderPath, this.CurrentPostFile.FileName + this.CurrentPostFile.FileExtension);
                string crfFilePath = Path.Combine(this.ProgramFolderPath, this.CurrentPostFile.Children[0].FileName + this.CurrentPostFile.Children[0].FileExtension);

                if (File.Exists(txtFilePath) && File.Exists(crfFilePath))
                {
                    // Process TXT file
                    List<string> txtFileContents = File.ReadAllLines(txtFilePath).ToList();

                    // Reset variables
                    var recordedPass = new List<string>();
                    var referencePointList = new List<int>();
                    var copiedReferencePointList = new List<int>();
                    var copiedReferencePoint = 0;
                    var fillerSeamNumber = 0;
                    var referencePoint = 0;
                    var startPoint = 0;

                    for (var i = 0; i < txtFileContents.Count; i++)
                    {
                        if (txtFileContents[i].Contains("PASS ON"))
                        {
                            var reachedFirstNonRapidMove = false;
                            var reachedFirstPtpAfterNonRapidMoves = false;

                            recordedPass.Clear();

                            string[] testString = txtFileContents[i].Replace("! PASS ON: ", string.Empty).Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                            int.TryParse(testString[0], out int result1);
                            fillerSeamNumber = result1;

                            int.TryParse(testString[1], out int result2);
                            referencePoint = result2;

                            int.TryParse(testString[2], out int result3);
                            startPoint = result3;

                            // Use sub-index until PASS OFF, search for multiple welding passes connected using Ptp moves (this is NOT SUPPORTED)
                            for (int j = i + 1; j < txtFileContents.Count; j++)
                            {
                                if (!txtFileContents[j].Contains("PASS OFF"))
                                {
                                    recordedPass.Add(txtFileContents[j]); // Record pass point

                                    if (txtFileContents[j].Contains("GC (") ||
                                        txtFileContents[j].Contains("ARC ("))
                                    {
                                        if (!reachedFirstNonRapidMove)
                                        {
                                            // Assume that the first non rapid move is a Linear Point
                                            int.TryParse(txtFileContents[j].Replace("GC (", string.Empty).Replace(")", string.Empty), out int result);
                                            copiedReferencePoint = result;

                                            reachedFirstNonRapidMove = true;
                                        }

                                        if (reachedFirstPtpAfterNonRapidMoves)
                                        {
                                            // This means we have found a sequence of welding points, followed by a Ptp link, followed by a new sequence of welding points
                                            this.NotifyUser("Error: CLOOS multi-layer only supports one welding pass per operation");
                                            return;
                                        }
                                    }
                                    else if (txtFileContents[j].Contains("GP ("))
                                    {
                                        if (reachedFirstNonRapidMove)
                                        {
                                            reachedFirstPtpAfterNonRapidMoves = true;
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }

                        if (txtFileContents[i].Contains("PASS OFF"))
                        {
                            // Start filling the multi-pass sequence
                            for (var j = 1; j <= fillerSeamNumber; j++)
                            {
                                txtFileContents[i] += "\r\n" + $"FILL(1,{j},{referencePoint},{startPoint})";

                                referencePointList.Add(referencePoint);
                                copiedReferencePointList.Add(copiedReferencePoint);

                                for (var k = 0; k < recordedPass.Count; k++)
                                {
                                    if (recordedPass[k].Contains("GC ("))
                                    {
                                        txtFileContents[i] += "\r\n" + $"GC ({startPoint})";
                                        startPoint++;
                                    }
                                    else if (recordedPass[k].Contains("ARC ("))
                                    {
                                        txtFileContents[i] += "\r\n" + $"ARC ({startPoint - 1},{startPoint},{startPoint + 1})";
                                        startPoint += 2;
                                    }
                                    else if (!(recordedPass[k].Contains("ROOTON") || recordedPass[k].Contains("ROOTOFF")))
                                    {
                                        txtFileContents[i] += "\r\n" + recordedPass[k];
                                    }
                                }

                                referencePoint++;
                            }
                        }
                    }

                    // Overwrite TXT file
                    File.WriteAllLines(txtFilePath, txtFileContents);

                    // Process CRF file
                    List<string> crfFileContents = File.ReadAllLines(crfFilePath).ToList();

                    string referencePointString = string.Empty;

                    for (var i = 0; i < referencePointList.Count; i++)
                    {
                        for (var j = 0; j < crfFileContents.Count; j++)
                        {
                            // Locate each reference point in the CRF file
                            // Replace the index of the copied point based on the multi-pass welding parameters
                            if (crfFileContents[j].Contains($"{copiedReferencePointList[i]:00000},00100,00000,00001"))
                            {
                                referencePointString += crfFileContents[j - 1] + "\r\n" + crfFileContents[j].Replace(
                                                            $"{copiedReferencePointList[i]:00000},00100,00000,00001",
                                                            $"{referencePointList[i]:00000},00100,00000,00001") + "\r\n";
                            }
                        }
                    }

                    // Locate the bottom of the CRF file and place the list of reference points
                    for (var i = 0; i < crfFileContents.Count; i++)
                    {
                        if (crfFileContents[i].Contains("09000,00100,00000,00001"))
                        {
                            crfFileContents[i - 1] = referencePointString + crfFileContents[i - 1];
                        }
                    }

                    // Overwrite the CRF file
                    File.WriteAllLines(crfFilePath, crfFileContents);
                }
            }
        }
    }
}
