﻿// <copyright file="MainProcessorAbb.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.MainProcessor
{
    using Robotmaster.Processor.Common.Default.MainProcessor;

    /// <summary>
    /// Abb base class.
    /// Contains the Abb methods executed when Calculated is clicked such as event creation automation.
    /// </summary>
    internal class MainProcessorAbb : MainProcessor
    {
    }
}