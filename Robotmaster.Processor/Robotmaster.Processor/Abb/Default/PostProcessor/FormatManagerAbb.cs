﻿// <copyright file="FormatManagerAbb.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.PostProcessor
{
    /// <summary>
    /// ABB Format Manager.
    /// </summary>
    internal static class FormatManagerAbb
    {
        private static string feedFormat = "{0:0.00}";

        private static string genericFormat = "{0:0.00}";

        private static string quaternionsOutputFormat = "{0:0.#######}";

        /// <summary>
        /// Gets extension for the posted program.
        /// </summary>
        internal static string MainProgramExtension => ".MOD";

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this float value)
        {
            return string.Format(genericFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this double value)
        {
            return string.Format(genericFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted Speed value.</returns>
        internal static string FeedFormatted(this double value)
        {
            return string.Format(feedFormat, value);
        }

        /// <summary>
        /// Formatting of quaternions output.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted quaternions value.</returns>
        internal static string QuaternionsFormatted(this double value)
        {
            return string.Format(quaternionsOutputFormat, value);
        }
    }
}
