// <copyright file="PostProcessorAbb.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Kinematics;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Abb.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Abb.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    internal class PostProcessorAbb : PostProcessor
    {
        /// <summary>
        ///     Gets or sets a value indicating whether or not the robot in the actual program task is holding the work object (RTCP).
        ///     <para> TRUE:  The robot is holding the work object, i.e. using a stationary tool. </para>
        ///     <para> FALSE: The robot is not holding the work object, i.e. the robot is holding the tool. </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: robhold.
        /// </remarks>
        internal virtual bool IsRobotHoldingTheWorkObject { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the program is generated in synchronized/dynamic mode (movable user frame).
        ///     <para> See also: <see cref="IsUserCoordinateSystemFixed"/>. </para>
        ///     <para> TRUE:  ABB header output for fixed user coordinate system (static mode). </para>
        ///     <para> FALSE: ABB header output for movable user coordinate system (dynamic mode), i.e. coordinated external axes are used. </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: ufprog.
        /// </remarks>
        internal virtual bool IsSynchronizedMode { get; set; }

        /// <summary>
        ///     Gets or sets the list containing all the custom zone(s) to be initialized.
        /// </summary>
        internal virtual List<string> CustomZoneInitializerList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets the list containing all the custom zone variable name(s).
        /// </summary>
        internal virtual List<string> CustomZoneNameList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets the list containing all the work object frames to be initialized.
        /// </summary>
        internal virtual List<string> WorkObjectInitializerList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets the list containing all the tool frames to be initialized.
        /// </summary>
        internal virtual List<string> ToolFrameInitializerList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets the external axis key-value pair.
        /// <para>
        ///     Key : External axis label (from ROBX).
        /// </para>
        /// <para>
        ///     Value : Joint index used to access joint value of individual external axis through the. <code>PathNode.JointValue</code> Dictionary.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[0]" /> : External axis eax_a.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[1]" /> : External axis eax_b.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[2]" /> : External axis eax_c.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[4]" /> : External axis eax_d.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[5]" /> : External axis eax_e.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[6]" /> : External axis eax_f.
        /// </para>
        /// </summary>
        internal virtual KeyValuePair<string, int>[] ExternalAxes { get; set; } = new KeyValuePair<string, int>[6];

        /// <summary>
        ///     Gets the moves sub <see cref="FileSection"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Abb"/> convention for main programs.
        /// <para>
        ///     Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        /// </para>
        /// </summary>
        /// <param name="postFile">The main <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void StartMainPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            // Main section
            postFile.FileSection.Header += "%%%  " + "\r\n" + "  VERSION:1" + "\r\n" + "  LANGUAGE:ENGLISH" + "\r\n" +
                                           "%%%" + "\r\n" + "\r\n" + "MODULE " +
                                           (postFile.FileName ?? this.ProgramName.Replace(" ", "_")) +
                                           "\r\n" + "  !GENERATED BY ROBOTMASTER" + "\r\n";

            // Declaration sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add Declaration sub-section

            // ABB main sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection());
            postFile.FileSection.SubFileSections[1].Header += "  PROC " + postFile.FileName + "_MAIN()" + "\r\n";

            // ABB main sub-section footer
            postFile.FileSection.SubFileSections[1].Footer += "  ENDPROC" + "\r\n" + "ENDMODULE";

            // Creates path for submodules in the declaration sub-section.
            if (AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) > 0)
            {
                // Example:  LOCAL VAR string stPath := "HOME:";
                postFile.FileSection.SubFileSections[0].StreamWriter.WriteLine("  LOCAL VAR string stPath := \""
                    + AbbProcessorSwitches.GetMultiFileDirectory(this.CellSettingsNode) + "\";");
            }
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Abb"/> convention.
        /// <para>
        ///     Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        /// </para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            // Main section
            postFile.FileSection.Header += "%%%  " + "\r\n" + "  VERSION:1" + "\r\n" + "  LANGUAGE:ENGLISH" + "\r\n" +
                                           "%%%" + "\r\n" + "\r\n" + "MODULE " +
                                           (postFile.FileName ?? this.ProgramName.Replace(" ", "_")) +
                                           "\r\n" + "  !GENERATED BY ROBOTMASTER" + "\r\n";

            // Declaration sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add Declaration sub-section

            // ABB main sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection());
            postFile.FileSection.SubFileSections[1].Header += "  PROC " + postFile.FileName + "_MAIN()" + "\r\n";

            string mechanicalUnitName = this.FormatMechanicalUnitNameOfCurrentConfiguration();

            // ActUnit is used to activate a mechanical unit.
            if (mechanicalUnitName != string.Empty)
            {
                postFile.FileSection.SubFileSections[1].Header += "    ActUnit " + mechanicalUnitName + ";" + "\r\n";
            }

            // Outputs Acceleration and Deceleration for first point of first operation.
            if (AbbMotionSettings.GetIsAccelerationAndDecelerationOverrideEnabled(operationNode))
            {
                CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();

                // checks for the first point of first operation.
                if (firstOperation != null && OperationManager.GetFirstPoint(firstOperation) == currentPoint)
                {
                    postFile.FileSection.SubFileSections[1].Header +=
                        "    AccSet " + AbbMotionSettings.GetAccelerationAndDecelerationForJointMove(operationNode) + ", " +
                        AbbMotionSettings.GetAccelerationAndDecelerationRampForJointMove(operationNode) + ";" + "\r\n";
                }
            }

            if (AbbMotionSettings.GetIsLinearConfigurationMonitoringEnabled(operationNode))
            {
                string configurationStatus = "    ConfL" + (AbbMotionSettings.GetLinearConfigurationMonitoringStatus(operationNode) == 1 ? "\\On; " : "\\Off") + "\r\n";

                postFile.FileSection.SubFileSections[1].Header += configurationStatus;

                if (AbbMotionSettings.GetLinearConfigurationMonitoringLocation(operationNode) == 1)
                {
                    postFile.FileSection.SubFileSections[1].Footer += configurationStatus;
                }
            }

            if (AbbMotionSettings.GetIsSingularityAvoidanceControlEnabled(operationNode))
            {
                postFile.FileSection.SubFileSections[1].Header += "    SingArea" + (AbbMotionSettings.GetSingularityAvoidanceControlType(operationNode) == 1 ? "\\Wrist;" : "\\Off;") + "\r\n";
            }

            // DeactUnit is used to deactivate a mechanical unit.
            if (mechanicalUnitName != string.Empty)
            {
                postFile.FileSection.SubFileSections[1].Footer += "    DeactUnit " + mechanicalUnitName + ";" + "\r\n";
            }

            // ABB main sub-section footer
            postFile.FileSection.SubFileSections[1].Footer += "  ENDPROC" + "\r\n" + "ENDMODULE";
        }

        /// <summary>
        ///     Calls the current <see cref="PostFile"/> in its parent <see cref="PostFile"/> following <see cref="Abb"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to be called in its parent.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void CallPostFileIntoParent(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            // TODO: (AT-474) Implement module name.
            // Example of submodules declaration:  LOCAL VAR string stProgram_1_1 := "Program_1_1.MOD";
            postFile.Parent.FileSection.SubFileSections[0].StreamWriter.WriteLine("  LOCAL VAR string st" + postFile.FileName + " := \"" + postFile.FileName + ".MOD\";");

            //// Example of sub module call:
            ////    Load stPath \File:=stProgram_1_1;
            ////    %"Program_1_1_Main"%;
            ////    UnLoad stPath \File:=stProgram_1_1;
            postFile.Parent.FileSection.SubFileSections[1].StreamWriter.WriteLine("    Load stPath \\File:=st" + postFile.FileName + ";");
            postFile.Parent.FileSection.SubFileSections[1].StreamWriter.WriteLine("    %\"" + postFile.FileName + "_MAIN" + "\"%;");
            postFile.Parent.FileSection.SubFileSections[1].StreamWriter.WriteLine("    UnLoad stPath \\File:=st" + postFile.FileName + ";");
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formatting following <see cref="Abb"/> convention.
        /// <para>
        ///     Replaces or inserts information in the file that could not be computed when the file was started (see <see cref="StartPostFile"/>).
        /// </para>
        /// <para>
        ///     Resets file splitting conditions.
        /// </para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void EndPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            this.PointNumberInPostFile = 0;
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formatting following <see cref="Abb"/> convention for main programs.
        /// <para>
        ///     Replaces or inserts information in the file that could not be computed when the file was started (see <see cref="StartMainPostFile"/>).
        /// </para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void EndMainPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            this.FormatCustomZoneInitializerList(postFile);
            this.FormatWorkObjectInitializerList(postFile);
            this.FormatToolFrameInitializerList(postFile);
        }

        /// <summary>
        ///     Formats the custom zone list.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        internal virtual void FormatCustomZoneInitializerList(PostFile postFile)
        {
            if (this.CustomZoneInitializerList != null && this.CustomZoneInitializerList.Any())
            {
                postFile.FileSection.Header += string.Join("\r\n", this.CustomZoneInitializerList.ToArray()) + "\r\n";
                this.CustomZoneInitializerList.Clear();
            }
        }

        /// <summary>
        ///     Formats the work object list.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        internal virtual void FormatWorkObjectInitializerList(PostFile postFile)
        {
            if (this.WorkObjectInitializerList != null && this.WorkObjectInitializerList.Any())
            {
                postFile.FileSection.Header += string.Join("\r\n", this.WorkObjectInitializerList.ToArray()) + "\r\n";
                this.WorkObjectInitializerList.Clear();
            }
        }

        /// <summary>
        ///     Formats the tool frame list.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        internal virtual void FormatToolFrameInitializerList(PostFile postFile)
        {
            if (this.ToolFrameInitializerList != null && this.ToolFrameInitializerList.Any())
            {
                postFile.FileSection.Header += string.Join("\r\n", this.ToolFrameInitializerList.ToArray()) + "\r\n";
                this.ToolFrameInitializerList.Clear();
            }
        }

        /// <summary>
        ///     Formats the custom zone data.
        /// <para>
        ///     The zone data is used to specify how a position is to be terminated, i.e. how close to the programmed position the axes must be before moving towards the next position.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: zonedata.
        /// </remarks>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted custom zone data.</returns>
        internal virtual string FormatCustomZoneDataInitializer(CbtNode operationNode)
        {
            return ":=[" +

                   // RAPID component name: finep.
                   "FALSE," +

                   // RAPID component name: pzone_tcp.
                   AbbMotionSettings.GetZoneDataCustomTcpPathSize(operationNode) + "," +

                   // RAPID component name: pzone_ori.
                   AbbMotionSettings.GetZoneDataCustomReorientationPathSize(operationNode) + "," +

                   // RAPID component name: pzone_eax.
                   AbbMotionSettings.GetZoneDataCustomExternalAxesPathSize(operationNode) + "," +

                   // RAPID component name: zone_ori.
                   AbbMotionSettings.GetZoneDataCustomReorientationSize(operationNode) + "," +

                   // RAPID component name: zone_leax.
                   AbbMotionSettings.GetZoneDataCustomLinearExternalAxesSize(operationNode) + "," +

                   // RAPID component name: zone_reax.
                   AbbMotionSettings.GetZoneDataCustomRotaryExternalAxesSize(operationNode) + "];";
        }

        /// <summary>
        ///     Find next available name among the children of the current <see cref="PostFile"/> parent.
        /// </summary>
        /// <param name="postFile">Current <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        /// <returns>Next available name.</returns>
        internal virtual string FindNextAvailableName(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Logic can be added here to manage naming of robotmaster sub-program when implemented.
            string subPostFileNamePattern = this.CurrentPostFile.Parent.FileName + "_";
            var maxIndex = 1;

            while (this.CurrentPostFile.Parent.Children.Any(pf => pf.FileName == subPostFileNamePattern + maxIndex))
            {
                maxIndex++;
            }

            return subPostFileNamePattern + maxIndex;
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            // Check if Setup is Rtcp
            this.IsRobotHoldingTheWorkObject = SetupManager.IsRtcp(this.SetupNode);

            // Initialize the external axis key-value pair.
            if (this.ExternalAxesJoints.Any())
            {
                this.ExternalAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_a").Success);
                this.ExternalAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_b").Success);
                this.ExternalAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_c").Success);
                this.ExternalAxes[3] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_d").Success);
                this.ExternalAxes[4] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_e").Success);
                this.ExternalAxes[5] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_eax_f").Success);

                // Fallback if external axes not properly configured.
                if (this.ExternalAxes.All(eax => eax.Equals(default(KeyValuePair<string, int>))))
                {
                    // TODO Re-factor the lines below so that we do not expose the Manipulator class once AT-1139 is DONE (Blocked by)

                    // All rotaries from current configuration (kinematic chain).
                    IEnumerable<Manipulator> rotaries = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode))
                                                                   .SelectMany(node => node.GetComponents<Manipulator>()
                                                                       .Where(rotary => rotary is Rotary));

                    // All rails from current configuration (kinematic chain).
                    IEnumerable<Manipulator> rails = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode))
                                                                .SelectMany(node => node.GetComponents<Manipulator>()
                                                                    .Where(rail => rail is Rail));

                    if (rotaries.Any() && rails.Any())
                    {
                        // Fallback if both type of external axes are present in current configuration.
                        this.ExternalAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R1").Success);
                        this.ExternalAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R2").Success);
                        this.ExternalAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E1").Success);
                        this.ExternalAxes[3] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E2").Success);
                        this.ExternalAxes[4] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E3").Success);
                        this.ExternalAxes[5] = default(KeyValuePair<string, int>);
                    }
                    else if (rotaries.Any())
                    {
                        // Fallback if only rotary axes exist in current configuration.
                        this.ExternalAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R1").Success);
                        this.ExternalAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R2").Success);
                        this.ExternalAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R3").Success);
                        this.ExternalAxes[3] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R4").Success);
                        this.ExternalAxes[4] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R5").Success);
                        this.ExternalAxes[5] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "R6").Success);
                    }
                    else if (rails.Any())
                    {
                        // Fallback if only rail axes exist in current configuration.
                        this.ExternalAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E1").Success);
                        this.ExternalAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E2").Success);
                        this.ExternalAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E3").Success);
                        this.ExternalAxes[3] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E4").Success);
                        this.ExternalAxes[4] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E5").Success);
                        this.ExternalAxes[5] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "E6").Success);
                    }
                }
            }

            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            // Multi-file output
            if (AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) <= 0) //// and no sub-programNode exist
            {
                this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.FileExtension = FormatManagerAbb.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
            else
            {
                this.CurrentPostFile.InsertIntermediateParent(); //// Create the main program
                this.CurrentPostFile.Parent.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.Parent.FileExtension = FormatManagerAbb.MainProgramExtension;
                this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, firstOperation, firstPoint);
                this.CurrentPostFile.FileExtension = FormatManagerAbb.MainProgramExtension;

                this.CallPostFileIntoParent(this.CurrentPostFile, firstOperation, firstPoint);
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            if (this.IsZoneTypeCustom(operationNode))
            {
                // Add the zone data to the custom zone list
                this.AddZoneDataToCustomZoneInitializerList(operationNode);
            }

            // Add the tool frame data to the tool frame list
            this.AddWorkObjectToWorkObjectInitializerList(operationNode);
            this.AddToolFrameToToolFrameInitializerList(operationNode);
        }

        /// <summary>
        ///     Gets a value indicating whether a custom zone type is used.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns><c>true</c> if custom zone type used otherwise, <c>false</c>.</returns>
        internal virtual bool IsZoneTypeCustom(CbtNode operationNode)
        {
            return AbbMotionSettings.GetAbsoluteJointZoneType(operationNode) == 1 ||
                   AbbMotionSettings.GetCartesianJointZoneType(operationNode) == 1 ||
                   AbbMotionSettings.GetLinearZoneType(operationNode) == 1 ||
                   AbbMotionSettings.GetCircularZoneType(operationNode) == 1;
        }

        /// <summary>
        ///     Adds the zone data declaration to the <see cref="CustomZoneInitializerList"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void AddZoneDataToCustomZoneInitializerList(CbtNode operationNode)
        {
            string customZoneName = AbbMotionSettings.GetZoneDataCustomVariableName(operationNode);

            if (!this.CustomZoneNameList.Contains(customZoneName))
            {
                this.CustomZoneInitializerList.Add("  CONST zonedata " + customZoneName + this.FormatCustomZoneDataInitializer(operationNode));
            }

            if (this.CustomZoneNameList.Contains(customZoneName) && !this.CustomZoneInitializerList.Contains("  CONST zonedata " + customZoneName + this.FormatCustomZoneDataInitializer(operationNode)))
            {
                customZoneName += "_" + this.OperationNumber;
                this.CustomZoneInitializerList.Add("  CONST zonedata " + customZoneName + this.FormatCustomZoneDataInitializer(operationNode));
            }

            this.CustomZoneNameList.Add(customZoneName);
        }

        /// <summary>
        ///     Adds work object to the <see cref="WorkObjectInitializerList"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void AddWorkObjectToWorkObjectInitializerList(CbtNode operationNode)
        {
            if (AbbProcessorSwitches.GetWorkObjectDataOutputType(this.CellSettingsNode) == 1 && !this.WorkObjectInitializerList.Contains(this.FormatWorkObject(operationNode)) && (OperationManager.GetUserFrame(operationNode).Number != 0))
            {
                this.WorkObjectInitializerList.Add(this.FormatWorkObject(operationNode));
            }
        }

        /// <summary>
        ///     Adds tool frame to the <see cref="ToolFrameInitializerList"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void AddToolFrameToToolFrameInitializerList(CbtNode operationNode)
        {
            if (AbbProcessorSwitches.GetToolDataOutputType(this.CellSettingsNode) == 1 && !this.ToolFrameInitializerList.Contains(this.FormatToolData(operationNode)))
            {
                this.ToolFrameInitializerList.Add(this.FormatToolData(operationNode));
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // Multi-file Output
            if ((AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) > 0) && (this.PointNumberInPostFile >= AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode)))
            {
                this.EndPostFile(this.CurrentPostFile, operationNode, point);

                var newPostFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostFile);
                this.CurrentPostFile = newPostFile;

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, operationNode, point);
                this.CurrentPostFile.FileExtension = FormatManagerAbb.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, operationNode, point);

                this.CallPostFileIntoParent(this.CurrentPostFile, operationNode, point);
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine("    " + beforePointEvent.ToCode() + ";");
        }

        /// <inheritdoc/>
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(inlineEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine("    " + afterPointEvent.ToCode() + ";");
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            // Outputs acceleration and deceleration before linear moves.
            // Example: AccSet 20, 20;
            this.Moves.Write(this.FormatAccelerationAndDeceleration(point, operationNode));

                // LC move example
            // Example: MoveL [[80.98,-5.05,-33.37],[0,0.9807853,0,-0.19509],[-1,-1,0,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[10.00,500,5000,1000],fine,tl_\Wobj:=wob3;.
            this.Moves.WriteLine(this.FormatMotionType(point) + "[" +
                                 this.FormatPosition(point, operationNode) + "," +
                                 this.FormatOrientation(point, operationNode) + "," +
                                 this.TurnsAndConfig(point) + "," +
                                 this.FormatExternalAxesValues(point) + "]," +
                                 this.FormatSpeedData(point, operationNode) + "," +
                                 this.FormatZoneData(point, operationNode) + "," +
                                 this.FormatToolFrameName(operationNode) +
                                 "\\Wobj:=" + this.FormatWorkObjectName(operationNode) + ";");
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            // CC move example
            // Example: MoveC [[-328.66,5.66,-200.00],[0,1,0,0],[0,0,-1,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[[-331.00,0.00,-200.00],[0,1,0,0],[0,0,0,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[50.00,500,5000,1000],fine,tl_\Wobj:=wob;.
            if (point.IsArcMiddlePoint())
            {
                // Outputs acceleration and deceleration before circular moves.
                // Example: AccSet 20, 20;
                this.Moves.Write(this.FormatAccelerationAndDeceleration(point, operationNode));

                this.Moves.Write(this.FormatMotionType(point) + "[" +
                                 this.FormatPosition(point, operationNode) + "," +
                                 this.FormatOrientation(point, operationNode) + "," +
                                 this.TurnsAndConfig(point) + "," +
                                 this.FormatExternalAxesValues(point) + "],");
            }
            else
            {
                this.Moves.WriteLine("[" +
                                     this.FormatPosition(point, operationNode) + "," +
                                     this.FormatOrientation(point, operationNode) + "," +
                                     this.TurnsAndConfig(point) + "," +
                                     this.FormatExternalAxesValues(point) + "]," +
                                     this.FormatSpeedData(point, operationNode) + "," +
                                     this.FormatZoneData(point, operationNode) + "," +
                                     this.FormatToolFrameName(operationNode) +
                                     "\\Wobj:=" + this.FormatWorkObjectName(operationNode) + ";");
            }
        }

        /// <inheritdoc/>
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            // Outputs acceleration and deceleration before joint moves.
            // Example: AccSet 100, 100;
            this.Moves.Write(this.FormatAccelerationAndDeceleration(point, operationNode));

            // JC move example
            // Example: MoveJ [[100.11,-5.05,12.82],[0,0.9807853,0,-0.19509],[-1,-1,0,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[1000.00,500,5000,1000],fine,tl_\Wobj:=wob3;.
            this.Moves.WriteLine(this.FormatMotionType(point) + "[" +
                                 this.FormatPosition(point, operationNode) + "," +
                                 this.FormatOrientation(point, operationNode) + "," +
                                 this.TurnsAndConfig(point) + "," +
                                 this.FormatExternalAxesValues(point) + "]," +
                                 this.FormatSpeedData(point, operationNode) + "," +
                                 this.FormatZoneData(point, operationNode) + "," +
                                 this.FormatToolFrameName(operationNode) +
                                 "\\Wobj:=" + this.FormatWorkObjectName(operationNode) + ";");
        }

        /// <inheritdoc/>
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            // Outputs acceleration and deceleration before joint space moves.
            // Example: AccSet 100, 100;
            this.Moves.Write(this.FormatAccelerationAndDeceleration(point, operationNode));

            // JJ move example
            // Example: MoveAbsJ [[90.00,-45.00,-45.00,0.00,-90.00,0.00],[9E9,9E9,9E9,9E9,9E9,9E9]],v500,fine,tl_;.
            this.Moves.WriteLine(this.FormatMotionType(point) + "[" +
                                 this.FormatJointValues(point) + "," +
                                 this.FormatExternalAxesValues(point) + "]," +
                                 this.FormatSpeedData(point, operationNode) + "," +
                                 this.FormatZoneData(point, operationNode) + "," +
                                 this.FormatToolFrameName(operationNode) + ";");
        }

        /// <inheritdoc/>
        internal override void RunAfterProgramOutput()
        {
            base.RunAfterProgramOutput();

            CbtNode lastOperation = ProgramManager.GetOperationNodes(this.ProgramNode).Last();
            PathNode lastPoint = OperationManager.GetLastPoint(lastOperation);

            if (AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) <= 0)
            {
                this.FormatCustomZoneInitializerList(this.CurrentPostFile);
                this.FormatWorkObjectInitializerList(this.CurrentPostFile);
                this.FormatToolFrameInitializerList(this.CurrentPostFile);
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);
            }
            else
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);

                // Multi-file output
                PostFile parentPostFile = this.CurrentPostFile.Parent;

                // While root is not reached
                while (!ReferenceEquals(parentPostFile, this.MainPostFile.Parent))
                {
                    // End main post-file
                    this.EndMainPostFile(parentPostFile, lastOperation, lastPoint);
                    parentPostFile = parentPostFile.Parent;
                }
            }
        }

        /// <summary>
        ///     Formats the acceleration and deceleration.
        /// <para>
        ///     Defines output of the acceleration and deceleration values and acceleration and deceleration ramp.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: AccSet.
        ///     RAPID component name: Acc.
        ///     RAPID component name: Ramp.
        /// </remarks>
        /// <param name="point">Current operation.</param>
        /// <param name="operationNode">Current point.</param>
        /// <returns>Formatted AccSet.</returns>
        internal virtual string FormatAccelerationAndDeceleration(PathNode point, CbtNode operationNode)
        {
            bool IsAccelerationAndDecelerationDifferent() => AbbMotionSettings.GetAccelerationAndDecelerationForJointMove(operationNode) !=
                                                             AbbMotionSettings.GetAccelerationAndDecelerationForLinearAndCircularMove(operationNode) ||
                                                             AbbMotionSettings.GetAccelerationAndDecelerationRampForJointMove(operationNode) !=
                                                             AbbMotionSettings.GetAccelerationAndDecelerationRampForLinearAndCircularMove(operationNode);

            if (AbbMotionSettings.GetIsAccelerationAndDecelerationOverrideEnabled(operationNode))
            {
                PathNode previousPoint = point.PreviousPoint(operationNode);

                if (point.MoveType() == MoveType.Rapid)
                {
                    if (previousPoint != null && previousPoint.MoveType() != point.MoveType())
                    {
                        if (IsAccelerationAndDecelerationDifferent())
                        {
                            return "    AccSet " + AbbMotionSettings.GetAccelerationAndDecelerationForJointMove(operationNode) + ", " + AbbMotionSettings.GetAccelerationAndDecelerationRampForJointMove(operationNode) + ";" + "\r\n";
                        }
                    }
                }
                else
                {
                    if (previousPoint != null && previousPoint.MoveType() == MoveType.Rapid)
                    {
                        if (IsAccelerationAndDecelerationDifferent())
                        {
                            return "    AccSet " + AbbMotionSettings.GetAccelerationAndDecelerationForLinearAndCircularMove(operationNode) + ", " + AbbMotionSettings.GetAccelerationAndDecelerationRampForLinearAndCircularMove(operationNode) + ";" + "\r\n";
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///     Formats motion type at given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted ABB motion type.</returns>
        internal virtual string FormatMotionType(PathNode point)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    switch (point.MoveSpace())
                    {
                        case MoveSpace.Cartesian:
                            return "    MoveJ ";
                        case MoveSpace.Joint:
                            return "    MoveAbsJ ";
                        default:
                            return string.Empty;
                    }

                case MoveType.Linear:
                    return "    MoveL ";
                case MoveType.Circular:
                    return "    MoveC ";
                case MoveType.Spline:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats JJ position.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>ABB formatted JJ position.</returns>
        internal virtual string FormatJointValues(PathNode point)
        {
            return "[" +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]).Formatted() +
                   "]";
        }

        /// <summary>
        ///     Formats position output at given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>ABB formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return "[" + pointPositionInUserFrame.X.Formatted() + "," + pointPositionInUserFrame.Y.Formatted() + "," + pointPositionInUserFrame.Z.Formatted() + "]";
        }

        /// <summary>
        ///     Formats orientation output at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>ABB formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // Gets the quaternions in user frame
            Matrix3X3 pathOrientation = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Orientation;
            if (this.EnableToolFrameExtraRotation)
            {
                pathOrientation = pathOrientation * this.ToolFrameExtraRotation;
            }

            Quaternion quaternions = ConvertRotationRepresentation.RotationMatrixtoQuaternion(pathOrientation);

            return "[" + quaternions.W.QuaternionsFormatted() + "," + quaternions.X.QuaternionsFormatted() + "," + quaternions.Y.QuaternionsFormatted() + "," + quaternions.Z.QuaternionsFormatted() + "]";
        }

        /// <summary>
        ///     Formats configuration outputs.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <returns>ABB formatted configuration.</returns>
        internal virtual string TurnsAndConfig(PathNode point)
        {
            return "[" + this.Turns(point) + "," + this.Configuration() + "]";
        }

        /// <summary>
        ///     Outputs the 3 turns parameters: cf1, cf4, cf6.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <returns>The 3 turn parameters.</returns>
        internal virtual string Turns(PathNode point)
        {
            return Convert.ToString((int)Math.Floor(point.JointValue(this.JointFormat["J1"]) / 90)) + "," +
                   Convert.ToString((int)Math.Floor(point.JointValue(this.JointFormat["J4"]) / 90)) + "," +
                   Convert.ToString((int)Math.Floor(point.JointValue(this.JointFormat["J6"]) / 90));
        }

        /// <summary>
        ///     Outputs the configuration parameter: cfx.
        /// </summary>
        /// <returns>Configuration parameter.</returns>
        internal virtual string Configuration()
        {
            if (this.CachedRobotConfig.BaseConfiguration == BaseConfig.Front)
            {
                if (this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up)
                {
                    return this.CachedRobotConfig.WristConfiguration == WristConfig.Positive ? "0" : "1";
                }

                return this.CachedRobotConfig.WristConfiguration == WristConfig.Positive ? "2" : "3";
            }

            if (this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up)
            {
                return this.CachedRobotConfig.WristConfiguration == WristConfig.Positive ? "4" : "5";
            }

            return this.CachedRobotConfig.WristConfiguration == WristConfig.Positive ? "6" : "7";
        }

        /// <summary>
        ///     Formats external axes.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted ABB external axes.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point)
        {
            // The position of the external logical axes is expressed in degrees or mm (depending on the type of axis).
            // The mapping is [eax_a,eax_b,eax_c,eax_d,eax_e,eax_f].
            // Example: [9E9,9E9,9E9,9E9,9E9,9E9] The value 9E9 is defined for axes which are not connected.
            return "[" +
                   (this.ExternalAxes[0].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[0].Value).Formatted()) + "," +
                   (this.ExternalAxes[1].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[1].Value).Formatted()) + "," +
                   (this.ExternalAxes[2].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[2].Value).Formatted()) + "," +
                   (this.ExternalAxes[3].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[3].Value).Formatted()) + "," +
                   (this.ExternalAxes[4].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[4].Value).Formatted()) + "," +
                   (this.ExternalAxes[5].Equals(default(KeyValuePair<string, int>)) ? "9E9" : point.JointValue(this.ExternalAxes[5].Value).Formatted()) +
                   "]";
        }

        /// <summary>
        ///     Formats the speed data at given point.
        /// <para>
        ///     Speed data is used to specify the velocity at which both the robot and the external axes move.
        /// </para>
        /// <para>
        ///     Speed data defines the velocity of: the tool center point, the tool reorientation and the linear or rotating external axes.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: speeddata.
        /// </remarks>
        /// <param name="point">Current operation.</param>
        /// <param name="operationNode">Current point.</param>
        /// <returns>Formatted speed data.</returns>
        internal virtual string FormatSpeedData(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    switch (point.MoveSpace())
                    {
                        case MoveSpace.Cartesian:
                            return FormatSpeedDataHelper(AbbMotionSettings.GetCartesianJointSpeedType(operationNode));
                        case MoveSpace.Joint:
                            return FormatSpeedDataHelper(AbbMotionSettings.GetAbsoluteJointSpeedType(operationNode));
                        default:
                            return string.Empty;
                    }

                case MoveType.Linear:
                    return FormatSpeedDataHelper(AbbMotionSettings.GetLinearSpeedType(operationNode));
                case MoveType.Circular:
                    return FormatSpeedDataHelper(AbbMotionSettings.GetCircularSpeedType(operationNode));
                case MoveType.Spline:
                    return string.Empty;
                default:
                    return string.Empty;
            }

            string FormatSpeedDataHelper(int speedType)
            {
                switch (speedType)
                {
                    case 0:
                        if (point.MoveType() == MoveType.Linear)
                        {
                            return AbbMotionSettings.GetSpeedDataCustomVariableNameForLinearMotion(operationNode);
                        }

                        if (point.MoveType() == MoveType.Circular)
                        {
                            return AbbMotionSettings.GetSpeedDataCustomVariableNameForCircularMotion(operationNode);
                        }

                        if (point.MoveType() == MoveType.Rapid && point.MoveSpace() == MoveSpace.Joint)
                        {
                            return AbbMotionSettings.GetSpeedDataCustomVariableNameForAbsoluteJointMotion(operationNode);
                        }

                        if (point.MoveType() == MoveType.Rapid && point.MoveSpace() == MoveSpace.Cartesian)
                        {
                            return AbbMotionSettings.GetSpeedDataCustomVariableNameForCartesianJointMotion(operationNode);
                        }

                        return string.Empty;
                    case 1:
                    {
                        return "[" +

                               // RAPID component name: v_tcp.
                               point.Feedrate().LinearFeedrate.Formatted() + "," +

                               // RAPID component name: v_ori.
                               AbbMotionSettings.GetSpeedDataCustomTcpReorientationVelocity(operationNode) + "," +

                               // RAPID component name: v_leax.
                               AbbMotionSettings.GetSpeedDataCustomExternalAxesLinearVelocity(operationNode) + "," +

                               // RAPID component name: v_reax.
                               AbbMotionSettings.GetSpeedDataCustomExternalAxesRotationalVelocity(operationNode) +
                               "]";
                    }

                    case 2:
                        return "v5";
                    case 3:
                        return "v10";
                    case 4:
                        return "v20";
                    case 5:
                        return "v30";
                    case 6:
                        return "v40";
                    case 7:
                        return "v50";
                    case 8:
                        return "v60";
                    case 9:
                        return "v80";
                    case 10:
                        return "v100";
                    case 11:
                        return "v200";
                    case 12:
                        return "v300";
                    case 13:
                        return "v400";
                    case 14:
                        return "v500";
                    case 15:
                        return "v600";
                    case 16:
                        return "v800";
                    case 17:
                        return "v1000";
                    case 18:
                        return "v1500";
                    case 19:
                        return "v2000";
                    case 20:
                        return "v2500";
                    case 21:
                        return "v3000";
                    case 22:
                        return "v4000";
                    case 23:
                        return "v5000";
                    case 24:
                        return "v6000";
                    case 25:
                        return "v7000";
                    case 26:
                        return "vmax";
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        ///     Format zone data.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted zone data.</returns>
        internal virtual string FormatZoneData(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    switch (point.MoveSpace())
                    {
                        case MoveSpace.Cartesian:
                            return FormatZoneDataHelper(AbbMotionSettings.GetCartesianJointZoneType(operationNode));
                        case MoveSpace.Joint:
                            return FormatZoneDataHelper(AbbMotionSettings.GetAbsoluteJointZoneType(operationNode));
                        default:
                            return string.Empty;
                    }

                case MoveType.Linear:
                    return FormatZoneDataHelper(AbbMotionSettings.GetLinearZoneType(operationNode));
                case MoveType.Circular:
                    return FormatZoneDataHelper(AbbMotionSettings.GetCircularZoneType(operationNode));
                case MoveType.Spline:
                    return string.Empty;
                default:
                    return string.Empty;
            }

            string FormatZoneDataHelper(int zoneType)
            {
                switch (zoneType)
                {
                    case 0:
                        return "fine";
                    case 1:
                        return AbbMotionSettings.GetZoneDataCustomVariableName(operationNode);
                    case 2:
                        return "z0";
                    case 3:
                        return "z1";
                    case 4:
                        return "z5";
                    case 5:
                        return "z10";
                    case 6:
                        return "z15";
                    case 7:
                        return "z20";
                    case 8:
                        return "z30";
                    case 9:
                        return "z40";
                    case 10:
                        return "z50";
                    case 11:
                        return "z60";
                    case 12:
                        return "z80";
                    case 13:
                        return "z100";
                    case 14:
                        return "z150";
                    case 15:
                        return "z200";
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        ///     Formats the work object data.
        /// <para>
        ///     wobjdata wobj# :=[ robhold, ufprog, ufmec, uframe, oframe ].
        /// </para>
        /// <para>
        ///     robhold :   <see cref="IsRobotHoldingTheWorkObject"/>.
        /// </para>
        /// <para>
        ///     ufprog  :   <see cref="IsUserCoordinateSystemFixed"/>.
        /// </para>
        /// <para>
        ///     ufmec   :   mechanical unit name.
        /// </para>
        /// <para>
        ///     uframe  :   user coordinate system.
        /// </para>
        /// <para>
        ///     oframe  :   object coordinate system.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     The work object normally represents the physical work piece.
        ///     It is composed of two coordinate systems: the user frame (uframe) and the object frame (oframe), where the latter is a child to the former.
        ///     A work object user frame is referenced from the world coordinate system and the work object frame is referenced from the user frame coordinate system.
        ///     When programming a robot, all targets (positions) are related to the object frame of a work object.
        ///     The default work object is wobj0, which is always defined in the controller and always coincides with the base frame of the robot.
        /// </remarks>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted work object data.</returns>
        internal virtual string FormatWorkObject(CbtNode operationNode)
        {
            switch (AbbProcessorSwitches.GetWorkObjectDataOutputType(this.CellSettingsNode))
            {
                case 0:
                    return string.Empty;
                case 1:
                    return "  PERS wobjdata " + this.FormatWorkObjectName(operationNode) + ":=[" +
                           (this.IsRobotHoldingTheWorkObject ? "TRUE" : "FALSE") + "," +
                           (this.IsSynchronizedMode ? "FALSE" : "TRUE") + "," +
                           (this.IsSynchronizedMode ? "\"" + this.FormatMechanicalUnitNameOfCurrentConfiguration() + "\"" : "\"\"") + "," +
                           this.FormatUserFrame(operationNode) + "," +
                           this.FormatObjectFrame(operationNode) + "];";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the mechanical unit name of the current configuration.
        /// </summary>
        /// <returns>Formatted mechanical unit name.</returns>
        internal virtual string FormatMechanicalUnitNameOfCurrentConfiguration()
        {
            // TODO Re-factor the line below so that we do not expose the Manipulator class once AT-1139 is DONE (Blocked by)
            // All rotaries from current configuration (kinematic chain).
            IEnumerable<Manipulator> rotaries = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode))
                                                           .SelectMany(node => node.GetComponents<Manipulator>().Where(rotary => rotary is Rotary));

            if (rotaries.Any())
            {
                // All rotaries from current configuration with valid label naming convention.
                IEnumerable<Manipulator> validRotaries = rotaries.Where(rotary => Regex.IsMatch(rotary.MainJoints.First().Name, "_eax_[a-f]$"));

                // Here we assume that there is only a single station mechanical unit name inside the kinematic chain.
                return validRotaries.Any() ?
                    Regex.Match(validRotaries.First().MainJoints.First().Name, ".+?(?=_eax_[a-f]$)").Value
                    : "STN1";
            }

            return string.Empty;
        }

        /// <summary>
        ///     Formats the user coordinate system.
        /// <para>
        ///     uframe: [ [x ,y ,z], [q1, q2 ,q3 , q4] ].
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted user coordinate system.</returns>
        /// <remarks>
        ///     RAPID component name: uframe.
        ///     If the robot is holding the tool (<see cref="IsRobotHoldingTheWorkObject"/> is FALSE), the user coordinate system is defined in the world coordinate system.
        ///     If a stationary tool is used (<see cref="IsRobotHoldingTheWorkObject"/> is TRUE), the user coordinate system is defined in the wrist coordinate system.
        ///     For movable user frame (<see cref="IsUserCoordinateSystemFixed"/> is FALSE), the user frame is continuously defined by the system.
        /// </remarks>
        internal virtual string FormatUserFrame(CbtNode operationNode)
        {
            if (this.IsSynchronizedMode)
            {
                return "[[0,0,0],[1,0,0,0]]";
            }

            Matrix4X4 uFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(OperationManager.GetUserFrame(operationNode), this.SceneCbtRoot, this.OperationCbtRoot, null);
            Quaternion uFrameQuaternions = ConvertRotationRepresentation.RotationMatrixtoQuaternion(uFrameMatrix.Orientation);

            string userFramePosition = "[" +
                                       uFrameMatrix.Position.X.Formatted() + "," +
                                       uFrameMatrix.Position.Y.Formatted() + "," +
                                       uFrameMatrix.Position.Z.Formatted() +
                                       "]";

            string uFrameRotation = "[" +
                                    uFrameQuaternions.W.QuaternionsFormatted() + "," +
                                    uFrameQuaternions.X.QuaternionsFormatted() + "," +
                                    uFrameQuaternions.Y.QuaternionsFormatted() + "," +
                                    uFrameQuaternions.Z.QuaternionsFormatted() +
                                    "]";

            return "[" + userFramePosition + "," + uFrameRotation + "]";
        }

        /// <summary>
        ///     Formats the object coordinate system.
        /// <para>
        ///     oframe: [ [x ,y ,z], [q1, q2 ,q3 , q4] ].
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted object coordinate system.</returns>
        /// <remarks>
        ///     RAPID component name: oframe.
        ///     The object coordinate system is defined in the user coordinate system.
        /// </remarks>
        internal virtual string FormatObjectFrame(CbtNode operationNode)
        {
            if (this.IsSynchronizedMode)
            {
                // Get object frame matrix w.r.t user reference frame found in cell extension file.
                // If user reference frame not found in cell extension file, then the object frame will be set as the user frame.
                Matrix4X4 objectFrameFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(OperationManager.GetUserFrame(operationNode), this.SceneCbtRoot, this.OperationCbtRoot, SetupManager.GetConfiguration(this.SetupNode)?.UserFrameReferenceNode);

                // Get object frame orientation in quaternions.
                Quaternion objectFrameQuaternions = ConvertRotationRepresentation.RotationMatrixtoQuaternion(objectFrameFrameMatrix.Orientation);

                string objectFramePosition = "[" +
                                             objectFrameFrameMatrix.Position.X.Formatted() + "," +
                                             objectFrameFrameMatrix.Position.Y.Formatted() + "," +
                                             objectFrameFrameMatrix.Position.Z.Formatted() +
                                             "]";

                string objectFrameRotation = "[" +
                                             objectFrameQuaternions.W.QuaternionsFormatted() + "," +
                                             objectFrameQuaternions.X.QuaternionsFormatted() + "," +
                                             objectFrameQuaternions.Y.QuaternionsFormatted() + "," +
                                             objectFrameQuaternions.Z.QuaternionsFormatted() +
                                             "]";

                return "[" + objectFramePosition + "," + objectFrameRotation + "]";
            }

            return "[[0,0,0],[1,0,0,0]]";
        }

        /// <summary>
        ///     Formats the tool data.
        /// <para>
        ///     Tool data is used to describe the characteristics of a tool.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     RAPID component name: tooldata.
        /// </remarks>
        /// <param name="operation">Current operation.</param>
        /// <returns>Tool data.</returns>
        internal virtual string FormatToolData(CbtNode operation)
        {
            Matrix4X4 tcpFrameValue = OperationManager.GetTcpFrameValue(operation);
            Matrix3X3 toolFrameOrientation = tcpFrameValue.Orientation;
            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameOrientation = toolFrameOrientation * this.ToolFrameExtraRotation;
            }

            Quaternion quaternions = ConvertRotationRepresentation.RotationMatrixtoQuaternion(toolFrameOrientation);

            switch (AbbProcessorSwitches.GetToolDataOutputType(this.CellSettingsNode))
            {
                case 0:
                    return string.Empty;
                case 1:
                    return "  PERS tooldata " + this.FormatToolFrameName(operation) + ":=[" +
                           (this.IsRobotHoldingTheWorkObject ? "FALSE" : "TRUE") + ",[[" +
                           tcpFrameValue.Position.X.Formatted() + "," +
                           tcpFrameValue.Position.Y.Formatted() + "," +
                           tcpFrameValue.Position.Z.Formatted() + "],[" +
                           quaternions.W.QuaternionsFormatted() + "," +
                           quaternions.X.QuaternionsFormatted() + "," +
                           quaternions.Y.QuaternionsFormatted() + "," +
                           quaternions.Z.QuaternionsFormatted() + "]]," +
                           this.FormatLoadData() + "];";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the load data.
        /// <para>
        ///     Load data is used to describe loads attached to the mechanical interface of the robot (the robot’s mounting flange).
        /// </para>
        /// <para>
        ///     The tool load is specified in the tool data (<see cref="FormatToolData"/>) which includes load data.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     It is important to always define the actual tool load and when used, the payload of the robot too.
        ///     Incorrect definitions of load data can result in overloading of the robot mechanical structure.
        ///     When incorrect load data is specified, it can often lead to impaired path accuracy including a risk of overshooting and risk of overloading the mechanical structure.
        /// </remarks>
        /// <remarks>
        ///     RAPID component name: loaddata.
        /// </remarks>
        /// <returns>Formatted load data.</returns>
        internal virtual string FormatLoadData()
        {
            return "[" + AbbProcessorSwitches.GetToolDataLoadWeight(this.CellSettingsNode) + ",[" + AbbProcessorSwitches.GetToolDataCenterOfGravity(this.CellSettingsNode) + "],[" + AbbProcessorSwitches.GetToolDataAxesOfMoment(this.CellSettingsNode) + "]," + AbbProcessorSwitches.GetToolDataMomentsOfInertia(this.CellSettingsNode) + "]";
        }

        /// <summary>
        ///     Formats the work object name.
        /// </summary>
        /// <param name="operation">Current operation.</param>
        /// <returns>Formatted work object name.</returns>
        internal virtual string FormatWorkObjectName(CbtNode operation)
        {
            return AbbProcessorSwitches.GetIsAppendProgramNameToWorkObjectEnabled(this.CellSettingsNode)
                ? this.CurrentPostFile.FileName + "_" + AbbProcessorSwitches.GetWorkObjectDataName(this.CellSettingsNode) + OperationManager.GetUserFrame(operation).Number
                : AbbProcessorSwitches.GetWorkObjectDataName(this.CellSettingsNode) + OperationManager.GetUserFrame(operation).Number;
        }

        /// <summary>
        ///     Format the tool frame name.
        /// </summary>
        /// <param name="operation">Current operation.</param>
        /// <returns>Tool frame name.</returns>
        internal virtual string FormatToolFrameName(CbtNode operation)
        {
            return AbbProcessorSwitches.GetIsAppendProgramNameToToolDataEnabled(this.CellSettingsNode)
                ? this.CurrentPostFile.FileName + "_" + AbbProcessorSwitches.GetToolDataName(this.CellSettingsNode) + OperationManager.GetTcpId(operation).ToString()
                : AbbProcessorSwitches.GetToolDataName(this.CellSettingsNode) + OperationManager.GetTcpId(operation).ToString();
        }

        /// <inheritdoc/>
        internal override bool IsProgramInputValid()
        {
            const string mechanicalUnitPattern = "_eax_[a-f]$";

            // Verifies if the device has the "label" for the rail axis properly defined in the ROBX file.
            if (this.Rails.Any() && !this.Rails.All(x => Regex.IsMatch(x.Name, mechanicalUnitPattern)))
            {
                this.NotifyUser("WARNING: The rail external axes \"label\" tag in the ROBX file is not valid. Rail output will not follow brand convention or defined labels.", true, false);
            }

            // Verifies if the device has the "label" for the rotary axis properly defined in the ROBX file.
            if (this.Rotaries.Any() && !this.Rotaries.All(x => Regex.IsMatch(x.Name, mechanicalUnitPattern)))
            {
                this.NotifyUser("WARNING: The rotary external axes \"label\" tag in the ROBX file is not valid. Rotary output will not follow brand convention or defined labels.", true, false);
            }

            // The reference frame used is cell fixed and a child of the world. It has to be task fixed when using synchronized base. Thus, the table offset will be w.r.t. world.
            if (this.Rotaries.Any() && !this.IsUserCoordinateSystemFixed())
            {
                Logger.Info("WARNING: Robotic code is output for external axis synchronized motion. Make sure your controller is properly set up.");
            }

            // Verifies if the "user_frame_reference" in the cell extension file is found.
            if (this.Rotaries.Any() && SetupManager.GetConfiguration(this.SetupNode)?.UserFrameReferenceNode == null)
            {
                this.NotifyUser("WARNING: User frame reference is missing. The \"user_frame_reference\" tag in the cell extension file is not defined. Posting will have object frame set as the user frame.", true, false);
            }

            // The header output is different for static/dynamic and so are the points. The code can't support mixed use of dynamic/static user frames.
            if (this.Rotaries.Any() && !this.IsUserFrameSingleType())
            {
                this.NotifyUser("ERROR: The session contains mixed use of static and dynamic user frames.\r\nPlease use a single type. Posting will be stopped.", true, false);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether or not a fixed user coordinate system is used.
        ///     <para> TRUE:  ABB user frame output for fixed user coordinate system (static mode). </para>
        ///     <para> FALSE: ABB user frame output for movable user coordinate system (dynamic mode), i.e. coordinated external axes are used. </para>
        /// </summary>
        /// <returns><c>true</c> if static frame used otherwise, <c>false</c>.</returns>
        /// <remarks>
        ///     RAPID component name: ufprog.
        /// </remarks>
        internal virtual bool IsUserCoordinateSystemFixed()
        {
            IEnumerable<CbtNode> allOperationNodes = ProgramManager.GetOperationNodes(this.ProgramNode);

            foreach (CbtNode op in allOperationNodes)
            {
                // Variable checking if any of the user frame parent frame has a joint, meaning its not the world frame.
                bool isDynamicUserFrame = OperationManager.GetUserFrame(op).FrameNode.GetAllParents().Any(p => p.HasComponent<Joint>());

                // original output: OperationManager.GetUserFrame(op).Type == UserFrameType.CellFixed
                if (!isDynamicUserFrame)
                {
                    // Sets output for Static user frame header = TRUE
                    this.IsSynchronizedMode = false;
                    return true;
                }
            }

            this.IsSynchronizedMode = true;
            return false;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the session only uses User Frames of a single type: Dynamic or Static.
        /// </summary>
        /// <returns><c>true</c> if user frame used is of a single type otherwise, <c>false</c>.</returns>
        internal virtual bool IsUserFrameSingleType()
        {
            IEnumerable<CbtNode> allOperationNodes = ProgramManager.GetOperationNodes(this.ProgramNode);

            // Checks if within the operations the user frames are of the same type, all dynamic or all static.
            return allOperationNodes.All(op => OperationManager.GetUserFrame(op).FrameNode.GetAllParents().Any(p => p.HasComponent<Joint>())) ||
                   allOperationNodes.Select(op => !OperationManager.GetUserFrame(op).FrameNode.GetAllParents().Any(p => p.HasComponent<Joint>())).All(allStatic => allStatic);
        }
    }
}
