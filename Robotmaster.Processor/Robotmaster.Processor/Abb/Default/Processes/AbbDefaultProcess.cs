﻿// <copyright file="AbbDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Abb default process.
    /// </summary>
    [DataContract(Name = "AbbDefaultProcess")]
    public class AbbDefaultProcess : PackageProcess, IAbbProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbDefaultProcess"/> class.
        /// </summary>
        public AbbDefaultProcess()
        {
            this.Name = "ABB Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
