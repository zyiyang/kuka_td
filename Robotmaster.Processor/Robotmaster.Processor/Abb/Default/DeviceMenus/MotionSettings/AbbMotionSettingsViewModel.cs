// <copyright file="AbbMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     AbbMotionSettingsViewModel View Model.
    /// </summary>
    public class AbbMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;
        private static bool expandable4;
        private static bool expandable5;
        private static bool expandable6;
        private static bool expandable7;

        /// <summary>
        ///     Gets the zone.
        /// </summary>
        public static Dictionary<int, string> AbsoluteJointZoneTypeSource => new Dictionary<int, string>
        {
            { 0, "Fine" },
            { 1, "CustomZone" },
            { 2, "Z0" },
            { 3, "Z1" },
            { 4, "Z5" },
            { 5, "Z10" },
            { 6, "Z15" },
            { 7, "Z20" },
            { 8, "Z30" },
            { 9, "Z40" },
            { 10, "Z50" },
            { 11, "Z60" },
            { 12, "Z80" },
            { 13, "Z100" },
            { 14, "Z150" },
            { 15, "Z200" },
        };

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        public static Dictionary<int, string> AbsoluteJointSpeedTypeSource => new Dictionary<int, string>
        {
            { 0, "CustomVariable" },
            { 1, "CustomSpeed" },
            { 2, "V5" },
            { 3, "V10" },
            { 4, "V20" },
            { 5, "V30" },
            { 6, "V40" },
            { 7, "V50" },
            { 8, "V60" },
            { 9, "V80" },
            { 10, "V100" },
            { 11, "V200" },
            { 12, "V300" },
            { 13, "V400" },
            { 14, "V500" },
            { 15, "V600" },
            { 16, "V800" },
            { 17, "V1000" },
            { 18, "V1500" },
            { 19, "V2000" },
            { 20, "V2500" },
            { 21, "V3000" },
            { 22, "V4000" },
            { 23, "V5000" },
            { 24, "V6000" },
            { 25, "V7000" },
            { 26, "VMax" },
        };

        /// <summary>
        ///     Gets the zone.
        /// </summary>
        public static Dictionary<int, string> CartesianJointZoneTypeSource => new Dictionary<int, string>
        {
            { 0, "Fine" },
            { 1, "CustomZone" },
            { 2, "Z0" },
            { 3, "Z1" },
            { 4, "Z5" },
            { 5, "Z10" },
            { 6, "Z15" },
            { 7, "Z20" },
            { 8, "Z30" },
            { 9, "Z40" },
            { 10, "Z50" },
            { 11, "Z60" },
            { 12, "Z80" },
            { 13, "Z100" },
            { 14, "Z150" },
            { 15, "Z200" },
        };

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        public static Dictionary<int, string> CartesianJointSpeedTypeSource => new Dictionary<int, string>
        {
            { 0, "CustomVariable" },
            { 1, "CustomSpeed" },
            { 2, "V5" },
            { 3, "V10" },
            { 4, "V20" },
            { 5, "V30" },
            { 6, "V40" },
            { 7, "V50" },
            { 8, "V60" },
            { 9, "V80" },
            { 10, "V100" },
            { 11, "V200" },
            { 12, "V300" },
            { 13, "V400" },
            { 14, "V500" },
            { 15, "V600" },
            { 16, "V800" },
            { 17, "V1000" },
            { 18, "V1500" },
            { 19, "V2000" },
            { 20, "V2500" },
            { 21, "V3000" },
            { 22, "V4000" },
            { 23, "V5000" },
            { 24, "V6000" },
            { 25, "V7000" },
            { 26, "VMax" },
        };

        /// <summary>
        ///     Gets the zone.
        /// </summary>
        public static Dictionary<int, string> LinearZoneTypeSource => new Dictionary<int, string>
        {
            { 0, "Fine" },
            { 1, "CustomZone" },
            { 2, "Z0" },
            { 3, "Z1" },
            { 4, "Z5" },
            { 5, "Z10" },
            { 6, "Z15" },
            { 7, "Z20" },
            { 8, "Z30" },
            { 9, "Z40" },
            { 10, "Z50" },
            { 11, "Z60" },
            { 12, "Z80" },
            { 13, "Z100" },
            { 14, "Z150" },
            { 15, "Z200" },
        };

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        public static Dictionary<int, string> LinearSpeedTypeSource => new Dictionary<int, string>
        {
            { 0, "CustomVariable" },
            { 1, "CustomSpeed" },
            { 2, "V5" },
            { 3, "V10" },
            { 4, "V20" },
            { 5, "V30" },
            { 6, "V40" },
            { 7, "V50" },
            { 8, "V60" },
            { 9, "V80" },
            { 10, "V100" },
            { 11, "V200" },
            { 12, "V300" },
            { 13, "V400" },
            { 14, "V500" },
            { 15, "V600" },
            { 16, "V800" },
            { 17, "V1000" },
            { 18, "V1500" },
            { 19, "V2000" },
            { 20, "V2500" },
            { 21, "V3000" },
            { 22, "V4000" },
            { 23, "V5000" },
            { 24, "V6000" },
            { 25, "V7000" },
            { 26, "VMax" },
        };

        /// <summary>
        ///     Gets the zone.
        /// </summary>
        public static Dictionary<int, string> CircularZoneTypeSource => new Dictionary<int, string>
        {
            { 0, "Fine" },
            { 1, "CustomZone" },
            { 2, "Z0" },
            { 3, "Z1" },
            { 4, "Z5" },
            { 5, "Z10" },
            { 6, "Z15" },
            { 7, "Z20" },
            { 8, "Z30" },
            { 9, "Z40" },
            { 10, "Z50" },
            { 11, "Z60" },
            { 12, "Z80" },
            { 13, "Z100" },
            { 14, "Z150" },
            { 15, "Z200" },
        };

        /// <summary>
        ///     Gets the speed.
        /// </summary>
        public static Dictionary<int, string> CircularSpeedTypeSource => new Dictionary<int, string>
        {
            { 0, "CustomVariable" },
            { 1, "CustomSpeed" },
            { 2, "V5" },
            { 3, "V10" },
            { 4, "V20" },
            { 5, "V30" },
            { 6, "V40" },
            { 7, "V50" },
            { 8, "V60" },
            { 9, "V80" },
            { 10, "V100" },
            { 11, "V200" },
            { 12, "V300" },
            { 13, "V400" },
            { 14, "V500" },
            { 15, "V600" },
            { 16, "V800" },
            { 17, "V1000" },
            { 18, "V1500" },
            { 19, "V2000" },
            { 20, "V2500" },
            { 21, "V3000" },
            { 22, "V4000" },
            { 23, "V5000" },
            { 24, "V6000" },
            { 25, "V7000" },
            { 26, "VMax" },
        };

        /// <summary>
        ///     Gets the singularity avoidance control.
        /// </summary>
        public static Dictionary<int, string> SingularityAvoidanceControlTypeSource => new Dictionary<int, string>
        {
            { 0, "Off" },
            { 1, "Wrist" },
        };

        /// <summary>
        ///     Gets the linear configuration monitoring.
        /// </summary>
        public static Dictionary<int, string> LinearConfigurationMonitoringStatusSource => new Dictionary<int, string>
        {
            { 0, "Off" },
            { 1, "On" },
        };

        /// <summary>
        ///     Gets the linear configuration monitoring location.
        /// </summary>
        public static Dictionary<int, string> LinearConfigurationMonitoringLocationSource => new Dictionary<int, string>
        {
            { 0, "At start only" },
            { 1, "At start and end" },
        };

        /// <summary>
        ///     Gets or sets the custom speed data variable name for absolute joint motion.
        /// </summary>
        public string SpeedDataCustomVariableNameForAbsoluteJointMotion
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForAbsoluteJointMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForAbsoluteJointMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the custom speed data variable name for Cartesian joint motion.
        /// </summary>
        public string SpeedDataCustomVariableNameForCartesianJointMotion
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForCartesianJointMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForCartesianJointMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the custom speed data variable name for linear motion.
        /// </summary>
        public string SpeedDataCustomVariableNameForLinearMotion
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForLinearMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForLinearMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the custom speed data variable name for circular motion.
        /// </summary>
        public string SpeedDataCustomVariableNameForCircularMotion
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForCircularMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.SpeedDataCustomVariableNameForCircularMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the custom zone data variable name.
        /// </summary>
        public string ZoneDataCustomVariableName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ZoneDataCustomVariableName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ZoneDataCustomVariableName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the path zone size (the radius) for TCP in mm.
        ///     A corner path (parabola) is generated as soon as the edge of the zone is reached.
        ///     RAPID component name: pzone_tcp.
        /// </summary>
        public double ZoneDataCustomTcpPathSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomTcpPathSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomTcpPathSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the path zone size (the radius) for the tool reorientation in mm.
        ///     Reorientation starts as soon as the TCP reaches the zone.
        ///     Reorientation will be smoother if the zone size is increased, and there is less of a risk of having to reduce the velocity to carry out the reorientation.
        ///     RAPID component name: pzone_ori.
        /// </summary>
        public double ZoneDataCustomReorientationPathSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomReorientationPathSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomReorientationPathSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the path zone size (the radius) for external axes in mm.
        ///     External axes start to move towards the next position as soon as the TCP reaches the zone.
        ///     In this way, a slow axis can start accelerating at an earlier stage and thus execute more smoothly.
        ///     RAPID component name: pzone_eax.
        /// </summary>
        public double ZoneDataCustomExternalAxesPathSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomExternalAxesPathSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomExternalAxesPathSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the zone size for the tool reorientation in degrees.
        ///     RAPID component name: zone_ori.
        /// </summary>
        public double ZoneDataCustomReorientationSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomReorientationSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomReorientationSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the zone size for linear external axes in mm.
        ///     RAPID component name: zone_leax.
        /// </summary>
        public double ZoneDataCustomLinearExternalAxesSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomLinearExternalAxesSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomLinearExternalAxesSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the zone size for rotating external axes in degrees.
        ///     RAPID component name: zone_reax.
        /// </summary>
        public double ZoneDataCustomRotaryExternalAxesSize
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomRotaryExternalAxesSize)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ZoneDataCustomRotaryExternalAxesSize)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the reorientation velocity of the TCP expressed in degrees/s.
        ///     RAPID component name: v_ori.
        /// </summary>
        public int SpeedDataCustomTcpReorientationVelocity
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomTcpReorientationVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomTcpReorientationVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the velocity of linear external axes in mm/s.
        ///     RAPID component name: v_leax.
        /// </summary>
        public int SpeedDataCustomExternalAxesLinearVelocity
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomExternalAxesLinearVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomExternalAxesLinearVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the velocity of rotating external axes in degrees/s.
        ///     RAPID component name: v_reax.
        /// </summary>
        public int SpeedDataCustomExternalAxesRotationalVelocity
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomExternalAxesRotationalVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.SpeedDataCustomExternalAxesRotationalVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the acceleration and deceleration of joint moves as a percentage of the normal values.
        ///     Input value of 100 corresponds to 100% of the maximum acceleration and deceleration specified in the controller.
        ///     RAPID component name: Acc.
        /// </summary>
        public int AccelerationAndDecelerationForJointMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationForJointMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationForJointMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the rate at which acceleration and deceleration of joint moves increases as a percentage of the normal values.
        ///     Input value of 20 corresponds to 20% of the maximum rate specified in the controller.
        ///     RAPID component name: Ramp.
        /// </summary>
        public int AccelerationAndDecelerationRampForJointMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationRampForJointMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationRampForJointMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the acceleration and deceleration of linear and circular moves as a percentage of the normal values.
        ///     Input value of 100 corresponds to 100% of the maximum acceleration and deceleration specified in the controller.
        ///     RAPID component name: Acc.
        /// </summary>
        public int AccelerationAndDecelerationForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationForLinearAndCircularMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationForLinearAndCircularMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the rate at which acceleration and deceleration of linear and circular moves increases as a percentage of the normal values.
        ///     Input value of 20 corresponds to 20% of the maximum rate specified in the controller.
        ///     RAPID component name: Ramp.
        /// </summary>
        public int AccelerationAndDecelerationRampForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationRampForLinearAndCircularMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AccelerationAndDecelerationRampForLinearAndCircularMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the singularity avoidance control feature.
        /// </summary>
        public bool IsSingularityAvoidanceControlEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsSingularityAvoidanceControlEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsSingularityAvoidanceControlEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the linear configuration monitoring feature.
        /// </summary>
        public bool IsLinearConfigurationMonitoringEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearConfigurationMonitoringEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearConfigurationMonitoringEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether indicates whether the acceleration and deceleration override feature is used.
        ///     The acceleration and deceleration applies to both the robot and external axes until a new AccSet instruction is executed.
        ///     RAPID component name: AccSet.
        /// </summary>
        public bool IsAccelerationAndDecelerationOverrideEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAccelerationAndDecelerationOverrideEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAccelerationAndDecelerationOverrideEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the zone data for absolute joint motion.
        ///     Zone data describes the size of the generated corner path.
        ///     Zone data specifies how close to the programmed position the axes must be before moving towards the next position.
        /// </summary>
        public int AbsoluteJointZoneType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.AbsoluteJointZoneType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.AbsoluteJointZoneType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsZoneTypeCustomZone));
            }
        }

        /// <summary>
        ///     Gets or sets the speed data for absolute joint motion.
        ///     Speed data is used to specify the velocity at which both the robot and the external axes move.
        ///     If CustomSpeed selected, then speed value is taken from the operation in task.
        /// </summary>
        public int AbsoluteJointSpeedType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.AbsoluteJointSpeedType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.AbsoluteJointSpeedType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsAbsoluteJointSpeedTypeCustomVariable));
                this.OnPropertyChanged(nameof(this.IsSpeedTypeCustomSpeed));
            }
        }

        /// <summary>
        ///     Gets or sets the zone data for joint Cartesian motion.
        ///     Zone data describes the size of the generated corner path.
        ///     Zone data specifies how close to the programmed position the axes must be before moving towards the next position.
        /// </summary>
        public int CartesianJointZoneType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CartesianJointZoneType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CartesianJointZoneType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsZoneTypeCustomZone));
            }
        }

        /// <summary>
        ///     Gets or sets The speed data for joint Cartesian motion.
        ///     Speed data is used to specify the velocity at which both the robot and the external axes move.
        ///     If CustomSpeed selected, then speed value is taken from the operation in task.
        /// </summary>
        public int CartesianJointSpeedType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CartesianJointSpeedType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CartesianJointSpeedType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsCartesianJointSpeedTypeCustomVariable));
                this.OnPropertyChanged(nameof(this.IsSpeedTypeCustomSpeed));
            }
        }

        /// <summary>
        ///     Gets or sets the zone data for linear motion.
        ///     Zone data describes the size of the generated corner path.
        ///     Zone data specifies how close to the programmed position the axes must be before moving towards the next position.
        /// </summary>
        public int LinearZoneType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearZoneType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearZoneType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsZoneTypeCustomZone));
            }
        }

        /// <summary>
        ///     Gets or sets the speed data for linear motion.
        ///     Speed data is used to specify the velocity at which both the robot and the external axes move.
        ///     If CustomSpeed selected, then speed value is taken from the operation in task.
        /// </summary>
        public int LinearSpeedType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearSpeedType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearSpeedType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsLinearSpeedTypeCustomVariable));
                this.OnPropertyChanged(nameof(this.IsSpeedTypeCustomSpeed));
            }
        }

        /// <summary>
        ///     Gets or sets the zone data for circular motion.
        ///     Zone data describes the size of the generated corner path.
        ///     Zone data specifies how close to the programmed position the axes must be before moving towards the next position.
        /// </summary>
        public int CircularZoneType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularZoneType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularZoneType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsZoneTypeCustomZone));
            }
        }

        /// <summary>
        ///     Gets or sets the speed data for circular motion.
        ///     Speed data is used to specify the velocity at which both the robot and the external axes move.
        /// </summary>
        public int CircularSpeedType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularSpeedType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularSpeedType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsCircularSpeedTypeCustomVariable));
                this.OnPropertyChanged(nameof(this.IsSpeedTypeCustomSpeed));
            }
        }

        /// <summary>
        ///     Gets or sets the singularity avoidance control type.
        ///     The singularity avoidance control is used to define how the robot is to move in the proximity of singular points.
        ///     If Wrist selected, then the orientation of the tool may be changed slightly in order to pass a singular point.
        ///     RAPID component name: SingArea.
        /// </summary>
        public int SingularityAvoidanceControlType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.SingularityAvoidanceControlType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.SingularityAvoidanceControlType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear configuration monitoring status.
        ///     It is used to specify whether or not the robot’s configuration is to be monitored during linear or circular movement.
        ///     RAPID component name: ConfL.
        /// </summary>
        public int LinearConfigurationMonitoringStatus
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearConfigurationMonitoringStatus)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearConfigurationMonitoringStatus)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear configuration monitoring location.
        /// </summary>
        public int LinearConfigurationMonitoringLocation
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearConfigurationMonitoringLocation)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearConfigurationMonitoringLocation)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the absolute joint speed type is a custom variable.
        /// </summary>
        public bool IsAbsoluteJointSpeedTypeCustomVariable => this.AbsoluteJointSpeedType == 0;

        /// <summary>
        ///     Gets a value indicating whether the Cartesian joint speed type is a custom variable.
        /// </summary>
        public bool IsCartesianJointSpeedTypeCustomVariable => this.CartesianJointSpeedType == 0;

        /// <summary>
        ///     Gets a value indicating whether the linear speed type is a custom variable.
        /// </summary>
        public bool IsLinearSpeedTypeCustomVariable => this.LinearSpeedType == 0;

        /// <summary>
        ///     Gets a value indicating whether the circular speed type is a custom variable.
        /// </summary>
        public bool IsCircularSpeedTypeCustomVariable => this.CircularSpeedType == 0;

        /// <summary>
        ///     Gets a value indicating whether the speed type is a custom speed.
        /// </summary>
        public bool IsSpeedTypeCustomSpeed => this.AbsoluteJointSpeedType == 1 || this.CartesianJointSpeedType == 1 || this.LinearSpeedType == 1 || this.CircularSpeedType == 1;

        /// <summary>
        ///     Gets a value indicating whether the zone is a custom zone.
        /// </summary>
        public bool IsZoneTypeCustomZone => this.AbsoluteJointZoneType == 1 || this.CartesianJointZoneType == 1 || this.LinearZoneType == 1 || this.CircularZoneType == 1;

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable4"/> is checked.
        /// </summary>
        public bool IsExpandable4Checked
        {
            get => expandable4;

            set
            {
                expandable4 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable5"/> is checked.
        /// </summary>
        public bool IsExpandable5Checked
        {
            get => expandable5;

            set
            {
                expandable5 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable6"/> is checked.
        /// </summary>
        public bool IsExpandable6Checked
        {
            get => expandable6;

            set
            {
                expandable6 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable7"/> is checked.
        /// </summary>
        public bool IsExpandable7Checked
        {
            get => expandable7;

            set
            {
                expandable7 = value;
                this.OnPropertyChanged();
            }
        }
    }
}