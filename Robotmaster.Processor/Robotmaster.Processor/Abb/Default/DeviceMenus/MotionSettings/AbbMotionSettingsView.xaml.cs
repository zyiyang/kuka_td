// <copyright file="AbbMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for AbbMotionSettings.
    /// </summary>
    public partial class AbbMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbMotionSettingsView"/> class.
        /// </summary>
        public AbbMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="AbbMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public AbbMotionSettingsView(AbbMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public AbbMotionSettingsViewModel ViewModel { get; }
    }
}