// <copyright file="AbbMotionSettingsBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.DeviceMenus.MotionSettings
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing <see cref="AbbMotionSettingsView"/>.
    /// </summary>
    internal class AbbMotionSettingsBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the View Model.
        /// </summary>
        private AbbMotionSettingsViewModel ViewModel { get; set; }

        /// <summary>
        ///      Sets up the View Model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}