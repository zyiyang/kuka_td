// <copyright file="AbbProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for AbbProcessorSwitches.
    /// </summary>
    public partial class AbbProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbProcessorSwitchesView"/> class.
        /// </summary>
        public AbbProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="AbbProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public AbbProcessorSwitchesView(AbbProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public AbbProcessorSwitchesViewModel ViewModel { get; }
    }
}