﻿<?xml version="1.0" encoding="utf-8"?>
<ProcessPackage>
  <section ClassName="AbbProcessorSwitches">
    <SettingContract
      Label="ABB Processor Switches"
      LabelResourceAssembly="Robotmaster.Processor.dll"
      LabelResourceTypeName="Robotmaster.Processor.Abb.Default.AbbResources"
      LabelResourceVariableName="AbbProcessorSwitchesLabel"
      ContractKey="DM_ProcessorSwitchesAbb_C23T6789"
      Domain="Cell"
      InvalidDomains="Setup|Program|Operation"
      UiHandlerAssemblyName="Robotmaster.Processor.dll"
      UiHandlerBehaviorName="Robotmaster.Processor.Abb.Default.DeviceMenus.ProcessorSwitches.AbbProcessorSwitchesBehavior"
      DataHandlerAssemblyName="null"
      DataHandlerTypeName="null"
      DataHandlerMethodName="null"
      DataTypeAssemblyName="Robotmaster.Rise.dll"
      DataTypeName="Robotmaster.Rise.BaseClasses.Settings.ExternalMenuData"
      ImageUri="null"
      ImageKey="f0c1" />
    <CustomSettings>
      <Cat label="ABB - Processor Switches" enable="true" visible="true">
        <NumParam label="Program maximum length" type="integer" defVal="0" postVar="MaximumNumberOfPointsPerFile" enable="true" visible="true" UID="{EB434F5A-62E2-44B6-BAD1-9A81952B78AE}">
          <Desc>
            <Text line="The maximum number of points allowed per individual code file. If 0, then there is no maximum." />
          </Desc>
        </NumParam>
        <TxtParam label="Multi-file directory" defVal="HOME:" postVar="MultiFileDirectory" enable="true" visible="true" UID="{54A735EF-796F-4109-8D86-60892E2AD2C3}">
          <Desc>
            <Text line="The root directory from where the program module is loaded in program memory during execution." />
          </Desc>
        </TxtParam>
        <ListParam label="Work object data output type" defVal="1" postVar="WorkObjectDataOutputType" outputMode="index" enable="true" visible="true" UID="{BF5F2B8B-7935-4D6C-B5FB-1C7235B2505A}">
          <Desc>
            <Text line="The work object data output type." />
            <Text line="If by value, then the work object data is explicitly output in the header of the program." />
            <Text line="If by reference, then the work object data is not explicitly output in the header of the program. The work object data used will be referenced by its name found in the controller." />
            <Text line="RAPID component name: wobjdata." />
          </Desc>
          <List>
            <Item index="0" itemLabel="By reference" />
            <Item index="1" itemLabel="By value" />
          </List>
        </ListParam>
        <TxtParam label="Work object name" defVal="wobj" postVar="WorkObjectDataName" enable="true" visible="true" UID="{A7E0E13D-1B9D-41E8-9A67-5B3EB4A1AA22}">
          <Desc>
            <Text line="The work object data name prefix." />
            <Text line="The work object data suffix is the user frame number." />
          </Desc>
        </TxtParam>
        <BoolParam label="Append program name" defVal="false" postVar="IsAppendProgramNameToWorkObjectEnabled" enable="true" visible="true" UID="{5D318077-AF71-4431-9233-E78697053307}">
          <Desc>
            <Text line="Indicates whether the program name will be appended to the work object name output." />
            <Text line="Check this option to append the program name to the work object output." />
            <Text line="Example: program_1_wobj1." />
          </Desc>
        </BoolParam>
        <ListParam label="Tool data output type" defVal="1" postVar="ToolDataOutputType" outputMode="index" enable="true" visible="true" UID="{8286816F-FB42-4E3D-A477-5EFAD022C0F4}">
          <Desc>
            <Text line="The tool data output type." />
            <Text line="If by value, then the tool data is explicitly output in the header of the program." />
            <Text line="If by reference, then the tool data is not explicitly output in the header of the program. The tool data used will be referenced by its name found in the controller." />
            <Text line="RAPID component name: tooldata." />
          </Desc>
          <List>
            <Item index="0" itemLabel="By reference" />
            <Item index="1" itemLabel="By value" />
          </List>
        </ListParam>
        <TxtParam label="Tool data name" defVal="tool" postVar="ToolDataName" enable="true" visible="true" UID="{71E4384B-A719-46EE-AB20-FF5278A6F981}">
          <Desc>
            <Text line="The tool data name prefix." />
            <Text line="The tool data suffix is the tool frame number." />
          </Desc>
        </TxtParam>
        <BoolParam label="Append program name" defVal="false" postVar="IsAppendProgramNameToToolDataEnabled" enable="true" visible="true" UID="{C4AC8CC3-C659-4647-85C8-FF65ED3DE787}">
          <Desc>
            <Text line="Indicates whether the program name will be appended to the tool data name output." />
            <Text line="Check this option to append the program name to the tool data output." />
            <Text line="Example: program_1_tool1." />
          </Desc>
        </BoolParam>
        <NumParam label="Load weight" type="double" defVal="20" postVar="ToolDataLoadWeight" enable="true" visible="true" UID="{DC3EF853-8BDC-4243-9039-9B25F9D034C8}">
          <Desc>
            <Text line="The weight of the load attached to the flange in kg." />
            <Text line="RAPID component name: mass." />
          </Desc>
        </NumParam>
        <TxtParam label="Center of gravity [cox,coy,coz]" defVal="0,0,220" postVar="ToolDataCenterOfGravity" enable="true" visible="true" UID="{8B58C2AE-4614-45CD-900D-D86527A3AA27}">
          <Desc>
            <Text line="The center of gravity of the load attached to the flange." />
            <Text line="RAPID component name: cog." />
          </Desc>
        </TxtParam>
        <TxtParam label="Axes of moment [q1,q2,q3,q4]" defVal="1,0,0,0" postVar="ToolDataAxesOfMoment" enable="true" visible="true" UID="{501AE2D6-A3F7-4AB2-82EA-6158EC9A04B7}">
          <Desc>
            <Text line="The orientation of the tool load coordinate system defined by the inertial axes of the tool load." />
            <Text line="Expressed in the wrist coordinate system as a quaternion (q1, q2, q3, q4)." />
            <Text line="RAPID component name: aom." />
          </Desc>
        </TxtParam>
        <TxtParam label="Moments of inertia [ix,iy,iz]" defVal="0,0,0" postVar="ToolDataMomentsOfInertia" enable="true" visible="true" UID="{D003ADEA-1C60-4F7A-84C8-C9988C1252EB}">
          <Desc>
            <Text line="The moment of inertia of the load around the x,y,z-axis of the tool load coordinate system in kg·m²." />
            <Text line="RAPID component name: ix, iy, iz." />
          </Desc>
        </TxtParam>
      </Cat>
    </CustomSettings>
  </section>
</ProcessPackage>