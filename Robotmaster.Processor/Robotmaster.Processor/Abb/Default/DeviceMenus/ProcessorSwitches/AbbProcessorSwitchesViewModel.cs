// <copyright file="AbbProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     AbbProcessorSwitchesViewModel View Model.
    /// </summary>
    public class AbbProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;

        /// <summary>
        ///     Gets the work object data output type.
        /// </summary>
        public static Dictionary<int, string> WorkObjectDataOutputTypeSource => new Dictionary<int, string>
        {
            { 0, "By reference" },
            { 1, "By value" },
        };

        /// <summary>
        ///     Gets the tool data output type.
        /// </summary>
        public static Dictionary<int, string> ToolDataOutputTypeSource => new Dictionary<int, string>
        {
            { 0, "By reference" },
            { 1, "By value" },
        };

        /// <summary>
        ///     Gets or sets the root directory from where the program module is loaded in program memory during execution.
        /// </summary>
        public string MultiFileDirectory
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.MultiFileDirectory)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.MultiFileDirectory)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the work object data name prefix.
        ///     The work object data suffix is the user frame number.
        /// </summary>
        public string WorkObjectDataName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.WorkObjectDataName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.WorkObjectDataName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether indicates whether the program name will be appended to the work object name output.
        /// </summary>
        public bool IsAppendProgramNameToWorkObjectEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAppendProgramNameToWorkObjectEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAppendProgramNameToWorkObjectEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tool data name prefix.
        ///     The tool data suffix is the tool frame number.
        /// </summary>
        public string ToolDataName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether indicates whether the program name will be appended to the tool data name output.
        /// </summary>
        public bool IsAppendProgramNameToToolDataEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAppendProgramNameToToolDataEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsAppendProgramNameToToolDataEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the center of gravity of the load attached to the flange.
        ///     RAPID component name: cog.
        /// </summary>
        public string ToolDataCenterOfGravity
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataCenterOfGravity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataCenterOfGravity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the orientation of the tool load coordinate system defined by the inertial axes of the tool load.
        ///     Expressed in the wrist coordinate system as a quaternion (q1, q2, q3, q4).
        ///     RAPID component name: aom.
        /// </summary>
        public string ToolDataAxesOfMoment
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataAxesOfMoment)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataAxesOfMoment)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the moment of inertia of the load around the x,y,z-axis of the tool load coordinate system in kg·m².
        ///     RAPID component name: ix, iy, iz.
        /// </summary>
        public string ToolDataMomentsOfInertia
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataMomentsOfInertia)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolDataMomentsOfInertia)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum number of points allowed per individual code file. If 0, then there is no maximum.
        /// </summary>
        public int MaximumNumberOfPointsPerFile
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsProgramMultiFileOutput));
            }
        }

        /// <summary>
        ///     Gets or sets the weight of the load attached to the flange in kg.
        ///     RAPID component name: mass.
        /// </summary>
        public double ToolDataLoadWeight
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ToolDataLoadWeight)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ToolDataLoadWeight)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the work object data output type.
        ///     If by value, then the work object data is explicitly output in the header of the program.
        ///     If by reference, then the work object data is not explicitly output in the header of the program. The work object data used will be referenced by its name found in the controller.
        ///     RAPID component name: wobjdata.
        /// </summary>
        public int WorkObjectDataOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.WorkObjectDataOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.WorkObjectDataOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tool data output type.
        ///     If by value, then the tool data is explicitly output in the header of the program.
        ///     If by reference, then the tool data is not explicitly output in the header of the program. The tool data used will be referenced by its name found in the controller.
        ///     RAPID component name: tooldata.
        /// </summary>
        public int ToolDataOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolDataOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolDataOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the program is multi-file.
        /// </summary>
        public bool IsProgramMultiFileOutput => this.MaximumNumberOfPointsPerFile != 0;

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }
    }
}