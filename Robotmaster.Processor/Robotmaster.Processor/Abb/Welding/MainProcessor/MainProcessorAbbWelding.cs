﻿// <copyright file="MainProcessorAbbWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.MainProcessor
{
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Abb.Default.MainProcessor;
    using Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbTouchSensing;
    using Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbWelding;
    using Robotmaster.Processor.Abb.Welding.Processes.Events;
    using Robotmaster.Processor.Abb.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="MainProcessorAbb" />.
    ///     <para>
    ///         Implements the Abb Welding dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorAbbWelding : MainProcessorAbb
    {
        /// <summary>
        ///     Gets or sets a value for the current operation displacement frame.
        /// </summary>
        internal virtual int DisplacementFrameNumber { get; set; }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            base.EditOperationBeforeAllPointEdits(operationNode);

            // Checks for the application type to edit operation before all the points.
            switch (OperationManager.GetApplicationType(operationNode))
            {
                case ApplicationType.TouchSensing:
                    // Touch sensing operation.
                    this.EditTouchSensingOperation(operationNode);
                    break;
                case ApplicationType.Welding:
                    // Welding operation with or without referenced touch sensing.
                    this.EditWeldingWithReferencedTouchSensingOperation(operationNode);
                    break;
            }
        }

        /// <inheritdoc />
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Checks for the application type.
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool touchSensingOperation = applicationType == ApplicationType.TouchSensing;

            if (weldingOperation)
            {
                //// Adds welding events.
                //// Example :
                ////    JC X, Y, Z, I, J, K
                ////    ArcStart, seam1, weld1
                ////    LC X, Y, Z, I, J, K
                point = PointManager.AddEventToPathPoint(
                    new ArcStart
                    {
                        SeamdataTemplate = AbbWeldingAuxiliaryMenu.GetSeamdataTemplate(operationNode),
                        WelddataTemplate = AbbWeldingAuxiliaryMenu.GetWelddataTemplate(operationNode),
                    },
                    EventIndex.Inline,
                    point,
                    operationNode);

                ////    JC X, Y, Z, I, J, K
                ////    ArcStart, seam1, weld1
                ////    weave1 On
                ////    LC X, Y, Z, I, J, K
                if (AbbWeldingAuxiliaryMenu.GetIsWeavingEnabled(operationNode) == true)
                {
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOn
                        {
                            WeaveOnTemplate = AbbWeldingAuxiliaryMenu.GetWeavedataTemplate(operationNode),
                        },
                        EventIndex.Inline,
                        point,
                        operationNode);
                }

                ////    JC X, Y, Z, I, J, K
                ////    ArcStart, seam1, weld1
                ////    weave1 On
                ////    track1 On
                ////    LC X, Y, Z, I, J, K
                if (AbbWeldingAuxiliaryMenu.GetIsTrackingEnabled(operationNode) == true)
                {
                    point = PointManager.AddEventToPathPoint(
                        new TrackOn
                        {
                            TrackOnTemplate = AbbWeldingAuxiliaryMenu.GetTrackdataTemplate(operationNode),
                        },
                        EventIndex.Inline,
                        point,
                        operationNode);
                }
            }

            if (touchSensingOperation)
            {
                // Adds touch sensing events.
                // Example:
                // Search_1D robM1
                point = PointManager.AddEventToPathPoint(
                    new Search1D()
                    {
                        // Adds to point list the displacement frame prefix and current touch operation displacement frame number.
                        Search1DDisplacementFrame = AbbWeldingProcessMenu.GetDisplacementFramePrefix(this.ProgramNode),
                        Search1DDisplacementFrameNumber = this.DisplacementFrameNumber,
                    },
                    EventIndex.Inline,
                    point,
                    operationNode);
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Checks for the application type.
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            bool weldingOperation = applicationType == ApplicationType.Welding;

            if (weldingOperation)
            {
                ////    LC X, Y, Z, I, J, K
                ////    ArcEnd
                ////    LC X, Y, Z, I, J, K
                point = PointManager.AddEventToPathPoint(
                    new ArcEnd { },
                    EventIndex.Inline,
                    point,
                    operationNode);

                ////    LC X, Y, Z, I, J, K
                ////    ArcEnd
                ////    weave1 Off
                ////    LC X, Y, Z, I, J, K
                if (AbbWeldingAuxiliaryMenu.GetIsWeavingEnabled(operationNode) == true)
                {
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOff
                        {
                            WeaveOffTemplate = AbbWeldingAuxiliaryMenu.GetWeavedataTemplate(operationNode),
                        },
                        EventIndex.Inline,
                        point,
                        operationNode);
                }

                ////    LC X, Y, Z, I, J, K
                ////    ArcEnd
                ////    weave1 Off
                ////    track1 Off
                ////    LC X, Y, Z, I, J, K
                if (AbbWeldingAuxiliaryMenu.GetIsTrackingEnabled(operationNode) == true)
                {
                    point = PointManager.AddEventToPathPoint(
                        new TrackOff
                        {
                            TrackOffTemplate = AbbWeldingAuxiliaryMenu.GetTrackdataTemplate(operationNode),
                        },
                        EventIndex.Inline,
                        point,
                        operationNode);
                }
            }

            return point;
        }

        /// <summary>
        ///     Edits the current touch sensing operation.
        /// </summary>
        /// <param name="operationNode">Current touch sensing operation.</param>
        internal virtual void EditTouchSensingOperation(CbtNode operationNode)
        {
            // Calculates the displacement frame number to be used for this touch sensing operation.
            this.DisplacementFrameNumber = this.GetNextAvailableDisplacementFrameNumber(operationNode);

            // Sets the touch sensing displacement frame number and stores inside the auxiliary menu of the current touch sensing operation.
            AbbTouchSensingAuxiliaryMenu.SetDisplacementFrameNumber(operationNode, this.DisplacementFrameNumber);
        }

        /// <summary>
        ///     Edits the current welding (with or without referenced touch sensing) operation.
        /// </summary>
        /// <param name="operationNode">Current welding (with or without referenced touch sensing) operation.</param>
        internal virtual void EditWeldingWithReferencedTouchSensingOperation(CbtNode operationNode)
        {
            CbtNode touchSensingReferencedOperationNode = OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot);

            // Calculates the displacement frame number used if a touch sensing operation is referenced.
            if (touchSensingReferencedOperationNode != null)
            {
                switch (OperationManager.GetApplicationType(touchSensingReferencedOperationNode))
                {
                    case ApplicationType.TouchSensing:
                        this.DisplacementFrameNumber = AbbTouchSensingAuxiliaryMenu.GetDisplacementFrameNumber(touchSensingReferencedOperationNode);
                        break;
                    default:
                        this.NotifyUser("WARNING: " + OperationManager.GetOperationName(operationNode) + " references an invalid operation. Default offset registry will be applied", true);
                        this.DisplacementFrameNumber = AbbWeldingProcessMenu.GetDisplacementFrameStartNumber(this.ProgramNode);
                        break;
                }

                // Adds PDispSet to the first point of current welding with referenced touch sensing operation.
                PointManager.AddEventToPathPoint(
                    new PDispSet()
                    {
                        PDispSetDisplacementFrame = AbbWeldingProcessMenu.GetDisplacementFramePrefix(this.ProgramNode),
                        PDispSetDisplacementFrameNumber = this.DisplacementFrameNumber,
                    },
                    EventIndex.Before,
                    OperationManager.GetFirstPoint(operationNode),
                    operationNode);

                // Adds PDispOff event to the last point of current welding with referenced touch sensing operation.
                PointManager.AddEventToPathPoint(new PDispOff(), EventIndex.Before, OperationManager.GetLastPoint(operationNode), operationNode);
            }
            else
            {
                // If the current operation does not use referenced touch sensing, then no displacement frame is applied.
            }
        }

        /// <summary>
        ///     Gets the next displacement frame number to use for the current touch sensing operation.
        /// </summary>
        /// <param name="operationNode">The current operation which needs a new displacement frame number.</param>
        /// <returns>The calculated displacement frame number.</returns>
        internal virtual int GetNextAvailableDisplacementFrameNumber(CbtNode operationNode)
        {
            // Gets the displacement frame starting number.
            int displacementStartingNumber = AbbWeldingProcessMenu.GetDisplacementFrameStartNumber(this.ProgramNode);

            // If displacement frame numbering method is set to increment it increments the number at every subsequent touch operation.
            if (AbbWeldingProcessMenu.GetDisplacementFrameNumberingMethod(this.ProgramNode) == 1)
            {
                // Gets the number of previous touch operation before the current operation.
                int previousTouchSensingOperationCount = SetupManager.GetOperationNodes(this.SetupNode).TakeWhile(op => op != operationNode)
                    .Count(op => OperationManager.GetApplicationType(op) == ApplicationType.TouchSensing);

                // Shifts the displacement frame number by the calculated count.
                displacementStartingNumber += previousTouchSensingOperationCount;
            }

            return displacementStartingNumber;
        }
    }
}