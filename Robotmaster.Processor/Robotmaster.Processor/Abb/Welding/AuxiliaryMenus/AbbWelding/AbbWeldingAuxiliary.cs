﻿// <copyright file="AbbWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "AbbWeldingAuxiliary")]
    public class AbbWeldingAuxiliary : AuxiliaryMenu, IAbbProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbWeldingAuxiliary"/> class.
        /// </summary>
        public AbbWeldingAuxiliary()
        {
            this.Name = "Abb Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "AbbWeldingAuxiliaryMenu";
        }
    }
}
