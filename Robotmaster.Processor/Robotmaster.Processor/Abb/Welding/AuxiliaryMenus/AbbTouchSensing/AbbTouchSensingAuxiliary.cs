﻿// <copyright file="AbbTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbTouchSensing
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "AbbTouchSensingAuxiliary")]
    public class AbbTouchSensingAuxiliary : AuxiliaryMenu, IAbbProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AbbTouchSensingAuxiliary"/> class.
        /// </summary>
        public AbbTouchSensingAuxiliary()
        {
            this.Name = "Abb Touch Sensing";
            this.ApplicationType = ApplicationType.TouchSensing;
            this.AuxiliaryMenuFileName = "AbbTouchSensingAuxiliaryMenu";
        }
    }
}
