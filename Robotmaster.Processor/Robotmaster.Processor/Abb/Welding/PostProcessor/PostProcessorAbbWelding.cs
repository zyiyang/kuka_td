// <copyright file="PostProcessorAbbWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.PostProcessor
{
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Abb.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Abb.Default.PostProcessor;
    using Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbTouchSensing;
    using Robotmaster.Processor.Abb.Welding.AuxiliaryMenus.AbbWelding;
    using Robotmaster.Processor.Abb.Welding.Processes.Events;
    using Robotmaster.Processor.Abb.Welding.Processes.ProcessMenus;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;

    /// <summary>
    ///     This class inherits from <see cref="PostProcessorAbb"/>.
    ///     <para>
    ///         Implements the ABB welding dedicated properties and methods of the post-processor in order to output ABB welding robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorAbbWelding : PostProcessorAbb
    {
        /// <summary>
        ///     Gets or sets the list containing all the displacement frames to be initialized.
        /// </summary>
        internal virtual List<string> DisplacementFrameInitializerList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets a value indicating whether the point is inside the welding process.
        /// </summary>
        protected bool IsInsideWelding { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the ArcStart event is present.
        /// </summary>
        protected bool IsArcStartEventPresent { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the ArcEnd event is present.
        /// </summary>
        protected bool IsArcEndEventPresent { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the Search_1D event is present.
        /// </summary>
        protected bool IsSearch1DEventPresent { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether there are accumulated searches in a single operation.
        /// </summary>
        protected bool IsSearch1DAccumulated { get; set; }

        /// <inheritdoc />
        internal override void EndMainPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            base.EndMainPostFile(postFile, operationNode, currentPoint);
            this.FormatDisplacementFrameInitializerList(postFile);
        }

        /// <inheritdoc />
        internal override void RunAfterProgramOutput()
        {
            // Copied entire logic from abb default post processor to call the displacement frame initializer list inside the if condition.
            base.RunAfterProgramOutput();

            CbtNode lastOperation = ProgramManager.GetOperationNodes(this.ProgramNode).Last();
            PathNode lastPoint = OperationManager.GetLastPoint(lastOperation);

            if (AbbProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) <= 0)
            {
                this.FormatCustomZoneInitializerList(this.CurrentPostFile);
                this.FormatWorkObjectInitializerList(this.CurrentPostFile);
                this.FormatToolFrameInitializerList(this.CurrentPostFile);

                // Formats the touch sensing displacement frames to be output.
                // Single change done in this method.
                this.FormatDisplacementFrameInitializerList(this.CurrentPostFile);
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);
            }
            else
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);

                // Multi-file output
                PostFile parentPostFile = this.CurrentPostFile.Parent;

                // While root is not reached
                while (!ReferenceEquals(parentPostFile, this.MainPostFile.Parent))
                {
                    // End main post-file
                    this.EndMainPostFile(parentPostFile, lastOperation, lastPoint);
                    parentPostFile = parentPostFile.Parent;
                }
            }
        }

        /// <inheritdoc />
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            base.RunBeforeOperationOutput(operationNode);

            // Sets accumulated search variable to false before the operations
            this.IsSearch1DAccumulated = false;

            // Checks for the application type
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            bool touchSensingOperation = applicationType == ApplicationType.TouchSensing;

            if (touchSensingOperation)
            {
                // Adds each displacement frame to the displacement frame list.
                this.AddDisplacementFrameToDisplacementFrameInitializerList(operationNode);
            }
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            base.RunBeforePointOutput(point, operationNode);

            // Checks for all events at point level.
            bool isEventListInlinePresent = point.Events() != null && point.Events().EventListInline != null;
            this.IsSearch1DEventPresent = isEventListInlinePresent && point.Events().EventListInline.Any(p => p is Search1D);
            this.IsArcStartEventPresent = isEventListInlinePresent && point.Events().EventListInline.Any(p => p is ArcStart);
            this.IsArcEndEventPresent = isEventListInlinePresent && point.Events().EventListInline.Any(p => p is ArcEnd);
        }

        /// <inheritdoc />
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            base.RunAfterPointOutput(point, operationNode);

            // Sets the IsInsideWelding variable to false after the ArcEnd event.
            if (this.IsArcEndEventPresent)
            {
                this.IsInsideWelding = false;
            }
        }

        /// <inheritdoc />
        internal override string FormatMotionType(PathNode point)
        {
            // Outputs motion type for the ArcStart events.
            if (this.IsArcStartEventPresent)
            {
                // Activates welding output for subsequent points after the ArcStart event.
                this.IsInsideWelding = true;
                return point.MoveType() == MoveType.Linear ? "    ArcLStart " : "    ArcCStart ";
            }

            // Outputs motion type for the ArcEnd events.
            // Logic needs to be rechecked at this stage as we are passing the point.NextPoint(operationNode) for Circular moves.
            if (point.Events() != null && point.Events().EventListInline != null && point.Events().EventListInline.Any(p => p is ArcEnd))
            {
                return point.MoveType() == MoveType.Linear ? "    ArcLEnd " : "    ArcCEnd ";
            }

            // Outputs motion type for the in between events welding, ArcL and ArcC.
            if (this.IsInsideWelding)
            {
                switch (point.MoveType())
                {
                    case MoveType.Linear:
                        return "    ArcL ";
                    case MoveType.Circular:
                        return "    ArcC ";
                    case MoveType.Spline:
                    case MoveType.Rapid:
                        return string.Empty;
                    default:
                        return string.Empty;
                }
            }

            return base.FormatMotionType(point);
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            // ArcL move example
            // ArcL [[74.21,-110.00,0.71],[0,0.9238795,0,-0.3826834],[-1,-1,-1,0],[9E9,9E9,9E9,9E9,9E9,9E9]],[125.00,500,5000,1000],seam1,weld1\Weave:=weave1,z5,Torch1\Wobj:=wob1\Track:=track1;.
            this.Moves.WriteLine(this.FormatMotionType(point) + "[" +
            this.FormatPosition(point, operationNode) + "," +
            this.FormatOrientation(point, operationNode) + "," +
            this.TurnsAndConfig(point) + "," +
            this.FormatExternalAxesValues(point) + "]," +
            this.FormatSpeedData(point, operationNode) + "," +
            this.FormatSeamData(point, operationNode) +
            this.FormatWeldData(point, operationNode) +
            this.FormatZoneData(point, operationNode) + "," +
            this.FormatToolFrameName(operationNode) +
            "\\Wobj:=" + this.FormatWorkObjectName(operationNode) +
            this.FormatTrackData(point, operationNode) + ";");
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            // ArcC move example
            // ArcC [[-328.66,5.66,-200.00],[0,1,0,0],[0,0,-1,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[[-331.00,0.00,-200.00],[0,1,0,0],[0,0,0,1],[9E9,9E9,9E9,9E9,9E9,9E9]],[50.00,500,5000,1000],seam1,weld1\Weave:=weave1,z5,Torch1\Wobj:=wob1\Track:=track1;.
            if (point.IsArcMiddlePoint())
            {
                this.Moves.Write(this.FormatMotionType(point.NextPoint(operationNode)) + "[" +
                                 this.FormatPosition(point, operationNode) + "," +
                                 this.FormatOrientation(point, operationNode) + "," +
                                 this.TurnsAndConfig(point) + "," +
                                 this.FormatExternalAxesValues(point) + "],");
            }
            else
            {
                this.Moves.WriteLine("[" +
                                     this.FormatPosition(point, operationNode) + "," +
                                     this.FormatOrientation(point, operationNode) + "," +
                                     this.TurnsAndConfig(point) + "," +
                                     this.FormatExternalAxesValues(point) + "]," +
                                     this.FormatSpeedData(point, operationNode) + "," +
                                     this.FormatSeamData(point, operationNode) +
                                     this.FormatWeldData(point, operationNode) +
                                     this.FormatZoneData(point, operationNode) + "," +
                                     this.FormatToolFrameName(operationNode) +
                                     "\\Wobj:=" + this.FormatWorkObjectName(operationNode) +
                                     this.FormatTrackData(point, operationNode) + ";");
            }
        }

        /// <inheritdoc />
        internal override void OutputMove(PathNode point, CbtNode operationNode)
        {
            // Checks for the application type.
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            bool touchSensingOperation = applicationType == ApplicationType.TouchSensing;

            if (touchSensingOperation)
            {
                if (this.IsSearch1DEventPresent)
                {
                    // Search_1D instruction example.
                    // Search_1D robM1,[[93.50,-23.25,30.00],[0.1759199,-0.8204732,0.3398511,0.4247082],[-1,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]],[[93.50,-23.25,0.00],[0.1759199,-0.8204732,0.3398511,0.4247082],[-1,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]],[1000.00,500,5000,1000],Torch1\Wobj:=wob1;
                    this.Moves.WriteLine("    Search_1D " + this.FormatDisplacementFrameName(operationNode) + ",[" +
                                         this.FormatPosition(point, operationNode) + "," +
                                         this.FormatOrientation(point, operationNode) + "," +
                                         this.TurnsAndConfig(point) + "," +
                                         this.FormatExternalAxesValues(point) + "]," +
                                         this.FormatPosition(point.NextPoint(operationNode), operationNode) + "," +
                                         this.FormatOrientation(point.NextPoint(operationNode), operationNode) + "," +
                                         this.TurnsAndConfig(point.NextPoint(operationNode)) + "," +
                                         this.FormatExternalAxesValues(point.NextPoint(operationNode)) + "]," +
                                         this.FormatSpeedData(point, operationNode) + "," +
                                         this.FormatToolFrameName(operationNode) +
                                         "\\Wobj:=" + this.FormatWorkObjectName(operationNode) +
                                         this.FormatAccumulatedSearch1D(operationNode) + ";");

                    // Sets variable to accumulate touch senses to true after the first Search_1D instruction.
                    this.IsSearch1DAccumulated = true;
                    return;
                }

                if (point.PathFlag() == PathFlag.LastPointOfProcess)
                {
                    // Outputs touch sense linear retract as a standard MoveL after the Search_1D instruction.
                    base.OutputMove(point, operationNode);
                    return;
                }

                if (point.PathFlag() == PathFlag.OutsideOfProcess)
                {
                    // Output any points outside Search_1D instruction.
                    base.OutputMove(point, operationNode);
                }
            }
            else
            {
                // Output any points from other operation types normally.
                base.OutputMove(point, operationNode);
            }
        }

        /// <summary>
        ///     Formats seamdata output.
        /// <para>
        ///     Seam data describes the start and end phases of a welding process.
        /// </para>
        /// </summary>
        internal virtual string FormatSeamData(PathNode point, CbtNode operationNode)
        {
            return this.IsInsideWelding
                ? AbbWeldingAuxiliaryMenu.GetSeamdataTemplate(operationNode) + ","
                : string.Empty;
        }

        /// <summary>
        ///     Formats welddata output.
        /// <para>
        ///     Weld data describes the weld phase of the welding process.
        /// </para>
        /// </summary>
        internal virtual string FormatWeldData(PathNode point, CbtNode operationNode)
        {
            // Outputs welddata and weavedata together, as they are a combined parameter in ABB.
            return this.IsInsideWelding
                ? AbbWeldingAuxiliaryMenu.GetWelddataTemplate(operationNode) + this.FormatWeaveData(point, operationNode) + ","
                : string.Empty;
        }

        /// <summary>
        ///     Formats weavedata output.
        /// <para>
        ///     Weave data describes the weaving that is to take place during the heat and weld phases.
        /// </para>
        /// </summary>
        internal virtual string FormatWeaveData(PathNode point, CbtNode operationNode)
        {
            return AbbWeldingAuxiliaryMenu.GetIsWeavingEnabled(operationNode)
                ? "\\Weave:=" + AbbWeldingAuxiliaryMenu.GetWeavedataTemplate(operationNode)
                : string.Empty;
        }

        /// <summary>
        ///     Formats trackdata output.
        /// <para>
        ///     Track data describes the parameters used for tracking.
        /// </para>
        /// </summary>
        internal virtual string FormatTrackData(PathNode point, CbtNode operationNode)
        {
            return this.IsInsideWelding && AbbWeldingAuxiliaryMenu.GetIsTrackingEnabled(operationNode)
                ? "\\Track:=" + AbbWeldingAuxiliaryMenu.GetTrackdataTemplate(operationNode)
                : string.Empty;
        }

        /// <inheritdoc />
        internal override string FormatZoneData(PathNode point, CbtNode operationNode)
        {
            return this.IsArcStartEventPresent || this.IsArcEndEventPresent
                ? "fine"
                : base.FormatZoneData(point, operationNode);
        }

        /// <summary>
        ///     Formats the accumulated Search_1D output.
        /// <para>
        ///     The PrePDisp feature is used to add the current touch sense to the previous touch sense.
        /// </para>
        /// </summary>
        internal virtual string FormatAccumulatedSearch1D(CbtNode operationNode)
        {
            return this.IsSearch1DAccumulated
                ? "\\PrePDisp:=" + this.FormatDisplacementFrameName(operationNode)
                : string.Empty;
        }

        /// <summary>
        ///     Formats the displacement frame name.
        /// <para>
        ///     The displacement frame is a pose type variable used to store the touch sense information.
        /// </para>
        /// </summary>
        internal virtual string FormatDisplacementFrameName(CbtNode operationNode)
        {
            return AbbWeldingProcessMenu.GetDisplacementFramePrefix(this.ProgramNode) +
                   AbbTouchSensingAuxiliaryMenu.GetDisplacementFrameNumber(operationNode);
        }

        /// <summary>
        ///     Formats the displacement frame pose to be output at header declaration and the displacement frame value reset list.
        /// <para>
        ///     The displacement frame is a pose type variable used to store the touch sense information.
        /// </para>
        /// </summary>
        internal virtual string FormatDisplacementFramePoseDeclaration(CbtNode operationNode)
        {
            switch (AbbWeldingProcessMenu.GetDisplacementFrameDeclaration(this.ProgramNode))
            {
                case 0:
                    // Formats the displacement frame when there is no pose variable declaration.
                    return "    " + this.FormatDisplacementFrameName(operationNode) + ":=[[0,0,0],[1,0,0,0]];";
                case 1:
                    // Formats the displacement frame when there is a pose variable declaration.
                    return "  PERS pose " + this.FormatDisplacementFrameName(operationNode) + ":=[[0,0,0],[1,0,0,0]];";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the displacement frame list.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to end.</param>
        internal virtual void FormatDisplacementFrameInitializerList(PostFile postFile)
        {
            if (this.DisplacementFrameInitializerList != null && this.DisplacementFrameInitializerList.Any())
            {
                if (AbbWeldingProcessMenu.GetDisplacementFrameDeclaration(this.ProgramNode) == 1)
                {
                    // Outputs the displacement frames variables declaration at the start of the file.
                    postFile.FileSection.Header += string.Join("\r\n", this.DisplacementFrameInitializerList.ToArray()) + "\r\n";
                    this.DisplacementFrameInitializerList.Clear();
                }
                else
                {
                    // Outputs the displacement frames reset at the start of the process.
                    postFile.FileSection.SubFileSections[1].Header += string.Join("\r\n", this.DisplacementFrameInitializerList.ToArray()) + "\r\n";
                    this.DisplacementFrameInitializerList.Clear();
                }
            }
        }

        /// <summary>
        ///     Adds displacement frame to the <see cref="DisplacementFrameInitializerList"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void AddDisplacementFrameToDisplacementFrameInitializerList(CbtNode operationNode)
        {
            if (!this.DisplacementFrameInitializerList.Contains(this.FormatDisplacementFramePoseDeclaration(operationNode)))
            {
                // Adds each operation displacement frame to the list.
                this.DisplacementFrameInitializerList.Add(this.FormatDisplacementFramePoseDeclaration(operationNode));
            }
        }
    }
}