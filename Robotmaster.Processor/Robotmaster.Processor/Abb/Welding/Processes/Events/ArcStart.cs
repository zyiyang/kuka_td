﻿// <copyright file="ArcStart.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     ArcStart event.
    /// </summary>
    [DataContract(Name = "AbbArcStart")]
    public class ArcStart : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ArcStart"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ArcStart()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the welding Seamdata template.
        /// </summary>
        [DataMember(Name = "SeamdataTemplate")]
        public virtual string SeamdataTemplate { get; set; } = "seam1";

        /// <summary>
        ///     Gets or sets the welding Welddata template.
        /// </summary>
        [DataMember(Name = "WelddataTemplate")]
        public virtual string WelddataTemplate { get; set; } = "weld1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"ArcStart, {this.SeamdataTemplate}, {this.WelddataTemplate}";
        }
    }
}
