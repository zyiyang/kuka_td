﻿// <copyright file="TrackOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Track ON event.
    /// </summary>
    [DataContract(Name = "AbbTrackOn")]
    public class TrackOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TrackOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public TrackOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the seam tracking ON template.
        /// </summary>
        [DataMember(Name = "TrackOnTemplate")]
        public virtual string TrackOnTemplate { get; set; } = "track1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.TrackOnTemplate} On";
        }
    }
}
