﻿// <copyright file="PDispOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     PDispOff event.
    /// </summary>
    [DataContract(Name = "AbbPDispOff")]
    public class PDispOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PDispOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public PDispOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before;
        }

        /// <summary>
        ///     Display displacement frame deactivation in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "PDispOff";
        }
    }
}
