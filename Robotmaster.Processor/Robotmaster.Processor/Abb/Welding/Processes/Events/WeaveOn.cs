﻿// <copyright file="WeaveOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weave ON event.
    /// </summary>
    [DataContract(Name = "AbbWeaveOn")]
    public class WeaveOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeaveOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeaveOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the weave ON template.
        /// </summary>
        [DataMember(Name = "WeaveOnTemplate")]
        public virtual string WeaveOnTemplate { get; set; } = "weave1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeaveOnTemplate} On";
        }
    }
}
