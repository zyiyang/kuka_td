﻿// <copyright file="ArcEnd.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     ArcEnd event.
    /// </summary>
    [DataContract(Name = "AbbArcEnd")]
    public class ArcEnd : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ArcEnd"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public ArcEnd()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "ArcEnd";
        }
    }
}
