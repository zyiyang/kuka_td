﻿// <copyright file="Search1D.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Search 1D event.
    /// </summary>
    [DataContract(Name = "AbbSearch1D")]
    public class Search1D : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Search1D"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public Search1D()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the displacement frame prefix.
        /// </summary>
        [DataMember(Name = "Search1DDisplacementFrame")]
        public virtual string Search1DDisplacementFrame { get; set; } = "robM";

        /// <summary>
        ///     Gets or sets the displacement frame number.
        /// </summary>
        [DataMember(Name = "Search1DDisplacementFrameNumber")]
        public virtual int Search1DDisplacementFrameNumber { get; set; } = 1;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{"Search_1D " + this.Search1DDisplacementFrame + this.Search1DDisplacementFrameNumber}";
        }
    }
}
