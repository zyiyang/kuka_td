﻿// <copyright file="WeaveOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weave OFF event.
    /// </summary>
    [DataContract(Name = "AbbWeaveOff")]
    public class WeaveOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeaveOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeaveOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the weave OFF template.
        /// </summary>
        [DataMember(Name = "WeaveOffTemplate")]
        public virtual string WeaveOffTemplate { get; set; } = "weave1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeaveOffTemplate} Off";
        }
    }
}
