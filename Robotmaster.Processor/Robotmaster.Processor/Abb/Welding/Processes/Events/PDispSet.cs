﻿// <copyright file="PDispSet.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     PDispSet event.
    /// </summary>
    [DataContract(Name = "AbbPDispSet")]
    public class PDispSet : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PDispSet"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public PDispSet()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before;
        }

        /// <summary>
        ///     Gets or sets the displacement frame prefix for shift activation.
        /// </summary>
        [DataMember(Name = "PDispSetDisplacementFrame")]
        public virtual string PDispSetDisplacementFrame { get; set; } = "robM";

        /// <summary>
        ///     Gets or sets the displacement frame number for shift activation.
        /// </summary>
        [DataMember(Name = "PDispSetDisplacementFrameNumber")]
        public virtual int PDispSetDisplacementFrameNumber { get; set; } = 1;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{"PDispSet " + this.PDispSetDisplacementFrame + this.PDispSetDisplacementFrameNumber}";
        }
    }
}
