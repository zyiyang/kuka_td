﻿// <copyright file="TrackOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Track OFF event.
    /// </summary>
    [DataContract(Name = "AbbTrackOff")]
    public class TrackOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TrackOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public TrackOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the seam tracking OFF template.
        /// </summary>
        [DataMember(Name = "TrackOffTemplate")]
        public virtual string TrackOffTemplate { get; set; } = "track1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.TrackOffTemplate} Off";
        }
    }
}
