﻿// <copyright file="AbbWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Abb.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Abb.Welding.MainProcessor;
    using Robotmaster.Processor.Abb.Welding.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Abb welding process.
    /// </summary>
    [DataContract(Name = "AbbWeldingProcess")]
    public class AbbWeldingProcess : PackageProcess, IAbbProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbbWeldingProcess"/> class.
        /// </summary>
        public AbbWeldingProcess()
        {
            this.Name = "ABB Welding Process";
            this.PostProcessorType = typeof(PostProcessorAbbWelding);
            this.MainProcessorType = typeof(MainProcessorAbbWelding);
            this.ProcessMenuFileName = "AbbWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
