﻿// <copyright file="PostProcessorOtcDaihenWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.PostProcessor
{
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.PostProcessor;

    /// <summary>
    ///     This class inherits from <see cref="PostProcessorOtcDaihen"/>.
    ///     <para>
    ///         Implements the OTC Daihen welding dedicated properties and methods of the post-processor in order to output OTC Daihen welding robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorOtcDaihenWelding : PostProcessorOtcDaihen
    {
    }
}