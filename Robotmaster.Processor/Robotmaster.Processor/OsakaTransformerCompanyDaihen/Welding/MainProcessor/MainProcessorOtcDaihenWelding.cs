﻿// <copyright file="MainProcessorOtcDaihenWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.MainProcessor;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.AuxiliaryMenus.OtcWelding;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="MainProcessorOtcDaihen" />.
    ///     <para>
    ///         Implements the OTC Daihen Welding dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorOtcDaihenWelding : MainProcessorOtcDaihen
    {
        /// <inheritdoc/>
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            if (applicationType == ApplicationType.Welding)
            {
                // ASWBML 1,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                point = PointManager.AddEventToPathPoint(
                    new WeldOn
                    {
                        WeldOnCommand = OtcDaihenWeldingProcessMenu.GetWeldOnCommand(this.ProgramNode),
                        PowerSupplyName = OtcDaihenWeldingProcessMenu.GetPowerSupplyName(this.ProgramNode),
                        ArcStartPowerSupplyNumber = OtcDaihenWeldingAuxiliaryMenu.GetArcStartPowerSupplyNumber(operationNode),
                        ArcStartCommandFileNumber = OtcDaihenWeldingAuxiliaryMenu.GetArcStartCommandFileNumber(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                if (OtcDaihenWeldingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1)
                {
                    // WS 1
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOn
                        {
                            WeaveOnCommand = OtcDaihenWeldingProcessMenu.GetWeaveOnCommand(this.ProgramNode),
                            WeaveStartCommandFileNumber = OtcDaihenWeldingAuxiliaryMenu.GetWeaveStartCommandFileNumber(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Apply first the FanucMainProcessor method (base class)
            point = base.EditLastPointOfContact(operationNode, point);

            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            if (applicationType == ApplicationType.Welding)
            {
                if (OtcDaihenWeldingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1)
                {
                    // WE 1
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOff
                        {
                            WeaveOffCommand = OtcDaihenWeldingProcessMenu.GetWeavelOffCommand(this.ProgramNode),
                            WeaveStartCommandFileNumber = OtcDaihenWeldingAuxiliaryMenu.GetWeaveStartCommandFileNumber(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }

                // AEWBML 1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                point = PointManager.AddEventToPathPoint(
                    new WeldOff
                    {
                        WeldOffCommand = OtcDaihenWeldingProcessMenu.GetWeldOffCommand(this.ProgramNode),
                        PowerSupplyName = OtcDaihenWeldingProcessMenu.GetPowerSupplyName(this.ProgramNode),
                        ArcEndPowerSupplyNumber = OtcDaihenWeldingAuxiliaryMenu.GetArcEndPowerSupplyNumber(operationNode),
                        ArcEndCommandFileNumber = OtcDaihenWeldingAuxiliaryMenu.GetArcEndCommandFileNumber(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);
            }

            return point;
        }
    }
}