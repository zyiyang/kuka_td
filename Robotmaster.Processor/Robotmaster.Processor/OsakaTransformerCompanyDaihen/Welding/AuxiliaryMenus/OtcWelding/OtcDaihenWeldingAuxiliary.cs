﻿// <copyright file="OtcDaihenWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.AuxiliaryMenus.OtcWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "OtcDaihenWeldingAuxiliary")]
    public class OtcDaihenWeldingAuxiliary : AuxiliaryMenu, IOtcDaihenProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OtcDaihenWeldingAuxiliary"/> class.
        /// </summary>
        public OtcDaihenWeldingAuxiliary()
        {
            this.Name = "OTC Daihen Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "OtcDaihenWeldingAuxiliaryMenu";
        }
    }
}
