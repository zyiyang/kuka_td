﻿// <copyright file="WeaveOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weave ON event.
    /// </summary>
    [DataContract(Name = "OtcWeaveOn")]
    public class WeaveOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeaveOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeaveOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the weave ON command.
        /// </summary>
        [DataMember(Name = "WeaveOnCommand")]
        public virtual string WeaveOnCommand { get; set; } = "WS";

        /// <summary>
        ///     Gets or sets the weave weave start command file number.
        /// </summary>
        [DataMember(Name = "WeaveStartCommandFileNumber")]
        public virtual string WeaveStartCommandFileNumber { get; set; } = "1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeaveOnCommand} {this.WeaveStartCommandFileNumber}";
        }
    }
}
