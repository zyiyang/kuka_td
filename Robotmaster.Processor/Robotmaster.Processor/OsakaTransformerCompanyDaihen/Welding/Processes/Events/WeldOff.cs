﻿// <copyright file="WeldOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld OFF event.
    /// </summary>
    [DataContract(Name = "OtcWeldOff")]
    public class WeldOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the weld ON command.
        /// </summary>
        [DataMember(Name = "WeldOffCommand")]
        public virtual string WeldOffCommand { get; set; } = "AE";

        /// <summary>
        ///     Gets or sets the power supply name.
        /// </summary>
        [DataMember(Name = "PowerSupplyName")]
        public virtual string PowerSupplyName { get; set; } = "WBML";

        /// <summary>
        ///     Gets or sets the arc end welding power supply number.
        /// </summary>
        [DataMember(Name = "ArcEndPowerSupplyNumber")]
        public virtual string ArcEndPowerSupplyNumber { get; set; } = "1";

        /// <summary>
        ///     Gets or sets the arc end welding command file number.
        /// </summary>
        [DataMember(Name = "ArcEndCommandFileNumber")]
        public virtual string ArcEndCommandFileNumber { get; set; } = "1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeldOffCommand}{this.PowerSupplyName} {this.ArcEndPowerSupplyNumber},{this.ArcEndCommandFileNumber},2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        }
    }
}
