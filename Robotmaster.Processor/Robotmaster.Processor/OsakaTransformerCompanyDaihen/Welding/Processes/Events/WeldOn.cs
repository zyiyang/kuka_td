﻿// <copyright file="WeldOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld ON event.
    /// </summary>
    [DataContract(Name = "OtcWeldOn")]
    public class WeldOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the weld ON command.
        /// </summary>
        [DataMember(Name = "WeldOnCommand")]
        public virtual string WeldOnCommand { get; set; } = "AS";

        /// <summary>
        ///     Gets or sets the power supply name.
        /// </summary>
        [DataMember(Name = "PowerSupplyName")]
        public virtual string PowerSupplyName { get; set; } = "WBML";

        /// <summary>
        ///     Gets or sets the arc start welding power supply number.
        /// </summary>
        [DataMember(Name = "ArcStartPowerSupplyNumber")]
        public virtual string ArcStartPowerSupplyNumber { get; set; } = "1";

        /// <summary>
        ///     Gets or sets the arc start welding command file number.
        /// </summary>
        [DataMember(Name = "ArcStartCommandFileNumber")]
        public virtual string ArcStartCommandFileNumber { get; set; } = "1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeldOnCommand}{this.PowerSupplyName} {this.ArcStartPowerSupplyNumber},{this.ArcStartCommandFileNumber},0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
        }
    }
}
