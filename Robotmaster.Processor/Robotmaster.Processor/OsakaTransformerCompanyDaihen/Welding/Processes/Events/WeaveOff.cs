﻿// <copyright file="WeaveOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weave OFF event.
    /// </summary>
    [DataContract(Name = "OtcWeaveOff")]
    public class WeaveOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeaveOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeaveOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the weave OFF command.
        /// </summary>
        [DataMember(Name = "WeaveOffCommand")]
        public virtual string WeaveOffCommand { get; set; } = "WE";

        /// <summary>
        ///     Gets or sets the weave start command file number.
        /// </summary>
        [DataMember(Name = "WeaveStartCommandFileNumber")]
        public virtual string WeaveStartCommandFileNumber { get; set; } = "1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeaveOffCommand} {this.WeaveStartCommandFileNumber}";
        }
    }
}
