﻿// <copyright file="OtcWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.MainProcessor;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.PostProcessor;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines OTC Daihen welding process.
    /// </summary>
    [DataContract(Name = "OtcWeldingProcess")]
    public class OtcWeldingProcess : PackageProcess, IOtcDaihenProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OtcWeldingProcess"/> class.
        /// </summary>
        public OtcWeldingProcess()
        {
            this.Name = "OTC Daihen Welding Process";
            this.PostProcessorType = typeof(PostProcessorOtcDaihenWelding);
            this.MainProcessorType = typeof(MainProcessorOtcDaihenWelding);
            this.AddEventToMet(typeof(WeldOn));
            this.AddEventToMet(typeof(WeldOff));
            this.AddEventToMet(typeof(WeaveOn));
            this.AddEventToMet(typeof(WeaveOff));
            this.ProcessMenuFileName = "OtcDaihenWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
