// <copyright file="OtcDaihenProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     OtcDaihenProcessorSwitchesViewModel View Model.
    /// </summary>
    public class OtcDaihenProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        /// <summary>
        ///     Gets or sets the maximum number of points allowed per individual code file. If 0, then there is no maximum.
        /// </summary>
        public int MaximumNumberOfPointsPerFile
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the machine unit name.
        /// </summary>
        public string MachineUnitName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.MachineUnitName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.MachineUnitName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the subprogram name. Leave blank to use subprogram extension.
        /// </summary>
        public string SubProgramName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.SubProgramName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.SubProgramName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }
    }
}