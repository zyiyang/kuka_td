// <copyright file="OtcDaihenProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for OtcDaihenProcessorSwitches.
    /// </summary>
    public partial class OtcDaihenProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OtcDaihenProcessorSwitchesView"/> class.
        /// </summary>
        public OtcDaihenProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OtcDaihenProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="OtcDaihenProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public OtcDaihenProcessorSwitchesView(OtcDaihenProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public OtcDaihenProcessorSwitchesViewModel ViewModel { get; }
    }
}