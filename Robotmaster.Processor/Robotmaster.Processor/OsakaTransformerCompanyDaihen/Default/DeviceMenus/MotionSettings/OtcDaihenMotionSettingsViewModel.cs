// <copyright file="OtcDaihenMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     OtcDaihenMotionSettingsViewModel View Model.
    /// </summary>
    public class OtcDaihenMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;

        /// <summary>
        ///     Gets or sets the joint speed (%).
        ///     Robotic syntax: R = Speed.
        /// </summary>
        public double JointSpeed
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint acceleration and deceleration for joint motion.
        ///     Acceleration number can be 0, 1, 2 or 3. The lower the number, the more acceleration and deceleration the axis will have.
        ///     Robotic syntax: AC = Acceleration number.
        /// </summary>
        public int JointAccelerationAndDecelerationForJointMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForJointMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForJointMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the smoothness for joint motion.
        ///     Smoothness number can be 0, 1, 2 or 3. The higher the number, the smoother the motion.
        ///     Robotic syntax: SM = Smoothness number.
        /// </summary>
        public int JointMotionSmoothness
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionSmoothness)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionSmoothness)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint motion accuracy.
        ///     Accuracy number can be 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: A = Accuracy number.
        /// </summary>
        public int JointMotionAccuracy
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionAccuracy)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionAccuracy)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint acceleration and deceleration for linear motion.
        ///     Acceleration number can be 0, 1, 2 or 3. The lower the number, the more acceleration and deceleration the axis will have.
        ///     Robotic syntax: AC = Acceleration number.
        /// </summary>
        public int JointAccelerationAndDecelerationForLinearMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForLinearMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForLinearMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the smoothness for linear motion.
        ///     Smoothness number can be 0, 1, 2 or 3. The higher the number, the smoother the motion.
        ///     Robotic syntax: SM = Smoothness number.
        /// </summary>
        public int LinearMotionSmoothness
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionSmoothness)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionSmoothness)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear motion accuracy.
        ///     Accuracy number can be 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: A = Accuracy number.
        /// </summary>
        public int LinearMotionAccuracy
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionAccuracy)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionAccuracy)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis acceleration and deceleration for circular motion.
        ///     Acceleration number can be 0, 1, 2 or 3. The lower the number, the more acceleration and deceleration the axis will have.
        ///     Robotic syntax: AC = Acceleration number.
        /// </summary>
        public int JointAccelerationAndDecelerationForCircularMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForCircularMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForCircularMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the smoothness for circular motion.
        ///     Smoothness number can be 0, 1, 2 or 3. The higher the number, the smoother the motion.
        ///     Robotic syntax: SM = Smoothness number.
        /// </summary>
        public int CircularMotionSmoothness
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionSmoothness)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionSmoothness)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the circular motion accuracy.
        ///     Accuracy number can be 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: A = Accuracy number.
        /// </summary>
        public int CircularMotionAccuracy
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionAccuracy)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionAccuracy)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of fine joint motion. If disabled, then continuous motion is used.
        ///     Robotic syntax: F.
        /// </summary>
        public bool IsOutputOfFineJointMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineJointMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineJointMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of the in-position check for joint motion.
        ///     Each time the command position is reached, the robot pauses before advancing to the next point.
        /// </summary>
        public bool IsOutputOfInPositionCheckForJointMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForJointMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForJointMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of fine linear motion. If disabled, then continuous motion is used.
        ///     Robotic syntax: F.
        /// </summary>
        public bool IsOutputOfFineLinearMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineLinearMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineLinearMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of the in-position check for linear motion.
        ///     Each time the command position is reached, the robot pauses before advancing to the next point.
        /// </summary>
        public bool IsOutputOfInPositionCheckForLinearMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForLinearMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForLinearMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of fine circular motion. If disabled, then continuous motion is used.
        ///     Robotic syntax: F.
        /// </summary>
        public bool IsOutputOfFineCircularMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineCircularMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfFineCircularMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of the in-position check for circular motion.
        ///     Each time the command position is reached, the robot pauses before advancing to the next point.
        /// </summary>
        public bool IsOutputOfInPositionCheckForCircularMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForCircularMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputOfInPositionCheckForCircularMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }
    }
}