﻿// <copyright file="MainProcessorOtcDaihen.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.MainProcessor
{
    using Robotmaster.Processor.Common.Default.MainProcessor;

    /// <summary>
    /// OTC base class.
    /// Contains the OTC methods executed when Calculated is clicked such as event creation automation.
    /// </summary>
    internal class MainProcessorOtcDaihen : MainProcessor
    {
    }
}