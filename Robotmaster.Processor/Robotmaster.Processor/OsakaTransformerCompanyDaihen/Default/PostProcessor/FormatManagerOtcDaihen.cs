﻿// <copyright file="FormatManagerOtcDaihen.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.PostProcessor
{
    using Robotmaster.Kinematics;

    /// <summary>
    ///     OTC Daihen Format Manager.
    /// </summary>
    internal static class FormatManagerOtcDaihen
    {
        private const string FeedFormat = "{0:0.0}";

        private const string GenericFormat = "{0:0.000}";

        private const string FileExtensionFormat = "{0:000}";

        /// <summary>
        ///     Creates the main program name.
        /// </summary>
        /// <param name="machineUnitName">Machine Unit Name.</param>
        /// <returns>String with program extension.</returns>
        internal static string MainProgramNameFormatted(string machineUnitName)
        {
            return machineUnitName + "-A";
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted Speed value.</returns>
        internal static string FeedFormatted(this int value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted Speed value.</returns>
        internal static string FeedFormatted(this double value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted Speed value.</returns>
        internal static string FeedFormatted(this float value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this int value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this double value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this float value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Program number numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        internal static string FileExtensionFormatted(this int value)
        {
            return "." + string.Format(FileExtensionFormat, value);
        }

        /// <summary>
        ///     Formats base configuration output.
        /// <para>"T" Front, "B" Back.</para>
        /// </summary>
        /// <param name="value">Base configuration.</param>
        /// <returns>OTC Daihen formatted value.</returns>
        internal static string Formatted(this BaseConfig value)
        {
            switch (value)
            {
                case BaseConfig.Back:
                    return "3";
                case BaseConfig.Front:
                    return "2";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats wrist configuration output.
        /// <para>"0" Positive, "1" Negative.</para>
        /// </summary>
        /// <param name="value">Wrist configuration.</param>
        /// <returns>OTC Daihen formatted value.</returns>
        internal static string Formatted(this WristConfig value)
        {
            switch (value)
            {
                case WristConfig.Negative:
                    return "1";
                case WristConfig.Positive:
                    return "0";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats Elbow configuration output.
        /// <para>"0" Up, "1" Down.</para>
        /// </summary>
        /// <param name="value">Elbow configuration.</param>
        /// <returns>OTC Daihen formatted value.</returns>
        internal static string Formatted(this ElbowConfig value)
        {
            switch (value)
            {
                case ElbowConfig.Down:
                    return "1";
                case ElbowConfig.Up:
                    return "0";
                default:
                    return string.Empty;
            }
        }
    }
}
