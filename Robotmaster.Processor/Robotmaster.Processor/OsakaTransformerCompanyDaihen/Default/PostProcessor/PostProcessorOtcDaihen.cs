﻿// <copyright file="PostProcessorOtcDaihen.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    internal class PostProcessorOtcDaihen : PostProcessor
    {
        /// <summary>
        ///     Gets or sets the line number of the current file.
        /// </summary>
        internal virtual int PointNumberOfCurrentFile { get; set; }

        /// <summary>
        ///     Gets or sets the move instruction.
        /// </summary>
        /// <remarks>
        ///     The move instruction triggers robotic motion.
        /// </remarks>
        internal virtual string MoveInstruction { get; set; } = "MOVEX ";

        /// <summary>
        ///     Gets or sets the mechanism number.
        /// </summary>
        internal virtual string RobotMechanismNumber { get; set; }

        /// <summary>
        ///     Gets or sets the mechanism number.
        /// </summary>
        internal virtual string SpeedMasterRobot { get; set; } = "MS";

        /// <summary>
        ///     Gets or sets Cartesian pose type.
        /// </summary>
        internal virtual string CartesianPoseType { get; set; } = "X";

        /// <summary>
        ///     Gets or sets Angular pose type.
        /// </summary>
        internal virtual string AngularPoseType { get; set; } = "J";

        /// <summary>
        ///     Gets or sets a sequence of external axes mechanism numbers out of robot mechanism.
        /// </summary>
        internal virtual IEnumerable<Joint> ExternalAxesOutOfRobotMechanism { get; set; }

        /// <summary>
        ///    Gets or sets extension index.
        /// </summary>
        internal virtual int ProgramExtension { get; set; } = 0;

        /// <summary>
        ///     Gets the current "move" and "position" <see cref="FileSection.StreamWriter"/>.
        ///     <para>
        ///         This property dynamically follows the <see cref="PostProcessor.CurrentPostFile"/>.
        ///     </para>
        /// </summary>
        internal virtual StreamWriter CurrentPositionAndMoveSectionWriter => this.CurrentPostFile.FileSection.StreamWriter;

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            if (OtcDaihenProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.SetupNode) <= 0)
            {
                // Single file output
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
            else
            {
                // Multi-file output
                this.CurrentPostFile.InsertIntermediateParent(); //// Create master (main) program
                this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

                this.ProgramExtension++;

                this.CallPostFileIntoParent(this.CurrentPostFile, firstOperation, firstPoint);
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            // Update user coordinate system number.
            if (previousOperationNode == null || OperationManager.GetUserFrame(previousOperationNode).Number != OperationManager.GetUserFrame(operationNode).Number)
            {
                // Output the user coordinate system number.
                this.CurrentPositionAndMoveSectionWriter.WriteLine("\r\n" + this.FormatUserCoordinateSystemNumber(operationNode) + "\r\n");
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // Multi-file Output
            if ((OtcDaihenProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.SetupNode) > 0) && (this.PointNumberOfCurrentFile >= OtcDaihenProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.SetupNode)))
            {
                // Resets file splitting conditions.
                this.PointNumberOfCurrentFile = 0;

                var newPostFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostFile);
                this.CurrentPostFile = newPostFile;

                this.ProgramExtension++;

                this.StartPostFile(this.CurrentPostFile, operationNode, point);
                this.CallPostFileIntoParent(this.CurrentPostFile, operationNode, point);

                this.CurrentPositionAndMoveSectionWriter.WriteLine("\r\n" + this.FormatUserCoordinateSystemNumber(operationNode) + "\r\n");
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.PointNumberOfCurrentFile++;

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.Write(" " + inlineEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.WriteLine(afterPointEvent.ToCode());
        }

        /// <summary>
        ///     Calls the current <see cref="PostFile"/> in its parent <see cref="PostFile"/> following <see cref="Motoman"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to be called in its parent.</param>
        /// <param name="operationNode">The current operation.</param>
        /// <param name="point">The current point.</param>
        internal virtual void CallPostFileIntoParent(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            //// Example:
            ////    CALLP 001;
            postFile.Parent.FileSection.StreamWriter.WriteLine("CALLP " + this.ProgramExtension.ToString("000"));
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="OsakaTransformerCompanyDaihen"/> convention.
        ///     <para>
        ///         Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///         Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        ///     </para>
        /// </summary>
        /// <param name="postFile"><see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        /// TODO: check if the program name can be any string with "-A" appended
        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            if (OtcDaihenProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.SetupNode) <= 0)
            {
                // The file name is the name of the machine unit.
                // ASCII files will have an added "-A" in the file name.
                postFile.FileName = FormatManagerOtcDaihen.MainProgramNameFormatted(OtcDaihenProcessorSwitches.GetMachineUnitName(this.SetupNode));

                // The file extension is the program number.
                postFile.FileExtension = this.ProgramExtension.FileExtensionFormatted();

                postFile.FileSection.Header += "\'PROGRAM " + postFile.FileName + postFile.FileExtension + "\r\n" +
                                               "'PROGRAMMED USING ROBOTMASTER" + "\r\n" + "\r\n" +
                                               "USE " + this.ProgramExtension.ToString("000") + "\r\n";

                postFile.FileSection.Footer += "END" + "\r\n";
            }
            else
            {
                postFile.FileName =
                    OtcDaihenProcessorSwitches.GetSubProgramName(this.SetupNode) == string.Empty ||
                    OtcDaihenProcessorSwitches.GetSubProgramName(this.SetupNode) == OtcDaihenProcessorSwitches.GetMachineUnitName(this.SetupNode)
                        ? this.ProgramExtension.ToString("000") + "-A"
                        : OtcDaihenProcessorSwitches.GetSubProgramName(this.SetupNode).ToUpper().Replace(" ", "_") + "-A";

                postFile.FileExtension = this.ProgramExtension.FileExtensionFormatted();

                postFile.FileSection.Header += "\'PROGRAM CALLP " + postFile.FileName + postFile.FileExtension +
                                               "\r\n" + "'PROGRAMMED USING ROBOTMASTER" + "\r\n" + "\r\n" +
                                               "USE " + this.ProgramExtension.ToString("000") + "\r\n";

                postFile.FileSection.Footer += "END" + "\r\n";
            }
        }

        /// <summary>
        ///     Starts the <paramref name="mainPostFile"/> formatting following <see cref="OsakaTransformerCompanyDaihen"/> convention.
        ///     <para>
        ///         Initializes <paramref name="mainPostFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///         Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        ///     </para>
        /// </summary>
        /// <param name="mainPostFile"><see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartMainPostFile(PostFile mainPostFile, CbtNode operationNode, PathNode point)
        {
            // The file name is the name of the machine unit.
            // ASCII files will have an added "-A" in the file name.
            mainPostFile.FileName = FormatManagerOtcDaihen.MainProgramNameFormatted(OtcDaihenProcessorSwitches.GetMachineUnitName(this.SetupNode));

            // The file extension is the program number.
            mainPostFile.FileExtension = this.ProgramExtension.FileExtensionFormatted();

            mainPostFile.FileSection.Header += "\'PROGRAM " + mainPostFile.FileName + mainPostFile.FileExtension + "\r\n" +
                                           "'PROGRAMMED USING ROBOTMASTER" + "\r\n";

            mainPostFile.FileSection.Footer += "END" + "\r\n";
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.Write(this.MoveInstruction);
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionParameters(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.RobotMechanismNumber + this.CartesianPoseType + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatInterpolationType(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write("(" + this.FormatPosition(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatOrientation(point, operationNode) + ")U,");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatConfig(point) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionSpeed(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatToolNumber(operationNode) + "," + this.SpeedMasterRobot);
            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// 1000.000,0.000,0.000,0.000,0.000
                this.CurrentPositionAndMoveSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentPositionAndMoveSectionWriter.WriteLine();
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.Write(this.MoveInstruction);
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionParameters(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.RobotMechanismNumber + this.CartesianPoseType + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatInterpolationType(point, operationNode));
            this.CurrentPositionAndMoveSectionWriter.Write(point.IsArcMiddlePoint() ? "1," : "2,");
            this.CurrentPositionAndMoveSectionWriter.Write("(" + this.FormatPosition(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatOrientation(point, operationNode) + ")U,");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatConfig(point) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionSpeed(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatToolNumber(operationNode) + "," + this.SpeedMasterRobot);
            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// 1000.000,0.000,0.000,0.000,0.000
                this.CurrentPositionAndMoveSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentPositionAndMoveSectionWriter.WriteLine();
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.Write(this.MoveInstruction);
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionParameters(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.RobotMechanismNumber + this.CartesianPoseType + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatInterpolationType(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write("(" + this.FormatPosition(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatOrientation(point, operationNode) + ")U,");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatConfig(point) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionSpeed(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatToolNumber(operationNode) + "," + this.SpeedMasterRobot);
            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// 1000.000,0.000,0.000,0.000,0.000
                this.CurrentPositionAndMoveSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentPositionAndMoveSectionWriter.WriteLine();
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.CurrentPositionAndMoveSectionWriter.Write(this.MoveInstruction);
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatMotionParameters(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.RobotMechanismNumber + this.AngularPoseType + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatInterpolationType(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write("(" + this.FormatJointSpaceValues(point) + ")");
            this.CurrentPositionAndMoveSectionWriter.Write("," + this.FormatMotionSpeed(point, operationNode) + ",");
            this.CurrentPositionAndMoveSectionWriter.Write(this.FormatToolNumber(operationNode) + "," + this.SpeedMasterRobot);
            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// 1000.000,0.000,0.000,0.000,0.000
                this.CurrentPositionAndMoveSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentPositionAndMoveSectionWriter.WriteLine();
        }

        /// <summary>
        ///     Formats joint space values at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted joint space values at a given point.</returns>
        internal virtual string FormatJointSpaceValues(PathNode point)
        {
            return point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]).Formatted() + "," +
                   point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]).Formatted();
        }

        /// <summary>
        ///     Formats position.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>OTC Daihen formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return pointPositionInUserFrame.X.Formatted() + "," + pointPositionInUserFrame.Y.Formatted() + "," + pointPositionInUserFrame.Z.Formatted();
        }

        /// <summary>
        ///     Formats orientation.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>OTC Daihen formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return euler[0].Formatted() + "," + euler[1].Formatted() + "," + euler[2].Formatted();
        }

        /// <inheritdoc/>
        internal override void InitalizeJointLists()
        {
            base.InitalizeJointLists();

            this.RobotMechanismNumber = "M" + this.RobotJoints.First().GroupNumber;

            this.ExternalAxesOutOfRobotMechanism = this.ExternalAxesJoints.Where(j => j.GroupNumber != this.RobotMechanismNumber);
        }

        /// <summary>
        ///     Formats external axes values output at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted OTC Daihen External Axes.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point, CbtNode operationNode)
        {
            string output = string.Empty;

            IEnumerable<string> externalAxesMechanismNumberOrderedSet = this.ExternalAxesOutOfRobotMechanism.Select(j => j.GroupNumber).OrderBy(gn => gn).Distinct();
            var currentMechanismAxes = new List<Joint>(6);

            foreach (string groupNumber in externalAxesMechanismNumberOrderedSet)
            {
                output += ",M" + groupNumber + this.AngularPoseType + ",P,(";

                currentMechanismAxes = this.ExternalAxesOutOfRobotMechanism.Where(j => j.GroupNumber == groupNumber).OrderBy(joint => joint.Name).ToList();

                for (var i = 0; i < currentMechanismAxes.Count; i++)
                {
                    output += point.JointValue(this.JointFormat[currentMechanismAxes[i].Name]).Formatted();
                    if (i != currentMechanismAxes.Count - 1)
                    {
                        output += ",";
                    }
                }

                // TODO: Make sure that joint speed for external axes can be set in % of maximum power
                output += "),R=" + OtcDaihenMotionSettings.GetJointSpeed(operationNode).FeedFormatted() + "," + this.FormatToolNumber(operationNode);
            }

            return output;
        }

        /// <summary>
        ///     Formats the interpolation type.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted interpolation type.</returns>
        /// <remarks>
        ///     Interpolation type determines the path along which the robot moves between path points.
        /// </remarks>
        internal virtual string FormatInterpolationType(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return "P";
                case MoveType.Linear:
                    return "L";
                case MoveType.Circular:
                    return "C";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the tool number.
        /// <para>
        ///     H = &lt;N&gt;.
        /// </para>
        /// <para>
        ///     &lt;N&gt; :   Tool number.
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted tool number.</returns>
        internal virtual string FormatToolNumber(CbtNode operationNode)
        {
            return "H=" + OperationManager.GetTcpId(operationNode);
        }

        /// <summary>
        ///     Formats the user coordinate system number.
        /// <para>
        ///     CHGCOORD &lt;N&gt;.
        /// </para>
        /// <para>
        ///     &lt;N&gt; :   User coordinate system number.
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted user coordinate system number.</returns>
        internal virtual string FormatUserCoordinateSystemNumber(CbtNode operationNode)
        {
            return "CHGCOORD " + OperationManager.GetUserFrame(operationNode).Number;
        }

        /// <summary>
        ///     Formats motion parameters: Accuracy, Acceleration, Termination Type, Smoothness, Mechanism number.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted motion parameters.</returns>
        internal virtual string FormatMotionParameters(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return "A=" + OtcDaihenMotionSettings.GetJointMotionAccuracy(operationNode) + "," +
                           "AC=" + OtcDaihenMotionSettings.GetJointAccelerationAndDecelerationForJointMotion(operationNode) + "," +
                           (OtcDaihenMotionSettings.GetIsOutputOfFineJointMotionEnabled(operationNode) ? "F" : "SM=" + OtcDaihenMotionSettings.GetJointMotionSmoothness(operationNode));
                case MoveType.Linear:
                    return "A=" + OtcDaihenMotionSettings.GetLinearMotionAccuracy(operationNode) + "," +
                           "AC=" + OtcDaihenMotionSettings.GetJointAccelerationAndDecelerationForLinearMotion(operationNode) + "," +
                           (OtcDaihenMotionSettings.GetIsOutputOfFineLinearMotionEnabled(operationNode) ? "F" : "SM=" + OtcDaihenMotionSettings.GetLinearMotionSmoothness(operationNode));
                case MoveType.Circular:
                    return "A=" + OtcDaihenMotionSettings.GetCircularMotionAccuracy(operationNode) + "," +
                           "AC=" + OtcDaihenMotionSettings.GetJointAccelerationAndDecelerationForCircularMotion(operationNode) + "," +
                           (OtcDaihenMotionSettings.GetIsOutputOfFineCircularMotionEnabled(operationNode) ? "F" : "SM=" + OtcDaihenMotionSettings.GetCircularMotionSmoothness(operationNode));
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats Joint Speed/Feed-rate (depending on motion type).
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Joint Speed/Feed-rate.</returns>
        internal virtual string FormatMotionSpeed(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return "R=" + OtcDaihenMotionSettings.GetJointSpeed(operationNode).FeedFormatted();
                case MoveType.Linear:
                case MoveType.Circular:
                    return "S=" + point.Feedrate().LinearFeedrate.FeedFormatted();
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats configuration at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>OTC Daihen formatted configuration.</returns>
        internal virtual string FormatConfig(PathNode point)
        {
            return "CONF=" + this.FormatConfigBits(point);
        }

        /// <summary>
        ///     Formats the calculated configuration bits at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>OTC Daihen formatted configuration bits.</returns>
        internal virtual string FormatConfigBits(PathNode point)
        {
            // "0" Positive "1" Negative
            // "0" Up "1" Down
            // "2" Front "3" Back

            //// Example : 002
            return this.CachedRobotConfig.WristConfiguration.Formatted() +
                   this.CachedRobotConfig.ElbowConfiguration.Formatted() +
                   this.CachedRobotConfig.BaseConfiguration.Formatted();
        }
    }
}
