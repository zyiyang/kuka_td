﻿// <copyright file="HiwinDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Hiwin.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Hiwin default process.
    /// </summary>
    [DataContract(Name = "HiwinDefaultProcess")]
    public class HiwinDefaultProcess : PackageProcess, IHiwinProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HiwinDefaultProcess"/> class.
        /// </summary>
        public HiwinDefaultProcess()
        {
            this.Name = "Hiwin Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
