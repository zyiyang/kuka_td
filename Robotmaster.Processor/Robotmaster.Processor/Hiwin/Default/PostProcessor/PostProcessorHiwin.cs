﻿// <copyright file="PostProcessorHiwin.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Hiwin.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Hiwin.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     <see cref="PostProcessorHiwin"/> inherits from <see cref="PostProcessor"/>.
    ///     <para>
    ///         Implements the Hiwin dedicated properties and methods of the post-processor in order to output Hiwin default robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorHiwin : PostProcessor
    {
        /// <summary>
        ///     Gets the current point <see cref="FileSection"/>.
        /// </summary>
        internal virtual FileSection PointFileSection => this.CurrentPostFile.FileSection.SubFileSections[0];

        /// <summary>
        ///     Gets the current program <see cref="FileSection"/>.
        /// </summary>
        internal virtual FileSection ProgramFileSection => this.CurrentPostFile.FileSection.SubFileSections[1];

        /// <summary>
        ///     Gets the current point declarations <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter PointDeclarations => this.PointFileSection.StreamWriter;

        /// <summary>
        ///     Gets the current base declarations <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter BaseDeclarations => this.ProgramFileSection.SubFileSections[0].StreamWriter;

        /// <summary>
        ///     Gets the current tool declarations <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter ToolDeclarations => this.ProgramFileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets the current joint space declarations <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter JointSpaceDeclarations => this.ProgramFileSection.SubFileSections[2].StreamWriter;

        /// <summary>
        ///     Gets the current moves <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.ProgramFileSection.SubFileSections[3].StreamWriter;

        /// <summary>
        ///     Gets or sets the list of used Base numbers.
        /// </summary>
        internal virtual List<int> UsedBaseNumbersList { get; set; } = new List<int>();

        /// <summary>
        ///     Gets or sets the list of used Tool numbers.
        /// </summary>
        internal virtual List<int> UsedToolNumbersList { get; set; } = new List<int>();

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            // Define the current post file (HRB)
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_").ToUpper();
            this.CurrentPostFile.FileExtension = ".HRB";

            // Define the Post File structure
            this.StartPostFile(this.CurrentPostFile);

            // Reset variables
            this.UsedBaseNumbersList.Clear();
            this.UsedToolNumbersList.Clear();
            this.PointNumberInProgram = -1;
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Hiwin"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void StartPostFile(PostFile postFile)
        {
            // Define file sections
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Point section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Program section
            postFile.FileSection.SubFileSections[1].SubFileSections.Add(new FileSection()); // Base declarations
            postFile.FileSection.SubFileSections[1].SubFileSections.Add(new FileSection()); // Tool declarations
            postFile.FileSection.SubFileSections[1].SubFileSections.Add(new FileSection()); // Joint Space move declarations
            postFile.FileSection.SubFileSections[1].SubFileSections.Add(new FileSection()); // Moves section

            // Populate headers and footers
            this.PointFileSection.Header += ";Version:320" + "\r\n" +
                                            ";[Point&S]" + "\r\n";
            this.PointFileSection.Footer += ";[Point&E]" + "\r\n";

            DateTime currentTime = this.ProcessorDateTime;

            this.ProgramFileSection.Header += ";[Program&SV2]" + "\r\n" +
                                              ";" + "\r\n" +
                                              "; Generated by Robotmaster for HIWIN 3.20" + "\r\n" +
                                              $"; Creation date: {currentTime.Year:0000}/{currentTime.Month:00}/{currentTime.Day:00} {currentTime.Hour:00}:{currentTime.Minute:00}:{currentTime.Second:00}" + "\r\n" +
                                              ";" + "\r\n";

            this.ProgramFileSection.Footer += ";[Program&E]" + "\r\n";

            this.BaseDeclarations.WriteLine("; SET BASE FRAMES");

            this.ToolDeclarations.WriteLine("; SET TOOL FRAMES");
        }

        /// <inheritdoc />
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            // Output operation name as comment
            this.Moves.WriteLine("; " + OperationManager.GetOperationName(operationNode));

            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            // Output Base frame
            this.OutputBaseFrame(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode);

            // Output Tool Frame
            this.OutputToolFrame(operationNode);
        }

        /// <summary>
        ///     Outputs the Hiwin Coordinate System.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        internal virtual void OutputBaseFrame(CbtNode operationNode, CbtNode referenceFrameNode)
        {
            UserFrame userFrame = OperationManager.GetUserFrame(operationNode);

            if (!this.UsedBaseNumbersList.Contains(userFrame.Number))
            {
                this.BaseDeclarations.WriteLine(this.FormatBaseFrame(userFrame, referenceFrameNode));

                this.UsedBaseNumbersList.Add(userFrame.Number);
            }
        }

        /// <summary>
        ///     Formats the Hiwin Coordinate System Output.
        /// </summary>
        /// <param name="userFrame">The user frame.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        /// <returns>Formatted Hiwin Coordinate System Output.</returns>
        internal virtual string FormatBaseFrame(UserFrame userFrame, CbtNode referenceFrameNode)
        {
            Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrameNode);
            Vector3 eulerAngles = ConvertRotationRepresentation.RotationMatrixToEuler(
                userFrameMatrix.Orientation,
                EulerConvention.Rzyx,
                FrameConvention.World,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);

            // Correct the order of the angles
            var userFrameOrientation = new Vector3(eulerAngles.Z, eulerAngles.Y, eulerAngles.X);

            return string.Format(
                "FRAME rmBASE_{0}" + "\r\n" +
                "rmBASE_{0}.X = {1:0.000}" + "\r\n" +
                "rmBASE_{0}.Y = {2:0.000}" + "\r\n" +
                "rmBASE_{0}.Z = {3:0.000}" + "\r\n" +
                "rmBASE_{0}.A = {4:0.000}" + "\r\n" +
                "rmBASE_{0}.B = {5:0.000}" + "\r\n" +
                "rmBASE_{0}.C = {6:0.000}" + "\r\n" +
                "SET_BASE {0}" + "\r\n" +
                "SET_BASE rmBASE_{0}",
                userFrame.Number,
                userFrameMatrix.Position.X,
                userFrameMatrix.Position.Y,
                userFrameMatrix.Position.Z,
                userFrameOrientation.X,
                userFrameOrientation.Y,
                userFrameOrientation.Z);
        }

        /// <summary>
        ///     Outputs the Hiwin Tool Frame.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void OutputToolFrame(CbtNode operationNode)
        {
            int toolFrameNumber = OperationManager.GetTcpId(operationNode);

            if (!this.UsedToolNumbersList.Contains(toolFrameNumber))
            {
                this.ToolDeclarations.WriteLine(this.FormatToolFrame(operationNode, toolFrameNumber));

                this.UsedToolNumbersList.Add(toolFrameNumber);
            }
        }

        /// <summary>
        ///     Formats the Hiwin Tool Frame Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="toolFrameNumber">The tool frame number.</param>
        /// <returns>Formatted Hiwin Tool Frame Output.</returns>
        internal virtual string FormatToolFrame(CbtNode operationNode, int toolFrameNumber)
        {
            Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);
            Vector3 eulerAngles = ConvertRotationRepresentation.RotationMatrixToEuler(
                toolFrameMatrix.Orientation,
                EulerConvention.Rzyx,
                FrameConvention.World,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);

            // Correct the order of the angles
            var toolFrameOrientation = new Vector3(eulerAngles.Z, eulerAngles.Y, eulerAngles.X);

            return string.Format(
                "FRAME rmTOOL_{0}" + "\r\n" +
                "rmTOOL_{0}.X = {1:0.000}" + "\r\n" +
                "rmTOOL_{0}.Y = {2:0.000}" + "\r\n" +
                "rmTOOL_{0}.Z = {3:0.000}" + "\r\n" +
                "rmTOOL_{0}.A = {4:0.000}" + "\r\n" +
                "rmTOOL_{0}.B = {5:0.000}" + "\r\n" +
                "rmTOOL_{0}.C = {6:0.000}" + "\r\n" +
                "SET_TOOL {0}" + "\r\n" +
                "SET_TOOL rmTOOL_{0}",
                toolFrameNumber,
                toolFrameMatrix.Position.X,
                toolFrameMatrix.Position.Y,
                toolFrameMatrix.Position.Z,
                toolFrameOrientation.X,
                toolFrameOrientation.Y,
                toolFrameOrientation.Z);
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc />
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Skip Tool Change Macro Output
            if (beforePointEvent.GetType() == typeof(ToolChange))
            {
                return;
            }

            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(inlineEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatMoveOutput(point, operationNode));
            this.PointDeclarations.WriteLine(this.FormatPointDeclaration(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (point.IsArcMiddlePoint())
            {
                this.Moves.WriteLine(this.FormatMoveOutput(point, operationNode));
            }

            this.PointDeclarations.WriteLine(this.FormatPointDeclaration(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatMoveOutput(point, operationNode));
            this.PointDeclarations.WriteLine(this.FormatPointDeclaration(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatMoveOutput(point, operationNode));
            this.JointSpaceDeclarations.WriteLine(this.FormatJointSpaceDeclaration(point, operationNode));
        }

        /// <summary>
        ///     Formats the Hiwin point declaration.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Hiwin point declaration.</returns>
        internal virtual string FormatPointDeclaration(PathNode point, CbtNode operationNode)
        {
            Matrix4X4 pointMatrixInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);
            Vector3 pointPosition = pointMatrixInUserFrame.Position;
            Vector3 eulerAngles = this.RobotFormatter.MatrixToEuler(pointMatrixInUserFrame);

            return string.Format(
                "DECL E6POINT P{0}={{A1 {1:0.000}, A2 {2:0.000}, A3 {3:0.000}, A4 {4:0.000}, A5 {5:0.000}, A6 {6:0.000}, X {7:0.000}, Y {8:0.000}, Z {9:0.000}, A {10:0.000}, B {11:0.000}, C {12:0.000}{15}}}" + "\r\n" +
                ";NULL; Tool={13}; Base={14}",
                this.PointNumberInProgram,
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]),
                pointPosition.X,
                pointPosition.Y,
                pointPosition.Z,
                eulerAngles.X,
                eulerAngles.Y,
                eulerAngles.Z,
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number,
                this.FormatExternalAxesValues(point));
        }

        /// <summary>
        ///     Formats the Hiwin joint space move declaration.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Hiwin joint space move declaration.</returns>
        internal virtual string FormatJointSpaceDeclaration(PathNode point, CbtNode operationNode)
        {
            return string.Format(
                "E6AXIS P{0:0}={{A1 {1:0.000}, A2 {2:0.000}, A3 {3:0.000}, A4 {4:0.000}, A5 {5:0.000}, A6 {6:0.000}{7}}}",
                this.PointNumberInProgram,
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]),
                this.FormatExternalAxesValues(point));
        }

        /// <summary>
        ///     Formats the Hiwin move output.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Hiwin move output.</returns>
        internal virtual string FormatMoveOutput(PathNode point, CbtNode operationNode)
        {
            int acceleration;
            int contValue;
            float velocity;

            if (point.MoveType() == MoveType.Linear || point.MoveType() == MoveType.Circular)
            {
                acceleration = HiwinMotionSettings.GetLinearCircularIsAccelerationOutput(operationNode)
                    ? HiwinMotionSettings.GetLinearCircularAcceleration(operationNode)
                    : 0;

                contValue = HiwinMotionSettings.GetLinearCircularTerminationType(operationNode) == 1
                    ? HiwinMotionSettings.GetLinearCircularContinuousMotionValue(operationNode)
                    : -1;

                velocity = point.Feedrate().LinearFeedrate;
            }
            else
            {
                acceleration = HiwinMotionSettings.GetPtpIsAccelerationOutput(operationNode) ? HiwinMotionSettings.GetPtpAcceleration(operationNode) : 0;

                contValue = HiwinMotionSettings.GetPtpTerminationType(operationNode) == 1
                    ? HiwinMotionSettings.GetPtpContinuousMotionValue(operationNode)
                    : -1;

                velocity = HiwinMotionSettings.GetPtpVelocity(operationNode);
            }

            return string.Format(
                "{0} {1} {2} {3}{4} TOOL[{5}] BASE[{6}]",
                point.MoveType() == MoveType.Linear ? "LIN" : point.MoveType() == MoveType.Circular ? "CIRC" : point.MoveType() == MoveType.Rapid ? "PTP" : "Error",
                point.MoveType() == MoveType.Circular ? $"P{this.PointNumberInProgram} P{this.PointNumberInProgram + 1}" : $"P{this.PointNumberInProgram}",
                contValue < 0 ? "FINE" : $"CONT={contValue}%",
                $"Vel={(point.MoveType() == MoveType.Rapid ? $"{velocity}%" : $"{velocity}mm/s")}",
                acceleration <= 0 ? string.Empty : $" Acc={acceleration}%",
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number);
        }

        /// <summary>
        ///     Formats external axes values output at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Hiwin External Axes.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point)
        {
            string output = string.Empty;

            for (var axisIndex = 1; axisIndex <= 5; axisIndex++)
            {
                output += this.JointFormat.TryGetValue($"E{axisIndex}", out int i)
                    ? $", E{axisIndex} {point.JointValue(i):0.000}"
                    : string.Empty;
            }

            return output;
        }

        /// <inheritdoc />
        internal override bool IsProgramInputValid()
        {
            // Verifies if the device has the "label" for the rail and rotaries axes properly defined in the ROBX file.
            if (!this.IsExternalAxesNameValid())
            {
                this.NotifyUser("ERROR: The rail/rotary axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the rail and rotary axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the rail and rotary axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsExternalAxesNameValid()
        {
            var externalAxes = new List<string>() { "E1", "E2", "E3", "E4", "E5" };
            return !(this.Rails.Any() && this.Rails.Any(x => !externalAxes.Contains(x.Name))) &&
                   !(this.Rotaries.Any() && this.Rotaries.Any(x => !externalAxes.Contains(x.Name)));
        }
    }
}
