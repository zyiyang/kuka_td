﻿// <copyright file="MotomanWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Motoman.Default.Processes;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;
    using Robotmaster.Processor.Motoman.Welding.MainProcessor;
    using Robotmaster.Processor.Motoman.Welding.PostProcessor;
    using Robotmaster.Processor.Motoman.Welding.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    /// Defines Motoman welding process.
    /// </summary>
    [DataContract(Name = "MotomanWeldingProcess")]
    public class MotomanWeldingProcess : PackageProcess, IMotomanProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MotomanWeldingProcess"/> class.
        /// </summary>
        public MotomanWeldingProcess()
        {
            this.Name = "Motoman Welding Process";
            this.PostProcessorType = typeof(PostProcessorMotomanWelding);
            this.MainProcessorType = typeof(MainProcessorMotomanWelding);
            this.AddEventToMet(typeof(CallJob));
            this.AddEventToMet(typeof(TrackOff));
            this.AddEventToMet(typeof(TrackOn));
            this.AddEventToMet(typeof(WeaveOff));
            this.AddEventToMet(typeof(WeaveOn));
            this.AddEventToMet(typeof(WeldOff));
            this.AddEventToMet(typeof(WeldOn));
            this.AddEventToMet(typeof(Timer));
            this.AddEventToMet(typeof(Pause));
            this.AddEventToMet(typeof(Comment));
            this.ProcessMenuFileName = "MotomanWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
