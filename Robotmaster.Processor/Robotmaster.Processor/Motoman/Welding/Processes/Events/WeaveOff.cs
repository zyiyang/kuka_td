﻿// <copyright file="WeaveOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Weave off event.
    /// </summary>
    [DataContract(Name = "MotomanWeaveOff")]
    public class WeaveOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeaveOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public WeaveOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event is synchronized.
        /// </summary>
        [DataMember(Name = "IsSychronized")]
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the weave off command.
        /// </summary>
        [DataMember(Name = "WeaveOffCommand")]
        public virtual string WeaveOffCommand { get; set; } = "WVOFF";

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.WeaveOffCommand}";
        }
    }
}
