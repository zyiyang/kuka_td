﻿// <copyright file="WeldOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Weld off event.
    /// </summary>
    [DataContract(Name = "MotomanWeldOff")]
    public class WeldOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeldOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public WeldOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the arc Off command.
        /// </summary>
        [DataMember(Name = "ArcOffCommand")]
        public virtual string ArcOffCommand { get; set; } = "ARCOF AEF";

        /// <summary>
        /// Gets or sets the arc Off file number.
        /// </summary>
        [DataMember(Name = "ArcFileNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int ArcFileNumber { get; set; } = 1;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.ArcOffCommand}#({this.ArcFileNumber})";
        }
    }
}
