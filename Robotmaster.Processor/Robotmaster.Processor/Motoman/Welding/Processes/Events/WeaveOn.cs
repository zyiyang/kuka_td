﻿// <copyright file="WeaveOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Weave on event.
    /// </summary>
    [DataContract(Name = "MotomanWeaveOn")]
    public class WeaveOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeaveOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public WeaveOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event is synchronized.
        /// </summary>
        [DataMember(Name = "IsSynchronized")]
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the weave on command.
        /// </summary>
        [DataMember(Name = "WeaveOnCommand")]
        public virtual string WeaveOnCommand { get; set; } = "WVON";

        /// <summary>
        /// Gets or sets The Weave On file number. Example: WVON WEV#(1).
        /// </summary>
        [DataMember(Name = "WeaveOnFileNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int WeaveOnFileNumber { get; set; } = 1;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.WeaveOnCommand} WEV#({this.WeaveOnFileNumber})";
        }
    }
}
