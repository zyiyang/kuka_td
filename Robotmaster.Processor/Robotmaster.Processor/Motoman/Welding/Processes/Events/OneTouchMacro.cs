﻿// <copyright file="OneTouchMacro.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using Microsoft.Scripting.Utils;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;

    /// <summary>
    /// Motoman one point touch macro event.
    /// </summary>
    [DataContract(Name = "OneTouchMacro")]
    internal class OneTouchMacro : Macro
    {
        /// <summary>
        /// Gets or sets the deviation data.
        /// </summary>
        [DataMember(Name = "ShiftVariable")]
        public string ShiftVariable { get; set; } = string.Empty;

        public override string ToString()
        {
            return $"{base.ToString()} {this.ShiftVariable}";
        }
    }
}