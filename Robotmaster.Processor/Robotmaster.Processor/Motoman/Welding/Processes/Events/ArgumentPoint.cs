﻿// <copyright file="ArgumentPoint.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;

    /// <summary>
    /// Argument point event.
    /// </summary>
    [DataContract(Name = "MotomanArgumentPoint")]
    public class ArgumentPoint : Comment
    {
    }
}
