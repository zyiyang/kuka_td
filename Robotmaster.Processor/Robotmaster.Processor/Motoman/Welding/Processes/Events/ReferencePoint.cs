﻿// <copyright file="ReferencePoint.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Reference point event.
    /// </summary>
    [DataContract(Name = "MotomanReferencePoint")]
    public class ReferencePoint : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReferencePoint"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ReferencePoint()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event is synchronized.
        /// </summary>
        [DataMember(Name = "IsSynchronized")]
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the reference point command.
        /// </summary>
        [DataMember(Name = "SearchDirectionCommand")]
        public virtual string SearchDirectionCommand { get; set; } = "REFP";

        /// <summary>
        /// Gets or sets the reference point number.
        /// </summary>
        [DataMember(Name = "ReferencePointNumber")]
        public virtual int ReferencePointNumber { get; set; } = 3;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.SearchDirectionCommand} {this.ReferencePointNumber}";
        }
    }
}
