﻿// <copyright file="TrackOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Track off event.
    /// </summary>
    [DataContract(Name = "MotomanTrackOff")]
    public class TrackOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrackOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public TrackOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event is synchronized.
        /// </summary>
        [DataMember(Name = "IsSychronized")]
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the seam tracking off command.
        /// </summary>
        [DataMember(Name = "SeamTrackingOffCommand")]
        public virtual string SeamTrackingOffCommand { get; set; } = "COMARCOF";

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.SeamTrackingOffCommand}";
        }
    }
}
