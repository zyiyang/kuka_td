﻿// <copyright file="WeldOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Weld on event.
    /// </summary>
    [DataContract(Name = "MotomanWeldOn")]
    public class WeldOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeldOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public WeldOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the arc On command.
        /// </summary>
        [DataMember(Name = "ArcOnCommand")]
        public virtual string ArcOnCommand { get; set; } = "ARCON ASF";

        /// <summary>
        /// Gets or sets the arc On file number.
        /// </summary>
        [DataMember(Name = "ArcFileNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int ArcFileNumber { get; set; } = 1;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.ArcOnCommand}#({this.ArcFileNumber})";
        }
    }
}
