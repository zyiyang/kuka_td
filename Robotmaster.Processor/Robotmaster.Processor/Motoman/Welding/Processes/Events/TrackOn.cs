﻿// <copyright file="TrackOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Track on event.
    /// </summary>
    [DataContract(Name = "MotomanTrackOn")]
    public class TrackOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrackOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public TrackOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event is synchronized.
        /// </summary>
        [DataMember(Name = "IsSychronized")]
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the seam tracking on command.
        /// </summary>
        [DataMember(Name = "SeamTrackingOnCommand")]
        public virtual string SeamTrackingOnCommand { get; set; } = "COMARCON";

        /// <summary>
        /// Gets or sets The Weave On file number. Example: COMARCON WEV#(1) U/D=240 L/R=1.
        /// </summary>
        [DataMember(Name = "WeaveOnFileNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual int WeaveOnFileNumber { get; set; } = 1;

        /// <summary>
        /// Gets or sets the Upward/Downward sensing condition. Example: U/D=240.
        /// </summary>
        [DataMember(Name = "UDSensingCondition")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual double UdSensingCondition { get; set; } = 240;

        /// <summary>
        /// Gets or sets the Seam tracking position in corner. Example: L/R=1.
        /// </summary>
        [DataMember(Name = "LRSensingCondition")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual double LrSensingCondition { get; set; } = 1;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.SeamTrackingOnCommand} WEV#({this.WeaveOnFileNumber}) U/D={this.UdSensingCondition} L/R={this.LrSensingCondition}";
        }
    }
}
