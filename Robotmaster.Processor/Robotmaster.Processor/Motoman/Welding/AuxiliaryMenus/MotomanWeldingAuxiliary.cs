﻿// <copyright file="MotomanWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.AuxiliaryMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    /// <inheritdoc />
    [DataContract(Name = "MotomanWeldingAuxiliary")]
    public class MotomanWeldingAuxiliary : AuxiliaryMenu, IMotomanProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanWeldingAuxiliary"/> class.
        /// </summary>
        public MotomanWeldingAuxiliary()
        {
            this.Name = "Motoman Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "MotomanWeldingAuxiliaryMenu";
        }
    }
}
