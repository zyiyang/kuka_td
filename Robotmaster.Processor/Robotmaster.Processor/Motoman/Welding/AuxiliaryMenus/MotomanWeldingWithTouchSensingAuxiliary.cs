﻿// <copyright file="MotomanWeldingWithTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.AuxiliaryMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    /// <inheritdoc />
    [DataContract(Name = "MotomanWeldingWithTouchSensingAuxiliary")]
    public class MotomanWeldingWithTouchSensingAuxiliary : AuxiliaryMenu, IMotomanProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanWeldingWithTouchSensingAuxiliary"/> class.
        /// </summary>
        public MotomanWeldingWithTouchSensingAuxiliary()
        {
            this.Name = "Motoman Welding With Touch Sensing";
            this.ApplicationType = ApplicationType.WeldingWithTouchSensing;
            this.AuxiliaryMenuFileName = "MotomanWeldingWithTouchSensingAuxiliaryMenu";
        }
    }
}
