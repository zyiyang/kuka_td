﻿// <copyright file="MotomanTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.AuxiliaryMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    /// <inheritdoc />
    [DataContract(Name = "MotomanTouchSensingAuxiliary")]
    public class MotomanTouchSensingAuxiliary : AuxiliaryMenu, IMotomanProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanTouchSensingAuxiliary"/> class.
        /// </summary>
        public MotomanTouchSensingAuxiliary()
        {
            this.Name = "Motoman Touch Sensing";
            this.ApplicationType = ApplicationType.TouchSensing;
            this.AuxiliaryMenuFileName = "MotomanTouchSensingAuxiliaryMenu";
        }
    }
}
