﻿// <copyright file="PostProcessorMotomanWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Motoman.Default.PostProcessor;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;
    using Robotmaster.Processor.Motoman.Welding.Processes.Events;
    using Robotmaster.Processor.Motoman.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// This class inherits from <see cref="PostProcessorMotoman"/>.
    /// <para>Implements the Motoman welding dedicated properties and methods of the post-processor in order to output Motoman welding robot codes.</para>
    /// </summary>
    internal class PostProcessorMotomanWelding : PostProcessorMotoman
    {
        /// <summary>
        ///     Gets or sets a value indicating whether ABC Euler angles for DX100 controls with multi-pass setup is output.
        /// </summary>
        internal virtual bool EnableAbcEulerAngleOutput { get; set; } = true;

        /// <summary>
        ///     Gets or sets the single shift variable used by a one touch macro.
        ///     It used for Pxxxx controller variable.
        /// </summary>
        internal virtual int MacroShiftVariable { get; set; }

        /// <summary>
        ///     Gets or sets the current local variable storing the shift resulting from touch operation (1D/2D/3D).
        ///     This shift can result from several combined shift.
        ///     It used for LPxxxx local variable.
        /// </summary>
        internal virtual int TouchShiftVariable { get; set; }

        /// <summary>
        /// Gets or sets the current operation touches count.
        /// </summary>
        internal virtual int CurrentTouchCount { get; set; }

        internal override bool IsProgramInputValid()
        {
            bool isProgramInputValid = base.IsProgramInputValid();

            IEnumerable<CbtNode> programOperations = ProgramManager.GetOperationNodes(this.ProgramNode);
            foreach (var weldingOperation in programOperations.Where(op => OperationManager.GetApplicationType(op) == ApplicationType.Welding))
            {
                var referencedTouchSensingOperation = OperationManager.GetTouchSensingOperation(weldingOperation, this.OperationCbtRoot);
                if (referencedTouchSensingOperation != null && (!programOperations.Contains(referencedTouchSensingOperation)))
                {
                    this.NotifyUser($"{OperationManager.GetOperationName(referencedTouchSensingOperation)} referenced by {OperationManager.GetOperationName(weldingOperation)} is not assigned to the {ProgramManager.GetProgramName(this.ProgramNode)}. Posting will be stopped.");
                    isProgramInputValid = false;
                    break;
                }
            }

            return isProgramInputValid;
        }

        internal override void RunBeforeProgramOutput()
        {
            base.RunBeforeProgramOutput();
            this.EnableAbcEulerAngleOutput = MotomanWeldingProcessMenu.GetEulerOutputConvention(this.ProgramNode) == 1;

            this.MacroShiftVariable = MotomanWeldingProcessMenu.GetMacroShiftVariable(this.ProgramNode);
            this.TouchShiftVariable = MotomanWeldingProcessMenu.GetTouchOperationShiftVariableStart(this.ProgramNode);

            this.NumberOfLocalVariables[5] = this.TouchShiftVariable + 1;

            this.RobotAxisPositionVariables.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.User, this.RobotAxisPositionVariableSpaceDataType));
            this.RobotAxisPositionVariableSpaceDataType = MotomanPostype.User;

            ++this.NumberOfPositionDataItems[3];
            this.RobotAxisPositionVariables.StreamWriter.WriteLine($"P{this.MacroShiftVariable:D4}=0,0,0,0,0,0");

            // Initialize the LP variable using a Cartesian P variable as a template.
            // The actual value of P at this stage doesn't matter since it will be overridden.
            this.Moves.WriteLine($"SET LP000 P{this.MacroShiftVariable:D4}");
        }

        internal override void RunAfterOperationOutput(CbtNode operationNode)
        {
            base.RunAfterOperationOutput(operationNode);

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing || OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                // Increment local variable for shift.
                // This assumes only one shift is used by touch sensing operation
                // TODO: to modified if 2 shift touch sensing are implemented
                ++this.NumberOfLocalVariables[5];
            }
        }

        /// <inheritdoc/>
        internal override void OutputMove(PathNode point, CbtNode operationNode)
        {
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                if (point.Events()?.EventListBefore != null
                    && point.Events().EventListBefore.Any(x => x is ArgumentPoint))
                {
                    // Output only flagged points inside touch macro
                    // Only positions will be output for those points no move commands.
                    this.OutputOnlyPosition(point, operationNode);
                    return;
                }

                if (point.PathFlag() == PathFlag.OutsideOfProcess)
                {
                    // Output points outside touch macro
                    base.OutputMove(point, operationNode);
                    return;
                }
            }
            else
            {
                // Output any other point normally
                base.OutputMove(point, operationNode);
                return;
            }
        }

        internal virtual void OutputOnlyPosition(PathNode point, CbtNode operationNode)
        {
            //// Example of robot axis teaching position data:
            ////    C0002=80.979,-5.049,-33.372,180.000,22.500,0.000
            this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));

            // Filter for base (rail) axes
            if (this.Rails.Any())
            {
                //// Example of base (rail) axis teaching position data:
                ////    BC0002=0,0,0
                this.BaseAxisTeachingPositions.StreamWriter.WriteLine(this.FormatBaseAxisTeachingPositionData(point, operationNode));
            }

            // Filter for station (rotary) axes
            if (this.Rotaries.Any())
            {
                //// Example of station (rotary) axis teaching position data:
                ////    EC0002=-2,11,0,14
                this.StationAxisTeachingPositions.StreamWriter.WriteLine(this.FormatStationAxisTeachingPositionData(point, operationNode));
            }
        }

        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            if (beforePointEvent is ArgumentPoint)
            {
                return;
            }

            base.OutputBeforePointEvent(beforePointEvent, point, operationNode);
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                if (afterPointEvent is OneTouchMacro macroEvent)
                {
                    macroEvent.Arguments[1] = $"C{(this.NumberOfPositionDataItems[0] - 3).TeachingPositionNumberFormatted()}";
                    macroEvent.Arguments[3] = $"C{(this.NumberOfPositionDataItems[0] - 2).TeachingPositionNumberFormatted()}";
                    macroEvent.Arguments[5] = $"C{(this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted()}";

                    if (this.Rotaries.Any())
                    {
                        //// Example of station (rotary) axis teaching position move:
                        macroEvent.Arguments[2] = $"EC{(this.NumberOfPositionDataItems[2] - 3).TeachingPositionNumberFormatted()}";
                        macroEvent.Arguments[4] = $"EC{(this.NumberOfPositionDataItems[2] - 2).TeachingPositionNumberFormatted()}";
                        macroEvent.Arguments[6] = $"EC{(this.NumberOfPositionDataItems[2] - 1).TeachingPositionNumberFormatted()}";
                    }

                    if (this.Rails.Any())
                    {
                        //// Example of station (rotary) axis teaching position move:
                        macroEvent.Arguments[2] = $"B{(this.NumberOfPositionDataItems[1] - 3).TeachingPositionNumberFormatted()}";
                        macroEvent.Arguments[4] = $"B{(this.NumberOfPositionDataItems[1] - 2).TeachingPositionNumberFormatted()}";
                        macroEvent.Arguments[6] = $"B{(this.NumberOfPositionDataItems[1] - 1).TeachingPositionNumberFormatted()}";
                    }

                    this.Moves.WriteLine(macroEvent.ToMotomanCode());
                    this.CurrentTouchCount++;

                    // Resets the shift angle to assure one touch macro only provide a XYZ shift.
                    this.Moves.WriteLine($"SETE P{this.MacroShiftVariable:D4} (4) 0");
                    this.Moves.WriteLine($"SETE P{this.MacroShiftVariable:D4} (5) 0");
                    this.Moves.WriteLine($"SETE P{this.MacroShiftVariable:D4} (6) 0");

                    return;
                }
            }

            base.OutputAfterPointEvent(afterPointEvent, point, operationNode);
        }

        internal override string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            if (!this.EnableAbcEulerAngleOutput)
            {
                return base.FormatOrientation(point, operationNode);
            }

            // Gets the Euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = ConvertRotationRepresentation.RotationMatrixToEuler(
                pointFrameInUserFrame.Orientation,
                EulerConvention.Rzyz,
                FrameConvention.BodyFixed,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);

            return euler[0].Formatted() + "," + (90 - euler[1]).Formatted() + "," + (-euler[2]).Formatted();
        }
    }
}