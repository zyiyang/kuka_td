﻿// <copyright file="MainProcessorMotomanWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Welding.MainProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Component.Types;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.Motoman.Default.MainProcessor;
    using Robotmaster.Processor.Motoman.Default.PostProcessor;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;
    using Robotmaster.Processor.Motoman.Welding.AuxiliaryMenus;
    using Robotmaster.Processor.Motoman.Welding.Processes.Events;
    using Robotmaster.Processor.Motoman.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.ApplicationLayer.Managers.Keys;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// This class inherits from <see cref="MainProcessorMotoman"/>.
    /// <para>Implements the Motoman Welding dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorMotomanWelding : MainProcessorMotoman
    {
        /// <summary>
        ///     Gets or sets the global combined shift of a touch operation (1D/2D/3D).
        ///     It used for LPxxxx local variable.
        /// </summary>
        internal virtual int TouchShiftVariable { get; set; }

        /// <summary>
        ///     Gets or sets the single shift variable used by one touch macro.
        ///     It used for Pxxxx controller variable.
        /// </summary>
        internal virtual int MacroShiftVariable { get; set; }

        /// <summary>
        /// Gets or sets the current operation touches count.
        /// </summary>
        internal virtual int CurrentTouchCount { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the external axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> ExternalAxesJoints { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the rotary axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> Rotaries { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the rail axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> Rails { get; set; }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            base.EditOperationBeforeAllPointEdits(operationNode);

            this.InitalizeJointLists();

            this.CurrentTouchCount = 0;
            this.MacroShiftVariable = MotomanWeldingProcessMenu.GetMacroShiftVariable(this.ProgramNode);
            this.TouchShiftVariable = MotomanWeldingProcessMenu.GetTouchOperationShiftVariableStart(this.ProgramNode);

            switch (OperationManager.GetApplicationType(operationNode))
            {
                case ApplicationType.TouchSensing:
                    this.EditTouchSensingOperation(operationNode);
                    break;
                case ApplicationType.Welding:
                    this.EditWeldingOperation(operationNode);
                    break;
            }
        }

        /// <summary>
        ///     Initialize the <see cref="Joint"/> lists used in the current setup.
        /// </summary>
        internal virtual void InitalizeJointLists()
        {
            this.JointFormat = SetupManager.GetJointFormat(this.SetupNode);

            List<CbtNode> allManipulators = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode));
            IEnumerable<CbtNode> externalAxesManipulators = allManipulators.Where(m => !m.HasComponent(SceneKeys.Robot));

            this.ExternalAxesJoints = externalAxesManipulators
                .SelectMany(CellManager.GetManipulatorJoints)
                .OrderBy(joint => joint.Name);
            this.Rotaries = this.ExternalAxesJoints
                .Where(x => x != null)
                .Where(x => x.AxisType == AxisType.Revolute);
            this.Rails = this.ExternalAxesJoints
                .Where(x => x != null)
                .Where(x => x.AxisType == AxisType.Prismatic);
        }

        /// <summary>
        /// Edit a touch sensing operation.
        /// </summary>
        /// <param name="operationNode">Touch sensing operationNode.</param>
        internal virtual void EditTouchSensingOperation(CbtNode operationNode)
        {
            // Get the Operation Shift Variable Start
            int touchOperationShiftVariableStart = MotomanWeldingProcessMenu.GetTouchOperationShiftVariableStart(this.ProgramNode);

            // If incremental registry offset numbering
            if (MotomanWeldingProcessMenu.GetTouchShiftNumbering(this.ProgramNode) == 1)
            {
                // Get the touch operation count before the current operation
                int previousTouchSensingOperationCount = SetupManager.GetOperationNodes(this.SetupNode)
                    .TakeWhile(op => op != operationNode)
                    .Count(op => OperationManager.GetApplicationType(op) == ApplicationType.TouchSensing || OperationManager.GetApplicationType(op) == ApplicationType.WeldingWithTouchSensing);

                // Shift the touch offset registry by the calculated count.
                // This assumes only one shift is used by touch sensing operation
                // TODO: to modified if 2 shift touch sensing are implemented
                touchOperationShiftVariableStart += previousTouchSensingOperationCount;
            }

            // Example:
            // SET LP0017 LP0000
            PointManager.AddEventToPathPoint(
                new DataSet()
                {
                    DestinationData = $"LP{touchOperationShiftVariableStart:D4}",
                    SourceData = $"LP0000",
                },
                EventIndex.After,
                OperationManager.GetLastPoint(operationNode),
                operationNode);

            MotomanTouchSensingAuxiliaryMenu.SetTouchShiftVariable(operationNode, touchOperationShiftVariableStart);
        }

        /// <summary>
        /// Edit a welding operation.
        /// </summary>
        /// <param name="operationNode">Touch sensing operationNode.</param>
        internal virtual void EditWeldingOperation(CbtNode operationNode)
        {
        }

        /// <inheritdoc/>
        internal override PathNode EditPoint(CbtNode operationNode, PathNode point)
        {
            point = base.EditPoint(operationNode, point);

            if (this.IsTouchPoint(operationNode, point))
            {
                point = this.EditTouchPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits touch point.
        ///     <para>This method is called in <see cref="EditPoint"/> when <seealso cref="MainProcessor.IsTouchPoint"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditTouchPoint(CbtNode operationNode, PathNode point)
        {
            // Adds touch sensing event
            // Example:
            // REFP 3
            point = PointManager.AddEventToPathPoint(
                new ArgumentPoint()
                {
                    CommentText = "Macro reference point 2:",
                },
                EventIndex.Before,
                point,
                operationNode);

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Apply first the base class method
            point = base.EditFirstPointOfContact(operationNode, point);

            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            if (applicationType == ApplicationType.Welding)
            {
                if (MotomanWeldingAuxiliaryMenu.GetWeldOnTimer(operationNode) > 0)
                {
                    // Example:
                    // TIMER T=1
                    point = PointManager.AddEventToPathPoint(
                            new Timer()
                            {
                                Time = MotomanWeldingAuxiliaryMenu.GetWeldOnTimer(operationNode),
                            },
                            EventIndex.Before,
                            point,
                            operationNode);
                }

                // Example:
                // ARCON ASF#(1)
                point = PointManager.AddEventToPathPoint(
                    new WeldOn
                    {
                        ArcOnCommand = MotomanWeldingProcessMenu.GetWeldOnCommand(this.ProgramNode),
                        ArcFileNumber = MotomanWeldingAuxiliaryMenu.GetArcOnFileNumber(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                // Adds Welding events
                int weldingType = MotomanWeldingAuxiliaryMenu.GetWeldingType(operationNode);
                switch (weldingType)
                {
                    case 1:
                        {
                            // Example:
                            // WVON WEV#(28)
                            point = PointManager.AddEventToPathPoint(
                                new WeaveOn
                                {
                                    IsSynchronized = this.Rotaries.Any(),
                                    WeaveOnCommand = MotomanWeldingProcessMenu.GetWeavingOnCommand(this.ProgramNode),
                                    WeaveOnFileNumber = MotomanWeldingAuxiliaryMenu.GetWeaveOnFileNumber(operationNode),
                                },
                                EventIndex.After,
                                point,
                                operationNode);
                            break;
                        }

                    case 2:
                        {
                            // Example:
                            // COMARCON WEV#(28) U/D=240 L/R=1
                            point = PointManager.AddEventToPathPoint(
                                new TrackOn
                                {
                                    IsSynchronized = this.Rotaries.Any(),
                                    SeamTrackingOnCommand = MotomanWeldingProcessMenu.GetSeamTrackingOnCommand(this.ProgramNode),
                                    WeaveOnFileNumber = MotomanWeldingAuxiliaryMenu.GetWeaveOnFileNumber(operationNode),
                                    UdSensingCondition = MotomanWeldingAuxiliaryMenu.GetUdSensingCondition(operationNode),
                                    LrSensingCondition = MotomanWeldingAuxiliaryMenu.GetLrSensingCondition(operationNode),
                                },
                                EventIndex.After,
                                point,
                                operationNode);
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }
            }

            if (applicationType == ApplicationType.TouchSensing || (applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point).IsTouchSensingPoint))
            {
                // Adds touch sensing event
                // Example:
                // REFP 4
                point = PointManager.AddEventToPathPoint(
                    new ArgumentPoint()
                    {
                        CommentText = "Macro reference point 1:",
                    },
                    EventIndex.Before,
                    point,
                    operationNode);
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Apply first the base class method
            point = base.EditLastPointOfContact(operationNode, point);

            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            if (applicationType == ApplicationType.Welding)
            {
                // Adds Welding events
                int weldingType = MotomanWeldingAuxiliaryMenu.GetWeldingType(operationNode);
                switch (weldingType)
                {
                    case 1:
                        // Example:
                        // WVOFF
                        point = PointManager.AddEventToPathPoint(
                            new WeaveOff
                            {
                                IsSynchronized = this.Rotaries.Any(),
                                WeaveOffCommand = MotomanWeldingProcessMenu.GetWeavingOffCommand(this.ProgramNode),
                            },
                            EventIndex.After,
                            point,
                            operationNode);
                        break;

                    case 2:
                        // Example:
                        // COMARCOF
                        point = PointManager.AddEventToPathPoint(
                            new TrackOff()
                            {
                                IsSynchronized = this.Rotaries.Any(),
                                SeamTrackingOffCommand = MotomanWeldingProcessMenu.GetSeamTrackingOffCommand(this.ProgramNode),
                            },
                            EventIndex.After,
                            point,
                            operationNode);
                        break;
                }

                // Example:
                // ARCOF AEF#(1)
                point = PointManager.AddEventToPathPoint(
                    new WeldOff
                    {
                        ArcOffCommand = MotomanWeldingProcessMenu.GetWeldOffCommand(this.ProgramNode),
                        ArcFileNumber = MotomanWeldingAuxiliaryMenu.GetArcOffFileNumber(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                double weldOffTimer = MotomanWeldingAuxiliaryMenu.GetWeldOffTimer(operationNode);
                if (weldOffTimer > 0)
                {
                    // Example:
                    // TIMER T=1
                    point = PointManager.AddEventToPathPoint(
                            new Timer()
                            {
                                Time = weldOffTimer,
                            },
                            EventIndex.After,
                            point,
                            operationNode);
                }
            }

            if (applicationType == ApplicationType.TouchSensing || (applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point).IsTouchSensingPoint))
            {
                // Adds touch sensing event
                // Example:
                // REFP 5
                point = PointManager.AddEventToPathPoint(
                    new ArgumentPoint()
                    {
                        CommentText = "Macro reference point 3:",
                    },
                    EventIndex.Before,
                    point,
                    operationNode);

                // Adds one touch macro event
                // Example:
                // MACRO1 MJ#(4) ARGF1 ARGFC00004 ARGFC00005 ARGFC00006 ARGF20 ARGF150 ARGF20 ARGF25 ARGF10 ARGF1 ARGF0 ARGF0 ARGF3
                this.CurrentTouchCount++;

                string[] menuArguments = MotomanWeldingProcessMenu.GetTouchMacroArguments(this.ProgramNode).Split(',');
                var macroArguments = new string[16];

                ////// 16 arguments
                macroArguments[0] = "1";
                macroArguments[1] = string.Empty; // Reference points/axis position filled by the post processor
                macroArguments[2] = string.Empty; // Reference points/axis position filled by the post processor
                macroArguments[3] = string.Empty; // Reference points/axis position filled by the post processor
                macroArguments[4] = string.Empty; // Reference points/axis position filled by the post processor
                macroArguments[5] = string.Empty; // Reference points/axis position filled by the post processor
                macroArguments[6] = string.Empty; // Reference points/axis position filled by the post processor

                for (int i = 0; i < 9; i++)
                {
                    if (i < menuArguments.Length)
                    {
                        macroArguments[7 + i] = menuArguments[i];
                    }
                    else
                    {
                        macroArguments[7 + i] = string.Empty;
                    }
                }

                point = PointManager.AddEventToPathPoint(
                    new OneTouchMacro()
                    {
                        MacroName = "1TCH",
                        MacroId = MotomanWeldingProcessMenu.GetTouchMacroId(this.ProgramNode),
                        ShiftVariable = $"P{this.MacroShiftVariable:D4}",
                        Arguments = macroArguments,
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                point = PointManager.AddEventToPathPoint(
                    new DataSet()
                    {
                        SourceData = $"P{this.MacroShiftVariable:D4}",
                        DestinationData = $"LP{this.CurrentTouchCount:D4}",
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                // Adds shift add event.
                // Combine all offset in on shift variable.
                // Example:
                // ADD P0010 P0011
                point = PointManager.AddEventToPathPoint(
                    new DataAdd()
                    {
                        DestinationData = $"LP0000",
                        SourceData = $"LP{this.CurrentTouchCount:D4}",
                    },
                    EventIndex.After,
                    point,
                    operationNode);
            }

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditFirstNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            point = base.EditFirstNoneRapidMove(operationNode, point);

            // Calculate the touch offset registry used if a touch sensing operation is referenced
            // Get the Touch shift variable Start
            int touchOffsetRegistry;
            void HandleReferenceTouchSensingOperation(CbtNode referenceTouchSensingOperation)
            {
                switch (OperationManager.GetApplicationType(referenceTouchSensingOperation))
                {
                    case ApplicationType.TouchSensing:
                        touchOffsetRegistry = MotomanTouchSensingAuxiliaryMenu.GetTouchShiftVariable(referenceTouchSensingOperation);
                        break;
                    case ApplicationType.WeldingWithTouchSensing:
                        touchOffsetRegistry = MotomanWeldingWithTouchSensingAuxiliaryMenu.GetTouchShiftVariable(referenceTouchSensingOperation);
                        break;
                    default:
                        this.NotifyUser(OperationManager.GetOperationName(operationNode) + " references an invalid operation. Default offset registry will be applied");
                        touchOffsetRegistry = MotomanWeldingProcessMenu.GetTouchOperationShiftVariableStart(this.ProgramNode);
                        break;
                }
            }

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding)
            {
                var referencedTouchSensingOperation = OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot);

                if (referencedTouchSensingOperation != null)
                {
                    HandleReferenceTouchSensingOperation(referencedTouchSensingOperation);
                }
                else
                {
                    // Get the touch operation count before the current operation
                    var lastTouchSensingOperation = SetupManager.GetOperationNodes(this.SetupNode)
                        .TakeWhile(op => op != operationNode)
                        .LastOrDefault(op => OperationManager.GetApplicationType(op) == ApplicationType.TouchSensing || OperationManager.GetApplicationType(op) == ApplicationType.WeldingWithTouchSensing);

                    if (lastTouchSensingOperation != null)
                    {
                        HandleReferenceTouchSensingOperation(lastTouchSensingOperation);
                    }
                    else
                    {
                        touchOffsetRegistry = MotomanWeldingProcessMenu.GetTouchOperationShiftVariableStart(this.ProgramNode);
                    }
                }

                int shiftOutputType = MotomanWeldingProcessMenu.GetShiftOutputType(this.SetupNode);
                if (shiftOutputType != 0)
                {
                    point = PointManager.AddEventToPathPoint(
                        new ShiftOn
                        {
                            IsSynchronized = shiftOutputType == 2,
                            ShiftVariable = $"LP{touchOffsetRegistry:D4}",
                        },
                        EventIndex.Before,
                        point,
                        operationNode);
                }
            }

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditLastNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            // Apply first the base class method
            point = base.EditLastNoneRapidMove(operationNode, point);

            int shiftOutputType = MotomanWeldingProcessMenu.GetShiftOutputType(this.SetupNode);
            if (shiftOutputType != 0)
            {
                if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding)
            {
                point = PointManager.AddEventToPathPoint(
                    new ShiftOff()
                    {
                        IsSynchronized = shiftOutputType == 2,
                    },
                    EventIndex.After,
                    point,
                    operationNode);
            }
            }

            return point;
        }
    }
}