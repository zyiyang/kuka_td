﻿// <copyright file="MainProcessorMotoman.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    /// <summary>
    /// This class inherits from <see cref="MainProcessor"/>.
    /// <para>Implements the Motoman dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorMotoman : MainProcessor
    {
        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            this.ToolingActivationMacro = "CALL JOB:" + CommonToolingActivationSettings.GetToolingActivationMacro(operationNode);
            this.ToolingDeactivationMacro = "CALL JOB:" + CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode);
            base.EditOperationBeforeAllPointEdits(operationNode);
        }

        /// <inheritdoc />
        internal override PathNode AddToolChangeMacroEvent(CbtNode operationNode, PathNode point)
        {
            this.ToolChangeMacro = "CALL JOB:" + OperationManager.GetTool(operationNode).Name.Replace(" ", string.Empty).ToUpper();
            return base.AddToolChangeMacroEvent(operationNode, point);
        }
    }
}