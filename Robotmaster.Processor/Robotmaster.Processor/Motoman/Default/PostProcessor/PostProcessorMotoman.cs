// <copyright file="PostProcessorMotoman.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Component.Robot6J;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.FrameHierarchy;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Motoman.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Motoman.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.ApplicationLayer.Managers.Keys;
    using Robotmaster.Rise.Event;
    using Robotmaster.Wpf.Controls;

    internal class PostProcessorMotoman : PostProcessor
    {
        /// <summary>
        ///     A position data type enum.
        /// <para>
        ///     ///POSTYPE PULSE|BASE|ROBOT|USER.
        /// </para>
        /// </summary>
        internal enum MotomanPostype
        {
            /// <summary>
            ///     Pulse data
            /// </summary>
            Undefined,

            /// <summary>
            ///     Angle data
            /// </summary>
            Angle,

            /// <summary>
            ///     Pulse data
            /// </summary>
            Pulse,

            /// <summary>
            ///     Cartesian data, base coordinate system
            /// </summary>
            Base,

            /// <summary>
            ///     Cartesian data, robot coordinate system
            /// </summary>
            Robot,

            /// <summary>
            ///     Cartesian data, user coordinate system
            /// </summary>
            User,
        }

        /// <summary>
        ///     Gets the JOB pseudo instruction.
        /// </summary>
        /// <remarks>
        ///     Shows that it is a job.
        /// </remarks>
        internal static string JobPseudoInstruction { get; } = "/JOB";

        /// <summary>
        ///     Gets the POS pseudo instruction.
        /// </summary>
        /// <remarks>
        ///     Represents the position data.
        /// </remarks>
        internal static string PosPseudoInstruction { get; } = "//POS";

        /// <summary>
        ///     Gets the INST pseudo instruction.
        /// </summary>
        /// <remarks>
        ///     Represents that it is an instruction.
        /// </remarks>
        internal static string InstPseudoInstruction { get; } = "//INST";

        /// <summary>
        ///     Gets the NOP pseudo instruction.
        /// </summary>
        /// <remarks>
        ///     Represents a no operation command.
        /// </remarks>
        internal static string NopPseudoInstruction { get; } = "NOP";

        /// <summary>
        ///     Gets the FPT tag.
        /// </summary>
        /// <remarks>
        ///     Specifies the end-point of the arc (the point at which the curvature of the arc is to be changed).
        ///     Needed when path contains continuous circular arcs.
        ///     Supported on DX controllers.
        /// </remarks>
        internal static string ArcEndPointTag { get; } = "FPT";

        /// <summary>
        ///     Gets or sets the robot axis position variable Cxxxxx data type (PULSE|BASE|ROBOT|USER).
        /// </summary>
        internal virtual MotomanPostype RobotAxisTeachingPositionsDataType { get; set; } = MotomanPostype.Undefined;

        /// <summary>
        ///     Gets or sets the robot axis position variable Pyyyy data type (PULSE|BASE|ROBOT|USER).
        /// </summary>
        internal virtual MotomanPostype RobotAxisPositionVariableSpaceDataType { get; set; } = MotomanPostype.Undefined;

        /// <summary>
        ///     Gets or sets the number of position data items.
        /// <para>
        ///     Number of position data items : ///NPOS &lt;C&gt;,&lt;BC&gt;,&lt;EC&gt;,&lt;P&gt;,&lt;BP&gt;,&lt;EX&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[0]" /> : Number of robot axis teaching positions &lt;C&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[1]" /> : Number of base (rail) axis teaching positions &lt;BC&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[2]" /> : Number of station (rotary) axis teaching positions &lt;EC&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[3]" /> : Number of robot axis position variables &lt;P&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[4]" /> : Number of base (rail) axis position variables &lt;BP&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[5]" /> : Number of station (rotary) axis position variables &lt;EX&gt;.
        /// </para>
        /// </summary>
        internal virtual int[] NumberOfPositionDataItems { get; set; } = new int[6];

        /// <summary>
        ///     Gets or sets the number local variables.
        /// <para>
        ///     Number of local variables : ///LVARS &lt;LB&gt;,&lt;LI&gt;,&lt;LD&gt;,&lt;LR&gt;,&lt;LS&gt;,&lt;LP&gt;,&lt;LBP&gt;,&lt;LEX&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[0]" /> : Number of byte type local variables &lt;LB&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[1]" /> : Number of integer type local variables &lt;LI&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[2]" /> : Number of double-precision type local variables &lt;LD&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[3]" /> : Number of real number type local variables &lt;LR&gt;.
        /// </para>
        /// <para>
        ///     (Only for NX/DX Series) <see cref="T:int[4]" /> : Number of character type local variables &lt;LS&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[5]" /> : Number of robot axis position type local variables &lt;LP&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[6]" /> : Number of base axis position type local variables &lt;LBP&gt;.
        /// </para>
        /// <para>
        ///     <see cref="T:int[7]" /> : Number of external (station) axis position type local variables &lt;LEX&gt;.
        /// </para>
        /// </summary>
        internal virtual int[] NumberOfLocalVariables { get; set; } = new int[8];

        /// <summary>
        ///     Gets or sets the station axes key-value pair.
        /// <para>
        ///     Key : Station label (from ROBX).
        /// </para>
        /// <para>
        ///     Value : Joint index used to access joint value of individual station axis through the. <code>PathNode.JointValue</code> Dictionary.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[0]" /> : Station axis R1.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[1]" /> : Station axis R2.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[2]" /> : Station axis R3.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[3]" /> : Station axis R4.
        /// </para>
        /// </summary>
        internal virtual KeyValuePair<string, int>[] StationAxes { get; set; } = new KeyValuePair<string, int>[4];

        /// <summary>
        ///     Gets or sets the base axes key-value pair.
        /// <para>
        /// <para>
        ///     Key : Base label (from ROBX).
        /// </para>
        /// <para>
        ///     Value : Joint index used to access joint value of individual base axis through the. <code>PathNode.JointValue</code> Dictionary.
        /// </para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[0]" /> : Base axis E1.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[1]" /> : Base axis E2.
        /// </para>
        /// <para>
        ///     <see cref="T:KeyValuePair&lt;string, int&gt;[2]" /> : Base axis E3.
        /// </para>
        /// </summary>
        internal virtual KeyValuePair<string, int>[] BaseAxes { get; set; } = new KeyValuePair<string, int>[3];

        /// <summary>
        ///     Gets or sets the CurrentPostFileHeader header.
        /// <para>
        ///     The JOB pseudo instruction, the job name, the POS pseudo instruction and the number of position data items (NPOS) are output in this section.
        /// </para>
        /// <para>
        ///     JOB pseudo-code : <see cref="JobPseudoInstruction"/>.
        /// </para>
        /// <para>
        ///     Job name : <see cref="FormatJobName"/>.
        /// </para>
        /// <para>
        ///     POS pseudo-code : <see cref="PosPseudoInstruction"/>.
        /// </para>
        /// <para>
        ///     NPOS : <see cref="FormatNumberOfPositionDataItems"/>.
        /// </para>
        /// </summary>
        internal virtual string CurrentPostFileHeader
        {
            get => this.CurrentPostFile.FileSection.Header;
            set => this.CurrentPostFile.FileSection.Header = value;
        }

        /// <summary>
        ///     Gets or sets the CurrentPostFileFooter footer.
        /// </summary>
        internal virtual string CurrentPostFileFooter
        {
            get => this.CurrentPostFile.FileSection.Footer;
            set => this.CurrentPostFile.FileSection.Footer = value;
        }

        /// <summary>
        ///     Gets the RobotAxisTeachingPositions <see cref="FileSection"/>.
        /// <para>
        ///     The position data type, space data type, user coordinate system number, tool number and robot configuration are output in <see cref="RobotAxisTeachingPositions"/>.Header.
        /// </para>
        /// <para>
        ///     Position data type : <see cref="FormatRobotPositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Robot Output Format.
        /// </para>
        /// <para>
        ///     User coordinate system number : <see cref="FormatUserCoordinateSystemNumber"/>.
        /// </para>
        /// <para>
        ///     Tool number : <see cref="FormatToolNumber"/>.
        /// </para>
        /// <para>
        ///     Robot configuration : <see cref="FormatRobotConfiguration"/>.
        /// </para>
        /// <para>
        ///     The robot axis teaching positions &lt;C&gt;, the updated robot configuration (if any) and the overridden user/tool number (if any) are output in <see cref="RobotAxisTeachingPositions"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : Cxxxxx = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : Cxxxxx = &lt;X&gt;,&lt;Y&gt;,&lt;Z&gt;,&lt;Rx&gt;,&lt;Ry&gt;,&lt;Rz&gt;,&lt;Re&gt;.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     xxxxx : A number from 00000 to 09997.
        /// </remarks>
        internal virtual FileSection RobotAxisTeachingPositions => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[0];

        /// <summary>
        ///     Gets the BaseAxisTeachingPositions <see cref="FileSection"/>.
        /// <para>
        ///     The position data type, space data type, user coordinate system number, tool number and robot configuration are output in <see cref="BaseAxisTeachingPositions"/>.Header.
        /// </para>
        /// <para>
        ///     Position data type : <see cref="FormatBasePositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Base Output Format.
        /// </para>
        /// <para>
        ///     User coordinate system number : <see cref="FormatUserCoordinateSystemNumber"/>.
        /// </para>
        /// <para>
        ///     Tool number : <see cref="FormatToolNumber"/>.
        /// </para>
        /// <para>
        ///     Robot configuration : <see cref="FormatRobotConfiguration"/>.
        /// </para>
        /// <para>
        ///     The external base (rail) axis teaching positions &lt;C&gt; and the overridden user/tool number (if any) are output in <see cref="BaseAxisTeachingPositions"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : Cxxxxx = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : Cxxxxx = &lt;X&gt;,&lt;Y&gt;,&lt;Z&gt;,&lt;Rx&gt;,&lt;Ry&gt;,&lt;Rz&gt;,&lt;Re&gt;.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     xxxxx : A number from 00000 to 09997.
        /// </remarks>
        internal virtual FileSection BaseAxisTeachingPositions => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[1];

        /// <summary>
        ///     Gets the StationAxisTeachingPositions <see cref="FileSection"/>.
        /// <para>
        ///     The position data type and space data type are output in <see cref="StationAxisTeachingPositions"/>.Header.
        /// </para>
        /// <para>
        ///     Position data type : <see cref="FormatStationPositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Station Output Format.
        /// </para>
        /// <para>
        ///     The external station (rotary) axis teaching positions &lt;C&gt; are output in <see cref="StationAxisTeachingPositions"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : Cxxxxx = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Angle data : EXyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;,&lt;4&gt;,&lt;5&gt;,&lt;6&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : N/A.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     xxxxx : A number from 00000 to 09997.
        /// </remarks>
        internal virtual FileSection StationAxisTeachingPositions => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[2];

        /// <summary>
        ///     Gets the RobotAxisPositionVariables <see cref="FileSection"/>.
        /// <para>
        ///     The position data type, space data type, user coordinate system number, tool number and robot configuration are output in <see cref="RobotAxisPositionVariables"/>.Header.
        /// </para>
        /// <para>
        ///     Position data type : <see cref="FormatRobotPositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Robot Output Format.
        /// </para>
        /// <para>
        ///     User coordinate system number : <see cref="FormatUserCoordinateSystemNumber"/>.
        /// </para>
        /// <para>
        ///     Tool number : <see cref="FormatToolNumber"/>.
        /// </para>
        /// <para>
        ///     Robot configuration : <see cref="FormatRobotConfiguration"/>.
        /// </para>
        /// <para>
        ///     The robot axis position variables &lt;P&gt;, the updated robot configuration (if any) and the overridden user/tool number (if any) are output in <see cref="RobotAxisPositionVariables"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : Pyyyy = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : Pyyyy = &lt;X&gt;,&lt;Y&gt;,&lt;Z&gt;,&lt;Rx&gt;,&lt;Ry&gt;,&lt;Rz&gt;,&lt;Re&gt;.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     yyyy : A number from 0000 to 9999.
        /// </remarks>
        internal virtual FileSection RobotAxisPositionVariables => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[3];

        /// <summary>
        ///     Gets the BaseAxisPositionVariables <see cref="FileSection"/>.
        /// <para>
        ///     The position data type and space data type  are output in <see cref="BaseAxisPositionVariables"/>.Header.
        /// </para>
        /// <para>
        ///     Position data type : <see cref="FormatBasePositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Base Output Format.
        /// </para>
        /// <para>
        ///     The external base (rail) axis position variables &lt;BP&gt; are output in <see cref="BaseAxisPositionVariables"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : BPyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     yyyy : A number from 0000 to 9999.
        /// </remarks>
        internal virtual FileSection BaseAxisPositionVariables => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[4];

        /// <summary>
        ///     Gets the StationAxisPositionVariables <see cref="FileSection"/>.
        /// <para>
        ///     The position data type and space data type are output in <see cref="StationAxisPositionVariables"/>.Header.
        /// </para>
        /// <para>
        ///     POSTYPE : <see cref="FormatStationPositionDataType"/>.
        /// </para>
        /// <para>
        ///     Space data type : Station Output Format.
        /// </para>
        /// <para>
        ///     The external station (rotary) axis position variables &lt;EX&gt; are output in <see cref="StationAxisPositionVariables"/>.StreamWriter.
        /// </para>
        /// <para>
        ///     Pulse data : EXyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;,&lt;4&gt;,&lt;5&gt;,&lt;6&gt;.
        /// </para>
        /// <para>
        ///     Angle data : EXyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;,&lt;4&gt;,&lt;5&gt;,&lt;6&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : N/A.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     yyyy : A number from 0000 to 9999.
        /// </remarks>
        internal virtual FileSection StationAxisPositionVariables => this.CurrentPostFile.FileSection.SubFileSections[0].SubFileSections[5];

        /// <summary>
        ///     Gets or sets the Moves header.
        /// <para>
        ///     The instruction pseudo-code (INST), date (DATE), job comment (COMM), job attribute (ATTR), relative job teaching coordinate system (FRAME), control groups (GROUP#) and number of local variables (LVARS) are output in this section.
        /// </para>
        /// <para>
        ///     Instruction pseudo-code : <see cref="InstPseudoInstruction"/>.
        /// </para>
        /// <para>
        ///      Date : <see cref="FormatDate"/>.
        /// </para>
        /// <para>
        ///     Job comment : <see cref="FormatJobComment"/>.
        /// </para>
        /// <para>
        ///     Job attribute : <see cref="FormatJobAttribute"/>.
        /// </para>
        /// <para>
        ///     Relative job teaching coordinate system : <see cref="FormatRelativeJobTeachingCoordinateSystem"/>.
        /// </para>
        /// <para>
        ///     Control groups : <see cref="FormatControlGroups"/>.
        /// </para>
        /// <para>
        ///     Number of local variables : <see cref="FormatNumberOfLocalVariables"/>.
        /// </para>
        /// </summary>
        internal virtual string MovesHeader
        {
            get => this.CurrentPostFile.FileSection.SubFileSections[1].Header;
            set => this.CurrentPostFile.FileSection.SubFileSections[1].Header = value;
        }

        /// <summary>
        ///     Gets the Moves sub <see cref="FileSection"/>.
        /// <para>
        ///     The move instructions are output in this section.
        /// </para>
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Formats the job name.
        /// <para>
        ///     //NAME &lt;JOB NAME&gt;.
        /// </para>
        /// <para>
        ///     &lt;JOB NAME&gt; :   Job name (max: 32 characters).
        /// </para>
        /// </summary>
        /// <param name="postfile">Current post-file.</param>
        /// <returns>Formatted job name.</returns>
        internal virtual string FormatJobName(PostFile postfile)
        {
            return "//NAME " + postfile.FileName;
        }

        /// <summary>
        ///     Formats the number of position data items.
        /// <para>
        ///     ///NPOS &lt;C&gt;,&lt;BC&gt;,&lt;EC&gt;,&lt;P&gt;,&lt;BP&gt;,&lt;EX&gt;.
        /// </para>
        /// <para>
        ///     &lt;C&gt;     : Number of robot axis teaching positions.
        /// </para>
        /// <para>
        ///     &lt;BC&gt;    : Number of base axis teaching positions.
        /// </para>
        /// <para>
        ///     &lt;EC&gt;    : Number of external (station) axis teaching positions.
        /// </para>
        /// <para>
        ///     &lt;P&gt;     : Number of robot axis position variables.
        /// </para>
        /// <para>
        ///     &lt;BP&gt;    : Number of base axis position variables.
        /// </para>
        /// <para>
        ///     &lt;EX&gt;    : Number of external (station) axis position variables.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     NPOS for position variables are set based on the number of points used in the MOVE section (not the HEADER section).
        ///     ATTENTION : It is possible to have position variables in the MOVE section that are not defined in the HEADER (they are read from the controller), that's why NPOS has to be set carefully.
        /// </remarks>
        /// <returns>Formatted number of position data items.</returns>
        internal virtual string FormatNumberOfPositionDataItems()
        {
            int[] tempArray = this.NumberOfPositionDataItems;

            tempArray[3] -= MotomanProcessorSwitches.GetRobotAxisPositionVariableStartingLocation(this.CellSettingsNode);
            tempArray[4] -= MotomanProcessorSwitches.GetBaseAxisPositionVariableStartingLocation(this.CellSettingsNode);
            tempArray[5] -= MotomanProcessorSwitches.GetExternalAxisPositionVariableStartingLocation(this.CellSettingsNode);

            return "///NPOS " + string.Join(",", tempArray);
        }

        /// <summary>
        ///     Formats the user coordinate system number.
        /// <para>
        ///     ///USER &lt;N&gt;.
        /// </para>
        /// <para>
        ///     &lt;N&gt; :   User coordinate system number.
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted user coordinate system number.</returns>
        internal virtual string FormatUserCoordinateSystemNumber(CbtNode operationNode)
        {
            return "///USER " + OperationManager.GetUserFrame(operationNode).Number;
        }

        /// <summary>
        ///     Formats the tool number.
        /// <para>
        ///     ///TOOL &lt;N&gt;.
        /// </para>
        /// <para>
        ///     &lt;N&gt; :   Tool number.
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted tool number.</returns>
        internal virtual string FormatToolNumber(CbtNode operationNode)
        {
            return "///TOOL " + OperationManager.GetTcpId(operationNode);
        }

        /// <summary>
        ///     Formats the position data type of the robot.
        /// <para>
        ///     ///POSTYPE PULSE|BASE|ROBOT|USER.
        /// </para>
        /// <para>
        ///     |PULSE|     : Pulse data.
        /// </para>
        /// <para>
        ///     |BASE|      : Cartesian data, base coordinate system.
        /// </para>
        /// <para>
        ///     |ROBOT|     : Cartesian data, robot coordinate system.
        /// </para>
        /// <para>
        ///     |USER|      : Cartesian data, user coordinate system.
        /// </para>
        /// </summary>
        /// <returns>Formatted position data type of the robot.</returns>
        internal virtual string FormatRobotPositionDataType()
        {
            if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0)
            {
                switch (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode))
                {
                    case 0:
                        return "///POSTYPE BASE";
                    case 1:
                        return "///POSTYPE ROBOT";
                    case 2:
                        return "///POSTYPE USER";
                    default:
                        this.NotifyUser("Robot position data type is invalid.");
                        return "///POSTYPE"; // this will not load in controller
                }
            }

            return "///POSTYPE PULSE";
        }

        /// <summary>
        ///     Formats the position data type of the external base (rail) axis.
        /// <para>
        ///     ///POSTYPE PULSE|BASE|ROBOT|USER.
        /// </para>
        /// <para>
        ///     |PULSE|     : Pulse data.
        /// </para>
        /// <para>
        ///     |BASE|      : Cartesian data, base coordinate system.
        /// </para>
        /// <para>
        ///     |ROBOT|     : Cartesian data, robot coordinate system.
        /// </para>
        /// <para>
        ///     |USER|      : Cartesian data, user coordinate system.
        /// </para>
        /// </summary>
        /// <returns>Formatted position data type the external base (rail) axis.</returns>
        internal virtual string FormatBasePositionDataType()
        {
            if (MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 0)
            {
                switch (MotomanProcessorSwitches.GetReferenceFrameOfBase(this.CellSettingsNode))
                {
                    case 0:
                        return "///POSTYPE BASE";
                    case 1:
                        return "///POSTYPE ROBOT";
                    case 2:
                        return "///POSTYPE USER";
                    default:
                        this.NotifyUser("External base (rail) axis position data type is invalid.");
                        return "///POSTYPE"; // this will not load in controller
                }
            }

            return "///POSTYPE PULSE";
        }

        /// <summary>
        ///     Formats the position data type of the external station (rotary) axis.
        /// <para>
        ///     ///POSTYPE PULSE|BASE|ROBOT|USER.
        /// </para>
        /// <para>
        ///     |PULSE|     : Pulse data.
        /// </para>
        /// <para>
        ///     |BASE|      : Cartesian data, base coordinate system.
        /// </para>
        /// <para>
        ///     |ROBOT|     : Cartesian data, robot coordinate system.
        /// </para>
        /// <para>
        ///     |USER|      : Cartesian data, user coordinate system.
        /// </para>
        /// </summary>
        /// <returns>Formatted position data type of the external station (rotary) axis.</returns>
        internal virtual string FormatStationPositionDataType()
        {
            return MotomanProcessorSwitches.GetStationOutputFormat(this.CellSettingsNode) == 0 ? "///POSTYPE ANGLE" : "///POSTYPE PULSE";
        }

        /// <summary>
        ///     Formats the robot configuration that is used during and after this pseudo instruction.
        /// <para>
        ///     ///RCONF &lt;l&gt;,&lt;m&gt;,&lt;n&gt;,&lt;o&gt;,&lt;p&gt;,&lt;q&gt;.
        /// </para>
        /// <para>
        ///     &lt;l&gt; :   &#xD; 0: Flip, 1: No-flip.
        /// </para>
        /// <para>
        ///     &lt;m&gt; :   0: Upper arm, 1: Lower arm.
        /// </para>
        /// <para>
        ///     &lt;n&gt; :   0: Front, 1: Rear.
        /// </para>
        /// <para>
        ///     &lt;o&gt; :   0: R &lt; 180, 1: R &gt;= 180.
        /// </para>
        /// <para>
        ///     &lt;p&gt; :   0: T &lt; 180, 1: T &gt;= 180.
        /// </para>
        /// <para>
        ///     &lt;q&gt; :   0: S &lt; 180, 1: S &gt;= 180.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted robot configuration.</returns>
        internal virtual string FormatRobotConfiguration(PathNode point)
        {
            if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 1 || this.CachedPreviousPoint == null)
            {
                return string.Empty;
            }

            string currentConfig = this.StatusBits(point) + "," + this.TurnBits(point);
            string previousConfig = this.StatusBits(this.CachedPreviousPoint) + "," + this.TurnBits(this.CachedPreviousPoint);

            return this.CachedPreviousPoint.MoveSpace() == MoveSpace.Joint || currentConfig != previousConfig
                ? "///RCONF " + currentConfig + (MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode) != 0 ? ",0,0" : string.Empty) + "\r\n"
                : string.Empty;
        }

        /// <summary>
        ///     Formats the date following Motoman convention.
        /// <para>
        ///     ///DATE YYYY/MM/DD HH:TT.
        /// </para>
        /// <para>
        ///     YYYY : Year.
        /// </para>
        /// <para>
        ///     MM : Month.
        /// </para>
        /// <para>
        ///     DD : Day.
        /// </para>
        /// <para>
        ///     HH : Hour.
        /// </para>
        /// <para>
        ///     TT : Minute.
        /// </para>
        /// </summary>
        /// <returns>Formatted date following Motoman convention.</returns>
        internal virtual string FormatDate()
        {
            return "///DATE " + this.ProcessorDateTime.ToString("yyyy/MM/dd") + " " + this.ProcessorDateTime.ToString("HH:mm");
        }

        /// <summary>
        ///     Formats the job comment.
        /// <para>
        ///     ///COMM &lt;COMMENT CHARACTER LINE&gt;.
        /// </para>
        /// <para>
        ///     &lt;COMMENT CHARACTER LINE&gt; : maximum 32 characters.
        /// </para>
        /// </summary>
        /// <returns>Formatted job comment.</returns>
        internal virtual string FormatJobComment()
        {
            return "///COMM " + (string.IsNullOrWhiteSpace(MotomanProcessorSwitches.GetJobComment(this.CellSettingsNode)) ? "BY-ROBOTMASTER" : MotomanProcessorSwitches.GetJobComment(this.CellSettingsNode).Substring(0, Math.Min(MotomanProcessorSwitches.GetJobComment(this.CellSettingsNode).Length, 31)));
        }

        /// <summary>
        ///     Formats the job attribute.
        /// <para>
        ///    ///ATTR &lt;JD&gt;,&lt;DD&gt;,&lt;SC&gt;,{RO|WO|RW},&lt;RJ&gt;.
        /// </para>
        /// <para>
        ///     &lt;JD&gt;    : Job destroy.
        /// </para>
        /// <para>
        ///     &lt;DD&gt;    : Directory destroy.
        /// </para>
        /// <para>
        ///     &lt;SC&gt;    : Save complete.
        /// </para>
        /// <para>
        ///     &lt;RO&gt;    : Read only.
        /// </para>
        /// <para>
        ///     &lt;WO&gt;    : Write only.
        /// </para>
        /// <para>
        ///     &lt;RW&gt;    : Read/Write.
        /// </para>
        /// <para>
        ///     &lt;RJ&gt;    : Relative job.
        /// </para>
        /// </summary>
        /// <returns>Formatted job attribute.</returns>
        internal virtual string FormatJobAttribute()
        {
            string jobFilePermissionAttribute = string.Empty;
            switch (MotomanProcessorSwitches.GetJobFilePermissionAttribute(this.CellSettingsNode))
            {
                case 0:
                    jobFilePermissionAttribute = "RO";
                    break;
                case 1:
                    jobFilePermissionAttribute = "WO";
                    break;
                case 2:
                    jobFilePermissionAttribute = "RW";
                    break;
                default: break;
            }

            return "///ATTR " +
                   (MotomanProcessorSwitches.GetIsJobDestroyAttributeEnabled(this.CellSettingsNode) ? "JD," : string.Empty) +
                   (MotomanProcessorSwitches.GetIsDirectoryDestroyAttributeEnabled(this.CellSettingsNode) ? "DD," : string.Empty) +
                   (MotomanProcessorSwitches.GetIsSaveCompleteAttributeEnabled(this.CellSettingsNode) ? "SC," : string.Empty) +
                   jobFilePermissionAttribute +
                   (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 ? ",RJ" : string.Empty);
        }

        /// <summary>
        ///     Formats the relative job teaching coordinate system.
        /// <para>
        ///     ///FRAME BASE|ROBOT|USER&lt;N&gt;.
        /// </para>
        /// <para>
        ///     |BASE|  : Base coordinate system or WCS (Cartesian).
        /// </para>
        /// <para>
        ///     |ROBOT| : Robot coordinate system (Cartesian).
        /// </para>
        /// <para>
        ///     |USER|  : User coordinate system (Cartesian).
        /// </para>
        /// <para>
        ///     &lt;N&gt;   : User coordinate system number.
        /// </para>
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted relative job teaching coordinate system.</returns>
        internal virtual string FormatRelativeJobTeachingCoordinateSystem(CbtNode operationNode)
        {
            string referenceFrame = string.Empty;

            switch (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode))
            {
                case 0:
                    referenceFrame = "BASE";
                    break;
                case 1:
                    referenceFrame = "ROBOT";
                    break;
                case 2:
                    referenceFrame = "USER " + OperationManager.GetUserFrame(operationNode).Number;
                    break;
                default:
                    break;
            }

            return "////FRAME " + referenceFrame;
        }

        /// <summary>
        ///     Formats the control group(s).
        /// <para>
        ///     ///GROUP# &lt;m1&gt;,&lt;m2&gt;,&lt;m3&gt;.
        /// </para>
        /// <para>
        ///     &lt;m1&gt;,&lt;m2&gt; and &lt;m3&gt; can take the value for any of the 3 units:
        /// </para>
        /// <para>
        ///     ROBOT   : RB1,RB2,...,RB8.
        /// </para>
        /// <para>
        ///     BASE    : BS1,BS2,...,BS8.
        /// </para>
        /// <para>
        ///     STATION : ST1,ST2,...,ST8.
        /// </para>
        /// </summary>
        /// <returns>Formatted control group(s).</returns>
        /// <remarks>
        ///     The control group numbers are taken from the "groupNumber" tag found in the ROBX.
        ///     The control unit numbers are taken from the "label" tag found in the ROBX.
        /// </remarks>
        internal virtual string FormatControlGroups()
        {
            // All manipulators: robots + rotaries + rails
            IEnumerable<CbtNode> allManipulatorNodes = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode));

            // TODO Re-factor the line below so that we do not expose the Manipulator class once AT-1139 is DONE (Blocked by)
            IEnumerable<Manipulator> general6JRobots = allManipulatorNodes.SelectMany(node => node.GetComponents<Manipulator>()).Where(robot => robot is General6JRobot);

            // All robots and external axes joints
            IEnumerable<Joint> allRobotManipulatorsAndExternalAxesManipulators = this.RobotJoints.Concat(this.ExternalAxesJoints);

            // Sorted list containing all distinct group numbers
            List<string> controlGroups = allRobotManipulatorsAndExternalAxesManipulators
                                        .Select(manipulator => manipulator.GroupNumber)
                                        .Distinct()
                                        .OrderBy(x => x)
                                        .ToList();

            string formattedControlGroups = string.Empty;
            int cgCount = controlGroups.Count();

            for (var i = 1; i <= cgCount; i++)
            {
                formattedControlGroups += "///GROUP" + controlGroups[i - 1] + " ";

                int srCount = general6JRobots.Count(x => int.Parse(x.MainJoints[0].GroupNumber) == i);

                //// Example of dual robots:
                ////    ///GROUP1 RB1
                ////    ///GROUP2 RB2
                for (var ix = 1; ix <= srCount; ix++)
                {
                    //// Example of robot name:
                    ////    RB1,RB2
                    formattedControlGroups += "RB" + ix + ",";
                }

                //// Example of single robot with dual rotaries:
                ////    ///GROUP1 RB1
                ////    ///GROUP2 ST1,ST2
                if (this.Rotaries.Any())
                {
                    //// Example of station (rotary) axis name:
                    ////    ST1,ST2
                    string[] rotaryNames = this.Rotaries.Where(x => int.Parse(x.GroupNumber) == i).Select(k => k.Name.Substring(0, 3)).OrderBy(num => num).Distinct().ToArray();
                    formattedControlGroups += string.Join(",", rotaryNames);
                }

                //// Example of single robot with single rail:
                ////    ///GROUP1 RB1,BS1
                if (this.Rails.Any())
                {
                    //// Example of base (rail) axis name:
                    ////    BS1
                    string[] railNames = this.Rails.Where(x => int.Parse(x.GroupNumber) == i).Select(k => k.Name.Substring(0, 3)).OrderBy(num => num).Distinct().ToArray();
                    formattedControlGroups += string.Join(",", railNames);
                }

                if (formattedControlGroups[formattedControlGroups.Length - 1] == ',')
                {
                    formattedControlGroups = formattedControlGroups.Remove(formattedControlGroups.Length - 1);
                }

                if (i != cgCount)
                {
                    formattedControlGroups += "\r\n";
                }
            }

            return formattedControlGroups;
        }

        /// <summary>
        ///     Formats the number of local variables.
        /// <para>
        ///    ///LVARS &lt;LB&gt;,&lt;LI&gt;,&lt;LD&gt;,&lt;LR&gt;,&lt;LS&gt;,&lt;LP&gt;,&lt;LBP&gt;,&lt;LEX&gt;.
        /// </para>
        /// <para>
        ///     &lt;LB&gt;    : Number of byte type local variables.
        /// </para>
        /// <para>
        ///     &lt;LI&gt;    : Number of integer type local variables.
        /// </para>
        /// <para>
        ///     &lt;LD&gt;    : Number of double-precision type local variables.
        /// </para>
        /// <para>
        ///     &lt;LR&gt;    : Number of real number type local variables.
        /// </para>
        /// <para>
        ///     (Only for NX/DX Series) &lt;LS&gt;    : Number of character type local variables.
        /// </para>
        /// <para>
        ///     &lt;LP&gt;    : Number of robot axis position type local variables.
        /// </para>
        /// <para>
        ///     &lt;LBP&gt;   : Number of base axis position type local variables.
        /// </para>
        /// <para>
        ///     &lt;LEX&gt;   : Number of external (station) axis position type local variables.
        /// </para>
        /// </summary>
        /// <returns>Formatted number of local variables.</returns>
        internal virtual string FormatNumberOfLocalVariables()
        {
            List<int> numberOfLocalVariables = this.NumberOfLocalVariables.ToList();
            if (MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode) == 0)
            {
                numberOfLocalVariables.RemoveAt(4); // No LS variables.
            }

            return "///LVARS " + string.Join(",", numberOfLocalVariables);
        }

        /// <summary>
        ///     Starts the main <paramref name="mainPostFile"/> formatting following <see cref="Motoman"/> convention.
        ///     <para>Initializes <paramref name="mainPostFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="mainPostFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartMainPostFile(PostFile mainPostFile, CbtNode operationNode, PathNode point)
        {
            mainPostFile.FileSection.Header += JobPseudoInstruction + "\r\n" +
                                               this.FormatJobName(mainPostFile) + "\r\n";

            mainPostFile.FileSection.StreamWriter.Write(this.FormatNumberOfPositionDataItems() + "\r\n");

            if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 && MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 2)
            {
                // Cartesian output.
                mainPostFile.FileSection.StreamWriter.Write(this.FormatUserCoordinateSystemNumber(operationNode) + "\r\n");
            }

            mainPostFile.FileSection.StreamWriter.Write(this.FormatToolNumber(operationNode) + "\r\n");

            mainPostFile.FileSection.StreamWriter.Write(InstPseudoInstruction + "\r\n" +
                                                        this.FormatDate() + "\r\n" +
                                                        this.FormatJobComment() + "\r\n" +
                                                        this.FormatJobAttribute() + "\r\n" +
                                                        this.FormatControlGroups() + "\r\n" +
                                                        NopPseudoInstruction + "\r\n");

            mainPostFile.FileSection.Footer = "END" + "\r\n";
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Motoman"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Position data
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Move instruction

            // Create a sub-file section for every type of position data: |C|,|BC|,|EC|,|P|,|BP|,|EX|.
            for (var i = 0; i < 6; i++)
            {
                postFile.FileSection.SubFileSections[0].SubFileSections.Add(new FileSection());
            }

            postFile.FileSection.Header += JobPseudoInstruction + "\r\n" +
                                           this.FormatJobName(postFile) + "\r\n" +
                                           PosPseudoInstruction + "\r\n";

            // Robot axis teaching positions <C>
            postFile.FileSection.SubFileSections[0].SubFileSections[0].Header += this.FormatRobotPositionDataType() + "\r\n";

            // Cartesian or pulse output
            postFile.FileSection.SubFileSections[0].SubFileSections[0].Header += (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 ? "///RECTAN" : "///PULSE") + "\r\n";
            postFile.FileSection.SubFileSections[0].SubFileSections[0].Header += this.FormatRobotConfiguration(point);

            // Base (rail) axis teaching positions <BC>
            if (this.Rails.Any())
            {
                postFile.FileSection.SubFileSections[0].SubFileSections[1].Header += this.FormatBasePositionDataType() + "\r\n" +
                                                         (MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 0 ? "///RECTAN" : "///PULSE") + "\r\n" +
                                                         (MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode) != 0 ? "///RCONF 0,0,0,0,0,0,0,0" : "///RCONF 0,0,0,0,0,0") + "\r\n";
            }

            // Station (rotary) axis teaching positions <EC>
            if (this.Rotaries.Any())
            {
                postFile.FileSection.SubFileSections[0].SubFileSections[2].Header += this.FormatStationPositionDataType() + "\r\n" +
                                                            (MotomanProcessorSwitches.GetStationOutputFormat(this.CellSettingsNode) == 0 ? "///ANGLE" : "///PULSE") + "\r\n";
            }

            postFile.FileSection.SubFileSections[1].Header += InstPseudoInstruction + "\r\n" +
                                                              this.FormatDate() + "\r\n" +
                                                              this.FormatJobComment() + "\r\n" +
                                                              this.FormatJobAttribute() + "\r\n";
            if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0)
            {
                // Cartesian output.
                postFile.FileSection.SubFileSections[1].Header += this.FormatRelativeJobTeachingCoordinateSystem(operationNode) + "\r\n";
            }

            postFile.FileSection.SubFileSections[1].Header += this.FormatControlGroups() + "\r\n";

            if (MotomanProcessorSwitches.GetJointSpaceMoveOutputVariables(this.CellSettingsNode) == 1)
            {
                if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 && MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 2)
                {
                    // Cartesian output.
                    postFile.FileSection.SubFileSections[0].SubFileSections[0].StreamWriter.WriteLine(this.FormatUserCoordinateSystemNumber(operationNode));
                }

                postFile.FileSection.SubFileSections[0].SubFileSections[0].StreamWriter.WriteLine(this.FormatToolNumber(operationNode));
                this.OutputReferencePoint(point, operationNode, 1);
            }

            postFile.FileSection.Footer += "END" + "\r\n";
        }

        /// <summary>
        ///     Calls the current <see cref="PostFile"/> in its parent <see cref="PostFile"/> following <see cref="Motoman"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to be called in its parent.</param>
        /// <param name="operationNode">The current operation.</param>
        /// <param name="point">The current point.</param>
        internal virtual void CallPostFileIntoParent(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            //// Example:
            //// CALL JOB:BTEST1 UF#(1)
            postFile.Parent.FileSection.StreamWriter.Write("CALL JOB:" + postFile.FileName);
            if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0)
            {
                // Cartesian output.
                postFile.Parent.FileSection.StreamWriter.Write(" UF#(" + OperationManager.GetUserFrame(operationNode).Number + ")");
            }

            postFile.Parent.FileSection.StreamWriter.WriteLine();
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formatting following <see cref="Motoman"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void EndPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Base (rail) axis position variables <BP>
            if (this.NumberOfPositionDataItems[4] > MotomanProcessorSwitches.GetBaseAxisPositionVariableStartingLocation(this.CellSettingsNode))
            {
                this.BaseAxisPositionVariables.Header += MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 0
                                                            ? "///POSTYPE BASE" + "\r\n" + "///RECTAN" + "\r\n"
                                                            : "///POSTYPE PULSE" + "\r\n" + "///PULSE" + "\r\n";
            }

            // Station (rotary) axis position variables <EX>
            if (this.NumberOfPositionDataItems[5] > MotomanProcessorSwitches.GetExternalAxisPositionVariableStartingLocation(this.CellSettingsNode))
            {
                this.StationAxisPositionVariables.Header += MotomanProcessorSwitches.GetStationOutputFormat(this.CellSettingsNode) == 0
                                                            ? "///POSTYPE ANGLE" + "\r\n" + "///ANGLE" + "\r\n"
                                                            : "///POSTYPE PULSE" + "\r\n" + "///PULSE" + "\r\n";
            }

            // Number of position data items
            this.CurrentPostFileHeader += this.FormatNumberOfPositionDataItems() + "\r\n";

            ////TODO - Local Variables are not always output!! In some cases (at least XRC controller) Local Variables throw an error in the controller
            this.MovesHeader += this.FormatNumberOfLocalVariables() + "\r\n" + NopPseudoInstruction + "\r\n";

            this.RobotAxisTeachingPositionsDataType = MotomanPostype.Undefined;
            this.RobotAxisPositionVariableSpaceDataType = MotomanPostype.Undefined;
            Array.Clear(this.NumberOfLocalVariables, 0, 0);
            this.ResetNumberOfPositionDataItems();
        }

        /// <summary>
        ///     Ends the main <paramref name="postFile"/> formatting following <see cref="Motoman"/> convention.
        ///     <para> Replaces or inserts information in the file that could not be computed when the file was started (see <see cref="StartMainPostFile"/>).</para>
        /// </summary>
        /// <param name="postFile">The main <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void EndMainPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            Array.Clear(this.NumberOfLocalVariables, 0, 0);
            this.ResetNumberOfPositionDataItems();
        }

        /// <summary>
        ///     Find last available name among the children of the current <see cref="PostFile"/> parent.
        /// </summary>
        /// <param name="postFile">Current <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="point">Current point.</param>
        /// <returns>Last available name.</returns>
        internal virtual string FindNextAvailableName(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Logic can be added here to manage naming of Robotmaster sub-program(s).
            string subPostFileNamePattern = postFile.Parent.FileName + "_";
            var maxIndex = 1;
            while (this.CurrentPostFile.Parent.Children.Any(pf => pf.FileName == subPostFileNamePattern + maxIndex))
            {
                maxIndex++;
            }

            return subPostFileNamePattern + maxIndex;
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // Multi-file Output
            if ((MotomanProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) > 0) && (this.NumberOfPositionDataItems[0] > MotomanProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode)))
            {
                this.EndPostFile(this.CurrentPostFile, operationNode, point);
                var newPostFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostFile);
                this.CurrentPostFile = newPostFile;
                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, operationNode, point);
                this.CurrentPostFile.FileExtension = FormatManagerMotoman.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, operationNode, point);

                // Multi-file output: Robot axis teaching positions <C>
                this.RobotAxisTeachingPositions.Header += "///RCONF " + this.StatusBits(point) + "," + this.TurnBits(point) + (MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode) != 0 ? ",0,0" : string.Empty) + "\r\n";

                if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 && MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 2)
                {
                    // Cartesian output.
                    this.RobotAxisTeachingPositions.Header += this.FormatUserCoordinateSystemNumber(operationNode) + "\r\n";
                }

                this.RobotAxisTeachingPositions.Header += this.FormatToolNumber(operationNode) + "\r\n";

                // Multi-file output: Base (rail) axis teaching positions <BC>
                if (this.Rails.Any())
                {
                    if (MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 0 && MotomanProcessorSwitches.GetReferenceFrameOfBase(this.CellSettingsNode) == 2)
                    {
                        // Cartesian output.
                        this.BaseAxisTeachingPositions.Header += this.FormatUserCoordinateSystemNumber(operationNode) + "\r\n";
                    }

                    this.BaseAxisTeachingPositions.Header += this.FormatToolNumber(operationNode) + "\r\n";
                }

                this.CallPostFileIntoParent(this.CurrentPostFile, operationNode, point);
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            // Update user coordinate system number.
            if (previousOperationNode == null || OperationManager.GetUserFrame(previousOperationNode).Number != OperationManager.GetUserFrame(operationNode).Number)
            {
                // Update user frame in robot axis teaching positions section.
                if (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0)
                {
                    // Cartesian output.
                    if (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 2)
                    {
                        this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatUserCoordinateSystemNumber(operationNode));
                    }

                    if (this.Rails.Any() && MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 0 && MotomanProcessorSwitches.GetReferenceFrameOfBase(this.CellSettingsNode) == 2)
                    {
                        // Update user frame in base axis teaching positions section.
                        this.BaseAxisTeachingPositions.StreamWriter.WriteLine(this.FormatUserCoordinateSystemNumber(operationNode));
                    }
                }
            }

            // Update tool number.
            if (previousOperationNode == null || OperationManager.GetTcpId(previousOperationNode) != OperationManager.GetTcpId(operationNode))
            {
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatToolNumber(operationNode));

                if (this.Rails.Any())
                {
                    // Update tool number in base axis teaching positions section.
                    this.BaseAxisTeachingPositions.StreamWriter.WriteLine(this.FormatToolNumber(operationNode));
                }
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            if (this.Rotaries.Any())
            {
                this.StationAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_R1").Success);
                this.StationAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_R2").Success);
                this.StationAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_R3").Success);
                this.StationAxes[3] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_R4").Success);
            }

            if (this.Rails.Any())
            {
                this.BaseAxes[0] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_E1").Success);
                this.BaseAxes[1] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_E2").Success);
                this.BaseAxes[2] = this.JointFormat.FirstOrDefault(label => Regex.Match(label.Key, "_E3").Success);
            }

            // Pyyyy starting location.
            this.NumberOfPositionDataItems[3] = MotomanProcessorSwitches.GetRobotAxisPositionVariableStartingLocation(this.CellSettingsNode);

            // BPyyyy starting location.
            this.NumberOfPositionDataItems[4] = MotomanProcessorSwitches.GetBaseAxisPositionVariableStartingLocation(this.CellSettingsNode);

            // EXyyyy starting location.
            this.NumberOfPositionDataItems[5] = MotomanProcessorSwitches.GetExternalAxisPositionVariableStartingLocation(this.CellSettingsNode);

            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            // Multi-file output
            if (MotomanProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) <= 0) //// and no sub-programNode exist
            {
                this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.FileExtension = FormatManagerMotoman.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
            else
            {
                this.CurrentPostFile.InsertIntermediateParent(); //// Create main (master) program
                this.CurrentPostFile.Parent.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.Parent.FileExtension = FormatManagerMotoman.MainProgramExtension;
                this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, firstOperation, firstPoint);
                this.CurrentPostFile.FileExtension = FormatManagerMotoman.MainProgramExtension;

                this.CallPostFileIntoParent(this.CurrentPostFile, firstOperation, firstPoint);
                this.ResetNumberOfPositionDataItems();
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void RunAfterProgramOutput()
        {
            CbtNode lastOperation = ProgramManager.GetOperationNodes(this.ProgramNode).Last();
            PathNode lastPoint = OperationManager.GetLastPoint(lastOperation);

            if (MotomanProcessorSwitches.GetMaximumNumberOfPointsPerFile(this.CellSettingsNode) <= 0)
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);
            }
            else
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);

                // Multi-file output
                PostFile parentPostFile = this.CurrentPostFile.Parent;
                while (!ReferenceEquals(parentPostFile, this.MainPostFile.Parent)) //// While root is not reached
                {
                    // End main post-file
                    this.EndMainPostFile(parentPostFile, lastOperation, lastPoint);
                    parentPostFile = parentPostFile.Parent;
                }
            }
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.Write(" " + inlineEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            if (MotomanProcessorSwitches.GetJointSpaceMoveOutputVariables(this.CellSettingsNode) == 1)
            {
                this.RobotAxisTeachingPositions.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.User, this.RobotAxisTeachingPositionsDataType));
                this.RobotAxisTeachingPositionsDataType = MotomanPostype.User;
            }

            //// Example of robot configuration when using Cartesian output format (relative job):
            ////    RCONF 0,0,0,1,1,0,0,0
            this.RobotAxisTeachingPositions.StreamWriter.Write(this.FormatRobotConfiguration(point));

            //// Example of robot axis teaching position data:
            ////    C0002=80.979,-5.049,-33.372,180.000,22.500,0.000
            this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));

            //// Example of robot axis teaching position move:
            ////    MOVL C0002 V=10.0
            this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));

            //// Example of base (rail) axis teaching position data:
            ////    BC0002=0,0,0
            //// Example of station (rotary) axis teaching position data:
            ////    EC0002=-2,11,0,14
            //// Example of external axes teaching position move:
            ////    MOVJ C00002 BC00002 VJ=25.00 +MOVJ EC00002
            this.OutputBaseAxisAsTeachingPosition(point, operationNode);
            this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
            this.Moves.Write(this.FormatPositionLevel(point, operationNode));
            this.OutputStationAxesAsTeachingPosition(point, operationNode);

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.Moves.WriteLine();
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (MotomanProcessorSwitches.GetJointSpaceMoveOutputVariables(this.CellSettingsNode) == 1)
            {
                this.RobotAxisTeachingPositions.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.User, this.RobotAxisTeachingPositionsDataType));
                this.RobotAxisTeachingPositionsDataType = MotomanPostype.User;
            }

            //// Example of robot configuration when using Cartesian output format (relative job):
            ////    RCONF 0,0,0,1,1,0,0,0
            this.RobotAxisTeachingPositions.StreamWriter.Write(this.FormatRobotConfiguration(point));

            PathNode previousPoint = point.PreviousPoint(operationNode);
            PathNode nextPoint = point.NextPoint(operationNode);

            // Single Circular Arc (SCA)
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for a single circular arc:
            //    C0100=-323.000,8.000,-200.000,180.000,0.000,0.000     // P0: Copy of P1 as a LC move
            //    C0101=-323.000,8.000,-200.000,180.000,0.000,0.000     // P1: First CC move in Motoman robotic code (arc start point)
            //    C0102=-328.657,5.657,-200.000,180.000,0.000,0.000     // P2: First CC move in Robotmaster UI (arc midpoint)
            //    C0103=-331.000,0.000,-200.000,180.000,0.000,0.000     // P3: Second CC move in Robotmaster UI (arc end point)
            //    C0104=-331.000,0.000,-200.000,180.000,0.000,0.000     // P4: Copy of P3 as a LC move
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for a single circular arc:
            //    MOVL C0100 V=50.0     // P0
            //    MOVC C0101 V=50.0     // P1
            //    MOVC C0102 V=50.0     // P2
            //    MOVC C0103 V=50.0     // P3
            //    MOVL C0104 V=50.0     // P4
            // ********************************************************************************************************************
            //
            // Continuous Circular Arcs (CCA) - DX controller
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for continuous circular arcs:
            //    C0100=44.351,-27.073,0.000,180.000,0.000,0.000    // P0: Copy of P1 as a LC move
            //    C0101=44.351,-27.073,0.000,180.000,0.000,0.000    // P1: First CC move in Motoman robotic code (arc start point)
            //    C0102=22.627,-5.348,0.000,180.000,0.000,0.000     // P2: First CC move in Robotmaster UI (arc midpoint)
            //    C0103=0.902,-27.073,0.000,180.000,0.000,0.000     // P3: Second CC move in Robotmaster UI (arc end point with FPT)
            //    C0104=22.627,-48.797,0.000,180.000,0.000,0.000    // P4: Third CC move in Robotmaster UI (arc midpoint)
            //    C0105=44.351,-27.073,0.000,180.000,0.000,0.000    // P5: Fourth CC move in Robotmaster UI (arc end point)
            //    C0106=44.351,-27.073,0.000,180.000,0.000,0.000    // P6: Copy of P5 as a LC move
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for continuous circular arcs:
            //    MOVL C0100 V=50.0         // P0
            //    MOVC C0101 V=50.0         // P1
            //    MOVC C0102 V=50.0         // P2
            //    MOVC C0103 V=50.0 FPT     // P3
            //    MOVC C0104 V=50.0         // P4
            //    MOVC C0105 V=50.0         // P5
            //    MOVL C0106 V=50.0         // P6
            // ********************************************************************************************************************
            //
            // Continuous Circular Arcs (CCA) - NX controller
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for continuous circular arcs:
            //    C0100=44.351,-27.073,0.000,180.000,0.000,0.000    // P0: Copy of P1 as a LC move
            //    C0101=44.351,-27.073,0.000,180.000,0.000,0.000    // P1: First CC move in Motoman robotic code (arc start point)
            //    C0102=22.627,-5.348,0.000,180.000,0.000,0.000     // P2: First CC move in Robotmaster UI (arc midpoint)
            //    C0103=0.902,-27.073,0.000,180.000,0.000,0.000     // P3: Second CC move in Robotmaster UI (arc end point)
            //    C0104=0.902,-27.073,0.000,180.000,0.000,0.000     // P4: Second CC move in Robotmaster UI as a LC move (linear interpolation step as separator)
            //    C0105=0.902,-27.073,0.000,180.000,0.000,0.000     // P5: Second CC move in Robotmaster UI (arc start point)
            //    C0106=22.627,-48.797,0.000,180.000,0.000,0.000    // P6: Third CC move in Robotmaster UI (arc midpoint)
            //    C0107=44.351,-27.073,0.000,180.000,0.000,0.000    // P7: Fourth CC move in Robotmaster UI (arc end point)
            //    C0108=44.351,-27.073,0.000,180.000,0.000,0.000    // P8: Copy of P7 as a LC move
            // ********************************************************************************************************************
            // Example of robot axis teaching position data for continuous circular arcs:
            //    MOVL C0100 V=50.0         // P0
            //    MOVC C0101 V=50.0         // P1
            //    MOVC C0102 V=50.0         // P2
            //    MOVC C0103 V=50.0         // P3
            //    MOVL C0104 V=50.0         // P4
            //    MOVC C0105 V=50.0         // P5
            //    MOVC C0106 V=50.0         // P6
            //    MOVC C0107 V=50.0         // P7
            //    MOVL C0108 V=50.0         // P8
            // ********************************************************************************************************************
            if (point.IsArcMiddlePoint() && previousPoint?.MoveType() != MoveType.Circular) //// P0-P1-P2 (SCA/CCA)
            {
                // Copy of arc start point as a LC move
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(previousPoint, operationNode));
                this.Moves.Write((this.Rotaries.Any() ? "SMOVL C" : "MOVL C") + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());
                this.OutputBaseAxisAsTeachingPosition(previousPoint, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(previousPoint, operationNode));
                this.Moves.Write(this.FormatPositionLevel(previousPoint, operationNode));
                this.OutputStationAxesAsTeachingPosition(previousPoint, operationNode);

                this.Moves.WriteLine();

                // Arc start point
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(previousPoint, operationNode));
                this.Moves.Write((this.Rotaries.Any() ? "SMOVC C" : "MOVC C") + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());
                this.OutputBaseAxisAsTeachingPosition(previousPoint, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(previousPoint, operationNode));
                this.Moves.Write(this.FormatPositionLevel(previousPoint, operationNode));
                this.OutputStationAxesAsTeachingPosition(previousPoint, operationNode);
                this.Moves.WriteLine();

                // Arc midpoint
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));
                this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                this.OutputStationAxesAsTeachingPosition(point, operationNode);
                this.Moves.WriteLine();
            }
            else if (nextPoint?.MoveType() != MoveType.Circular) //// P3-P4 (SCA), P5-P6 (CCA)
            {
                // Arc end point
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));
                this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                this.OutputStationAxesAsTeachingPosition(point, operationNode);
                this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
                this.Moves.WriteLine();

                // Copy of arc end point as a LC move
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                this.Moves.Write((this.Rotaries.Any() ? "SMOVL C" : "MOVL C") + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());
                this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                this.OutputStationAxesAsTeachingPosition(point, operationNode);
                this.Moves.WriteLine();
            }
            else if (previousPoint.IsArcMiddlePoint() && nextPoint.IsArcMiddlePoint()) //// P3 (CCA DX controller), P3-P4-P5 (CCA NX/DX controller)
            {
                // ERC/MRC/XRC controllers or NX controller or DX controller not using FPT technology
                int controllerType = MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode);
                if (controllerType == 0 || controllerType == 1 || (controllerType == 2 && !MotomanProcessorSwitches.GetIsCircularMoveTechnologyUsingFpt(this.CellSettingsNode)))
                {
                    // Arc end point of current circular movement (P3)
                    this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                    this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));
                    this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                    this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                    this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                    this.OutputStationAxesAsTeachingPosition(point, operationNode);
                    this.Moves.WriteLine();

                    // Linear interpolation step added (at identical point) to separate continuous circular arcs (P4)
                    this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                    this.Moves.Write((this.Rotaries.Any() ? "SMOVL C" : "MOVL C") + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());
                    this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                    this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                    this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                    this.OutputStationAxesAsTeachingPosition(point, operationNode);
                    this.Moves.WriteLine();

                    // Arc start point of following circular movement (P5)
                    this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                    this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));
                    this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                    this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                    this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                    this.OutputStationAxesAsTeachingPosition(point, operationNode);
                    this.Moves.WriteLine();
                }

                // DX controller using FPT technology
                if (controllerType == 2 && MotomanProcessorSwitches.GetIsCircularMoveTechnologyUsingFpt(this.CellSettingsNode))
                {
                    // Arc end-point with FPT (P3)
                    this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                    this.Moves.Write((this.Rotaries.Any() ? "SMOVC C" : "MOVC C") + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());
                    this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                    this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                    this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                    this.Moves.Write(" " + ArcEndPointTag);
                    this.OutputStationAxesAsTeachingPosition(point, operationNode);
                    this.Moves.WriteLine();
                }
            }
            else if (point.IsArcMiddlePoint() && previousPoint?.MoveType() == MoveType.Circular && nextPoint?.MoveType() == MoveType.Circular) //// P4 (CCA)
            {
                // Arc midpoint
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));
                this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));
                this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                this.OutputStationAxesAsTeachingPosition(point, operationNode);
                this.Moves.WriteLine();
            }
        }

        /// <inheritdoc/>
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            if (MotomanProcessorSwitches.GetJointSpaceMoveOutputVariables(this.CellSettingsNode) == 1)
            {
                this.RobotAxisTeachingPositions.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.User, this.RobotAxisTeachingPositionsDataType));
                this.RobotAxisTeachingPositionsDataType = MotomanPostype.User;
            }

            //// Example of robot configuration when using Cartesian output format (relative job):
            ////    RCONF 0,0,0,1,1,0,0,0
            this.RobotAxisTeachingPositions.StreamWriter.Write(this.FormatRobotConfiguration(point));

            //// Example of robot axis teaching position data:
            ////    C0002=80.979,-5.049,-33.372,180.000,22.500,0.000
            this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));

            //// Example of robot axis teaching position move:
            ////    MOVJ C0002 V=10.0
            this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));

            //// Example of base (rail) axis teaching position data:
            ////    BC0002=0,0,0
            //// Example of station (rotary) axis teaching position data:
            ////    EC0002=-2,11,0,14
            //// Example of external axes teaching position move:
            ////    MOVJ C00002 BC00002 VJ=25.00 +MOVJ EC00002
            this.OutputBaseAxisAsTeachingPosition(point, operationNode);
            this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
            this.Moves.Write(this.FormatPositionLevel(point, operationNode));
            this.OutputStationAxesAsTeachingPosition(point, operationNode);

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.Moves.WriteLine();
        }

        /// <inheritdoc/>
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            if (MotomanProcessorSwitches.GetJointSpaceMoveOutputVariables(this.CellSettingsNode) == 0)
            {
                this.RobotAxisPositionVariables.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.Pulse, this.RobotAxisPositionVariableSpaceDataType));
                this.RobotAxisPositionVariableSpaceDataType = MotomanPostype.Pulse;

                //// Example of robot axis teaching position data:
                ////    P0015=0,0,0,0,0,0
                this.RobotAxisPositionVariables.StreamWriter.WriteLine(this.FormatRobotAxisPositionVariableData(point, operationNode));

                //// Example of robot axis teaching position move:
                ////    MOVJ P0015 VJ=10.0
                this.Moves.Write(this.FormatRobotAxisPositionVariableMove(point, operationNode));

                //// Example of base (rail) axis position variable data:
                ////    BP0002=0,0,0
                //// Example of station (rotary) axis position variable data:
                ////    EX0002=-2,11,0,14
                //// Example of external axes position variable move:
                ////    MOVJ P00002 BP00002 VJ=25.00 +MOVJ EX00002
                this.OutputExternalAxesAsPositionVariable(point, operationNode);
            }
            else
            {
                this.RobotAxisTeachingPositions.StreamWriter.Write(this.CompareFormatPoseType(MotomanPostype.Pulse, this.RobotAxisTeachingPositionsDataType));
                this.RobotAxisTeachingPositionsDataType = MotomanPostype.Pulse;

                //// Example of robot axis teaching position data:
                ////    C00015=0,0,0,0,0,0
                this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));

                //// Example of robot axis teaching position move:
                ////    MOVJ C0002 V=10.0
                this.Moves.Write(this.FormatRobotAxisTeachingPositionMove(point, operationNode));

                //// Example of base (rail) axis teaching position data:
                ////    BC0002=0,0,0
                //// Example of station (rotary) axis teaching position data:
                ////    EC0002=-2,11,0,14
                //// Example of external axes teaching position move:
                ////    MOVJ C00002 BC00002 VJ=25.00 +MOVJ EC00002
                this.OutputBaseAxisAsTeachingPosition(point, operationNode);
                this.Moves.Write(this.FormatPlaySpeed(point, operationNode));
                this.Moves.Write(this.FormatPositionLevel(point, operationNode));
                this.OutputStationAxesAsTeachingPosition(point, operationNode);
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.Moves.WriteLine();
        }

        /// <summary>
        ///     Output reference point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="referenceNumber">The reference number.</param>
        internal virtual void OutputReferencePoint(PathNode point, CbtNode operationNode, int referenceNumber)
        {
            //// Example of robot configuration when using Cartesian output format (relative job):
            ////    RCONF 0,0,0,1,1,0,0,0
            this.RobotAxisTeachingPositions.StreamWriter.Write(this.FormatRobotConfiguration(point));

            //// Example of robot axis teaching position data:
            ////    C0002=80.979,-5.049,-33.372,180.000,22.500,0.000
            this.RobotAxisTeachingPositions.StreamWriter.WriteLine(this.FormatRobotAxisTeachingPositionData(point, operationNode));

            //// Example of robot axis teaching position move:
            ////    REFP C0002
            this.Moves.Write((this.Rotaries.Any() ? "S" : string.Empty) + $"REFP {referenceNumber} C" + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted());

            //// Example of base (rail) axis teaching position data:
            ////    BC0002=0,0,0
            //// Example of station (rotary) axis teaching position data:
            ////    EC0002=-2,11,0,14
            //// Example of external axes teaching position move:
            ////    MOVJ C00002 BC00002 VJ=25.00 +MOVJ EC00002

            // External axes
            if (this.ExternalAxesJoints.Any())
            {
                // Filter for base (rail) axes
                if (this.Rails.Any())
                {
                    //// Example of base (rail) axis teaching position data:
                    ////    BC0002=0,0,0
                    this.BaseAxisTeachingPositions.StreamWriter.WriteLine(this.FormatBaseAxisTeachingPositionData(point, operationNode));

                    //// Example of base (rail) axis teaching position move:
                    ////    BC0002
                    this.Moves.Write(this.FormatBaseAxisTeachingPositionMove(point, operationNode));
                }

                // Filter for station (rotary) axes
                if (this.Rotaries.Any())
                {
                    //// Example of station (rotary) axis teaching position data:
                    ////    EC0002=-2,11,0,14
                    this.StationAxisTeachingPositions.StreamWriter.WriteLine(this.FormatStationAxisTeachingPositionData(point, operationNode));

                    //// Example of station (rotary) axis teaching position move:
                    ////    +MOVJ EC0002
                    this.Moves.Write(" " + "+REFP" + " " + "EC" + (this.NumberOfPositionDataItems[2] - 1).TeachingPositionNumberFormatted());
                }
            }

            this.Moves.WriteLine();
        }

        /// <summary>
        ///     Compares the format of the POSTYPE.
        /// </summary>
        /// <param name="requestedPosType">The requested POSTYPE.</param>
        /// <param name="currentPostType">The current POSTYPE.</param>
        /// <returns>The formatted POSTYPE.</returns>
        internal virtual string CompareFormatPoseType(MotomanPostype requestedPosType, MotomanPostype currentPostType)
        {
            if (requestedPosType != currentPostType)
            {
                switch (requestedPosType)
                {
                    case MotomanPostype.Robot:
                        return "///POSTYPE ROBOT" + "\r\n" + "///ROBOT" + "\r\n";
                    case MotomanPostype.Angle:
                        return "///POSTYPE ANGLE" + "\r\n" + "///ANGLE" + "\r\n";
                    case MotomanPostype.Pulse:
                        return "///POSTYPE PULSE" + "\r\n" + "///PULSE" + "\r\n";
                    case MotomanPostype.Base:
                        return "///POSTYPE BASE" + "\r\n" + "///BASE" + "\r\n";
                    case MotomanPostype.User:
                        return "///POSTYPE USER" + "\r\n" + "///RECTAN" + "\r\n";
                    case MotomanPostype.Undefined:
                    default:
                        this.NotifyUser("ERROR: The position data type is invalid.");
                        Logger.Info("ERROR: The position data type is invalid.");
                        throw new ArgumentOutOfRangeException(nameof(requestedPosType), requestedPosType, null);
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///     Output external axes - base (BP) and station (EX) - as position variable.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputExternalAxesAsPositionVariable(PathNode point, CbtNode operationNode)
        {
            // External axes
            if (this.ExternalAxesJoints.Any())
            {
                // Filter for base (rail) axes
                if (this.Rails.Any())
                {
                    //// Example of base (rail) axis position variable data:
                    ////    BP0002=0,0,0
                    this.BaseAxisPositionVariables.StreamWriter.WriteLine(this.FormatBaseAxisPositionVariableData(point, operationNode));

                    //// Example of base (rail) axis position variable move:
                    ////    BP0002
                    this.Moves.Write(this.FormatBaseAxisPositionVariableMove(point, operationNode));
                }

                // Filter for station (rotary) axes
                if (this.Rotaries.Any())
                {
                    //// Example of station (rotary) axis position variable data:
                    ////    EX0002=-2,11,0,14
                    this.StationAxisPositionVariables.StreamWriter.WriteLine(this.FormatStationAxisPositionVariableData(point, operationNode));

                    //// Example of station (rotary) axis position variable move:
                    ////    +MOVJ EX0002
                    this.Moves.Write(this.FormatStationAxisPositionVariableMove(point, operationNode));
                }
            }
        }

        /// <summary>
        ///     Output base external axis (BC) as teaching position.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputBaseAxisAsTeachingPosition(PathNode point, CbtNode operationNode)
        {
            // Filter for base (rail) axes
            if (this.ExternalAxesJoints.Any() && this.Rails.Any())
            {
                //// Example of base (rail) axis teaching position data:
                ////    BC0002=0,0,0
                this.BaseAxisTeachingPositions.StreamWriter.WriteLine(this.FormatBaseAxisTeachingPositionData(point, operationNode));

                //// Example of base (rail) axis teaching position move:
                ////    BC0002
                this.Moves.Write(this.FormatBaseAxisTeachingPositionMove(point, operationNode));
            }
        }

        /// <summary>
        ///     Output station external axes (EC) as teaching position.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputStationAxesAsTeachingPosition(PathNode point, CbtNode operationNode)
        {
            // Filter for station (rotary) axes
            if (this.ExternalAxesJoints.Any() && this.Rotaries.Any())
            {
                //// Example of station (rotary) axis teaching position data:
                ////    EC0002=-2,11,0,14
                this.StationAxisTeachingPositions.StreamWriter.WriteLine(this.FormatStationAxisTeachingPositionData(point, operationNode));

                //// Example of station (rotary) axis teaching position move:
                ////    +MOVJ EC0002
                this.Moves.Write(this.FormatStationAxisTeachingPositionMove(point, operationNode));
            }
        }

        /// <summary>
        ///     Formats the joint space values at a point.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <returns>Formatted joint space values at a point.</returns>
        internal virtual string FormatJointSpaceValues(PathNode point)
        {
            return (MotomanProcessorSwitches.GetJ1ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]) * MotomanProcessorSwitches.GetJ1DegToPulseFactor(this.CellSettingsNode))).PulseFormatted() + "," +
                   (MotomanProcessorSwitches.GetJ2ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]) * MotomanProcessorSwitches.GetJ2DegToPulseFactor(this.CellSettingsNode))).PulseFormatted() + "," +
                   (MotomanProcessorSwitches.GetJ3ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]) * MotomanProcessorSwitches.GetJ3DegToPulseFactor(this.CellSettingsNode))).PulseFormatted() + "," +
                   (MotomanProcessorSwitches.GetJ4ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]) * MotomanProcessorSwitches.GetJ4DegToPulseFactor(this.CellSettingsNode))).PulseFormatted() + "," +
                   (MotomanProcessorSwitches.GetJ5ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]) * MotomanProcessorSwitches.GetJ5DegToPulseFactor(this.CellSettingsNode))).PulseFormatted() + "," +
                   (MotomanProcessorSwitches.GetJ6ZeroPulseValue(this.CellSettingsNode) + (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]) * MotomanProcessorSwitches.GetJ6DegToPulseFactor(this.CellSettingsNode))).PulseFormatted();
        }

        /// <summary>
        ///     Calculates and outputs the configuration bits.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Motoman formatted configuration bits.</returns>
        internal virtual string StatusBits(PathNode point)
        {
            int frSwitch = this.CachedRobotConfig.BaseConfiguration == BaseConfig.Back ? 1 : 0;
            int ulSwitch = this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Down ? 1 : 0;
            int nfSwitch;

            if (MotomanProcessorSwitches.GetControllerType(this.CellSettingsNode) == 2)
            {
                // Logic for Motoman DX controllers.
                nfSwitch = this.CachedRobotConfig.WristConfiguration == WristConfig.Negative ? 1 : 0;
            }
            else
            {
                // Logic for Motoman ERC/MRC/XRC/NX controllers.
                double j4Value = point.JointValue(this.JointFormat["J4"]);
                nfSwitch = ((j4Value > 90 && j4Value <= 270) || (j4Value > -270 && j4Value <= -90)) ? 1 : 0;
            }

            return nfSwitch + "," + ulSwitch + "," + frSwitch;
        }

        /// <summary>
        ///     Calculates and formats the configuration bits.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Motoman formatted configuration bits.</returns>
        internal virtual string TurnBits(PathNode point)
        {
            int saxRange = ((point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]) > -180) && (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]) <= 180)) ? 0 : 1;
            int raxRange = ((point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]) > -180) && (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]) <= 180)) ? 0 : 1;
            int taxRange = ((point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]) > -180) && (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]) <= 180)) ? 0 : 1;
            return raxRange + "," + taxRange + "," + saxRange;
        }

        /// <summary>
        ///     Formats position output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Motoman formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return pointPositionInUserFrame.X.Formatted() + "," + pointPositionInUserFrame.Y.Formatted() + "," + pointPositionInUserFrame.Z.Formatted();
        }

        /// <summary>
        ///     Formats orientation output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Motoman formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // Gets the Euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return euler[0].Formatted() + "," + euler[1].Formatted() + "," + euler[2].Formatted();
        }

        /// <summary>
        ///     Formats the interpolation type.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted interpolation type.</returns>
        /// <remarks>
        ///     Interpolation type determines the path along which the robot moves between playback steps.
        ///     Playback is the act of executing a taught job.
        ///     If the interpolation type is omitted during teaching, the data used from the previously taught step is automatically used.
        /// </remarks>
        internal virtual string FormatInterpolationType(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return "MOVJ";
                case MoveType.Linear:
                    return this.Rotaries.Any() ? "SMOVL" : "MOVL";
                case MoveType.Circular:
                    return this.Rotaries.Any() ? "SMOVC" : "MOVC";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the robot axis teaching position data.
        /// <para>
        ///     Pulse data : Cxxxxx = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : Cxxxxx = &lt;X&gt;,&lt;Y&gt;,&lt;Z&gt;,&lt;Rx&gt;,&lt;Ry&gt;,&lt;Rz&gt;,&lt;Re&gt;.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted robot axis teaching position data.</returns>
        internal virtual string FormatRobotAxisTeachingPositionData(PathNode point, CbtNode operationNode)
        {
            return "C" + (++this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted() + "=" +
                   (MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 && point.PathSetting().MotionType != DeviceMotionType.JointSpaceMove ?
                       this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode) : this.FormatJointSpaceValues(point));
        }

        /// <summary>
        ///     Formats the external base (rail) axis teaching position data.
        /// <para>
        ///     Pulse data : BCxxxxx = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : BCxxxxx = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external base (rail) axis teaching position data.</returns>
        internal virtual string FormatBaseAxisTeachingPositionData(PathNode point, CbtNode operationNode)
        {
            var bAxisData = new string[3];

            bAxisData[0] = this.BaseAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[0].Value) * MotomanProcessorSwitches.GetBase1DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase1ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[0].Value).Formatted();
            bAxisData[1] = this.BaseAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[1].Value) * MotomanProcessorSwitches.GetBase2DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase2ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[1].Value).Formatted();
            bAxisData[2] = this.BaseAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[2].Value) * MotomanProcessorSwitches.GetBase3DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase3ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[2].Value).Formatted();

            return "BC" + (++this.NumberOfPositionDataItems[1] - 1).TeachingPositionNumberFormatted() + "=" + string.Join(",", bAxisData.Where(s => !string.IsNullOrEmpty(s)));
        }

        /// <summary>
        ///     Formats the external station (rotary) axis teaching position data.
        /// <para>
        ///     Pulse data : ECxxxxx = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;,&lt;4&gt;,&lt;5&gt;,&lt;6&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : N/A.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external (station) axis teaching position data.</returns>
        internal virtual string FormatStationAxisTeachingPositionData(PathNode point, CbtNode operationNode)
        {
            var sAxisData = new string[4];

            if (MotomanProcessorSwitches.GetStationOutputFormat(this.CellSettingsNode) == 0)
            {
                // Angle format
                sAxisData[0] = this.StationAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[0].Value).Formatted();
                sAxisData[1] = this.StationAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[1].Value).Formatted();
                sAxisData[2] = this.StationAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[2].Value).Formatted();
                sAxisData[3] = this.StationAxes[3].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[3].Value).Formatted();
            }
            else
            {
                // Pulse format
                sAxisData[0] = this.StationAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[0].Value) * MotomanProcessorSwitches.GetStation1DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation1ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[1] = this.StationAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[1].Value) * MotomanProcessorSwitches.GetStation2DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation2ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[2] = this.StationAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[2].Value) * MotomanProcessorSwitches.GetStation3DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation3ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[3] = this.StationAxes[3].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[3].Value) * MotomanProcessorSwitches.GetStation4DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation4ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
            }

            return "EC" + (++this.NumberOfPositionDataItems[2] - 1).TeachingPositionNumberFormatted() + "=" + string.Join(",", sAxisData.Where(s => !string.IsNullOrEmpty(s)));
        }

        /// <summary>
        ///     Formats the robot axis position variable data.
        /// <para>
        ///     Pulse data : Pyyyy = &lt;S&gt;,&lt;L&gt;,&lt;U&gt;,&lt;R&gt;,&lt;B&gt;,&lt;T&gt;,&lt;E&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : Pyyyy = &lt;X&gt;,&lt;Y&gt;,&lt;Z&gt;,&lt;Rx&gt;,&lt;Ry&gt;,&lt;Rz&gt;,&lt;Re&gt;.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted robot axis position variable data.</returns>
        internal virtual string FormatRobotAxisPositionVariableData(PathNode point, CbtNode operationNode)
        {
            return "P" + (++this.NumberOfPositionDataItems[3] - 1).PositionVariableNumberFormatted() + "=" +
                ((MotomanProcessorSwitches.GetRobotOutputFormat(this.CellSettingsNode) == 0 && point.PathSetting().MotionType != DeviceMotionType.JointSpaceMove) ?
                this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode) : this.FormatJointSpaceValues(point));
        }

        /// <summary>
        ///     Formats the external base (rail) axis position variable data.
        /// <para>
        ///     Pulse data : BPyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : BPyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external base (rail) axis position variable data.</returns>
        internal virtual string FormatBaseAxisPositionVariableData(PathNode point, CbtNode operationNode)
        {
            var bAxisData = new string[3];

            bAxisData[0] = this.BaseAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[0].Value) * MotomanProcessorSwitches.GetBase1DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase1ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[0].Value).Formatted();
            bAxisData[1] = this.BaseAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[1].Value) * MotomanProcessorSwitches.GetBase2DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase2ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[1].Value).Formatted();
            bAxisData[2] = this.BaseAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : MotomanProcessorSwitches.GetBaseOutputFormat(this.CellSettingsNode) == 1 ? ((point.JointValue(this.BaseAxes[2].Value) * MotomanProcessorSwitches.GetBase3DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetBase3ZeroPulseValue(this.CellSettingsNode)).PulseFormatted() : point.JointValue(this.BaseAxes[2].Value).Formatted();

            return "BP" + (++this.NumberOfPositionDataItems[4] - 1).PositionVariableNumberFormatted() + "=" + string.Join(",", bAxisData.Where(s => !string.IsNullOrEmpty(s)));
        }

        /// <summary>
        ///     Formats the external station (rotary) axis position variable data.
        /// <para>
        ///     Pulse data : EXyyyy = &lt;1&gt;,&lt;2&gt;,&lt;3&gt;,&lt;4&gt;,&lt;5&gt;,&lt;6&gt;.
        /// </para>
        /// <para>
        ///     Cartesian data : N/A.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external (station) axis position variable data.</returns>
        internal virtual string FormatStationAxisPositionVariableData(PathNode point, CbtNode operationNode)
        {
            var sAxisData = new string[4];

            if (MotomanProcessorSwitches.GetStationOutputFormat(this.CellSettingsNode) == 0)
            {
                // Angle format
                sAxisData[0] = this.StationAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[0].Value).Formatted();
                sAxisData[1] = this.StationAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[1].Value).Formatted();
                sAxisData[2] = this.StationAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[2].Value).Formatted();
                sAxisData[3] = this.StationAxes[3].Equals(default(KeyValuePair<string, int>)) ? string.Empty : point.JointValue(this.StationAxes[3].Value).Formatted();
            }
            else
            {
                // Pulse format
                sAxisData[0] = this.StationAxes[0].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[0].Value) * MotomanProcessorSwitches.GetStation1DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation1ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[1] = this.StationAxes[1].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[1].Value) * MotomanProcessorSwitches.GetStation2DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation2ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[2] = this.StationAxes[2].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[2].Value) * MotomanProcessorSwitches.GetStation3DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation3ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
                sAxisData[3] = this.StationAxes[3].Equals(default(KeyValuePair<string, int>)) ? string.Empty : ((point.JointValue(this.StationAxes[3].Value) * MotomanProcessorSwitches.GetStation4DegreeToPulseFactor(this.CellSettingsNode)) + MotomanProcessorSwitches.GetStation4ZeroPulseValue(this.CellSettingsNode)).PulseFormatted();
            }

            return "EX" + (++this.NumberOfPositionDataItems[5] - 1).PositionVariableNumberFormatted() + "=" + string.Join(",", sAxisData.Where(s => !string.IsNullOrEmpty(s)));
        }

        /// <summary>
        ///     Formats the robot axis teaching position move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted robot axis teaching position move.</returns>
        internal virtual string FormatRobotAxisTeachingPositionMove(PathNode point, CbtNode operationNode)
        {
            return this.FormatInterpolationType(point, operationNode) + " " + "C" + (this.NumberOfPositionDataItems[0] - 1).TeachingPositionNumberFormatted();
        }

        /// <summary>
        ///     Formats the external base (rail) axis teaching position move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external base (rail) axis teaching position move.</returns>
        internal virtual string FormatBaseAxisTeachingPositionMove(PathNode point, CbtNode operationNode)
        {
            return " " + "BC" + (this.NumberOfPositionDataItems[1] - 1).TeachingPositionNumberFormatted();
        }

        /// <summary>
        ///     Formats the external station (rotary) axis teaching position move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external (station) axis teaching position move.</returns>
        internal virtual string FormatStationAxisTeachingPositionMove(PathNode point, CbtNode operationNode)
        {
            return " " + "+MOVJ" + " " + "EC" + (this.NumberOfPositionDataItems[2] - 1).TeachingPositionNumberFormatted();
        }

        /// <summary>
        ///     Formats the robot axis position variable move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted robot axis position variable move.</returns>
        internal virtual string FormatRobotAxisPositionVariableMove(PathNode point, CbtNode operationNode)
        {
            string comment = string.Empty;

            // Do not output approach/retract point in program (home start/end position).
            if (this.CachedIsHomeOutput != null && !this.CachedIsHomeOutput.Value)
            {
                comment = "'";
            }

            return comment + this.FormatInterpolationType(point, operationNode) + " " + "P" + (this.NumberOfPositionDataItems[3] - 1).PositionVariableNumberFormatted() + (this.Rails.Any() ? string.Empty : this.FormatPlaySpeed(point, operationNode) + this.FormatPositionLevel(point, operationNode));
        }

        /// <summary>
        ///     Formats the external base (rail) axis position variable move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external base (rail) axis position variable move.</returns>
        internal virtual string FormatBaseAxisPositionVariableMove(PathNode point, CbtNode operationNode)
        {
            return " " + "BP" + (this.NumberOfPositionDataItems[4] - 1).PositionVariableNumberFormatted() + this.FormatPlaySpeed(point, operationNode) + this.FormatPositionLevel(point, operationNode);
        }

        /// <summary>
        ///     Formats the external station (rotary) axis position variable move.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted external (station) axis position variable move.</returns>
        internal virtual string FormatStationAxisPositionVariableMove(PathNode point, CbtNode operationNode)
        {
            return " " + "+MOVJ" + " " + "EX" + (this.NumberOfPositionDataItems[5] - 1).PositionVariableNumberFormatted();
        }

        /// <summary>
        ///     Formats the play speed depending on the interpolation type.
        /// <para>
        ///     VJ  :   Joint speed (0.01 to 100.00%).
        /// </para>
        /// <para>
        ///     V   :   Tool center point speed (0.1 to 1500.0 mm/s).
        /// </para>
        /// <para>
        ///     VR  :   Position angular speed (0.1 to 180.0 degrees/s).
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted play speed.</returns>
        /// <remarks>
        ///     Play speed is the rate at which the robot moves.
        ///     If the play speed setting is omitted during teaching, the data used from the previously taught step is automatically used.
        /// </remarks>
        internal virtual string FormatPlaySpeed(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return " " + "VJ=" + MotomanMotionSettings.GetJointSpeed(operationNode).FeedFormatted();
                case MoveType.Linear:
                case MoveType.Circular:
                    // It is not possible to have both V and VR defined in the command.
                    return " " + (MotomanMotionSettings.GetIsPositionAngularSpeedEnabled(operationNode)
                               ? "VR=" + MotomanMotionSettings.GetPositionAngularSpeed(operationNode).FeedFormatted()
                               : "V=" + point.Feedrate().LinearFeedrate.FeedFormatted());
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the position level depending on the interpolation type.
        /// <para>
        ///     PL  :   Position level (0 to 8).
        /// </para>
        /// <para>
        ///     0: Complete positioning to the target point.
        /// </para>
        /// <para>
        ///     1 (fine) to 8 (rough): Inward turning operation.
        /// </para>
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted position level.</returns>
        /// <remarks>
        ///     The position level is the degree of approximation of the manipulator to a taught position.
        ///     If the position level is not set, the precision depends on the operation speed.
        /// </remarks>
        internal virtual string FormatPositionLevel(PathNode point, CbtNode operationNode)
        {
            if (point.MoveType() == MoveType.Rapid && MotomanMotionSettings.GetIsJointPositionLevelEnabled(operationNode))
            {
                return " " + "PL=" + MotomanMotionSettings.GetJointPositionLevel(operationNode);
            }

            if (point.MoveType() == MoveType.Linear && MotomanMotionSettings.GetIsLinearPositionLevelEnabled(operationNode))
            {
                return " " + "PL=" + MotomanMotionSettings.GetLinearPositionLevelValue(operationNode);
            }

            if (point.MoveType() == MoveType.Circular && MotomanMotionSettings.GetIsCircularPositionLevelEnabled(operationNode))
            {
                return " " + "PL=" + MotomanMotionSettings.GetCircularPositionLevelValue(operationNode);
            }

            return string.Empty;
        }

        /// <summary>
        ///     Resets <see cref="NumberOfPositionDataItems"/> array.
        /// </summary>
        internal virtual void ResetNumberOfPositionDataItems()
        {
            // The teaching positions are reset by default to 0.
            this.NumberOfPositionDataItems[0] = 0;
            this.NumberOfPositionDataItems[1] = 0;
            this.NumberOfPositionDataItems[2] = 0;

            // The position variables are reset by default to the starting location set in the post-switches.
            this.NumberOfPositionDataItems[3] = MotomanProcessorSwitches.GetRobotAxisPositionVariableStartingLocation(this.CellSettingsNode);
            this.NumberOfPositionDataItems[4] = MotomanProcessorSwitches.GetBaseAxisPositionVariableStartingLocation(this.CellSettingsNode);
            this.NumberOfPositionDataItems[5] = MotomanProcessorSwitches.GetExternalAxisPositionVariableStartingLocation(this.CellSettingsNode);
        }

        /// <inheritdoc/>
        internal override bool IsProgramInputValid()
        {
            // Verifies if the program is using multiple user frames.
            if (this.IsMultiUserFrameProgram())
            {
                this.NotifyUser("ERROR: Multi-user frame in a single job not supported, please contact your Robotmaster reseller. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the device has the "groupNumber" defined in the ROBX file.
            if (!this.IsGroupNumberValid())
            {
                this.NotifyUser("ERROR: The \"groupNumber\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the device has the "label" for the base (rail) axis properly defined in the ROBX file.
            if (!this.AllBaseAxesLabelsValid())
            {
                this.NotifyUser("ERROR: The base (rail) axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the device has the "label" for the station (rotary) axes properly defined in the ROBX file.
            if (!this.AllStationAxesLabelsValid())
            {
                this.NotifyUser("ERROR: The station (rotary) axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the reference frame used in the program is valid.
            if (!this.IsReferenceFrameValid())
            {
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the base (rail) axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the base (rail) axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool AllBaseAxesLabelsValid()
        {
            const string mechanismRotaryNumberPattern = "^BS+([1-3]_)E[1-3]$";
            return this.Rails.All(x => Regex.IsMatch(x.Name, mechanismRotaryNumberPattern));
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the station (rotary) axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the station (rotary) axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool AllStationAxesLabelsValid()
        {
            const string mechanismRotaryNumberPattern = "^ST+([1-4]_)R[1-4]$";
            return this.Rotaries.All(x => Regex.IsMatch(x.Name, mechanismRotaryNumberPattern));
        }

        /// <summary>
        ///     Gets a value indicating whether the "groupNumber" tag(s) found in the ROBX are valid.
        /// </summary>
        /// <returns><c>true</c> if the "groupNumber" tag(s) are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsGroupNumberValid()
        {
            // All manipulators: robots + rotaries + rails
            IEnumerable<CbtNode> allManipulatorNodes = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode));

            // TODO Re-factor the return statement below so that we do not expose the Manipulator class once AT-1139 is DONE (Blocked by)
            return allManipulatorNodes.Select(node => node.GetComponents<Manipulator>()
                                                          .Single()
                                                          .MainJoints[0]?.GroupNumber)
                                      .All(groupNumber => !string.IsNullOrEmpty(groupNumber) && groupNumber.All(char.IsDigit));
        }

        /// <summary>
        ///     Gets a value indicating whether the program has multiple user frames.
        /// </summary>
        /// <returns><c>true</c> if the program has multiple user frames; otherwise, <c>false</c>.</returns>
        internal virtual bool IsMultiUserFrameProgram()
        {
            // Gets all the (assigned and unassigned) operations nodes
            IEnumerable<CbtNode> operationNodes = ProgramManager.GetOperationNodes(this.ProgramNode);
            CbtNode firstOperationNode = operationNodes.First();
            return operationNodes.Any(op => OperationManager.GetUserFrame(op).Number != OperationManager.GetUserFrame(firstOperationNode).Number);
        }

        /// <summary>
        ///     Gets a value indicating whether the reference frame used in the program is valid.
        /// </summary>
        /// <returns><c>true</c> if the program uses a valid reference frame; otherwise, <c>false</c>.</returns>
        internal virtual bool IsReferenceFrameValid()
        {
            // Gets all the (assigned and unassigned) operations nodes
            var operationNodes = new List<CbtNode>(ApplicationManager.GetAllOperations(this.OperationCbtRoot));

            // If programming with respect to the base (world frame) or robot frame (robot root frame)
            if (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) != 2)
            {
                var popupMessageBaseFrame = false;
                var popupMessageRobotFrame = false;
                var warningMessageBaseFrame = false;
                var warningMessageRobotFrame = false;

                foreach (CbtNode op in operationNodes)
                {
                    // Get user frame matrix represented in W.C.S
                    Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(OperationManager.GetUserFrame(op), this.SceneCbtRoot, this.OperationCbtRoot, null);

                    // Base frame (world frame)
                    if (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 0)
                    {
                        // Get W.C.S matrix
                        Matrix4X4 worldFrameMatrix = FrameManager.GetWorldMatrix(this.SceneCbtRoot);

                        if (worldFrameMatrix != userFrameMatrix)
                        {
                            popupMessageBaseFrame = true;
                        }
                        else
                        {
                            warningMessageBaseFrame = true;
                        }
                    }

                    // Robot frame (root frame)
                    if (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 1)
                    {
                        // Get robot frame
                        CbtNode robotFrameNode = this.SceneCbtRoot.GetChildren(SceneKeys.Robot).First();

                        // TODO: Does not support robot mounted on a rail
                        if (this.Rails.Any())
                        {
                            this.NotifyUser("ERROR: Some operations are output with respect to the robot frame (robot root) while the robot is mounted on a rail. We do not support this configuration at the moment.", true, false);
                            return false;
                        }

                        // TODO: Does not support multi-robot
                        if (this.SceneCbtRoot.GetChildren(SceneKeys.Robot).Single() == null)
                        {
                            this.NotifyUser("ERROR: Some operations are output with respect to the robot frame (robot root) with a multi-robot program. We do not support this configuration at the moment.", true, false);
                            return false;
                        }

                        // Get robot frame (root frame) matrix represented in W.C.S
                        Matrix4X4 transformationMatrixFromWorldToBase = FrameManager.GetTransformationMatrix(this.SceneCbtRoot, robotFrameNode);

                        if (transformationMatrixFromWorldToBase != userFrameMatrix)
                        {
                            popupMessageRobotFrame = true;
                        }
                        else
                        {
                            warningMessageRobotFrame = true;
                        }
                    }
                }

                if (MotomanProcessorSwitches.GetReferenceFrameOfRobot(this.CellSettingsNode) == 0)
                {
                    if (popupMessageBaseFrame)
                    {
                        Logger.Info("WARNING: Some operations are output with respect to a base frame (W.C.S) that is not equivalent to the default one defined in Robotmaster.");
                        if (MessageWindow.Show("WARNING: Some operations are output with respect to a base frame (W.C.S) that is not equivalent to the default one defined in Robotmaster.", "Post anyway") == MessageWindowResult.Cancel)
                        {
                            return false;
                        }
                    }

                    if (warningMessageBaseFrame)
                    {
                        this.NotifyUser("WARNING: Some operations are output with respect to the base (W.C.S).", true);
                    }
                }
                else
                {
                    if (popupMessageRobotFrame)
                    {
                        Logger.Info("WARNING: Some operations are output with respect to a robot frame (robot root) that is not equivalent to the default one defined in Robotmaster.");
                        if (MessageWindow.Show("WARNING: Some operations are output with respect to a robot frame (robot root) that is not equivalent to the default one defined in Robotmaster.", "Post anyway") == MessageWindowResult.Cancel)
                        {
                            return false;
                        }
                    }

                    if (warningMessageRobotFrame)
                    {
                        this.NotifyUser("WARNING: Some operations are output with respect to the robot frame (robot root).", true);
                    }
                }
            }

            return true;
        }
    }
}
