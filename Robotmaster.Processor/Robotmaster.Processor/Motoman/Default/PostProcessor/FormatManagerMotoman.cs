﻿// <copyright file="FormatManagerMotoman.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.PostProcessor
{
    /// <summary>
    ///     Motoman Format Manager.
    /// </summary>
    public static class FormatManagerMotoman
    {
        /// <summary>
        ///     Gets the tolerance used for comparison of floating point numbers.
        /// </summary>
        public const double Tolerance = 10E-6;

        private const string FeedFormat = "{0:0.0}";

        private const string GenericFormat = "{0:0.000}";

        private const string TeachingPositionNumberFormat = "{0:00000.#}";

        private const string PositionVariableNumberFormat = "{0:0000.#}";

        private const string PulseFormat = "{0:0}";

        /// <summary>
        ///     Gets main program extension.
        /// </summary>
        public static string MainProgramExtension => ".JBI";

        /// <summary>
        ///     Adds the program extension.
        /// </summary>
        /// <param name="value">Any object.</param>
        /// <returns>String with program extension.</returns>
        public static string WithProgramExtension(this int value)
        {
            return value + MainProgramExtension;
        }

        /// <summary>
        ///     Formats the teaching position (&lt;Cxxxxx&gt;,&lt;BCxxxx&gt;,&lt;ECxxxx&gt;) point number with leading zeros.
        /// </summary>
        /// <param name="value">Any string.</param>
        /// <returns>String with leading zeros.</returns>
        public static string TeachingPositionNumberFormatted(this int value)
        {
            return string.Format(TeachingPositionNumberFormat, value);
        }

        /// <summary>
        ///     Formats the position variable (&lt;Pyyyy&gt;,&lt;BPyyyy&gt;,&lt;EXyyyy&gt;) point number with leading zeros.
        /// </summary>
        /// <param name="value">Any string.</param>
        /// <returns>String with leading zeros.</returns>
        public static string PositionVariableNumberFormatted(this int value)
        {
            return string.Format(PositionVariableNumberFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this int value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this double value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this float value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Pulse value.</returns>
        public static string PulseFormatted(this int value)
        {
            return string.Format(PulseFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Pulse value.</returns>
        public static string PulseFormatted(this double value)
        {
            return string.Format(PulseFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Pulse value.</returns>
        public static string PulseFormatted(this float value)
        {
            return string.Format(PulseFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this int value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this double value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this float value)
        {
            return string.Format(FeedFormat, value);
        }
    }
}