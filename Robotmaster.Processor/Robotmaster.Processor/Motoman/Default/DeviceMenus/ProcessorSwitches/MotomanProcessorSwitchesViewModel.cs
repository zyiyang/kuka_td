// <copyright file="MotomanProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     MotomanProcessorSwitchesViewModel View Model.
    /// </summary>
    public class MotomanProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;
        private static bool expandable4;
        private static bool expandable5;

        /// <summary>
        ///     Gets the controller type.
        /// </summary>
        public static Dictionary<int, string> ControllerTypeSource => new Dictionary<int, string>
        {
            { 0, "ERC/MRC/XRC Series" },
            { 1, "NX Series" },
            { 2, "DX Series" },
        };

        /// <summary>
        ///     Gets the robot output format.
        /// </summary>
        public static Dictionary<int, string> RobotOutputFormatSource => new Dictionary<int, string>
        {
            { 0, "Cartesian output" },
            { 1, "Pulse output" },
        };

        /// <summary>
        ///     Gets the base (rail) output format.
        /// </summary>
        public static Dictionary<int, string> BaseOutputFormatSource => new Dictionary<int, string>
        {
            { 0, "Cartesian output" },
            { 1, "Pulse output" },
        };

        /// <summary>
        ///     Gets the station (rotary) output format.
        /// </summary>
        public static Dictionary<int, string> StationOutputFormatSource => new Dictionary<int, string>
        {
            { 0, "Angle output" },
            { 1, "Pulse output" },
        };

        /// <summary>
        ///     Gets the robot reference frame.
        /// </summary>
        public static Dictionary<int, string> ReferenceFrameOfRobotSource => new Dictionary<int, string>
        {
            { 0, "BASE frame (W.C.S)" },
            { 1, "ROBOT frame" },
            { 2, "USER frame" },
        };

        /// <summary>
        ///     Gets the base (rail) reference frame.
        /// </summary>
        public static Dictionary<int, string> ReferenceFrameOfBaseSource => new Dictionary<int, string>
        {
            { 0, "BASE frame (W.C.S)" },
            { 1, "ROBOT frame" },
            { 2, "USER frame" },
        };

        /// <summary>
        ///     Gets the jJ move variable.
        /// </summary>
        public static Dictionary<int, string> JointSpaceMoveOutputVariablesSource => new Dictionary<int, string>
        {
            { 0, "Use P-variables" },
            { 1, "Use C-variables" },
        };

        /// <summary>
        ///     Gets the job file permission.
        /// </summary>
        public static Dictionary<int, string> JobFilePermissionAttributeSource => new Dictionary<int, string>
        {
            { 0, "Read only" },
            { 1, "Write only" },
            { 2, "Read/Write" },
        };

        /// <summary>
        ///     Gets or sets the Job comment.
        ///     Robotic syntax: ///COMM |COMMENT CHARACTER LINE|.
        /// </summary>
        public string JobComment
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.JobComment)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.JobComment)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum number of points allowed per individual code file. If 0, then there is no maximum.
        /// </summary>
        public int MaximumNumberOfPointsPerFile
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumNumberOfPointsPerFile)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the offset used to locate the starting data register of the robot axis position variables.
        /// </summary>
        public int RobotAxisPositionVariableStartingLocation
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.RobotAxisPositionVariableStartingLocation)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.RobotAxisPositionVariableStartingLocation)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the offset used to locate the starting data register of the base (rail) axis position variables.
        /// </summary>
        public int BaseAxisPositionVariableStartingLocation
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.BaseAxisPositionVariableStartingLocation)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.BaseAxisPositionVariableStartingLocation)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the offset used to locate the starting data register of the station (rotary) axis position variables.
        /// </summary>
        public int ExternalAxisPositionVariableStartingLocation
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ExternalAxisPositionVariableStartingLocation)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ExternalAxisPositionVariableStartingLocation)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 1 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J1DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J1DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J1DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 1 zero pulse value. This is the angular value of joint 1 when the pulse of joint 1 is at zero.
        /// </summary>
        public double J1ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J1ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J1ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 2 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J2DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J2DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J2DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 2 zero pulse value. This is the angular value of joint 2 when the pulse of joint 2 is at zero.
        /// </summary>
        public double J2ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J2ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J2ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 3 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J3DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J3DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J3DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 3 zero pulse value. This is the angular value of joint 3 when the pulse of joint 3 is at zero.
        /// </summary>
        public double J3ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J3ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J3ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 4 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J4DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J4DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J4DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 4 zero pulse value. This is the angular value of joint 4 when the pulse of joint 4 is at zero.
        /// </summary>
        public double J4ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J4ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J4ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 5 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J5DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J5DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J5DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 5 zero pulse value. This is the angular value of joint 5 when the pulse of joint 5 is at zero.
        /// </summary>
        public double J5ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J5ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J5ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 6 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double J6DegToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J6DegToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J6DegToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint 6 zero pulse value. This is the angular value of joint 6 when the pulse of joint 6 is at zero.
        /// </summary>
        public double J6ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J6ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.J6ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 1 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double Station1DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station1DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station1DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 1 zero pulse value. This is the angular value of station 1 when the pulse of station 1 is at zero.
        /// </summary>
        public double Station1ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station1ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station1ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 2 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double Station2DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station2DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station2DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 2 zero pulse value. This is the angular value of station 2 when the pulse of station 2 is at zero.
        /// </summary>
        public double Station2ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station2ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station2ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 3 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double Station3DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station3DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station3DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 3 zero pulse value. This is the angular value of station 3 when the pulse of station 3 is at zero.
        /// </summary>
        public double Station3ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station3ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station3ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 4 degree to pulse conversion factor.
        ///     This is the ratio of the angular change between two points to the pulse change between the same two points.
        /// </summary>
        public double Station4DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station4DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station4DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) 4 zero pulse value. This is the angular value of station 4 when the pulse of station 4 is at zero.
        /// </summary>
        public double Station4ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station4ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Station4ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 1 degree to pulse conversion factor.
        ///     This is the ratio of the linear distance change between two points to the pulse change between the same two points.
        /// </summary>
        public double Base1DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base1DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base1DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 1 zero pulse value. This is the linear distance of base 1 when the pulse of base 1 is at zero.
        /// </summary>
        public double Base1ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base1ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base1ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 2 degree to pulse conversion factor.
        ///     This is the ratio of the linear distance change between two points to the pulse change between the same two points.
        /// </summary>
        public double Base2DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base2DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base2DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 2 zero pulse value. This is the linear distance of base 2 when the pulse of base 2 is at zero.
        /// </summary>
        public double Base2ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base2ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base2ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 3 degree to pulse conversion factor.
        ///     This is the ratio of the linear distance change between two points to the pulse change between the same two points.
        /// </summary>
        public double Base3DegreeToPulseFactor
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base3DegreeToPulseFactor)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base3DegreeToPulseFactor)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) 3 zero pulse value. This is the linear distance of base 3 when the pulse of base 3 is at zero.
        /// </summary>
        public double Base3ZeroPulseValue
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base3ZeroPulseValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.Base3ZeroPulseValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the FPT command for circular move.
        ///     If unchecked, then it will use duplicated point (legacy technology).
        ///     For continuous circular arcs, two arcs must be separated from each other by a duplicated point.
        ///     If checked, then it will use the FPT command (new technology).
        ///     For continuous circular arcs, the FPT tag is used (FPT = Arc end-point setting).
        /// </summary>
        public bool IsCircularMoveTechnologyUsingFpt
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularMoveTechnologyUsingFpt)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularMoveTechnologyUsingFpt)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the job destroy attribute.
        ///     Robotic syntax: JD.
        /// </summary>
        public bool IsJobDestroyAttributeEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJobDestroyAttributeEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJobDestroyAttributeEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the directory destroy attribute.
        ///     Robotic syntax: DD.
        /// </summary>
        public bool IsDirectoryDestroyAttributeEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsDirectoryDestroyAttributeEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsDirectoryDestroyAttributeEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the save complete attribute.
        ///     Robotic syntax: SC.
        /// </summary>
        public bool IsSaveCompleteAttributeEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsSaveCompleteAttributeEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsSaveCompleteAttributeEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the controller type.
        ///     The robotic code can follow the format of the ERC/MRC/XRC, NX or DX series.
        /// </summary>
        public int ControllerType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ControllerType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ControllerType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the robot output format.
        ///     The relative job: Cartesian output.
        ///     Outputs Cartesian values (x, y, z, rx, ry, rz).
        ///     The standard job: pulse output.
        ///     Outputs pulse values (j1, j2, j3, j4, j5, j6).
        /// </summary>
        public int RobotOutputFormat
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.RobotOutputFormat)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.RobotOutputFormat)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base (rail) output format.
        ///     The Cartesian output : 1-axis Cartesian data, 2-axis Cartesian data, 3-axis Cartesian data.
        ///     The pulse output : 1-axis pulse data, 2-axis pulse data, 3-axis pulse data.
        /// </summary>
        public int BaseOutputFormat
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.BaseOutputFormat)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.BaseOutputFormat)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the station (rotary) output format.
        ///     The angular output: 1-axis angular data, 2-axis angular data, 3-axis angular data.
        ///     The pulse output : 1-axis pulse data, 2-axis pulse data, 3-axis pulse data.
        /// </summary>
        public int StationOutputFormat
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.StationOutputFormat)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.StationOutputFormat)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the robot reference frame.
        ///     BASE frame is the base coordinate system in Cartesian space (sometimes referred to as world coordinate system).
        ///     ROBOT frame is the robot coordinate system in Cartesian space.
        ///     USER frame is the user coordinate system in Cartesian space.
        /// </summary>
        public int ReferenceFrameOfRobot
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ReferenceFrameOfRobot)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ReferenceFrameOfRobot)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets bASE frame is the base coordinate system in Cartesian space (sometimes referred to as world coordinate system).
        ///     ROBOT frame is the robot coordinate system in Cartesian space.
        ///     USER frame is the user coordinate system Cartesian space.
        /// </summary>
        public int ReferenceFrameOfBase
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ReferenceFrameOfBase)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ReferenceFrameOfBase)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the JJ move variable type.
        ///     If P-variables selected, then the maximum amount of JJ moves is 128.
        /// </summary>
        public int JointSpaceMoveOutputVariables
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.JointSpaceMoveOutputVariables)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.JointSpaceMoveOutputVariables)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the job file permission attribute.
        ///     Robotic syntax: RO.
        ///     Robotic syntax: WO.
        ///     Robotic syntax: RW.
        /// </summary>
        public int JobFilePermissionAttribute
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.JobFilePermissionAttribute)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.JobFilePermissionAttribute)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable4"/> is checked.
        /// </summary>
        public bool IsExpandable4Checked
        {
            get => expandable4;

            set
            {
                expandable4 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable5"/> is checked.
        /// </summary>
        public bool IsExpandable5Checked
        {
            get => expandable5;

            set
            {
                expandable5 = value;
                this.OnPropertyChanged();
            }
        }
    }
}