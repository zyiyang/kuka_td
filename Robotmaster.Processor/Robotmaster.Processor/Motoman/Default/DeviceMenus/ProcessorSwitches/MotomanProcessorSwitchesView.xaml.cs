// <copyright file="MotomanProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for MotomanProcessorSwitches.
    /// </summary>
    public partial class MotomanProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanProcessorSwitchesView"/> class.
        /// </summary>
        public MotomanProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="MotomanProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public MotomanProcessorSwitchesView(MotomanProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public MotomanProcessorSwitchesViewModel ViewModel { get; }
    }
}