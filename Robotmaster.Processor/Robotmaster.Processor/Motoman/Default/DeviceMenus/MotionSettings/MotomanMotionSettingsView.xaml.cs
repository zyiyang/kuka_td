// <copyright file="MotomanMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for MotomanMotionSettings.
    /// </summary>
    public partial class MotomanMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanMotionSettingsView"/> class.
        /// </summary>
        public MotomanMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="MotomanMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public MotomanMotionSettingsView(MotomanMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public MotomanMotionSettingsViewModel ViewModel { get; }
    }
}