// <copyright file="MotomanMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     MotomanMotionSettingsViewModel View Model.
    /// </summary>
    public class MotomanMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;
        private static bool expandable4;

        /// <summary>
        ///     Gets or sets the joint speed (%).
        ///     The joint speed is represented as a ratio of the maximum speed (0.01 to 100.00%).
        ///     Robotic syntax: VJ = Joint speed.
        /// </summary>
        public int JointSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the position level number for joint motion.
        ///     Position level number can be 0, 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: PL = Position level number.
        /// </summary>
        public int JointPositionLevel
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointPositionLevel)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointPositionLevel)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the position level number for linear motion.
        ///     Position level number can be 0, 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: PL = Position level number.
        /// </summary>
        public int LinearPositionLevelValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearPositionLevelValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearPositionLevelValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the position level number for circular motion.
        ///     Position level number can be 0, 1, 2, 3, 4, 5, 6, 7 or 8. The lower the number, the more accurate the motion.
        ///     Robotic syntax: PL = Position level number.
        /// </summary>
        public int CircularPositionLevelValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularPositionLevelValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularPositionLevelValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the position angular speed (deg/s).
        ///     The position angular speed represents the wrist orientation speed (0.1 to 180.0 degrees/s).
        ///     Robotic syntax: VR = Position angular speed.
        /// </summary>
        public int PositionAngularSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.PositionAngularSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.PositionAngularSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint position level.
        /// </summary>
        public bool IsJointPositionLevelEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointPositionLevelEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointPositionLevelEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the linear position level.
        /// </summary>
        public bool IsLinearPositionLevelEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearPositionLevelEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearPositionLevelEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the circular position level.
        /// </summary>
        public bool IsCircularPositionLevelEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularPositionLevelEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularPositionLevelEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the position angular speed.
        /// </summary>
        public bool IsPositionAngularSpeedEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsPositionAngularSpeedEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsPositionAngularSpeedEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable4"/> is checked.
        /// </summary>
        public bool IsExpandable4Checked
        {
            get => expandable4;

            set
            {
                expandable4 = value;
                this.OnPropertyChanged();
            }
        }
    }
}