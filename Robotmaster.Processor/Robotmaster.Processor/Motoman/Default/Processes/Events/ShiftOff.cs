﻿// <copyright file="ShiftOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Motoman.Welding.Processes.Events;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Shift off event.
    /// </summary>
    [DataContract(Name = "MotomanShiftOff")]
    public class ShiftOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShiftOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ShiftOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether shift is synchronized.
        /// </summary>
        [DataMember(Name = "IsSychronized")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the set command.
        /// </summary>
        [DataMember(Name = "ShiftOffCommand")]
        public virtual string ShiftOffCommand { get; set; } = "SFTOF";

        /// <summary>
        /// Gets or sets the Coordinate systems type.
        /// </summary>
        [DataMember(Name = "CoordinateSystemsType")]
        public virtual CoordinateSystemsType CoordinateSystemsType { get; set; } = CoordinateSystemsType.Undefined;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            string coordinateSystemsTypeOutput = this.CoordinateSystemsType == CoordinateSystemsType.Undefined ? string.Empty : $" {this.CoordinateSystemsType.ToRobotCodeString()}";
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.ShiftOffCommand}{coordinateSystemsTypeOutput}";
        }
    }
}
