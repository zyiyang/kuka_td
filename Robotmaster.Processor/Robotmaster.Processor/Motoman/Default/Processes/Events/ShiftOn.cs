﻿// <copyright file="ShiftOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Shift on event.
    /// </summary>
    [DataContract(Name = "MotomanShiftOn")]
    public class ShiftOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShiftOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ShiftOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets a value indicating whether shift is synchronized.
        /// </summary>
        [DataMember(Name = "IsSychronized")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual bool IsSynchronized { get; set; }

        /// <summary>
        /// Gets or sets the set command.
        /// </summary>
        [DataMember(Name = "ShiftOnCommand")]
        public virtual string ShiftOnCommand { get; set; } = "SFTON";

        /// <summary>
        /// Gets or sets the deviation data.
        /// </summary>
        [DataMember(Name = "ShiftVariable")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string ShiftVariable { get; set; } = "P0120";

        /// <summary>
        /// Gets or sets the coordinate type.
        /// </summary>
        [DataMember(Name = "CoordinateType")]
        public virtual CoordinateSystemsType CoordinateSystemsType { get; set; } = CoordinateSystemsType.Undefined;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            string coordinateSystemsTypeOutput = this.CoordinateSystemsType == CoordinateSystemsType.Undefined ? string.Empty : $" {this.CoordinateSystemsType.ToRobotCodeString()}";
            return $"{(this.IsSynchronized ? "S" : string.Empty)}{this.ShiftOnCommand} {this.ShiftVariable}{coordinateSystemsTypeOutput}";
        }
    }
}
