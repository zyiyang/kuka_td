﻿// <copyright file="CallJob.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Call Job event.
    /// </summary>
    [DataContract(Name = "MotomanCallJob")]
    public class CallJob : Event
    {
        private const string CallCommand = "CALL JOB:";

        /// <summary>
        /// Initializes a new instance of the <see cref="CallJob"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public CallJob()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        [DataMember(Name = "JobName")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string JobName { get; set; } = string.Empty;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{CallCommand}{this.JobName}";
        }
    }
}
