﻿// <copyright file="CoordinateSystemsTypeHelper.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    /// <summary>
    ///     Coordinate systems type helper.
    /// </summary>
    public static class CoordinateSystemsTypeHelper
    {
        /// <summary>
        ///     Convert the value of a <see cref="CoordinateSystemsType"/> to the corresponding robot code.
        /// </summary>
        /// <param name="coordinateSystemsType">The coordinate systems type.</param>
        /// <returns>The coordinate systems type robot code.</returns>
        public static string ToRobotCodeString(this CoordinateSystemsType coordinateSystemsType)
        {
            switch (coordinateSystemsType)
            {
                case CoordinateSystemsType.Undefined:
                    return string.Empty;

                case CoordinateSystemsType.BaseFrame:
                    return "BF";

                case CoordinateSystemsType.ToolFrame:
                    return "TF";

                case CoordinateSystemsType.UserFrame:
                    return "UF";

                case CoordinateSystemsType.TravelingAxis:
                    return "BP";

                case CoordinateSystemsType.ExternalAxis:
                    return "EX";
                default:
                    return string.Empty;
            }
        }
    }
}