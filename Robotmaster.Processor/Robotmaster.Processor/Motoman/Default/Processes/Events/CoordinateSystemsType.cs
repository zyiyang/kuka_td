﻿// <copyright file="CoordinateSystemsType.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;

    /// <summary>
    ///     Coordinate systems type.
    /// </summary>
    [DataContract(Name = "MotomanCoordinateSystemsType")]
    public enum CoordinateSystemsType
    {
        /// <summary>
        ///     Undefined.
        /// </summary>
        [EnumMember(Value = "Undefined")]
        Undefined,

        /// <summary>
        ///     Base Frame.
        /// </summary>
        [EnumMember(Value = "BaseFrame")]
        BaseFrame,

        /// <summary>
        ///     Robot Frame.
        /// </summary>
        [EnumMember(Value = "RobotFrame")]
        RobotFrame,

        /// <summary>
        ///     Tool Frame.
        /// </summary>
        [EnumMember(Value = "ToolFrame")]
        ToolFrame,

        /// <summary>
        ///     User Frame.
        /// </summary>
        [EnumMember(Value = "UserFrame")]
        UserFrame,

        /// <summary>
        ///     Traveling Axis.
        /// </summary>
        [EnumMember(Value = "TravelingAxis")]
        TravelingAxis,

        /// <summary>
        ///     External Axis.
        /// </summary>
        [EnumMember(Value = "ExternalAxis")]
        ExternalAxis,
    }
}
