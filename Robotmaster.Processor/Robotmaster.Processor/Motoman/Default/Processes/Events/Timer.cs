﻿// <copyright file="Timer.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Timer event.
    /// </summary>
    [DataContract(Name = "MotomanTimer")]
    public class Timer : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Timer"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public Timer()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the timer command.
        /// </summary>
        [DataMember(Name = "TimerCommand")]
        public virtual string TimerCommand { get; set; } = "TIMER T=";

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        [DataMember(Name = "Time")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual double Time { get; set; }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.TimerCommand} {this.Time}";
        }
    }
}
