﻿// <copyright file="Macro.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Motoman Macro1 event.
    /// </summary>
    [DataContract(Name = "MotomanMacro")]
    public class Macro : Event
    {
        private const string ArgumentPrefix = " ARGF";

        /// <summary>
        /// Initializes a new instance of the <see cref="Macro"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public Macro()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the macro name.
        /// </summary>
        [DataMember(Name = "MacroName")]
        public virtual string MacroName { get; set; } = "MacroName";

        /// <summary>
        /// Gets or sets the macro id.
        /// </summary>
        [DataMember(Name = "MacroId")]
        public virtual int MacroId { get; set; } = 1;

        /// <summary>
        /// Gets or sets the arguments command.
        /// </summary>
        [DataMember(Name = "Arguments")]
        public virtual string[] Arguments { get; set; } = new string[16];

        /// <summary>
        ///     Return a string representing the current event.
        ///     This string is displayed in point list.
        /// </summary>
        /// <returns>
        ///      A string representing the current event.
        /// </returns>
        public override string ToString()
        {
            return this.MacroName;
        }

        /// <summary>
        ///     Return a string representing the robot code corresponding to the current event.
        /// </summary>
        /// <returns>
        ///      A string representing the robot code corresponding to the current event.
        /// </returns>
        public virtual string ToMotomanCode()
        {
            IEnumerable<string> notEmptyArguments = this.Arguments.Where(x => !string.IsNullOrWhiteSpace(x));
            if (notEmptyArguments.Any())
            {
                return $"MACRO1 MJ#({this.MacroId}){ArgumentPrefix}{string.Join(ArgumentPrefix, notEmptyArguments)}";
            }

            return $"MACRO1 MJ#({this.MacroId})";
        }
    }
}
