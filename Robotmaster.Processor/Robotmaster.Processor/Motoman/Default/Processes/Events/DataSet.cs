﻿// <copyright file="DataSet.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <inheritdoc />
    /// <summary>
    ///    SET event.
    ///    Sets <see cref="DestinationData"/> to <see cref="SourceData"/>.
    /// </summary>
    [DataContract(Name = "MotomanDataSet")]
    public class DataSet : Event
    {
        private const string SetCommand = "SET";

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataSet"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public DataSet()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        ///     Gets or sets the result stored destination.
        /// </summary>
        [DataMember(Name = "DestinationData")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string DestinationData { get; set; } = "P1020";

        /// <summary>
        ///     Gets or sets the expression.
        /// </summary>
        [DataMember(Name = "SourceData")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string SourceData { get; set; } = "P0006";

        /// <summary>
        ///     Display format in point list.
        ///     <para>
        ///         Format: SET &lt;<see cref="DestinationData"/>&gt; &lt;<see cref="SourceData"/>&gt;.
        ///    </para>
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{SetCommand} {this.DestinationData} {this.SourceData}";
        }
    }
}
