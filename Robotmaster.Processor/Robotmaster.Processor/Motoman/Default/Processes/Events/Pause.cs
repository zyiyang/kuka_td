﻿// <copyright file="Pause.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Timer event.
    /// </summary>
    [DataContract(Name = "MotomanPause")]
    public class Pause : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pause"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public Pause()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "PAUSE";
        }
    }
}
