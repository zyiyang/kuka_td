﻿// <copyright file="Comment.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Comment event.
    /// </summary>
    [DataContract(Name = "MotomanComment")]
    public class Comment : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Comment"/> class.
        /// Default constructor is required for MET AddComment.
        /// </summary>
        public Comment()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.After | EventIndex.Before;
        }

        /// <summary>
        /// Gets or sets the comment text. Maximum 32 character.
        /// </summary>
        [DataMember(Name = "CommentText")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string CommentText { get; set; } = string.Empty;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            if (this.CommentText.Length > 32)
            {
                //// Maximum 32 character.
                return "'" + this.CommentText.Substring(0, 32);
            }

            return "'" + this.CommentText;
        }
    }
}
