﻿// <copyright file="MotomanDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Motoman.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Motoman.Default.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Motoman welding process.
    /// </summary>
    [DataContract(Name = "MotomanDefaultProcess")]
    public class MotomanDefaultProcess : PackageProcess, IMotomanProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MotomanDefaultProcess"/> class.
        /// </summary>
        public MotomanDefaultProcess()
        {
            this.Name = "Motoman Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.AddEventToMet(typeof(Timer));
            this.AddEventToMet(typeof(Comment));
            this.AddEventToMet(typeof(CallJob));
            this.AddEventToMet(typeof(Pause));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
