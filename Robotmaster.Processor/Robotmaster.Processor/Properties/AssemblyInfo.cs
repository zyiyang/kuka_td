// <copyright file="AssemblyInfo.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Robotmaster.Processor")]
[assembly: AssemblyDescription("Robotmaster Processor.")]
[assembly: AssemblyCompany("Hypertherm Robotic Software Inc.")]
[assembly: AssemblyCopyright("Copyright 2002-2019 Hypertherm Robotic Software Inc. All rights reserved.")]
[assembly: AssemblyTrademark("Robotmaster is a registered trademark licensed exclusively to Hypertherm Robotic Software Inc.")]
[assembly: AssemblyVersion("0.11.28.*")]
[assembly: InternalsVisibleTo("Robotmaster.Processor.Test,PublicKey=00240000048000009400000006020000002400005253413100"
                              + "04000001000100cf9a4935128751cd4ac21df1c7d075d2426e2981370880fde1116ed20561fd094e1d03d639aead80"
                              + "03a70c991728c59ddb5c6c7449d29a0d47a5d2c737e5c04d0a4f2c1edd75707d3b59a7f44c848074dcf52b1aa223fd"
                              + "3a1c6db08ec6e1e7bdb28659b001df45aac4f9fed3d2303bf0302482195004dc9f311482904a89abd5")]
[assembly: NeutralResourcesLanguage("en", UltimateResourceFallbackLocation.MainAssembly)]
