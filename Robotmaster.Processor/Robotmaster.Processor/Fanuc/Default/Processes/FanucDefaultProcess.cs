﻿// <copyright file="FanucDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Fanuc.Default.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Fanuc default process.
    /// </summary>
    [DataContract(Name = "FanucDefaultProcess")]
    public class FanucDefaultProcess : PackageProcess, IFanucProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucDefaultProcess"/> class.
        /// </summary>
        public FanucDefaultProcess()
        {
            this.Name = "Fanuc Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.AddEventToMet(typeof(Comment));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
