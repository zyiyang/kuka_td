// <copyright file="FanucMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for FanucMotionSettings.
    /// </summary>
    public partial class FanucMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucMotionSettingsView"/> class.
        /// </summary>
        public FanucMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="FanucMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public FanucMotionSettingsView(FanucMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public FanucMotionSettingsViewModel ViewModel { get; }
    }
}