// <copyright file="FanucMotionSettingsBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.MotionSettings
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing <see cref="FanucMotionSettingsView"/>.
    /// </summary>
    internal class FanucMotionSettingsBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the View Model.
        /// </summary>
        private FanucMotionSettingsViewModel ViewModel { get; set; }

        /// <summary>
        ///      Sets up the View Model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}