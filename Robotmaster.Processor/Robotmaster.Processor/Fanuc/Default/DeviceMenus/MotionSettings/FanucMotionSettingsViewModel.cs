// <copyright file="FanucMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     FanucMotionSettingsViewModel View Model.
    /// </summary>
    public class FanucMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;
        private static bool expandable4;

        /// <summary>
        ///     Gets the termination.
        /// </summary>
        public static Dictionary<int, string> JointMotionTerminationTypeSource => new Dictionary<int, string>
        {
            { 0, "Not specified" },
            { 1, "Fine" },
            { 2, "Continuous" },
        };

        /// <summary>
        ///     Gets the termination.
        /// </summary>
        public static Dictionary<int, string> LinearMotionTerminationTypeSource => new Dictionary<int, string>
        {
            { 0, "Not specified" },
            { 1, "Fine" },
            { 2, "Continuous" },
        };

        /// <summary>
        ///     Gets the termination.
        /// </summary>
        public static Dictionary<int, string> CircularMotionTerminationTypeSource => new Dictionary<int, string>
        {
            { 0, "Not specified" },
            { 1, "Fine" },
            { 2, "Continuous" },
        };

        /// <summary>
        ///     Gets or sets the CNT joint positioning path value.
        ///     When the CNT positioning path is specified, the robot approaches a target point but does not stop at the point and moves to the next point.
        ///     How closely the robot must approach a target point can be defined by specifying a value from 0 to 100.
        ///     The smaller the CNT value, the closer the robot gets to the point.
        ///     Robotic syntax: CNT.
        /// </summary>
        public int CntJointPositioningPathValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntJointPositioningPathValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntJointPositioningPathValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the CNT linear positioning path value.
        ///     When the CNT positioning path is specified, the robot approaches a target point but does not stop at the point and moves to the next point.
        ///     How closely the robot must approach a target point can be defined by specifying a value from 0 to 100.
        ///     The smaller the CNT value, the closer the robot gets to the point.
        ///     Robotic syntax: CNT.
        /// </summary>
        public int CntLinearPositioningPathValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntLinearPositioningPathValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntLinearPositioningPathValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the CNT circular positioning path value.
        ///     When the CNT positioning path is specified, the robot approaches a target point but does not stop at the point and moves to the next point.
        ///     How closely the robot must approach a target point can be defined by specifying a value from 0 to 100.
        ///     The smaller the CNT value, the closer the robot gets to the point.
        ///     Robotic syntax: CNT.
        /// </summary>
        public int CntCircularPositioningPathValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntCircularPositioningPathValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CntCircularPositioningPathValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint maximum speed (%). The joint speed ranges from 1 to 100%.
        ///     The instruction specifies the percentage of the maximum speed during joint motion.
        /// </summary>
        public int JointSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint acceleration and deceleration (%). The acceleration override value ranges from 0 to 150%.
        ///     The instruction specifies the percentage of the acceleration/deceleration rate during joint motion.
        ///     Robotic syntax: ACC.
        /// </summary>
        public int JointAccelerationAndDecelerationForJointMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForJointMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForJointMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint acceleration and deceleration (%). The acceleration override value ranges from 0 to 150%.
        ///     The instruction specifies the percentage of the acceleration/deceleration rate during linear motion.
        ///     Robotic syntax: ACC.
        /// </summary>
        public int JointAccelerationAndDecelerationForLinearMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForLinearMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForLinearMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint acceleration and deceleration (%). The acceleration override value ranges from 0 to 150%.
        ///     The instruction specifies the percentage of the acceleration/deceleration rate during circular motion.
        ///     Robotic syntax: ACC.
        /// </summary>
        public int JointAccelerationAndDecelerationForCircularMotion
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForCircularMotion)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAccelerationAndDecelerationForCircularMotion)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint acceleration/deceleration output for joint motion.
        /// </summary>
        public bool IsJointAccelerationDecelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointAccelerationDecelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointAccelerationDecelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint acceleration/deceleration output for linear motion.
        /// </summary>
        public bool IsLinearAccelerationDecelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearAccelerationDecelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearAccelerationDecelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint acceleration/deceleration output for circular motion.
        /// </summary>
        public bool IsCircularAccelerationDecelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularAccelerationDecelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularAccelerationDecelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the PTH motion output.
        ///     The instruction is used to improve the performance of continuous motion by forcing the robot to maintain the original path.
        ///     The instruction can only be used with continuous (CNT) move.
        ///     Robotic syntax: PTH.
        /// </summary>
        public bool IsPthMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsPthMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsPthMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the minimum rotation output.
        ///     The MROT motion modifier instructs the robot to take the minimum rotation path considering soft joint limits.
        ///     Robotic syntax: MROT.
        /// </summary>
        public bool IsMinimumRotationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsMinimumRotationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsMinimumRotationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the coordinated motion output.
        ///     The instruction ensures TCP path and speed maintenance in coordinated motions by taking into account coordinated groups/axes.
        ///     Robotic syntax: COORD.
        /// </summary>
        public bool IsCoordinatedMotionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCoordinatedMotionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCoordinatedMotionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the termination type of joint motion.
        /// </summary>
        public int JointMotionTerminationType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.JointMotionTerminationType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.JointMotionTerminationType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsJointMotionTerminationTypeContinuous));
            }
        }

        /// <summary>
        ///     Gets or sets the termination type of linear motion.
        /// </summary>
        public int LinearMotionTerminationType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearMotionTerminationType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearMotionTerminationType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsLinearMotionTerminationTypeContinuous));
            }
        }

        /// <summary>
        ///     Gets or sets the termination type of circular motion.
        /// </summary>
        public int CircularMotionTerminationType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularMotionTerminationType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularMotionTerminationType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsCircularMotionTerminationTypeContinuous));
            }
        }

        /// <summary>
        ///     Gets a value indicating whether joint motion termination type is continuous.
        /// </summary>
        public bool IsJointMotionTerminationTypeContinuous => this.JointMotionTerminationType == 2;

        /// <summary>
        ///     Gets a value indicating whether linear motion termination type is continuous.
        /// </summary>
        public bool IsLinearMotionTerminationTypeContinuous => this.LinearMotionTerminationType == 2;

        /// <summary>
        ///     Gets a value indicating whether circular motion termination type is continuous.
        /// </summary>
        public bool IsCircularMotionTerminationTypeContinuous => this.CircularMotionTerminationType == 2;

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable4"/> is checked.
        /// </summary>
        public bool IsExpandable4Checked
        {
            get => expandable4;

            set
            {
                expandable4 = value;
                this.OnPropertyChanged();
            }
        }
    }
}