// <copyright file="FanucProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     FanucProcessorSwitchesViewModel View Model.
    /// </summary>
    public class FanucProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;

        /// <summary>
        ///     Gets the feed output.
        /// </summary>
        public static Dictionary<int, string> FeedOutputTypeSource => new Dictionary<int, string>
        {
            { 0, "By value" },
            { 1, "By register" },
        };

        /// <summary>
        ///     Gets the user frame output type.
        /// </summary>
        public static Dictionary<int, string> UserFrameOutputTypeSource => new Dictionary<int, string>
        {
            { 0, "Disable" },
            { 1, "By register" },
            { 2, "By value" },
        };

        /// <summary>
        ///     Gets the tool frame output type.
        /// </summary>
        public static Dictionary<int, string> ToolFrameOutputTypeSource => new Dictionary<int, string>
        {
            { 0, "Disable" },
            { 1, "By register" },
            { 2, "By value" },
        };

        /// <summary>
        ///     Gets or sets the maximum number of lines allowed per individual code file. If 0, then there is no maximum.
        /// </summary>
        public int MaximumLinePerFile
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumLinePerFile)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumLinePerFile)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the minimum tool number allowed.
        /// </summary>
        public int MinimumToolNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MinimumToolNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MinimumToolNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum tool number allowed.
        /// </summary>
        public int MaximumToolNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumToolNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumToolNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the user frame position register number.
        ///     If user frame is output by value. The position register is used for defining the user frame in the robotic code.
        ///     Robotic syntax: PR[number].
        /// </summary>
        public int UserFrameRegisterNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.UserFrameRegisterNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.UserFrameRegisterNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tool frame position register number.
        ///     If tool frame is output by value. The position register is used for defining the tool frame in the robotic code.
        ///     Robotic syntax: PR[number].
        /// </summary>
        public int ToolFrameRegisterNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ToolFrameRegisterNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ToolFrameRegisterNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the feed output type.
        ///     If by value, then feeds are output directly on each move.
        ///     If by register, then a register number is called on each move. The actual feed value is found in the register of the controller.
        /// </summary>
        public int FeedOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.FeedOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.FeedOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the user frame output type.
        ///     If by value, then user frame used is explicitly output in the robotic code.
        ///     If by register, then user frame used is found in the register of the controller.
        ///     Robotic syntax: UFRAME.
        /// </summary>
        public int UserFrameOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.UserFrameOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.UserFrameOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsUserFrameOutputByValue));
            }
        }

        /// <summary>
        ///     Gets or sets the tool frame output type.
        ///     If by value, then tool frame used is explicitly output in the robotic code.
        ///     If by register, then tool frame used is found in the register of the controller.
        ///     Robotic syntax: UTOOL.
        /// </summary>
        public int ToolFrameOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolFrameOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolFrameOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsToolFrameOutputByValue));
            }
        }

        /// <summary>
        ///     Gets a value indicating whether user frame is output by value.
        /// </summary>
        public bool IsUserFrameOutputByValue => this.UserFrameOutputType == 2;

        /// <summary>
        ///     Gets a value indicating whether tool frame is output by value.
        /// </summary>
        public bool IsToolFrameOutputByValue => this.ToolFrameOutputType == 2;

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }
    }
}