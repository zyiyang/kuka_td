// <copyright file="FanucProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for FanucProcessorSwitches.
    /// </summary>
    public partial class FanucProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucProcessorSwitchesView"/> class.
        /// </summary>
        public FanucProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="FanucProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public FanucProcessorSwitchesView(FanucProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public FanucProcessorSwitchesViewModel ViewModel { get; }
    }
}