// <copyright file="FanucProcessorSwitchesBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.DeviceMenus.ProcessorSwitches
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing <see cref="FanucProcessorSwitchesView"/>.
    /// </summary>
    internal class FanucProcessorSwitchesBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the View Model.
        /// </summary>
        private FanucProcessorSwitchesViewModel ViewModel { get; set; }

        /// <summary>
        ///      Sets up the View Model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}