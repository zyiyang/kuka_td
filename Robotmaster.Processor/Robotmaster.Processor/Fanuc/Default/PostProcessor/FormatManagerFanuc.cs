﻿// <copyright file="FormatManagerFanuc.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.Kinematics;

    /// <summary>
    ///     Fanuc Format Manager.
    /// </summary>
    internal static class FormatManagerFanuc
    {
        private const string LengthOutputFormat = "{0,9:###0.000}";

        private const string GenericFormat = "{0,9:###0.000}";

        private const string JointValueOutputFormat = "{0,9:###0.000}";

        /// <summary>
        ///     Gets main program extension.
        /// </summary>
        internal static string MainProgramExtension => ".LS";

        /// <summary>
        ///     Formats string as a Fanuc registry if enabled.
        /// </summary>
        /// <param name="value">Int to be formatted.</param>
        /// <param name="enable">Enable or disable formatting as registry.</param>
        /// <returns>String formatted as registry.</returns>
        internal static string AsRegistry(this int value, int enable)
        {
            return enable == 1 ? " R[" + value + "]" : value.ToString();
        }

        /// <summary>
        ///     Formats string as a Fanuc registry if enabled.
        /// </summary>
        /// <param name="value">Double to be formatted.</param>
        /// <param name="enable">Enable or disable formatting as registry.</param>
        /// <returns>String formatted as registry.</returns>
        internal static string AsRegistry(this double value, int enable)
        {
            return enable == 1 ? " R[" + value + "]" : value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Formats string as a Fanuc registry if enabled.
        /// </summary>
        /// <param name="value">Float to be formatted.</param>
        /// <param name="enable">Enable or disable formatting as registry.</param>
        /// <returns>String formatted as registry.</returns>
        internal static string AsRegistry(this float value, int enable)
        {
            return enable == 1 ? " R[" + value + "]" : value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Formats string as a Fanuc registry if enabled.
        /// </summary>
        /// <param name="value">String to be formatted.</param>
        /// <param name="enable">Enable or disable formatting as registry.</param>
        /// <returns>String formatted as registry.</returns>
        internal static string AsRegistry(this string value, int enable)
        {
            return enable == 1 ? " R[" + value + "]" : value;
        }

        /// <summary>
        ///     Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>String with speed/feed units.</returns>
        internal static string WithSpeedUnits(this int value)
        {
            // TODO : (RMV7-4992) Implements units
            return value + " mm/sec";
        }

        /// <summary>
        ///     Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>String with speed/feed units.</returns>
        internal static string WithSpeedUnits(this double value)
        {
            // TODO : (RMV7-4992) Implements units
            return value + " mm/sec";
        }

        /// <summary>
        ///     Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>String with speed/feed units.</returns>
        internal static string WithSpeedUnits(this float value)
        {
            // TODO : (RMV7-4992) Implements units
            return value + " mm/sec";
        }

        /// <summary>
        ///     Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any string.</param>
        /// <returns>String with speed/feed units.</returns>
        internal static string WithSpeedUnits(this string value)
        {
            // TODO : (RMV7-4992) Implements units
            return value + " mm/sec";
        }

        /// <summary>
        ///     Adds the length units.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>String with length units.</returns>
        internal static string WithLenghUnit(this int value)
        {
            // TODO : (RMV7-4992) Implements units
            return string.Format(LengthOutputFormat, value) + "  mm";
        }

        /// <summary>
        ///     Adds the length units.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>String with length units.</returns>
        internal static string WithLenghUnit(this double value)
        {
            // TODO : (RMV7-4992) Implements units
            return string.Format(LengthOutputFormat, value) + "  mm";
        }

        /// <summary>
        ///     Adds the length units.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>String with length units.</returns>
        internal static string WithLenghUnit(this float value)
        {
            // TODO : (RMV7-4992) Implements units
            return string.Format(LengthOutputFormat, value) + "  mm";
        }

        /// <summary>
        ///     Adds the length units.
        /// </summary>
        /// <param name="value">Any string.</param>
        /// <returns>String with length units.</returns>
        internal static string WithLenghUnit(this string value)
        {
            // TODO : (RMV7-4992) Implements units
            return string.Format(LengthOutputFormat, value) + "  mm";
        }

        /// <summary>
        ///     Adds the angular units.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>String with angular units.</returns>
        internal static string WithAngularUnit(this int value)
        {
            return string.Format(JointValueOutputFormat, value) + " deg";
        }

        /// <summary>
        ///     Adds the angular units.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>String with angular units.</returns>
        internal static string WithAngularUnit(this double value)
        {
            return string.Format(JointValueOutputFormat, value) + " deg";
        }

        /// <summary>
        ///     Adds the angular units.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>String with angular units.</returns>
        internal static string WithAngularUnit(this float value)
        {
            return string.Format(JointValueOutputFormat, value) + " deg";
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this int value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this double value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        internal static string Formatted(this float value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Formats base configuration output.
        /// <para>"T" Front, "B" Back.</para>
        /// </summary>
        /// <param name="value">Base configuration.</param>
        /// <returns>Fanuc formatted value.</returns>
        internal static string Formatted(this BaseConfig value)
        {
            switch (value)
            {
                case BaseConfig.Back:
                    return "B";
                case BaseConfig.Front:
                    return "T";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats wrist configuration output.
        /// <para>"F" Flip, "N" No flip.</para>
        /// </summary>
        /// <param name="value">Wrist configuration.</param>
        /// <returns>Fanuc formatted value.</returns>
        internal static string Formatted(this WristConfig value)
        {
            switch (value)
            {
                case WristConfig.Negative:
                    return "N";
                case WristConfig.Positive:
                    return "F";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats Elbow configuration output.
        /// <para>"U" Up, "D" Down.</para>
        /// </summary>
        /// <param name="value">Elbow configuration.</param>
        /// <returns>Fanuc formatted value.</returns>
        internal static string Formatted(this ElbowConfig value)
        {
            switch (value)
            {
                case ElbowConfig.Down:
                    return "D";
                case ElbowConfig.Up:
                    return "U";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Parses a connection string (Key = Value) to a <see cref="Dictionary{TKey,TValue}"/>.
        ///     <para>
        ///         Value pairs are separated by semicolons (;). The equal sign (=) connects each key and its value.
        ///     </para>
        ///     <para>
        ///         Keys must be unique (left side).
        ///         If there are duplicated keys only the first one will be added.
        ///         Spaces before and after keys and values are ignored.
        ///     </para>
        ///     <para>
        ///         Example: Parses "R1=E1; R2=E2 ; E1 = E3" to  { "R1", "E1" },{ "R2", "E2" },{ "E1", "E3" }.
        ///     </para>
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        /// <returns>The equivalent dictionary.</returns>
        internal static Dictionary<string, string> ParseConnectionStringToDictionary(string connectionString)
        {
            MatchCollection matchCollection = Regex.Matches(connectionString, @"\s*(?<key>[^\s*;=\s*]+)\s*=\s*((?<value>[^\s*;]*))");
            IEnumerable<Match> enumerable = matchCollection.Cast<Match>();
            return enumerable.ToDictionary(match => match.Groups["key"].Value, match => match.Groups["value"].Value);
        }
    }
}