// <copyright file="PostProcessorFanuc.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Component.Types;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Fanuc.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Fanuc.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     <see cref="PostProcessorFanuc"/> inherits from <see cref="PostProcessor"/>.
    ///     <para>
    ///         Implements the Fanuc dedicated properties and methods of the post-processor in order to output Fanuc default robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorFanuc : PostProcessor
    {
        /// <summary>
        ///     Gets or sets the line number of the master file (main program).
        /// </summary>
        internal virtual int LineNumberOfMasterFile { get; set; }

        /// <summary>
        ///     Gets or sets the line number of the current file.
        /// </summary>
        internal virtual int LineNumberOfCurrentFile { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether there is a multi-turn for joint 5.
        /// </summary>
        internal virtual bool MultiturnJ5 { get; set; } = false;

        /// <summary>
        ///     Gets or sets the robot motion group number.
        /// </summary>
        internal virtual string RobotMotionGroupNumber { get; set; }

        /// <summary>
        ///     Gets or sets a sequence of external axes joints in the robot motion group.
        /// </summary>
        internal virtual IEnumerable<Joint> ExternalAxesJointsInRobotMotionGroup { get; set; }

        /// <summary>
        ///     Gets or sets a sequence of external axes joints out of robot motion group.
        /// </summary>
        internal virtual IEnumerable<Joint> ExternalAxesJointsOutOfRobotMotionGroup { get; set; }

        /// <summary>
        ///     Gets the current "move" <see cref="FileSection.StreamWriter"/>.
        ///     <para>
        ///         This property dynamically follows the <see cref="PostProcessor.CurrentPostFile"/>.
        ///     </para>
        /// </summary>
        internal virtual StreamWriter CurrentMoveSectionWriter => this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter;

        /// <summary>
        ///     Gets the current "position" <see cref="FileSection.StreamWriter"/>.
        ///     <para>
        ///         This property dynamically follows the <see cref="PostProcessor.CurrentPostFile"/>.
        ///     </para>
        /// </summary>
        internal virtual StreamWriter CurrentPositionSectionWriter => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets or sets a value indicating whether the program is RTCP.
        /// </summary>
        internal virtual bool IsRtcp { get; set; }

        /// <summary>
        ///     Starts the <paramref name="mainPostFile"/> formatting following <see cref="Fanuc"/> convention.
        ///     <para>
        ///         Initializes <paramref name="mainPostFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     </para>
        ///     <para>
        ///         Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        ///     </para>
        /// </summary>
        /// <param name="mainPostFile">The main <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartMainPostFile(PostFile mainPostFile, CbtNode operationNode, PathNode point)
        {
            mainPostFile.FileSection.Header =
                "/PROG  " + mainPostFile.FileName + "\r\n" +
                "/ATTR" + "\r\n" +
                "OWNER           = MNEDITOR;" + "\r\n" +
                "COMMENT         = \"BY ROBOTMASTER\" ;" + "\r\n" +
                "PROG_SIZE       = 0;" + "\r\n" +
                "CREATE          = DATE " + this.ProcessorDateTime.ToString("yy-MM-dd") + "  TIME " + this.ProcessorDateTime.ToString("HH:mm:ss") + ";" + "\r\n" +
                "MODIFIED        = DATE " + this.ProcessorDateTime.ToString("yy-MM-dd") + "  TIME " + this.ProcessorDateTime.ToString("HH:mm:ss") + ";" + "\r\n" +
                "FILE_NAME       = ;" + "\r\n" +
                "VERSION         = 0;" + "\r\n" +
                "LINE_COUNT      = 0;" + "\r\n" +
                "MEMORY_SIZE     = 0;" + "\r\n" +
                "PROTECT         = READ_WRITE;" + "\r\n" +
                "TCD:  STACK_SIZE        = 0," + "\r\n" +
                "      TASK_PRIORITY     = 50," + "\r\n" +
                "      TIME_SLICE        = 0," + "\r\n" +
                "      BUSY_LAMP_OFF     = 0," + "\r\n" +
                "      ABORT_REQUEST     = 0," + "\r\n" +
                "      PAUSE_REQUEST     = 0;" + "\r\n" +
                this.FormatDefaultGroup() + "\r\n" +
                "CONTROL_CODE = 00000000 00000000;" + "\r\n";

            // Move sub-section
            mainPostFile.FileSection.SubFileSections.Add(new FileSection()); // Add Move sub-section
            mainPostFile.FileSection.SubFileSections[0].Header = "/MN" + "\r\n";

            // FormatPosition sub-section
            mainPostFile.FileSection.SubFileSections.Add(new FileSection()); // Add FormatPosition sub-section
            mainPostFile.FileSection.SubFileSections[1].Header = "/POS" + "\r\n";
            mainPostFile.FileSection.SubFileSections[1].Footer = "/END";
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Fanuc"/> convention.
        ///     <para>
        ///         Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     </para>
        ///     <para>
        ///         Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.
        ///     </para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            postFile.FileSection.Header =
                "/PROG  " + postFile.FileName + "\r\n" +
                "/ATTR" + "\r\n" +
                "OWNER           = MNEDITOR;" + "\r\n" +
                "COMMENT         = \"BY ROBOTMASTER\" ;" + "\r\n" +
                "PROG_SIZE       = 0;" + "\r\n" +
                "CREATE          = DATE " + this.ProcessorDateTime.ToString("yy-MM-dd") + "  TIME " + this.ProcessorDateTime.ToString("HH:mm:ss") + ";" + "\r\n" +
                "MODIFIED        = DATE " + this.ProcessorDateTime.ToString("yy-MM-dd") + "  TIME " + this.ProcessorDateTime.ToString("HH:mm:ss") + ";" + "\r\n" +
                "FILE_NAME       = ;" + "\r\n" +
                "VERSION         = 0;" + "\r\n" +
                "LINE_COUNT      = " + "###To_be_replaced_by_the_line_count###" + ";" + "\r\n" +
                "MEMORY_SIZE     = 0;" + "\r\n" +
                "PROTECT         = READ_WRITE;" + "\r\n" +
                "TCD:  STACK_SIZE        = 0," + "\r\n" +
                "      TASK_PRIORITY     = 50," + "\r\n" +
                "      TIME_SLICE        = 0," + "\r\n" +
                "      BUSY_LAMP_OFF     = 0," + "\r\n" +
                "      ABORT_REQUEST     = 0," + "\r\n" +
                "      PAUSE_REQUEST     = 0;" + "\r\n" +
                this.FormatDefaultGroup() + "\r\n" +
                "CONTROL_CODE = 00000000 00000000;" + "\r\n";

            // Move sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add Move sub-section
            postFile.FileSection.SubFileSections[0].Header = "/MN" + "\r\n";

            // FormatPosition sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add FormatPosition sub-section
            postFile.FileSection.SubFileSections[1].Header = "/POS" + "\r\n";
            postFile.FileSection.SubFileSections[1].Footer = "/END" + "\r\n";
        }

        /// <summary>
        ///     Calls the current <see cref="PostFile"/> in its parent <see cref="PostFile"/> following <see cref="Motoman"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to be called in its parent.</param>
        /// <param name="operationNode">The current operation.</param>
        /// <param name="point">The current point.</param>
        internal virtual void CallPostFileIntoParent(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            //// Example:
            ////    2: CALL PROGRAM1_1;
            postFile.Parent.FileSection.SubFileSections[0].StreamWriter.WriteLine(++this.LineNumberOfMasterFile + ": CALL " + postFile.FileName + " ;");
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formatting following <see cref="Fanuc"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to ended.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void EndPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Replaces by the line number of the current file which couldn't be computed when the file was started.
            postFile.FileSection.Header = postFile.FileSection.Header.Replace("###To_be_replaced_by_the_line_count###", this.LineNumberOfCurrentFile.ToString());

            // Resets file splitting conditions.
            this.LineNumberOfCurrentFile = 0;
        }

        /// <summary>
        ///     Ends the <paramref name="mainPostFile"/> formatting following <see cref="Fanuc"/> convention for the main program.
        /// </summary>
        /// <param name="mainPostFile">The <see cref="PostFile"/> to ended.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void EndMainPostFile(PostFile mainPostFile, CbtNode operationNode, PathNode point)
        {
            // Resets master file splitting conditions.
            this.LineNumberOfMasterFile = 0;
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            // Check if setup is Rtcp
            this.IsRtcp = SetupManager.IsRtcp(this.SetupNode);

            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            if (FanucProcessorSwitches.GetMaximumLinePerFile(this.CellSettingsNode) <= 0)
            {
                // Single file output
                this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_").ToUpper();
                this.CurrentPostFile.FileExtension = FormatManagerFanuc.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
            else
            {
                // Multi-file output
                this.CurrentPostFile.InsertIntermediateParent(); //// Create master (main) program
                this.CurrentPostFile.Parent.FileName = this.ProgramName.Replace(" ", "_").ToUpper();
                this.CurrentPostFile.Parent.FileExtension = FormatManagerFanuc.MainProgramExtension;
                this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, firstOperation, firstPoint);
                this.CurrentPostFile.FileExtension = FormatManagerFanuc.MainProgramExtension;

                this.CallPostFileIntoParent(this.CurrentPostFile, firstOperation, firstPoint);
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
        }

        /// <inheritdoc/>
        internal override void RunAfterProgramOutput()
        {
            base.RunAfterProgramOutput();

            CbtNode lastOperation = ProgramManager.GetOperationNodes(this.ProgramNode).Last();
            PathNode lastPoint = OperationManager.GetLastPoint(lastOperation);

            if (FanucProcessorSwitches.GetMaximumLinePerFile(this.CellSettingsNode) <= 0)
            {
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);
            }
            else
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);

                // Multi-file output
                PostFile parentPostFile = this.CurrentPostFile.Parent;
                while (!ReferenceEquals(parentPostFile, this.MainPostFile.Parent)) //// While root is not reached
                {
                    // End master (main) post-file
                    this.EndMainPostFile(parentPostFile, lastOperation, lastPoint);
                    parentPostFile = parentPostFile.Parent;
                }
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            base.RunBeforeOperationOutput(operationNode);

            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            // User Frame
            if (previousOperationNode == null || // first operation or when user frame change
                OperationManager.GetUserFrame(operationNode) != OperationManager.GetUserFrame(previousOperationNode))
            {
                this.FormatUserFrameOutput(operationNode);
            }

            // Tool Frame
            if (previousOperationNode == null || // first operation or when tool frame change
                OperationManager.GetTcpFrameValue(operationNode) != OperationManager.GetTcpFrameValue(previousOperationNode)) //// Matrix comparison
            {
                this.FormatToolFrame(operationNode);
            }
        }

        /// <summary>
        ///     Formats the user frame definition at a given operation.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void FormatUserFrameOutput(CbtNode operationNode)
        {
            UserFrame userFrameNode = OperationManager.GetUserFrame(operationNode);

            if (userFrameNode.Number == 0 || FanucProcessorSwitches.GetUserFrameOutputType(this.CellSettingsNode) == 1)
            {
                //// Example:
                ////    8: UFRAME_NUM = 1 ;
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UFRAME_NUM =" : " UTOOL_NUM =") + userFrameNode.Number + " ;");
            }
            else if (FanucProcessorSwitches.GetUserFrameOutputType(this.CellSettingsNode) == 2)
            {
                Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(OperationManager.GetUserFrame(operationNode), this.SceneCbtRoot, this.OperationCbtRoot, null);
                Vector3 userFrameEuler = this.RobotFormatter.MatrixToEuler(userFrameMatrix);
                int userFrameRegistryNumber = FanucProcessorSwitches.GetUserFrameRegisterNumber(this.CellSettingsNode);
                int toolFrameRegistryNumber = FanucProcessorSwitches.GetToolFrameRegisterNumber(this.CellSettingsNode);

                //// Example:
                ////    1: PR[3, 1]= 1160.539011 ;
                ////    2: PR[3, 2] = 0;
                ////    3: PR[3, 3] = 4.601451;
                ////    4: PR[3, 4] = 0;
                ////    5: PR[3, 5] = 0;
                ////    6: PR[3, 6] = 0;
                ////    7: UFRAME[1] = PR[3];
                ////    8: UFRAME_NUM = 1 ;
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",1] = " + userFrameMatrix.Position.X.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",2] = " + userFrameMatrix.Position.Y.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",3] = " + userFrameMatrix.Position.Z.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",4] = " + userFrameEuler.X.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",5] = " + userFrameEuler.Y.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + ",6] = " + userFrameEuler.Z.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UFRAME[" : " UTOOL[") + userFrameNode.Number + "] = PR[" + (!this.IsRtcp ? userFrameRegistryNumber : toolFrameRegistryNumber) + "] ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UFRAME_NUM = " : " UTOOL_NUM = ") + userFrameNode.Number + " ;");
            }
        }

        /// <summary>
        ///     Formats the tool frame definition at a given operation.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void FormatToolFrame(CbtNode operationNode)
        {
            if (FanucProcessorSwitches.GetToolFrameOutputType(this.CellSettingsNode) == 1)
            {
                //// Example:
                ////    16: UTOOL_NUM = 8;
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UTOOL_NUM =" : " UFRAME_NUM =") + OperationManager.GetTcpId(operationNode) + " ;");
            }
            else if (FanucProcessorSwitches.GetToolFrameOutputType(this.CellSettingsNode) == 2)
            {
                Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

                if (this.EnableToolFrameExtraRotation)
                {
                    toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
                }

                Vector3 toolFrameEuler = this.RobotFormatter.MatrixToEuler(toolFrameMatrix);
                int toolFrameRegistryNumber = FanucProcessorSwitches.GetToolFrameRegisterNumber(this.CellSettingsNode);
                int userFrameRegistryNumber = FanucProcessorSwitches.GetUserFrameRegisterNumber(this.CellSettingsNode);

                //// Example:
                ////    9: PR[4, 1] = -352.64472;
                ////    10: PR[4, 2] = 0;
                ////    11: PR[4, 3] = 469.275923;
                ////    12: PR[4, 4] = 0;
                ////    13: PR[4, 5] = -60.000527;
                ////    14: PR[4, 6] = 0;
                ////    15: UTOOL[8] = PR[4];
                ////    16: UTOOL_NUM = 8;
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",1] = " + toolFrameMatrix.Position.X.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",2] = " + toolFrameMatrix.Position.Y.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",3] = " + toolFrameMatrix.Position.Z.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",4] = " + toolFrameEuler.X.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",5] = " + toolFrameEuler.Y.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + ",6] = " + toolFrameEuler.Z.ToString("0.000") + " ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UTOOL[" : " UFRAME[") + OperationManager.GetTcpId(operationNode) + "] = PR[" + (!this.IsRtcp ? toolFrameRegistryNumber : userFrameRegistryNumber) + "] ; ");
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + (!this.IsRtcp ? " UTOOL_NUM = " : " UFRAME_NUM = ") + OperationManager.GetTcpId(operationNode) + " ;");
            }
        }

        /// <summary>
        ///     Find last available name among the children of the current <see cref="PostFile"/> parent.
        /// </summary>
        /// <param name="postFile">Current <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        /// <returns>The last available name.</returns>
        internal virtual string FindNextAvailableName(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Logic can be added here to manage naming of robotmaster sub-program(s) when implemented.
            string subPostFileNamePattern = postFile.Parent.FileName + "_";
            var maxIndex = 1;
            while (this.CurrentPostFile.Parent.Children.Any(pf => pf.FileName == subPostFileNamePattern + maxIndex))
            {
                maxIndex++;
            }

            return subPostFileNamePattern + maxIndex;
        }

        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // Multi-file Output
            if ((FanucProcessorSwitches.GetMaximumLinePerFile(this.CellSettingsNode) > 0) && (this.LineNumberOfCurrentFile >= FanucProcessorSwitches.GetMaximumLinePerFile(this.CellSettingsNode)))
            {
                this.EndPostFile(this.CurrentPostFile, operationNode, point);

                var newPostFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostFile);
                this.CurrentPostFile = newPostFile;

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, operationNode, point);
                this.CurrentPostFile.FileExtension = FormatManagerFanuc.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, operationNode, point);

                this.CallPostFileIntoParent(this.CurrentPostFile, operationNode, point);
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.LineNumberOfCurrentFile++;
            this.CurrentMoveSectionWriter.WriteLine(this.LineNumberOfCurrentFile + ": " + beforePointEvent.ToCode() + " ;");
        }

        /// <inheritdoc/>
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.CurrentMoveSectionWriter.Write(" " + inlineEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.LineNumberOfCurrentFile++;
            this.CurrentMoveSectionWriter.WriteLine(this.LineNumberOfCurrentFile + ": " + afterPointEvent.ToCode() + " ;");
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            //// Moves
            //// Example:
            ////    L P[29] 50 mm/sec CNT85 ACC65  ;
            this.LineNumberOfCurrentFile++;
            this.CurrentMoveSectionWriter.Write(this.LineNumberOfCurrentFile + ":");
            this.CurrentMoveSectionWriter.Write(this.FormatMotionType(point));
            this.CurrentMoveSectionWriter.Write(" " + this.FormatPointNumber(this.PointNumberInProgram));
            this.CurrentMoveSectionWriter.Write(this.FormatFeed(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatPositioningPath(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatAcceleration(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatExtraMotionInstructions(point, operationNode));
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentMoveSectionWriter.WriteLine(" ;");

            //// Positions
            //// Example:
            ////     P[2]{
            ////     GP1:
            ////     UF: 1, UT: 1,         CONFIG: 'N U T, 0, 0, 0',
            ////           X = -5.00  mm,       Y = -15.00  mm,       Z = 100.00  mm,
            ////          W = 180.00 deg,       P = 0.00 deg,       R = 0.00 deg,
            ////          E1 = -179.5206 deg,       E2 = 0.00 deg
            ////     };
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPointNumber(this.PointNumberInProgram) + "{");
            this.CurrentPositionSectionWriter.WriteLine(this.FormatMotionGroupNumber(this.RobotMotionGroupNumber));
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPositionUfAndUt(operationNode) + ",\t" + this.FormatConfig(point));
            this.CurrentPositionSectionWriter.Write(this.FormatPosition(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatOrientation(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            this.CurrentPositionSectionWriter.WriteLine("\r\n" + "};");
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            //// Circular move(s)
            //// Example:
            ////    25:C P[9]
            ////        P[10] 20 mm / sec CNT85 ACC65;
            if (point.IsArcMiddlePoint())
            {
                // Arc middle point move
                this.LineNumberOfCurrentFile++;
                this.CurrentMoveSectionWriter.Write(this.LineNumberOfCurrentFile + ":");
                this.CurrentMoveSectionWriter.Write(this.FormatMotionType(point));
                this.CurrentMoveSectionWriter.WriteLine(" " + this.FormatPointNumber(this.PointNumberInProgram));
            }
            else
            {
                // Arc end point move
                this.CurrentMoveSectionWriter.Write("     ");
                this.CurrentMoveSectionWriter.Write(this.FormatPointNumber(this.PointNumberInProgram));
                this.CurrentMoveSectionWriter.Write(this.FormatFeed(point, operationNode));
                this.CurrentMoveSectionWriter.Write(this.FormatPositioningPath(point, operationNode));
                this.CurrentMoveSectionWriter.Write(this.FormatAcceleration(point, operationNode));
                this.CurrentMoveSectionWriter.Write(this.FormatExtraMotionInstructions(point, operationNode));
                this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
                this.CurrentMoveSectionWriter.WriteLine(" ;");
            }

            //// Circular position
            //// Example:
            ////     P[9]{
            ////     GP1:
            ////     UF: 1, UT: 1,         CONFIG: 'N U T, 0, 0, 0',
            ////           X = -5.00  mm,       Y = -15.00  mm,       Z = 100.00  mm,
            ////          W = 180.00 deg,       P = 0.00 deg,       R = 0.00 deg,
            ////          E1 = -179.5206 deg,       E2 = 0.00 deg
            ////     };
            this.CurrentPositionSectionWriter.WriteLine(" " + this.FormatPointNumber(this.PointNumberInProgram) + "{");
            this.CurrentPositionSectionWriter.WriteLine(this.FormatMotionGroupNumber(this.RobotMotionGroupNumber));
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPositionUfAndUt(operationNode) + ",\t" + this.FormatConfig(point));
            this.CurrentPositionSectionWriter.Write(this.FormatPosition(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatOrientation(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            this.CurrentPositionSectionWriter.WriteLine("\r\n" + "};");
        }

        /// <inheritdoc/>
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            //// Moves
            //// Example:
            ////    J P[29] 50 mm/sec CNT85 ACC65  ;
            this.LineNumberOfCurrentFile++;
            this.CurrentMoveSectionWriter.Write(this.LineNumberOfCurrentFile + ":");
            this.CurrentMoveSectionWriter.Write(this.FormatMotionType(point));
            this.CurrentMoveSectionWriter.Write(" " + this.FormatPointNumber(this.PointNumberInProgram));
            this.CurrentMoveSectionWriter.Write(this.FormatFeed(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatPositioningPath(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatAcceleration(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatExtraMotionInstructions(point, operationNode));
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentMoveSectionWriter.WriteLine(" ;");

            //// Positions
            //// Example:
            ////     P[2]{
            ////     GP1:
            ////     UF: 1, UT: 1,         CONFIG: 'N U T, 0, 0, 0',
            ////           X = -5.00  mm,       Y = -15.00  mm,       Z = 100.00  mm,
            ////          W = 180.00 deg,       P = 0.00 deg,       R = 0.00 deg,
            ////          E1 = -179.5206 deg,       E2 = 0.00 deg
            ////     };
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPointNumber(this.PointNumberInProgram) + "{");
            this.CurrentPositionSectionWriter.WriteLine(this.FormatMotionGroupNumber(this.RobotMotionGroupNumber));
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPositionUfAndUt(operationNode) + ",\t" + this.FormatConfig(point));
            this.CurrentPositionSectionWriter.Write(this.FormatPosition(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatOrientation(point, operationNode));
            this.CurrentPositionSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            this.CurrentPositionSectionWriter.WriteLine("\r\n" + "};");
        }

        /// <inheritdoc/>
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            //// JJ move
            //// Example:
            ////    1: J P[1] 50% FINE;
            this.LineNumberOfCurrentFile++;
            this.CurrentMoveSectionWriter.Write(this.LineNumberOfCurrentFile + ":");
            this.CurrentMoveSectionWriter.Write(this.FormatMotionType(point));
            this.CurrentMoveSectionWriter.Write(" " + this.FormatPointNumber(this.PointNumberInProgram));
            this.CurrentMoveSectionWriter.Write(this.FormatFeed(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatPositioningPath(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatAcceleration(point, operationNode));
            this.CurrentMoveSectionWriter.Write(this.FormatExtraMotionInstructions(point, operationNode));
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
            this.CurrentMoveSectionWriter.WriteLine(" ;");

            //// JJ position
            //// Example:
            ////    P[1]{
            ////    GP1:
            ////    UF: 1, UT: 1,
            ////    J1 = 0.00 deg,       J2 = 0.00 deg,       J3 = 0.00 deg,
            ////    J4 = 0.00 deg,       J5 = 0.00 deg,       J6 = 0.00 deg,
            ////    E1 = -179.5206 deg,       E2 = 0.00 deg
            ////    };
            // TODO: (RMV7-4988) Group implementation (need structure)
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPointNumber(this.PointNumberInProgram) + "{");
            this.CurrentPositionSectionWriter.WriteLine(this.FormatMotionGroupNumber(this.RobotMotionGroupNumber));
            this.CurrentPositionSectionWriter.WriteLine(this.FormatPositionUfAndUt(operationNode) + ",");
            this.CurrentPositionSectionWriter.Write(this.FormatJointValues(point));
            this.CurrentPositionSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
            this.CurrentPositionSectionWriter.WriteLine("\r\n" + "};");
        }

        /// <summary>
        ///     Formats JJ position at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Fanuc formatted JJ position.</returns>
        internal virtual string FormatJointValues(PathNode point)
        {
            //// Example:
            ////    J1 = 0.00 deg,       J2 = 0.00 deg,       J3 = 0.00 deg,
            ////    J4 = 0.00 deg,       J5 = 0.00 deg,       J6 = 0.00 deg
            return
                "\t" + "J1= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J1"))) + "," +
                "\t" + "J2= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J2"))) + "," +
                "\t" + "J3= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J3"))) + "," + "\r\n" +
                "\t" + "J4= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J4"))) + "," +
                "\t" + "J5= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J5"))) + "," +
                "\t" + "J6= " + this.FormatJointValueWithUnit(point, this.RobotJoints.Single(j => j.Name.Contains("J6")));
        }

        /// <summary>
        ///     Formats value with units of a joint at a given point using <see cref="PostProcessor.JointFormat"/>/>.
        ///     <para>
        ///         The <see cref="Joint"/> can be a robot, rotary or rail axis.
        ///     </para>
        /// </summary>
        /// <param name="point">The point from which the joint value is read.</param>
        /// <param name="joint">The joint to output the value.</param>
        /// <returns>Fanuc formatted joint value with units.</returns>
        internal virtual string FormatJointValueWithUnit(PathNode point, Joint joint)
        {
            //// Example:
            //// 0.00 deg
            return
                //// Value
                point.JointValue(this.JointFormat[joint.Name]).Formatted() +
                //// Unit
                (joint.AxisType == AxisType.Revolute ? " deg" : " mm");
        }

        /// <summary>
        ///     Formats configuration at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Fanuc formatted configuration.</returns>
        internal virtual string FormatConfig(PathNode point)
        {
            return "CONFIG: '" + this.FormatConfigBits(point) + ", " + this.FormatTurnBits(point) + "',";
        }

        /// <summary>
        ///     Formats the calculated configuration bits at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Fanuc formatted configuration Bits.</returns>
        internal virtual string FormatConfigBits(PathNode point)
        {
            // "F" Flip "N" No flip
            // "L" Left "R" Right (multi-turn J5)
            // "U" Up "D" Down
            // "T" Front "B" Back

            //// Example : N U T
            if (!this.MultiturnJ5)
            {
                return this.CachedRobotConfig.WristConfiguration.Formatted() + " " +
                       this.CachedRobotConfig.ElbowConfiguration.Formatted() + " " +
                       this.CachedRobotConfig.BaseConfiguration.Formatted();
            }
            else
            {
                return this.FormatMultiturnJ5ConfigBit(point.JointValue(this.JointFormat["J5"])) +
                       this.CachedRobotConfig.ElbowConfiguration.Formatted() + " " +
                       this.CachedRobotConfig.BaseConfiguration.Formatted();
            }
        }

        /// <summary>
        ///     Formats the calculated turn bits at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Fanuc formatted configuration bits.</returns>
        internal virtual string FormatTurnBits(PathNode point)
        {
            ////  Example: 0, 0, 0
            List<double> robotJointValues = this.RobotJoints.Select(joint => point.JointValue(this.JointFormat[joint.Name])).ToList();

            // TODO: (RMV7-4988) Check multi-turn J5
            if (!this.MultiturnJ5)
            {
                return (Math.Sign(robotJointValues[0]) * (int)((Math.Abs(robotJointValues[0]) + 180) / 360)) +
                       ", " +
                       (Math.Sign(robotJointValues[3]) * (int)((Math.Abs(robotJointValues[3]) + 180) / 360)) +
                       ", " +
                       (Math.Sign(robotJointValues[5]) * (int)((Math.Abs(robotJointValues[5]) + 180) / 360));
            }
            else
            {
                return (Math.Sign(robotJointValues[0]) * (int)((Math.Abs(robotJointValues[0]) + 180) / 360)) +
                       ", " +
                       (Math.Sign(robotJointValues[4]) * (int)((Math.Abs(robotJointValues[4]) + 180) / 360)) +
                       ", " +
                       (Math.Sign(robotJointValues[5]) * (int)((Math.Abs(robotJointValues[5]) + 180) / 360));
            }
        }

        /// <summary>
        ///     Formats position at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Fanuc formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            //// Example:        X = -5.00  mm,       Y = -15.00  mm,       Z = 100.00  mm

            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return
                "\t" + "X = " + pointPositionInUserFrame.X.WithLenghUnit() + "," +
                "\t" + "Y = " + pointPositionInUserFrame.Y.WithLenghUnit() + "," +
                "\t" + "Z = " + pointPositionInUserFrame.Z.WithLenghUnit();
        }

        /// <summary>
        ///     Formats orientation at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Fanuc formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            //// Example:       W = 180.00 deg,       P = 0.00 deg,       R = 0.00 deg

            // Gets the Euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return
                ",\r\n" +
                "\t" + "W = " + euler[0].WithAngularUnit() + "," +
                "\t" + "P = " + euler[1].WithAngularUnit() + "," +
                "\t" + "R = " + euler[2].WithAngularUnit();
        }

        /// <inheritdoc/>
        internal override void InitalizeJointLists()
        {
            base.InitalizeJointLists();

            this.RobotMotionGroupNumber = this.RobotJoints.First().GroupNumber;
            this.ExternalAxesJointsInRobotMotionGroup = this.ExternalAxesJoints.Where(j => j.GroupNumber == this.RobotMotionGroupNumber);
            this.ExternalAxesJointsOutOfRobotMotionGroup = this.ExternalAxesJoints.Where(j => j.GroupNumber != this.RobotMotionGroupNumber);
        }

        /// <summary>
        ///     Formats external axes values.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Fanuc formatted external axes values.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point, CbtNode operationNode)
        {
            //// Example:
            ////    GP2:
            ////          UF : 1, UT : 1,
            ////          J1 = 55.00 deg,       J2 = 8.00 deg
            ////    or
            ////          E1=    36.10 deg,       E2=    -8.00 deg
            string output = string.Empty;
            var i = 0;

            foreach (Joint joint in this.ExternalAxesJointsInRobotMotionGroup)
            {
                //// Example:
                ////       ,       E2=    -8.00 deg
                output += "," + ((i % 3 == 0) ? "\r\n" : string.Empty) + //// Line jump every 3 external axes
                          "\t" + joint.Name + "= " + this.FormatJointValueWithUnit(point, joint);
                i++;
            }

            foreach (string groupNumber in this.ExternalAxesJointsOutOfRobotMotionGroup.Select(j => j.GroupNumber).OrderBy(gn => gn).Distinct())
            {
                //// Example:
                ////    GP2:
                ////          UF : 1, UT : 1,
                output += "\r\n" +
                          this.FormatMotionGroupNumber(groupNumber) + "\r\n" +
                          this.FormatPositionUfAndUt(operationNode);

                i = 0;
                foreach (Joint joint in this.ExternalAxesJointsOutOfRobotMotionGroup.Where(j => j.GroupNumber == groupNumber).OrderBy(joint => joint.Name))
                {
                    //// Example:
                    ////       ,       J2=    -8.00 deg
                    output += "," + ((i % 3 == 0) ? "\r\n" : string.Empty) + //// Line jump every 3 external axes
                              "\t" + joint.Name + "= " + this.FormatJointValueWithUnit(point, joint);
                    i++;
                }
            }

            return output;
        }

        /// <summary>
        ///     Formats user frame and tool frame in positions at a given operation.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Fanuc User Frame and Tool frame.</returns>
        internal virtual string FormatPositionUfAndUt(CbtNode operationNode)
        {
            // Example: UF: 1, UT: 1
            return "\t" + "UF : " + (!this.IsRtcp ? OperationManager.GetUserFrame(operationNode).Number : OperationManager.GetTcpId(operationNode)) + ", UT : " + (!this.IsRtcp ? OperationManager.GetTcpId(operationNode) : OperationManager.GetUserFrame(operationNode).Number);
        }

        /// <summary>
        ///     Formats elbow configuration output in the case of a "multi-turn J5" robot.
        ///     <para>"N" No flip, "F" Flip.</para>
        /// </summary>
        /// <param name="value">J5 value.</param>
        /// <returns>Fanuc formatted elbow configuration in the case of a "multi-turn J5" robot.</returns>
        internal virtual string FormatMultiturnJ5ConfigBit(double value)
        {
            // TODO: (RMV7-4988) Check multi-turn J5 : "L" Left "R" Right (multi-turn J5) ? or "N" No flip, "F" Flip?
            if (((int)((value % 360) / 180) + 1) == 1)
            {
                return "N";
            }

            return "F";
        }

        /// <summary>
        ///     Formats motion group number.
        /// </summary>
        /// <param name="group">Group number.</param>
        /// <returns>Formatted Fanuc group.</returns>
        internal virtual string FormatMotionGroupNumber(string group)
        {
            //// Example: GP2 :
            return "   " + "GP" + group + ":";
        }

        /// <summary>
        ///     Formats motion type at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Fanuc motion type.</returns>
        internal virtual string FormatMotionType(PathNode point)
        {
            switch (point.PathSetting().MotionType)
            {
                case DeviceMotionType.JointSpaceMove:
                    return "J";
                case DeviceMotionType.JointMove:
                    return "J";
                case DeviceMotionType.LinearMove:
                    return "L";
                case DeviceMotionType.ArcMove:
                    return "C";
                case DeviceMotionType.SafeApproachMove:
                case DeviceMotionType.SafeRetractMove:
                    return point.MoveType() == MoveType.Linear ? "L" : "J";
                case DeviceMotionType.Invalid:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats a given point number.
        /// </summary>
        /// <param name="pointNumber">Point number.</param>
        /// <returns>Formatted Fanuc point number.</returns>
        internal virtual string FormatPointNumber(int pointNumber)
        {
            //// Example: P[1]
            return "P[" + pointNumber + "]";
        }

        /// <summary>
        ///     Formats the feed or speed depending on the motion type at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Fanuc feed or speed.</returns>
        internal virtual string FormatFeed(PathNode point, CbtNode operationNode)
        {
            switch (point.PathSetting().MotionType)
            {
                case DeviceMotionType.JointSpaceMove:
                case DeviceMotionType.JointMove:
                    return " " + FanucMotionSettings.GetJointSpeed(operationNode).AsRegistry(FanucProcessorSwitches.GetFeedOutputType(this.CellSettingsNode)) + " %";
                case DeviceMotionType.LinearMove:
                    return " " + point.Feedrate().LinearFeedrate.AsRegistry(FanucProcessorSwitches.GetFeedOutputType(this.CellSettingsNode)).WithSpeedUnits();
                case DeviceMotionType.ArcMove:
                    return " " + point.Feedrate().LinearFeedrate.AsRegistry(FanucProcessorSwitches.GetFeedOutputType(this.CellSettingsNode)).WithSpeedUnits();
                case DeviceMotionType.SafeApproachMove:
                case DeviceMotionType.SafeRetractMove:
                    return point.MoveType() == MoveType.Linear ?
                        " " + point.Feedrate().LinearFeedrate.AsRegistry(FanucProcessorSwitches.GetFeedOutputType(this.CellSettingsNode)).WithSpeedUnits() :
                        " " + FanucMotionSettings.GetJointSpeed(operationNode).AsRegistry(FanucProcessorSwitches.GetFeedOutputType(this.CellSettingsNode)) + " %";
                case DeviceMotionType.Invalid:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the acceleration instruction depending on the motion type at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Fanuc acceleration instruction output.</returns>
        internal virtual string FormatAcceleration(PathNode point, CbtNode operationNode)
        {
            switch (point.PathSetting().MotionType)
            {
                case DeviceMotionType.JointSpaceMove:
                    return string.Empty;
                case DeviceMotionType.JointMove:
                    return FanucMotionSettings.GetIsJointAccelerationDecelerationEnabled(operationNode) ? " ACC" + FanucMotionSettings.GetJointAccelerationAndDecelerationForJointMotion(operationNode) : string.Empty;
                case DeviceMotionType.LinearMove:
                    return FanucMotionSettings.GetIsLinearAccelerationDecelerationEnabled(operationNode) ? " ACC" + FanucMotionSettings.GetJointAccelerationAndDecelerationForLinearMotion(operationNode) : string.Empty;
                case DeviceMotionType.ArcMove:
                    return FanucMotionSettings.GetIsCircularAccelerationDecelerationEnabled(operationNode) ? " ACC" + FanucMotionSettings.GetJointAccelerationAndDecelerationForCircularMotion(operationNode) : string.Empty;
                case DeviceMotionType.SafeApproachMove:
                case DeviceMotionType.SafeRetractMove:
                    return point.MoveType() == MoveType.Linear && FanucMotionSettings.GetIsLinearAccelerationDecelerationEnabled(operationNode)
                        ? " ACC" + FanucMotionSettings.GetJointAccelerationAndDecelerationForLinearMotion(operationNode) : FanucMotionSettings.GetIsJointAccelerationDecelerationEnabled(operationNode)
                            ? " ACC" + FanucMotionSettings.GetJointAccelerationAndDecelerationForJointMotion(operationNode) : string.Empty;
                case DeviceMotionType.Invalid:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the positioning path depending on the motion type (FINE/CNT) at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Fanuc positioning path.</returns>
        internal virtual string FormatPositioningPath(PathNode point, CbtNode operationNode)
        {
            switch (point.PathSetting().MotionType)
            {
                case DeviceMotionType.JointSpaceMove:
                    return " FINE";
                case DeviceMotionType.JointMove:
                    return GetJointMotionTerminationType();
                case DeviceMotionType.LinearMove:
                    return GetLinearMotionTerminationType();
                case DeviceMotionType.ArcMove:
                    return GetCircularMotionTerminationType();
                case DeviceMotionType.SafeApproachMove:
                case DeviceMotionType.SafeRetractMove:
                    return point.MoveType() == MoveType.Linear ? GetLinearMotionTerminationType() : GetJointMotionTerminationType();
                case DeviceMotionType.Invalid:
                    return string.Empty;
                default:
                    return string.Empty;
            }

            string GetJointMotionTerminationType()
            {
                switch (FanucMotionSettings.GetJointMotionTerminationType(operationNode))
                {
                    case 0: // Not specified
                        return string.Empty;
                    case 1: // Fine
                        return " FINE";
                    case 2: // Continuous
                        return " CNT" + FanucMotionSettings.GetCntJointPositioningPathValue(operationNode);
                    default:
                        return string.Empty;
                }
            }

            string GetLinearMotionTerminationType()
            {
                switch (FanucMotionSettings.GetLinearMotionTerminationType(operationNode))
                {
                    case 0: // Not specified
                        return string.Empty;
                    case 1: // Fine
                        return " FINE";
                    case 2: // Continuous
                        return " CNT" + FanucMotionSettings.GetCntLinearPositioningPathValue(operationNode);
                    default:
                        return string.Empty;
                }
            }

            string GetCircularMotionTerminationType()
            {
                switch (FanucMotionSettings.GetCircularMotionTerminationType(operationNode))
                {
                    case 0: // Not specified
                        return string.Empty;
                    case 1: // Fine
                        return " FINE";
                    case 2: // Continuous
                        return " CNT" + FanucMotionSettings.GetCntCircularPositioningPathValue(operationNode);
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        ///     Formats extra motion instructions at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Fanuc extra motion instructions output.</returns>
        internal virtual string FormatExtraMotionInstructions(PathNode point, CbtNode operationNode)
        {
            string extraMotionInstructions = string.Empty;

            // Example: 4:J P[2] 50% MROT  ;
            if (FanucMotionSettings.GetIsMinimumRotationEnabled(operationNode) && point.PathSetting().MotionType == DeviceMotionType.JointMove)
            {
                extraMotionInstructions += " MROT";
            }

            // Example: 6:L P[4] 10 mm/sec COORD;
            if (FanucMotionSettings.GetIsCoordinatedMotionEnabled(operationNode) && this.ExternalAxesJoints.Any())
            {
                // The COORD motion option applies only to linear (LC) and circular (CC) motion instructions.
                if (point.MoveType() == MoveType.Linear || point.MoveType() == MoveType.Circular)
                {
                    extraMotionInstructions += " COORD"; //// TODO: Add COORD[LDR] and COORD[R[...]] COORDINATED MOTION OPTION
                }
            }

            // Example: 6:L P[4] 10 mm/sec RTCP;
            if (this.IsRtcp && point.MoveType() != MoveType.Rapid)
            {
                extraMotionInstructions += " RTCP";
            }

            return extraMotionInstructions;
        }

        /// <summary>
        ///     Formats the Fanuc default group.
        ///     <para>
        ///         DEFAULT_GROUP &lt;Group 1&gt;,&lt;Group 2&gt;,&lt;Group 3&gt;,&lt;Group 4&gt;,&lt;Group 5&gt;.
        ///     </para>
        ///     <para>
        ///         Group # = *, indicates that the Group # is inactive.
        ///     </para>
        ///     <para>
        ///         Group # = 1, indicates that the Group # is active.
        ///     </para>
        /// </summary>
        /// <remarks>
        ///     DEFAULT_GROUP configures the motion groups.
        /// </remarks>
        /// <returns>Formatted Fanuc default group.</returns>
        internal virtual string FormatDefaultGroup()
        {
            // All robots and external axes joints
            IEnumerable<Joint> allRobotManipulatorsAndExternalAxesManipulators = this.RobotJoints.Concat(this.ExternalAxesJoints);

            // Sequence containing all distinct group numbers
            IEnumerable<string> controlGroups = allRobotManipulatorsAndExternalAxesManipulators
                                                .Select(manipulator => manipulator.GroupNumber)
                                                .Distinct();

            //// Example: DEFAULT_GROUP   = 1,1,*,*,*;
            var defaultGroup = "DEFAULT_GROUP   = ";

            // The highest group number can be 5.
            for (var i = 1; i <= 5; i++)
            {
                defaultGroup += controlGroups.Contains(i.ToString()) ? "1" : "*";
                defaultGroup += i != 5 ? "," : ";";
            }

            return defaultGroup;
        }

        /// <inheritdoc/>
        internal override bool IsProgramInputValid()
        {
            // Verifies if the device has the "groupNumber" defined in the ROBX file.
            if (!this.IsGroupNumberValid())
            {
                this.NotifyUser("ERROR: The \"groupNumber\" tag in the ROBX file is not valid. The \"groupNumber\" must be between 1 and 5. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the device has the "label" for the rail and rotaries axes properly defined in the ROBX file.
            if (!this.IsExternalAxesNameValid())
            {
                this.NotifyUser("ERROR: The rail/rotary axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the rail and rotary axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the rail and rotary axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsExternalAxesNameValid()
        {
            var externalAxes = new List<string>() { "E1", "E2", "E3", "E4", "E5" };
            return !(this.Rails.Any() && this.Rails.Any(x => !externalAxes.Contains(x.Name))) &&
                   !(this.Rotaries.Any() && this.Rotaries.Any(x => !externalAxes.Contains(x.Name)));
        }

        /// <summary>
        ///     Gets a value indicating whether the "groupNumber" tag(s) found in the ROBX are valid.
        /// </summary>
        /// <returns><c>true</c> if the "groupNumber" tag(s) are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsGroupNumberValid()
        {
            // All manipulators: robots + rotaries + rails
            IEnumerable<CbtNode> allManipulatorNodes = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode));

            // TODO Re-factor the return statement below so that we do not expose the Manipulator class once AT-1139 is DONE (Blocked by)
            return allManipulatorNodes.Select(node => node.GetComponents<Manipulator>()
                                                          .Single()
                                                          .MainJoints[0]?.GroupNumber)
                                      .All(groupNumber => !string.IsNullOrEmpty(groupNumber) &&
                                                          groupNumber.All(char.IsDigit) &&
                                                          int.Parse(groupNumber) > 0 &&
                                                          int.Parse(groupNumber) < 6);
        }
    }
}
