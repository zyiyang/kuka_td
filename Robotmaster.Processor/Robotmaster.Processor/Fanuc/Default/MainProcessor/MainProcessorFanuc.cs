﻿// <copyright file="MainProcessorFanuc.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.Default.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    /// <inheritdoc />
    /// <summary>
    ///     This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    ///     <para>
    ///         Implements the Fanuc dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorFanuc : MainProcessor
    {
        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            this.ToolingActivationMacro = "CALL " + CommonToolingActivationSettings.GetToolingActivationMacro(operationNode);
            this.ToolingDeactivationMacro = "CALL " + CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode);
            base.EditOperationBeforeAllPointEdits(operationNode);
        }

        /// <inheritdoc />
        internal override PathNode AddToolChangeMacroEvent(CbtNode operationNode, PathNode point)
        {
            this.ToolChangeMacro = "CALL " + OperationManager.GetTool(operationNode).Name.Replace(" ", "_").ToUpper();
            return base.AddToolChangeMacroEvent(operationNode, point);
        }
    }
}