// <copyright file="FanucWeldingSanyProcessMenu.cs" company="Hypertherm Robotic Software Inc.">
// Copyright 2002-2020 Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>
// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.ProcessMenus
{
    using System;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Manages the data of the external menu Fanuc Welding Parameters.
    /// </summary>
    internal static class FanucWeldingSanyProcessMenu
    {
        /// <summary>
        ///     The cache for retrieved SMS values.
        /// </summary>
        private static readonly SmsValueCache Cache = new SmsValueCache();

        /// <summary>
        ///     Checks if the external menu is loaded.
        /// </summary>
        /// <param name="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu exists; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsLoaded(object node)
        {
            return ExternalMenuManager.IsExternalMenuLoaded("PM_ProcessMenusFanucWeldingSany_7588CC57", node);
        }

        /// <summary>
        ///     Checks if the external menu is overridden.
        /// </summary>
        /// <param node="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu is overridden; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsOverridden(object node)
        {
            return ExternalMenuManager.IsExternalMenuOverridden("PM_ProcessMenusFanucWeldingSany_7588CC57", node);
        }

        /// <summary>
        ///     Gets The application name. Example: ARC Welding Equipment Number : 1
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The application name. Example: ARC Welding Equipment Number : 1
        /// </returns>
        internal static string GetApplicationName(object node, bool useCache = true)
        {
            const string settingUid = "{21D0062C-38E2-4A24-8D4E-172D3A2357BE}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The application name. Example: ARC Welding Equipment Number : 1
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetApplicationName(object node, string value, bool useCache = true)
        {
            const string settingUid = "{21D0062C-38E2-4A24-8D4E-172D3A2357BE}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The arc ON command. Example: Arc Start
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The arc ON command. Example: Arc Start
        /// </returns>
        internal static string GetWeldOnCommand(object node, bool useCache = true)
        {
            const string settingUid = "{CFCB1E99-38CB-48F6-94EA-E344EC2398E9}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The arc ON command. Example: Arc Start
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeldOnCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{CFCB1E99-38CB-48F6-94EA-E344EC2398E9}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The arc OFF command. Example: Arc End
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The arc OFF command. Example: Arc End
        /// </returns>
        internal static string GetWeldOffCommand(object node, bool useCache = true)
        {
            const string settingUid = "{595FC434-ABB0-48E9-9E2E-028A4CAA7A38}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The arc OFF command. Example: Arc End
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeldOffCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{595FC434-ABB0-48E9-9E2E-028A4CAA7A38}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The weaving ON command. Example: Weave
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The weaving ON command. Example: Weave
        /// </returns>
        internal static string GetWeaveOnCommand(object node, bool useCache = true)
        {
            const string settingUid = "{9EB420E6-85AE-46E3-9B5B-680D646EAC49}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The weaving ON command. Example: Weave
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeaveOnCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{9EB420E6-85AE-46E3-9B5B-680D646EAC49}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The weaving OFF command. Example: Weave End
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The weaving OFF command. Example: Weave End
        /// </returns>
        internal static string GetWeaveOffCommand(object node, bool useCache = true)
        {
            const string settingUid = "{83200D96-0C8B-4FEA-B125-E03B49368157}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The weaving OFF command. Example: Weave End
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeaveOffCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{83200D96-0C8B-4FEA-B125-E03B49368157}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The seam tracking ON command. Example: Track
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The seam tracking ON command. Example: Track
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </returns>
        internal static string GetSeamTrackingOnCommand(object node, bool useCache = true)
        {
            const string settingUid = "{BDC47373-41BB-41DA-A472-755FA11D192C}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The seam tracking ON command. Example: Track
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetSeamTrackingOnCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{BDC47373-41BB-41DA-A472-755FA11D192C}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The seam tracking OFF command. Example: Track End
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The seam tracking OFF command. Example: Track End
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </returns>
        internal static string GetSeamTrackingOffCommand(object node, bool useCache = true)
        {
            const string settingUid = "{02E3AD0E-834F-49DE-8715-253A5834F0EB}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets The seam tracking OFF command. Example: Track End
        ///     
        ///     Seam tracking automatically adjusts the robot's vertical and lateral trajectory to compensate for part warping or misplacement.
        ///     
        ///     Through Arc Seam Tracking (TAST) measures welding current at the sides of the joint during weave motion and makes adjustments to the weld path automatically.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetSeamTrackingOffCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{02E3AD0E-834F-49DE-8715-253A5834F0EB}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets RPM stores the seam tracking positional offset data from the tracked root pass.
        ///     
        ///     The RPM data can be played back on consecutive passes with the Multi Pass Offset (MP) added to make pre-programmed adjustments in weld length, placement, and torch and work angles.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing RPM stores the seam tracking positional offset data from the tracked root pass.
        ///     
        ///     The RPM data can be played back on consecutive passes with the Multi Pass Offset (MP) added to make pre-programmed adjustments in weld length, placement, and torch and work angles.
        /// </returns>
        internal static string GetRootPassMemorizationCommand(object node, bool useCache = true)
        {
            const string settingUid = "{FFBD9A2E-667F-46F9-A9F1-CD1449C16659}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (string)cachedValue;
                }
                else
                {
                    string result = ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return ExternalMenuManager.GetUserValueForProcessor(settingUid, node);
            }
        }

        /// <summary>
        ///     Sets RPM stores the seam tracking positional offset data from the tracked root pass.
        ///     
        ///     The RPM data can be played back on consecutive passes with the Multi Pass Offset (MP) added to make pre-programmed adjustments in weld length, placement, and torch and work angles.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetRootPassMemorizationCommand(object node, string value, bool useCache = true)
        {
            const string settingUid = "{FFBD9A2E-667F-46F9-A9F1-CD1449C16659}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The Weld Speed Output Method.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The Weld Speed Output Method.
        /// </returns>
        internal static int GetWeldSpeedOutputMethod(object node, bool useCache = true)
        {
            const string settingUid = "{503D582D-F073-4DE9-A7BB-B518219626FE}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The Weld Speed Output Method.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetWeldSpeedOutputMethod(object node, string value, bool useCache = true)
        {
            const string settingUid = "{503D582D-F073-4DE9-A7BB-B518219626FE}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The Touch Offset Registry Numbering Method.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The Touch Offset Registry Numbering Method.
        /// </returns>
        internal static int GetTouchOffsetNumberingMethod(object node, bool useCache = true)
        {
            const string settingUid = "{8041C64D-09E9-4808-8886-3F207389BDC6}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The Touch Offset Registry Numbering Method.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetTouchOffsetNumberingMethod(object node, string value, bool useCache = true)
        {
            const string settingUid = "{8041C64D-09E9-4808-8886-3F207389BDC6}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets Starting registry number for touch.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing Starting registry number for touch.
        /// </returns>
        internal static int GetTouchOffsetRegistryStartingNumber(object node, bool useCache = true)
        {
            const string settingUid = "{2F260558-4686-4D82-9126-33348CC8E4ED}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets Starting registry number for touch.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetTouchOffsetRegistryStartingNumber(object node, int value, bool useCache = true)
        {
            const string settingUid = "{2F260558-4686-4D82-9126-33348CC8E4ED}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }
        /// <summary>
        ///     Clears the SMS value cache.
        /// </summary>
        ///
        internal static void ClearCache()
        {
            Cache.Clear();
        }
    }
}
