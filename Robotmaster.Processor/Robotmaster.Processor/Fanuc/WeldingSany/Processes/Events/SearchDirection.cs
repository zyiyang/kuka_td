﻿// <copyright file="SearchDirection.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Search direction event.
    /// </summary>
    [DataContract(Name = "FanucSearchDirection")]
    public class SearchDirection : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SearchDirection"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SearchDirection()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Inline;
        }

        /// <summary>
        ///     Gets or sets the search direction command.
        /// </summary>
        [DataMember(Name = "SearchDirectionCommand")]
        public string SearchDirectionCommand { get; set; } = "Search";

        /// <summary>
        ///     Gets or sets the direction vector.
        /// </summary>
        [DataMember(Name = "SearchDirectionVector")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public SearchDirections SearchDirectionVector { get; set; } = SearchDirections.PlusX;

        /// <summary>
        ///     Gets or sets the touch offset.
        /// </summary>
        [DataMember(Name = "TouchSensingPositionRegistry")]
        public int TouchSensingPositionRegistry { get; set; } = 2;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SearchDirectionCommand}[{this.SearchDirectionVector.ToString().Replace("Plus", string.Empty).Replace("Minus", "-")}]";
        }
    }
}
