﻿// <copyright file="SeamTrackingOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Track OFF event.
    /// </summary>
    [DataContract(Name = "FanucSeamTrackingOff")]
    public class SeamTrackingOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SeamTrackingOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SeamTrackingOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the seam tracking OFF command.
        /// </summary>
        [DataMember(Name = "SeamTrackingOffCommand")]
        public string SeamTrackingOffCommand { get; set; } = "Track End";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SeamTrackingOffCommand}";
        }
    }
}
