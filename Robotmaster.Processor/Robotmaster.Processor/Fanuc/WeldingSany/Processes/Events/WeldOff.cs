﻿// <copyright file="WeldOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld OFF event.
    /// </summary>
    [DataContract(Name = "FanucWeldOff")]
    public class WeldOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the weld OFF command.
        /// </summary>
        [DataMember(Name = "WeldOffCommand")]
        public string WeldOffCommand { get; set; } = "Weld End";

        /// <summary>
        ///     Gets or sets the schedule / process.
        /// </summary>
        [DataMember(Name = "WeldScheduleProcess")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string WeldScheduleProcess { get; set; } = "1,1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeldOffCommand}[{this.WeldScheduleProcess}]";
        }
    }
}
