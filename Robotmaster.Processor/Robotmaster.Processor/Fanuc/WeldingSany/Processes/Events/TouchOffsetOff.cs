﻿// <copyright file="TouchOffsetOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Touch offset OFF event.
    /// </summary>
    [DataContract(Name = "FanucTouchOffsetOff")]
    public class TouchOffsetOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TouchOffsetOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public TouchOffsetOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the touch OFF command.
        /// </summary>
        [DataMember(Name = "TouchOffsetOffCommand")]
        public string TouchOffsetOffCommand { get; set; } = "Touch Offset End";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.TouchOffsetOffCommand}";
        }
    }
}
