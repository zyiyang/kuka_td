﻿// <copyright file="SeamTrackingOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Track ON event.
    /// </summary>
    [DataContract(Name = "FanucSeamTrackingOn")]
    public class SeamTrackingOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SeamTrackingOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SeamTrackingOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the seam tracking ON command.
        /// </summary>
        [DataMember(Name = "SeamTrackingOnCommand")]
        public string SeamTrackingOnCommand { get; set; } = "Track";

        /// <summary>
        ///     Gets or sets the tracking number.
        /// </summary>
        [DataMember(Name = "SeamTrackingNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public int SeamTrackingNumber { get; set; } = 1;

        /// <summary>
        ///     Gets or sets the RPM commands.
        /// </summary>
        [DataMember(Name = "RootPassMemorizationCommand")]
        public string RootPassMemorizationCommand { get; set; } = "RPM";

        /// <summary>
        ///     Gets or sets the RPM Buffer Number.
        /// </summary>
        [DataMember(Name = "RootPassMemorizationNumber")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double RootPassMemorizationNumber { get; set; } = 10;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SeamTrackingOnCommand} TAST[{this.SeamTrackingNumber}] {this.RootPassMemorizationCommand}[{this.RootPassMemorizationNumber}]";
        }
    }
}
