﻿// <copyright file="WeldOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld ON event.
    /// </summary>
    [DataContract(Name = "FanucWeldOn")]
    public class WeldOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the weld ON command.
        /// </summary>
        [DataMember(Name = "WeldOnCommand")]
        public string WeldOnCommand { get; set; } = "Weld Start";

        /// <summary>
        ///     Gets or sets the schedule / type.
        /// </summary>
        [DataMember(Name = "WeldScheduleProcess")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string WeldScheduleProcess { get; set; } = "1,1";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeldOnCommand}[{this.WeldScheduleProcess}]";
        }
    }
}
