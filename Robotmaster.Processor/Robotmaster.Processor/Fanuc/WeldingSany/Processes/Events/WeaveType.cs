﻿// <copyright file="WeaveType.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;

    /// <summary>
    ///     Weave type.
    /// </summary>
    [DataContract(Name = "FanucWeaveType")]
    public enum WeaveType
    {
        /// <summary>
        ///     Sinusoid weave.
        /// </summary>
        [EnumMember(Value = "Sine")]
        Sine,

        /// <summary>
        ///     Circular weave.
        /// </summary>
        [EnumMember(Value = "Circle")]
        Circle,

        /// <summary>
        ///     Figure8 weave.
        /// </summary>
        [EnumMember(Value = "Figure8")]
        Figure8,
    }
}
