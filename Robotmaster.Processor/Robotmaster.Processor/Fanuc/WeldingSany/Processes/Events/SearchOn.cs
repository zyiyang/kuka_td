﻿// <copyright file="SearchOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Search ON event.
    /// </summary>
    [DataContract(Name = "FanucSearchOn")]
    public class SearchOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SearchOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SearchOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the search ON command.
        /// </summary>
        [DataMember(Name = "SearchOnCommand")]
        public string SearchOnCommand { get; set; } = "Search Start";

        /// <summary>
        ///     Gets or sets the schedule / type.
        /// </summary>
        [DataMember(Name = "WeldScheduleProcess")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public string SearchScheduleProcess { get; set; } = "1,1";

        /// <summary>
        ///     Gets or sets the touch offset.
        /// </summary>
        [DataMember(Name = "TouchSensingPositionRegistry")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public int TouchSensingPositionRegistry { get; set; } = 2;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SearchOnCommand}[{this.SearchScheduleProcess}] " + $"PR[{this.TouchSensingPositionRegistry}]";
        }
    }
}
