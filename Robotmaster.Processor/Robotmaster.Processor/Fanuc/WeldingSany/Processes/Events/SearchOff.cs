﻿// <copyright file="SearchOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Search OFF event.
    /// </summary>
    [DataContract(Name = "FanucSearchOff")]
    public class SearchOff : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SearchOff"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public SearchOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the Search OFF command.
        /// </summary>
        [DataMember(Name = "SearchOffCommand")]
        public string SearchOffCommand { get; set; } = "Search End";

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SearchOffCommand}";
        }
    }
}
