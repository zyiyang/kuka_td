﻿// <copyright file="TouchOffsetOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Touch offset ON event.
    /// </summary>
    [DataContract(Name = "FanucTouchOffsetOn")]
    public class TouchOffsetOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TouchOffsetOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public TouchOffsetOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the touch ON command.
        /// </summary>
        [DataMember(Name = "TouchOffsetOnCommand")]
        public string TouchOffsetOnCommand { get; set; } = "Touch Offset";

        /// <summary>
        ///     Gets or sets the touch offset.
        /// </summary>
        [DataMember(Name = "TouchSensingPositionRegistry")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public int TouchSensingPositionRegistry { get; set; } = 2;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.TouchOffsetOnCommand} " + $"PR[{this.TouchSensingPositionRegistry}]";
        }
    }
}
