﻿// <copyright file="WeaveOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weave ON event.
    /// </summary>
    [DataContract(Name = "FanucWeaveOn")]
    public class WeaveOn : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeaveOn"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeaveOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the weave ON command.
        /// </summary>
        [DataMember(Name = "WeaveOnCommand")]
        public string WeaveOnCommand { get; set; } = "Weave";

        /// <summary>
        ///     Gets or sets the weave schedule.
        /// </summary>
        [DataMember(Name = "WeaveSchedule")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public int WeaveSchedule { get; set; } = 1;

        /// <summary>
        ///     Gets or sets the weave pattern.
        /// </summary>
        [DataMember(Name = "WeavePattern")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public WeaveType WeavePattern { get; set; } = WeaveType.Sine;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.WeaveOnCommand} {this.WeavePattern}[{this.WeaveSchedule}]";
        }
    }
}
