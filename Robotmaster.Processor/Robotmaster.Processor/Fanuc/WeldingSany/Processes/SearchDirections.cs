﻿// <copyright file="SearchDirections.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.Processes
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Fanuc Welding Search Directions.
    /// </summary>
    [DataContract(Name = "SearchDirections")]
    public enum SearchDirections
    {
        /// <summary>
        /// +X.
        /// </summary>
        [EnumMember(Value = "PlusX")]
        PlusX,

        /// <summary>
        /// -X.
        /// </summary>
        [EnumMember(Value = "MinusX")]
        MinusX,

        /// <summary>
        /// +Y.
        /// </summary>
        [EnumMember(Value = "PlusY")]
        PlusY,

        /// <summary>
        /// -Y.
        /// </summary>
        [EnumMember(Value = "MinusY")]
        MinusY,

        /// <summary>
        /// +Z.
        /// </summary>
        [EnumMember(Value = "PlusZ")]
        PlusZ,

        /// <summary>
        /// -Z.
        /// </summary>
        [EnumMember(Value = "MinusZ")]
        MinusZ,
    }
}
