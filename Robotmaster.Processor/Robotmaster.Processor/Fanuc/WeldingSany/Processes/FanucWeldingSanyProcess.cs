﻿// <copyright file="FanucWeldingSanyProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Fanuc.WeldingSany.MainProcessor;
    using Robotmaster.Processor.Fanuc.WeldingSany.PostProcessor;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Fanuc welding Sany process.
    /// </summary>
    [DataContract(Name = "FanucWeldingSanyProcess")]
    public class FanucWeldingSanyProcess : PackageProcess, IFanucProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucWeldingSanyProcess"/> class.
        /// </summary>
        public FanucWeldingSanyProcess()
        {
            this.Name = "Fanuc Welding Sany Process";
            this.PostProcessorType = typeof(PostProcessorFanucWeldingSany);
            this.MainProcessorType = typeof(MainProcessorFanucWeldingSany);
            this.AddEventToMet(typeof(WeldOn));
            this.AddEventToMet(typeof(WeldOff));
            this.AddEventToMet(typeof(WeaveOn));
            this.AddEventToMet(typeof(WeaveOff));
            this.AddEventToMet(typeof(SeamTrackingOn));
            this.AddEventToMet(typeof(SeamTrackingOff));
            this.AddEventToMet(typeof(SearchOn));
            this.AddEventToMet(typeof(SearchOff));
            this.AddEventToMet(typeof(SearchDirection));
            this.ProcessMenuFileName = "FanucWeldingSanyProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
