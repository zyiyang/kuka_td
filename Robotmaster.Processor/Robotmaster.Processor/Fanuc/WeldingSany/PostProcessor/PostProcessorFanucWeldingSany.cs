﻿// <copyright file="PostProcessorFanucWeldingSany.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.PostProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Fanuc.Default.PostProcessor;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="PostProcessorFanuc"/>.
    ///     <para>
    ///         Implements the Fanuc welding dedicated properties and methods of the post-processor in order to output Fanuc welding robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorFanucWeldingSany : PostProcessorFanuc
    {
        /// <inheritdoc />
        internal override void StartMainPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            base.StartMainPostFile(postFile, operationNode, point);

            if (!string.IsNullOrEmpty(FanucWeldingSanyProcessMenu.GetApplicationName(this.ProgramNode)))
            {
                postFile.FileSection.Header += "/APPL" + "\r\n" +
                                               "  " + FanucWeldingSanyProcessMenu.GetApplicationName(this.ProgramNode) + ";" + "\r\n";
            }
        }

        /// <inheritdoc />
        internal override void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            base.StartPostFile(postFile, operationNode, point);

            if (!string.IsNullOrEmpty(FanucWeldingSanyProcessMenu.GetApplicationName(this.ProgramNode)))
            {
                postFile.FileSection.Header += "/APPL" + "\r\n" +
                                               "  " + FanucWeldingSanyProcessMenu.GetApplicationName(this.ProgramNode) + ";" + "\r\n";
            }
        }

        /// <inheritdoc />
        internal override string FormatFeed(PathNode point, CbtNode operationNode)
        {
            // Weld feed output only during weld
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            if ((applicationType == ApplicationType.Welding || (applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false))
                && point.PathFlag() != PathFlag.OutsideOfProcess
                && FanucWeldingSanyProcessMenu.GetWeldSpeedOutputMethod(this.ProgramNode) == 0)
            {
                return " " + "WELD_SPEED";
            }

            return base.FormatFeed(point, operationNode);
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            PathNode previousPoint = point.PreviousPoint(operationNode);

            // Check if the previous point is a Search Point. If so, we will skip its output
            if (previousPoint?.Events()?.EventListInline != null)
            {
                foreach (Event eventInline in previousPoint.Events().EventListInline)
                {
                    if (eventInline is SearchDirection)
                    {
                        // Rewind the point number and skip point output
                        this.PointNumberInProgram--;

                        return;
                    }
                }
            }

            var forceFineTermination = false;

            // Check if current point is a Search Point. If so, force FINE output
            if (point.Events()?.EventListInline != null)
            {
                foreach (Event eventInline in point.Events().EventListInline)
                {
                    if (eventInline is SearchDirection)
                    {
                        forceFineTermination = true;
                    }
                }
            }

            // Check if current point is an Arc Start or Arc End. If so, force FINE output
            if (point.Events()?.EventListAfter != null)
            {
                foreach (Event eventAfter in point.Events().EventListAfter)
                {
                    if (eventAfter is WeldOn ||
                        eventAfter is WeldOff)
                    {
                        forceFineTermination = true;
                    }
                }
            }

            if (forceFineTermination)
            {
                //// Moves
                //// Example:
                ////    L P[4] 125 mm/sec FINE Search[X] ;

                this.LineNumberOfCurrentFile++;
                this.CurrentMoveSectionWriter.Write(this.LineNumberOfCurrentFile + ":");
                this.CurrentMoveSectionWriter.Write(this.FormatMotionType(point));
                this.CurrentMoveSectionWriter.Write(" " + this.FormatPointNumber(this.PointNumberInProgram));
                this.CurrentMoveSectionWriter.Write(this.FormatFeed(point, operationNode));
                this.CurrentMoveSectionWriter.Write(" FINE");
                this.CurrentMoveSectionWriter.Write(this.FormatAcceleration(point, operationNode));
                this.CurrentMoveSectionWriter.Write(this.FormatExtraMotionInstructions(point, operationNode));
                this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
                this.CurrentMoveSectionWriter.WriteLine(" ;");

                //// Positions
                //// Example:
                ////     P[2]{
                ////     GP1:
                ////     UF: 1, UT: 1,         CONFIG: 'N U T, 0, 0, 0',
                ////           X = -5.00  mm,       Y = -15.00  mm,       Z = 100.00  mm,
                ////          W = 180.00 deg,       P = 0.00 deg,       R = 0.00 deg,
                ////          E1 = -179.5206 deg,       E2 = 0.00 deg
                ////     };

                this.CurrentPositionSectionWriter.WriteLine(this.FormatPointNumber(this.PointNumberInProgram) + "{");
                this.CurrentPositionSectionWriter.WriteLine(this.FormatMotionGroupNumber(this.RobotMotionGroupNumber));
                this.CurrentPositionSectionWriter.WriteLine(this.FormatPositionUfAndUt(operationNode) + ",\t" + this.FormatConfig(point));
                this.CurrentPositionSectionWriter.Write(this.FormatPosition(point, operationNode));
                this.CurrentPositionSectionWriter.Write(this.FormatOrientation(point, operationNode));
                this.CurrentPositionSectionWriter.Write(this.FormatExternalAxesValues(point, operationNode));
                this.CurrentPositionSectionWriter.WriteLine("\r\n" + "};");

                return;
            }

            // Regular Linear Output
            base.OutputLinearMove(point, operationNode);
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            if (OperationManager.GetPreviousOperationNodeInProgram(operationNode) == null)
            {
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " CALL QQ0001 ;");
            }

            base.RunBeforeOperationOutput(operationNode);

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " DO[22:Jia si]=ON  ;");
            }
        }

        /// <inheritdoc/>
        internal override void RunAfterOperationOutput(CbtNode operationNode)
        {
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                this.CurrentMoveSectionWriter.WriteLine(++this.LineNumberOfCurrentFile + ":" + " DO[22:Jia si]=OFF  ;");
            }

            base.RunAfterOperationOutput(operationNode);
        }
    }
}