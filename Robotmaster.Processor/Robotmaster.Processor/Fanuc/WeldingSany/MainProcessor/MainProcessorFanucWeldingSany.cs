﻿// <copyright file="MainProcessorFanucWeldingSany.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.MainProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.Fanuc.Default.MainProcessor;
    using Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucTouchSensing;
    using Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucWelding;
    using Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucWeldingWithTouchSensing;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes.Events;
    using Robotmaster.Processor.Fanuc.WeldingSany.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="MainProcessorFanuc" />.
    ///     <para>
    ///         Implements the Fanuc Welding dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorFanucWeldingSany : MainProcessorFanuc
    {
        /// <summary>
        /// Gets or sets the cached touch sensing registry number.
        /// </summary>
        internal int CurrentTouchSensingRegistryNumber { get; set; }

        /// <summary>
        /// Gets or sets the current touch group.
        /// </summary>
        internal int CurrentTouchGroup { get; set; }

        /// <summary>
        /// Gets or sets the current touch.
        /// </summary>
        internal int CurrentTouch { get; set; }

        /// <summary>
        /// Gets or sets the operation touch counts.
        /// </summary>
        internal List<int> OperationTouchGroupCounts { get; set; }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            base.EditOperationBeforeAllPointEdits(operationNode);

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing ||
                OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                this.EditTouchSensingOperation(operationNode);
            }
        }

        /// <inheritdoc/>
        internal override void EditOperationAfterAllPointEdits(CbtNode operationNode)
        {
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding ||
                OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                this.EditWeldingOperation(operationNode);
            }
        }

        /// <summary>
        ///     Gets the next touch offset registry number to use for the current touch sensing operation.
        /// </summary>
        /// <param name="operationNode">The current operation which needs a new touch offset registry number.</param>
        /// <returns>The calculated touch offset registry number.</returns>
        internal virtual int GetNextAvailableTouchOffsetRegistryNumber(CbtNode operationNode)
        {
            for (int i = ProgramManager.GetOperationNodes(this.ProgramNode).ToList().IndexOf(operationNode) - 1; i >= 0; i--)
            {
                CbtNode operation = ProgramManager.GetOperationNodes(this.ProgramNode).ElementAt(i);

                for (PathNode point = OperationManager.GetLastPoint(operation); point != null; point = point.PreviousPoint(operation))
                {
                    if (point.Events()?.EventListInline != null)
                    {
                        foreach (Event eventInline in point.Events().EventListInline)
                        {
                            if (eventInline is SearchOn mySearchOn)
                            {
                                return mySearchOn.TouchSensingPositionRegistry + 1;
                            }
                        }
                    }
                }
            }

            return -1;
        }

        /// <summary>
        ///     Edit the current touch sensing operation.
        /// </summary>
        /// <param name="operationNode">Current touch sensing operation.</param>
        internal void EditTouchSensingOperation(CbtNode operationNode)
        {
            if (FanucWeldingSanyProcessMenu.GetTouchOffsetNumberingMethod(this.ProgramNode) == 1)
            {
                this.CurrentTouchSensingRegistryNumber =
                    this.GetNextAvailableTouchOffsetRegistryNumber(operationNode) == -1
                        ? FanucWeldingSanyProcessMenu.GetTouchOffsetRegistryStartingNumber(this.ProgramNode)
                        : this.GetNextAvailableTouchOffsetRegistryNumber(operationNode);
            }
            else
            {
                this.CurrentTouchSensingRegistryNumber = FanucWeldingSanyProcessMenu.GetTouchOffsetRegistryStartingNumber(this.ProgramNode);
            }

            this.CurrentTouchGroup = 0;
            this.CurrentTouch = 0;

            this.OperationTouchGroupCounts = new List<int>();

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                this.OperationTouchGroupCounts.Add(FanucTouchSensingAuxiliaryMenu.GetSearchGroup1TouchCount(operationNode));
                this.OperationTouchGroupCounts.Add(FanucTouchSensingAuxiliaryMenu.GetSearchGroup2TouchCount(operationNode));
                this.OperationTouchGroupCounts.Add(FanucTouchSensingAuxiliaryMenu.GetSearchGroup3TouchCount(operationNode));
            }
            else //// Welding with Touch Sensing
            {
                this.OperationTouchGroupCounts.Add(FanucWeldingWithTouchSensingAuxiliaryMenu.GetSearchGroup1TouchCount(operationNode));
                this.OperationTouchGroupCounts.Add(FanucWeldingWithTouchSensingAuxiliaryMenu.GetSearchGroup2TouchCount(operationNode));
                this.OperationTouchGroupCounts.Add(FanucWeldingWithTouchSensingAuxiliaryMenu.GetSearchGroup3TouchCount(operationNode));
            }

            this.OperationTouchGroupCounts.RemoveAll(c => c == 0);
        }

        /// <summary>
        /// Edit a Welding operation.
        /// </summary>
        /// <param name="operationNode">Welding operationNode.</param>
        internal virtual void EditWeldingOperation(CbtNode operationNode)
        {
            // Get the Touch Sensing Operation Node
            CbtNode touchSensingOperationNode;

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding && OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot) != null)
            {
                touchSensingOperationNode = OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot);

                var touchSensingFound = false;

                foreach (CbtNode operation in ProgramManager.GetOperationNodes(this.ProgramNode))
                {
                    if (operation == touchSensingOperationNode)
                    {
                        touchSensingFound = true;
                        break;
                    }

                    if (operation == operationNode)
                    {
                        break;
                    }
                }

                if (!touchSensingFound)
                {
                    this.NotifyUser($"Warning in {OperationManager.GetOperationName(operationNode)}: {OperationManager.GetOperationName(touchSensingOperationNode)} is referenced before being called in {ProgramManager.GetProgramName(this.ProgramNode)}");
                }
            }
            else if (OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                touchSensingOperationNode = operationNode;
            }
            else
            {
                touchSensingOperationNode = null;
            }

            // For the Touch Sensing Operation Node, populate the Touch Search Group List
            var touchSearchGroupsList = new List<TouchSearchGroup>();
            var currentSearchGroupNumber = 0;

            if (touchSensingOperationNode != null)
            {
                for (PathNode point = OperationManager.GetFirstPoint(touchSensingOperationNode); point != null; point = point.NextPoint(touchSensingOperationNode))
                {
                    if (point.Events()?.EventListInline != null)
                    {
                        foreach (Event eventInline in point.Events().EventListInline)
                        {
                            if (eventInline is SearchDirection mySearchDirection)
                            {
                                if (currentSearchGroupNumber != mySearchDirection.TouchSensingPositionRegistry)
                                {
                                    touchSearchGroupsList.Add(new TouchSearchGroup());
                                    currentSearchGroupNumber = mySearchDirection.TouchSensingPositionRegistry;
                                }

                                TouchSearchGroup currentSearchGroup = touchSearchGroupsList[touchSearchGroupsList.Count - 1];
                                Vector3 pointInUserFrame = point.PathPointFrameInUserFrame(touchSensingOperationNode, this.SceneCbtRoot).Position;
                                Vector3 nextPointInUserFrame = point.NextPoint(touchSensingOperationNode).PathPointFrameInUserFrame(touchSensingOperationNode, this.SceneCbtRoot).Position;

                                currentSearchGroup.TouchSensingPositionRegistry = currentSearchGroupNumber;
                                currentSearchGroup.TouchPoints.Add(nextPointInUserFrame);
                                currentSearchGroup.TouchDirections.Add((nextPointInUserFrame - pointInUserFrame).GetNormalized());
                                currentSearchGroup.AveragePoint = ((currentSearchGroup.AveragePoint * (currentSearchGroup.TouchPoints.Count - 1)) + nextPointInUserFrame) / currentSearchGroup.TouchPoints.Count;
                                currentSearchGroup.ReferencePoint = this.GetReferencePoint(currentSearchGroup);
                            }
                        }
                    }
                }
            }

            // Using Touch Search Groups, add Correction Activations
            var addCorrectionOnEvent = false;
            var addCorrectionOffEvent = false;
            var isCorrectionOn = false;
            int currentGroupIndex = -1;

            if (touchSearchGroupsList.Count > 0)
            {
                for (PathNode point = OperationManager.GetFirstPoint(operationNode); point != null; point = point.NextPoint(operationNode))
                {
                    if (point.Events()?.EventListAfter != null)
                    {
                        foreach (Event eventAfter in point.Events().EventListAfter)
                        {
                            switch (eventAfter)
                            {
                                case WeldOn _:
                                    addCorrectionOnEvent = true;
                                    isCorrectionOn = true;
                                    break;
                                case WeldOff _:
                                    addCorrectionOffEvent = true;
                                    isCorrectionOn = false;
                                    break;
                            }
                        }

                        if (addCorrectionOnEvent)
                        {
                            currentGroupIndex = this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList);

                            TouchSearchGroup myTouchSearchGroup = touchSearchGroupsList[currentGroupIndex];

                            point = PointManager.AddEventToPathPoint(
                                new TouchOffsetOn
                                {
                                    TouchSensingPositionRegistry = myTouchSearchGroup.TouchSensingPositionRegistry,
                                },
                                EventIndex.Before,
                                point,
                                operationNode);

                            addCorrectionOnEvent = false;
                        }

                        if (addCorrectionOffEvent)
                        {
                            point = PointManager.AddEventToPathPoint(
                                new TouchOffsetOff
                                {
                                },
                                EventIndex.After,
                                point,
                                operationNode);

                            addCorrectionOffEvent = false;
                        }
                    }

                    if (!point.IsArcMiddlePoint() && isCorrectionOn)
                    {
                        if (currentGroupIndex != this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList))
                        {
                            currentGroupIndex = this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList);

                            TouchSearchGroup myTouchSearchGroup = touchSearchGroupsList[currentGroupIndex];

                            point = PointManager.AddEventToPathPoint(
                                new TouchOffsetOff
                                {
                                },
                                EventIndex.Before,
                                point,
                                operationNode);

                            point = PointManager.AddEventToPathPoint(
                                new TouchOffsetOn
                                {
                                    TouchSensingPositionRegistry = myTouchSearchGroup.TouchSensingPositionRegistry,
                                },
                                EventIndex.Before,
                                point,
                                operationNode);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating the Reference Point for perpendicular 2D and 3D touches.
        /// </summary>
        /// <param name="touchSearchGroup">The Touch Search Group.</param>
        /// <returns>The Reference Point.</returns>
        internal Vector3 GetReferencePoint(TouchSearchGroup touchSearchGroup)
        {
            int touchCount = touchSearchGroup.TouchPoints.Count;

            if (touchCount == 2 || touchCount == 3)
            {
                Vector3 firstTouchDirection, secondTouchDirection, thirdTouchDirection;

                Vector3 firstTouchPoint, secondTouchPoint, thirdTouchPoint;

                if (touchCount == 2)
                {
                    firstTouchDirection = touchSearchGroup.TouchDirections[0];
                    secondTouchDirection = touchSearchGroup.TouchDirections[1];
                    thirdTouchDirection = Vector3.CrossProduct(firstTouchDirection, secondTouchDirection);

                    if (thirdTouchDirection.Length < 0.01)
                    {
                        return touchSearchGroup.AveragePoint;
                    }

                    thirdTouchDirection.Normalize();

                    firstTouchPoint = touchSearchGroup.TouchPoints[0];
                    secondTouchPoint = touchSearchGroup.TouchPoints[1];
                    thirdTouchPoint = touchSearchGroup.TouchPoints[0];
                }
                else
                {
                    firstTouchDirection = touchSearchGroup.TouchDirections[0];
                    secondTouchDirection = touchSearchGroup.TouchDirections[1];
                    thirdTouchDirection = touchSearchGroup.TouchDirections[2];

                    if (Vector3.CrossProduct(firstTouchDirection, secondTouchDirection).Length < 0.01 ||
                        Vector3.CrossProduct(firstTouchDirection, thirdTouchDirection).Length < 0.01 ||
                        Vector3.CrossProduct(secondTouchDirection, thirdTouchDirection).Length < 0.01)
                    {
                        return touchSearchGroup.AveragePoint;
                    }

                    firstTouchPoint = touchSearchGroup.TouchPoints[0];
                    secondTouchPoint = touchSearchGroup.TouchPoints[1];
                    thirdTouchPoint = touchSearchGroup.TouchPoints[2];
                }

                Matrix3X3 mat;
                mat.E11 = firstTouchDirection.X;
                mat.E12 = firstTouchDirection.Y;
                mat.E13 = firstTouchDirection.Z;
                mat.E21 = secondTouchDirection.X;
                mat.E22 = secondTouchDirection.Y;
                mat.E23 = secondTouchDirection.Z;
                mat.E31 = thirdTouchDirection.X;
                mat.E32 = thirdTouchDirection.Y;
                mat.E33 = thirdTouchDirection.Z;
                mat.Inverse();

                var touchVector = new Vector3(Vector3.DotProduct(firstTouchDirection, firstTouchPoint), Vector3.DotProduct(secondTouchDirection, secondTouchPoint), Vector3.DotProduct(thirdTouchDirection, thirdTouchPoint));

                return mat * touchVector;
            }

            // If it's not possible to calculate, return the Average Point
            return touchSearchGroup.AveragePoint;
        }

        /// <summary>
        ///     Gets a value indicating the index of the closest Search Group to the current point.
        /// </summary>
        /// <param name="operationNode">The operation node.</param>
        /// <param name="point">The point.</param>
        /// <param name="touchSearchGroups">The list of touch search groups.</param>
        /// <returns>Index of the closest Search Group.</returns>
        internal int GetClosestSearchGroup(CbtNode operationNode, PathNode point, List<TouchSearchGroup> touchSearchGroups)
        {
            double maxDistance = double.MaxValue;
            int closestIndex = -1;

            for (var i = 0; i < touchSearchGroups.Count; i++)
            {
                double testValue = (point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position - touchSearchGroups[i].ReferencePoint).Length;

                if (testValue < maxDistance)
                {
                    maxDistance = testValue;
                    closestIndex = i;
                }
            }

            return closestIndex;
        }

        /// <inheritdoc/>
        internal override PathNode EditPoint(CbtNode operationNode, PathNode point)
        {
            point = base.EditPoint(operationNode, point);

            PathNode nextPoint = point.NextPoint(operationNode);

            if (nextPoint != null && this.IsTouchPoint(operationNode, nextPoint))
            {
                point = this.EditTouchPoint(operationNode, point);
            }

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            if (applicationType == ApplicationType.Welding)
            {
                //// Adds welding events.
                //// Example :
                ////    21:  Weld Start[1,1];
                ////    22:  Weave Sine[1];
                ////    23:  Track TAST[1] RPM[10];

                // Weld Start[1,1]
                point = PointManager.AddEventToPathPoint(
                    new WeldOn
                    {
                        WeldOnCommand = FanucWeldingSanyProcessMenu.GetWeldOnCommand(this.ProgramNode),
                        WeldScheduleProcess = FanucWeldingAuxiliaryMenu.GetWeldScheduleProcess(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                if (FanucWeldingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1)
                {
                    // Weave Sine[1]
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOn
                        {
                            WeavePattern = (WeaveType)FanucWeldingAuxiliaryMenu.GetWeavePattern(operationNode),
                            WeaveSchedule = FanucWeldingAuxiliaryMenu.GetWeaveSchedule(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }

                if (FanucWeldingAuxiliaryMenu.GetSeamTrackingStatus(operationNode) == 1)
                {
                    // Track TAST[1] RPM[10]
                    point = PointManager.AddEventToPathPoint(
                        new SeamTrackingOn
                        {
                            SeamTrackingNumber = FanucWeldingAuxiliaryMenu.GetSeamTrackingNumber(operationNode),
                            RootPassMemorizationCommand = FanucWeldingSanyProcessMenu.GetRootPassMemorizationCommand(this.ProgramNode),
                            RootPassMemorizationNumber = FanucWeldingAuxiliaryMenu.GetRootPassMemorizationRegistryNumber(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }
            }
            else if (applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false)
            {
                //// Adds welding events.
                //// Example :
                ////    21:  Weld Start[1,1];
                ////    22:  Weave Sine[1];
                ////    23:  Track TAST[1] RPM[10];

                // Weld Start[1,1]
                point = PointManager.AddEventToPathPoint(
                    new WeldOn
                    {
                        WeldOnCommand = FanucWeldingSanyProcessMenu.GetWeldOnCommand(this.ProgramNode),
                        WeldScheduleProcess = FanucWeldingWithTouchSensingAuxiliaryMenu.GetWeldScheduleProcess(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                if (FanucWeldingWithTouchSensingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1)
                {
                    // Weave Sine[1]
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOn
                        {
                            WeavePattern = (WeaveType)FanucWeldingWithTouchSensingAuxiliaryMenu.GetWeavePattern(operationNode),
                            WeaveSchedule = FanucWeldingWithTouchSensingAuxiliaryMenu.GetWeaveSchedule(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }

                if (FanucWeldingWithTouchSensingAuxiliaryMenu.GetSeamTrackingStatus(operationNode) == 1)
                {
                    // Track TAST[1] RPM[10]
                    point = PointManager.AddEventToPathPoint(
                        new SeamTrackingOn
                        {
                            SeamTrackingNumber = FanucWeldingWithTouchSensingAuxiliaryMenu.GetSeamTrackingNumber(operationNode),
                            RootPassMemorizationCommand = FanucWeldingSanyProcessMenu.GetRootPassMemorizationCommand(this.ProgramNode),
                            RootPassMemorizationNumber = FanucWeldingWithTouchSensingAuxiliaryMenu.GetRootPassMemorizationRegistryNumber(operationNode),
                        },
                        EventIndex.After,
                        point,
                        operationNode);
                }
            }

            return point;
        }

        /// <summary>
        ///     Edits touch point.
        ///     <para>This method is called in <see cref="EditPoint"/> when <see cref="MainProcessor.IsTouchPoint"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal PathNode EditTouchPoint(CbtNode operationNode, PathNode point)
        {
            //// Adds search direction event.
            //// Example:
            ////      9:L P[4] 125 mm/sec FINE Search[X] ;

            if (this.CurrentTouchGroup <= this.OperationTouchGroupCounts.Count - 1)
            {
                if (this.CurrentTouch <= this.OperationTouchGroupCounts[this.CurrentTouchGroup] - 1)
                {
                    if (this.CurrentTouch == 0)
                    {
                        point = PointManager.AddEventToPathPoint(
                            new SearchOn
                            {
                                SearchScheduleProcess = FanucTouchSensingAuxiliaryMenu.GetSearchSchedule(operationNode),
                                TouchSensingPositionRegistry = this.CurrentTouchSensingRegistryNumber,
                            },
                            EventIndex.Before,
                            point,
                            operationNode);
                    }

                    point = PointManager.AddEventToPathPoint(
                        new SearchDirection
                        {
                            SearchDirectionVector = this.SearchDirectionCalculation(operationNode, point),
                            TouchSensingPositionRegistry = this.CurrentTouchSensingRegistryNumber,
                        },
                        EventIndex.Inline,
                        point,
                        operationNode);

                    if (point.MoveType() != MoveType.Linear)
                    {
                        this.NotifyUser($"Warning in {OperationManager.GetOperationName(operationNode)}: Touch Search point must be Linear");
                    }

                    if (this.CurrentTouch == this.OperationTouchGroupCounts[this.CurrentTouchGroup] - 1)
                    {
                        point = PointManager.AddEventToPathPoint(
                            new SearchOff
                            {
                            },
                            EventIndex.After,
                            point.NextPoint(operationNode, 2),
                            operationNode);

                        this.CurrentTouch = 0;
                        this.CurrentTouchGroup++;

                        if (FanucWeldingSanyProcessMenu.GetTouchOffsetNumberingMethod(this.ProgramNode) == 1)
                        {
                            this.CurrentTouchSensingRegistryNumber++;
                        }
                    }
                    else
                    {
                        this.CurrentTouch++;
                    }
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            // Apply first the FanucMainProcessor method (base class)
            point = base.EditLastPointOfContact(operationNode, point);

            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingWithReferencedTouchSensingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithNoEmbeddedTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingWithReferencedTouchSensingOperation || weldingWithNoEmbeddedTouchSensingOperation)
            {
                //// Adds welding events.
                //// Example :
                ////    225:  Weld End[1,1];
                ////    226:  Weave End;
                ////    227:  Track End;

                // Weld End[1,1]
                point = PointManager.AddEventToPathPoint(
                    new WeldOff
                    {
                        WeldOffCommand = FanucWeldingSanyProcessMenu.GetWeldOffCommand(this.ProgramNode),
                        WeldScheduleProcess = FanucWeldingAuxiliaryMenu.GetWeldScheduleProcess(operationNode),
                    },
                    EventIndex.After,
                    point,
                    operationNode);

                if ((weldingWithReferencedTouchSensingOperation &&
                     FanucWeldingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1)
                    || (weldingWithNoEmbeddedTouchSensingOperation &&
                        FanucWeldingWithTouchSensingAuxiliaryMenu.GetWeavingStatus(operationNode) == 1))
                {
                    // Weave End
                    point = PointManager.AddEventToPathPoint(
                        new WeaveOff(),
                        EventIndex.After,
                        point,
                        operationNode);
                }

                if ((weldingWithReferencedTouchSensingOperation &&
                     FanucWeldingAuxiliaryMenu.GetSeamTrackingStatus(operationNode) == 1)
                    || (weldingWithNoEmbeddedTouchSensingOperation &&
                        FanucWeldingWithTouchSensingAuxiliaryMenu.GetSeamTrackingStatus(operationNode) == 1))
                {
                    // Track End
                    point = PointManager.AddEventToPathPoint(
                        new SeamTrackingOff(),
                        EventIndex.After,
                        point,
                        operationNode);
                }
            }

            return point;
        }

        /// <summary>
        ///     Detect the closest user frame XYZ signed search direction based on the path direction.
        /// </summary>
        /// <param name="operationNode">The operation node.</param>
        /// <param name="point">Search point.</param>
        /// <returns>XYZ signed search direction.</returns>
        internal SearchDirections SearchDirectionCalculation(CbtNode operationNode, PathNode point)
        {
            Vector3 pathDirection = point.NextPoint(operationNode).PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position - point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;
            double[] pathDirectionArray = { Math.Abs(pathDirection.X), Math.Abs(pathDirection.Y), Math.Abs(pathDirection.Z) };
            int index = Array.IndexOf(pathDirectionArray, pathDirectionArray.Max());
            switch (index)
            {
                case 0:
                    return pathDirection.X > 0 ? SearchDirections.PlusX : SearchDirections.MinusX;
                case 1:
                    return pathDirection.Y > 0 ? SearchDirections.PlusY : SearchDirections.MinusY;
                case 2:
                    return pathDirection.Z > 0 ? SearchDirections.PlusZ : SearchDirections.MinusZ;
                default:
                    return SearchDirections.PlusX;
            }
        }
    }
}