﻿// <copyright file="FanucTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucTouchSensing
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "FanucTouchSensingAuxiliary")]
    public class FanucTouchSensingAuxiliary : AuxiliaryMenu, IFanucProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucTouchSensingAuxiliary"/> class.
        /// </summary>
        public FanucTouchSensingAuxiliary()
        {
            this.Name = "Fanuc Touch Sensing";
            this.ApplicationType = ApplicationType.TouchSensing;
            this.AuxiliaryMenuFileName = "FanucTouchSensingAuxiliaryMenu";
        }
    }
}
