﻿// <copyright file="FanucWeldingWithTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucWeldingWithTouchSensing
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "FanucWeldingWithTouchSensingAuxiliary")]
    public class FanucWeldingWithTouchSensingAuxiliary : AuxiliaryMenu, IFanucProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucWeldingWithTouchSensingAuxiliary"/> class.
        /// </summary>
        public FanucWeldingWithTouchSensingAuxiliary()
        {
            this.Name = "Fanuc Welding with Touch Sensing";
            this.ApplicationType = ApplicationType.WeldingWithTouchSensing;
            this.AuxiliaryMenuFileName = "FanucWeldingWithTouchSensingAuxiliaryMenu";
        }
    }
}
