﻿// <copyright file="FanucWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Fanuc.WeldingSany.AuxiliaryMenus.FanucWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "FanucWeldingAuxiliary")]
    public class FanucWeldingAuxiliary : AuxiliaryMenu, IFanucProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FanucWeldingAuxiliary"/> class.
        /// </summary>
        public FanucWeldingAuxiliary()
        {
            this.Name = "Fanuc Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "FanucWeldingAuxiliaryMenu";
        }
    }
}
