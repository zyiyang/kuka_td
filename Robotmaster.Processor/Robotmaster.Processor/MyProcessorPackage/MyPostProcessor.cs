// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyPostProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Robotmaster.Processor.MyProcessorPackage
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Ookii.Dialogs.Wpf;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Path;
    using Robotmaster.Rise.Modules.RiseWindow;
    using Robotmaster.Rise.ProcessorPackage;
    using Robotmaster.Wpf.Controls;

    /// <summary>
    ///     Implements the entry method of the post processor.
    /// </summary>
    public partial class MyProcessorPackageBehavior
    {
        /// <summary>
        ///     Generates the robot code for each given program.
        /// </summary>
        /// <param name="programNodes">
        ///     ProgramNode nodes to use for the generation of robot code.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///    <paramref name="programNodes"/> is <see langword="null" />.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     <paramref name="programNodes"/> is empty.
        /// </exception>
        public void RunPostProcessor(IEnumerable<CbtNode> programNodes)
        {
            if (programNodes == null)
            {
                throw new ArgumentNullException(nameof(programNodes));
            }

            IList<CbtNode> programNodeList = programNodes as IList<CbtNode> ?? programNodes.ToList();
            if (!programNodeList.Any())
            {
                throw new ArgumentException(nameof(programNodes));
            }

            // Post all program independently
            foreach (CbtNode programNode in programNodeList)
            {
                this.PostProcessOneProgram(programNode);
            }
        }

        /// <summary>
        /// Generates the robot code for one program.
        /// </summary>
        /// <param name="programNode">
        ///     ProgramNode node to use for the generation of robot code.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///    <paramref name="programNode"/> is <see langword="null" />.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     <paramref name="programNode"/> is empty.
        /// </exception>
        internal void PostProcessOneProgram(CbtNode programNode)
        {
            // Get the operation list
            IEnumerable<CbtNode> operationNodes = ProgramManager.GetOperationNodes(programNode);
            string programName = ProgramManager.GetProgramName(programNode);

            if (operationNodes == null || !operationNodes.Any())
            {
                // Skip the post-processing of an empty program.
                return;
            }

            // Update the scene for each processor run (per program) for proper data retrieval (point coordinates, part placement, TCP, tool,...)
            SceneManager.SetLayer(operationNodes.First(), this.SceneCbtRoot);

            // Destination folder selection
            DirectoryInfo programDir = null;
            var forceAskOutputDestination = false;
            while (programDir == null)
            {
                // When the destination folder dialog is prompted
                if (Properties.Settings.Default.AlwaysAskOutputDestination || forceAskOutputDestination)
                {
                    var destinationFolderSelectionDialog = new VistaFolderBrowserDialog() { Description = @"Select destination folder", UseDescriptionForTitle = true };
                    if (Directory.Exists(Properties.Settings.Default.DestinationFolder))
                    {
                        // Opens the folder dialog at the current destination folder settings if it exists.
                        // The user could have specified a valid path that doesn't exist yet.
                        destinationFolderSelectionDialog.SelectedPath = new DirectoryInfo(Properties.Settings.Default.DestinationFolder).FullName;
                    }

                    bool? result = destinationFolderSelectionDialog.ShowDialog();
                    if (result != true)
                    {
                        this.Broadcast(RiseBehavior.MessageDisplayMessage, "Posting canceled");
                        return;
                    }

                    Properties.Settings.Default.DestinationFolder = destinationFolderSelectionDialog.SelectedPath;
                }

                // Destination folder test
                try
                {
                    programDir = new DirectoryInfo(Properties.Settings.Default.DestinationFolder);
                }
                catch
                {
                    this.Broadcast(RiseBehavior.MessageDisplayMessage, "Invalid destination folder");

                    // It will loop again and prompt the folder dialog
                    forceAskOutputDestination = true;
                }
            }

            programDir.Create(); // The user could have specified a valid path that doesn't exist yet.
            string programPath = programDir.FullName;

            // Initiate and run PostProcessor
            CbtNode setupNode = ProgramManager.GetSetupNode(programNode);
            Process process = SetupManager.GetConfiguration(setupNode).Process;

            Type postProcessorType;

            // Load post processor if specified by the process
            if (process is PackageProcess packageProcess && packageProcess.PostProcessorType != null)
            {
                // Get main processor type if specified by the process
                postProcessorType = packageProcess.PostProcessorType;
            }
            else
            {
                // Fall back case uses the default brand post processor
                string postProcessorBrandType = string.Empty;
                switch (this.RobotFormatter.Brand.ToUpper())
                {
                    case "FANUC":
                        postProcessorBrandType = "PostProcessorFanuc";

                        if (this.RobotFormatter.BrandSubType == "type=\"Hiwin\"" || this.RobotFormatter.BrandSubType == "Hiwin")
                        {
                            postProcessorBrandType = "PostProcessorHiwin";
                        }

                        break;
                    case "HIWIN":
                        postProcessorBrandType = "PostProcessorHiwin";
                        break;
                    case "KUKA":
                        postProcessorBrandType = "PostProcessorKuka";
                        break;
                    case "MOTOMAN":
                        postProcessorBrandType = "PostProcessorMotoman";
                        break;
                    case "STAUBLI":
                        postProcessorBrandType = "PostProcessorStaubli";

                        if (this.RobotFormatter.BrandSubType == "type=\"Cloos\"" || this.RobotFormatter.BrandSubType == "Cloos")
                        {
                            postProcessorBrandType = "PostProcessorCloos";
                        }

                        break;
                    case "CLOOS":
                        postProcessorBrandType = "PostProcessorCloos";
                        break;
                    case "ABB":
                        postProcessorBrandType = "PostProcessorAbb";
                        break;
                    case "TOSHIBA":
                        postProcessorBrandType = "PostProcessorToshiba";
                        break;
                    case "UNIVERSAL ROBOTS":
                        postProcessorBrandType = "PostProcessorUniversalRobots";
                        break;
                    case "KAWASAKI":
                        postProcessorBrandType = "PostProcessorKawasaki";

                        if (this.RobotFormatter.BrandSubType == "type=\"Igm\"" || this.RobotFormatter.BrandSubType == "Igm")
                        {
                            postProcessorBrandType = "PostProcessorIgm";
                        }

                        break;
                    case "IGM":
                        postProcessorBrandType = "PostProcessorIgm";
                        break;
                    case "OTC DAIHEN":
                        postProcessorBrandType = "PostProcessorOtcDaihen";
                        break;
                    case "NACHI":
                        postProcessorBrandType = "PostProcessorNachi";
                        break;
                }

                // Get the corresponding brand type
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                IEnumerable<Type> postProcessorTypes = myAssembly.GetTypes().Where(p => typeof(PostProcessor).IsAssignableFrom(p));
                postProcessorType = postProcessorTypes.SingleOrDefault(x => x.Name == postProcessorBrandType);
            }

            // Create an instance of the main processor
            if (postProcessorType == null)
            {
                this.Broadcast(RiseBehavior.MessageDisplayMessage, "The code generation for this brand is not supported by the processor package currently loaded.");
                return;
            }

            var postProcessor = (PostProcessor)Activator.CreateInstance(postProcessorType);

            // Defines entry parameter of the post processor
            postProcessor.OperationCbtRoot = this.OperationCbtRoot;
            postProcessor.SceneCbtRoot = this.SceneCbtRoot;
            postProcessor.RobotFormatter = this.RobotFormatter;
            postProcessor.ProcessorPackageBehavior = this;

            // Runs the post processor
            postProcessor.Run(programNode);

            // Outputs pathx files
            if (Properties.Settings.Default.OutputPathX)
            {
                var i = 0;
                foreach (CbtNode operationNode in operationNodes)
                {
                    PathImporter.ExportPath(Path.Combine(programPath, programName + i + ".pathx"), i, PathType.Path5X, OperationManager.GetPathEngine(operationNode));
                    i++;
                }
            }

            // Open the text editor (using file framework 3)
            if (postProcessor.MainPostFile != null)
            {
                string mainPostFileFullPath = Path.Combine(programPath, postProcessor.MainPostFile.RelativePath, postProcessor.MainPostFile.FileName + postProcessor.MainPostFile.FileExtension);
                if (Properties.Settings.Default.OpenEditor && File.Exists(mainPostFileFullPath))
                {
                    try
                    {
                        var editorInfo = new System.Diagnostics.ProcessStartInfo
                        {
                            FileName = Properties.Settings.Default.EditorApplication,
                            Arguments = "\"" + Path.Combine(mainPostFileFullPath) + "\"",
                        };
                        System.Diagnostics.Process.Start(editorInfo);
                    }
                    catch
                    {
                        MessageWindow.Show("Couldn't open " + Properties.Settings.Default.EditorApplication, ProcessorResources.MyProcessorPackage_Okay);
                    }
                }
            }
        }
    }
}