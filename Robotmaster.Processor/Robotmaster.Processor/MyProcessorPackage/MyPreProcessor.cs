﻿// <copyright file="MyPreProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.MyProcessorPackage
{
    using System;
    using System.Collections.Generic;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Data;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Operation.Types;

    /// <summary>
    ///     Implements the entry method of the pre-processor.
    /// </summary>
    public partial class MyProcessorPackageBehavior
    {
        /// <summary>
        ///     Runs the pre-operation.
        /// </summary>
        /// <param name="operation"> operation node. </param>
        public void RunPreProcessor(CbtNode operation)
        {
        }

        /// <summary>
        /// Adds a linear move from dept to retract.
        /// Assumptions :
        ///     1) There is only one linear move which is the actual drilling motion, the rest is given by CAM system as rapid moves
        ///     2) Does not check if points are already added!.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        private static void LonghandSimpleDrill(PathEngine pathEngine)
        {
            foreach (PathNode pathNode in pathEngine)
            {
                if (pathNode.MoveType() == MoveType.Linear)
                {
                    PathNode previousNode = pathEngine.FindNode(pathNode, -1, pathEngine.CurrentStageKey);
                    if (previousNode == null)
                    {
                        throw new InvalidOperationException("Imported path does not obey the joint-joint-linear-joint move sequence for drilling");
                    }

                    PathNode nextNode = pathEngine.FindNode(pathNode, 1, pathEngine.CurrentStageKey);
                    if (nextNode == null)
                    {
                        throw new InvalidOperationException("Imported path does not obey the joint-joint-linear-joint move sequence for drilling");
                    }

                    if (previousNode.MoveType() != MoveType.Linear)
                    {
                        PathNode newNode = pathEngine.CreateNode(pathEngine.CurrentStageKey);
                        CopyPathNode(previousNode, newNode);
                        ((IPathMotion)newNode).MoveType = MoveType.Linear; // If the feed needs to be updated, do it here
                        pathEngine.InsertBefore(nextNode, newNode, pathEngine.CurrentStageKey);
                    }
                }
            }
        }

        /// <summary>
        ///     Convert canned simple drill cycles to longhand cycles. Only drill points (at depth) should be in the pathEngine
        ///     TODO : Recheck the sign of Depth.
        /// </summary>
        /// <param name="pathEngine"> Path engine.</param>
        /// <param name="cycleOperationData"> Drilling operation data. </param>
        private static void CannedSimpleDrill(PathEngine pathEngine, CycleApplicationData cycleOperationData)
        {
            int numberOfNodes = pathEngine.CurrentCount;
            PathNode pathNode = pathEngine.FindNode(0, pathEngine.CurrentStageKey);

            for (var i = 0; i < numberOfNodes; i++)
            {
                ((IPathMotion)pathNode).MoveType = MoveType.Linear; // it comes as JC from PathX

                if (!cycleOperationData.UseClearanceAtPathStartAndEndOnly)
                {
                    PathNode newNodeClearenceBefore = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid);
                    pathEngine.InsertBefore(pathNode, newNodeClearenceBefore, pathEngine.CurrentStageKey);
                }

                PathNode newNodeRetractBefore = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Rapid);
                pathEngine.InsertBefore(pathNode, newNodeRetractBefore, pathEngine.CurrentStageKey);

                PathNode newNodeDepth = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.DepthHeight, MoveType.Linear);
                pathEngine.InsertBefore(pathNode, newNodeDepth, pathEngine.CurrentStageKey);

                PathNode nextpathNode = pathEngine.FindNode(pathNode, 1, pathEngine.CurrentStageKey);

                if (nextpathNode == null)
                {
                    PathNode newNodeRetractAfter = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Linear);
                    pathEngine.Add(newNodeRetractAfter, pathEngine.CurrentStageKey);

                    if (!cycleOperationData.UseClearanceAtPathStartAndEndOnly)
                    {
                        PathNode newNodeClearenceAfter = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid);
                        pathEngine.Add(newNodeClearenceAfter, pathEngine.CurrentStageKey);
                    }
                }
                else
                {
                    PathNode newNodeRetractAfter = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Linear);
                    pathEngine.InsertBefore(nextpathNode, newNodeRetractAfter, pathEngine.CurrentStageKey);

                    if (!cycleOperationData.UseClearanceAtPathStartAndEndOnly)
                    {
                        PathNode newNodeClearenceAfter = CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid);
                        pathEngine.InsertBefore(nextpathNode, newNodeClearenceAfter, pathEngine.CurrentStageKey);
                    }

                    pathEngine.Remove(pathNode, pathEngine.CurrentStageKey); // we do not need contact point in drilling
                    pathNode = nextpathNode;
                }
            }
        }

        /// <summary>
        ///     Convert canned peck drill cycles to longhand cycles. Only drill points (at contact) should be in the pathEngine
        ///     options :   1) simple peck drill
        ///                 2) peck drill with chip break (will be activated if chip break value is greater than zero)
        ///                 3) peck drill with return to retract (if return to retract is activated, it overwrites the chip break)
        ///     Additional parameter : Return to clearance after every drill cycle.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="cycleOperationData"> Drilling operation data. </param>
        /// <param name="isReturnToRetract"> Flag indicates is return to retract. </param>
        /// <param name="chipBreakDepth"> Chip break depth. </param>
        /// <param name="peckDepth"> Peck depth. </param>
        private static void CannedDeepHoleDrill(PathEngine pathEngine, CycleApplicationData cycleOperationData, bool isReturnToRetract, double chipBreakDepth, double peckDepth)
        {
            int numberOfNodes = pathEngine.CurrentCount;
            PathNode pathNode = pathEngine.FindNode(0, pathEngine.CurrentStageKey);

            for (var i = 0; i < numberOfNodes; i++)
            {
                PathNode nextpathNode = pathEngine.FindNode(pathNode, 1, pathEngine.CurrentStageKey);

                // Define a list to store the created pathNodes
                var pathNodeList = new List<PathNode>();

                if (i == 0)
                {
                    // first point has to have clearance
                    pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid)); // fast
                }

                if (i != 0 && !cycleOperationData.UseClearanceAtPathStartAndEndOnly)
                {
                    pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid)); // fast
                }

                pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Rapid)); // fast

                double peckNumber = System.Math.Ceiling((cycleOperationData.RetractHeight - cycleOperationData.DepthHeight) / peckDepth) - 1;

                for (var j = 1; j <= peckNumber; j++)
                {
                    pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -(cycleOperationData.RetractHeight - (j * peckDepth)), MoveType.Linear)); // slow

                    if (isReturnToRetract)
                    {
                        pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Linear)); // fast
                        pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -(cycleOperationData.RetractHeight - (j * peckDepth)), MoveType.Linear)); // fast
                    }
                    else if (!chipBreakDepth.Equals(0.0))
                    {
                        pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -(cycleOperationData.RetractHeight - (j * peckDepth) + chipBreakDepth), MoveType.Linear)); // slow
                        pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -(cycleOperationData.RetractHeight - (j * peckDepth)), MoveType.Linear)); // fast
                    }
                }

                pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.DepthHeight, MoveType.Linear)); // slow
                pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.RetractHeight, MoveType.Linear)); // fast

                if (nextpathNode != null && !cycleOperationData.UseClearanceAtPathStartAndEndOnly)
                {
                    pathNodeList.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid)); // fast
                }

                pathEngine.InsertRangeBefore(pathNode, pathNodeList, pathEngine.CurrentStageKey);

                if (nextpathNode == null)
                {
                    pathEngine.Add(CreateNodeAlongToolVector(pathEngine, pathNode, -cycleOperationData.ClearanceHeight, MoveType.Rapid), pathEngine.CurrentStageKey); // fast
                    pathEngine.Remove(pathNode, pathEngine.CurrentStageKey);
                    return;
                }

                pathEngine.Remove(pathNode, pathEngine.CurrentStageKey); // be careful - what if the node has other objects other than path5xElement??
                pathNode = nextpathNode;
            }
        }

        // internal static bool IsNotPassedDepth(PathNode bottom, PathNode top)
        // {
        //    var topPoint = (Path5XElement)top;
        //    var bottomPoint = (Path5XElement)bottom;
        //    return System.Math.Sign(bottomPoint.X - topPoint.X) == System.Math.Sign(bottomPoint.I) && System.Math.Sign(bottomPoint.Y - topPoint.Y) == System.Math.Sign(bottomPoint.J) && System.Math.Sign(bottomPoint.Z - topPoint.Z) == System.Math.Sign(bottomPoint.K);
        // }

        /// <summary>
        ///     Creates a new path node distance away from referenceNode along referenceNode vector.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="referenceNode"> Reference node. </param>
        /// <param name="distance"> Distance. </param>
        /// <param name="moveType"> Move type. </param>
        /// <returns> Path node. </returns>
        private static PathNode CreateNodeAlongToolVector(PathEngine pathEngine, PathNode referenceNode, double distance, MoveType moveType)
        {
            PathNode newNode = pathEngine.CreateNode(pathEngine.CurrentStageKey);
            CopyPathNode(referenceNode, newNode);

            Vector3 direction = ((IPathToolDirection)referenceNode).ToolDirection;
            ((IPathPosition)newNode).Position = ((IPathPosition)referenceNode).Position + (direction * distance);
            ((IPathMotion)newNode).MoveType = moveType;
            return newNode;
        }

        /// <summary>
        ///     Creates a new path node distance away from referenceNode along a given vector.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="referenceNode"> Reference node. </param>
        /// <param name="distance"> Distance. </param>
        /// <param name="vector"> Vector. </param>
        /// <param name="moveType"> Move type. </param>
        /// <returns> Path node. </returns>
        private static PathNode CreateNodeAlongVector(PathEngine pathEngine, PathNode referenceNode, double distance, Vector3 vector, MoveType moveType)
        {
            PathNode newNode = pathEngine.CreateNode(pathEngine.CurrentStageKey);
            CopyPathNode(referenceNode, newNode);

            vector = vector.GetNormalized();

            ((IPathPosition)newNode).Position = ((IPathPosition)referenceNode).Position + (vector * distance);
            ((IPathMotion)newNode).MoveType = moveType;

            return newNode;
        }

        /// <summary>
        ///     Creates a new path node at the depth of the canned operation.
        ///     This method should always be called for the functions that accepts all the pathEngine points at depth.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="cycleOperationData"> Canned operation data. </param>
        private static void UpdatePathEngineAsAllPointsAtDepth(PathEngine pathEngine, CycleApplicationData cycleOperationData)
        {
            // Here we do not add/remove any points, we just update them!!
            // Normalize the vectors.
            List<Vector3> plungeVectors;

            // The points in pathEngine are w.r.t. clearance
            if (cycleOperationData.ReferencePoint == CyclePointReference.Clearance)
            {
                foreach (PathNode node in pathEngine)
                {
                    plungeVectors = AdjustPlungeVectors(node, cycleOperationData);
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + (cycleOperationData.ClearanceHeight * plungeVectors[0]) + ((cycleOperationData.RetractHeight + cycleOperationData.DepthHeight) * plungeVectors[1]);
                }
            }

            // The points in pathEngine are w.r.t. retract
            if (cycleOperationData.ReferencePoint == CyclePointReference.Retract)
            {
                foreach (PathNode node in pathEngine)
                {
                    plungeVectors = AdjustPlungeVectors(node, cycleOperationData);
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + ((cycleOperationData.RetractHeight + cycleOperationData.DepthHeight) * plungeVectors[1]);
                }
            }

            // The points in pathEngine are w.r.t. contact
            if (cycleOperationData.ReferencePoint == CyclePointReference.Contact)
            {
                foreach (PathNode node in pathEngine)
                {
                    plungeVectors = AdjustPlungeVectors(node, cycleOperationData);
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + (cycleOperationData.DepthHeight * plungeVectors[1]);
                }
            }
        }

        /// <summary>
        ///     Creates a new path node at the contact of the canned operation
        ///     This method should always be called for the functions that accepts all the pathEngine points at contact.
        /// TODO : Implement the plunge vectors.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="cycleOperationData"> Canned operation data. </param>
        private static void UpdatePathEngineAsAllPointsAtContact(PathEngine pathEngine, CycleApplicationData cycleOperationData)
        {
            // Here we do not add/remove any points, we are just updating them!!
            // The points in pathEngine are w.r.t. clearance
            if (cycleOperationData.ReferencePoint == CyclePointReference.Clearance)
            {
                foreach (PathNode node in pathEngine)
                {
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + (cycleOperationData.ClearanceHeight * ((IPathToolDirection)node).ToolDirection);
                }
            }

            // The points in pathEngine are w.r.t. retract
            if (cycleOperationData.ReferencePoint == CyclePointReference.Retract)
            {
                foreach (PathNode node in pathEngine)
                {
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + (cycleOperationData.RetractHeight * ((IPathToolDirection)node).ToolDirection);
                }
            }

            // The points in pathEngine are w.r.t. depth (depth value is always negative)
            if (cycleOperationData.ReferencePoint == CyclePointReference.Depth)
            {
                foreach (PathNode node in pathEngine)
                {
                    ((IPathPosition)node).Position = ((IPathPosition)node).Position + (cycleOperationData.DepthHeight * ((IPathToolDirection)node).ToolDirection);
                }
            }
        }

        /// <summary>
        ///     We expect to get up to 2 plunge vectors.
        /// </summary>
        /// <param name="node"> Path node. </param>
        /// <param name="cycleOperationData"> Canned operation data. </param>
        /// <returns> A list of adjusted plunge vectors. </returns>
        private static List<Vector3> AdjustPlungeVectors(PathNode node, CycleApplicationData cycleOperationData)
        {
            var plungeVectors = new List<Vector3>(); // Get the plunge vectors from Aux menu.
            var vectorList = new List<Vector3>();

            // Get tool vector from the point (in 5x tool paths, tool vector for each point is different)
            // Normalize the vectors.
            Vector3 toolVector = ((IPathToolDirection)node).ToolDirection.GetNormalized();
            Vector3 plungeVector1 = toolVector;
            Vector3 plungeVector2 = toolVector;

            plungeVector1 = plungeVectors[0].GetNormalized();
            vectorList.Add(plungeVector1);

            if (plungeVectors.Count == 2)
            {
                plungeVector2 = plungeVectors[1].GetNormalized();
                vectorList.Add(plungeVector2);
            }

            return vectorList;
        }

        /// <summary>
        ///     Breaks all arcs to lines with a given tolerance
        ///     TODO : Indexing might be problematic, needs to be tested. This is very slow.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="tolLine"> Line tolerance. </param>
        private static void BreakAllArcsIntoLines(PathEngine pathEngine, double tolLine)
        {
            PathNode currentNode = pathEngine.FindNode(0, pathEngine.CurrentStageKey); // first node
            for (var i = 0; i < pathEngine.CurrentCount; i++)
            {
                PathNode previousNode = pathEngine.FindNode(currentNode, -1, pathEngine.CurrentStageKey);
                PathNode nextNode = pathEngine.FindNode(currentNode, 1, pathEngine.CurrentStageKey);

                // Either the first point or the last point of the pathEngine
                // Assumes that any given path never starts with a CC move
                if (previousNode == null)
                {
                    currentNode = nextNode;
                    continue;
                }

                if (nextNode == null)
                {
                    continue;
                }

                if (currentNode.MoveType() == MoveType.Circular && (previousNode.MoveType() == MoveType.Linear || previousNode.MoveType() == MoveType.Rapid) && nextNode.MoveType() == MoveType.Circular)
                {
                    Vector3 posXyzStart = previousNode.Position();
                    Vector3 posXyzMid = currentNode.Position();
                    Vector3 posXyzFin = nextNode.Position();

                    // Move this method to RmMath
                    Interpolator.ArcInterpolation(posXyzStart, posXyzMid, posXyzFin, tolLine, out List<Vector3> interXyzList);

                    foreach (Vector3 interPoint in interXyzList)
                    {
                        PathNode newNode = pathEngine.CreateNode(pathEngine.CurrentStageKey);
                        CopyPathNode(currentNode, newNode);
                        ((IPathPosition)newNode).Position = new Vector3(interPoint[0], interPoint[1], interPoint[2]);
                        ((IPathMotion)newNode).MoveType = MoveType.Linear;

                        pathEngine.InsertBefore(nextNode, newNode, pathEngine.CurrentStageKey);
                    }

                    ((IPathMotion)nextNode).MoveType = MoveType.Linear;
                    pathEngine.Remove(currentNode, pathEngine.CurrentStageKey);
                }

                currentNode = nextNode;
            }
        }

        /// <summary>
        ///     Breaks arcs whose radius are smaller than given radius tolerance into lines with a given line tolerance.
        ///     TODO : Indexing might be problematic, needs to be tested. Also think on merging this method with the BreakAllArcsIntoLines method.
        /// </summary>
        /// <param name="pathEngine"> Path engine. </param>
        /// <param name="tolRadius"> Radius tolerance. </param>
        /// <param name="tolLine"> Line tolerance. </param>
        private static void BreakSmallArcsIntoLines(PathEngine pathEngine, double tolRadius, double tolLine)
        {
            PathNode currentNode = pathEngine.FindNode(0, pathEngine.CurrentStageKey); // first node
            for (var i = 0; i < pathEngine.CurrentCount; i++)
            {
                PathNode previousNode = pathEngine.FindNode(currentNode, -1, pathEngine.CurrentStageKey);
                PathNode nextNode = pathEngine.FindNode(currentNode, 1, pathEngine.CurrentStageKey);

                // Either the first point or the last point of the pathEngine
                // Assumes that any given path never starts with a CC move
                if (previousNode == null)
                {
                    currentNode = nextNode;
                    continue;
                }

                if (nextNode == null)
                {
                    continue;
                }

                if (currentNode.MoveType() == MoveType.Circular && (previousNode.MoveType() == MoveType.Linear || previousNode.MoveType() == MoveType.Rapid) && nextNode.MoveType() == MoveType.Circular)
                {
                    Vector3 posXyzStart = previousNode.Position();
                    Vector3 posXyzMid = currentNode.Position();
                    Vector3 posXyzFin = nextNode.Position();
                    Vector3 pCenter;
                    double radius;
                    Vector3 eP123Unit;
                    double phiVc1ToVc3;
                    double garma12;
                    double phiVc2ToVc3;
                    bool succArc = Interpolator.ArcParametersFromThreePoints(posXyzStart, posXyzMid, posXyzFin, out pCenter, out radius, out eP123Unit, out phiVc1ToVc3, out garma12, out phiVc2ToVc3);
                    if (!succArc)
                    {
                        throw new Exception("the points are collinear");
                    }

                    if (radius < tolRadius)
                    {
                        // Move this method to RmMath
                        Interpolator.ArcInterpolation(posXyzStart, posXyzMid, posXyzFin, tolLine, out List<Vector3> interXyzList);
                        foreach (Vector3 interPoint in interXyzList)
                        {
                            PathNode newNode = pathEngine.CreateNode(pathEngine.CurrentStageKey);
                            CopyPathNode(currentNode, newNode);
                            ((IPathPosition)newNode).Position = new Vector3(interPoint[0], interPoint[1], interPoint[2]);
                            ((IPathMotion)newNode).MoveType = MoveType.Linear;

                            pathEngine.InsertBefore(nextNode, newNode, pathEngine.CurrentStageKey);
                        }

                        ((IPathMotion)nextNode).MoveType = MoveType.Linear;
                        pathEngine.Remove(currentNode, pathEngine.CurrentStageKey);
                    }
                }

                currentNode = nextNode;
            }
        }

        /// <summary>
        ///     Copy the path5XElement object of a node to another node.
        /// </summary>
        /// <param name="referenceNode"> Reference path node. </param>
        /// <param name="newNode"> New path node. </param>
        private static void CopyPathNode(PathNode referenceNode, PathNode newNode)
        {
            ((IPathPosition)newNode).Position = ((IPathPosition)referenceNode).Position;
            ((IPathToolDirection)newNode).ToolDirection = ((IPathToolDirection)referenceNode).ToolDirection;

            ((IPathMotion)newNode).MoveType = ((IPathMotion)referenceNode).MoveType;
            ((IPathMotion)newNode).MoveSpace = ((IPathMotion)referenceNode).MoveSpace;

            ((IPathFeedrate)newNode).LinearFeedrate = ((IPathFeedrate)referenceNode).LinearFeedrate;
            ((IPathFeedrate)newNode).AngularFeedrate = ((IPathFeedrate)referenceNode).AngularFeedrate;

            ((IPathFlags)newNode).PathFlag = ((IPathFlags)referenceNode).PathFlag;
            ((IPathFlags)newNode).IsArcMiddlePoint = ((IPathFlags)referenceNode).IsArcMiddlePoint;
        }

        /// <summary>
        ///     Convert rapid moves to linear moves (in path5x level).
        /// </summary>
        /// <param name="operationNode">operation node.</param>
        private void ConvertJoint2Linear(CbtNode operationNode)
        {
            ////if (TaskManager.IsConvertJointToLinear(operation))
            {
                PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);

                foreach (PathNode pathNode in pathEngine)
                {
                    if (((IPathMotion)pathNode).MoveType == MoveType.Rapid && ((IPathMotion)pathNode).MoveSpace == MoveSpace.Cartesian)
                    {
                        // TODO: feedrate management is required. However, this is a big task that we need to discuss on this first.
                        ((IPathMotion)pathNode).MoveType = MoveType.Linear;
                    }
                }
            }
        }

        /// <summary>
        ///     Main processing for drilling operation.
        /// </summary>
        /// <param name="operationNode">Operation node.</param>
        /// <param name="isLongHand">Long hand cycle.</param>
        /// <param name="peckDepth">Peck depth.</param>
        private void ProcessDrillingOperation(CbtNode operationNode, bool isLongHand, double peckDepth)
        {
            PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);

            // Here do Drilling
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Cycle)
            {
                // Process the canned cycles
                if (OperationManager.GetOperationType(operationNode) == OperationType.Task)
                {
                    // TODO: Get the cycle application data.
                    CycleApplicationData cycleApplicationData = null;

                    // For now, this is how we differentiate between simple canned drill and deep hole canned drill
                    // Simple canned drill
                    if (Math.Abs(peckDepth) < double.Epsilon)
                    {
                        // here the path engine is updated as all the points are w.r.t. depth (required for CannedSimpleDrill)
                        UpdatePathEngineAsAllPointsAtDepth(pathEngine, cycleApplicationData);
                        CannedSimpleDrill(pathEngine, cycleApplicationData);
                    }

                    // Deep Hole canned drill
                    if (Math.Abs(peckDepth) > double.Epsilon)
                    {
                        // here the path engine is updated as all the points are w.r.t. contact (required for CannedDeepHoleDrill)
                        UpdatePathEngineAsAllPointsAtContact(pathEngine, cycleApplicationData);
                        CannedDeepHoleDrill(pathEngine, cycleApplicationData, false, 0.0, 0.0);
                    }
                }

                // Long hand drill cycle
                else
                {
                    // TODO: Remove it once the pathx file does not have the retract point
                    if (isLongHand)
                    {
                        LonghandSimpleDrill(pathEngine);
                    }
                }
            }
        }

        /// <summary>
        /// Main processing for Arc to Line conversion.
        /// </summary>
        /// <param name="operationNode">Process Node.</param>
        /// <param name="lineToleranceForArcs">Line tolerance.</param>
        /// <param name="radiusToleranceForArcs">Radius tolerance.</param>
        /// <param name="isArcToLines">Convert arc to lines.</param>
        private void BreakArcsIntoLines(CbtNode operationNode, double lineToleranceForArcs, double radiusToleranceForArcs, bool isArcToLines)
        {
            PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);
            if (isArcToLines)
            {
                if (Math.Abs(radiusToleranceForArcs) < double.Epsilon)
                {
                    // Break All Arcs into Lines
                    BreakAllArcsIntoLines(pathEngine, lineToleranceForArcs);
                }
                else
                {
                    // Break Small Arcs into Lines
                    BreakSmallArcsIntoLines(pathEngine, radiusToleranceForArcs, lineToleranceForArcs);
                }
            }
        }
    }
}
