﻿// <copyright file="MySimulationProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.MyProcessorPackage
{
    using Robotmaster.ComponentBasedTree;

    /// <summary>
    ///     Implements the entry method of the simulation processor.
    /// </summary>
    public partial class MyProcessorPackageBehavior
    {
        /// <summary>
        ///     Runs the simulation processor.
        /// </summary>
        /// <param name="operation">Operation.</param>
        public void RunSimulationProcessor(CbtNode operation)
        {
        }
    }
}
