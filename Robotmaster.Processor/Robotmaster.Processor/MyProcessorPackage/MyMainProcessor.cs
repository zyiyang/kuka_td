﻿// <copyright file="MyMainProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.MyProcessorPackage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Implements the entry method of the main processor.
    /// </summary>
    public partial class MyProcessorPackageBehavior
    {
        /// <summary>
        ///     Runs the main processor depending on the selected brand and selected process.
        /// </summary>
        /// <param name="process">User defined process.</param>
        /// <param name="operationNode">Operation to be processed.</param>
        public void RunMainProcessor(Process process, CbtNode operationNode)
        {
            Type mainProcessorType = null;

            if (process is PackageProcess packageProcess && packageProcess.PostProcessorType != null)
            {
                // Get main processor type if specified by the process
                mainProcessorType = packageProcess.MainProcessorType;
            }
            else
            {
                // Fall back case uses the default brand main processor
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                IEnumerable<Type> mainProcessorTypes = myAssembly.GetTypes().Where(p => typeof(MainProcessor).IsAssignableFrom(p));

                string mainProcessorBrandType = string.Empty;
                switch (this.RobotFormatter.Brand.ToUpper())
                {
                    case "FANUC":
                        mainProcessorBrandType = "MainProcessorFanuc";

                        if (this.RobotFormatter.BrandSubType == "type=\"Hiwin\"" || this.RobotFormatter.BrandSubType == "Hiwin")
                        {
                            mainProcessorBrandType = "MainProcessorHiwin";
                        }

                        break;
                    case "KAWASAKI":
                        mainProcessorBrandType = "MainProcessorKawasaki";

                        if (this.RobotFormatter.BrandSubType == "type=\"Igm\"" || this.RobotFormatter.BrandSubType == "Igm")
                        {
                            mainProcessorBrandType = "MainProcessorIgm";
                        }

                        break;
                    case "HIWIN":
                        mainProcessorBrandType = "MainProcessorHiwin";
                        break;
                    case "IGM":
                        mainProcessorBrandType = "MainProcessorIgm";
                        break;
                    case "KUKA":
                        mainProcessorBrandType = "MainProcessorKuka";
                        break;
                    case "MOTOMAN":
                        mainProcessorBrandType = "MainProcessorMotoman";
                        break;
                    case "ABB":
                        mainProcessorBrandType = "MainProcessorAbb";
                        break;
                    case "STAUBLI":
                        mainProcessorBrandType = "MainProcessorStaubli";

                        if (this.RobotFormatter.BrandSubType == "type=\"Cloos\"" || this.RobotFormatter.BrandSubType == "Cloos")
                        {
                            mainProcessorBrandType = "MainProcessorCloos";
                        }

                        break;
                    case "CLOOS":
                        mainProcessorBrandType = "MainProcessorCloos";
                        break;
                    case "UNIVERSAL ROBOTS":
                        mainProcessorBrandType = "MainProcessorUniversalRobots";
                        break;
                    case "OTC DAIHEN":
                        mainProcessorBrandType = "MainProcessorOtcDaihen";
                        break;
                    case "NACHI":
                        mainProcessorBrandType = "MainProcessorNachi";
                        break;
                }

                // Get the corresponding brand type
                mainProcessorType = mainProcessorTypes.SingleOrDefault(x => x.Name == mainProcessorBrandType);
            }

            // Create an instance of the main processor
            MainProcessor mainProcessor;
            if (mainProcessorType != null)
            {
                mainProcessor = (MainProcessor)Activator.CreateInstance(mainProcessorType);
            }
            else
            {
                mainProcessor = new MainProcessor();
            }

            mainProcessor.OperationCbtRoot = this.OperationCbtRoot;
            mainProcessor.SceneCbtRoot = this.SceneCbtRoot;
            mainProcessor.RobotFormatter = this.RobotFormatter;
            mainProcessor.ProcessorPackageBehavior = this;
            mainProcessor.Run(operationNode);
        }
    }
}
