﻿// <copyright file="MyProcessorPackageBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.MyProcessorPackage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Implements plug of the processor package.
    /// </summary>
    public partial class MyProcessorPackageBehavior : ProcessorPackageBehavior
    {
        /// <summary>
        ///     Plugs of the processor package behavior.
        /// </summary>
        /// <inheritdoc/>
        protected override void OnPlug()
        {
            // Plug options.
            new ProcessorOptionsBehavior().Plug(this);

            // Assign processors.
            this.ProcessorPackage.PreProcessorDelegate = this.RunPreProcessor;
            this.ProcessorPackage.MainProcessorDelegate = this.RunMainProcessor;
            this.ProcessorPackage.SimulationProcessorDelegate = this.RunSimulationProcessor;
            this.ProcessorPackage.PostProcessorDelegate = this.RunPostProcessor;

            // Add default device menu common to all robot brands
            this.ProcessorPackage.AddDeviceMenu("CommonToolingActivationSettings");

            // Get the list of process
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            IEnumerable<Type> processTypes = myAssembly.GetTypes().Where(p => typeof(Process).IsAssignableFrom(p));
            IEnumerable<Type> auxiliaryMenu = myAssembly.GetTypes().Where(p => typeof(AuxiliaryMenu).IsAssignableFrom(p));

            Type iBrandProcessType = null;

            // Add brand dependent device, process and auxiliary menu(s)
            switch (this.RobotFormatter.Brand.ToUpper())
            {
                case "ABB":
                {
                    this.ProcessorPackage.AddDeviceMenu("AbbProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("AbbMotionSettings");
                    iBrandProcessType = typeof(IAbbProcess);
                    break;
                }

                case "FANUC":
                {
                    this.ProcessorPackage.AddDeviceMenu("FanucProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("FanucMotionSettings");
                    iBrandProcessType = typeof(IFanucProcess);

                    if (this.RobotFormatter.BrandSubType == "type=\"Hiwin\"" || this.RobotFormatter.BrandSubType == "Hiwin")
                    {
                        this.ProcessorPackage.AddDeviceMenu("HiwinMotionSettings");
                        iBrandProcessType = typeof(IHiwinProcess);
                    }

                    break;
                }

                case "HIWIN":
                {
                    this.ProcessorPackage.AddDeviceMenu("HiwinMotionSettings");
                    iBrandProcessType = typeof(IHiwinProcess);
                    break;
                }

                case "KAWASAKI":
                {
                    this.ProcessorPackage.AddDeviceMenu("KawasakiProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("KawasakiMotionSettings");
                    iBrandProcessType = typeof(IKawasakiProcess);

                    if (this.RobotFormatter.BrandSubType == "type=\"Igm\"" || this.RobotFormatter.BrandSubType == "Igm")
                    {
                        this.ProcessorPackage.AddDeviceMenu("IgmProcessorSwitches");
                        this.ProcessorPackage.AddDeviceMenu("IgmMotionSettings");
                        iBrandProcessType = typeof(IIgmProcess);
                    }

                    break;
                }

                case "IGM":
                {
                    this.ProcessorPackage.AddDeviceMenu("IgmProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("IgmMotionSettings");
                    iBrandProcessType = typeof(IIgmProcess);
                    break;
                }

                case "KUKA":
                {
                    this.ProcessorPackage.AddDeviceMenu("KukaProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("KukaMotionSettings");
                    iBrandProcessType = typeof(IKukaProcess);
                    break;
                }

                case "MOTOMAN":
                {
                    this.ProcessorPackage.AddDeviceMenu("MotomanProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("MotomanMotionSettings");
                    iBrandProcessType = typeof(IMotomanProcess);
                    break;
                }

                case "STAUBLI":
                {
                    this.ProcessorPackage.AddDeviceMenu("StaubliMotionSettings");
                    iBrandProcessType = typeof(IStaubliProcess);

                    if (this.RobotFormatter.BrandSubType == "type=\"Cloos\"" || this.RobotFormatter.BrandSubType == "Cloos")
                    {
                        this.ProcessorPackage.AddDeviceMenu("CloosProcessorSwitches");
                        this.ProcessorPackage.AddDeviceMenu("CloosMotionSettings");
                        iBrandProcessType = typeof(ICloosProcess);
                    }

                    break;
                }

                case "CLOOS":
                {
                    this.ProcessorPackage.AddDeviceMenu("CloosProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("CloosMotionSettings");
                    iBrandProcessType = typeof(ICloosProcess);
                    break;
                }

                case "UNIVERSAL ROBOTS":
                {
                    this.ProcessorPackage.AddDeviceMenu("UniversalRobotsMotionSettings");
                    iBrandProcessType = typeof(IUniversalRobotsProcess);
                    break;
                }

                case "OTC DAIHEN":
                {
                    this.ProcessorPackage.AddDeviceMenu("OtcDaihenProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("OtcDaihenMotionSettings");
                    iBrandProcessType = typeof(IOtcDaihenProcess);
                    break;
                }

                case "NACHI":
                {
                    this.ProcessorPackage.AddDeviceMenu("OtcDaihenProcessorSwitches");
                    this.ProcessorPackage.AddDeviceMenu("OtcDaihenMotionSettings");
                    iBrandProcessType = typeof(INachiProcess);
                    break;
                }

                default:
                {
                    break;
                }
            }

            if (iBrandProcessType != null)
            {
                // Add all the brand process
                IEnumerable<Type> brandProcessTypes = processTypes.Where(p => iBrandProcessType.IsAssignableFrom(p));
                foreach (Type brandProcessType in brandProcessTypes)
                {
                    this.ProcessorPackage.AddProcess(brandProcessType);
                }

                // Add all the brand auxiliary menus
                IEnumerable<Type> brandAuxiliaryTypes = auxiliaryMenu.Where(p => iBrandProcessType.IsAssignableFrom(p));
                foreach (Type brandAuxiliaryType in brandAuxiliaryTypes)
                {
                    this.ProcessorPackage.AddAuxiliaryMenu(brandAuxiliaryType);
                }
            }
        }
    }
}
