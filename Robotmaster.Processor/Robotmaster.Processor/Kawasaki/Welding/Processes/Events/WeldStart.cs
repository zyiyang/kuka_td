﻿// <copyright file="WeldStart.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld start event.
    /// </summary>
    [DataContract(Name = "WeldStart")]
    public class WeldStart : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldStart"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldStart()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Gets or sets the weld condition number.
        /// </summary>
        [DataMember(Name = "WeldConditionNumber")]
        public virtual int WeldConditionNumber { get; set; } = 0;

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Weld Start - Condition Number " + this.WeldConditionNumber;
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return string.Empty;
        }
    }
}
