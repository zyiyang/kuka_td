﻿// <copyright file="WeldEnd.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Welding.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     Weld end event.
    /// </summary>
    [DataContract(Name = "WeldEnd")]
    public class WeldEnd : Event
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WeldEnd"/> class.
        ///     Default constructor is required for MET Add.
        /// </summary>
        public WeldEnd()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        ///     Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Weld End";
        }

        /// <summary>The code output format of the event.</summary>
        /// <returns>
        ///     The information that will be output in the robot code (post).
        /// </returns>
        public override string ToCode()
        {
            return string.Empty;
        }
    }
}
