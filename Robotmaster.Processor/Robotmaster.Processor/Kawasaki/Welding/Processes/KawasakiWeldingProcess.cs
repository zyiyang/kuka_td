﻿// <copyright file="KawasakiWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Kawasaki.Welding.MainProcessor;
    using Robotmaster.Processor.Kawasaki.Welding.PostProcessor;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Kawasaki welding process.
    /// </summary>
    [DataContract(Name = "KawasakiWeldingProcess")]
    public class KawasakiWeldingProcess : PackageProcess, IKawasakiProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiWeldingProcess"/> class.
        /// </summary>
        public KawasakiWeldingProcess()
        {
            this.Name = "Kawasaki Welding Process";
            this.PostProcessorType = typeof(PostProcessorKawasakiWelding);
            this.MainProcessorType = typeof(MainProcessorKawasakiWelding);
            this.ProcessMenuFileName = "KawasakiWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
