﻿// <copyright file="MainProcessorKawasakiWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Welding.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Kawasaki.Default.MainProcessor;
    using Robotmaster.Processor.Kawasaki.Welding.Processes.Events;
    using Robotmaster.Processor.Kawasaki.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="MainProcessorKawasaki" />.
    ///     <para>
    ///         Implements the Kawasaki Welding dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorKawasakiWelding : MainProcessorKawasaki
    {
        /// <summary>
        ///     Gets or sets the highest used weld condition number.
        /// </summary>
        internal virtual int HighestUsedWeldConditionNumber { get; set; }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            // Reset weld condition number
            this.HighestUsedWeldConditionNumber = 1;

            // Check previous operations to increase the highest used weld condition counter
            foreach (CbtNode operation in ProgramManager.GetOperationNodes(this.ProgramNode))
            {
                ApplicationType applicationType = OperationManager.GetApplicationType(operation);

                bool weldingOperation = applicationType == ApplicationType.Welding;
                bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing;

                if (weldingOperation || weldingWithTouchSensingOperation)
                {
                    if (KawasakiWeldingProcessMenu.IsOverridden(operation))
                    {
                        this.HighestUsedWeldConditionNumber++;
                    }
                }

                if (operation == operationNode)
                {
                    break;
                }
            }

            base.EditOperationBeforeAllPointEdits(operationNode);
        }

        /// <inheritdoc />
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                if (KawasakiWeldingProcessMenu.IsOverridden(operationNode))
                {
                    point = this.AddWeldStartEvent(operationNode, point, EventIndex.After, this.HighestUsedWeldConditionNumber);
                }
                else
                {
                    point = this.AddWeldStartEvent(operationNode, point, EventIndex.After, 1);
                }
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                point = this.AddWeldEndEvent(operationNode, point, EventIndex.After);
            }

            return point;
        }

        /// <summary>
        ///     Adds tooling activation event to the specified point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The index of the event.</param>
        /// <param name="weldConditionNumber">The weld condition number.</param>
        /// <returns>The point to which the tool activation event has been added.</returns>
        internal virtual PathNode AddWeldStartEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex, int weldConditionNumber)
        {
            point = PointManager.AddEventToPathPoint(
                new WeldStart
                {
                    WeldConditionNumber = weldConditionNumber,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds tooling activation event to the specified point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The index of the event.</param>
        /// <returns>The point to which the tool activation event has been added.</returns>
        internal virtual PathNode AddWeldEndEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new WeldEnd
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }
    }
}
