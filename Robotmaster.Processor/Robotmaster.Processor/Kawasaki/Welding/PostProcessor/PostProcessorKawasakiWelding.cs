﻿// <copyright file="PostProcessorKawasakiWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Welding.PostProcessor
{
    using System.Collections.Generic;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Kawasaki.Default.PostProcessor;
    using Robotmaster.Processor.Kawasaki.Welding.Processes.Events;
    using Robotmaster.Processor.Kawasaki.Welding.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     This class inherits from <see cref="PostProcessorKawasaki"/>.
    ///     <para>
    ///         Implements the Kawasaki welding dedicated properties and methods of the post-processor in order to output Kawasaki welding robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorKawasakiWelding : PostProcessorKawasaki
    {
        /// <summary>
        ///     Gets or sets the list of used weld condition numbers.
        /// </summary>
        internal virtual List<int> UsedWeldConditionNumbersList { get; set; } = new List<int>();

        /// <summary>
        ///     Gets or sets the cached weld condition number.
        /// </summary>
        internal virtual int CachedWeldConditionNumber { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating the weld state.
        /// </summary>
        internal virtual PathFlag WeldState { get; set; }

        /// <summary>
        ///     Gets or sets the Weld conditions string.
        /// </summary>
        internal virtual string WeldConditions
        {
            get => this.CurrentPostFile.FileSection.SubFileSections[2].Header;
            set => this.CurrentPostFile.FileSection.SubFileSections[2].Header = value;
        }

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            base.RunBeforeProgramOutput();

            // Reset list of used weld condition numbers
            this.UsedWeldConditionNumbersList.Clear();

            // Reset active weld condition
            this.WeldState = PathFlag.OutsideOfProcess;
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            PathNode previousPoint = point.PreviousPoint(operationNode);

            if (previousPoint?.Events()?.EventListAfter != null)
            {
                foreach (Event eventAfter in previousPoint.Events().EventListAfter)
                {
                    if (eventAfter is WeldStart)
                    {
                        this.WeldState = PathFlag.InsideProcess;
                    }
                    else if (eventAfter is WeldEnd)
                    {
                        this.WeldState = PathFlag.OutsideOfProcess;
                    }
                }
            }

            if (point?.Events()?.EventListAfter != null)
            {
                foreach (Event eventAfter in point.Events().EventListAfter)
                {
                    if (eventAfter is WeldStart myWeldStart)
                    {
                        this.WeldState = PathFlag.FirstPointOfProcess;

                        this.CachedWeldConditionNumber = myWeldStart.WeldConditionNumber;

                        if (!this.UsedWeldConditionNumbersList.Contains(this.CachedWeldConditionNumber))
                        {
                            this.UsedWeldConditionNumbersList.Add(this.CachedWeldConditionNumber);

                            this.OutputWeldCondition(operationNode, this.CachedWeldConditionNumber);
                        }
                    }
                    else if (eventAfter is WeldEnd)
                    {
                        this.WeldState = PathFlag.LastPointOfProcess;
                    }
                }
            }

            base.RunBeforePointOutput(point, operationNode);
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            if (!(beforePointEvent is WeldStart || beforePointEvent is WeldEnd))
            {
                base.OutputBeforePointEvent(beforePointEvent, point, operationNode);
            }
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            if (!(afterPointEvent is WeldStart || afterPointEvent is WeldEnd))
            {
                base.OutputAfterPointEvent(afterPointEvent, point, operationNode);
            }
        }

        /// <summary>
        ///     Outputs the Weld Condition.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="weldConditionNumber">The weld condition number.</param>
        internal virtual void OutputWeldCondition(CbtNode operationNode, int weldConditionNumber)
        {
            this.WeldConditions += string.Format(
                "\r\n" +
                "  W1SET {0}  =  {1}, {2}, {3}, {4}, {5}, {6}, {7}" + "\r\n" +
                "  W2SET {0}  =  {8}, {9}, {10}, {11}" + "\r\n",
                weldConditionNumber,
                KawasakiWeldingProcessMenu.GetWeldingSpeed(operationNode),
                KawasakiWeldingProcessMenu.GetWeldingCurrent(operationNode),
                KawasakiWeldingProcessMenu.GetWeldingVoltage(operationNode),
                KawasakiWeldingProcessMenu.GetWeavingWidth(operationNode),
                KawasakiWeldingProcessMenu.GetWeavingFrequency(operationNode),
                KawasakiWeldingProcessMenu.GetPatternNumber(operationNode),
                KawasakiWeldingProcessMenu.GetPoleRatio(operationNode),
                KawasakiWeldingProcessMenu.GetArcSpotTime(operationNode),
                KawasakiWeldingProcessMenu.GetArcSpotWeldingCurrent(operationNode),
                KawasakiWeldingProcessMenu.GetArcSpotWeldingVoltage(operationNode),
                KawasakiWeldingProcessMenu.GetArcSpotPoleRatio(operationNode));
        }

        /// <inheritdoc />
        internal override void OutputLinearMovesSection(PathNode point, CbtNode operationNode)
        {
            string linearInstruction =
                this.WeldState == PathFlag.FirstPointOfProcess ? "WS" :
                this.WeldState == PathFlag.InsideProcess ? "WC" :
                this.WeldState == PathFlag.LastPointOfProcess ? "WE" : "MOVE";

            this.Moves.WriteLine(
                $"  L{linearInstruction} Point[{this.CartesianPointCounter}]{(this.WeldState == PathFlag.InsideProcess ? "," + this.CachedWeldConditionNumber : this.WeldState == PathFlag.LastPointOfProcess ? "," + this.CachedWeldConditionNumber + "," + this.CachedWeldConditionNumber : string.Empty)}");
        }

        /// <inheritdoc />
        internal override void OutputCircularCartesianPositionsSection(PathNode point, CbtNode operationNode)
        {
            string circularInstruction1 =
                this.WeldState == PathFlag.InsideProcess ? "WC" :
                this.WeldState == PathFlag.LastPointOfProcess ? "WC" : "MOVE";

            string circularInstruction2 =
                this.WeldState == PathFlag.InsideProcess ? "WC" :
                this.WeldState == PathFlag.LastPointOfProcess ? "WE" : "MOVE";

            this.Moves.WriteLine($"  C1{circularInstruction1} Point[{this.CartesianPointCounter}]{(this.WeldState == PathFlag.InsideProcess || this.WeldState == PathFlag.LastPointOfProcess ? "," + this.CachedWeldConditionNumber : string.Empty)}");
            this.Moves.WriteLine($"  C2{circularInstruction2} Point[{this.CartesianPointCounter + 1}]{(this.WeldState == PathFlag.InsideProcess ? "," + this.CachedWeldConditionNumber : this.WeldState == PathFlag.LastPointOfProcess ? "," + this.CachedWeldConditionNumber + "," + this.CachedWeldConditionNumber : string.Empty)}");
        }
    }
}
