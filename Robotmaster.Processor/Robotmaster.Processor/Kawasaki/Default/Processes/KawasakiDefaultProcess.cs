﻿// <copyright file="KawasakiDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Kawasaki default process.
    /// </summary>
    [DataContract(Name = "KawasakiDefaultProcess")]
    public class KawasakiDefaultProcess : PackageProcess, IKawasakiProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiDefaultProcess"/> class.
        /// </summary>
        public KawasakiDefaultProcess()
        {
            this.Name = "Kawasaki Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
