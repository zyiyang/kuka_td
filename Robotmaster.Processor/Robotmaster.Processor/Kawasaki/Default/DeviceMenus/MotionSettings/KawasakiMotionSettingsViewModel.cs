// <copyright file="KawasakiMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     KawasakiMotionSettingsViewModel View Model.
    /// </summary>
    public class KawasakiMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;

        /// <summary>
        ///     Gets the termination.
        /// </summary>
        public static Dictionary<int, string> JointMotionTerminationTypeSource => new Dictionary<int, string>
        {
            { 0, "FINE" },
            { 1, "CP" },
        };

        /// <summary>
        ///     Gets the termination.
        /// </summary>
        public static Dictionary<int, string> LinCircMotionTerminationTypeSource => new Dictionary<int, string>
        {
            { 0, "FINE" },
            { 1, "CP" },
        };

        /// <summary>
        ///     Gets or sets the joint axis velocity (%).
        /// </summary>
        public double JointAxisVelocity
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointAxisVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointAxisVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis motion accuracy value.
        /// </summary>
        public double JointMotionAccuracy
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionAccuracy)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionAccuracy)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis acceleration (%).
        /// </summary>
        public int JointAxisAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis deceleration (%).
        /// </summary>
        public int JointAxisDeceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisDeceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisDeceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear/circular motion accuracy value.
        /// </summary>
        public double LinCircMotionAccuracy
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinCircMotionAccuracy)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinCircMotionAccuracy)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear/circular axis acceleration (%).
        /// </summary>
        public int LinCircMotionAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinCircMotionAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinCircMotionAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear/circular axis deceleration (%).
        /// </summary>
        public int LinCircMotionDeceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinCircMotionDeceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinCircMotionDeceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint acceleration/deceleration output.
        /// </summary>
        public bool IsJointAccelerationDecelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointAccelerationDecelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointAccelerationDecelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the linear/circular acceleration/deceleration output.
        /// </summary>
        public bool IsLinCircAccelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinCircAccelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinCircAccelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint motion termination type.
        /// </summary>
        public int JointMotionTerminationType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.JointMotionTerminationType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.JointMotionTerminationType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear/circular motion termination type.
        /// </summary>
        public int LinCircMotionTerminationType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinCircMotionTerminationType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinCircMotionTerminationType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }
    }
}