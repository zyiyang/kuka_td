// <copyright file="KawasakiMotionSettings.cs" company="Hypertherm Robotic Software Inc.">
// Copyright 2002-2020 Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>
// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------

namespace Robotmaster.Processor.Kawasaki.Default.DeviceMenus.MotionSettings
{
    using System;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Manages the data of the external menu Kawasaki Motion Settings.
    /// </summary>
    internal static class KawasakiMotionSettings
    {
        /// <summary>
        ///     The cache for retrieved SMS values.
        /// </summary>
        private static readonly SmsValueCache Cache = new SmsValueCache();

        /// <summary>
        ///     Checks if the external menu is loaded.
        /// </summary>
        /// <param name="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu exists; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsLoaded(object node)
        {
            return ExternalMenuManager.IsExternalMenuLoaded("DM_KawasakiMotionSettings_H3CS9L4V", node);
        }

        /// <summary>
        ///     Checks if the external menu is overridden.
        /// </summary>
        /// <param node="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu is overridden; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsOverridden(object node)
        {
            return ExternalMenuManager.IsExternalMenuOverridden("DM_KawasakiMotionSettings_H3CS9L4V", node);
        }

        /// <summary>
        ///     Gets The joint axis velocity (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis velocity (%).
        /// </returns>
        internal static double GetJointAxisVelocity(object node, bool useCache = true)
        {
            const string settingUid = "{9A637E62-5520-4829-94EB-7E63D80F0748}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (double)cachedValue;
                }
                else
                {
                    double result = double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis velocity (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisVelocity(object node, double value, bool useCache = true)
        {
            const string settingUid = "{9A637E62-5520-4829-94EB-7E63D80F0748}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint motion termination type.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint motion termination type.
        /// </returns>
        internal static int GetJointMotionTerminationType(object node, bool useCache = true)
        {
            const string settingUid = "{ACDE0A5C-2AD6-4313-956F-5143C97EB47F}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint motion termination type.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointMotionTerminationType(object node, string value, bool useCache = true)
        {
            const string settingUid = "{ACDE0A5C-2AD6-4313-956F-5143C97EB47F}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The joint axis motion accuracy value.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis motion accuracy value.
        /// </returns>
        internal static double GetJointMotionAccuracy(object node, bool useCache = true)
        {
            const string settingUid = "{BDCE6672-8678-4C91-95ED-8AC6607E9A72}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (double)cachedValue;
                }
                else
                {
                    double result = double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis motion accuracy value.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointMotionAccuracy(object node, double value, bool useCache = true)
        {
            const string settingUid = "{BDCE6672-8678-4C91-95ED-8AC6607E9A72}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets Enables or disables the joint acceleration/deceleration output.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing Enables or disables the joint acceleration/deceleration output.
        /// </returns>
        internal static bool GetIsJointAccelerationDecelerationEnabled(object node, bool useCache = true)
        {
            const string settingUid = "{3299FF69-2624-4D61-958B-95B91D905201}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (bool)cachedValue;
                }
                else
                {
                    bool result = bool.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return bool.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets Enables or disables the joint acceleration/deceleration output.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetIsJointAccelerationDecelerationEnabled(object node, bool value, bool useCache = true)
        {
            const string settingUid = "{3299FF69-2624-4D61-958B-95B91D905201}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis acceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis acceleration (%).
        /// </returns>
        internal static int GetJointAxisAcceleration(object node, bool useCache = true)
        {
            const string settingUid = "{C2258D64-712A-45A7-9306-2EADB105F720}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis acceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisAcceleration(object node, int value, bool useCache = true)
        {
            const string settingUid = "{C2258D64-712A-45A7-9306-2EADB105F720}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis deceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis deceleration (%).
        /// </returns>
        internal static int GetJointAxisDeceleration(object node, bool useCache = true)
        {
            const string settingUid = "{B42F3F37-DF89-4660-B67B-41D04626411A}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis deceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisDeceleration(object node, int value, bool useCache = true)
        {
            const string settingUid = "{B42F3F37-DF89-4660-B67B-41D04626411A}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The linear/circular motion termination type.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The linear/circular motion termination type.
        /// </returns>
        internal static int GetLinCircMotionTerminationType(object node, bool useCache = true)
        {
            const string settingUid = "{CD6BD712-3988-43C4-89EE-F5DA918B83B1}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The linear/circular motion termination type.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetLinCircMotionTerminationType(object node, string value, bool useCache = true)
        {
            const string settingUid = "{CD6BD712-3988-43C4-89EE-F5DA918B83B1}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The linear/circular motion accuracy value.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The linear/circular motion accuracy value.
        /// </returns>
        internal static double GetLinCircMotionAccuracy(object node, bool useCache = true)
        {
            const string settingUid = "{1AE62BEB-580B-40B8-95D2-C7DA966E1415}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (double)cachedValue;
                }
                else
                {
                    double result = double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return double.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The linear/circular motion accuracy value.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetLinCircMotionAccuracy(object node, double value, bool useCache = true)
        {
            const string settingUid = "{1AE62BEB-580B-40B8-95D2-C7DA966E1415}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets Enables or disables the linear/circular acceleration/deceleration output.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing Enables or disables the linear/circular acceleration/deceleration output.
        /// </returns>
        internal static bool GetIsLinCircAccelerationEnabled(object node, bool useCache = true)
        {
            const string settingUid = "{AAB81BAB-181D-4938-B0F4-1AAF5687016F}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (bool)cachedValue;
                }
                else
                {
                    bool result = bool.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return bool.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets Enables or disables the linear/circular acceleration/deceleration output.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetIsLinCircAccelerationEnabled(object node, bool value, bool useCache = true)
        {
            const string settingUid = "{AAB81BAB-181D-4938-B0F4-1AAF5687016F}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The linear/circular axis acceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The linear/circular axis acceleration (%).
        /// </returns>
        internal static int GetLinCircMotionAcceleration(object node, bool useCache = true)
        {
            const string settingUid = "{2D8C1D6F-C175-4E48-9AE8-CF245E08F6FB}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The linear/circular axis acceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetLinCircMotionAcceleration(object node, int value, bool useCache = true)
        {
            const string settingUid = "{2D8C1D6F-C175-4E48-9AE8-CF245E08F6FB}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The linear/circular axis deceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The linear/circular axis deceleration (%).
        /// </returns>
        internal static int GetLinCircMotionDeceleration(object node, bool useCache = true)
        {
            const string settingUid = "{D06DF721-FE57-4FA8-AF7E-AC3A1B9C44C5}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The linear/circular axis deceleration (%).
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetLinCircMotionDeceleration(object node, int value, bool useCache = true)
        {
            const string settingUid = "{D06DF721-FE57-4FA8-AF7E-AC3A1B9C44C5}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }
        /// <summary>
        ///     Clears the SMS value cache.
        /// </summary>
        ///
        internal static void ClearCache()
        {
            Cache.Clear();
        }
    }
}
