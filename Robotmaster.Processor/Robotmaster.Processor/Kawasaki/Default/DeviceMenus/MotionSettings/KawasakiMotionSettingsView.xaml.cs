// <copyright file="KawasakiMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for KawasakiMotionSettings.
    /// </summary>
    public partial class KawasakiMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiMotionSettingsView"/> class.
        /// </summary>
        public KawasakiMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="KawasakiMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public KawasakiMotionSettingsView(KawasakiMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public KawasakiMotionSettingsViewModel ViewModel { get; }
    }
}