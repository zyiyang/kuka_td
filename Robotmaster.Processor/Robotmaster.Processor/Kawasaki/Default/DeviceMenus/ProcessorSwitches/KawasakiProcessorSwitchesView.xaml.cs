// <copyright file="KawasakiProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for KawasakiProcessorSwitches.
    /// </summary>
    public partial class KawasakiProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiProcessorSwitchesView"/> class.
        /// </summary>
        public KawasakiProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KawasakiProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="KawasakiProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public KawasakiProcessorSwitchesView(KawasakiProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public KawasakiProcessorSwitchesViewModel ViewModel { get; }
    }
}