// <copyright file="KawasakiProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     KawasakiProcessorSwitchesViewModel View Model.
    /// </summary>
    public class KawasakiProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        /// <summary>
        ///     Gets the point data format.
        /// </summary>
        public static Dictionary<int, string> PointDataFormatSource => new Dictionary<int, string>
        {
            { 0, "As part of program body" },
            { 1, "As trans/joints blocks" },
        };

        /// <summary>
        ///     Gets or sets point data format condition.
        /// </summary>
        public int PointDataFormat
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.PointDataFormat)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.PointDataFormat)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }
    }
}