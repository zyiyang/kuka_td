﻿// <copyright file="PostProcessorKawasaki.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.PostProcessor
{
    using System;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Kawasaki.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Kawasaki.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Operation.Types;
    using Robotmaster.Rise.Event;

    internal class PostProcessorKawasaki : PostProcessor
    {
        /// <summary>
        ///     Gets the CartesianPositions sub <see cref="StreamWriter"/>.
        /// </summary>
        /// <para>
        ///     The Cartesian Position data is output in <see cref="CartesianPositions"/>.
        /// </para>
        /// <para>
        ///     Cartesian data : Point[xxxx] &lt;X&gt;, &lt;Y&gt;, &lt;Z&gt;, &lt;O&gt;, &lt;A&gt;, &lt;T&gt;.
        /// </para>
        /// <remarks>
        ///     xxxx : A number from 0 to 9999
        ///     Note : Syntax varies depending on the selected Point Data Format.
        /// </remarks>
        internal virtual StreamWriter CartesianPositions => this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter;

        /// <summary>
        ///     Gets the JointPositions sub <see cref="StreamWriter"/>.
        /// </summary>
        /// <para>
        ///     The Joint Position data is output in <see cref="JointPositions"/>.
        /// </para>
        /// <para>
        ///     Joint data : #jPointX &lt;J1&gt;, &lt;J2&gt;, &lt;J3&gt;, &lt;J4&gt;, &lt;J5&gt;, &lt;J6&gt;.
        /// </para>
        /// <remarks>
        ///     X : Point number
        ///     Note : Syntax varies depending on the selected Point Data Format.
        /// </remarks>
        internal virtual StreamWriter JointPositions => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets the Moves sub <see cref="StreamWriter"/>.
        /// </summary>
        /// <para>
        ///     The motion parameters, move instructions, base/tool data and robot configuration are output in this section.
        /// </para>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[2].StreamWriter;

        /// <summary>
        ///     Gets or sets the cached motion params/>.
        /// </summary>
        internal virtual KawasakiMotionParameters CachedMotionParameters { get; set; }

        /// <summary>
        ///     Gets or sets the cached previous motion params/>.
        /// </summary>
        internal virtual KawasakiMotionParameters CachedPreviousMotionParameters { get; set; }

        /// <summary>
        ///     Gets or sets the Cartesian Point Counter/>.
        /// </summary>
        internal virtual int CartesianPointCounter { get; set; }

        /// <summary>
        ///     Gets or sets the Joint Point Counter/>.
        /// </summary>
        internal virtual int JointPointCounter { get; set; }

        /// <inheritdoc />
        internal override void CachePointDataBeforeOutput(PathNode point, CbtNode operation)
        {
            base.CachePointDataBeforeOutput(point, operation);

            // Move Type
            this.CachedMotionParameters.MoveType = point.MoveType() == MoveType.Rapid
                ? MoveType.Rapid
                : MoveType.Linear; // Linear and Circular motion params are shared

            // Configuration
            if (point.MoveSpace() == MoveSpace.Cartesian)
            {
                this.CachedMotionParameters.RobotConfiguration = this.CachedRobotConfig;
            }

            // Joint speed
            this.CachedMotionParameters.JointSpeed = KawasakiMotionSettings.GetJointAxisVelocity(operation);

            // Feed-rate
            this.CachedMotionParameters.FeedRate = point.Feedrate().LinearFeedrate;

            // Cp Motion
            if ((this.CachedMotionParameters.MoveType == MoveType.Rapid && KawasakiMotionSettings.GetJointMotionTerminationType(operation) == 0) ||
                (this.CachedMotionParameters.MoveType == MoveType.Linear && KawasakiMotionSettings.GetLinCircMotionTerminationType(operation) == 0))
            {
                this.CachedMotionParameters.CpMotion = false;
            }
            else if ((this.CachedMotionParameters.MoveType == MoveType.Rapid && KawasakiMotionSettings.GetJointMotionTerminationType(operation) == 1) ||
                     (this.CachedMotionParameters.MoveType == MoveType.Linear && KawasakiMotionSettings.GetLinCircMotionTerminationType(operation) == 1))
            {
                this.CachedMotionParameters.CpMotion = true;
            }

            // Accuracy
            if (this.CachedMotionParameters.CpMotion)
            {
                this.CachedMotionParameters.Accuracy = this.CachedMotionParameters.MoveType == MoveType.Rapid
                    ? KawasakiMotionSettings.GetJointMotionAccuracy(operation)
                    : KawasakiMotionSettings.GetLinCircMotionAccuracy(operation);
            }

            // Use Acceleration
            if ((this.CachedMotionParameters.MoveType == MoveType.Rapid && !KawasakiMotionSettings.GetIsJointAccelerationDecelerationEnabled(operation)) ||
                (this.CachedMotionParameters.MoveType == MoveType.Linear && !KawasakiMotionSettings.GetIsLinCircAccelerationEnabled(operation)))
            {
                this.CachedMotionParameters.UseAcceleration = false;
            }
            else if ((this.CachedMotionParameters.MoveType == MoveType.Rapid && KawasakiMotionSettings.GetIsJointAccelerationDecelerationEnabled(operation)) ||
                     (this.CachedMotionParameters.MoveType == MoveType.Linear && KawasakiMotionSettings.GetIsLinCircAccelerationEnabled(operation)))
            {
                this.CachedMotionParameters.UseAcceleration = true;
            }

            // Acceleration and Deceleration
            if (this.CachedMotionParameters.UseAcceleration)
            {
                this.CachedMotionParameters.Acceleration = point.MoveType() == MoveType.Rapid
                    ? KawasakiMotionSettings.GetJointAxisAcceleration(operation)
                    : KawasakiMotionSettings.GetLinCircMotionAcceleration(operation);
                this.CachedMotionParameters.Deceleration = point.MoveType() == MoveType.Rapid
                    ? KawasakiMotionSettings.GetJointAxisDeceleration(operation)
                    : KawasakiMotionSettings.GetLinCircMotionDeceleration(operation);
            }
        }

        /// <inheritdoc />
        internal override void CachePointDataAfterOutput(PathNode point, CbtNode operation)
        {
            base.CachePointDataAfterOutput(point, operation);

            // Move Type
            this.CachedPreviousMotionParameters.MoveType = this.CachedMotionParameters.MoveType;

            // Configuration
            this.CachedPreviousMotionParameters.RobotConfiguration = this.CachedMotionParameters.RobotConfiguration;

            // Joint speed
            this.CachedPreviousMotionParameters.JointSpeed = this.CachedMotionParameters.JointSpeed;

            // Feed-rate
            this.CachedPreviousMotionParameters.FeedRate = this.CachedMotionParameters.FeedRate;

            // Cp Motion
            this.CachedPreviousMotionParameters.CpMotion = this.CachedMotionParameters.CpMotion;

            // Accuracy
            this.CachedPreviousMotionParameters.Accuracy = this.CachedMotionParameters.Accuracy;

            // Use Acceleration
            this.CachedPreviousMotionParameters.UseAcceleration = this.CachedMotionParameters.UseAcceleration;

            // Acceleration
            this.CachedPreviousMotionParameters.Acceleration = this.CachedMotionParameters.Acceleration;

            // Deceleration
            this.CachedPreviousMotionParameters.Deceleration = this.CachedMotionParameters.Deceleration;
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
            this.CurrentPostFile.FileExtension = ".AS";
            this.StartPostFile(this.CurrentPostFile);

            // Reset variables
            this.CachedPreviousMotionParameters = null;
            this.CachedMotionParameters = new KawasakiMotionParameters();
            this.CartesianPointCounter = 0;
            this.JointPointCounter = 0;
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Kawasaki"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void StartPostFile(PostFile postFile)
        {
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Position Data (LC/CC/JC)
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Position Data (JJ)
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Move instructions

            if (KawasakiProcessorSwitches.GetPointDataFormat(this.CellSettingsNode) == 0)
            {
                postFile.FileSection.SubFileSections[0].Header = ".PROGRAM " + this.CurrentPostFile.FileName + "()" + "\r\n" +
                                                                 "  ;PROGRAMMED USING ROBOTMASTER" + "\r\n";

                postFile.FileSection.SubFileSections[1].StreamWriter.WriteLine(string.Empty);

                postFile.FileSection.SubFileSections[2].Footer = ".END" + "\r\n";
            }
            else
            {
                postFile.FileSection.SubFileSections[0].Header = ".TRANS" + "\r\n";
                postFile.FileSection.SubFileSections[0].Footer = ".END" + "\r\n";

                postFile.FileSection.SubFileSections[1].Header = "\r\n" +
                                                                 ".JOINTS" + "\r\n";
                postFile.FileSection.SubFileSections[1].Footer = ".END" + "\r\n";

                postFile.FileSection.SubFileSections[2].Header = "\r\n" +
                                                                 ".PROGRAM " + this.CurrentPostFile.FileName + "()" + "\r\n" +
                                                                 "  ;PROGRAMMED USING ROBOTMASTER" + "\r\n";
                postFile.FileSection.SubFileSections[2].Footer = ".END" + "\r\n";
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            var previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            if (previousOperationNode == null)
            {
                this.Moves.WriteLine(this.FormatCoordinateSystem(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode));
                this.Moves.WriteLine(this.FormatToolFrame(operationNode));
            }
            else
            {
                if (OperationManager.GetUserFrame(previousOperationNode).Number != OperationManager.GetUserFrame(operationNode).Number)
                {
                    this.Moves.WriteLine(this.FormatCoordinateSystem(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode));
                }

                if (OperationManager.GetTcpId(previousOperationNode) != OperationManager.GetTcpId(operationNode))
                {
                    this.Moves.WriteLine(this.FormatToolFrame(operationNode));
                }
            }

            this.Moves.WriteLine("\r\n" + "  ;" + OperationManager.GetOperationName(operationNode));
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);

            this.Moves.Write(
                this.FormatMotionParameters(
                    point,
                    operationNode));
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Skip Tool Change Macro Output
            if (beforePointEvent.GetType() == typeof(ToolChange))
            {
                return;
            }

            // Output event
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            //// CartesianPositions Output
            this.OutputLinearCartesianPositionsSection(point, operationNode);

            //// Moves Output
            this.OutputLinearMovesSection(point, operationNode);

            this.CartesianPointCounter++;
        }

        /// <summary>
        ///     Outputs the Cartesian Positions section for linear moves.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        internal virtual void OutputLinearCartesianPositionsSection(PathNode point, CbtNode operationNode)
        {
            if (KawasakiProcessorSwitches.GetPointDataFormat(this.CellSettingsNode) == 0)
            {
                //// POINT Point[0] = TRANS(145.000, -44.000, 25.000, 45.000, 180.000, 180.000)
                this.CartesianPositions.WriteLine($"  POINT Point[{this.CartesianPointCounter}] = TRANS({this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)})");
            }
            else
            {
                //// Point[0] 145.000, -44.000, 25.000, 45.000, 180.000, 180.000
                this.CartesianPositions.WriteLine($"  Point[{this.CartesianPointCounter}] {this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)}");
            }
        }

        /// <summary>
        ///     Outputs the Moves section for linear moves.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        internal virtual void OutputLinearMovesSection(PathNode point, CbtNode operationNode)
        {
            //// LMOVE Point[0]
            this.Moves.WriteLine($"  LMOVE Point[{this.CartesianPointCounter}]");
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (!point.IsArcMiddlePoint())
            {
                //// CartesianPositions Output
                this.OutputCircularCartesianPositionsSection(point, operationNode);

                //// Moves Output
                this.OutputCircularMovesSection(point, operationNode);

                this.CartesianPointCounter += 2;
            }
        }

        /// <summary>
        ///     Outputs the Cartesian Positions section for circular moves.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        internal virtual void OutputCircularCartesianPositionsSection(PathNode point, CbtNode operationNode)
        {
            PathNode previousPoint = point.PreviousPoint(operationNode);

            if (KawasakiProcessorSwitches.GetPointDataFormat(this.CellSettingsNode) == 0)
            {
                //// POINT Point[5] = TRANS(127.929, -41.071, 0.000, 45.000, 180.000, 180.000)
                //// POINT Point[6] = TRANS(125.000, -34.000, 0.000, 45.000, 180.000, 180.000)
                this.CartesianPositions.WriteLine($"  POINT Point[{this.CartesianPointCounter}] = TRANS({this.FormatPosition(previousPoint, operationNode)}, {this.FormatOrientation(previousPoint, operationNode)})");
                this.CartesianPositions.WriteLine($"  POINT Point[{this.CartesianPointCounter + 1}] = TRANS({this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)})");
            }
            else
            {
                //// Point[5] 127.929, -41.071, 0.000, 45.000, 180.000, 180.000
                //// Point[6] 125.000, -34.000, 0.000, 45.000, 180.000, 180.000
                this.CartesianPositions.WriteLine($"  Point[{this.CartesianPointCounter}] {this.FormatPosition(previousPoint, operationNode)}, {this.FormatOrientation(previousPoint, operationNode)}");
                this.CartesianPositions.WriteLine($"  Point[{this.CartesianPointCounter + 1}] {this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)}");
            }
        }

        /// <summary>
        ///     Outputs the Moves section for circular moves.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        internal virtual void OutputCircularMovesSection(PathNode point, CbtNode operationNode)
        {
            //// C1MOVE Point[5]
            //// C2MOVE Point[6]
            this.Moves.WriteLine($"  C1MOVE Point[{this.CartesianPointCounter}]");
            this.Moves.WriteLine($"  C2MOVE Point[{this.CartesianPointCounter + 1}]");
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            //// CartesianPositions Output
            if (KawasakiProcessorSwitches.GetPointDataFormat(this.CellSettingsNode) == 0)
            {
                //// POINT Point[0] = TRANS(145.000, -44.000, 25.000, 45.000, 180.000, 180.000)
                this.CartesianPositions.WriteLine($"  POINT Point[{this.CartesianPointCounter}] = TRANS({this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)})");
            }
            else
            {
                //// Point[0] 145.000, -44.000, 25.000, 45.000, 180.000, 180.000
                this.CartesianPositions.WriteLine($"  Point[{this.CartesianPointCounter}] {this.FormatPosition(point, operationNode)}, {this.FormatOrientation(point, operationNode)}");
            }

            //// Moves Output
            //// JMOVE Point[0]
            this.Moves.WriteLine($"  JMOVE Point[{this.CartesianPointCounter}]");

            this.CartesianPointCounter++;
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            var jPointName = string.Empty;

            if (OperationManager.GetOperationType(operationNode) == OperationType.Home)
            {
                if (OperationManager.GetPreviousOperationNodeInProgram(operationNode) == null)
                {
                    jPointName = "appr";
                }
                else if (OperationManager.GetNextOperationNodeInProgram(operationNode) == null)
                {
                    jPointName = "retr";
                }
            }
            else
            {
                jPointName = "jPoint" + this.JointPointCounter;
            }

            //// JointPositions Output
            if (KawasakiProcessorSwitches.GetPointDataFormat(this.CellSettingsNode) == 0)
            {
                //// POINT #jPoint0 = #PPOINT(-16.000, -24.000, -85.000, 46.000, 100.000, 5.000)
                this.JointPositions.WriteLine($"  POINT #{jPointName} = #PPOINT({this.FormatJointValues(point)})");
            }
            else
            {
                //// #jPoint0 -16.000, -24.000, -85.000, 46.000, 100.000, 5.000
                this.JointPositions.WriteLine($"  #{jPointName} {this.FormatJointValues(point)}");
            }

            //// Moves Output
            //// JMOVE #jPoint0
            this.Moves.WriteLine($"  JMOVE #{jPointName}");

            if (OperationManager.GetOperationType(operationNode) != OperationType.Home)
            {
                this.JointPointCounter++;
            }
        }

        /// <summary>
        ///     Formats position output at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Kawasaki formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return $"{pointPositionInUserFrame.X:0.000}, {pointPositionInUserFrame.Y:0.000}, {pointPositionInUserFrame.Z:0.000}";
        }

        /// <summary>
        ///     Formats orientation output.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Kawasaki formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // Gets the Euler angles in user frame
            var pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            var eulerAngles = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return $"{eulerAngles.X:0.000}, {eulerAngles.Y:0.000}, {eulerAngles.Z:0.000}";
        }

        /// <summary>
        ///     Formats joint values at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Kawasaki formatted joint values.</returns>
        internal virtual string FormatJointValues(PathNode point)
        {
            return string.Format(
                "{0:0.000}, {1:0.000}, {2:0.000}, {3:0.000}, {4:0.000}, {5:0.000}",
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]),
                point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]));
        }

        /// <summary>
        ///     Formats the Kawasaki Coordinate System Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        /// <returns>Formatted Kawasaki Coordinate System Output.</returns>
        internal virtual string FormatCoordinateSystem(CbtNode operationNode, CbtNode referenceFrameNode)
        {
            var userFrame = OperationManager.GetUserFrame(operationNode);

            var userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrameNode);

            var eulerAngles = this.RobotFormatter.MatrixToEuler(userFrameMatrix);

            // POINT Base1 = TRANS(23.458, 1208.821, 83.722, -21.270, 6.609, -10.619)
            // BASE Base1
            return string.Format(
                "\r\n" +
                "  POINT Base{0} = TRANS({1:0.000}, {2:0.000}, {3:0.000}, {4:0.000}, {5:0.000}, {6:0.000})" + "\r\n" +
                "  BASE Base{0}",
                OperationManager.GetUserFrame(operationNode).Number,
                userFrameMatrix.Position.X,
                userFrameMatrix.Position.Y,
                userFrameMatrix.Position.Z,
                eulerAngles.X,
                eulerAngles.Y,
                eulerAngles.Z);
        }

        /// <summary>
        ///     Formats the Kawasaki Tool Frame Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Kawasaki Tool Frame Output.</returns>
        internal virtual string FormatToolFrame(CbtNode operationNode)
        {
            var toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
            }

            var eulerAngles = this.RobotFormatter.MatrixToEuler(toolFrameMatrix);

            // POINT Tool1 = TRANS(0.000, 182.817, 238.388, 90.000, 60.000, 180.000)
            // TOOL Tool1
            return string.Format(
                "\r\n" +
                "  POINT Tool{0} = TRANS({1:0.000}, {2:0.000}, {3:0.000}, {4:0.000}, {5:0.000}, {6:0.000})" + "\r\n" +
                "  TOOL Tool{0}",
                OperationManager.GetTcpId(operationNode),
                toolFrameMatrix.Position.X,
                toolFrameMatrix.Position.Y,
                toolFrameMatrix.Position.Z,
                eulerAngles.X,
                eulerAngles.Y,
                eulerAngles.Z);
        }

        /// <summary>
        ///     Formats motion parameters at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Kawasaki formatted position.</returns>
        internal virtual string FormatMotionParameters(PathNode point, CbtNode operationNode)
        {
            var output = string.Empty;

            // Configuration
            if (point.MoveSpace() == MoveSpace.Cartesian)
            {
                output += this.FormatRobotConfiguration();
            }

            // Speed
            output += this.FormatSpeed();

            // Termination
            output += this.FormatTermination();

            // Acceleration
            output += this.FormatAcceleration();

            if (this.CachedPreviousMotionParameters == null)
            {
                this.CachedPreviousMotionParameters = new KawasakiMotionParameters();
            }

            return output;
        }

        /// <summary>
        ///     Formats robot configuration at given point.
        /// </summary>
        /// <returns>Kawasaki formatted robot configuration.</returns>
        internal virtual string FormatRobotConfiguration()
        {
            var output = string.Empty;

            if (this.CachedPreviousMotionParameters == null ||
                this.CachedPreviousMotionParameters.RobotConfiguration == null ||
                this.CachedMotionParameters.RobotConfiguration.BaseConfiguration != this.CachedPreviousMotionParameters.RobotConfiguration.BaseConfiguration ||
                this.CachedMotionParameters.RobotConfiguration.ElbowConfiguration != this.CachedPreviousMotionParameters.RobotConfiguration.ElbowConfiguration ||
                this.CachedMotionParameters.RobotConfiguration.WristConfiguration != this.CachedPreviousMotionParameters.RobotConfiguration.WristConfiguration)
            {
                if (this.CachedMotionParameters.RobotConfiguration.BaseConfiguration == BaseConfig.Front)
                {
                    output += "  RIGHTY" + "\r\n";
                }
                else
                {
                    output += "  LEFTY" + "\r\n";
                }

                if (this.CachedMotionParameters.RobotConfiguration.ElbowConfiguration == ElbowConfig.Up)
                {
                    output += "  ABOVE" + "\r\n";
                }
                else
                {
                    output += "  BELOW" + "\r\n";
                }

                if (this.CachedMotionParameters.RobotConfiguration.WristConfiguration == WristConfig.Positive)
                {
                    output += "  DWRIST" + "\r\n";
                }
                else
                {
                    output += "  UWRIST" + "\r\n";
                }
            }

            return output;
        }

        /// <summary>
        ///     Formats speed at given point.
        /// </summary>
        /// <returns>Kawasaki formatted speed.</returns>
        internal virtual string FormatSpeed()
        {
            var output = string.Empty;

            if (this.CachedMotionParameters.MoveType == MoveType.Rapid)
            {
                if (this.CachedPreviousMotionParameters == null ||
                    this.CachedMotionParameters.MoveType != this.CachedPreviousMotionParameters.MoveType ||
                    Math.Abs(this.CachedMotionParameters.JointSpeed - this.CachedPreviousMotionParameters.JointSpeed) > 1e-3)
                {
                    output += "  SPEED " + $"{this.CachedMotionParameters.JointSpeed:0.###}" + " ALWAYS" + "\r\n";
                }
            }
            else
            {
                if (this.CachedPreviousMotionParameters == null ||
                    this.CachedMotionParameters.MoveType != this.CachedPreviousMotionParameters.MoveType ||
                    Math.Abs(this.CachedMotionParameters.FeedRate - this.CachedPreviousMotionParameters.FeedRate) > 1e-3)
                {
                    output += "  SPEED " + $"{this.CachedMotionParameters.FeedRate:0.###}" + " MM/S ALWAYS" + "\r\n";
                }
            }

            return output;
        }

        /// <summary>
        ///     Formats termination at given point.
        /// </summary>
        /// <returns>Kawasaki formatted termination.</returns>
        internal virtual string FormatTermination()
        {
            var output = string.Empty;

            if (!this.CachedMotionParameters.CpMotion)
            {
                if (this.CachedPreviousMotionParameters == null ||
                    this.CachedMotionParameters.CpMotion != this.CachedPreviousMotionParameters.CpMotion)
                {
                    output += "  CP OFF" + "\r\n";
                }
            }
            else
            {
                if (this.CachedPreviousMotionParameters == null ||
                    this.CachedMotionParameters.CpMotion != this.CachedPreviousMotionParameters.CpMotion)
                {
                    output += "  CP ON" + "\r\n";
                }

                if (this.CachedPreviousMotionParameters == null ||
                    Math.Abs(this.CachedMotionParameters.Accuracy - this.CachedPreviousMotionParameters.Accuracy) > 1e-3)
                {
                    output += "  ACCURACY " + $"{this.CachedMotionParameters.Accuracy:0.###}" + " ALWAYS" + "\r\n";
                }
            }

            return output;
        }

        /// <summary>
        ///     Formats acceleration at given point.
        /// </summary>
        /// <returns>Kawasaki formatted acceleration.</returns>
        internal virtual string FormatAcceleration()
        {
            var output = string.Empty;

            if (this.CachedMotionParameters.UseAcceleration)
            {
                if (this.CachedPreviousMotionParameters == null ||
                    Math.Abs(this.CachedMotionParameters.Acceleration - this.CachedPreviousMotionParameters.Acceleration) > 1e-3 ||
                    Math.Abs(this.CachedMotionParameters.Deceleration - this.CachedPreviousMotionParameters.Deceleration) > 1e-3)
                {
                    output += "  ACCEL " + $"{this.CachedMotionParameters.Acceleration:0.###}" + " ALWAYS" + "\r\n";
                    output += "  DECEL " + $"{this.CachedMotionParameters.Deceleration:0.###}" + " ALWAYS" + "\r\n";
                }
            }

            return output;
        }
    }
}
