﻿// <copyright file="KawasakiMotionParameters.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.PostProcessor
{
    using Robotmaster.Device;
    using Robotmaster.Device.Path.Types;

    internal class KawasakiMotionParameters
    {
        /// <summary>
        /// Gets or sets the cached motion type.
        /// </summary>
        public MoveType MoveType { get; set; }

        /// <summary>
        /// Gets or sets the cached robot configuration.
        /// </summary>
        public RobotConfiguration RobotConfiguration { get; set; } = null;

        /// <summary>
        /// Gets or sets the cached joint speed.
        /// </summary>
        public double JointSpeed { get; set; }

        /// <summary>
        /// Gets or sets the cached feed-rate.
        /// </summary>
        public double FeedRate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether CP motion is ON.
        /// </summary>
        public bool CpMotion { get; set; }

        /// <summary>
        /// Gets or sets the cached accuracy.
        /// </summary>
        public double Accuracy { get; set; } = -9999;

        /// <summary>
        /// Gets or sets a value indicating whether to use acceleration.
        /// </summary>
        public bool UseAcceleration { get; set; }

        /// <summary>
        /// Gets or sets the cached acceleration.
        /// </summary>
        public double Acceleration { get; set; } = -9999;

        /// <summary>
        /// Gets or sets the cached deceleration.
        /// </summary>
        public double Deceleration { get; set; } = -9999;
    }
}
