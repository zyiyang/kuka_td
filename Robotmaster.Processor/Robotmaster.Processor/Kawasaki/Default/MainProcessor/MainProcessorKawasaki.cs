﻿// <copyright file="MainProcessorKawasaki.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kawasaki.Default.MainProcessor
{
    using Robotmaster.Processor.Common.Default.MainProcessor;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Kawasaki dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorKawasaki : MainProcessor
    {
    }
}
