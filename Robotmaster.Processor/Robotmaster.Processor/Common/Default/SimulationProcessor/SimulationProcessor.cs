﻿// <copyright file="SimulationProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.SimulationProcessor
{
    /// <summary>
    ///     The simulation processor.
    /// </summary>
    public class SimulationProcessor
    {
    }
}