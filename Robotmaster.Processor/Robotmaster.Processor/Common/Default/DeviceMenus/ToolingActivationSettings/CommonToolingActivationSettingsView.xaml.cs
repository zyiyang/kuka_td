// <copyright file="CommonToolingActivationSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings
{
    /// <summary>
    ///     Interaction logic for CommonToolingActivationSettings.
    /// </summary>
    public partial class CommonToolingActivationSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CommonToolingActivationSettingsView"/> class.
        /// </summary>
        public CommonToolingActivationSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CommonToolingActivationSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="CommonToolingActivationSettingsViewModel"/> to map to the UI.
        /// </param>
        public CommonToolingActivationSettingsView(CommonToolingActivationSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public CommonToolingActivationSettingsViewModel ViewModel { get; }
    }
}