// <copyright file="CommonToolingActivationSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     CommonToolingActivationSettingsViewModel View Model.
    /// </summary>
    public class CommonToolingActivationSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;

        /// <summary>
        ///     Gets the tooling process condition.
        /// </summary>
        public static Dictionary<int, string> ToolingActivationDeactivationConditionSource => new Dictionary<int, string>
        {
            { 0, "No tooling activation" },
            { 1, "Activate/Deactivate at the start and end of file." },
            { 2, "Activate/Deactivate with tool call." },
            { 3, "Activate/Deactivate at every path." },
        };

        /// <summary>
        ///     Gets the tooling process start condition.
        /// </summary>
        public static Dictionary<int, string> ToolingActivationConditionSource => new Dictionary<int, string>
        {
            { 0, "Before the linear plunge move" },
            { 1, "After the linear plunge move" },
            { 2, "Before the first point of contact with part" },
            { 3, "After the first point of contact with part" },
            { 4, "Before the first none joint move" },
            { 5, "After the first none joint move" },
            { 6, "Before the first move of the operation" },
            { 7, "After the first move of the operation" },
        };

        /// <summary>
        ///     Gets the tooling process end condition.
        /// </summary>
        public static Dictionary<int, string> ToolingDeactivationConditionSource => new Dictionary<int, string>
        {
            { 0, "Before the linear retract move" },
            { 1, "After the linear retract move" },
            { 2, "Before the last point of contact with part" },
            { 3, "After the last point of contact with part" },
            { 4, "Before the last none joint move" },
            { 5, "After the last none joint move" },
            { 6, "Before the last move of the operation" },
            { 7, "After the last move of the operation" },
        };

        /// <summary>
        ///     Gets or sets the tooling activation call. The call is a macro that activates the tooling process.
        /// </summary>
        public string ToolingActivationMacro
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolingActivationMacro)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolingActivationMacro)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tooling deactivation call. The call is a macro that deactivates the tooling process.
        /// </summary>
        public string ToolingDeactivationMacro
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolingDeactivationMacro)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolingDeactivationMacro)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tooling process activation and deactivation condition.
        /// </summary>
        public int ToolingActivationDeactivationCondition
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingActivationDeactivationCondition)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingActivationDeactivationCondition)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsToolingActivationDeactivationPresent));
            }
        }

        /// <summary>
        ///     Gets or sets the output conditions for starting the tooling activation process at every pass.
        /// </summary>
        public int ToolingActivationCondition
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingActivationCondition)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingActivationCondition)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the output conditions for ending the tooling activation process at every pass.
        /// </summary>
        public int ToolingDeactivationCondition
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingDeactivationCondition)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolingDeactivationCondition)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ToolingActivationDeactivationCondition"/> is not set to "No tooling activation".
        /// </summary>
        public bool IsToolingActivationDeactivationPresent => this.ToolingActivationDeactivationCondition != 0;

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }
    }
}