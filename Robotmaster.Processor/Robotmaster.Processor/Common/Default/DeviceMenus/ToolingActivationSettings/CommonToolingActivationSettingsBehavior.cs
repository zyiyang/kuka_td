// <copyright file="CommonToolingActivationSettingsBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing <see cref="CommonToolingActivationSettingsView"/>.
    /// </summary>
    internal class CommonToolingActivationSettingsBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the View Model.
        /// </summary>
        private CommonToolingActivationSettingsViewModel ViewModel { get; set; }

        /// <summary>
        ///      Sets up the View Model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}