﻿// <copyright file="PostProcessorData.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.PostProcessor
{
    using System;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.PathEngine;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Operation.Types;

    internal abstract partial class PostProcessor
    {
        /// <summary>
        /// Gets or sets the current operation number.
        /// It is automatically incremented at every operation of the <see cref="OuputAllPoints"/> loop.
        /// </summary>
        internal virtual int OperationNumber { get; set; }

        /// <summary>
        /// Gets or sets the current task operation number.
        /// It is automatically incremented at every operation of the <see cref="OuputAllPoints"/> loop.
        /// </summary>
        internal virtual int TaskOperationNumber { get; set; }

        /// <summary>
        /// Gets or sets the current point number n the program.
        /// It is automatically incremented at every point of the <see cref="OuputAllPoints"/> loop.
        /// </summary>
        internal virtual int PointNumberInProgram { get; set; }

        /// <summary>
        /// Gets or sets the current point number in the operation.
        /// It is automatically incremented at every point of the <see cref="OuputAllPoints"/> loop.
        /// </summary>
        internal virtual int PointNumberInOperation { get; set; }

        /// <summary>
        /// Gets or sets the current point number in the file.
        /// It is automatically incremented at every point of <see cref="OuputAllPoints"/> loop.
        /// It is the responsibility of the derived <see cref="PostProcessor"/> class implementation to reset it at the proper time.
        /// </summary>
        internal virtual int PointNumberInPostFile { get; set; }

        /// <summary>
        /// Gets or sets the current Cartesian point number in the program.
        /// It is automatically incremented at every point of <see cref="OuputAllPoints"/> loop.
        /// </summary>
        internal virtual int CartesianPointNumberInProgram { get; set; }

        /// <summary>
        /// Gets or sets the cached robot configuration.
        /// It is refreshed at every operation except for <see cref="TransitionType"/> operations where it is recalculated at every point.
        /// <seealso cref="CachePointDataBeforeOutput"/>
        /// </summary>
        internal virtual RobotConfiguration CachedRobotConfig { get; set; }

        /// <summary>
        /// Gets or sets the previous operation.
        /// </summary>
        internal virtual CbtNode CachedPreviousOperation { get; set; }

        /// <summary>
        /// Gets or sets the previous point.
        /// </summary>
        internal virtual PathNode CachedPreviousPoint { get; set; }

        /// <summary>
        /// Gets or sets the previous rapid point.
        /// </summary>
        internal virtual PathNode CachedPreviousRapidPoint { get; set; }

        /// <summary>
        /// Gets or sets the previous linear point.
        /// </summary>
        internal virtual PathNode CachedPreviousLinearPoint { get; set; }

        /// <summary>
        /// Gets or sets the previous circular point.
        /// </summary>
        internal virtual PathNode CachedPreviousCircularPoint { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether to output the current home.
        ///     <c>null</c> if does not apply.
        /// </summary>
        internal virtual bool? CachedIsHomeOutput { get; set; }

        /// <summary>
        ///     Refreshes program level cached data.
        ///     <para> It is called once per <paramref name="program"/>.</para>
        /// </summary>
        /// <param name="program">Current program.</param>
        internal virtual void CacheProgramData(CbtNode program)
        {
        }

        /// <summary>
        /// Refreshes operation level cached data.
        /// <para> It is called once for per <paramref name="operation"/> in <see cref="OuputAllPoints"/>.</para>
        /// </summary>
        /// <param name="operation">Current operation.</param>
        internal virtual void CacheOperationData(CbtNode operation)
        {
            if (OperationManager.GetOperationType(operation) == OperationType.Home)
            {
                this.CachedIsHomeOutput = HomePositionManager.ShouldOutputHomeOperation(operation, this.OperationCbtRoot);
            }
            else
            {
                this.CachedIsHomeOutput = null;
            }

            if (OperationManager.GetOperationType(operation) == OperationType.Task)
            {
                this.TaskOperationNumber++;
            }

            this.CachedPreviousOperation = OperationManager.GetPreviousOperationNodeInProgram(operation);

            this.OperationNumber++;
            this.PointNumberInOperation = 0;

            // CachedRobotConfig caching at an operation level
            // For transition operation, CachedRobotConfig is recalculated at every point.
            this.CachedRobotConfig = OperationManager.GetRobotConfiguration(OperationManager.GetFirstPoint(operation), operation);
        }

        /// <summary>
        /// Refreshes point level cached data before <paramref name="point"/> is output.
        /// <para> It is once per <paramref name="point"/> in <see cref="OuputAllPoints"/>.</para>
        /// </summary>
        /// <param name="point">point.</param>
        /// <param name="operation">Current operation.</param>
        internal virtual void CachePointDataBeforeOutput(PathNode point, CbtNode operation)
        {
            //// Refresh before outputting point
            this.PointNumberInProgram++;
            this.PointNumberInOperation++;
            this.PointNumberInPostFile++;
            if (OperationManager.GetOperationType(operation) == OperationType.Transition)
            {
                // For transition operation, CachedRobotConfig is recalculated at every point.
                this.CachedRobotConfig = OperationManager.GetRobotConfiguration(point, operation);
            }
        }

        /// <summary>
        /// Refreshes point level cached data after <paramref name="point"/> has been output.
        /// <para> It is once per <paramref name="point"/> in <see cref="OuputAllPoints"/>.</para>
        /// </summary>
        /// <param name="point">point.</param>
        /// <param name="operation">Current operation.</param>
        internal virtual void CachePointDataAfterOutput(PathNode point, CbtNode operation)
        {
            //// Refresh after outputting point
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    this.CachedPreviousRapidPoint = point;
                    break;
                case MoveType.Linear:
                    this.CachedPreviousLinearPoint = point;
                    break;
                case MoveType.Circular:
                    this.CachedPreviousCircularPoint = point;
                    break;
            }

            if (point.MoveSpace() != MoveSpace.Joint)
            {
                this.CartesianPointNumberInProgram++;
            }

            this.CachedPreviousPoint = point;
        }
    }
}