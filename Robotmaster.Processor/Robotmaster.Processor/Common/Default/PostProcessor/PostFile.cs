﻿// <copyright file="PostFile.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    ///     Represents a file node (and tree) to be output by the <see cref="PostProcessor"/>.
    /// </summary>
    internal class PostFile
    {
        /// <summary>
        ///     Gets or sets the relative folder from the <see cref="Parent"/>.
        /// <para>
        ///     Useful for sub-folder creation.
        /// </para>
        /// </summary>
        internal string RelativePath { get; set; } = string.Empty;

        /// <summary>
        ///     Gets or sets the file name with extension.
        /// </summary>
        internal string FileName { get; set; }

        /// <summary>
        ///     Gets or sets the file extension.
        /// </summary>
        internal string FileExtension { get; set; } = string.Empty;

        /// <summary>
        ///     Gets the full file path.
        ///     Combines the file relative path, name and extension.
        /// <para>
        ///     If empty, the sub <see cref="Default.PostProcessor.FileSection"/> is output in the same file as the parent <see cref="Default.PostProcessor.FileSection"/>.
        /// </para>
        /// </summary>
        internal string RelativeFilePath => Path.Combine(this.RelativePath, this.FileName + this.FileExtension);

        /// <summary>
        ///     Gets or sets the parent file.
        ///     If null, this <see cref="PostFile"/> is the root or an independent file.
        /// </summary>
        internal PostFile Parent { get; set; }

        /// <summary>
        ///     Gets or sets the list of child <see cref="PostFile"/>.
        /// </summary>
        internal List<PostFile> Children { get; set; } = new List<PostFile>();

        /// <summary>
        ///     Gets or sets the file section.
        /// <para>
        ///     With its <see cref="Default.PostProcessor.FileSection.SubFileSections"/> it represents the file inner structure.
        /// </para>
        /// </summary>
        internal FileSection FileSection { get; set; } = new FileSection();

        /// <summary>
        ///     Adds a new child <see cref="PostFile"/> to <see cref="Children"/>.
        ///     Sets automatically the parent.
        /// </summary>
        /// <param name="newChild"><see cref="PostFile"/> to add.</param>
        internal void AddChild(PostFile newChild)
        {
            newChild.Parent?.Children.Remove(newChild);
            newChild.Parent = this;
            this.Children.Add(newChild);
        }

        /// <summary>
        ///     Replaces <paramref name="newChild"/> at the same index of <paramref name="oldChild"/> in this <see cref="PostFile"/>.
        /// </summary>
        /// <param name="newChild">
        ///     The new child to replace.
        /// </param>
        /// <param name="oldChild">
        ///     The old child which <paramref name="newChild"/> will replace.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     <paramref name="newChild"/> is not a child of this.
        /// </exception>
        internal void ReplaceChild(PostFile newChild, PostFile oldChild)
        {
            int index = this.Children.IndexOf(oldChild);

            // Not found
            if (index == -1)
            {
                throw new ArgumentException($@"{nameof(oldChild)} is not a child of this.", nameof(oldChild));
            }

            this.Children.RemoveAt(index);
            newChild.Parent?.Children.Remove(newChild);
            newChild.Parent = this;
            this.Children.Insert(index, newChild);
        }

        /// <summary>
        ///     Insert a new intermediate parent between this <see cref="PostFile"/> and its current parent.
        /// </summary>
        /// <exception cref="ArgumentException">
        ///     This <see cref="PostFile"/> does not have any parent.
        /// </exception>
        internal void InsertIntermediateParent()
        {
            PostFile oldParent = this.Parent;

            if (oldParent == null)
            {
                throw new ArgumentException($"This {nameof(PostFile)} does not have any parent.");
            }

            var newParentPostFile = new PostFile();
            oldParent.ReplaceChild(newParentPostFile, this);
            newParentPostFile.AddChild(this);
        }

        /// <summary>
        ///     Gets a value indicating whether this <see cref="PostFile"/> already exists in <paramref name="folderPath"/>.
        /// </summary>
        /// <param name="folderPath">
        ///     Destination folder.
        /// </param>
        /// <returns>
        ///     <c>true</c> if the file exists;
        ///     otherwise,<c>false</c>.
        /// </returns>
        internal bool Exist(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(this.FileName + this.FileExtension))
            {
                return false;
            }

            return File.Exists(Path.Combine(folderPath, this.RelativeFilePath));
        }

        /// <summary>
        ///     Recursive method to generates the robot code files associated to this <see cref="PostFile"/> and all its <see cref="Children"/>.
        /// </summary>
        /// <param name="folderPath">
        ///     Destination folder.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     The <paramref name="folderPath"/> path is empty.
        /// </exception>
        internal void GenerateFiles(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(folderPath))
            {
                throw new ArgumentException($"This destination folder is empty.");
            }

            this.GenerateFile(Path.Combine(folderPath, this.RelativePath));

            foreach (PostFile childrenPostFile in this.Children)
            {
                childrenPostFile.GenerateFiles(Path.Combine(folderPath, childrenPostFile.RelativePath));
            }
        }

        /// <summary>
        ///     Initiation method to write to the <see cref="Default.PostProcessor.FileSection"/> and its <see cref="Default.PostProcessor.FileSection.SubFileSections"/>.
        ///     It creates the first output file then calls the <see cref="WriteAllSections"/> method recursively.
        /// </summary>
        /// <param name="folderPath">Destination folder.</param>
        /// <exception cref="ArgumentException">
        ///     This <see cref="PostFile"/> does not have any parent.
        /// </exception>
        internal void GenerateFile(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(this.FileName + this.FileExtension))
            {
                throw new ArgumentException($"This {nameof(PostFile)} does not have any file name.");
            }

            // Create directories if needed
            string dir = Path.GetDirectoryName(Path.Combine(folderPath, this.RelativeFilePath));
            if (dir != null && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            using (var fileStream = new FileStream(Path.Combine(folderPath, this.RelativeFilePath), FileMode.Create))
            using (var streamWriter = new StreamWriter(fileStream, Encoding.ASCII) { AutoFlush = true })
            {
                this.WriteAllSections(this.FileSection, streamWriter);
            }
        }

        /// <summary>
        ///     Recursive method to write a <see cref="Default.PostProcessor.FileSection"/> and his <see cref="Default.PostProcessor.FileSection.SubFileSections"/>.
        /// <para>
        ///     Writing happens in the following order
        ///     <see cref="Default.PostProcessor.FileSection.Header"/>,
        ///     <see cref="Default.PostProcessor.FileSection.MemoryStream"/>,
        ///     <see cref="Default.PostProcessor.FileSection.SubFileSections"/>,
        ///     <see cref="Default.PostProcessor.FileSection.Footer"/>.
        /// </para>
        /// </summary>
        /// <param name="fileSection"> File section to write.</param>
        /// <param name="streamWriter">Destination streamWriter.</param>
        private void WriteAllSections(FileSection fileSection, StreamWriter streamWriter)
        {
            // Writes header
            streamWriter.Write(fileSection.Header);

            fileSection.MemoryStream.Position = 0;
            fileSection.MemoryStream.CopyTo(streamWriter.BaseStream);
            fileSection.MemoryStream.Close();

            foreach (FileSection subFileSection in fileSection.SubFileSections)
            {
                    // Writes sub post blocks in the same file
                    this.WriteAllSections(subFileSection, streamWriter);
            }

            // Writes footer
            streamWriter.Write(fileSection.Footer);
        }
    }
}
