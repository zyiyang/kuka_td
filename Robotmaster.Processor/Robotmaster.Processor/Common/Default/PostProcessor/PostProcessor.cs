﻿// <copyright file="PostProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using NLog;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Component.Types;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.MyProcessorPackage;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.ApplicationLayer.Managers.Keys;
    using Robotmaster.Rise.BaseClasses.Robot;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;
    using Robotmaster.Rise.Modules.RiseWindow;
    using Robotmaster.Wpf.Controls;

    /// <summary>
    ///     Post-processor base class.
    ///     Contains the methods executed when output program is clicked.
    ///     <para>It post-processes Robotmaster data in order to output robot code.</para>
    /// </summary>
    internal abstract partial class PostProcessor
    {
        /// <summary>
        ///     Gets current class logger.
        /// </summary>
        internal static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///     Gets the full path of the main <see cref="PostFile"/>.
        /// </summary>
        internal virtual string FullPath => Path.Combine(this.ProgramFolderPath, this.MainPostFile.FileName + this.MainPostFile.FileExtension);

        /// <summary>
        ///     Gets the <see cref="DateTime"/> to use.
        ///     Uses the default minimum date if <c>Properties.Settings.Default.UseActualDateTime</c> is <c>false</c>.
        ///     Useful when testing to avoid output code differences.
        /// </summary>
        internal virtual DateTime ProcessorDateTime => Properties.Settings.Default.UseActualDateTime ? DateTime.Now : DateTime.MinValue;

        /// <summary>
        ///     Gets or sets the operationNode Cbt Root.
        /// </summary>
        internal virtual CbtNode CellSettingsNode { get; set; }

        /// <summary>
        ///     Gets or sets the current setup.
        /// </summary>
        internal virtual CbtNode SetupNode { get; set; }

        /// <summary>
        ///     Gets or sets the current program.
        /// </summary>
        internal virtual CbtNode ProgramNode { get; set; }

        /// <summary>
        ///     Gets or sets the current program name.
        /// </summary>
        internal virtual string ProgramName { get; set; }

        /// <summary>
        ///     Gets or sets the current program folder path.
        /// </summary>
        internal virtual string ProgramFolderPath { get; set; }

        /// <summary>
        ///     Gets the main program file.
        /// </summary>
        internal virtual PostFile MainPostFile => this.RootPostFile.Children.First();

        /// <summary>
        ///     Gets or sets the current <see cref="PostFile"/>.
        ///     It is the responsibility of the derived <see cref="PostProcessor"/> class implementation to update it when a new <see cref="PostFile"/> is created.
        /// <remarks>
        ///     It acts as a dynamic writing head changing from file to file as the program is being written.
        /// </remarks>
        /// </summary>
        internal virtual PostFile CurrentPostFile { get; set; }

        /// <summary>
        ///     Gets or sets the root of operation CBT (Component Based Tree).
        ///     This tree stores the data and the settings for each entity in Robotmaster hierarchies (tasks, programs, operations, etc.)
        ///     except the spatial data which is stored in the Scene CBT.
        /// <remarks>
        ///     No to be mistaken with OperationNode.
        /// </remarks>
        /// </summary>
        /// <seealso cref="SceneCbtRoot"/>
        internal virtual CbtNode OperationCbtRoot { get; set; }

        /// <summary>
        ///     Gets or sets the root of the Scene CBT (Component Based Tree).
        ///     This tree stores the spatial information including the models and relations between the physical elements
        ///     in the application (axes, models, frames, etc.).
        /// </summary>
        /// <seealso cref="OperationCbtRoot"/>
        internal virtual CbtNode SceneCbtRoot { get; set; }

        /// <summary>
        ///     Gets or sets the processor package behavior.
        /// </summary>
        internal virtual MyProcessorPackageBehavior ProcessorPackageBehavior { get; set; }

        /// <summary>
        ///     Gets or sets the robot formatter.
        ///     <para>Provides all the brand formatting methods used by Robotmaster UI.</para>
        /// <remarks>
        ///     No to be mistaken with FormatManagers which can implement additional formatting methods for the processor package.
        /// </remarks>
        /// </summary>
        internal virtual RobotFormatter RobotFormatter { get; set; }

        /// <summary>
        ///     Gets or sets the joint format dictionary: the pairs of joint name (key) and index (value).
        ///     <para>It is to be used to retrieve the joint value in <c>point.JointValues()</c>.</para>
        /// </summary>
        /// <example>
        ///     <code>
        ///     point.JointValues()[JointFormat["J1"]]
        ///     // or
        ///     point.JointValues(JointFormat["J1"])
        ///     </code>
        /// </example>
        internal virtual Dictionary<string, int> JointFormat { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the external axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> ExternalAxesJoints { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the rotary axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> Rotaries { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the rail axes <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> Rails { get; set; }

        /// <summary>
        ///     Gets or sets the sequence of all the robot <see cref="Joint"/>s.
        /// </summary>
        internal virtual IEnumerable<Joint> RobotJoints { get; set; }

        /// <summary>
        ///     Gets or sets the home position settings data <see cref="Joint"/>s.
        /// </summary>
        /// <remarks>
        ///     It contains the home position data for both active and inactive joints.
        /// </remarks>
        internal virtual HomePositionData HomePositionData { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether multiple different tools are used in the program.
        /// </summary>
        internal virtual bool IsMultiToolProgram { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the tool frame extra rotation should be applied.
        /// </summary>
        internal virtual bool EnableToolFrameExtraRotation { get; set; }

        /// <summary>
        ///     Gets or sets the tool frame extra rotation.
        ///     This is an experimental feature.
        /// </summary>
        internal virtual Matrix3X3 ToolFrameExtraRotation { get; set; }

        /// <summary>
        ///     Gets the root file of the file tree.
        /// <remarks>
        ///     This file is not meant to be used.
        ///     It allows consistent <see cref="PostFile"/> manipulation.
        /// </remarks>
        /// </summary>
        private PostFile RootPostFile { get; } = new PostFile();

        /// <summary>
        ///     Notify the user with a message.
        /// </summary>
        /// <param name="message">The message to be displayed to the user.</param>
        /// <param name="outputInLogger">Indicates whether the message will also be used to output in the Logger.</param>
        /// <param name="autoDismiss">Indicates whether the notification will auto dismiss.</param>
        internal virtual void NotifyUser(string message, bool outputInLogger = false, bool autoDismiss = true)
        {
            ////this.NotificationItems.Add(new NotificationViewModel { Caption = message, AutoDismiss = autoDismiss });
            this.ProcessorPackageBehavior.Broadcast(autoDismiss ? RiseBehavior.MessageDisplayMessage : RiseBehavior.MessageDisplayStickyMessage, message);

            if (outputInLogger)
            {
                Logger.Info(message);
            }
        }

        /// <summary>
        ///     Runs the post-processor entry method.
        /// </summary>
        /// <param name="programNode">The program node to post process.</param>
        internal virtual void Run(CbtNode programNode)
        {
            this.ProgramNode = programNode;
            this.CellSettingsNode = CellManager.GetCellSettingsNode(this.OperationCbtRoot);
            this.SetupNode = ProgramManager.GetSetupNode(this.ProgramNode);
            this.ProgramFolderPath = Properties.Settings.Default.DestinationFolder;
            this.ProgramName = ProgramManager.GetProgramName(this.ProgramNode);

            if (ProgramManager.GetOperationNodes(this.ProgramNode).Select(op => OperationManager.GetTool(op)).Distinct().Count() != 1)
            {
                this.IsMultiToolProgram = true;
            }

            this.InitalizeJointLists();
            this.HomePositionData = EntitySettingManager.GetSettingObject<HomePositionData>(this.ProgramNode);
            this.ReadCustomValues();

            var postFile = new PostFile();  // Output framework 3
            this.RootPostFile.AddChild(postFile);
            this.CurrentPostFile = postFile;

            if (this.IsProgramInputValid())
            {
                this.OuputAllPoints();
                if (this.IsProgramOutputValid())
                {
                    this.GenerateFiles();
                }
            }
        }

        /// <summary>
        ///     Initialize the <see cref="Joint"/> lists used in the current setup.
        /// </summary>
        internal virtual void InitalizeJointLists()
        {
            this.JointFormat = SetupManager.GetJointFormat(this.SetupNode);

            List<CbtNode> allManipulators = CellManager.GetManipulatorNodes(SetupManager.GetConfiguration(this.SetupNode));
            IEnumerable<CbtNode> robotManipulators = allManipulators.Where(m => m.HasComponent(SceneKeys.Robot));
            IEnumerable<CbtNode> externalAxesManipulators = allManipulators.Where(m => !m.HasComponent(SceneKeys.Robot));

            this.RobotJoints = robotManipulators
                .SelectMany(CellManager.GetManipulatorJoints)
                .Where(joint => this.JointFormat.ContainsKey(joint.Name))
                .OrderBy(joint => joint.Name);
            this.ExternalAxesJoints = externalAxesManipulators
                .SelectMany(CellManager.GetManipulatorJoints)
                .OrderBy(joint => joint.Name);

            this.Rotaries = this.ExternalAxesJoints
                .Where(x => x != null)
                .Where(x => x.AxisType == AxisType.Revolute);
            this.Rails = this.ExternalAxesJoints
                .Where(x => x != null)
                .Where(x => x.AxisType == AxisType.Prismatic);
        }

        /// <summary>
        ///     Read custom values.
        /// </summary>
        internal virtual void ReadCustomValues()
        {
            var cellCustomValues = (Dictionary<string, string>)this.CellSettingsNode.GetComponent("CellExtensionCustomValues"); // TODO: To be replaced by CellManager.GetCellCustomValues(this.CellSettingsNode)
            if (cellCustomValues == null)
            {
                return;
            }

            //// Experimental feature activated with custom values

            // Tool Frame Extra Rotation
            var featureToolframeextrarotationmatrix3x3 = "_FEATURE_ToolFrameExtraRotationMatrix3X3";
            if (cellCustomValues.TryGetValue(featureToolframeextrarotationmatrix3x3, out string toolFrameExtraRotationMatrix3X3String))
            {
                Matrix3X3 toolFrameExtraRotation = Matrix3X3.Identity;

                double[] doubleArray = new double[9];
                try
                {
                    string[] stringArray = toolFrameExtraRotationMatrix3X3String.Split(',');
                    if (stringArray.Length == 9)
                    {
                        for (int i = 0; i < 9; i++)
                        {
                            doubleArray[i] = double.Parse(stringArray[i], CultureInfo.InvariantCulture);
                        }

                        toolFrameExtraRotation.E11 = doubleArray[0];
                        toolFrameExtraRotation.E12 = doubleArray[1];
                        toolFrameExtraRotation.E13 = doubleArray[2];
                        toolFrameExtraRotation.E21 = doubleArray[3];
                        toolFrameExtraRotation.E22 = doubleArray[4];
                        toolFrameExtraRotation.E23 = doubleArray[5];
                        toolFrameExtraRotation.E31 = doubleArray[6];
                        toolFrameExtraRotation.E32 = doubleArray[7];
                        toolFrameExtraRotation.E33 = doubleArray[8];

                        Matrix3X3.Orthonormalize(toolFrameExtraRotation);

                        this.ToolFrameExtraRotation = toolFrameExtraRotation;
                        this.EnableToolFrameExtraRotation = true;
                    }
                    else
                    {
                        Logger.Info($"ERROR: The custom value {featureToolframeextrarotationmatrix3x3} does not have the proper length.");
                    }
                }
                catch (Exception)
                {
                    Logger.Info($"ERROR: Unable to parse {featureToolframeextrarotationmatrix3x3} custom value.");
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the input of the processor are valid.
        ///     If <c>false</c> post processing and file generation will be canceled.
        /// </summary>
        /// <returns><c>true</c> if the input are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsProgramInputValid()
        {
            return true;
        }

        /// <summary>
        ///     Output all the points of the program.
        /// </summary>
        internal virtual void OuputAllPoints()
        {
            this.CacheProgramData(this.ProgramNode);
            this.RunBeforeProgramOutput();

            foreach (CbtNode operationNode in ProgramManager.GetOperationNodes(this.ProgramNode))
            {
                // Update the scene for each operation for proper data retrieval (point coordinates, part placement, TCP, tool,...)
                SceneManager.SetLayer(operationNode, this.SceneCbtRoot);

                this.CacheOperationData(operationNode);
                this.RunBeforeOperationOutput(operationNode);

                for (PathNode point = OperationManager.GetFirstPoint(operationNode); point != null; point = point.NextPoint(operationNode))
                {
                    this.CachePointDataBeforeOutput(point, operationNode);
                    this.RunBeforePointOutput(point, operationNode);

                    this.OutputMove(point, operationNode);

                    this.CachePointDataAfterOutput(point, operationNode);
                    this.RunAfterPointOutput(point, operationNode);
                }

                this.RunAfterOperationOutput(operationNode);
            }

            this.RunAfterProgramOutput();
        }

        /// <summary>
        ///     Gets a value indicating whether the processor output is valid.
        ///     If <c>false</c> the file generation will be canceled.
        /// </summary>
        /// <returns><c>true</c> if the processor output is valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsProgramOutputValid()
        {
            bool isOutputValid = this.ShouldAnyFileBeOverwritten();

            return isOutputValid;
        }

        /// <summary>
        ///     Gets a value indicating whether the post processor should overwrite existing files.
        /// </summary>
        /// <returns><c>true</c> if the post processor should overwrite existing files; otherwise, <c>false</c>.</returns>
        internal virtual bool ShouldAnyFileBeOverwritten()
        {
            if (Properties.Settings.Default.DisplayWarningOnFileOverride && this.AnyOutputFileAlreadyExist())
            {
                MessageWindowResult userInput = MessageWindow.Show("The destination already has file(s) with the same name(s)", "Overwrite");
                switch (userInput)
                {
                    case MessageWindowResult.Yes:
                        return true;
                    default:
                        this.NotifyUser("Posting canceled");
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the current output of the post processor would overwrite any existing file.
        /// </summary>
        /// <returns><c>true</c> if the processor would overwrite any existing files; otherwise, <c>false</c>.</returns>
        internal virtual bool AnyOutputFileAlreadyExist()
        {
            return AnyFileAlreadyExist(this.MainPostFile);

            bool AnyFileAlreadyExist(PostFile postFile)
            {
                if (postFile.Exist(this.ProgramFolderPath))
                {
                    return true;
                }

                foreach (PostFile child in postFile.Children)
                {
                    return AnyFileAlreadyExist(child);
                }

                return false;
            }
        }

        /// <summary>
        ///     Generates the robot code files.
        /// </summary>
        internal virtual void GenerateFiles()
        {
            this.MainPostFile.GenerateFiles(this.ProgramFolderPath); // Output framework 3
        }

        /// <summary>
        ///     Runs code before the program output.
        /// </summary>
        internal virtual void RunBeforeProgramOutput()
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Runs code after the program output.
        /// </summary>
        internal virtual void RunAfterProgramOutput()
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Runs code before the <paramref name="operationNode"/> output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void RunBeforeOperationOutput(CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Runs code after the <paramref name="operationNode"/> output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void RunAfterOperationOutput(CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Runs code before the <paramref name="point"/> output.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Runs code after the <paramref name="point"/> output.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Outputs all <see cref="Event"/>s of the selected <paramref name="point"/> <c>List&lt;Event&gt;</c>.
        ///     <para>
        ///         The <paramref name="eventIndex"/> defines which of <see cref="IEvents.EventListBefore"/>,
        ///         <see cref="IEvents.EventListInline"/> or <see cref="IEvents.EventListAfter"/> to output.
        ///     </para>
        ///     <para>
        ///         The <paramref name="eventIndex"/> defines which of <c>OutputBeforePointEvent</c>,
        ///         <c>OutputBeforePointEvent</c> or <c>OutputAfterPointEvent</c> is used.
        ///     </para>
        ///     It is the responsibility of the derived <see cref="PostProcessor"/> class implementation to call this method.
        /// </summary>
        /// <param name="point">The point to read events from.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        /// <param name="eventIndex">Event index before/inline/after.</param>
        internal virtual void OutputAllSelectedEvents(PathNode point, CbtNode operationNode, EventIndex eventIndex)
        {
            if (point.Events() == null)
            {
                return;
            }

            switch (eventIndex)
            {
                case EventIndex.Before:

                    if (point.Events().EventListBefore != null)
                    {
                        foreach (Event eventBefore in point.Events().EventListBefore)
                        {
                            this.OutputBeforePointEvent(eventBefore, point, operationNode);
                        }
                    }

                    break;

                case EventIndex.Inline:

                    if (point.Events().EventListInline != null)
                    {
                        foreach (Event eventInline in point.Events().EventListInline)
                        {
                            this.OutputInlineEvent(eventInline, point, operationNode);
                        }
                    }

                    break;

                case EventIndex.After:

                    if (point.Events().EventListAfter != null)
                    {
                        foreach (Event eventAfter in point.Events().EventListAfter)
                        {
                            this.OutputAfterPointEvent(eventAfter, point, operationNode);
                        }
                    }

                    break;
            }
        }

        /// <summary>
        ///     Output the "before point" <see cref="Event"/>.
        ///     This method is used for all <see cref="Event"/> of <see cref="IEvents.EventListBefore"/>.
        /// </summary>
        /// <param name="beforePointEvent">The event to output.</param>
        /// <param name="point">The point to which <paramref name="beforePointEvent"/> belongs.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Output the "inline" <see cref="Event"/>.
        ///     This method is used for all <see cref="Event"/> of <see cref="IEvents.EventListInline"/>.
        /// </summary>
        /// <param name="inlineEvent">The event to output.</param>
        /// <param name="point">The point to which <paramref name="inlineEvent"/> belongs.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Output the "after point" <see cref="Event"/>.
        ///     This method is used for all <see cref="Event"/> of <see cref="IEvents.EventListAfter"/>.
        /// </summary>
        /// <param name="afterPointEvent">The event to output.</param>
        /// <param name="point">The point to which <paramref name="afterPointEvent"/> belongs.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // To be overridden by brand implementation if needed.
            // Leave empty here.
        }

        /// <summary>
        ///     Output a move according to its <see cref="DeviceMotionType"/>.
        ///     <para><seealso cref="OutputJointSpaceMove"/></para>
        ///     <para><seealso cref="OutputJointMove"/></para>
        ///     <para><seealso cref="OutputLinearMove"/></para>
        ///     <para><seealso cref="OutputCircularMove"/></para>
        /// </summary>
        /// <param name="point">Point to output.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal virtual void OutputMove(PathNode point, CbtNode operationNode)
        {
            switch (point.PathSetting().MotionType)
            {
                case DeviceMotionType.JointSpaceMove:
                    this.OutputJointSpaceMove(point, operationNode);
                    break;
                case DeviceMotionType.JointMove:
                    this.OutputJointMove(point, operationNode);
                    break;
                case DeviceMotionType.LinearMove:
                    this.OutputLinearMove(point, operationNode);
                    break;
                case DeviceMotionType.ArcMove:
                    this.OutputCircularMove(point, operationNode);
                    break;
                case DeviceMotionType.SafeApproachMove:
                case DeviceMotionType.SafeRetractMove:
                    switch (point.MoveType())
                    {
                        case MoveType.Rapid:
                            this.OutputJointMove(point, operationNode);
                            break;
                        case MoveType.Linear:
                            this.OutputLinearMove(point, operationNode);
                            break;
                        default: throw new ArgumentOutOfRangeException($"Invalid motion type.");
                    }

                    break;
                default: throw new ArgumentOutOfRangeException($"Invalid motion type.");
            }
        }

        /// <summary>
        ///     Output linear move defined in Cartesian space (LC).
        /// </summary>
        /// <param name="point">Point to output.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal abstract void OutputLinearMove(PathNode point, CbtNode operationNode);

        /// <summary>
        ///     Output arc move defined in Cartesian space (CC).
        /// </summary>
        /// <param name="point">Point to output.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal abstract void OutputCircularMove(PathNode point, CbtNode operationNode);

        /// <summary>
        ///     Output joint move defined in Cartesian space (JC).
        /// </summary>
        /// <param name="point">Point to output.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal abstract void OutputJointMove(PathNode point, CbtNode operationNode);

        /// <summary>
        ///     Output joint move defined in joint space (JJ).
        /// </summary>
        /// <param name="point">Point to output.</param>
        /// <param name="operationNode">The operation to which <paramref name="point"/> belongs.</param>
        internal abstract void OutputJointSpaceMove(PathNode point, CbtNode operationNode);
    }
}