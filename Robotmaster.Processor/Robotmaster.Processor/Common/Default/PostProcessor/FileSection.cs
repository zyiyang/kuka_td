// <copyright file="FileSection.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///     Implements blocks files structure for robot code writing.
    /// </summary>
    internal class FileSection
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FileSection"/> class.
        /// </summary>
        internal FileSection()
        {
            this.StreamWriter = new StreamWriter(this.MemoryStream) { AutoFlush = true };
        }

        /// <summary>
        ///     Gets or sets the header.
        /// <para>
        ///     Must remain short. Use stream for long output.
        /// </para>
        /// </summary>
        internal string Header { get; set; } = string.Empty;

        /// <summary>
        ///     Gets or sets the footer.
        /// <para>
        ///     Must remain short. Use stream for long output.
        /// </para>
        /// </summary>
        internal string Footer { get; set; } = string.Empty;

        /// <summary>
        ///     Gets the MemoryStream.
        /// </summary>
        internal MemoryStream MemoryStream { get; } = new MemoryStream();

        /// <summary>
        ///     Gets the StreamWriter.
        /// </summary>
        internal StreamWriter StreamWriter { get; }

        /// <summary>
        ///     Gets the list of sub post blocks.
        /// </summary>
        internal List<FileSection> SubFileSections { get; } = new List<FileSection>();
    }
}