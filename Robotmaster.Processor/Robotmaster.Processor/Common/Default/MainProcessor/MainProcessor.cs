﻿// <copyright file="MainProcessor.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.MainProcessor
{
    using System.Collections.Generic;
    using System.Linq;
    using NLog;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.PathEngine.Core.Helpers;
    using Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.MyProcessorPackage;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Operation.Types;
    using Robotmaster.Rise.BaseClasses.Robot;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;
    using Robotmaster.Rise.Modules.RiseWindow;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    /// Main processor base class.
    /// Contains the methods executed when "Calculate" is clicked such as event creation automation.
    /// </summary>
    internal class MainProcessor
    {
        /// <summary>
        ///     Gets current class logger.
        /// </summary>
        internal static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///     Gets or sets the root of operation CBT (Component Based Tree).
        ///     This tree stores the data and the settings for each entity in Robotmaster hierarchies (tasks, programs, operations, etc.)
        ///     except the spatial data which is stored in the Scene CBT.
        /// <remarks>
        ///     No to be mistaken with OperationNode.
        /// </remarks>
        /// </summary>
        /// <seealso cref="SceneCbtRoot"/>
        internal virtual CbtNode OperationCbtRoot { get; set; }

        /// <summary>
        ///     Gets or sets the root of the Scene CBT (Component Based Tree).
        ///     This tree stores the spatial information including the models and relations between the physical elements
        ///     in the application (axes, models, frames, etc.).
        /// </summary>
        /// <seealso cref="OperationCbtRoot"/>
        internal virtual CbtNode SceneCbtRoot { get; set; }

        /// <summary>
        ///     Gets or sets the robot formatter.
        ///     <para>Provides all the brand formatting methods used by Robotmaster UI.</para>
        /// <remarks>
        ///     No to be mistaken with FormatManagers which can implement additional formatting methods for the processor package.
        /// </remarks>
        /// </summary>
        internal virtual RobotFormatter RobotFormatter { get; set; }

        /// <summary>
        ///     Gets or sets the processor package behavior.
        /// </summary>
        internal virtual MyProcessorPackageBehavior ProcessorPackageBehavior { get; set; }

        /// <summary>
        ///     Gets or sets the joint format dictionary: the pairs of joint name (key) and index (value).
        ///     <para>It is to be used to retrieve the joint value in <c>point.JointValues()</c>.</para>
        /// </summary>
        /// <example>
        ///     <code>
        ///     point.JointValues()[JointFormat["J1"]]
        ///     // or
        ///     point.JointValues(JointFormat["J1"])
        ///     </code>
        /// </example>
        internal virtual Dictionary<string, int> JointFormat { get; set; }

        /// <summary>
        /// Gets or sets the cell.
        /// </summary>
        internal virtual CbtNode CellSettingsNode { get; set; }

        /// <summary>
        /// Gets or sets the current setup.
        /// </summary>
        internal virtual CbtNode SetupNode { get; set; }

        /// <summary>
        /// Gets or sets the current program.
        /// </summary>
        internal virtual CbtNode ProgramNode { get; set; }

        /// <summary>
        /// Gets or sets the current process.
        /// </summary>
        internal virtual Process Process { get; set; }

        /// <summary>
        /// Gets or sets the matrix representing the tool mounting orientation.
        /// </summary>
        internal virtual Matrix3X3 ToolMountingOrientation { get; set; }

        /// <summary>
        /// Gets or sets the tooling activation/deactivation mode.
        /// <para>index="0" No tooling activation.</para>
        /// <para>index="1" Activate/Deactivate at the start and end of file.</para>
        /// <para>index="2" Activate/Deactivate with tool call.</para>
        /// <para>index="3" Activate/Deactivate at every path.</para>
        /// </summary>
        internal virtual int ToolingActivationDeactivationCondition { get; set; }

        /// <summary>
        /// Gets or sets the tooling activation macro.
        /// </summary>
        internal virtual string ToolingActivationMacro { get; set; } = "TOOLON";

        /// <summary>
        /// Gets or sets the tooling deactivation macro.
        /// </summary>
        internal virtual string ToolingDeactivationMacro { get; set; } = "TOOLOFF";

        /// <summary>
        /// Gets or sets the tool change macro.
        /// </summary>
        internal virtual string ToolChangeMacro { get; set; } = "TOOLCHANGE";

        /// <summary>
        ///  Gets or sets a value indicating whether operationNode is in multi tool program.
        /// </summary>
        internal virtual bool IsOperationInMultiToolProgram { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether the <see cref="ToolChange"/> event creation is forced.
        /// </summary>
        internal virtual bool IsToolChangeEventForced { get; set; } = false;

        /// <summary>
        ///  Gets or sets a value indicating whether the first tool change is output before the home operation.
        /// </summary>
        internal virtual bool IsFirstToolChangeOutputBeforeHome { get; set; } = true;

        /// <summary>
        /// Gets or sets the tooling activation condition.
        /// <para>index="0" Before the linear plunge move.</para>
        /// <para>index="1" After the linear plunge move.</para>
        /// <para>index="2" Before the first point of contact with part.</para>
        /// <para>index="3" After the first point of contact with part.</para>
        /// <para>index="4" Before the first none joint move.</para>
        /// <para>index="5" After the first none joint move.</para>
        /// <para>index="6" Before the first point of the operation.</para>
        /// <para>index="7" After the first point of the operation.</para>
        /// </summary>
        internal virtual int ToolingActivationCondition { get; set; }

        /// <summary>
        /// Gets or sets the  tooling deactivation condition.
        /// <para>index="0" Before the linear retract move.</para>
        /// <para>index="1" After the linear retract move.</para>
        /// <para>index="2" Before the last point of contact with part.</para>
        /// <para>index="3" After the last point of contact with part.</para>
        /// <para>index="4" Before the last none joint move.</para>
        /// <para>index="5" After the last none joint move.</para>
        /// <para>index="6" Before the last point of the operation.</para>
        /// <para>index="7" After the last point of the operation.</para>
        /// </summary>
        internal virtual int ToolingDeactivationCondition { get; set; }

        /// <summary>
        ///     Notify the user with a message.
        /// </summary>
        /// <param name="message">The message to be displayed to the user.</param>
        /// <param name="outputInLogger">Indicates whether the message will also be used to output in the Logger.</param>
        /// <param name="autoDismiss">Indicates whether the notification will auto dismiss.</param>
        internal virtual void NotifyUser(string message, bool outputInLogger = false, bool autoDismiss = true)
        {
            ////this.NotificationItems.Add(new NotificationViewModel { Caption = message, AutoDismiss = autoDismiss });
            this.ProcessorPackageBehavior.Broadcast(autoDismiss ? RiseBehavior.MessageDisplayMessage : RiseBehavior.MessageDisplayStickyMessage, message);

            if (outputInLogger)
            {
                Logger.Info(message);
            }
        }

        /// <summary>
        ///     Runs the main processor entry method.
        /// </summary>
        /// <seealso cref="EditOperationBeforeAllPointEdits"/>
        /// <seealso cref="EditAllPoints"/>
        /// <seealso cref="EditOperationAfterAllPointEdits"/>
        /// <param name="operationNode">The operation to be processed.</param>
        internal virtual void Run(CbtNode operationNode)
        {
            this.CellSettingsNode = CellManager.GetCellSettingsNode(this.OperationCbtRoot);
            this.SetupNode = OperationManager.GetSetupNode(operationNode);
            this.ProgramNode = OperationManager.GetProgramNode(operationNode);
            this.Process = SetupManager.GetConfiguration(this.SetupNode).Process;
            this.JointFormat = SetupManager.GetJointFormat(this.SetupNode);
            this.ToolMountingOrientation = CellManager.GetToolMountingOrientation(this.OperationCbtRoot);

            if (ProgramManager.GetOperationNodes(this.ProgramNode).Select(OperationManager.GetTool).Distinct().Count() != 1)
            {
                this.IsOperationInMultiToolProgram = true;
            }

            this.ReadToolActivationMenu(operationNode);

            this.EditOperationBeforeAllPointEdits(operationNode);
            this.EditAllPoints(operationNode);
            this.EditOperationAfterAllPointEdits(operationNode);
        }

        /// <summary>
        ///     Reads Tooling activation and deactivation settings <seealso cref="CommonToolingActivationSettings"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void ReadToolActivationMenu(CbtNode operationNode)
        {
            if (CommonToolingActivationSettings.IsLoaded(operationNode))
            {
                this.ToolingActivationDeactivationCondition = CommonToolingActivationSettings.GetToolingActivationDeactivationCondition(operationNode);
                this.ToolingActivationMacro = CommonToolingActivationSettings.GetToolingActivationMacro(operationNode);
                this.ToolingDeactivationMacro = CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode);
                this.ToolingActivationCondition = CommonToolingActivationSettings.GetToolingActivationCondition(operationNode);
                this.ToolingDeactivationCondition = CommonToolingActivationSettings.GetToolingDeactivationCondition(operationNode);
            }
            else
            {
                this.NotifyUser("WARNING: Default tooling activation settings were not loaded.", true);
            }
        }

        /// <summary>
        ///     Edits an operation before the editing all its points by <seealso cref="EditAllPoints"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            CbtNode[] allProgramOperations = ProgramManager.GetOperationNodes(this.ProgramNode).ToArray();

            //// If this operationNode is the first operationNode
            if (operationNode == allProgramOperations.First())
            {
                this.EditFirstOperation(operationNode);
            }

            //// If this operation is the first none home operation
            var firstNoneHomeOperation = allProgramOperations.First(op => OperationManager.GetOperationType(op) != OperationType.Home);
            if (operationNode == firstNoneHomeOperation)
            {
                this.EditFirstNoneHomeOperation(operationNode);
            }

            //// If the previous operationNode and the current operationNode are using different tool.
            var previousOperation = OperationManager.GetPreviousOperationNodeInProgram(operationNode);
            if ((previousOperation != null) && (OperationManager.GetTool(operationNode) != OperationManager.GetTool(previousOperation)))
            {
                this.EditFirstOperationAfterToolChange(operationNode);
            }

            //// If this operation is the last none home operation
            var lastNoneHomeOperation = allProgramOperations.Last(op => OperationManager.GetOperationType(op) != OperationType.Home);
            if (operationNode == lastNoneHomeOperation)
            {
                this.EditLastNoneHomeOperation(operationNode);
            }

            //// If this operationNode is the first operationNode
            if (operationNode == allProgramOperations.Last())
            {
                this.EditLastOperation(operationNode);
            }
        }

        /// <summary>
        ///     Edits an operation if it is the first operation.
        ///     <para>Occurs before the editing of all its points by <seealso cref="EditAllPoints"/>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditFirstOperation(CbtNode operationNode)
        {
            //// Tool Change (before home)
            if (this.IsFirstToolChangeOutputBeforeHome && (this.IsOperationInMultiToolProgram || this.IsToolChangeEventForced))
            {
                this.ToolChangeMacro = "TOOL(" + OperationManager.GetTcpId(operationNode) + ")"; // Default tool change name : meant to be overridden following brand convention
                PathNode firstPoint = OperationManager.GetFirstPoint(operationNode);
                this.AddToolChangeMacroEvent(operationNode, firstPoint);
            }
        }

        /// <summary>
        ///     Edits an operation if it is the first non home operation.
        ///     <para>Occurs before the editing of all its points by <seealso cref="EditAllPoints"/>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditFirstNoneHomeOperation(CbtNode operationNode)
        {
            //// Tool Change (after home)
            if (!this.IsFirstToolChangeOutputBeforeHome && (this.IsOperationInMultiToolProgram || this.IsToolChangeEventForced))
            {
                this.ToolChangeMacro = "TOOL(" + OperationManager.GetTcpId(operationNode) + ")"; // Default tool change name : meant to be overridden following brand convention
                PathNode firstPoint = OperationManager.GetFirstPoint(operationNode);
                this.AddToolChangeMacroEvent(operationNode, firstPoint);
            }

            //// Tooling activation / deactivation
            //// at start and end
            //// and with tool call
            if (this.ToolingActivationDeactivationCondition == 1 || this.ToolingActivationDeactivationCondition == 2)
            {
                //// Get the path engine of an operationNode.
                PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);
                //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                string lastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngine);
                IEnumerable<PathNode> operationPoints = pathEngine.GetNodes(lastValidExistingStageKey, false).ToIEnumerable();

                PathNode point = operationPoints
                    .FirstOrDefault(firstPoint => this.IsToolActivationMove(operationNode, firstPoint));
                if (point != null)
                {
                    this.EditToolingActivationPoint(operationNode, point);
                }
            }
        }

        /// <summary>
        ///     Edits an operation if it is the first operation after a tool change.
        ///     <para>Does not occurs for the first none home operation see <see cref="EditFirstNoneHomeOperation"/>.</para>
        ///     <para>Occurs before the editing of all its points by <seealso cref="EditAllPoints"/>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditFirstOperationAfterToolChange(CbtNode operationNode)
        {
            //// Tool Change
            if (this.IsOperationInMultiToolProgram)
            {
                this.ToolChangeMacro = "TOOL(" + OperationManager.GetTcpId(operationNode) + ")"; // Default tool change name : meant to be overridden following brand convention
                PathNode firstPoint = OperationManager.GetFirstPoint(operationNode);
                this.AddToolChangeMacroEvent(operationNode, firstPoint);
            }

            //// Tooling activation / deactivation
            //// with tool change
            if (this.ToolingActivationDeactivationCondition == 2)
            {
                var previousOperation = OperationManager.GetPreviousOperationNodeInProgram(operationNode);
                if (previousOperation != null)
                {
                    //// Get the path engine of an operation.
                    var pathEngineFromPreviousOperation = OperationManager.GetPathEngine(previousOperation);
                    //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                    //// Get nodes in reversed order to optimize search.
                    string previousOperationLastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngineFromPreviousOperation);
                    var operationPointsFromPreviousOperation = pathEngineFromPreviousOperation.GetNodes(previousOperationLastValidExistingStageKey, true).ToIEnumerable();

                    PathNode pointFromPreviousOperation = operationPointsFromPreviousOperation
                        .FirstOrDefault(lastPoint => this.IsToolDeactivationMove(previousOperation, lastPoint));
                    if (pointFromPreviousOperation != null)
                    {
                        this.EditToolingDeactivationPoint(previousOperation, pointFromPreviousOperation);
                    }
                }

                //// Get the path engine of an operation.
                PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);
                //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                string lastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngine);
                IEnumerable<PathNode> operationPoints = pathEngine.GetNodes(lastValidExistingStageKey, false).ToIEnumerable();

                PathNode point = operationPoints
                    .FirstOrDefault(firstPoint => this.IsToolActivationMove(operationNode, firstPoint));

                if (point != null)
                {
                    this.EditToolingActivationPoint(operationNode, point);
                }
            }
        }

        /// <summary>
        ///     Edits an operation if it is the last operation.
        ///     <para>Occurs before the editing of all its points by <seealso cref="EditAllPoints"/>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditLastNoneHomeOperation(CbtNode operationNode)
        {
            //// Tooling activation / deactivation
            //// at start and end
            //// and with tool call.
            if (this.ToolingActivationDeactivationCondition == 1 || this.ToolingActivationDeactivationCondition == 2)
            {
                //// Get the path engine of an operationNode.
                PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);
                //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                //// Get nodes in reversed order to optimize search.
                string lastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngine);
                IEnumerable<PathNode> operationPoints = pathEngine.GetNodes(lastValidExistingStageKey, true).ToIEnumerable();

                var point = operationPoints
                    .FirstOrDefault(lastPoint => this.IsToolDeactivationMove(operationNode, lastPoint));
                if (point != null)
                {
                    this.EditToolingDeactivationPoint(operationNode, point);
                }
            }
        }

        /// <summary>
        ///     Edits an operation if it is the last operation.
        ///     <para>Occurs before the editing of all its points by <seealso cref="EditAllPoints"/>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditLastOperation(CbtNode operationNode)
        {
        }

        /// <summary>
        ///     Edits an operation after the editing of all its points by <seealso cref="EditAllPoints"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditOperationAfterAllPointEdits(CbtNode operationNode)
        {
        }

        /// <summary>
        ///     Edits all points of an operation.
        /// </summary>
        /// <seealso cref="EditPoint"/>
        /// <param name="operationNode">The operation.</param>
        internal virtual void EditAllPoints(CbtNode operationNode)
        {
            for (PathNode point = OperationManager.GetFirstPoint(operationNode); point != null; point = point.NextPoint(operationNode))
            {
                point = this.EditPoint(operationNode, point);
            }
        }

        /// <summary>
        ///     Edits a point.
        ///     This method is called at every point of <paramref name="operationNode"/> (<seealso cref="EditAllPoints"/>).
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditPoint(CbtNode operationNode, PathNode point)
        {
            // Assign previous point and next point for later use
            var previousPoint = point.PreviousPoint(operationNode);
            var nextPoint = point.NextPoint(operationNode);

            // First none rapid detection
            if (this.IsFirstNoneRapidMove(operationNode, point, previousPoint, nextPoint))
            {
                point = this.EditFirstNoneRapidMove(operationNode, point);
            }

            // Linear plunge detection
            if (this.IsPlungeMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation))
            {
                point = this.EditPlungeMove(operationNode, point);
            }

            // First point of contact with part detection
            if (this.IsFirstPointOfContact(operationNode, point, previousPoint, nextPoint))
            {
                point = this.EditFirstPointOfContact(operationNode, point);
            }

            // Last point of contact with part detection
            if (this.IsLastPointOfContact(operationNode, point, previousPoint, nextPoint))
            {
                point = this.EditLastPointOfContact(operationNode, point);
            }

            // Linear retract detection
            if (this.IsRetractMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation))
            {
                point = this.EditRetractMove(operationNode, point);
            }

            // Last none rapid detection
            if (this.IsLastNoneRapidMove(operationNode, point, previousPoint, nextPoint))
            {
                point = this.EditLastNoneRapidMove(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits plunge moves.
        ///     <para>This method is called in <see cref="EditPoint"/> when <see cref="IsPlungeMove"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditPlungeMove(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingActivationCondition == 0 || this.ToolingActivationCondition == 1))
            {
                point = this.EditToolingActivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits retract moves.
        ///     <para>This method is called in <see cref="EditPoint"/> when <see cref="IsRetractMove"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditRetractMove(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingDeactivationCondition == 0 || this.ToolingDeactivationCondition == 1))
            {
                point = this.EditToolingDeactivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits first point of contact.
        ///     <para>This method is called in <see cref="EditPoint"/> when <see cref="IsFirstPointOfContact"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingActivationCondition == 2 || this.ToolingActivationCondition == 3))
            {
                point = this.EditToolingActivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits last point of contact.
        ///     <para>This method is called in <see cref="EditPoint"/> when <see cref="IsLastPointOfContact"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingDeactivationCondition == 2 || this.ToolingDeactivationCondition == 3))
            {
                point = this.EditToolingDeactivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits first none rapid moves.
        /// <para>This method is called in <see cref="EditPoint"/> when <see cref="IsLastNoneRapidMove"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditFirstNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingActivationCondition == 4 || this.ToolingActivationCondition == 5))
            {
                point = this.EditToolingActivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Edits last none rapid moves.
        /// <para>This method is called in <see cref="EditPoint"/> when <see cref="IsLastNoneRapidMove"/> is <c>true</c>.</para>
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal virtual PathNode EditLastNoneRapidMove(CbtNode operationNode, PathNode point)
        {
            if (this.ToolingActivationDeactivationCondition == 3 && (this.ToolingDeactivationCondition == 4 || this.ToolingDeactivationCondition == 5))
            {
                point = this.EditToolingDeactivationPoint(operationNode, point);
            }

            return point;
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> meets the selected <see cref="ToolingActivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> meets the selected <see cref="ToolingActivationCondition"/>; otherwise, <c>false</c>.</returns>
        internal virtual bool IsToolActivationMove(CbtNode operationNode, PathNode point)
        {
            var previousPoint = point.PreviousPoint(operationNode);
            var nextPoint = point.NextPoint(operationNode);

            return this.IsToolActivationMove(operationNode, point, previousPoint, nextPoint);
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> meets the selected <see cref="ToolingActivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> meets the selected <see cref="ToolingActivationCondition"/>; otherwise, <c>false</c>.</returns>
        internal virtual bool IsToolActivationMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            switch (this.ToolingActivationCondition)
            {
                case 0: // Before the linear plunge move
                    return this.IsPlungeMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation);
                case 1: // After the linear plunge move"
                    return this.IsPlungeMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation);
                case 2: // Before the first point of contact with part"
                    return this.IsFirstPointOfContact(operationNode, point, previousPoint, nextPoint);
                case 3: // After the first point of contact with part"
                    return this.IsFirstPointOfContact(operationNode, point, previousPoint, nextPoint);
                case 4: // Before the first none rapid move"
                    return this.IsFirstNoneRapidMove(operationNode, point, previousPoint, nextPoint);
                case 5: // After the first none rapid move"
                    return this.IsFirstNoneRapidMove(operationNode, point, previousPoint, nextPoint);
                case 6: // Before the first point of the operationNode
                    return OperationManager.GetFirstPoint(operationNode) == point;
                case 7: // After the first point of the operationNode
                    return OperationManager.GetFirstPoint(operationNode) == point;
            }

            return false;
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> meets the selected <see cref="ToolingDeactivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> meets the selected <see cref="ToolingDeactivationCondition"/>; otherwise, <c>false</c>.</returns>
        internal virtual bool IsToolDeactivationMove(CbtNode operationNode, PathNode point)
        {
            var previousPoint = point.PreviousPoint(operationNode);
            var nextPoint = point.NextPoint(operationNode);

            return this.IsToolDeactivationMove(operationNode, point, previousPoint, nextPoint);
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> meets the selected <see cref="ToolingDeactivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> meets the selected <see cref="ToolingDeactivationCondition"/>; otherwise, <c>false</c>.</returns>
        internal virtual bool IsToolDeactivationMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            switch (this.ToolingDeactivationCondition)
            {
                case 0: // Before the linear retract move
                    return this.IsRetractMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation);
                case 1: // After the linear retract move"
                    return this.IsRetractMove(operationNode, point, previousPoint, nextPoint, this.ToolMountingOrientation);
                case 2: // Before the last point of contact with part"
                    return this.IsLastPointOfContact(operationNode, point, previousPoint, nextPoint);
                case 3: // After the last point of contact with part"
                    return this.IsLastPointOfContact(operationNode, point, previousPoint, nextPoint);
                case 4: // Before the last none rapid move"
                    return this.IsLastNoneRapidMove(operationNode, point, previousPoint, nextPoint);
                case 5: // After the last none rapid move"
                    return this.IsLastNoneRapidMove(operationNode, point, previousPoint, nextPoint);
                case 6: // Before the last point of the operationNode
                    return OperationManager.GetLastPoint(operationNode) == point;
                case 7: // After the last point of the operationNode
                    return OperationManager.GetLastPoint(operationNode) == point;
            }

            return false;
        }

        /// <summary>
        ///     Adds tool change event to the point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The point to which the <see cref="ToolChange"/> has been added.</returns>
        internal virtual PathNode AddToolChangeMacroEvent(CbtNode operationNode, PathNode point)
        {
            point = PointManager.AddEventToPathPoint(new ToolChange { ProcessActivationCommand = this.ToolChangeMacro }, EventIndex.Before, point, operationNode);
            return point;
        }

        /// <summary>
        ///     Edits the tooling activation point according to <see cref="ToolingActivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The point to which <see cref="ToolOn"/> has been added.</returns>
        internal virtual PathNode EditToolingActivationPoint(CbtNode operationNode, PathNode point)
        {
            if (OperationManager.GetOperationType(operationNode) == OperationType.Transition)
            {
                // Skips transition operation points.
                return point; // TODO: PROC-318 to be reviewed once macro tool change are implemented
            }

            switch (this.ToolingActivationCondition)
            {
                case 0: // Before the linear plunge move
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 1: // After the linear plunge move
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 2: // Before the first point of contact with part
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 3: // After the first point of contact with part
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 4: // Before the first none rapid move
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 5: // After the first none rapid move
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 6: // Before the first point of the operationNode
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 7: // After the first point of the operationNode
                    point = this.AddToolingActivationEvent(operationNode, point, EventIndex.After);
                    break;
            }

            return point;
        }

        /// <summary>
        ///     Adds tooling activation event to the specified point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The index of the event.</param>
        /// <returns>The point to which the tool activation event has been added.</returns>
        internal virtual PathNode AddToolingActivationEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new ToolOn
                {
                    ProcessActivationCommand = this.ToolingActivationMacro,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Edits the tooling deactivation point according to <see cref="ToolingDeactivationCondition"/>.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The point to which <see cref="ToolOff"/> has been added.</returns>
        internal virtual PathNode EditToolingDeactivationPoint(CbtNode operationNode, PathNode point)
        {
            if (OperationManager.GetOperationType(operationNode) == OperationType.Transition)
            {
                // Skips transition operation point.
                return point; // TODO: PROC-318 to be reviewed once macro tool change are implemented
            }

            switch (this.ToolingDeactivationCondition)
            {
                case 0: // Before the linear retract move
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 1: // After the linear retract move"
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 2: // Before the last point of contact with part"
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 3: // After the last point of contact with part"
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 4: // Before the last none rapid move"
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 5: // After the last none rapid move"
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.After);
                    break;
                case 6: // Before the last point of the operationNode
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.Before);
                    break;
                case 7: // After the last point of the operationNode
                    point = this.AddToolingDeactivationEvent(operationNode, point, EventIndex.After);
                    break;
            }

            return point;
        }

        /// <summary>
        ///     Adds tooling deactivation event to the specified point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The index of the event.</param>
        /// <returns>The point to which the tool deactivation event has been added.</returns>
        internal virtual PathNode AddToolingDeactivationEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new ToolOff
                {
                    ProcessDeactivationCommand = this.ToolingDeactivationMacro,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> is a first none rapid move.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> is a first none rapid move; otherwise, <c>false</c>.</returns>
        internal virtual bool IsFirstNoneRapidMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            return
                //// First none rapid detection
                //// Still catch if the first point of the operation is not a rapid move
                point.MoveType() != MoveType.Rapid
                && (previousPoint == null || previousPoint.MoveType() == MoveType.Rapid);
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> is a last none rapid move.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> is a first none rapid move; otherwise, <c>false</c>.</returns>
        internal virtual bool IsLastNoneRapidMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            return
                //// Last none rapid detection
                //// Still catch if the last point of the operation is not a rapid move
                point.MoveType() != MoveType.Rapid
                && (nextPoint == null || nextPoint.MoveType() == MoveType.Rapid);
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> is a plunge move.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <param name="toolMountingOrientation">The matrix representing the tool mounting orientation.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> is a plunge move; otherwise, <c>false</c>.</returns>
        internal virtual bool IsPlungeMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint, Matrix3X3 toolMountingOrientation)
        {
            if (previousPoint == null)
            {
                return false;
            }

            Vector3 previousPointPathDirection = previousPoint.PathSetting().PathDirection;
            Vector3 previousPointToolVector = Matrix3X3.TransposeAndMultiply(toolMountingOrientation, previousPoint.TcpOrientation().GetColumn(2));
            Vector3 currentPointPathDirection = point.PathSetting().PathDirection;

            // Check that no vector is a zero vector
            if (previousPointPathDirection != default(Vector3)
                && previousPointToolVector != default(Vector3)
                && currentPointPathDirection != default(Vector3))
            {
                return
                    //// Linear plunge detection
                    Vector3.AngleBetween(previousPointPathDirection, previousPointToolVector) < 0.1 &&
                    Vector3.AngleBetween(previousPointPathDirection, currentPointPathDirection) > 5;
            }

            return false;
        }

        /// <summary>
        ///     Gets a value indicating whether the move toward <paramref name="point" /> is a retract move.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <param name="toolMountingOrientation">The matrix representing the tool mounting orientation.</param>
        /// <returns><c>true</c> if the move toward <paramref name="point" /> is a retract move; otherwise, <c>false</c>.</returns>
        internal virtual bool IsRetractMove(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint, Matrix3X3 toolMountingOrientation)
        {
            if (previousPoint == null)
            {
                return false;
            }

            Vector3 previousPointPathDirection = previousPoint.PathSetting().PathDirection;
            Vector3 currentPointPathDirection = point.PathSetting().PathDirection;
            Vector3 currentPointToolVector = Matrix3X3.TransposeAndMultiply(toolMountingOrientation, point.TcpOrientation().GetColumn(2));

            // Check that no vector is a zero vector
            if (previousPointPathDirection != default(Vector3)
                && previousPointPathDirection != default(Vector3)
                && currentPointPathDirection != default(Vector3))
            {
                return
                    //// Linear retract detection
                    Vector3.AngleBetween(-currentPointPathDirection, currentPointToolVector) < 0.1 &&
                    Vector3.AngleBetween(currentPointPathDirection, previousPointPathDirection) > 5;
            }

            return false;
        }

        /// <summary>
        ///     Gets a value indicating whether <paramref name="point" /> is a first point of contact.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if <paramref name="point" /> is a first point of contact; otherwise, <c>false</c>.</returns>
        internal virtual bool IsFirstPointOfContact(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            return
                //// First point of contact with part detection
                (point.PathFlag() == PathFlag.OutsideOfProcess &&
                (nextPoint != null &&
                 nextPoint.PathFlag() != PathFlag.OutsideOfProcess))
                 //// Still catch if the first point of the operationNode is in process
                 || (previousPoint == null && point.PathFlag() != PathFlag.OutsideOfProcess);
        }

        /// <summary>
        ///     Gets a value indicating whether <paramref name="point" /> is a last point of contact.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="previousPoint">The previous point.</param>
        /// <param name="nextPoint">The next point.</param>
        /// <returns><c>true</c> if <paramref name="point" /> is a last point of contact; otherwise, <c>false</c>.</returns>
        internal virtual bool IsLastPointOfContact(CbtNode operationNode, PathNode point, PathNode previousPoint, PathNode nextPoint)
        {
            return
                //// Last point of contact with part detection
                point.PathFlag() != PathFlag.OutsideOfProcess &&
                (nextPoint == null ||
                 nextPoint.PathFlag() == PathFlag.OutsideOfProcess);
        }

        /// <summary>
        ///     Gets a value indicating whether <paramref name="point" /> is a touch point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns><c>true</c> if <paramref name="point" /> is a first point of contact; otherwise, <c>false</c>.</returns>
        internal virtual bool IsTouchPoint(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            if (applicationType == ApplicationType.TouchSensing || (applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point).IsTouchSensingPoint))
            {
                PathNode previousPoint = point.PreviousPoint(operationNode);
                return
                    //// Touch point detection
                    (previousPoint == null || previousPoint.PathFlag() == PathFlag.OutsideOfProcess) &&
                    (point.PathFlag() != PathFlag.OutsideOfProcess);
            }
            else
            {
                return false;
            }
        }
    }
}