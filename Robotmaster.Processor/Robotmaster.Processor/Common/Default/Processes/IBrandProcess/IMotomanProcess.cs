﻿// <copyright file="IMotomanProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Motoman process.
    /// </summary>
    internal interface IMotomanProcess
    {
    }
}
