﻿// <copyright file="IIgmProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Igm process.
    /// </summary>
    internal interface IIgmProcess
    {
    }
}
