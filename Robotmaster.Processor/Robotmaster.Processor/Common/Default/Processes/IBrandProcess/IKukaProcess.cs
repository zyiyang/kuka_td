﻿// <copyright file="IKukaProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Kuka process.
    /// </summary>
    internal interface IKukaProcess
    {
    }
}
