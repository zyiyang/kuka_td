﻿// <copyright file="IUniversalRobotsProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Universal Robots process.
    /// </summary>
    internal interface IUniversalRobotsProcess
    {
    }
}
