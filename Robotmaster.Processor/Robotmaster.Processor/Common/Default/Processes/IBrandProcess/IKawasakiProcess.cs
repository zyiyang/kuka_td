﻿// <copyright file="IKawasakiProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Kawasaki process.
    /// </summary>
    internal interface IKawasakiProcess
    {
    }
}
