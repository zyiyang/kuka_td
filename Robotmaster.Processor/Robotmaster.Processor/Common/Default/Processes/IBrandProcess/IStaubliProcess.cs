﻿// <copyright file="IStaubliProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.IBrandProcess
{
    /// <summary>
    ///     Staubli process.
    /// </summary>
    internal interface IStaubliProcess
    {
    }
}
