﻿// <copyright file="ToolChange.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <inheritdoc />
    /// <summary>
    /// Tool change event.
    /// </summary>
    [DataContract(Name = "ToolChange")]
    public class ToolChange : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolChange" /> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ToolChange()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        /// Gets or sets the tool change command.
        /// </summary>
        [DataMember(Name = "ProcessActivationCommand")]
        public virtual string ProcessActivationCommand { get; set; } = "TOOLCHANGE";

        /// <summary>
        /// Override of GetPointDisplayFormat method.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.ProcessActivationCommand}";
        }
    }
}
