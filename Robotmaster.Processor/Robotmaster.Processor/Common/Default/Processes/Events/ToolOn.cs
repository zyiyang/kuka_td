﻿// <copyright file="ToolOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Tool on event.
    /// </summary>
    [DataContract(Name = "ToolOn")]
    public class ToolOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ToolOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        /// Gets or sets the tooling activation command.
        /// </summary>
        [DataMember(Name = "ProcessActivationCommand")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string ProcessActivationCommand { get; set; } = "TOOLON";

        /// <summary>
        /// Override of GetPointDisplayFormat method.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.ProcessActivationCommand}";
        }
    }
}
