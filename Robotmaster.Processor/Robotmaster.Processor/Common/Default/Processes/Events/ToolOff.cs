﻿// <copyright file="ToolOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Tool Off event.
    /// </summary>
    [DataContract(Name = "ToolOff")]
    public class ToolOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ToolOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        /// Gets or sets the tooling deactivation command.
        /// </summary>
        [DataMember(Name = "ProcessDeactivationCommand")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public virtual string ProcessDeactivationCommand { get; set; } = "TOOLOFF";

        /// <summary>
        /// Override of GetPointDisplayFormat method.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.ProcessDeactivationCommand}";
        }
    }
}
