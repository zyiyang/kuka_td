﻿// <copyright file="PackageProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Default.Processes
{
    using System;
    using System.Runtime.Serialization;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     User defined process in the processor package.
    /// </summary>
    /// <inheritdoc/>
    [DataContract(Name = "PackageProcess")]
    public class PackageProcess : Process
    {
        /// <summary>
        ///     Gets or sets the type of the pre-processor to instantiate.
        /// </summary>
        internal Type PreProcessorType { get; set; } = null;

        /// <summary>
        ///     Gets or sets the type of the main processor to instantiate.
        /// </summary>
        internal Type MainProcessorType { get; set; } = null;

        /// <summary>
        ///    Gets or sets the type of the simulation processor to instantiate.
        /// </summary>
        internal Type SimulationProcessorType { get; set; } = null;

        /// <summary>
        ///     Gets or sets the type of the post-processor to instantiate.
        /// </summary>
        internal Type PostProcessorType { get; set; } = null;
    }
}