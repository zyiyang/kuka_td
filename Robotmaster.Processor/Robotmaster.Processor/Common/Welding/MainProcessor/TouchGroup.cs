﻿// <copyright file="TouchGroup.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Welding.MainProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    /// <summary>
    ///     The touch group data structure.
    /// </summary>
    internal class TouchGroup
    {
        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        internal string Type { get; set; }

        /// <summary>
        ///     Gets or sets the touch count.
        /// </summary>
        internal int TouchCount { get; set; }

        /// <summary>
        ///     Gets or sets the position.
        /// </summary>
        internal float Position { get; set; }

        /// <summary>
        ///     Gets or sets the path number.
        /// </summary>
        internal int PathNumber { get; set; }

        /// <summary>
        ///     Gets or sets the touch sense positions.
        /// </summary>
        internal List<Vector3> TouchSensePositions { get; set; } = new List<Vector3>();

        /// <summary>
        ///     Gets or sets the average touch sense position.
        /// </summary>
        internal Vector3 GetAverageTouchSensePosition()
        {
            return new Vector3(
                this.TouchSensePositions.Average(x => x.X),
                this.TouchSensePositions.Average(y => y.Y),
                this.TouchSensePositions.Average(z => z.Z));
        }

        /// <summary>
        ///     Gets or sets the distance between the point and the <see cref="TouchGroup.GetAverageTouchSensePosition()"/>.
        /// </summary>
        /// <param name="point">Current point.</param>
        internal double GetDistanceFromAverage(PathNode point)
        {
            Vector3 averageTouchSensePosition = this.GetAverageTouchSensePosition();
            return Math.Sqrt(
                Math.Pow(point.Position().X - averageTouchSensePosition.X, 2) +
                Math.Pow(point.Position().Y - averageTouchSensePosition.Y, 2) +
                Math.Pow(point.Position().Z - averageTouchSensePosition.Z, 2));
        }
    }
}
