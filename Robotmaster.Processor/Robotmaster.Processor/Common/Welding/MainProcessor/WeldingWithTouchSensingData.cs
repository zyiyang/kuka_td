﻿// <copyright file="WeldingWithTouchSensingData.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Common.Welding.MainProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Data;

    /// <summary>
    ///     The welding with embedded touch sensing data structure.
    /// </summary>
    internal class WeldingWithTouchSensingData
    {
        public WeldingWithTouchSensingData(WeldingWithTouchSensingApplicationData applicationData)
        {
            // Extract the touch sensing data.
            var touchSensingData = (Dictionary<string, object>)applicationData.Data["touchSensingData"];

            // Initialize version.
            this.VersionMajor = Convert.ToInt32(touchSensingData["versionMajor"]);
            this.VersionMinor = Convert.ToInt32(touchSensingData["versionMinor"]);

            if (this.VersionMajor == 1)
            {
                // Extract the touch group data.
                var touchGroupsData = (List<object>)touchSensingData["touchGroups"];

                // Build the touch sensing group data structure.
                foreach (object touchSenseGroup in touchGroupsData)
                {
                    var touchSense = (Dictionary<string, object>)touchSenseGroup;

                    this.TouchGroupsData.Add(
                        new TouchGroup
                        {
                            Type = Convert.ToString(touchSense["type"]),
                            TouchCount = Convert.ToInt32(touchSense["touchCount"]),
                            Position = Convert.ToSingle(touchSense["position"]),
                        });
                }
            }
        }

        /// <summary>
        ///     Gets or sets the version major.
        /// </summary>
        internal int VersionMajor { get; set; }

        /// <summary>
        ///     Gets or sets the version minor.
        /// </summary>
        internal int VersionMinor { get; set; }

        /// <summary>
        ///     Gets or sets the touch group data.
        /// </summary>
        protected List<TouchGroup> TouchGroupsData { get; set; } = new List<TouchGroup>();
    }
}
