// <copyright file="PostProcessorUniversalRobots.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.UniversalRobots.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.UniversalRobots.Default.Processes.Events;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    internal class PostProcessorUniversalRobots : PostProcessor
    {
        /// <summary>
        ///     Gets or sets the list of used user frames.
        /// </summary>
        internal virtual List<int> UsedUserFrameList { get; set; } = new List<int>();

        /// <summary>
        ///     Gets the moves sub <see cref="FileSection"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.StreamWriter;

        /// <summary>
        ///     Gets or sets a value indicating whether the move is a plunge or retract.
        /// </summary>
        internal virtual bool IsPlungeOrRetract { get; set; }

        /// <summary>
        ///     Gets or sets the Plunge/Retract Blend Radius.
        /// </summary>
        internal virtual double PlungeRetractBlendRadius { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the program is RTCP.
        /// </summary>
        internal virtual bool IsRtcp { get; set; }

        /// <summary>
        ///     Converts the input value from Degrees to Radians.
        /// </summary>
        /// <param name="input">The input value.</param>
        /// <returns>Input value in Radians.</returns>
        internal virtual double ToRad(double input) => input * Constants.DegToRad;

        /// <summary>
        ///     Converts the input value from Millimeters to Meters.
        /// </summary>
        /// <param name="input">The input value.</param>
        /// <returns>Input value in Meters.</returns>
        internal virtual double ToMeters(double input) => input / 1000;

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
            this.CurrentPostFile.FileExtension = FormatManagerUniversalRobots.MainProgramExtension;
            this.StartPostFile(this.CurrentPostFile);

            // Check if Setup is Rtcp
            this.IsRtcp = SetupManager.IsRtcp(this.SetupNode);

            // Clear list
            this.UsedUserFrameList.Clear(); // Coordinate systems are defined once and then called by their reference name. We need to clear the used User Frame list before starting each new program.
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="UniversalRobots"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void StartPostFile(PostFile postFile)
        {
            // Main section Header
            postFile.FileSection.Header +=
                "<URProgram createdIn=\"3.0.0.0\" lastSavedIn=\"3.0.0.0\" robotSerialNumber=\"\" name=\"" + postFile.FileName + "\" directory=\"\" installation=\"default\">" + "\r\n" +
                "  <children>" + "\r\n" +
                "    <MainProgram runOnlyOnce=\"true\">" + "\r\n" +
                "      <children>" + "\r\n" +
                "        <Script type=\"File\">" + "\r\n" +
                "          <cachedContents>" + "\r\n" +
                "            #" + postFile.FileName + "\r\n" +
                "            #Generated by Robotmaster" + "\r\n" + "\r\n";

            postFile.FileSection.Footer +=
                "          </cachedContents>" + "\r\n" +
                "          <file>" + this.ProcessorDateTime.ToString("yyyyMMdd") + "_" + this.ProcessorDateTime.ToString("HHmmss") + "-" + postFile.FileName + ".script" + "</file>" + "\r\n" +
                "        </Script>" + "\r\n" +
                "      </children>" + "\r\n" +
                "    </MainProgram>" + "\r\n" +
                "  </children>" + "\r\n" +
                "</URProgram>";
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            if (previousOperationNode == null)
            {
                this.Moves.Write(this.FormatCoordinateSystem(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode));
                this.Moves.Write(this.FormatToolFrame(operationNode));
            }
            else
            {
                if (OperationManager.GetUserFrame(previousOperationNode).Number != OperationManager.GetUserFrame(operationNode).Number)
                {
                    this.Moves.Write(this.FormatCoordinateSystem(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode));
                }

                if (OperationManager.GetTcpId(previousOperationNode) != OperationManager.GetTcpId(operationNode))
                {
                    this.Moves.Write(this.FormatToolFrame(operationNode));
                }
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Inline);
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Skip Tool Change Macro Output
            if (beforePointEvent.GetType() == typeof(ToolChange))
            {
                return;
            }

            // Output event
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            // Process PlungeMove Event
            if (inlineEvent.GetType() == typeof(Blend))
            {
                this.IsPlungeOrRetract = true;

                var blendEvent = (Blend)inlineEvent;
                this.PlungeRetractBlendRadius = blendEvent.BlendValue;
            }
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            // movel(pose_trans(Frame_1, p[0.145000, -0.044000, 0.000000, 2.221441, -2.221441, 0.000000]), a=1, v=68.1, r=0)
            this.Moves.Write(
                "            {0}(pose_trans(Frame_{1}, p[{2}{3}]), {4})\r\n",
                UniversalRobotsMotionSettings.GetUseMoveProcessCommand(operationNode) ? "movep" : "movel",
                !this.IsRtcp ? OperationManager.GetUserFrame(operationNode).Number : OperationManager.GetTcpId(operationNode),
                this.FormatPosition(point, operationNode),
                this.FormatOrientation(point, operationNode),
                this.FormatMotionParameters(point, operationNode));
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            PathNode previousPoint = point.PreviousPoint(operationNode);

            if (!point.IsArcMiddlePoint())
            {
                // movec(pose_trans(Frame_1, p[0.127929, -0.041071, 0.000000, 2.221441, -2.221441, 0.000000]),pose_trans(Frame_1, p[0.125000, -0.034000, 0.000000, 2.221441, -2.221441, 0.000000]), a=1, v=12.35, r=0)
                this.Moves.Write(
                    "            movec(pose_trans(Frame_{0}, p[{1}{2}]),pose_trans(Frame_{0}, p[{3}{4}]), {5})\r\n",
                    !this.IsRtcp ? OperationManager.GetUserFrame(operationNode).Number : OperationManager.GetTcpId(operationNode),
                    this.FormatPosition(previousPoint, operationNode),
                    this.FormatOrientation(previousPoint, operationNode),
                    this.FormatPosition(point, operationNode),
                    this.FormatOrientation(point, operationNode),
                    this.FormatMotionParameters(point, operationNode));
            }
        }

        /// <inheritdoc/>
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            // movej(get_inverse_kin(pose_trans(Frame_1, p[0.145000, -0.044000, 0.010000, 2.221441, -2.221441, 0.000000]), [0.0, -1.5707963267948966, 0.0, -1.5707963267948966, 2.248635305734936E-16, 0.0]), a=1.3, v=12.5, r=0)
            this.Moves.Write(
                "            movej(get_inverse_kin(pose_trans(Frame_{0}, p[{1}{2}]), [{3}]), {4})\r\n",
                !this.IsRtcp ? OperationManager.GetUserFrame(operationNode).Number : OperationManager.GetTcpId(operationNode),
                this.FormatPosition(point, operationNode),
                this.FormatOrientation(point, operationNode),
                this.FormatJointValues(point),
                this.FormatMotionParameters(point, operationNode));
        }

        /// <inheritdoc/>
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            // movej([0.000000, -1.570796, 1.570796, 0.000000, 1.570796, 1.570796], a=1, v=3.14, r=0)
            this.Moves.Write(
                "            movej([{0}], {1})\r\n",
                this.FormatJointValues(point),
                this.FormatMotionParameters(point, operationNode));
        }

        /// <summary>
        ///     Formats the Universal Robots Coordinate System Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        /// <returns>Formatted Universal Robots Coordinate System Output.</returns>
        internal virtual string FormatCoordinateSystem(CbtNode operationNode, CbtNode referenceFrameNode)
        {
            UserFrame userFrame = OperationManager.GetUserFrame(operationNode);
            Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrameNode);
            Vector4 angleAxis = ConvertRotationRepresentation.RotationMatrixtoLinearInvariants(userFrameMatrix.Orientation);

            if (!this.IsRtcp)
            {
                if (this.UsedUserFrameList.FindIndex(a => a == userFrame.Number) == -1)
                {
                    this.UsedUserFrameList.Add(userFrame.Number);

                    // Frame_1 = p[-0.600000, -0.257000, 0.340000, -0.359067, 0.148009, -0.161585]
                    return string.Format(
                        "            Frame_{0} = p[{1:0.000000}, {2:0.000000}, {3:0.000000}, {4:0.000000}, {5:0.000000}, {6:0.000000}]\r\n",
                        userFrame.Number,
                        this.ToMeters(userFrameMatrix.Position.X),
                        this.ToMeters(userFrameMatrix.Position.Y),
                        this.ToMeters(userFrameMatrix.Position.Z),
                        this.ToRad(angleAxis.X) * angleAxis.Y,
                        this.ToRad(angleAxis.X) * angleAxis.Z,
                        this.ToRad(angleAxis.X) * angleAxis.W);
                }
            }
            else
            {
                // set_tcp(p[-0.182818, 0.000000, 0.238388, 0.000000, -1.047207, 0.000000])
                return string.Format(
                    "            set_tcp(p[{0:0.000000}, {1:0.000000}, {2:0.000000}, {3:0.000000}, {4:0.000000}, {5:0.000000}])\r\n",
                    this.ToMeters(userFrameMatrix.Position.X),
                    this.ToMeters(userFrameMatrix.Position.Y),
                    this.ToMeters(userFrameMatrix.Position.Z),
                    this.ToRad(angleAxis.X) * angleAxis.Y,
                    this.ToRad(angleAxis.X) * angleAxis.Z,
                    this.ToRad(angleAxis.X) * angleAxis.W);
            }

            return string.Empty;
        }

        /// <summary>
        ///     Formats the Universal Robots Tool Frame Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Universal Robots Tool Frame Output.</returns>
        internal virtual string FormatToolFrame(CbtNode operationNode)
        {
            Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
            }

            Vector4 toolFrameOrientation = ConvertRotationRepresentation.RotationMatrixtoLinearInvariants(toolFrameMatrix.Orientation);

            if (!this.IsRtcp)
            {
                // set_tcp(p[-0.182818, 0.000000, 0.238388, 0.000000, -1.047207, 0.000000])
                return string.Format(
                    "            set_tcp(p[{0:0.000000}, {1:0.000000}, {2:0.000000}, {3:0.000000}, {4:0.000000}, {5:0.000000}])\r\n",
                    this.ToMeters(toolFrameMatrix.Position.X),
                    this.ToMeters(toolFrameMatrix.Position.Y),
                    this.ToMeters(toolFrameMatrix.Position.Z),
                    this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.Y,
                    this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.Z,
                    this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.W);
            }
            else
            {
                if (this.UsedUserFrameList.FindIndex(a => a == OperationManager.GetTcpId(operationNode)) == -1)
                {
                    this.UsedUserFrameList.Add(OperationManager.GetTcpId(operationNode));

                    // Frame_1 = p[-0.600000, -0.257000, 0.340000, -0.359067, 0.148009, -0.161585]
                    return string.Format(
                        "            Frame_{0} = p[{1:0.000000}, {2:0.000000}, {3:0.000000}, {4:0.000000}, {5:0.000000}, {6:0.000000}]\r\n",
                        OperationManager.GetTcpId(operationNode),
                        this.ToMeters(toolFrameMatrix.Position.X),
                        this.ToMeters(toolFrameMatrix.Position.Y),
                        this.ToMeters(toolFrameMatrix.Position.Z),
                        this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.Y,
                        this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.Z,
                        this.ToRad(toolFrameOrientation.X) * toolFrameOrientation.W);
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///     Formats position output at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Universal Robots formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Matrix4X4 pointMatrixInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);
            Vector3 pointPositionInUserFrame = !this.IsRtcp ? pointMatrixInUserFrame.Position : pointMatrixInUserFrame.GetInverse().Position;

            return string.Format(
                "{0:0.000000}, {1:0.000000}, {2:0.000000}, ",
                this.ToMeters(pointPositionInUserFrame.X),
                this.ToMeters(pointPositionInUserFrame.Y),
                this.ToMeters(pointPositionInUserFrame.Z));
        }

        /// <summary>
        ///     Formats orientation output.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Universal Robots formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // Gets the Euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector4 angleAxis = !this.IsRtcp ?
                ConvertRotationRepresentation.RotationMatrixtoLinearInvariants(pointFrameInUserFrame.Orientation) :
                ConvertRotationRepresentation.RotationMatrixtoLinearInvariants(pointFrameInUserFrame.GetInverse().Orientation);

            return string.Format(
                "{0:0.000000}, {1:0.000000}, {2:0.000000}",
                this.ToRad(angleAxis.X) * angleAxis.Y,
                this.ToRad(angleAxis.X) * angleAxis.Z,
                this.ToRad(angleAxis.X) * angleAxis.W);
        }

        /// <summary>
        ///     Formats joint values at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Universal Robots formatted joint values.</returns>
        internal virtual string FormatJointValues(PathNode point)
        {
            return string.Format(
                "{0:0.000000}, {1:0.000000}, {2:0.000000}, {3:0.000000}, {4:0.000000}, {5:0.000000}",
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name])),
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name])),
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name])),
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name])),
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name])),
                this.ToRad(point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name])));
        }

        /// <summary>
        ///     Formats the Universal Robots Motion Parameters.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Universal Robots Motion Parameters.</returns>
        internal virtual string FormatMotionParameters(PathNode point, CbtNode operationNode)
        {
            string output;

            if (point.MoveType() == MoveType.Rapid)
            {
                output = string.Format(
                    "a={0:0.#####}, v={1:0.#####}, r={2}",
                    this.ToRad(UniversalRobotsMotionSettings.GetJointAcceleration(operationNode)),
                    this.ToRad(UniversalRobotsMotionSettings.GetJointSpeed(operationNode)),
                    this.IsPlungeOrRetract && !UniversalRobotsMotionSettings.GetIsMotionBlendingForPlungeRetractMovesEnabled(operationNode) ? this.PlungeRetractBlendRadius : UniversalRobotsMotionSettings.GetIsJointMotionBlendingEnabled(operationNode) ? this.ToMeters(UniversalRobotsMotionSettings.GetMotionBlendRadiusForJointMove(operationNode)) : 0);
            }
            else
            {
                output = string.Format(
                    "a={0}, v={1:0.#####}, r={2}",
                    UniversalRobotsMotionSettings.GetTcpPathAcceleration(operationNode),
                    this.ToMeters(point.Feedrate().LinearFeedrate),
                    this.IsPlungeOrRetract && !UniversalRobotsMotionSettings.GetIsMotionBlendingForPlungeRetractMovesEnabled(operationNode) ? this.PlungeRetractBlendRadius : UniversalRobotsMotionSettings.GetIsLinearAndCircularMotionBlendingEnabled(operationNode) ? this.ToMeters(UniversalRobotsMotionSettings.GetMotionBlendRadiusForLinearAndCircularMove(operationNode)) : 0);
            }

            this.IsPlungeOrRetract = false;

            return output;
        }
    }
}
