﻿// <copyright file="FormatManagerUniversalRobots.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.PostProcessor
{
    /// <summary>
    /// Universal Robots Format Manager.
    /// </summary>
    internal static class FormatManagerUniversalRobots
    {
        /// <summary>
        /// Gets extension for the posted program.
        /// </summary>
        internal static string MainProgramExtension => ".urp";
    }
}
