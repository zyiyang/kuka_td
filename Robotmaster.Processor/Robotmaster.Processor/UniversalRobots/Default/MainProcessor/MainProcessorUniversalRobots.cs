﻿// <copyright file="MainProcessorUniversalRobots.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.UniversalRobots.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.UniversalRobots.Default.Processes.Events;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Universal Robots dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorUniversalRobots : MainProcessor
    {
        /// <summary>
        ///  Gets or sets a value indicating whether Blend event is to be added.
        /// </summary>
        internal virtual bool OutputBlendEvent { get; set; }

        /// <inheritdoc/>
        internal override PathNode EditPlungeMove(CbtNode operationNode, PathNode point)
        {
            point = this.AddBlendEvent(operationNode, point);

            point = base.EditPlungeMove(operationNode, point);

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditRetractMove(CbtNode operationNode, PathNode point)
        {
            PathNode previousPoint = point.PreviousPoint(operationNode);

            if (previousPoint != null)
            {
                previousPoint = this.AddBlendEvent(operationNode, previousPoint);
            }

            point = base.EditRetractMove(operationNode, point);

            return point;
        }

        /// <inheritdoc/>
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            this.OutputBlendEvent = UniversalRobotsMotionSettings.IsLoaded(operationNode) && !UniversalRobotsMotionSettings.GetIsMotionBlendingForPlungeRetractMovesEnabled(operationNode);

            base.EditOperationBeforeAllPointEdits(operationNode);
        }

        /// <summary>
        ///     Adds <see cref="Blend"/> event to the point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The point to which <see cref="Blend"/> has been added.</returns>
        internal virtual PathNode AddBlendEvent(CbtNode operationNode, PathNode point)
        {
            if (this.OutputBlendEvent)
            {
                point = PointManager.AddEventToPathPoint(new Blend { }, EventIndex.Inline, point, operationNode);
            }

            return point;
        }
    }
}
