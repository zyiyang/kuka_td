// <copyright file="UniversalRobotsMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     UniversalRobotsMotionSettingsViewModel View Model.
    /// </summary>
    public class UniversalRobotsMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;

        /// <summary>
        ///     Gets or sets the leading joint axis speed (deg/s).
        /// </summary>
        public double JointSpeed
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the leading joint axis acceleration (deg/s²).
        /// </summary>
        public double JointAcceleration
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint motion blend radius.
        /// </summary>
        public double MotionBlendRadiusForJointMove
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.MotionBlendRadiusForJointMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.MotionBlendRadiusForJointMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear/circular motion blend radius.
        /// </summary>
        public double MotionBlendRadiusForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.MotionBlendRadiusForLinearAndCircularMove)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.MotionBlendRadiusForLinearAndCircularMove)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP path acceleration.
        /// </summary>
        public double TcpPathAcceleration
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpPathAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpPathAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the joint motion blend output.
        /// </summary>
        public bool IsJointMotionBlendingEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointMotionBlendingEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointMotionBlendingEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the linear/circular motion blend output.
        /// </summary>
        public bool IsLinearAndCircularMotionBlendingEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearAndCircularMotionBlendingEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearAndCircularMotionBlendingEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the motion blend for plunge/retract moves.
        /// </summary>
        public bool IsMotionBlendingForPlungeRetractMovesEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsMotionBlendingForPlungeRetractMovesEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsMotionBlendingForPlungeRetractMovesEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the use of Move Process motion command (movep instead of movel).
        /// </summary>
        public bool UseMoveProcessCommand
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.UseMoveProcessCommand)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.UseMoveProcessCommand)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }
    }
}