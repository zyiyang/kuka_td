// <copyright file="UniversalRobotsMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for UniversalRobotsMotionSettings.
    /// </summary>
    public partial class UniversalRobotsMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UniversalRobotsMotionSettingsView"/> class.
        /// </summary>
        public UniversalRobotsMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UniversalRobotsMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="UniversalRobotsMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public UniversalRobotsMotionSettingsView(UniversalRobotsMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public UniversalRobotsMotionSettingsViewModel ViewModel { get; }
    }
}