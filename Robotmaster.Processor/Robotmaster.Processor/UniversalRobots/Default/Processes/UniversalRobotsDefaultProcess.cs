﻿// <copyright file="UniversalRobotsDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Universal Robots default process.
    /// </summary>
    [DataContract(Name = "UniversalRobotsDefaultProcess")]
    public class UniversalRobotsDefaultProcess : PackageProcess, IUniversalRobotsProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UniversalRobotsDefaultProcess"/> class.
        /// </summary>
        public UniversalRobotsDefaultProcess()
        {
            this.Name = "Universal Robots Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
