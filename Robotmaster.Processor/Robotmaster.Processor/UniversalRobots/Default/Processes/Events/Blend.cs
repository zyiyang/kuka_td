﻿// <copyright file="Blend.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.UniversalRobots.Default.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// PlungeMove event.
    /// </summary>
    [DataContract(Name = "Blend")]
    public class Blend : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Blend"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public Blend()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.IsVisible = false;
        }

        /// <summary>
        /// Gets or sets the Blend value.
        /// </summary>
        [DataMember(Name = "BlendValue")]
        public virtual double BlendValue { get; set; } = 0;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "Blend = " + this.BlendValue;
        }
    }
}