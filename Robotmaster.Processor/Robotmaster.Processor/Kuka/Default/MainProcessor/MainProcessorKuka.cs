// <copyright file="MainProcessorKuka.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.MainProcessor
{
    using System.Text.RegularExpressions;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Kuka dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorKuka : MainProcessor
    {
        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            this.IsToolChangeEventForced = KukaProcessorSwitches.GetToolChangeOutputType(this.CellSettingsNode) == 2;
            this.IsFirstToolChangeOutputBeforeHome = KukaProcessorSwitches.GetFirstToolChangeOutputCondition(this.CellSettingsNode) == 0;
            this.AddParenthesesToToolActivationDeactivationMacroEvent(operationNode);
            base.EditOperationBeforeAllPointEdits(operationNode);
        }

        /// <inheritdoc />
        internal override PathNode AddToolChangeMacroEvent(CbtNode operationNode, PathNode point)
        {
            if (KukaProcessorSwitches.GetToolChangeMacroNameType(this.CellSettingsNode) == 1)
            {
                // Tool name
                this.ToolChangeMacro = OperationManager.GetTool(operationNode).Name.Replace(" ", string.Empty).ToUpper() + "()";
            }
            else
            {
                // Custom name
                this.ToolChangeMacro = KukaProcessorSwitches.GetToolChangeMacroCustomName(this.CellSettingsNode) + "(" + OperationManager.GetTcpId(operationNode) + ")";
            }

            return base.AddToolChangeMacroEvent(operationNode, point);
        }

        /// <summary>
        ///     Adds the parentheses to the tooling activation/deactivation macro if no parenthesis are detected.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void AddParenthesesToToolActivationDeactivationMacroEvent(CbtNode operationNode)
        {
            if (!Regex.IsMatch(CommonToolingActivationSettings.GetToolingActivationMacro(operationNode), @"\(.*?\)$"))
            {
                // Tooling activation macro name with empty parenthesis
                this.ToolingActivationMacro = CommonToolingActivationSettings.GetToolingActivationMacro(operationNode) + "()";
            }

            if (!Regex.IsMatch(CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode), @"\(.*?\)$"))
            {
                // Tooling deactivation macro name with empty parenthesis
                this.ToolingDeactivationMacro = CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode) + "()";
            }
        }
    }
}