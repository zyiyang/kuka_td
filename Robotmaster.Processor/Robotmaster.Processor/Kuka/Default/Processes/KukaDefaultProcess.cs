﻿// <copyright file="KukaDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Kuka default process.
    /// </summary>
    [DataContract(Name = "KukaDefaultProcess")]
    public class KukaDefaultProcess : PackageProcess, IKukaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaDefaultProcess"/> class.
        /// </summary>
        public KukaDefaultProcess()
        {
            this.Name = "Kuka Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
