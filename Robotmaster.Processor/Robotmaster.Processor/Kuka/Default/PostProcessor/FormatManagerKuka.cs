﻿// <copyright file="FormatManagerKuka.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Kuka Format Manager.
    /// </summary>
    public static class FormatManagerKuka
    {
        /// <summary>
        /// Gets the tolerance used for comparison of floating point numbers.
        /// </summary>
        public const double Tolerance = 10E-6;

        private static string feedFormat = "{0:0.#}";

        private static string velcpFormat = "{0:0.00##}";

        private static string genericFormat = "{0:0.0000}";

        /// <summary>
        /// Gets extension for the posted program.
        /// </summary>
        public static string MainProgramExtension => ".SRC";

        /// <summary>
        /// Adds the program extension.
        /// </summary>
        /// <param name="value">Any string.</param>
        /// <returns>String with program extension.</returns>
        public static string WithProgramExtension(this string value)
        {
            return value + MainProgramExtension;
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any object.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this int value)
        {
            return string.Format(genericFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any object.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this double value)
        {
            return string.Format(genericFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any object.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this float value)
        {
            return string.Format(genericFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this int value)
        {
            return string.Format(feedFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this double value)
        {
            return string.Format(feedFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this float value)
        {
            return string.Format(feedFormat, value);
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted VEL.CP value.</returns>
        public static string VelCpFormatted(this int value)
        {
            return string.Format(velcpFormat, value); // TODO : (RMV7-5068) Verify unit implementation
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted VEL.CP value.</returns>
        public static string VelCpFormatted(this double value)
        {
            return string.Format(velcpFormat, value); // TODO : (RMV7-5068) Verify unit implementation
        }

        /// <summary>
        /// Generic numeric to string formating.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted VEL.CP value.</returns>
        public static string VelCpFormatted(this float value)
        {
            return string.Format(velcpFormat, value / 1000); // TODO: (RMV7 - 5068) Verify unit implementation
        }

        /// <summary>
        /// Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>String with speed/feed units.</returns>
        public static string WithSpeedUnits(this int value)
        {
            return value.FeedFormatted() + "mm/s";
        }

        /// <summary>
        /// Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>String with speed/feed units.</returns>
        public static string WithSpeedUnits(this double value)
        {
            return value.FeedFormatted() + "mm/s";
        }

        /// <summary>
        /// Adds the speed/feed units.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>String with speed/feed units.</returns>
        public static string WithSpeedUnits(this float value)
        {
            // TODO : (RMV7-5068) Implements units
            return value.FeedFormatted() + "mm/s";
        }

        /// <summary>
        /// Parses an inversed connection string (Value = Key) to a <see cref="Dictionary{TKey,TValue}"/>.
        /// <para>
        /// Value pairs are separated by semicolons (;). The equal sign (=) connects each value and its key.
        /// </para>
        /// <para>
        /// Keys must be unique (right side).
        /// If there are duplicated keys only the first one will be added.
        /// Spaces before and after keys and values are ignored.
        /// </para>
        /// <para>
        ///     Example: Parses "R1=E1; R2=E2 ; E1 = E3" to  { "E1", "R1" },{ "E2", "R2" },{ "E3", "E1" }.
        /// </para>
        /// </summary>
        /// <param name="inversedConnectionString">Inversed connection string.</param>
        /// <returns>The equivalent dictionary.</returns>
        [Obsolete("ParseInversedConnectionStringToDictionary is deprecated.")]
        public static Dictionary<string, string> ParseInversedConnectionStringToDictionary(string inversedConnectionString)
        {
            // For Kuka, the key is on the right side.
            var matchCollection = Regex.Matches(inversedConnectionString, @"\s*(?<value>[^\s*;=\s*]+)\s*=\s*((?<key>[^\s*;]*))");
            var enumerable = matchCollection.Cast<Match>();
            var dictonary = new Dictionary<string, string>();
            foreach (var match in enumerable)
            {
                // Adds pair if the key doesn't already exist
                if (!dictonary.Keys.Contains(match.Groups["key"].Value))
                {
                    dictonary.Add(match.Groups["key"].Value, match.Groups["value"].Value);
                }
            }

            return dictonary;
        }
    }
}
