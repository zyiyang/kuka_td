// <copyright file="PostProcessorKuka.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.DeviceMenus.ToolingActivationSettings;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Kuka.Default.MainProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Device;
    using Robotmaster.Rise.BaseClasses.Operation.Types;
    using Robotmaster.Rise.BaseClasses.Settings.Types;
    using Robotmaster.Rise.Event;
    using Robotmaster.Wpf.Controls;

    internal class PostProcessorKuka : PostProcessor
    {
        /// <summary>
        ///     Gets the moves sub <see cref="FileSection"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets the DAT main <see cref="FileSection"/>.
        /// </summary>
        internal virtual StreamWriter DatOut => this.CurrentPostFile.Children[0].FileSection.StreamWriter;

        /// <summary>
        ///     Gets or sets the list containing all the variable to be declared in the current (sub)program.
        /// </summary>
        internal virtual List<string> DeclarationList { get; set; } = new List<string>();

        /// <summary>
        ///     Gets or sets a value indicating whether the program is RTCP.
        /// </summary>
        internal virtual bool IsRtcp { get; set; }

        /// <summary>
        ///     Starts the main <paramref name="postFile"/> formating following <see cref="Kuka"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The main <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartMainPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            this.StartPostFile(postFile, operationNode, point);
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formating following <see cref="Kuka"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Main section
            if (KukaProcessorSwitches.GetIsOutputFilePropertiesEnabled(this.CellSettingsNode))
            {
                postFile.FileSection.Header += this.FormatFileProperties();
            }

            postFile.FileSection.Header += "DEF " + postFile.FileName + "()" + "\r\n" + "\r\n";

            // Declaration sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add Declaration sub-section

            // Kuka main sub-section
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Add Kuka main sub-section
            postFile.FileSection.SubFileSections[1].Footer += "\r\n" + "END";

            // Initialize DAT file
            this.InitializeDatFile(postFile);
        }

        /// <summary>
        ///     Initializes the DAT file corresponding to the SRC file.
        ///     The master file does not have a DAT file.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void InitializeDatFile(PostFile postFile)
        {
            // Outputs if the post-switch is enabled and if the postFile is not the master SRC file.
            if (KukaProcessorSwitches.GetDatFileOutput(this.CellSettingsNode) == 1 && postFile == this.CurrentPostFile)
            {
                var datFile = new PostFile(); // .DAT
                postFile.AddChild(datFile); // Add .DAT to .SRC
                datFile.FileName = postFile.FileName;
                datFile.FileExtension = ".DAT";

                // Main section
                if (KukaProcessorSwitches.GetIsOutputFilePropertiesEnabled(this.CellSettingsNode))
                {
                    datFile.FileSection.Header += this.FormatFileProperties();
                }

                datFile.FileSection.Header += "DEFDAT " + datFile.FileName + "\r\n" + "\r\n";
                datFile.FileSection.Header += this.FormatDatFileTemplate();
                datFile.FileSection.Footer += "\r\n" + "ENDDAT";
            }
        }

        /// <summary>
        ///     Format the DAT default template.
        /// </summary>
        /// <returns>The DAT default template.</returns>
        internal virtual string FormatDatFileTemplate()
        {
            return ";FOLD EXTERNAL DECLARATIONS" + "\r\n" +
                   ";FOLD BASISTECH EXT" + "\r\n" +
                   ";ENDFOLD (BASISTECH EXT)" + "\r\n" +
                   ";FOLD USER EXT" + "\r\n" +
                   ";Make your modifications here" + "\r\n" +
                   ";ENDFOLD (USER EXT)" + "\r\n" +
                   ";ENDFOLD (EXTERNAL DECLARATIONS)" + "\r\n" + "\r\n";
        }

        /// <summary>
        ///     Get the formatted file properties that are output in the <see cref="FileSection.Header"/>.
        /// </summary>
        /// <returns>The file properties.</returns>
        internal virtual string FormatFileProperties()
        {
            return "&ACCESS RVP  " + "\r\n" +
                   "&REL 1" + "\r\n" +
                   "&COMMENT GENERATED BY ROBOTMASTER" + "\r\n" +
                   "&PARAM TEMPLATE = C:\\KRC\\Roboter\\Template\\vorgabe" + "\r\n" +
                   "&PARAM EDITMASK = *" + "\r\n";
        }

        /// <summary>
        ///     Calls the current <see cref="PostFile"/> in its parent <see cref="PostFile"/> following <see cref="Kuka"/> convention.
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to be called in its parent.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">The current point.</param>
        internal virtual void CallPostFileIntoParent(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            //// Example:
            ////    EXT BTEST1()
            ////    BTEST1()
            postFile.Parent.FileSection.SubFileSections[0].StreamWriter.WriteLine("EXT " + postFile.FileName + "()");
            postFile.Parent.FileSection.SubFileSections[1].StreamWriter.WriteLine(postFile.FileName + "()");
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formating following <see cref="Kuka"/> convention.
        ///     <para> Replaces or inserts information in the file that could not be computed when the file was started (see <see cref="StartPostFile"/>).</para>
        ///     <para> Resets file splitting conditions.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to ended.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void EndPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            this.PointNumberInPostFile = 0;
            if (this.DeclarationList != null && this.DeclarationList.Any())
            {
                postFile.FileSection.SubFileSections[0].StreamWriter.WriteLine(string.Join("\r\n", this.DeclarationList.ToArray()) + "\r\n");
                this.DeclarationList.Clear();
            }
        }

        /// <summary>
        ///     Ends the <paramref name="postFile"/> formating following <see cref="Kuka"/> convention for main programs.
        /// </summary>
        /// <param name="postFile">The main <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal virtual void EndMainPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
        }

        /// <summary>
        ///     Find last available name among the children of the current <see cref="PostFile"/> parent.
        /// </summary>
        /// <param name="postFile">Current <see cref="PostFile"/>.</param>
        /// <param name="operationNode">Current operationNode node.</param>
        /// <param name="point">Current point.</param>
        /// <returns>Last available name.</returns>
        internal virtual string FindNextAvailableName(PostFile postFile, CbtNode operationNode, PathNode point)
        {
            // Logic can be added here to manage naming of robotmaster sub-program(s).
            string subPostFileNamePattern = this.CurrentPostFile.Parent.FileName + "_";
            var maxIndex = 1;

            while (this.CurrentPostFile.Parent.Children.Any(pf => pf.FileName == subPostFileNamePattern + maxIndex))
            {
                maxIndex++;
            }

            return subPostFileNamePattern + maxIndex;
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            // Check if setup is Rtcp
            this.IsRtcp = SetupManager.IsRtcp(this.SetupNode);

            this.VerifyExternalAxesId();

            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            this.InitializeToolChangeOutputParameters();

            // Multi-file output
            if (KukaProcessorSwitches.GetMaximumPointPerFile(this.CellSettingsNode) <= 0) //// and no sub-programNode exist
            {
                this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.FileExtension = FormatManagerKuka.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
            else
            {
                this.CurrentPostFile.InsertIntermediateParent(); //// Create main (master) program
                this.CurrentPostFile.Parent.FileName = this.ProgramName.Replace(" ", "_");
                this.CurrentPostFile.Parent.FileExtension = FormatManagerKuka.MainProgramExtension;
                this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, firstOperation, firstPoint);
                this.CurrentPostFile.FileExtension = FormatManagerKuka.MainProgramExtension;

                this.CallPostFileIntoParent(this.CurrentPostFile, firstOperation, firstPoint);
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
            }
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            if (previousOperationNode == null)
            {
                this.Moves.WriteLine(this.FormatBasInitialization() + "\r\n");
                this.OutputMotionSettingsBlocks(operationNode, null);
                this.OutputToolChangeMacro(operationNode);
                this.OutputToolActivationDeactivationMacro(operationNode);
                this.Moves.Write(KukaProcessorSwitches.GetBaseOutputType(this.CellSettingsNode) != 0 ? this.FormatBaseDefinition(operationNode) + "\r\n" : string.Empty);
                this.Moves.Write(KukaProcessorSwitches.GetToolOutputType(this.CellSettingsNode) != 0 ? this.FormatToolFrame(operationNode) + "\r\n" : string.Empty);
                this.Moves.Write(this.IsRtcp ? "$IPO_MODE = #TCP" + "\r\n" : string.Empty);
                this.Moves.Write(KukaProcessorSwitches.GetAdvanceLookAheadValue(this.CellSettingsNode) != 0 ? ("\r\n" + "$ADVANCE = " + KukaProcessorSwitches.GetAdvanceLookAheadValue(this.CellSettingsNode)) + "\r\n" : string.Empty);
            }
            else
            {
                this.OutputToolChangeMacro(operationNode);
                this.OutputToolActivationDeactivationMacro(operationNode);
                this.Moves.Write(KukaProcessorSwitches.GetBaseOutputType(this.CellSettingsNode) != 0 && OperationManager.GetUserFrame(operationNode) != OperationManager.GetUserFrame(previousOperationNode) ? this.FormatBaseDefinition(operationNode) + "\r\n" : string.Empty);
                this.Moves.Write(KukaProcessorSwitches.GetToolOutputType(this.CellSettingsNode) != 0 && OperationManager.GetTcpId(operationNode) != OperationManager.GetTcpId(previousOperationNode) ? this.FormatToolFrame(operationNode) + "\r\n" : string.Empty);
                this.OutputMotionSettingsBlocks(operationNode, previousOperationNode);
            }
        }

        /// <summary>
        ///     Verifies the brand specific external axes ID. Notifies the user if the axes ID does not follow Kuka convention [E1,E2,E3,E4,E5,E6].
        /// </summary>
        internal virtual void VerifyExternalAxesId()
        {
            var kukaExternalAxesConvention = new List<string> { "E1", "E2", "E3", "E4", "E5", "E6" };

            if (this.Rails.Any())
            {
                foreach (Joint railAxis in this.Rails)
                {
                    if (!kukaExternalAxesConvention.Contains(railAxis.Name, StringComparer.OrdinalIgnoreCase))
                    {
                        this.NotifyUser("ERROR: Rail axis ID not valid: " + railAxis.Name + ".", false, false);
                        Logger.Info("ERROR: Rail axis ID not valid: " + railAxis.Name + "." + " The XML axis ID, the ROBX label, the .V7 parent and home ID must be consistent. The ID must follow Kuka convention: E1, E2, E3, E4, E5, E6.");
                        break;
                    }
                }
            }

            if (this.Rotaries.Any())
            {
                foreach (Joint rotaryAxis in this.Rotaries)
                {
                    if (!kukaExternalAxesConvention.Contains(rotaryAxis.Name, StringComparer.OrdinalIgnoreCase))
                    {
                        this.NotifyUser("ERROR: Rotary axis ID not valid: " + rotaryAxis.Name, false, true);
                        Logger.Info("ERROR: Rotary axis ID not valid: " + rotaryAxis.Name + "." + " The XML axis ID, the ROBX label, the .V7 parent and home ID must be consistent. The ID must follow Kuka convention: E1, E2, E3, E4, E5, E6.");
                        break;
                    }
                }
            }
        }

        /// <summary>
        ///     Displays a message if multiple tools are used in the programNode <see cref ="PostProcessor"/>.
        /// </summary>
        internal virtual void InitializeToolChangeOutputParameters()
        {
            if (this.IsMultiToolProgram && KukaProcessorSwitches.GetToolChangeOutputType(this.CellSettingsNode) == 0)
            {
                MessageWindowResult userInput = MessageWindow.Show("Would you like to output tool change ? \r\n\r\nNo = Never output tool change \r\nCancel = Only this time", "Always output tool change", true);

                switch (userInput)
                {
                    case MessageWindowResult.Yes:
                        KukaProcessorSwitches.SetToolChangeOutputType(this.CellSettingsNode, "2"); // State of switch = Enable
                        break;
                    case MessageWindowResult.No:
                        KukaProcessorSwitches.SetToolChangeOutputType(this.CellSettingsNode, "1"); // State of switch = Disable
                        break;
                    case MessageWindowResult.Cancel:
                        break;
                }
            }
        }

        /// <summary>
        ///     Outputs the tool change macro <see cref ="MainProcessorKuka.AddToolChangeMacroEvent"/>.
        /// </summary>
        /// <param name="operationNode">Current operationNode node.</param>
        internal virtual void OutputToolChangeMacro(CbtNode operationNode)
        {
            Event macro = OperationManager.GetFirstPoint(operationNode).Events()?.EventListBefore?.FirstOrDefault(e => e.GetType() == typeof(ToolChange));

            if (macro != null && KukaProcessorSwitches.GetToolChangeOutputType(this.CellSettingsNode) != 1)
            {
                this.Moves.WriteLine(macro);
                if (KukaProcessorSwitches.GetToolChangeMacroNameType(this.CellSettingsNode) == 1)
                {
                    // Tool name
                    this.AddToDeclarationList("EXT " + OperationManager.GetTool(operationNode).Name.Replace(" ", string.Empty).ToUpper() + "()");
                }
                else
                {
                    // Custom name
                    string toolChangeMacroCustomName = KukaProcessorSwitches.GetToolChangeMacroCustomName(this.CellSettingsNode);
                    this.AddToDeclarationList("EXT " + (toolChangeMacroCustomName == string.Empty ? "TOOL" : toolChangeMacroCustomName) + "(INT:IN)");
                }
            }
        }

        /// <summary>
        ///     Outputs the tooling activation/deactivation macro <see cref ="CommonToolingActivationSettings"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void OutputToolActivationDeactivationMacro(CbtNode operationNode)
        {
            if (CommonToolingActivationSettings.GetToolingActivationDeactivationCondition(operationNode) != 0)
            {
                if (Regex.IsMatch(CommonToolingActivationSettings.GetToolingActivationMacro(operationNode), @"\(.*?\)$"))
                {
                    this.AddToDeclarationList("EXT " + CommonToolingActivationSettings.GetToolingActivationMacro(operationNode));
                }
                else
                {
                    // Adds the parentheses to the tooling activation macro if no parenthesis are detected.
                    this.AddToDeclarationList("EXT " + CommonToolingActivationSettings.GetToolingActivationMacro(operationNode) + "()");
                }

                if (Regex.IsMatch(CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode), @"\(.*?\)$"))
                {
                    this.AddToDeclarationList("EXT " + CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode));
                }
                else
                {
                    // Adds the parentheses to the tooling deactivation macro if no parenthesis are detected.
                    this.AddToDeclarationList("EXT " + CommonToolingActivationSettings.GetToolingDeactivationMacro(operationNode) + "()");
                }
            }
        }

        /// <summary>
        ///     Outputs the motion settings of <see cref ="FormatPtpMotionSettings"/>, <see cref ="FormatCartesianMotionSettings"/> and <see cref ="FormatPositioningSettings"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="previousOperationNode">Previous operation.</param>
        internal virtual void OutputMotionSettingsBlocks(CbtNode operationNode, CbtNode previousOperationNode)
        {
            var operationType = operationNode.GetComponent<OperationType>();
            string motionSettingsSpacer = string.Empty;

            if (previousOperationNode == null)
            {
                motionSettingsSpacer = "\r\n";
            }

            //// Remark : The equality of motion settings is true if the motion settings uses the same values no matter if they are overridden or not.
            if (previousOperationNode == null || KukaMotionSettings.IsOverridden(operationNode))
            {
                // Output PTP Motion Block
                if (previousOperationNode == null || this.IsPtpMotionBlockDifferent(operationNode, previousOperationNode))
                {
                    if (KukaMotionSettings.GetIsJointVelocityAndAccelerationEnabled(operationNode))
                    {
                        this.AddToDeclarationList("INT I");
                        this.Moves.WriteLine(this.FormatPtpMotionSettings(operationNode) + motionSettingsSpacer);
                    }
                }

                if (previousOperationNode == null || operationType == OperationType.Task)
                {
                    // Output LinAndArcMotion Motion Block
                    if ((previousOperationNode == null && KukaMotionSettings.GetIsContinuousPathParametersEnabled(operationNode)) || this.IsLinAndArcMotionBlockDifferent(operationNode, previousOperationNode))
                    {
                        this.Moves.WriteLine(this.FormatCartesianMotionSettings(operationNode) + motionSettingsSpacer);
                    }

                    // Output PositioningCriteria Motion Block
                    if ((previousOperationNode == null && KukaMotionSettings.GetIsApproximationEnabled(operationNode)) || this.IsPositionCriteriaMotionBlockDifferent(operationNode, previousOperationNode))
                    {
                        this.Moves.WriteLine(this.FormatPositioningSettings(operationNode) + motionSettingsSpacer);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the <see cref ="FormatPtpMotionSettings"/> of the <paramref name="operationNode"/> is different then the <paramref name="previousOperationNode"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="previousOperationNode">Previous operation.</param>
        /// <returns><c>true</c> if PtpMotionBlock is different otherwise, <c>false</c>.</returns>
        internal virtual bool IsPtpMotionBlockDifferent(CbtNode operationNode, CbtNode previousOperationNode) =>
           KukaMotionSettings.GetIsJointVelocityAndAccelerationEnabled(operationNode) &&
           ((KukaMotionSettings.GetJointAxisVelocity(operationNode) != KukaMotionSettings.GetJointAxisVelocity(previousOperationNode)) ||
            (KukaMotionSettings.GetJointAxisAcceleration(operationNode) != KukaMotionSettings.GetJointAxisAcceleration(previousOperationNode)));

        /// <summary>
        ///     Gets a value indicating whether the <see cref = "FormatCartesianMotionSettings"/> of the <paramref name="operationNode"/> is different then the <paramref name="previousOperationNode"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="previousOperationNode">Previous operation.</param>
        /// <returns><c>true</c> if LinAndArcMotionBlock is different otherwise, <c>false</c>.</returns>
        internal virtual bool IsLinAndArcMotionBlockDifferent(CbtNode operationNode, CbtNode previousOperationNode) =>
           KukaMotionSettings.GetIsContinuousPathParametersEnabled(operationNode) &&
           ((Math.Abs(KukaMotionSettings.GetTcpSwivelVelocity(operationNode) - KukaMotionSettings.GetTcpSwivelVelocity(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            ((Math.Abs(KukaMotionSettings.GetTcpRotationalVelocity(operationNode) - KukaMotionSettings.GetTcpRotationalVelocity(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetTcpPathAcceleration(operationNode) - KukaMotionSettings.GetTcpPathAcceleration(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetTcpSwivelAcceleration(operationNode) - KukaMotionSettings.GetTcpSwivelAcceleration(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetTcpRotationalAcceleration(operationNode) - KukaMotionSettings.GetTcpRotationalAcceleration(previousOperationNode)) > FormatManagerKuka.Tolerance)));

        /// <summary>
        ///     Gets a value indicating whether the <see cref = "FormatPositioningSettings" /> of the <paramref name="operationNode"/> is different then the <paramref name="previousOperationNode"/>.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="previousOperationNode">Previous operation.</param>
        /// <returns><c>true</c> if PositionCriteriaMotionBlock is different otherwise, <c>false</c>.</returns>
        internal virtual bool IsPositionCriteriaMotionBlockDifferent(CbtNode operationNode, CbtNode previousOperationNode) =>
           KukaMotionSettings.GetIsApproximationEnabled(operationNode) &&
           ((KukaMotionSettings.GetJointPositioningType(operationNode) == 1) ||
            (KukaMotionSettings.GetLinearPositioningType(operationNode) == 1) ||
            (KukaMotionSettings.GetCircularPositioningType(operationNode) == 1) ||
            (KukaMotionSettings.GetOrientationControlType(operationNode) != 0)) &&
           ((Math.Abs(KukaMotionSettings.GetApproximationDistanceForPtpMotionParameter(operationNode) - KukaMotionSettings.GetApproximationDistanceForPtpMotionParameter(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetApproximationDistanceParameter(operationNode) - KukaMotionSettings.GetApproximationDistanceParameter(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetApproximationOrientationParameter(operationNode) - KukaMotionSettings.GetApproximationOrientationParameter(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (Math.Abs(KukaMotionSettings.GetApproximationVelocityParameter(operationNode) - KukaMotionSettings.GetApproximationVelocityParameter(previousOperationNode)) > FormatManagerKuka.Tolerance) ||
            (this.FormatOrientationType(operationNode) != this.FormatOrientationType(previousOperationNode)));

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            // Multi-file Output
            if ((KukaProcessorSwitches.GetMaximumPointPerFile(this.CellSettingsNode) > 0) && (this.PointNumberInPostFile >= KukaProcessorSwitches.GetMaximumPointPerFile(this.CellSettingsNode)))
            {
                this.EndPostFile(this.CurrentPostFile, operationNode, point);

                var newPostFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostFile);
                this.CurrentPostFile = newPostFile;

                this.CurrentPostFile.FileName = this.FindNextAvailableName(this.CurrentPostFile, operationNode, point);
                this.CurrentPostFile.FileExtension = FormatManagerKuka.MainProgramExtension;
                this.StartPostFile(this.CurrentPostFile, operationNode, point);

                this.CallPostFileIntoParent(this.CurrentPostFile, operationNode, point);
            }

            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc/>
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <summary>
        ///     Formats the Kuka base definition at given operation.
        /// </summary>
        /// <param name="operationNode">Current operation node.</param>
        /// <returns>Kuka base definition.</returns>
        /// <remarks>
        /// For case 3 (Synchronized Base):
        ///     EK function is for activating geometric coupling on calibrated external base kinematics predefined in $MACHINE.DAT
        ///     $BASE = EK (Root, Kinematic identifier, Offset)
        ///     -> Root = Position of external kinematic root in $WORLD found in the $CONFIG.DAT
        ///     -> Kinematic identifier = External kinematics (#EASYS,#EBSYS,#ECSYS,#EDSYS,#EESYS,#EFSYS,#ERSYS)
        ///     -> Offset = Additional offset frame from the flange of the external axis kinematic system found in $MACHINE.DAT.
        /// </remarks>
        internal virtual string FormatBaseDefinition(CbtNode operationNode)
        {
            switch (KukaProcessorSwitches.GetBaseOutputType(this.CellSettingsNode))
            {
                case 1:
                    return
                        (!this.IsRtcp ? ";FOLD BASE OUTPUT" : ";FOLD TOOL OUTPUT") + "\r\n" +
                        (!this.IsRtcp ? "  $ACT_BASE = " : "  $ACT_TOOL = ") + OperationManager.GetUserFrame(operationNode).Number + "\r\n" +
                        (!this.IsRtcp ? "  $BASE=BASE_DATA[" : "  $TOOL=TOOL_DATA[") + OperationManager.GetUserFrame(operationNode).Number + "]" + "\r\n" +
                        (!this.IsRtcp ? ";ENDFOLD (BASE OUTPUT)" : ";ENDFOLD (TOOL OUTPUT)");
                case 2:
                    return (!this.IsRtcp ? "$BASE={" : "$TOOL={") + this.FormatBaseFrameValue(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode) + "}";
                case 3:
                    // Rtcp configurations are not considered in this case
                    // Retrieve the configuration from setup node
                    Configuration configuration = SetupManager.GetConfiguration(OperationManager.GetSetupNode(operationNode));
                    var referenceFrameNode = (CbtNode)null;
                    string dynUserFrameOutput = string.Empty;

                    if (OperationManager.GetUserFrame(operationNode).Type == UserFrameType.CellFixed)
                    {
                        // The reference frame used is cell fixed. It has to be task fixed when using synchronized base.
                        // Thus, the table offset will be w.r.t. world
                        this.NotifyUser("Base synchronization output: User frame reference is cell fixed. The user frame used as table offset will be expressed with respect to world frame.", false);
                    }

                    if (configuration?.UserFrameReferenceNode == null)
                    {
                        // The "user_frame_reference" in cell extension (*.V7) is null
                        // Thus, the table offset will be w.r.t. world
                        this.NotifyUser("Base synchronization output: User frame reference is missing.\r\nThe user frame used as table offset will be expressed with respect to world frame.", false);
                    }
                    else
                    {
                        // The reference frame in which the user frame values are given
                        referenceFrameNode = configuration.UserFrameReferenceNode;
                    }

                    // Output TABLE_OFFSET defined variable
                    if (KukaProcessorSwitches.GetIsOutputFilePropertiesEnabled(this.CellSettingsNode))
                    {
                        dynUserFrameOutput = "TABLE_OFFSET={FRAME: " + this.FormatBaseFrameValue(operationNode, referenceFrameNode) + "}" + "\r\n";
                    }

                    // Output root of external axis
                    dynUserFrameOutput += "$BASE=EK(MACHINE_DEF[" + KukaProcessorSwitches.GetMachineDefinitionNumber(this.CellSettingsNode) + "].ROOT,";

                    // Output kinematic identifier
                    dynUserFrameOutput += "MACHINE_DEF[" + KukaProcessorSwitches.GetMachineDefinitionNumber(this.CellSettingsNode) + "].MECH_TYPE,";

                    // Output offset
                    if (KukaProcessorSwitches.GetIsOutputFilePropertiesEnabled(this.CellSettingsNode))
                    {
                        dynUserFrameOutput += "TABLE_OFFSET)" + "\r\n";
                    }
                    else
                    {
                        dynUserFrameOutput += "{" + this.FormatBaseFrameValue(operationNode, referenceFrameNode) + "})" + "\r\n";
                    }

                    // Output number of current external base kinematic system
                    dynUserFrameOutput += "$ACT_EX_AX=" + KukaProcessorSwitches.GetExternalBaseKinematicSystemNumber(this.CellSettingsNode) + ";";

                    return dynUserFrameOutput;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the Kuka tool definition at given operation.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Kuka formatted tool definition.</returns>
        internal virtual string FormatToolFrame(CbtNode operationNode)
        {
            switch (KukaProcessorSwitches.GetToolOutputType(this.CellSettingsNode))
            {
                case 1:
                    return
                        (!this.IsRtcp ? ";FOLD TOOL OUTPUT" : ";FOLD BASE OUTPUT") + "\r\n" +
                        (!this.IsRtcp ? "  $ACT_TOOL = " : "  $ACT_BASE = ") + OperationManager.GetTcpId(operationNode) + "\r\n" +
                        (!this.IsRtcp ? "  $TOOL=TOOL_DATA[" : "  $BASE=BASE_DATA[") + OperationManager.GetTcpId(operationNode) + "]" + "\r\n" +
                        (!this.IsRtcp ? ";ENDFOLD (TOOL OUTPUT)" : ";ENDFOLD (BASE OUTPUT)");
                case 2:
                    return (!this.IsRtcp ? "$TOOL={" : "$BASE={") + this.FormatToolFrameValue(operationNode) + "}";
                default:
                    return string.Empty;
            }
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Skip Tool Change Macro Output
            if (beforePointEvent.GetType() == typeof(ToolChange))
            {
                return;
            }

            // Output event
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            ////Linear Speed change
            if (this.IsThereChangeInFeedRate(point) && (KukaProcessorSwitches.GetFeedOutputType(this.CellSettingsNode) == 1 || KukaProcessorSwitches.GetFeedOutputType(this.CellSettingsNode) == 2))
            {
                this.Moves.WriteLine(this.FormatLinearFeedOutput(point));
            }

            //// LC move example with external axes
            //// LIN  {X 0.9069,Y 0.6666,Z 0.1768,A -108.1324,B -13.4650,C 169.6304,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            this.Moves.Write(this.FormatMotionType(point) + "{" + this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode));

            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0
                this.Moves.Write("," + this.FormatExternalAxesValues(point));
            }

            this.Moves.WriteLine("}" + this.FormatPositioningCriteria(point, operationNode));
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            //// CC move example with external axes
            //// CIRC {X 296.4645,Y -6.4645,Z -30.0000,A 0.0000,B 0.0000,C 180.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0},{X 300.0000,Y -5.0000,Z -30.0000,A 0.0000,B 0.0000,C 180.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            if (point.IsArcMiddlePoint())
            {
                // Arc middle point move
                this.Moves.Write(this.FormatMotionType(point) + "{");
                this.Moves.Write(this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode));

                if (this.ExternalAxesJoints.Any())
                {
                    this.Moves.Write("," + this.FormatExternalAxesValues(point));
                }

                this.Moves.Write("},");
            }
            else
            {
                // Arc end point
                this.Moves.Write("{" + this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode));

                if (this.ExternalAxesJoints.Any())
                {
                    this.Moves.Write("," + this.FormatExternalAxesValues(point));
                }

                this.Moves.WriteLine("}" + this.FormatPositioningCriteria(point, operationNode));
            }
        }

        /// <inheritdoc/>
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            //// JC move example with external axes and status/turn bit
            //// PTP {X 23.8851,Y 15.9428,Z 94.2174,A -108.1324,B -13.4650,C 169.6304,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0, S 2, T 2}
            this.Moves.Write(this.FormatMotionType(point) + "{" + this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode));

            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0
                this.Moves.Write("," + this.FormatExternalAxesValues(point));
            }

            if (KukaProcessorSwitches.GetIsOutputStatusAndTurnsOnEveryPtpMoveEnabled(this.CellSettingsNode))
            {
                //// Status and Turn bit
                //// S 2, T 2
                this.Moves.Write(this.FormatStatusAndTurnBits(point, operationNode));
            }

            this.Moves.WriteLine("}" + this.FormatPositioningCriteria(point, operationNode));
        }

        /// <inheritdoc/>
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            //// JJ move example with external axes
            //// PTP  {A1 0.0000,A2 -90.0000,A3 90.0000,A4 0.0000,A5 0.0000,A6 0.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            this.Moves.Write(this.FormatMotionType(point) + "{");
            this.Moves.Write(this.FormatJointValues(point));

            if (this.ExternalAxesJoints.Any())
            {
                //// External axes
                //// E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0
                this.Moves.Write("," + this.FormatExternalAxesValues(point));
            }

            this.Moves.WriteLine("}");
        }

        /// <inheritdoc/>
        internal override void RunAfterProgramOutput()
        {
            CbtNode lastOperation = ProgramManager.GetOperationNodes(this.ProgramNode).Last();
            PathNode lastPoint = OperationManager.GetLastPoint(lastOperation);

            if (KukaProcessorSwitches.GetMaximumPointPerFile(this.CellSettingsNode) <= 0)
            {
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);
            }
            else
            {
                // End current post-file
                this.EndPostFile(this.CurrentPostFile, lastOperation, lastPoint);

                // Multi-file output
                PostFile parentPostFile = this.CurrentPostFile.Parent;
                while (!ReferenceEquals(parentPostFile, this.MainPostFile.Parent)) //// While root is not reached
                {
                    // End main post-file
                    this.EndMainPostFile(parentPostFile, lastOperation, lastPoint);
                    parentPostFile = parentPostFile.Parent;
                }
            }
        }

        /// <summary>
        ///     Formats joint values at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Kuka formatted joint values.</returns>
        internal virtual string FormatJointValues(PathNode point)
        {
            //// Joint values
            //// A1 0.0000,A2 -90.0000,A3 90.0000,A4 0.0000,A5 0.0000,A6 0.0000
            return
                "A1 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]).Formatted() + "," +
                "A2 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]).Formatted() + "," +
                "A3 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]).Formatted() + "," +
                "A4 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]).Formatted() + "," +
                "A5 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]).Formatted() + "," +
                "A6 " + point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]).Formatted();
        }

        /// <summary>
        ///     Formats position output at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Kuka formatted position.</returns>
        internal virtual string FormatPosition(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            return "X " + pointPositionInUserFrame.X.Formatted() + "," + "Y " + pointPositionInUserFrame.Y.Formatted() + "," + "Z " + pointPositionInUserFrame.Z.Formatted();
        }

        /// <summary>
        ///     Formats orientation output.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Kuka formatted orientation.</returns>
        internal virtual string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // Gets the Euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return "A " + euler[0].Formatted() + "," + "B " + euler[1].Formatted() + "," + "C " + euler[2].Formatted();
        }

        /// <summary>
        ///     Formats the base frame value at given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        /// <returns>Formatted Kuka User Frame (Base Frame).</returns>
        internal virtual string FormatBaseFrameValue(CbtNode operationNode, CbtNode referenceFrameNode)
        {
            Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(OperationManager.GetUserFrame(operationNode), this.SceneCbtRoot, this.OperationCbtRoot, referenceFrameNode);

            // set Euler convention
            Vector3 vector = this.RobotFormatter.MatrixToEuler(userFrameMatrix);

            // $BASE ={X 1750.0000,Y - 250.0000,Z 250.0000,A 0.0000,B 0.0000,C 0.0000}
            return "X " + userFrameMatrix.Position.X.Formatted() + "," +
                   "Y " + userFrameMatrix.Position.Y.Formatted() + "," +
                   "Z " + userFrameMatrix.Position.Z.Formatted() + "," +
                   "A " + vector.X.Formatted() + "," +
                   "B " + vector.Y.Formatted() + "," +
                   "C " + vector.Z.Formatted();
        }

        /// <summary>
        ///     Formats Tool Frame output at given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted Kuka Tool Frame.</returns>
        internal virtual string FormatToolFrameValue(CbtNode operationNode)
        {
            Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
            }

            // set Euler convention
            Vector3 vector = this.RobotFormatter.MatrixToEuler(toolFrameMatrix);

            // $TOOL={X 352.6447,Y 0.0000,Z 469.2759,A 180.0000,B -60.0005,C 0.0000}
            return "X " + toolFrameMatrix.Position.X.Formatted() + "," +
                   "Y " + toolFrameMatrix.Position.Y.Formatted() + "," +
                   "Z " + toolFrameMatrix.Position.Z.Formatted() + "," +
                   "A " + vector.X.Formatted() + "," +
                   "B " + vector.Y.Formatted() + "," +
                   "C " + vector.Z.Formatted();
        }

        /// <summary>
        ///     Formats motion type at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Formatted Kuka motion type.</returns>
        internal virtual string FormatMotionType(PathNode point)
        {
            switch (point.MoveType())
            {
                case MoveType.Rapid:
                    return "PTP  ";
                case MoveType.Linear:
                    return "LIN  ";
                case MoveType.Circular:
                    return "CIRC ";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats configuration at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Kuka formatted configuration.</returns>
        internal virtual string FormatStatusAndTurnBits(PathNode point, CbtNode operationNode)
        {
            return "," + this.FormatStatusBits() + "," + this.FormatTurnBits(point);
        }

        /// <summary>
        ///     Formats the calculated the status bits at given point.
        /// </summary>
        /// <returns>Kuka formatted status bits.</returns>
        internal virtual string FormatStatusBits()
        {
            if (KukaProcessorSwitches.GetStatusAndTurnsOutputFormat(this.CellSettingsNode) == 0)
            {
                // Binary format
                return "S \'" +
                    (this.CachedRobotConfig.WristConfiguration == WristConfig.Negative ? "1" : "0") +
                    (this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up ? "1" : "0") +
                    (this.CachedRobotConfig.BaseConfiguration == BaseConfig.Back ? "1" : "0") + "\'";
            }
            else
            {
                // Decimal format
                return "S " +
                    ((this.CachedRobotConfig.WristConfiguration == WristConfig.Negative ? 4 : 0) +
                    (this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up ? 2 : 0) +
                    (this.CachedRobotConfig.BaseConfiguration == BaseConfig.Back ? 1 : 0));
            }
        }

        /// <summary>
        ///     Formats the calculated turn bits at given point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Kuka formatted turn bits.</returns>
        internal virtual string FormatTurnBits(PathNode point)
        {
            if (KukaProcessorSwitches.GetStatusAndTurnsOutputFormat(this.CellSettingsNode) == 0)
            {
                // Binary format
                return "T \'" +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]) < 0 ? "1" : "0") +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]) < 0 ? "1" : "0") +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]) < 0 ? "1" : "0") +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]) < 0 ? "1" : "0") +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]) < 0 ? "1" : "0") +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]) < 0 ? "1" : "0") +
                    "\'";
            }
            else
            {
                // Decimal format
                return "T " +
                    ((point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]) < 0 ? 32 : 0) +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]) < 0 ? 16 : 0) +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]) < 0 ? 8 : 0) +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]) < 0 ? 4 : 0) +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]) < 0 ? 2 : 0) +
                    (point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]) < 0 ? 1 : 0));
            }
        }

        /// <summary>
        ///     Formats the linear feed speed output at given point.
        ///     Returns <c>string.empty</c> if the feed as already been output (by <see cref="PostProcessor.CachedPreviousLinearPoint"/>).
        /// </summary>
        /// <param name="point">For each point, we verify the feed.</param>
        /// <returns>Linear speed Output.</returns>
        internal virtual string FormatLinearFeedOutput(PathNode point) // only output if there is a change in the feed rate
        {
            if (this.CachedPreviousLinearPoint == null ||
                (point.Feedrate().LinearFeedrate.VelCpFormatted() !=
                 this.CachedPreviousLinearPoint.Feedrate().LinearFeedrate.VelCpFormatted()))
            {
                switch (KukaProcessorSwitches.GetFeedOutputType(this.CellSettingsNode))
                {
                    case 0:
                        return string.Empty;

                    case 1:
                        if (point.Feedrate().LinearFeedrate > 3000.0)
                        {
                            return "$VEL.CP = 3.0  ;NOTE:$VEL.CP has a maximum value of 3m/s!  ;" + point.Feedrate().LinearFeedrate + "mm/s";
                        }
                        else
                        {
                            return "$VEL.CP = " + point.Feedrate().LinearFeedrate.VelCpFormatted() + "  ;" + point.Feedrate().LinearFeedrate.WithSpeedUnits();
                        }

                    case 2:
                        if (point.Feedrate().LinearFeedrate > 3000.0)
                        {
                            return "BAS(#VEL_CP, 3.0)  ;NOTE:#VEL.CP has a maximum value of 3m/s!  ;" + point.Feedrate().LinearFeedrate + "mm/s";
                        }
                        else
                        {
                            return "BAS(#VEL_CP," + point.Feedrate().LinearFeedrate.VelCpFormatted() + ")  ;" + point.Feedrate().LinearFeedrate.WithSpeedUnits();
                        }

                    default:
                        this.NotifyUser("Feed output type not valid");
                        return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///      Gets a value indicating whether there is a feed rate change between a given point and <see cref="PostProcessor.CachedPreviousLinearPoint"/>.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns><c>true</c> if there is a feed rate change otherwise, <c>false</c>.</returns>
        internal virtual bool IsThereChangeInFeedRate(PathNode point) => (this.CachedPreviousLinearPoint == null || (point.Feedrate().LinearFeedrate.VelCpFormatted() != this.CachedPreviousLinearPoint.Feedrate().LinearFeedrate.VelCpFormatted())) ? true : false;

        /// <summary>
        ///     Formats the BAS initialization block.
        /// </summary>
        /// <returns>Formatted BAS initialization block.</returns>
        internal virtual string FormatBasInitialization()
        {
            this.AddToDeclarationList("EXT BAS(BAS_COMMAND: IN, REAL: IN)");

            return KukaProcessorSwitches.GetIsOutputBasInitMoveInsideFoldSectionEnabled(this.CellSettingsNode)
                ? (";FOLD INI" + "\r\n" +
                   "  ;FOLD BASISTECH INI" + "\r\n" +
                   "    GLOBAL INTERRUPT DECL 3 WHEN $STOPMESS==TRUE DO IR_STOPM ( )" + "\r\n" +
                   "    INTERRUPT ON 3" + "\r\n" +
                   "    BAS (#INITMOV, 0)" + "\r\n" +
                   "  ;ENDFOLD (BASISTECH INI)" + "\r\n" +
                   "  ;FOLD USER INI" + "\r\n" +
                   "    ;Make your modifications here" + "\r\n" +
                   "\r\n" +
                   "  ;ENDFOLD (USER INI)" + "\r\n" +
                   ";ENDFOLD (INI)")
                : "BAS (#INITMOV, 0)";
        }

        /// <summary>
        ///     Formats the PTP motion settings at a given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted PTP motion settings.</returns>
        internal virtual string FormatPtpMotionSettings(CbtNode operationNode)
        {
            return ";******SETTINGS FOR PTP MOTION***********************" + "\r\n" +
                   ";FOLD PTP $VEL_AXIS AND $ACC_AXIS" + "\r\n" +
                   "FOR I=1 TO 6" + "\r\n" +
                   "   $VEL_AXIS[I] = " + KukaMotionSettings.GetJointAxisVelocity(operationNode) + "\r\n" +
                   "   $ACC_AXIS[I] = " + KukaMotionSettings.GetJointAxisAcceleration(operationNode) + "\r\n" +
                   "ENDFOR" + "\r\n" +
                   ";ENDFOLD (PTP $VEL_AXIS AND $ACC_AXIS)";
        }

        /// <summary>
        ///     Formats the Cartesian motion settings at a given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>LIN and ARC motion block.</returns>
        internal virtual string FormatCartesianMotionSettings(CbtNode operationNode)
        {
            return ";******SETTINGS FOR LIN AND ARC MOTION***************" + "\r\n" +
                   ";FOLD LIN AND ARC MOTION VARIABLES" + "\r\n" +
                   "$VEL.ORI1 = " + KukaMotionSettings.GetTcpSwivelVelocity(operationNode) + "\r\n" +
                   "$VEL.ORI2 = " + KukaMotionSettings.GetTcpRotationalVelocity(operationNode) + "\r\n" +
                   "$ACC.CP = " + KukaMotionSettings.GetTcpPathAcceleration(operationNode) + "\r\n" +
                   "$ACC.ORI1 = " + KukaMotionSettings.GetTcpSwivelAcceleration(operationNode) + "\r\n" +
                   "$ACC.ORI2 = " + KukaMotionSettings.GetTcpRotationalAcceleration(operationNode) + "\r\n" +
                   ";ENDFOLD (LIN AND ARC MOTION VARIABLES)";
        }

        /// <summary>
        ///     Formats the positioning settings at a given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Positioning criteria block.</returns>
        internal virtual string FormatPositioningSettings(CbtNode operationNode)
        {
            int linearPositioningType = KukaMotionSettings.GetLinearPositioningType(operationNode);
            int circularPositioningType = KukaMotionSettings.GetCircularPositioningType(operationNode);
            int linearPositioningCriterionType = KukaMotionSettings.GetLinearPositioningCriterionType(operationNode);
            int circularPositioningCriterionType = KukaMotionSettings.GetCircularPositioningCriterionType(operationNode);

            return ";******SETTINGS FOR POSITIONING CRITERIA*************" + "\r\n" +
                   ";FOLD POSITIONING CRITERIA" + "\r\n" +

                   // PTP approximate positioning (% of 90 deg.) for PTP instruction in the advance run
                   (KukaMotionSettings.GetJointPositioningType(operationNode) == 1
                       ? "  $APO.CPTP = " + KukaMotionSettings.GetApproximationDistanceForPtpMotionParameter(operationNode) + "\r\n"
                       : string.Empty) +

                   // Translational distance criterion (mm) for LIN and CIRC instruction in the advance run
                   ((linearPositioningType == 1 || circularPositioningType == 1) && (linearPositioningCriterionType == 0 || circularPositioningCriterionType == 0)
                       ? "  $APO.CDIS = " + KukaMotionSettings.GetApproximationDistanceParameter(operationNode) + "\r\n"
                       : string.Empty) +

                   // Orientation distance (deg.) for LIN and CIRC instruction in the advance run
                   ((linearPositioningType == 1 || (circularPositioningType == 1)) && (linearPositioningCriterionType == 1 || circularPositioningCriterionType == 1)
                       ? "  $APO.CORI = " + KukaMotionSettings.GetApproximationOrientationParameter(operationNode) + "\r\n"
                       : string.Empty) +

                   // Velocity criterion (%) for LIN and CIRC instruction in the advance run
                   ((linearPositioningType == 1 || (circularPositioningType == 1)) && (linearPositioningCriterionType == 2 || circularPositioningCriterionType == 2)
                       ? "  $APO.CVEL = " + KukaMotionSettings.GetApproximationVelocityParameter(operationNode) + "\r\n"
                       : string.Empty) +

                   // Orientation control for CP instructions (LIN, CIRC) in the advance run
                   (KukaMotionSettings.GetOrientationControlType(operationNode) != 0
                       ? this.FormatOrientationType(operationNode) + "\r\n"
                       : string.Empty) +

                   ";ENDFOLD (POSITIONING CRITERIA)";
        }

        /// <summary>
        ///     Formats the orientation type in the positioning criteria block at a given operation.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <returns>Formatted orientation type.</returns>
        internal virtual string FormatOrientationType(CbtNode operationNode)
        {
            // Orientation control for CP instructions (LIN, CIRC) in the advance run
            switch (KukaMotionSettings.GetOrientationControlType(operationNode))
            {
                case 0:
                    return string.Empty;
                case 1:
                    // Variable orientation with possible reduction of velocity and acceleration
                    return "  $ORI_TYPE = #VAR";
                case 2:
                    // Variable orientation without reduction of velocity and acceleration
                    return "  $ORI_TYPE = #JOINT";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the positioning criteria ($APO) depending on the motion type at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Kuka positioning criteria ($APO).</returns>
        internal virtual string FormatPositioningCriteria(PathNode point, CbtNode operationNode)
        {
            switch (point.MoveType())
            {
                case MoveType.Linear:
                    switch (KukaMotionSettings.GetLinearPositioningType(operationNode))
                    {
                        case 0:
                            return string.Empty;
                        case 1:
                            switch (KukaMotionSettings.GetLinearPositioningCriterionType(operationNode))
                            {
                                case 0:
                                    return " C_DIS";
                                case 1:
                                    return " C_ORI";
                                case 2:
                                    return " C_VEL";
                                default:
                                    return string.Empty;
                            }

                        default:
                            return string.Empty;
                    }

                case MoveType.Circular:
                    switch (KukaMotionSettings.GetCircularPositioningType(operationNode))
                    {
                        case 0:
                            return string.Empty;
                        case 1:
                            switch (KukaMotionSettings.GetCircularPositioningCriterionType(operationNode))
                            {
                                case 0:
                                    return " C_DIS";
                                case 1:
                                    return " C_ORI";
                                case 2:
                                    return " C_VEL";
                                default:
                                    return string.Empty;
                            }

                        default:
                            return string.Empty;
                    }

                case MoveType.Rapid:
                    switch (KukaMotionSettings.GetJointPositioningType(operationNode))
                    {
                        case 0:
                            return string.Empty;
                        case 1:
                            return " C_PTP";
                        default:
                            return string.Empty;
                    }

                default:
                    this.NotifyUser("Spline movements are not supported at the moment");
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats external axes values output at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Kuka External Axes.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point)
        {
            //// External axes values (outputs active and inactive joints)
            ////    E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0
            string e1 = !this.JointFormat.TryGetValue("E1", out int i1) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E1", out double d1) ? "0.0" : d1.Formatted()) : point.JointValue(i1).Formatted();
            string e2 = !this.JointFormat.TryGetValue("E2", out int i2) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E2", out double d2) ? "0.0" : d2.Formatted()) : point.JointValue(i2).Formatted();
            string e3 = !this.JointFormat.TryGetValue("E3", out int i3) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E3", out double d3) ? "0.0" : d3.Formatted()) : point.JointValue(i3).Formatted();
            string e4 = !this.JointFormat.TryGetValue("E4", out int i4) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E4", out double d4) ? "0.0" : d4.Formatted()) : point.JointValue(i4).Formatted();
            string e5 = !this.JointFormat.TryGetValue("E5", out int i5) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E5", out double d5) ? "0.0" : d5.Formatted()) : point.JointValue(i5).Formatted();
            string e6 = !this.JointFormat.TryGetValue("E6", out int i6) ? (!this.HomePositionData.StartPoseValues.TryGetValue("E6", out double d6) ? "0.0" : d6.Formatted()) : point.JointValue(i6).Formatted();

            return "E1 " + e1 + "," + "E2 " + e2 + "," + "E3 " + e3 + "," + "E4 " + e4 + "," + "E5 " + e5 + "," + "E6 " + e6;
        }

        /// <summary>
        ///     Adds the variable to the <see cref="PostProcessorKuka.DeclarationList"/>.
        /// </summary>
        /// <param name="variableToBedAdded">Variable to be added in list.</param>
        internal virtual void AddToDeclarationList(string variableToBedAdded)
        {
            if (!this.DeclarationList.Contains(variableToBedAdded))
            {
                this.DeclarationList?.Add(variableToBedAdded);
            }
        }
    }
}
