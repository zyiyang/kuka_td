// <copyright file="KukaProcessorSwitchesBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing <see cref="KukaProcessorSwitchesView"/>.
    /// </summary>
    internal class KukaProcessorSwitchesBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the View Model.
        /// </summary>
        private KukaProcessorSwitchesViewModel ViewModel { get; set; }

        /// <summary>
        ///      Sets up the View Model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}