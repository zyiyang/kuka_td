// <copyright file="KukaProcessorSwitchesViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     KukaProcessorSwitchesViewModel View Model.
    /// </summary>
    public class KukaProcessorSwitchesViewModel : ExternalMenuUiHandlerViewModel
    {
        /// <summary>
        ///     Gets the output DAT file.
        /// </summary>
        public static Dictionary<int, string> DatFileOutputSource => new Dictionary<int, string>
        {
            { 0, KukaResources.DatFileOutputOnlyIfNeeded },
            { 1, KukaResources.Always },
        };

        /// <summary>
        ///     Gets the base output type.
        /// </summary>
        public static Dictionary<int, string> BaseOutputTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.Disable },
            { 1, KukaResources.DefineByRegister },
            { 2, KukaResources.DefineByValue },
            { 3, KukaResources.SynchronizedBase },
        };

        /// <summary>
        ///     Gets the tool output type.
        /// </summary>
        public static Dictionary<int, string> ToolOutputTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.Disable },
            { 1, KukaResources.DefineByRegister },
            { 2, KukaResources.DefineByValue },
        };

        /// <summary>
        ///     Gets the status and turns format.
        /// </summary>
        public static Dictionary<int, string> StatusAndTurnsOutputFormatSource => new Dictionary<int, string>
        {
            { 0, KukaResources.StatusAndTurnsFormatBinary },
            { 1, KukaResources.StatusAndTurnsFormatDecimal },
        };

        /// <summary>
        ///     Gets the feed output type.
        /// </summary>
        public static Dictionary<int, string> FeedOutputTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.None },
            { 1, KukaResources.FeedOutputFormatDirectToVelCp },
            { 2, KukaResources.FeedOutputFormatUsingBas },
        };

        /// <summary>
        ///     Gets the tool change output type.
        /// </summary>
        public static Dictionary<int, string> ToolChangeOutputTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.IfNeeded },
            { 1, KukaResources.Disable },
            { 2, KukaResources.Enable },
        };

        /// <summary>
        ///     Gets the tool change macro name type.
        /// </summary>
        public static Dictionary<int, string> ToolChangeMacroNameTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.CustomName },
            { 1, KukaResources.ToolName },
        };

        /// <summary>
        ///     Gets the first tool change output.
        /// </summary>
        public static Dictionary<int, string> FirstToolChangeOutputConditionSource => new Dictionary<int, string>
        {
            { 0, KukaResources.BeforeHome },
            { 1, KukaResources.AfterHome },
        };

        /// <summary>
        ///     Gets or sets the custom tool macro name with the TCP frame ID passed by value.
        /// </summary>
        public string ToolChangeMacroCustomName
        {
            get => this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolChangeMacroCustomName)).UserValue;

            set
            {
                this.GetParameterByPostVariable<TextParameter>(nameof(this.ToolChangeMacroCustomName)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum number of points allowed per individual code file. If 0, then there is no maximum.
        /// </summary>
        public int MaximumPointPerFile
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumPointPerFile)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumPointPerFile)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the advance look ahead value.
        ///     Robotic syntax: $ADVANCE.
        /// </summary>
        public int AdvanceLookAheadValue
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AdvanceLookAheadValue)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.AdvanceLookAheadValue)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the number of the current external base kinematic system.
        ///     Robotic syntax: $ACT_EX_AX.
        /// </summary>
        public int ExternalBaseKinematicSystemNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ExternalBaseKinematicSystemNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ExternalBaseKinematicSystemNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the machine definition number in the $config.dat file contains the ROOT values of the external axis.
        ///     Robotic syntax: MACHINE_DEF.
        /// </summary>
        public int MachineDefinitionNumber
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MachineDefinitionNumber)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MachineDefinitionNumber)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the file properties output.
        /// </summary>
        public bool IsOutputFilePropertiesEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputFilePropertiesEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputFilePropertiesEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of status and turns bit on every PTP move.
        /// </summary>
        public bool IsOutputStatusAndTurnsOnEveryPtpMoveEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputStatusAndTurnsOnEveryPtpMoveEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputStatusAndTurnsOnEveryPtpMoveEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of the basic package program BAS(#INITMOV,0) inside a fold command otherwise it will be output directly as a single command line.
        /// </summary>
        public bool IsOutputBasInitMoveInsideFoldSectionEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputBasInitMoveInsideFoldSectionEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputBasInitMoveInsideFoldSectionEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enables or disables the output of the table offset explicitly.
        /// </summary>
        public bool IsOutputTableOffsetExplicitlyEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputTableOffsetExplicitlyEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsOutputTableOffsetExplicitlyEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets indicates if the DAT file is to be output. It contains permanent data and point coordinates.
        /// </summary>
        public int DatFileOutput
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.DatFileOutput)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.DatFileOutput)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the base output type.
        ///     If by register, then base used is found in the register of the controller.
        ///     If by value, then base used is explicitly output in the robotic code.
        ///     If synchronized, then base is synchronized to external kinematics.
        ///     Robotic syntax: $BASE.
        /// </summary>
        public int BaseOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.BaseOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.BaseOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tool output type.
        ///     If by register, then base used is found in the register of the controller.
        ///     If by value, then base used is explicitly output in the robotic code.
        ///     Robotic syntax: $TOOL.
        /// </summary>
        public int ToolOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the output format of status and turns values.
        /// </summary>
        public int StatusAndTurnsOutputFormat
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.StatusAndTurnsOutputFormat)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.StatusAndTurnsOutputFormat)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the feed output type. Feeds can either be output directly to the $VEL.CP variable or can be set using the basic package program (BAS command).
        ///     Feeds can either be output directly to the $VEL.CP variable or can be set using the basic package program (BAS command).
        /// </summary>
        public int FeedOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.FeedOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.FeedOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the tool change macro output type.
        /// </summary>
        public int ToolChangeOutputType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolChangeOutputType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolChangeOutputType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsToolChangeMacroNameTypeEnabled));
                this.OnPropertyChanged(nameof(this.IsToolChangeMacroCustomNameEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the tool change macro name type.
        /// </summary>
        public int ToolChangeMacroNameType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolChangeMacroNameType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.ToolChangeMacroNameType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsToolChangeMacroCustomNameEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the first tool change output condition.
        /// </summary>
        public int FirstToolChangeOutputCondition
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.FirstToolChangeOutputCondition)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.FirstToolChangeOutputCondition)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsToolChangeMacroNameTypeEnabled));
                this.OnPropertyChanged(nameof(this.IsToolChangeMacroCustomNameEnabled));
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the tool change macro name type is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="ToolChangeOutputType"/> is not set to Disable otherwise, <c>false</c>.</returns>
        public bool IsToolChangeMacroNameTypeEnabled => this.ToolChangeOutputType != 1;

        /// <summary>
        ///     Gets a value indicating whether the tool change macro custom name is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="IsToolChangeMacroNameTypeEnabled"/> is true and the tool change macro name type is set to Custom name otherwise, <c>false</c>.</returns>
        public bool IsToolChangeMacroCustomNameEnabled => this.ToolChangeOutputType != 1 && this.ToolChangeMacroNameType == 0;
    }
}