// <copyright file="KukaProcessorSwitchesView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches
{
    /// <summary>
    ///     Interaction logic for KukaProcessorSwitches.
    /// </summary>
    public partial class KukaProcessorSwitchesView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaProcessorSwitchesView"/> class.
        /// </summary>
        public KukaProcessorSwitchesView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaProcessorSwitchesView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="KukaProcessorSwitchesViewModel"/> to map to the UI.
        /// </param>
        public KukaProcessorSwitchesView(KukaProcessorSwitchesViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public KukaProcessorSwitchesViewModel ViewModel { get; }
    }
}