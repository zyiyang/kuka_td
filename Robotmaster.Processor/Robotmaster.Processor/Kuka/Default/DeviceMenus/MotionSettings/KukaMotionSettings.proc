<?xml version="1.0" encoding="utf-8"?>
<ProcessPackage>
  <section ClassName="KukaMotionSettings">
    <SettingContract
      Label="Kuka Motion Settings"
      LabelResourceAssembly="Robotmaster.Processor.dll"
      LabelResourceTypeName="Robotmaster.Processor.Kuka.Default.KukaResources"
      LabelResourceVariableName="KukaMotionSettingsLabel"
      ContractKey="DM_KukaMotionSettings_3A6D5D77"
      Domain="Cell"
      UiHandlerAssemblyName="Robotmaster.Processor.dll"
      UiHandlerBehaviorName="Robotmaster.Processor.Kuka.Default.DeviceMenus.MotionSettings.KukaMotionSettingsBehavior"
      DataHandlerAssemblyName="null"
      DataHandlerTypeName="null"
      DataHandlerMethodName="null"
      DataTypeAssemblyName="Robotmaster.Rise.dll"
      DataTypeName="Robotmaster.Rise.BaseClasses.Settings.ExternalMenuData"
      ImageUri="null"
      ImageKey="f0c1" />
    <CustomSettings>
      <Cat label="Kuka - Motion Settings" enable="true" visible="true">
        <ListParam label="Joint positioning type" defVal="0" postVar="JointPositioningType" outputMode="index" enable="true" visible="true" UID="{1B2BAA64-00DD-4F51-9365-6438B8F22AD2}">
          <Desc>
            <Text line="The positioning type for joint motion." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Exact" />
            <Item index="1" itemLabel="Continuous" />
          </List>
        </ListParam>
        <ListParam label="Linear positioning type" defVal="1" postVar="LinearPositioningType" outputMode="index" enable="true" visible="true" UID="{B86261EF-9018-404D-9FA6-220A454FADCA}">
          <Desc>
            <Text line="The positioning type for linear motion." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Exact" />
            <Item index="1" itemLabel="Continuous" />
          </List>
        </ListParam>
        <ListParam label="Circular positioning type" defVal="1" postVar="CircularPositioningType" outputMode="index" enable="true" visible="true" UID="{4AB7B6FC-547B-47C7-868E-2602AF9CD6E6}">
          <Desc>
            <Text line="The positioning type for circular motion." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Exact" />
            <Item index="1" itemLabel="Continuous" />
          </List>
        </ListParam>
        <ListParam label="Linear positioning criterion type" defVal="0" postVar="LinearPositioningCriterionType" outputMode="index" enable="true" visible="true" UID="{76313DF2-F0CD-4985-8C75-1AAB3FF1D012}">
          <Desc>
            <Text line="The positioning criterion type for linear motion." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Distance" />
            <Item index="1" itemLabel="Orientation" />
            <Item index="2" itemLabel="Velocity" />
          </List>
        </ListParam>
        <ListParam label="Circular positioning criterion type" defVal="0" postVar="CircularPositioningCriterionType" outputMode="index" enable="true" visible="true" UID="{B5B6C1AA-99F7-4AF4-A927-C1E899F91474}">
          <Desc>
            <Text line="The positioning criterion type for circular motion." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Distance" />
            <Item index="1" itemLabel="Orientation" />
            <Item index="2" itemLabel="Velocity" />
          </List>
        </ListParam>
        <ListParam label="Orientation control" defVal="1" postVar="OrientationControlType" outputMode="index" enable="true" visible="true" UID="{A58C7217-B516-4484-A2CB-58C62A4FFE98}">
          <Desc>
            <Text line="The orientation control of a CP motion in the advance run." />
            <Text line="Robotic syntax: $ORI_TYPE." />
          </Desc>
          <List>
            <Item index="0" itemLabel="Off" />
            <Item index="1" itemLabel="Variable" />
            <Item index="2" itemLabel="Joint" />
          </List>
        </ListParam>
        <BoolParam label="Joint velocity and acceleration" defVal="true" postVar="IsJointVelocityAndAccelerationEnabled" enable="true" visible="true" UID="{70267B3E-93EF-49B0-9DBE-9469725EE2FA}">
          <Desc>
            <Text line="Enables or disables the joint velocity and acceleration output." />
          </Desc>
        </BoolParam>
        <NumParam label="Joint axis velocity" type="integer" defVal="50" postVar="JointAxisVelocity" enable="true" visible="true" UID="{522483D9-2AD8-447D-9C51-CDCE52E3DD34}">
          <Desc>
            <Text line="The joint axis velocity (%)." />
            <Text line="Robotic syntax: $VEL_AXIS[I]." />
          </Desc>
        </NumParam>
        <NumParam label="Joint axis acceleration" type="integer" defVal="50" postVar="JointAxisAcceleration" enable="true" visible="true" UID="{4EA414B6-4847-410D-8067-B6951D6A2FA1}">
          <Desc>
            <Text line="The joint axis acceleration (%)." />
            <Text line="Robotic syntax: $ACC_AXIS[I]." />
          </Desc>
        </NumParam>
        <BoolParam label="Continuous path parameters output" defVal="true" postVar="IsContinuousPathParametersEnabled" enable="true" visible="true" UID="{EDC9366C-3D3E-4545-9270-5BB4084209BC}">
          <Desc>
            <Text line="Enables or disables the continuous path parameters output." />
          </Desc>
        </BoolParam>
        <NumParam label="TCP swivel velocity" type="double" defVal="200" postVar="TcpSwivelVelocity" enable="true" visible="true" UID="{EAC65839-1B54-4850-9C00-DC95B697017F}">
          <Desc>
            <Text line="The TCP swivel velocity value (deg/s)." />
            <Text line="Robotic syntax: $VEL.ORI1."/>
          </Desc>
        </NumParam>
        <NumParam label="TCP Rotational Velocity" type="double" defVal="200" postVar="TcpRotationalVelocity" enable="true" visible="true" UID="{FFD55FCD-C80D-49FE-A44D-CD5EF4C3339F}">
          <Desc>
            <Text line="The TCP rotational velocity (deg/s)." />
            <Text line="Robotic syntax: $VEL.ORI2."/>
          </Desc>
        </NumParam>
        <NumParam label="TCP swivel acceleration" type="double" defVal="100" postVar="TcpSwivelAcceleration" enable="true" visible="true" UID="{5AD74268-95EC-47B3-B8CF-6A58D8622C38}">
          <Desc>
            <Text line="The TCP swivel acceleration (deg/s^2)." />
            <Text line="Robotic syntax: $ACC.ORI1."/>
          </Desc>
        </NumParam>
        <NumParam label="TCP rotational acceleration" type="double" defVal="100" postVar="TcpRotationalAcceleration" enable="true" visible="true" UID="{49FD3CB5-AE43-4B0D-B1D5-A02BEFFFAA76}">
          <Desc>
            <Text line="The TCP rotational acceleration (deg/s^2)." />
            <Text line="Robotic syntax: $ACC.ORI2."/>
          </Desc>
        </NumParam>
        <NumParam label="TCP path acceleration" type="double" defVal="3" postVar="TcpPathAcceleration" enable="true" visible="true" UID="{3CD52672-754B-4CFE-8C1F-72B3A2427716}">
          <Desc>
            <Text line="The TCP path acceleration (m/s^2)." />
            <Text line="Robotic syntax: $ACC.CP."/>
          </Desc>
        </NumParam>
        <BoolParam label="Approximation parameters" defVal="true" postVar="IsApproximationEnabled" enable="true" visible="true" UID="{BB30E58E-9B0E-4832-8736-999C237D1553}">
          <Desc>
            <Text line="Enables or disables the approximation parameters output." />
          </Desc>
        </BoolParam>
        <NumParam label="Approximation distance parameter" type="double" defVal="0.5" postVar="ApproximationDistanceParameter" enable="true" visible="true" UID="{1CD252AC-9286-4228-A9C9-11C706E0DA90}">
          <Desc>
            <Text line="The approximation distance parameter (mm)." />
            <Text line="Robotic syntax: $APO.CDIS."/>
          </Desc>
        </NumParam>
        <NumParam label="Approximation orientation parameter" type="double" defVal="0.0" postVar="ApproximationOrientationParameter" enable="true" visible="true" UID="{B8E4ECFF-F657-4CFF-B7C6-A7357910E6D3}">
          <Desc>
            <Text line="The approximation orientation parameter (deg)." />
            <Text line="Robotic syntax: $APO.CORI."/>
          </Desc>
        </NumParam>
        <NumParam label="Approximation distance for PTP motion" type="integer" defVal="30" postVar="ApproximationDistanceForPtpMotionParameter" enable="true" visible="true" UID="{D04AE002-A607-4E64-8455-F7A29295989B}">
          <Desc>
            <Text line="The approximation distance for PTP motion (%)." />
            <Text line="Robotic syntax: $APO.CPTP." />
          </Desc>
        </NumParam>
        <NumParam label="Approximation velocity parameter" type="double" defVal="0.0" postVar="ApproximationVelocityParameter" enable="true" visible="true" UID="{BC607C63-1718-4FD6-98B0-843D26EA3B25}">
          <Desc>
            <Text line="The approximation velocity parameter (%)." />
            <Text line="Robotic syntax: $APO.CVEL."/>
          </Desc>
        </NumParam>
      </Cat>
    </CustomSettings>
  </section>
</ProcessPackage>