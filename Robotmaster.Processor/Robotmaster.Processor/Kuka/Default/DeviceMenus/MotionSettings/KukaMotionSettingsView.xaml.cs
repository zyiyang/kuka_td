﻿// <copyright file="KukaMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for KukaMotionSettings.
    /// </summary>
    public partial class KukaMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaMotionSettingsView"/> class.
        /// </summary>
        public KukaMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="KukaMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public KukaMotionSettingsView(KukaMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public KukaMotionSettingsViewModel ViewModel { get; }
    }
}