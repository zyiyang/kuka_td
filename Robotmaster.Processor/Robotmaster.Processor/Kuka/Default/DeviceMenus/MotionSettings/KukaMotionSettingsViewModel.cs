﻿// <copyright file="KukaMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     KukaMotionSettingsViewModel View Model.
    /// </summary>
    public class KukaMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        /// <summary>
        ///     Gets the positioning type dictionary.
        /// </summary>
        public static Dictionary<int, string> PositioningTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.Exact },
            { 1, KukaResources.Continuous },
        };

        /// <summary>
        ///     Gets the orientation type dictionary.
        /// </summary>
        public static Dictionary<int, string> OrientationTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.Off },
            { 1, KukaResources.Variable },
            { 2, KukaResources.Joint },
        };

        /// <summary>
        ///     Gets the criterion types.
        /// </summary>
        public static Dictionary<int, string> CriterionTypeSource => new Dictionary<int, string>
        {
            { 0, KukaResources.Distance },
            { 1, KukaResources.Orientation },
            { 2, KukaResources.Velocity },
        };

        /// <summary>
        ///     Gets or sets the joint axis velocity.
        /// </summary>
        public int JointAxisVelocity
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis acceleration.
        /// </summary>
        public int JointAxisAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointAxisAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to use joint velocity and acceleration.
        /// </summary>
        public bool IsJointVelocityAndAccelerationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointVelocityAndAccelerationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointVelocityAndAccelerationEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint positioning type.
        /// </summary>
        public int JointPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.JointPositioningType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.JointPositioningType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceForPtpMotionParameterEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the linear positioning type.
        /// </summary>
        public int LinearPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearPositioningType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearPositioningType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationOrientationParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationVelocityParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsLinearPositioningCriterionTypeEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the circular positioning type.
        /// </summary>
        public int CircularPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularPositioningType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularPositioningType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationOrientationParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationVelocityParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsCircularPositioningCriterionTypeEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the positioning criterion type for linear motion.
        /// </summary>
        public int LinearPositioningCriterionType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearPositioningCriterionType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.LinearPositioningCriterionType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationOrientationParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationVelocityParameterEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the positioning criterion type for circular motion.
        /// </summary>
        public int CircularPositioningCriterionType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularPositioningCriterionType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.CircularPositioningCriterionType)).SelectedIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationOrientationParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationVelocityParameterEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the orientation control type.
        /// </summary>
        public int OrientationControlType
        {
            get => this.GetParameterByPostVariable<ListParameter>(nameof(this.OrientationControlType)).SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>(nameof(this.OrientationControlType)).SelectedIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to use the continuous path parameters.
        /// </summary>
        public bool IsContinuousPathParametersEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsContinuousPathParametersEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsContinuousPathParametersEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP swivel velocity.
        /// </summary>
        public double TcpSwivelVelocity
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpSwivelVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpSwivelVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP rotational velocity.
        /// </summary>
        public double TcpRotationalVelocity
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpRotationalVelocity)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpRotationalVelocity)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP swivel acceleration.
        /// </summary>
        public double TcpSwivelAcceleration
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpSwivelAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpSwivelAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP rotational acceleration.
        /// </summary>
        public double TcpRotationalAcceleration
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpRotationalAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.TcpRotationalAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the TCP path acceleration.
        /// </summary>
        public int TcpPathAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.TcpPathAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.TcpPathAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to use the approximation parameters.
        /// </summary>
        public bool IsApproximationEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsApproximationEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsApproximationEnabled)).UserValue = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceForPtpMotionParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationDistanceParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationOrientationParameterEnabled));
                this.OnPropertyChanged(nameof(this.IsApproximationVelocityParameterEnabled));
            }
        }

        /// <summary>
        ///     Gets or sets the approximation distance.
        /// </summary>
        public double ApproximationDistanceParameter
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationDistanceParameter)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationDistanceParameter)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the approximation orientation.
        /// </summary>
        public double ApproximationOrientationParameter
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationOrientationParameter)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationOrientationParameter)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the approximation velocity.
        /// </summary>
        public double ApproximationVelocityParameter
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationVelocityParameter)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.ApproximationVelocityParameter)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the approximation distance for PTP motion.
        /// </summary>
        public int ApproximationDistanceForPtpMotionParameter
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ApproximationDistanceForPtpMotionParameter)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.ApproximationDistanceForPtpMotionParameter)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the <see cref="IsLinearPositioningCriterionTypeEnabled"/> is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="IsLinearPositioningCriterionTypeEnabled"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsLinearPositioningCriterionTypeEnabled => this.LinearPositioningType == 1;

        /// <summary>
        ///     Gets a value indicating whether the <see cref="IsCircularPositioningCriterionTypeEnabled"/> is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="IsCircularPositioningCriterionTypeEnabled"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsCircularPositioningCriterionTypeEnabled => this.CircularPositioningType == 1;

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ApproximationDistanceParameter"/> is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="ApproximationDistanceParameter"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsApproximationDistanceParameterEnabled => this.IsApproximationEnabled &&
                                                               ((this.LinearPositioningType == 1 && this.LinearPositioningCriterionType == 0) ||
                                                                (this.CircularPositioningType == 1 && this.CircularPositioningCriterionType == 0));

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ApproximationOrientationParameter"/> is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="ApproximationOrientationParameter"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsApproximationOrientationParameterEnabled => this.IsApproximationEnabled &&
                                                                  ((this.LinearPositioningType == 1 && this.LinearPositioningCriterionType == 1) ||
                                                                   (this.CircularPositioningType == 1 && this.CircularPositioningCriterionType == 1));

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ApproximationVelocityParameter"/> is enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="ApproximationVelocityParameter"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsApproximationVelocityParameterEnabled => this.IsApproximationEnabled &&
                                                               ((this.LinearPositioningType == 1 && this.LinearPositioningCriterionType == 2) ||
                                                                (this.CircularPositioningType == 1 && this.CircularPositioningCriterionType == 2));

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ApproximationDistanceForPtpMotionParameter"/> enabled.
        /// </summary>
        /// <returns><c>true</c> if <see cref="ApproximationDistanceForPtpMotionParameter"/> is enabled otherwise, <c>false</c>.</returns>
        public bool IsApproximationDistanceForPtpMotionParameterEnabled => this.IsApproximationEnabled && this.JointPositioningType == 1;
    }
}
