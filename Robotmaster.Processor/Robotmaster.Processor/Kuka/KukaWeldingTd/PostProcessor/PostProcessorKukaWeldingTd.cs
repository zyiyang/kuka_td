﻿// <copyright file="PostProcessorKukaWeldingTd.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Kuka.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.KukaInline.PostProcessor;
    using Robotmaster.Processor.Kuka.KukaInline.ProcessMenus.MotionSettings;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.Event;

    internal class PostProcessorKukaWeldingTd : PostProcessorKukaInline
    {
        /// <summary>
        ///     Defines the Process Status Enum.
        /// </summary>
        internal enum ProcessStatus
        {
            None,
            Search,
            AfterSearch,
            ArcOn,
            ArcSwitch,
            ArcOff,
        }

        /// <summary>
        ///     Gets or sets the Weld Data Number.
        /// </summary>
        internal int WeldDataNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Weld Seam Number.
        /// </summary>
        internal int WeldSeamNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Process Status.
        /// </summary>
        internal ProcessStatus CurrentProcessStatus { get; set; }

        /// <summary>
        ///     Gets or sets the Cached Touch Search Event List.
        /// </summary>
        internal List<TouchSearch> CachedTouchSearchList { get; set; }

        // ADD for MultiPass
        //internal virtual PostFile MultiPassDatFile { get; set; }
        //internal virtual PostFile MultiPassSrcFile { get; set; }
        //internal virtual StreamWriter MultiPassDatWriter => this.MultiPassDatFile.FileSection.StreamWriter;
        //internal virtual StreamWriter MultiPassSrcWriter => this.MultiPassSrcFile.FileSection.StreamWriter;

        internal virtual StreamWriter MultiPassDatWriter => this.CurrentPostFile.Children[2].FileSection.StreamWriter;
        internal virtual StreamWriter MultiPassSrcWriter => this.CurrentPostFile.Children[1].FileSection.StreamWriter;
        internal virtual bool LcpdatOutput { get; set; } = false;
        internal virtual bool MultiPassOutput { get; set; } = false;
        internal virtual int MultiLayer { get; set; } = 6;
        internal virtual int MPassSegmentCounter { get; set; }
        internal virtual int MPassPointCounter { get; set; }
        internal virtual int ArcOnPointTemp { get; set; }
        internal virtual string MPassPositiv { get; set; } = "";
        internal virtual string MPassNegativ { get; set; } = "";
        internal virtual string E6mlDat { get; set; } = ";FOLD e6ML_SP0_ACT[128] Trigger Points of Root-Layer local" + "\r\n" +
                                                        "E6POS e6ML_SP0_ACT[128]" + "\r\n";
        internal virtual string FmlDat { get; set; } = ";FOLD fML_SP0_TTS[128] Trigger TTS-Points of Root-Layer local" + "\r\n" +
                                                        "FRAME fML_SP0_TTS[128]" + "\r\n";         
        internal virtual string MPassWeldPar { get; set; }






        internal virtual string MultiPassPosLayer(int LayerNum, int Xvalue, int Yvalue, int Zvalue)
        {
            return string.Format(";FOLD MultiLayer.OFFSET_LOCAL Layer: {0}, X {1}mm, Y {2}mm, Z {3}mm, A 0? B 0? C 0? Start X= 0mm, End X= 0mm, WeldPar: PAR;%{{PE}}%MKUKATPUSER" + "\r\n" +
                "" + "\r\n" +
                "iML_LayerFlag = {0}" + "\r\n" +
                "IF (ABS(LAYER))==iML_LayerFlag THEN " + "\r\n" +
                "  INTERRUPT OFF 21" + "\r\n" +
                "  fML_PathOffset[{0}]= {{x {1}, y {2}, z {3}, a 0, b 0, c 0}}" + "\r\n" +
                "  FOR pkt_nr=1 TO iML_pkt_max " + "\r\n" +
                "     fML_StartPointOffset[{0}]={{X 0.0, Y 0.0, Z 0.0, A 0.0, B 0.0, C 0.0}}" + "\r\n" +
                "     fML_EndPointOffset[{0}]={{X 0.0, Y 0.0, Z 0.0, A 0.0, B 0.0, C 0.0}}" + "\r\n" +
                "     IF pkt_nr==1 THEN" + "\r\n" +
                "        fML_StartPointOffset[{0}].X=fML_PointOffset{0}[1].x + (0)" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "     IF pkt_nr==iML_pkt_max THEN" + "\r\n" +
                "        fML_EndPointOffset[{0}].X=fML_PointOffset{0}[pkt_max].x + (0)" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "     ML_Calculation(fML_PathOffset[{0}],fML_PointOffset{0}[pkt_nr], e6ML_SP0_ACT[pkt_nr], fML_SP0_TTS[pkt_nr], e6ML_CP0_OST, fML_StartPointOffset[{0}], fML_EndPointOffset[{0}], rML_SP0_LBY[pkt_nr], rML_SP0_LBZ[pkt_nr], iML_SP0_INF[pkt_nr]) " + "\r\n" +
                "     e6ML_CP0[pkt_nr]=e6ML_CP0_OST" + "\r\n" +
                "     IF (e6ML_CP0[pkt_nr].X==9999.9) THEN" + "\r\n" +
                "        ML_CT_ErrMsg(3,1,pkt_nr)" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "  ENDFOR " + "\r\n" +
                "  ML_CP_to_XSP()" + "\r\n" +
                "  AS_MLDAT = AS_PAR" + "\r\n" +
                "  AW_MLDAT = AW_PAR" + "\r\n" +
                "  WV_MLDAT = WV_PAR" + "\r\n" +
                "  AC_MLDAT = AC_PAR" + "\r\n" +
                "  $CYCFLAG[101] = TRUE" + "\r\n" +
                "ENDIF " + "\r\n" +
                "" + "\r\n" +
                ";ENDFOLD" + "\r\n",
                LayerNum,
                Xvalue,
                Yvalue,
                Zvalue);

        }

        internal virtual string MultiPassNegLayer(int LayerNum, int Xvalue, int Yvalue, int Zvalue)
        {
            return string.Format(
                ";FOLD MultiLayer.OFFSET DataBase: 1, Layer: {0}, WeldPar: PAR;%{{PE}}%MKUKATPUSER" + "\r\n" +
                "" + "\r\n" +
                "iML_LayerFlag = {0}" + "\r\n" +
                "IF (ABS(LAYER))==iML_LayerFlag THEN " + "\r\n" +
                "  INTERRUPT OFF 21" + "\r\n" +
                "  fML_PathOffset[{0}].X= ML_Seam[1,2].X" + "\r\n" +
                "  fML_PathOffset[{0}].Y= ML_Seam[1,2].Y" + "\r\n" +
                "  fML_PathOffset[{0}].Z= ML_Seam[1,2].Z" + "\r\n" +
                "  fML_PathOffset[{0}].A= ML_Seam[1,2].A" + "\r\n" +
                "  fML_PathOffset[{0}].B= ML_Seam[1,2].B" + "\r\n" +
                "  fML_PathOffset[{0}].C= ML_Seam[1,2].C" + "\r\n" +
                "  FOR pkt_nr=1 TO iML_pkt_max " + "\r\n" +
                "     fML_StartPointOffset[{0}]={{X 0.0, Y 0.0, Z 0.0, A 0.0, B 0.0, C 0.0}}" + "\r\n" +
                "     fML_EndPointOffset[{0}]={{X 0.0, Y 0.0, Z 0.0, A 0.0, B 0.0, C 0.0}}" + "\r\n" +
                "     IF pkt_nr==1 THEN" + "\r\n" +
                "        fML_StartPointOffset[{0}].X=ML_Seam[1,2].X_Start" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "     IF pkt_nr==iML_pkt_max THEN" + "\r\n" +
                "        fML_EndPointOffset[{0}].X=ML_Seam[1,2].X_End" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "     ML_Calculation(fML_PathOffset[{0}],fML_PointOffset{0}[pkt_nr], e6ML_SP0_ACT[pkt_nr], fML_SP0_TTS[pkt_nr], e6ML_CP0_OST, fML_StartPointOffset[{0}], fML_EndPointOffset[{0}], rML_SP0_LBY[pkt_nr], rML_SP0_LBZ[pkt_nr], iML_SP0_INF[pkt_nr]) " + "\r\n" +
                "     e6ML_CP0[pkt_nr]=e6ML_CP0_OST" + "\r\n" +
                "     IF (e6ML_CP0[pkt_nr].X==9999.9) THEN" + "\r\n" +
                "        ML_CT_ErrMsg(3,1,pkt_nr)" + "\r\n" +
                "     ENDIF" + "\r\n" +
                "  ENDFOR " + "\r\n" +
                "  ML_CP_to_XSP()" + "\r\n" +
                "  AS_MLDAT = AS_PAR" + "\r\n" +
                "  AW_MLDAT = AW_PAR" + "\r\n" +
                "  WV_MLDAT = WV_PAR" + "\r\n" +
                "  AC_MLDAT = AC_PAR" + "\r\n" +
                "  $CYCFLAG[101] = TRUE" + "\r\n" +
                "ENDIF " + "\r\n" +
                "" + "\r\n" +
                ";ENDFOLD" + "\r\n",
                LayerNum,
                Xvalue,
                Yvalue,
                Zvalue);

        }

        internal virtual string MultiPassSrcFooter(int Layer)
        {
            return "" + "\r\n" +
                   ";FOLD MultiLayer.LAYER Direction: POSITIV, -----------------------------;%{PE}%MKUKATPUSER" + "\r\n" +
                   "" + "\r\n" +
                   "CASE 2" + "\r\n" +
                   "ML_CheckErr()" + "\r\n" +
                   "TRIGGER WHEN DISTANCE=0 DELAY=0 DO ML_CT_ErrMsg(2,1,giLAYER) PRIO=-1" + "\r\n" +
                   ";ENDFOLD" + "\r\n" +
                   MultiPassPosLayer(2, 0, 3, 2) +
                   MultiPassPosLayer(3, 0, 5, 3) +
                   MultiPassPosLayer(4, 0, 8, 5) +
                   MultiPassPosLayer(5, 0, 10, 6) +
                   MultiPassPosLayer(6, 0, 13, 8) +
                   this.MPassPositiv +
                   ";FOLD MultiLayer.LAYER Direction: NEGATIV, -----------------------------;%{PE}%MKUKATPUSER" + "\r\n" +
                   ";ENDFOLD" + "\r\n" +
                   "" + "\r\n" +
                   "CASE 3" + "\r\n" +
                   "ML_CheckErr()" + "\r\n" +
                   "TRIGGER WHEN DISTANCE=0 DELAY=0 DO ML_CT_ErrMsg(2,1,giLAYER) PRIO=-1" + "\r\n" +
                   ";ENDFOLD" + "\r\n" +
                   MultiPassNegLayer(2, 0, 0, 0) +
                   this.MPassNegativ +
                   ";FOLD MultiLayer.OFF ========================================;%{PE}%MKUKATPUSER" + "\r\n" +
                   "" + "\r\n" +
                   "DEFAULT " + "\r\n" +
                   "ML_CT_ErrMsg(2,1,layer) " + "\r\n" +
                   "ENDSWITCH " + "\r\n" +
                   "WAIT SEC 0" + "\r\n" +
                   "IF RootLayer THEN" + "\r\n" +
                   "   iML_pkt_max = pkt_max" + "\r\n" +
                   "   FOR pkt_nr=1 TO iML_pkt_max" + "\r\n" +
                   "      e6ML_SP0_ACT[pkt_nr]=ge6ML_SP0_ACT[pkt_nr]" + "\r\n" +
                   "      fML_SP0_TTS[pkt_nr]=gfML_SP0_TTS[pkt_nr]" + "\r\n" +
                   "      iML_SP0_INF[pkt_nr]=giML_SP0_INF[pkt_nr]" + "\r\n" +
                   "   ENDFOR" + "\r\n" +
                   "ENDIF" + "\r\n" +
                   "RootLayer = FALSE" + "\r\n" +
                   "$CYCFLAG[101] = FALSE" + "\r\n" +
                   ";ENDFOLD" + "\r\n" +
                   "END" + "\r\n";
        }
        internal virtual void MultiPassFixdatPostFile(PostFile postFile)
        {
            postFile.FileSection.Header =
                "&ACCESS RVO1" + "\r\n" +
                "&REL 2" + "\r\n" +
                "&PARAM EDITMASK = *" + "\r\n" +
                "&PARAM TEMPLATE = C:\\KRC\\Roboter\\Template\\MultiLayer_CP\\Template_CP" + "\r\n" +
                "DEFDAT  " + this.CurrentPostFile.Children[1].FileName + "\r\n" +
                ";FOLD EXTERNAL DECLARATIONS;%{PE}%MKUKATPBASIS,%CEXT,%VCOMMON,%P" + "\r\n" +
                ";FOLD BASISTECH EXT;%{PE}%MKUKATPBASIS,%CEXT,%VEXT,%P" + "\r\n" +
                "EXT  BAS (BAS_COMMAND  :IN,REAL  :IN )" + "\r\n" +
                "DECL INT SUCCESS" + "\r\n" +
                ";ENDFOLD (BASISTECH EXT)" + "\r\n" +
                ";FOLD USER EXT;%{E}%MKUKATPUSER,%CEXT,%VEXT,%P" + "\r\n" +
                ";Make your modifications here" + "\r\n" +
                ";ENDFOLD (USER EXT)" + "\r\n" +
                ";FOLD MultiLayer" + "\r\n" +
                "DECL INT iML_SP0_INF[128],iML_pkt_max" + "\r\n";
                //"" + "\r\n" +;
        }

        internal virtual void MultiPassFixPostFile(PostFile postFile)
        {
            postFile.FileSection.Header = "&ACCESS RVO1" + "\r\n" +
            "&REL 2" + "\r\n" +
            "&PARAM EDITMASK = *" + "\r\n" +
            "&PARAM TEMPLATE = C:\\KRC\\Roboter\\Template\\MultiLayer_CP\\Template_CP" + "\r\n" +
            "DEF " + this.CurrentPostFile.Children[1].FileName + "(LAYER:IN )" + "\r\n" +
            ";FOLD DECL" + "\r\n" +
            "DECL INT LAYER, LayerState " + "\r\n" +
            ";ENDFOLD (DECL)" + "\r\n" +
            ";FOLD INI;%{PE}" + "\r\n" +
            "  ;FOLD BASISTECH INI" + "\r\n" +
            "    GLOBAL INTERRUPT DECL 3 WHEN $STOPMESS==TRUE DO IR_STOPM ( )" + "\r\n" +
            "    INTERRUPT ON 3" + "\r\n" +
            "    BAS (#INITMOV,0 )" + "\r\n" +
            "  ;ENDFOLD (BASISTECH INI)" + "\r\n" +
            "  ;FOLD USER INI" + "\r\n" +
            "    ;Make your modifications here" + "\r\n" +
            "	giLayer = LAYER" + "\r\n" +
            "	Sens=1" + "\r\n" +
            "  ;ENDFOLD (USER INI)" + "\r\n" +
            ";ENDFOLD (INI)" + "\r\n" +
            ";FOLD WeldingParameter" + "\r\n" +
            "IF FALSE THEN" + "\r\n" +
            ";program your parameters here. To teach point is not necassary." + "\r\n" +
            ";FOLD ARCON PAR PTP Dummy Vel=100 % PDAT1 Tool[2]:tool3 Base[0] ;%{PE}" + "\r\n" +
            ";FOLD Parameters ;%{h}" + "\r\n" +
            ";Params IlfProvider=kukaroboter.arctech.arconptp; Kuka.PointName=Dummy; Kuka.BlendingEnabled=False; Kuka.MoveDataPtpName=PDAT1; Kuka.VelocityPtp=100; Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas5; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=PAR; ArcTech.SeamName=" + "\r\n" +
            ";FOLD Parameters ArcTechAdvanced ;%{h}" + "\r\n" +
            ";Params IgnitionErrorStrategy=1; WeldErrorStrategy=1; SlopeDistance=0" + "\r\n" +
            ";ENDFOLD ArcTechAdvancedFoldEnd" + "\r\n" +
            ";ENDFOLD" + "\r\n" +
            "$BWDSTART = FALSE" + "\r\n" +
            "PDAT_ACT = PPDAT1" + "\r\n" +
            "FDAT_ACT = FDummy" + "\r\n" +
            "BAS(#PTP_PARAMS, 100.0)" + "\r\n" +
            "TRIGGER WHEN DISTANCE = 1 DELAY = ATBg_PreDefinitionTime DO Arc_DefineStrikeParams(1, AS_PAR) PRIO = -1" + "\r\n" +
            "Arc_DefineCpPattern(#OffInAdvance, WV_PAR, TRUE)" + "\r\n" +
            "PTP XDummy" + "\r\n" +
            "Arc_On(1, AS_PAR, ATBg_StartErrSetField[1], AW_PAR, WV_PAR, ATBg_WeldErrSetField[1], #StdArcOn, \" \")" + "\r\n" +
            "Arc_DefineCpPattern(#OnInAdvance, WV_PAR, TRUE)" + "\r\n" +
            ";ENDFOLD" + "\r\n" +
            ";FOLD ARCOFF PAR LIN Dummy CPDAT1 Tool[2]:tool3 Base[0] ;%{PE}" + "\r\n" +
            ";FOLD Parameters ;%{h}" + "\r\n" +
            ";Params IlfProvider=kukaroboter.arctech.arcofflin; Kuka.PointName=Dummy; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT1;  Kuka.MovementParameterFieldEnabled=True; ArcSense.Enabled=False; ArcTech.WdatVarName=PAR" + "\r\n" +
            ";ENDFOLD" + "\r\n" +
            "$BWDSTART = FALSE" + "\r\n" +
            "LDAT_ACT = LCPDAT1" + "\r\n" +
            "FDAT_ACT = FDummy" + "\r\n" +
            "BAS(#CP_PARAMS, ATBg_BAS_VELDefinition)" + "\r\n" +
            "LIN XDummy" + "\r\n" +
            "Arc_Off(1, AC_PAR)" + "\r\n" +
            ";ENDFOLD" + "\r\n" +
            "ENDIF" + "\r\n" +
            ";ENDFOLD (WeldingParameter)" + "\r\n" +
            "" + "\r\n" +
            ";FOLD MultiLayer.ROOT : 1, ==================================;%{PE}%MKUKATPUSER" + "\r\n" +
            "" + "\r\n" +
            "ML_CT_INI()" + "\r\n" +
            "IF LAYER == 1 THEN" + "\r\n" +
            "LayerState = 1" + "\r\n" +
            "ENDIF" + "\r\n" +
            "IF (LAYER > 1) AND (LAYER <= 100) THEN" + "\r\n" +
            "LayerState = 2" + "\r\n" +
            "ENDIF" + "\r\n" +
            "IF (LAYER < -1) AND (LAYER >= -100) THEN" + "\r\n" +
            "LayerState = 3" + "\r\n" +
            "ENDIF" + "\r\n" +
            "SWITCH LayerState " + "\r\n" +
            "" + "\r\n" +
            "CASE 1" + "\r\n" +
            "ML_Reset() " + "\r\n" +
            "pkt_nr = 1 " + "\r\n" +
            "pkt_max = 0 " + "\r\n" +
            "iML_pkt_max = 0" + "\r\n" +
            "ML_CT_Root() " + "\r\n" +
            "RootLayer = TRUE" + "\r\n" +
            "ML_SP_INI()" + "\r\n" +
            ";ENDFOLD" + "\r\n";
        }


        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            KukaProcessorSwitches.SetBaseOutputType(this.CellSettingsNode, "0");
            KukaProcessorSwitches.SetToolOutputType(this.CellSettingsNode, "0");

            base.RunBeforeProgramOutput();

            //add LCPDAT plus
            this.LdatNumber = 2;

            this.WeldDataNumber = 9;
            this.WeldSeamNumber = 0;
            this.CurrentProcessStatus = ProcessStatus.None;
            this.CachedTouchSearchList = new List<TouchSearch>();

            //this.MultiPassOutput = SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("MultiPass", StringComparison.OrdinalIgnoreCase) != -1;
            this.MultiPassOutput = true;
            // add for new Mp 2020.3
            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);
            this.CurrentPostFile.InsertIntermediateParent();
            this.CurrentPostFile.Parent.FileName = this.ProgramName + "main";
            this.CurrentPostFile.Parent.FileExtension = FormatManagerKuka.MainProgramExtension;
            this.StartMainPostFile(this.CurrentPostFile.Parent, firstOperation, firstPoint);

            if (this.MultiPassOutput)
            {
                this.MPassSegmentCounter = 0;
                this.MPassPointCounter = 0;
            }
        }

        /// <inheritdoc/>
        internal override void InitializeKrcVersion()
        {
            //if (SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("_Krc2V55", StringComparison.OrdinalIgnoreCase) != -1) //// Specific for KSS 5.5
            //{
            //    this.KrcVersion = KukaKrcVersion.Krc2V55;
            //}
            //else if (SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("_Krc4V83", StringComparison.OrdinalIgnoreCase) != -1) //// Specific for KSS 8.3
            //{
            //    this.KrcVersion = KukaKrcVersion.Krc4V83;
            //}
            //else if (SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("_KRC2", StringComparison.OrdinalIgnoreCase) != -1) //// KRC2 fall-back
            //{
            //    this.KrcVersion = KukaKrcVersion.Krc2V55;
            //}
            //else if (SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("_KRC4", StringComparison.OrdinalIgnoreCase) != -1) //// KRC4 fall-back
            //{
            //    this.KrcVersion = KukaKrcVersion.Krc4V83;
            //}
            //else //// General fall-back
            //{
            //    this.KrcVersion = KukaKrcVersion.Krc2V55;
            //}
            this.KrcVersion = KukaKrcVersion.Krc4V83;
        }

        /// <inheritdoc/>
        internal override string FormatBasInitialization()
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = ";FOLD INI" + "\r\n" +
                             ";FOLD BASISTECH INI" + "\r\n" +
                             "GLOBAL INTERRUPT DECL 3 WHEN $STOPMESS==TRUE DO IR_STOPM ( )" + "\r\n" +
                             "INTERRUPT ON 3" + "\r\n" +
                             "BAS (#INITMOV,0 )" + "\r\n" +
                             ";ENDFOLD (BASISTECH INI)" + "\r\n" +
                             ";FOLD ARCTECH_ANALOG INI" + "\r\n" +
                             "IF A10_OPTION==#ACTIVE THEN" + "\r\n" +
                             "INTERRUPT DECL A_Arc_Control_Intr WHEN $CYCFLAG[A_CycFlagIndex1]==FALSE DO A10 (#APPL_ERROR)" + "\r\n" +
                             "INTERRUPT DECL A_Arc_Swi_Intr WHEN A_ARC_SWI==#ACTIVE DO A10 (#ARC_SEAM)" + "\r\n" +
                             "INTERRUPT DECL A_Arc_HPU_Intr WHEN A_FLY_ARC==TRUE DO A10 (#HPU_ARC)" + "\r\n" +
                             "INTERRUPT  ON A_Arc_HPU_Intr" + "\r\n" +
                             "A10_INI ( )" + "\r\n" +
                             "ENDIF" + "\r\n" +
                             ";ENDFOLD (ARCTECH_ANALOG INI)" + "\r\n" +
                             ";FOLD TOUCHSENS INI" + "\r\n" +
                             "IF H70_OPTION THEN" + "\r\n" +
                             "  IF (TOUCH_I[TOUCH_ACTIVE].IN_NR<>0) THEN" + "\r\n" +
                             "    IF (TOUCH_I[TOUCH_ACTIVE].IN_NR>0) THEN" + "\r\n" +
                             "      INTERRUPT DECL 15 WHEN $MEAS_PULSE[TOUCH_I[TOUCH_ACTIVE].IN_NR] DO H70 (6,CD0 )" + "\r\n" +
                             "    ELSE" + "\r\n" +
                             "      INTERRUPT DECL 15 WHEN $MEAS_PULSE[-TOUCH_I[TOUCH_ACTIVE].IN_NR]==FALSE DO H70 (6,CD0 )" + "\r\n" +
                             "    ENDIF" + "\r\n" +
                             "    INTERRUPT DECL 16 WHEN $ZERO_MOVE DO H70 (7,CD0 )" + "\r\n" +
                             "    INTERRUPT DECL 17 WHEN $TECHPAR_C[FG_TOUCH,8]>0.5 DO H70 (8,CD0)" + "\r\n" +
                             "  ENDIF" + "\r\n" +
                             "  H70 (1,CD0 )" + "\r\n" +
                             "ENDIF" + "\r\n" +
                             ";ENDFOLD (TOUCHSENS INI)" + "\r\n" +
                             ";ENDFOLD (INI)";
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = ";FOLD INI;%{{PE}}" + "\r\n" +
                             "  ;FOLD BASISTECH INI" + "\r\n" +
                             "    GLOBAL INTERRUPT DECL 3 WHEN $STOPMESS==TRUE DO IR_STOPM ( )" + "\r\n" +
                             "    INTERRUPT ON 3 " + "\r\n" +
                             "    BAS (#INITMOV,0 )" + "\r\n" +
                             "  ;ENDFOLD (BASISTECH INI)" + "\r\n" +
                             "  ;FOLD USER INI" + "\r\n" +
                             "    ;Make your modifications here" + "\r\n" +
                             " " + "\r\n" +
                             "  ;ENDFOLD (USER INI)" + "\r\n" +
                             ";ENDFOLD (INI)";
                    break;
            }

            return output;
        }

        /// <inheritdoc/>
        internal override string FormatFileProperties()
        {
            string output = string.Empty;
            this.InitializeKrcVersion();

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = "&ACCESS RVP  " + "\r\n" +
                             "&REL 6" + "\r\n" +
                             "&COMMENT GENERATED BY ROBOTMASTER" + "\r\n" +
                             "&PARAM TEMPLATE = C:\\KRC\\TP\\ArcTechAnalog\\Template\\VORGABE_Arc" + "\r\n" +
                             "&PARAM EDITMASK = *" + "\r\n";
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = "&ACCESS RVP  " + "\r\n" +
                             "&REL 6" + "\r\n" +
                             "&COMMENT GENERATED BY ROBOTMASTER" + "\r\n" +
                             "&PARAM EDITMASK = *" + "\r\n" +
                             "&PARAM TEMPLATE = C:\\KRC\\Roboter\\Template\\vorgabe" + "\r\n";
                    break;
            }

            return output;
        }

        /// <inheritdoc/>
        internal override string FormatDatFileTemplate()
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = ";FOLD EXTERNAL DECLARATIONS" + "\r\n" +
                             ";FOLD BASISTECH EXT" + "\r\n" +
                             "EXT BAS (BAS_COMMAND  :IN,REAL  :IN)" + "\r\n" +
                             "DECL INT SUCCESS" + "\r\n" +
                             ";ENDFOLD (BASISTECH EXT)" + "\r\n" +
                             string.Empty + "\r\n" +
                             ";FOLD TOUCHSENS EXT" + "\r\n" +
                             "EXT  H70(INT: IN, SRCH_TYP_2: OUT, E6POS: IN, SRCH_TYP_3: IN, SRCH_TYP_2: IN, SRCH_TYP_2: IN, SRCH_TYP_2: IN, SRCH_TYP_2: IN, SRCH_TYP_2: IN, INT: IN)" + "\r\n" +
                             "; ENDFOLD(TOUCHSENS EXT)" + "\r\n" +
                             string.Empty + "\r\n" +
                             ";FOLD USER EXT" + "\r\n" +
                             ";Make your modifications here" + "\r\n" +
                             ";ENDFOLD (USER EXT)" + "\r\n" +
                             ";ENDFOLD (EXTERNAL DECLARATIONS)" + "\r\n" +
                             "DECL SRCH_TYP_2 VCD1 ={ OPT 1,CAL_MODE FALSE, SRCH_OK TRUE,CAL_VAL 0.00,MES_VAL 0.00,OFFSET 0.00,TCH_RESULT_A 0.0,TCH_RESULT_B 0.0,TCH_RESULT_C 0.0,TCH_RESULT_D 0.0,TCH_RESULT_E 0.0,TCH_RESULT_F 0.0,B_X 0.000,B_Y 0.00,B_Z - 0.00,W_X 0.00,W_Y 0.00,W_Z 0.00,REF_X 0.00,REF_Y 0.00,REF_Z 0.00,MES_X 0.00,MES_Y 0.00,MES_Z 0.00}\r\n" +
                             "DECL SRCH_TYP_3 ZPA0 ={ MODE 1,SECMODE 0,APO 0,DIST 10.0,VEL 10.0,TOL 100.0,RET 1}\r\n" +
                             "\r\n";
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = ";FOLD EXTERNAL DECLARATIONS;%{{PE}}%MKUKATPBASIS,%CEXT,%VCOMMON,%P" + "\r\n" +
                             ";FOLD BASISTECH EXT;%{{PE}}%MKUKATPBASIS,%CEXT,%VEXT,%P" + "\r\n" +
                             "EXT  BAS (BAS_COMMAND  :IN,REAL  :IN )" + "\r\n" +
                             "DECL INT SUCCESS" + "\r\n" +
                             ";ENDFOLD (BASISTECH EXT)" + "\r\n" +
                             string.Empty + "\r\n" +
                             ";ENDFOLD (BASISTECH EXT)" + "\r\n" +
                             ";FOLD USER EXT;%{{E}}%MKUKATPUSER,%CEXT,%VEXT,%P" + "\r\n" +
                             ";Make your modifications here" + "\r\n" +
                             string.Empty + "\r\n" +
                             ";ENDFOLD (USER EXT)" + "\r\n" +
                             ";ENDFOLD (EXTERNAL DECLARATIONS)" + "\r\n" +
                             "DECL TSg_SearchProperty_T ZSP1={MoveType #MoveWithLin,TouchType #Single,DynamicStep #Slow,SensorNum 1,SearchDepth_Tol 50.0000}\r\n" +
                             "\r\n";
                    break;
            }

            return output;
        }

        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            base.RunBeforeOperationOutput(operationNode);


            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            this.LcpdatOutput = false;

            //First back to Children[0]
            this.CurrentPostFile = this.CurrentPostFile.Parent.Children[0];
            this.Moves.WriteLine(string.Empty);
            this.Moves.WriteLine(";-----" + OperationManager.GetOperationName(operationNode) + "-----");

            
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            if (applicationType == ApplicationType.Welding||applicationType == ApplicationType.TouchSensing)
            {

                this.Moves.WriteLine(OperationManager.GetOperationName(operationNode)+"()");

                var newPostsrcFile = new PostFile();
                this.CurrentPostFile.Parent.AddChild(newPostsrcFile);
                this.CurrentPostFile = newPostsrcFile;
                this.CurrentPostFile.FileName = OperationManager.GetOperationName(operationNode);
                this.CurrentPostFile.FileExtension = ".src";
                this.CurrentPostFile.FileSection.SubFileSections.Add(new FileSection());
                this.CurrentPostFile.FileSection.SubFileSections.Add(new FileSection());
                this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
                this.DatOut.WriteLine(
                    "DECL PDAT PPDAT1={ACC 100.000,APO_DIST 100.000,APO_MODE #CPTP,GEAR_JERK 50.0000}");


                if (applicationType == ApplicationType.Welding)
                {
                    var newMultiPostsrcFile = new PostFile();
                    var newMultiPostdatFile = new PostFile();
                    this.CurrentPostFile.AddChild(newMultiPostsrcFile);
                    this.CurrentPostFile.AddChild(newMultiPostdatFile);
                    this.CurrentPostFile.Children[1].FileName = OperationManager.GetOperationName(operationNode) + "sub";
                    this.CurrentPostFile.Children[1].FileExtension = ".src";
                    this.CurrentPostFile.Children[2].FileName = OperationManager.GetOperationName(operationNode) + "sub";
                    this.CurrentPostFile.Children[2].FileExtension = ".dat";
                    this.CurrentPostFile.Children[2].FileSection.Footer = "\r\nENDDAT";

                    this.MultiPassFixPostFile(this.CurrentPostFile.Children[1]);
                    this.MultiPassFixdatPostFile(this.CurrentPostFile.Children[2]);

                    this.MPassSegmentCounter = 0;
                    this.MPassPointCounter = 0;
                    this.MPassNegativ = "";
                    this.MPassPositiv = "";
                    this.E6mlDat = ";FOLD e6ML_SP0_ACT[128] Trigger Points of Root-Layer local" + "\r\n" +
                                   "E6POS e6ML_SP0_ACT[128]" + "\r\n";
                    this.FmlDat = ";FOLD fML_SP0_TTS[128] Trigger TTS-Points of Root-Layer local" + "\r\n" +
                                  "FRAME fML_SP0_TTS[128]" + "\r\n";
                    this.MPassWeldPar = "";
                }


            }
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.OutputEvent(beforePointEvent);
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.OutputEvent(afterPointEvent);
        }

        /// <summary>
        ///     Output event.
        /// </summary>
        /// <param name="pointEvent">The event to output.</param>
        internal void OutputEvent(Event pointEvent)
        {
            if (pointEvent is ArcData myArcDataEvent)
            {
                this.DatOut.WriteLine(this.FormatArcDataEvent(myArcDataEvent));
            }
            else if (pointEvent is ArcOn myArcOnEvent)
            {
                this.WeldDataNumber++;
                this.WeldSeamNumber++;

                this.Moves.WriteLine(myArcOnEvent);
                this.DatOut.WriteLine(this.FormatArcOnEvent(myArcOnEvent));

                this.CurrentProcessStatus = ProcessStatus.ArcOn;
            }
            else if (pointEvent is ArcOff myArcOffEvent)
            {
                this.Moves.WriteLine(myArcOffEvent);

                this.CurrentProcessStatus = ProcessStatus.ArcOff;
            }
            else if (pointEvent is WeldData myWeldDataEvent)
            {
                this.WeldDataNumber++;

                this.DatOut.WriteLine(this.FormatWeldDataEvent(myWeldDataEvent));
            }
            else if (pointEvent is Correction myCorrectionEvent)
            {
                this.Moves.WriteLine(myCorrectionEvent);

                if (myCorrectionEvent.SwitchedCorrection)
                {
                    this.Moves.WriteLine(this.FormatCorrectionOffEvent());
                }

                this.Moves.WriteLine(this.FormatCorrectionEvent(myCorrectionEvent));
            }
            else if (pointEvent is CorrectionOff)
            {
                this.Moves.WriteLine(this.FormatCorrectionOffEvent());
            }
            else if (pointEvent is TouchSearch myTouchSearch)
            {
                this.Moves.WriteLine(myTouchSearch);
                this.CachedTouchSearchList.Add(myTouchSearch);

                this.CurrentProcessStatus = ProcessStatus.Search;
            }
        }

        /// <summary>
        ///     Formats the Arc Data Event.
        /// </summary>
        /// <param name="arcDataEvent">Arc Data event.</param>
        /// <returns>Formatted Arc Data Event.</returns>
        internal string FormatArcDataEvent(ArcData arcDataEvent)
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = string.Format(
                        "DECL A_STRT_T A10BS={{GAS_PRE_T {0},START_T {1},ANA1 {2},ANA2 {3},ANA3 0.0,ANA4 0.0,ANA5 0.0,ANA6 0.0,ANA7 0.0,ANA8 0.0}}" + "\r\n" +
                        "DECL A_END_T A10EE={{END_TI 2.0,BURNBACK_T 0.0,GAS_POST_T 1.5,ANA1_E {4},ANA2_E {5},ANA3_E 0.0,ANA4_E 0.0,ANA5_E 0.0,ANA6_E 0.0,ANA7_E 0.0,ANA8_E 0.0}}" + "\r\n" +
                        "DECL A_SENSOR_T A10TT={{BIAS 0.0,H_GAIN 30.0,V_GAIN 0.0,ACCUM_OFS #IDLE,STS #ACTIVE}}" + "\r\n" +
                        "DECL A_SENSOR_T A10TT1={{BIAS 0.0,H_GAIN 20.0,V_GAIN 0.0,ACCUM_OFS #IDLE,STS #ACTIVE}}",
                        arcDataEvent.PreGasTime,
                        arcDataEvent.StartTime,
                        arcDataEvent.StartWeldVoltage,
                        arcDataEvent.StartWireVelocity,
                        arcDataEvent.EndWeldVoltage,
                        arcDataEvent.EndWireVelocity);
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = string.Format(
                        "DECL ATBg_Start_T AS_WDAT1={{JobModeId 1730196410,ParamSetId -538804060,StartTime {1},PreFlowTime {0},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,PurgeTime 0.0}}" + "\r\n" +
                        "DECL ATBg_Crater_T AC_WDAT1={{JobModeId 1730196410,ParamSetId 937237684,CraterTime 1.50000,PostflowTime 0.500000,Channel1 {4},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,BurnBackTime 0.0}}",
                        arcDataEvent.PreGasTime,
                        arcDataEvent.StartTime,
                        arcDataEvent.StartWeldVoltage,
                        arcDataEvent.StartWireVelocity,
                        arcDataEvent.EndWeldVoltage,
                        arcDataEvent.EndWireVelocity);
                    break;
            }

            return output;
        }

        /// <summary>
        ///     Formats the Arc On Event.
        /// </summary>
        /// <param name="arcOnEvent">Arc On event.</param>
        /// <returns>Formatted Arc On Event.</returns>
        internal string FormatArcOnEvent(ArcOn arcOnEvent)
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = string.Format(
                        "DECL A_WELD_T A10WW{0}={{VEL {1},ANA1 {2},ANA2 {3},ANA3 0.0,ANA4 0.0,ANA5 0.0,ANA6 0.0,ANA7 0.0,ANA8 5.0,WEAVFIG_MECH {4},WEAVLEN_MECH {5},WEAVAMP_MECH {6},WEAVANG_MECH {7},WEAVFIG_THER 0,ANA1_THERM 0.0,ANA2_THERM 0.0,WEAVLEN_THER 4.0,BURNBACK_T 0.0,ANA1_BB 24.0,ANA2_BB 400.0,ANA3_BB 0.0,ANA4_BB 0.0,ANA5_BB 0.0,ANA6_BB 0.0,ANA7_BB 0.0,ANA8_BB 0.0}}",
                        this.WeldDataNumber,
                        arcOnEvent.WeldVelocity,
                        arcOnEvent.WeldVoltage,
                        arcOnEvent.WireVelocity,
                        Convert.ToInt32(arcOnEvent.WeaveStyle),
                        arcOnEvent.WeaveLength,
                        arcOnEvent.WeaveAmplitude,
                        arcOnEvent.WeavePlane);
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = string.Format(
                        "DECL ATBg_Weld_T AW_WDAT{0}={{JobModeId 1730196410,ParamSetId 2058384531,Velocity {1},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0}}" + "\r\n" +
                        "DECL WTCg_WeaveDefinition_T WV_WDAT{0}={{Pattern #None,Length {5},Amplitude {6},Angle {7}}}" + "\r\n" +
                        "DECL ArcSenseParas asWeaveParas{0}={{IsArcSenseEnabled FALSE,ControlEnabled TRUE,KeepOffset TRUE,TeachNew FALSE,FindSeamMiddle FALSE,TrackingDelay 0.0,ControlSensibility 50.0000,MaxDeviation 5.00000,ControlSensibilityHeight 50.0000,Bias 0.0,PartGeometry #ANGLE90}}" + "\r\n",
                        this.WeldDataNumber,
                        arcOnEvent.WeldVelocity,
                        arcOnEvent.WeldVoltage,
                        arcOnEvent.WireVelocity,
                        Convert.ToInt32(arcOnEvent.WeaveStyle),
                        arcOnEvent.WeaveLength,
                        arcOnEvent.WeaveAmplitude,
                        arcOnEvent.WeavePlane);
                    if (MultiPassOutput == true)
                    {
                        this.MPassWeldPar = this.MPassWeldPar +string.Format(
                            "DECL ATBg_Start_T AS_PAR={{JobModeId 1730196410,ParamSetId -538804060,StartTime 0.100000,PreFlowTime 0.500000,Channel1 26.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,PurgeTime 0.0}}" + "\r\n" +
                            "DECL ATBg_Weld_T AW_PAR={{JobModeId 1730196410,ParamSetId 2058384531,Velocity {1},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0}}" +"\r\n" +
                            "DECL WTCg_WeaveDefinition_T WV_PAR={{Pattern #None,Length {5},Amplitude {6},Angle {7}}}" + "\r\n" +
                            "DECL ATBg_Crater_T AC_PAR={{JobModeId 1730196410,ParamSetId 937237684,CraterTime 1.50000,PostflowTime 0.500000,Channel1 20.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,BurnBackTime 0.0}}" + "\r\n" +
                            "DECL ATAg_SlopeOptDef_T SL_PAR={{Time 0.0,Distance 0.0,ToPosNext FALSE}}" + "\r\n" +
                            "" + "\r\n" +
                            "DECL ATBg_Start_T AS_MLDAT={{JobModeId 1730196410,ParamSetId -538804060,StartTime 0.100000,PreFlowTime 0.500000,Channel1 26.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,PurgeTime 0.0}}" + "\r\n" +
                            "DECL ATBg_Weld_T AW_MLDAT={{JobModeId 1730196410,ParamSetId 2058384531,Velocity {1},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0}}" + "\r\n" +
                            "DECL WTCg_WeaveDefinition_T WV_MLDAT={{Pattern #None,Length {5},Amplitude {6},Angle {7}}}" + "\r\n" +
                            "DECL ATBg_Crater_T AC_MLDAT={{JobModeId 1730196410,ParamSetId 937237684,CraterTime 1.50000,PostflowTime 0.500000,Channel1 20.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,BurnBackTime 0.0}}" + "\r\n" +
                            "DECL ATAg_SlopeOptDef_T SL_MLDAT={{Time 0.0,Distance 0.0,ToPosNext FALSE}}" + "\r\n" +
                            "" + "\r\n" +
                            "DECL ATBg_Start_T AS_WDAT1={{JobModeId 1730196410,ParamSetId -538804060,StartTime 0.100000,PreFlowTime 0.500000,Channel1 26.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,PurgeTime 0.0}}" + "\r\n"+
                            "DECL ATBg_Crater_T AC_WDAT1={{JobModeId 1730196410,ParamSetId 937237684,CraterTime 1.50000,PostflowTime 0.100000,Channel1 20.0000,Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0,BurnBackTime 0.0}}" + "\r\n" +
                            "DECL ATBg_Weld_T AW_WDAT1={{JobModeId 1730196410,ParamSetId 2058384531,Velocity {1},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0}}" + "\r\n" +
                            "DECL WTCg_WeaveDefinition_T WV_WDAT1={{Pattern #None,Length {5},Amplitude {6},Angle {7}}}" + "\r\n" +
                            "DECL ATAg_SlopeOptDef_T SL_WDAT1={{Time 0.0,Distance 0.0,ToPosNext FALSE}}" + "\r\n" +
                            "DECL ArcSenseParas asWeaveParas1={{IsArcSenseEnabled FALSE,ControlEnabled TRUE,KeepOffset TRUE,TeachNew FALSE,FindSeamMiddle FALSE,TrackingDelay 0.0,ControlSensibility 50.0000,MaxDeviation 5.00000,ControlSensibilityHeight 50.0000,Bias 0.0,PartGeometry #ANGLE90}}" + "\r\n",
                            this.WeldDataNumber,
                            arcOnEvent.WeldVelocity,
                            arcOnEvent.WeldVoltage,
                            arcOnEvent.WireVelocity,
                            Convert.ToInt32(arcOnEvent.WeaveStyle),
                            arcOnEvent.WeaveLength,
                            arcOnEvent.WeaveAmplitude,
                            arcOnEvent.WeavePlane);
                    }                   
                    break;
            }

            return output;
        }

        /// <summary>
        ///     Formats the Weld Data Event.
        /// </summary>
        /// <param name="weldDataEvent">Weld Data event.</param>
        /// <returns>Formatted Weld Data Event.</returns>
        internal string FormatWeldDataEvent(WeldData weldDataEvent)
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = string.Format(
                        "DECL A_WELD_T A10WW{0}={{VEL {1},ANA1 {2},ANA2 {3},ANA3 0.0,ANA4 0.0,ANA5 0.0,ANA6 0.0,ANA7 0.0,ANA8 5.0,WEAVFIG_MECH {4},WEAVLEN_MECH {5},WEAVAMP_MECH {6},WEAVANG_MECH {7},WEAVFIG_THER 0,ANA1_THERM 0.0,ANA2_THERM 0.0,WEAVLEN_THER 4.0,BURNBACK_T 0.0,ANA1_BB 24.0,ANA2_BB 400.0,ANA3_BB 0.0,ANA4_BB 0.0,ANA5_BB 0.0,ANA6_BB 0.0,ANA7_BB 0.0,ANA8_BB 0.0}}",
                        this.WeldDataNumber,
                        weldDataEvent.WeldVelocity,
                        weldDataEvent.WeldVoltage,
                        weldDataEvent.WireVelocity,
                        Convert.ToInt32(weldDataEvent.WeaveStyle),
                        weldDataEvent.WeaveLength,
                        weldDataEvent.WeaveAmplitude,
                        weldDataEvent.WeavePlane);
                    break;
                case KukaKrcVersion.Krc4V83:
                    output = string.Format(
                        "DECL ATBg_Weld_T AW_WDAT{0}={{JobModeId 1730196410,ParamSetId 2058384531,Velocity {1},Channel1 {2},Channel2 0.0,Channel3 0.0,Channel4 0.0,Channel5 0.0,Channel6 0.0,Channel7 0.0,Channel8 0.0}}" + "\r\n" +
                        "DECL WTCg_WeaveDefinition_T WV_WDAT{0}={{Pattern #None,Length {5},Amplitude {6},Angle {7}}}" + "\r\n" +
                        "DECL ArcSenseParas asWeaveParas{0}={{IsArcSenseEnabled FALSE,ControlEnabled TRUE,KeepOffset TRUE,TeachNew FALSE,FindSeamMiddle FALSE,TrackingDelay 0.0,ControlSensibility 50.0000,MaxDeviation 5.00000,ControlSensibilityHeight 50.0000,Bias 0.0,PartGeometry #ANGLE90}}" + "\r\n",
                        this.WeldDataNumber,
                        weldDataEvent.WeldVelocity,
                        weldDataEvent.WeldVoltage,
                        weldDataEvent.WireVelocity,
                        Convert.ToInt32(weldDataEvent.WeaveStyle),
                        weldDataEvent.WeaveLength,
                        weldDataEvent.WeaveAmplitude,
                        weldDataEvent.WeavePlane);
                    break;
            }

            return output;
        }

        /// <summary>
        ///     Formats the Correction Event.
        /// </summary>
        /// <param name="correctionEvent">Correction event.</param>
        /// <returns>Formatted Correction Event.</returns>
        internal string FormatCorrectionEvent(Correction correctionEvent)
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    switch (correctionEvent.CorrectionType)
                    {
                        case KukaWeldingTdCorrectionType.Undefined:
                            break;
                        case KukaWeldingTdCorrectionType.Correction1D:
                            output = string.Format(
                                ";FOLD CORR 1D CD{0} CONT;%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%V1DIM,%P 2:CD{0}, 3:1\r\n" +
                                "H70(10,VCD{0},POS_DUMMY,PA0,CD0,CD0,CD0,CD0,CD0,1)\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1);
                            break;
                        case KukaWeldingTdCorrectionType.Correction2D:
                            output = string.Format(
                                ";FOLD CORR 2D CD{0} CD{1} CONT;%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%V2DIM,%P 2:CD{0}, 3:CD{1}, 4:1\r\n" +
                                "H70(10,VCD{0},POS_DUMMY,PA0,CD0,CD0,VCD{1},CD0,CD0,1)\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1,
                                correctionEvent.CorrectionVariable2);
                            break;
                        case KukaWeldingTdCorrectionType.Correction3D:
                            output = string.Format(
                                ";FOLD CORR 3D CD{0} CD{1} CD{2} CONT;%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%V2DIM,%P 2:CD{0}, 3:CD{1}, 4:CD{2}, 5:1\r\n" +
                                "H70(10,VCD{0},POS_DUMMY,PA0,CD0,CD0,VCD{1},CD0,VCD{2},1)\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1,
                                correctionEvent.CorrectionVariable2,
                                correctionEvent.CorrectionVariable3);
                            break;
                        case KukaWeldingTdCorrectionType.Correction6D:
                            output = string.Format(
                                ";FOLD CORR 3D CD{{ {0}, {1}, {2} }},{{ {3}, {4}}},{{ {5} }} CONT;%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%V2DIM,%P 2:CD{0}, 3:CD{1}, 4:CD{2}, 5:CD{3}, 6:CD{4}, 7:CD{5}, 8:1\r\n" +
                                "H70(10,VCD{0},POS_DUMMY,PA0,VCD{1},VCD{2},VCD{3},VCD{4},VCD{5},1)\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1,
                                correctionEvent.CorrectionVariable2,
                                correctionEvent.CorrectionVariable3,
                                correctionEvent.CorrectionVariable4,
                                correctionEvent.CorrectionVariable5,
                                correctionEvent.CorrectionVariable6);

                            break;
                    }

                    break;
                case KukaKrcVersion.Krc4V83:
                    switch (correctionEvent.CorrectionType)
                    {
                        case KukaWeldingTdCorrectionType.Undefined:
                            break;
                        case KukaWeldingTdCorrectionType.Correction1D:
                            output = string.Format(
                                ";FOLD TouchSense Corr 1D CD{0} ;%{{E}}\r\n" +
                                ";FOLD Parameters Corr 1D ;%{{h}}\r\n" +
                                ";Params IlfProvider=KukaRoboter.TouchSense.Correction1D;Correction1D=CD{0}\r\n" +
                                ";ENDFOLD\r\n" +
                                "TS_BC6DCalc(VCD{0}, TSg_CD0, TSg_CD0, TSg_CD0, TSg_CD0, TSg_CD0)\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1);
                            break;
                        case KukaWeldingTdCorrectionType.Correction2D:
                            output = string.Format(
                               ";FOLD TouchSense Corr 2D CD{0} CD{1} ;%{{E}}\r\n" +
                                ";FOLD Parameters Corr 2D ;%{{h}}\r\n" +
                                ";Params IlfProvider=KukaRoboter.TouchSense.Correction2D;Correction1D=CD{0};Correction2D=CD{1}\r\n" +
                                ";ENDFOLD\r\n" +
                                "TS_BC6DCalc(VCD{0}, TSg_CD0, TSg_CD0, VCD{1}, TSg_CD0, TSg_CD0)\r\n" +
                                ";ENDFOLD",
                               correctionEvent.CorrectionVariable1,
                               correctionEvent.CorrectionVariable2);
                            break;
                        case KukaWeldingTdCorrectionType.Correction3D:
                            output = string.Format(
                                ";FOLD TouchSense Corr 3D CD{0} CD{1} CD{2} ;%{{E}}\r\n" +
                                ";FOLD Parameters Corr 3D ;%{{h}}\r\n" +
                                ";Params IlfProvider=KukaRoboter.TouchSense.Correction2D;Correction1D=CD{0};Correction2D=CD{1};Correction3D=CD{2}\r\n" +
                                ";ENDFOLD\r\n" +
                                "TS_BC6DCalc(VCD{0}, TSg_CD0, TSg_CD0, VCD{1}, TSg_CD0, VCD{2})\r\n" +
                                ";ENDFOLD",
                                correctionEvent.CorrectionVariable1,
                                correctionEvent.CorrectionVariable2,
                                correctionEvent.CorrectionVariable3);
                            break;
                        case KukaWeldingTdCorrectionType.Correction6D:
                            output = string.Format(
                                "; Need to be added\r\n",
                                correctionEvent.CorrectionVariable1,
                                correctionEvent.CorrectionVariable2,
                                correctionEvent.CorrectionVariable3,
                                correctionEvent.CorrectionVariable4,
                                correctionEvent.CorrectionVariable5,
                                correctionEvent.CorrectionVariable6);

                            break;
                    }

                    break;
            }

            return output;
        }

        /// <summary>
        ///     Formats the Correction Off Event.
        /// </summary>
        /// <returns>Formatted Correction Off Event.</returns>
        internal string FormatCorrectionOffEvent()
        {
            string output = string.Empty;

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    output = ";FOLD CORR OFF CONT;%{PE}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%VOFF,%P 2:1\r\n" +
                             "H70(10,CD0,POS_DUMMY,PA0,CD0,CD0,CD0,CD0,CD0,1)\r\n" +
                             ";ENDFOLD";

                    break;
                case KukaKrcVersion.Krc4V83:
                    output = ";FOLD TouchSense Corr Off ;%{E}\r\n" +
                             ";FOLD Parameters Corr Off ;%{h}\r\n" +
                             ";Params IlfProvider=KukaRoboter.TouchSense.CorrectionOFF\r\n" +
                             ";ENDFOLD\r\n" +
                             "TS_CorrOff()\r\n" +
                             ";ENDFOLD";
                    break;
            }

            return output;
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.DatOut.WriteLine(
                "DECL E6POS XP{0}={{{1}{2}}}\r\n" +
                "DECL FDAT FP{0}={{TOOL_NO {3},BASE_NO {4},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}",
                this.PointNumber,
                this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number);

            string currentLdat = this.FormatLcpdat(operationNode, point);

            // for Multi sub file
            if (this.LcpdatOutput == false)
            {
                this.DatOut.WriteLine(this.CachedLdat);
                this.LcpdatOutput = true;
            }

            

            if (currentLdat != this.CachedLdat)
            {
                this.LdatNumber++;
                this.CachedLdat = this.FormatLcpdat(operationNode, point); // We need to update the CachedLdat after the LdatNumber increment
                this.DatOut.WriteLine(this.CachedLdat);
            }

            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    if (this.CurrentProcessStatus == ProcessStatus.Search)
                    {
                        if (this.CachedTouchSearchList.Count > 0 && this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 1].TouchCount == 2)
                        {
                            this.Moves.WriteLine(
                                ";FOLD CORR 1D CD{0} CONT;%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%V1DIM,%P 2:CD{0}, 3:1\r\n" +
                                "H70(10,VCD{0},POS_DUMMY,PA0,CD0,CD0,CD0,CD0,CD0,1)\r\n" +
                                ";ENDFOLD",
                                this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 2].TouchSearchVariable);
                        }

                        this.Moves.WriteLine(
                            ";FOLD LIN P{0} Vel={1} m/s CPDAT{2} SEARCH VIA P{3} CD{4} PA0 Tool[{5}] Base[{6}];%{{PE}}%R 1.1.5,%MKUKATPTOUCHSENSE,%CSEARCH,%VNORMAL,%P 2:P{0}, 4:{1}, 6:CPDAT{2}, 8:P{3}, 9:CD{4}, 10:PA0\r\n" +
                            "$BWDSTART = FALSE\r\n" +
                            "H70(2,CD0,POS_DUMMY)\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{1})\r\n" +
                            "LIN XP{0} C_DIS\r\n" +
                            "H70(11,CD0,XP{3})\r\n" +
                            "H70(12,CD0,XP{0})\r\n" +
                            "H70(13,VCD{4},XP{3})\r\n" +
                            "H70(3,VCD{4},XP{0},ZPA0)\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.PointNumber + 1,
                            this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 1].TouchSearchVariable,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number);

                        this.Moves.WriteLine(
                            ";FOLD CORR OFF CONT;%{PE}%R 1.1.5,%MKUKATPTOUCHSENSE,%CCORR,%VOFF,%P 2:1\r\n" +
                            "H70(10,CD0,POS_DUMMY,PA0,CD0,CD0,CD0,CD0,CD0,1)\r\n" +
                            ";ENDFOLD");

                        this.CurrentProcessStatus = ProcessStatus.AfterSearch;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.AfterSearch)
                    {
                        // No Point Output
                        this.CurrentProcessStatus = ProcessStatus.None;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcOn)
                    {
                        this.Moves.WriteLine(
                            ";FOLD LIN P{0} Vel={1} m/s CPDAT{2} ARC_ON PS S Seam{3} Tool[{4}] Base[{5}];%{{PE}}%R 1.3.4,%MKUKATPARC,%CARC_ON,%VLIN,%P 1:LIN, 2:P{0}, 3:C_DIS, 5:{1}, 7:CPDAT{2}, 9:1, 10:S, 12:Seam{3}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{1})\r\n" +
                            "A10(#PRE_ARC_ON,A10BS,A_W_PARA_ACT,A_E_PARA_ACT,1)\r\n" +
                            "LIN XP{0}\r\n" +
                            "A10(#ARC_STRT)\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.WeldSeamNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number);

                        this.CurrentProcessStatus = ProcessStatus.ArcSwitch;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcSwitch)
                    {
                        this.Moves.WriteLine(
                            ";FOLD LIN P{0} CPDAT{2} ARC PS W{3} TRACK T1 Tool[{4}] Base[{5}];%{{PE}}%R 1.1.5,%MKUKATPARC,%CTRACK_SWI,%VLIN,%P 1:LIN, 2:P{0}, 3:, 5:{1}, 7:CPDAT{2}, 9:1, 11:W{3}, 12:2, 13:T1\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,LDEFAULT.VEL)\r\n" +
                            "PS_TEMP_SET=A10TT1\r\n" +
                            "A10(#PRE_ARC_SWI,A_S_PARA_ACT,A10WW{3},A_E_PARA_ACT,1)\r\n" +
                            "LIN XP{0} {6}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.WeldDataNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcOff)
                    {
                        this.Moves.WriteLine(
                            ";FOLD LIN P{0} CPDAT{2} ARC_OFF PS W{3} E TRACK T Seam{6} Tool[{4}] Base[{5}];%{{PE}}%R 1.1.5,%MKUKATPARC,%CTRACK_OFF,%VLIN,%P 1:LIN, 2:P{0}, 3:, 5:{1}, 7:CPDAT{2}, 9:1, 10:W{3}, 11:E, 12:2, 13:T, 14:Seam{6}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,LDEFAULT.VEL)\r\n" +
                            "PS_TEMP_SET=A10TT\r\n" +
                            "A10(#PRE_ARC_OFF,A_S_PARA_ACT,A10WW{3},A10EE,1)\r\n" +
                            "LIN XP{0}\r\n" +
                            "A10(#ARC_OFF)\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.WeldDataNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            this.WeldSeamNumber);

                        this.CurrentProcessStatus = ProcessStatus.None;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.None)
                    {
                        this.Moves.WriteLine(
                            ";FOLD LIN P{0}{1} Vel={2} m/s CPDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P{0}, 3:{6}, 5:{2}, 7:CPDAT{3}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{3}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{2})\r\n" +
                            "LIN XP{0} {6}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                    }

                    break;
                case KukaKrcVersion.Krc4V83:
                    if (this.CurrentProcessStatus == ProcessStatus.Search)
                    {
                        if (this.CachedTouchSearchList.Count > 0 && this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 1].TouchCount == 2)
                        {
                            this.Moves.WriteLine(
                                ";FOLD TouchSense Corr 1D CD{0} ;%{{E}}\r\n" +
                                ";FOLD Parameters Corr 1D ;%{{h}}\r\n" +
                                ";Params IlfProvider=KukaRoboter.TouchSense.Correction1D;Correction1D=CD{0}\r\n" +
                                ";ENDFOLD\r\n" +
                                "TS_BC6DCalc(VCD{0}, TSg_CD0, TSg_CD0, TSg_CD0, TSg_CD0, TSg_CD0)\r\n" +
                                ";ENDFOLD",
                                this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 2].TouchSearchVariable);
                        }

                        this.Moves.WriteLine(
                            ";FOLD LIN P{0} Vel = {1} m / s CPDAT{2} TouchSense SEARCH VIA P{3} CD{4} SP1 Tool[{5}] Base[{6}];%{{ PE}}\r\n" +
                            ";FOLD Parameters ;%{{h}}\r\n" +
                            ";Params IlfProvider=kukaroboter.touchsense.searchlin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT{2}; Kuka.VelocityPath=2; Kuka.MovementParameterFieldEnabled=True; MoveType=LIN\r\n" +
                            ";FOLD Parameters TouchSense ;%{{h}}\r\n" +
                            ";Params TouchSense.SearchViaPoint=P{3};TouchSense.ReferenceMove=CD{4};TouchSense.SearchParam=SP1\r\n" +
                            ";ENDFOLD\r\n" +
                            ";ENDFOLD\r\n" +
                            "$BWDSTART = FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{1})\r\n" +
                            "LIN XP{0} C_DIS\r\n" +
                            "TS_InitMovement($POS_ACT, XP{3}, ZSP1)\r\n" +
                            "TS_ManMoveToVia(XP{3})\r\n" +
                            "TS_Search(XP{3}, ZSP1, VCD{4})\r\n" +
                            "TS_SetCorrData(ZSP1, VCD{4})\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.PointNumber + 1,
                            this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 1].TouchSearchVariable,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number);

                        this.Moves.WriteLine(
                             ";FOLD TouchSense Corr Off ;%{E}\r\n" +
                             ";FOLD Parameters Corr Off ;%{h}\r\n" +
                             ";Params IlfProvider=KukaRoboter.TouchSense.CorrectionOFF\r\n" +
                             ";ENDFOLD\r\n" +
                             "TS_CorrOff()\r\n" +
                             ";ENDFOLD");

                        this.DatOut.WriteLine(
                            "DECL TSg_PCollCD_T VCD{0}={{CD_IPO_MODE #NotDefined,Mastered FALSE, SDir_W {{X 0.0,Y 0.0,Z 0.0,A 0.0,B 0.0,C 0.0}},Ref_W {{X 0.0,Y 0.0,Z 0.0,A 0.0,B 0.0,C 0.0}},Meas_W {{X 0.0,Y 0.0,Z 0.0,A 0.0,B 0.0,C 0.0}},Diff_W {{X 0.0,Y 0.0,Z 0.0,A 0.0,B 0.0,C 0.0}},VecLenDiff_W 0.0,MEnum #CDCalcOK,MC 1}}\r\n",
                            this.CachedTouchSearchList[this.CachedTouchSearchList.Count - 1].TouchSearchVariable);

                        this.CurrentProcessStatus = ProcessStatus.AfterSearch;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.AfterSearch)
                    {
                        // No Point Output
                        this.CurrentProcessStatus = ProcessStatus.None;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcOn)
                    {
                        if (MultiPassOutput == true)
                        {
                            this.MPassSegmentCounter++;
                            this.MPassPointCounter++;

                            //add ppdat in Multi.dat file
                            this.DatOut.WriteLine(
                                "DECL LDAT LCPDAT1={VEL 0.001,ACC 100,APO_DIST 5,APO_FAC 50,AXIS_VEL 100,AXIS_ACC 100,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0,GEAR_JERK 50.0,EXAX_IGN 0}\r\n");

                            //this.DatOut.WriteLine(this.CachedLdat);



                            this.MultiPassSrcWriter.WriteLine(
                                ";FOLD ARCON WDAT1 LIN P{0} Vel={1} m/s CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arconlin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT1; Kuka.VelocityPath={1}; Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas1; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=WDAT1; ArcTech.SeamName=Root\r\n" +
                                ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                                ";Params IgnitionErrorStrategy=1; WeldErrorStrategy=1; SlopeDistance=0\r\n" +
                                ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "FDAT_ACT=FP1\r\n" +
                                "BAS(#CP_PARAMS,{1})\r\n" +
                                "TRIGGER WHEN DISTANCE = 1 DELAY = ATBg_PreDefinitionTime DO Arc_DefineStrikeParams(1, AS_WDAT1) PRIO = -1\r\n" +
                                "Arc_DefineCpPattern(#OffInAdvance, WV_WDAT1, TRUE)\r\n" +
                                "LIN XP{0}\r\n" +
                                "Arc_On(1, AS_WDAT1, ATBg_StartErrSetField[1], AW_WDAT1, WV_WDAT1, ATBg_WeldErrSetField[1], #StdArcOn, \" \")\r\n" +
                                "Arc_DefineCpPattern(#OnInAdvance, WV_WDAT1, TRUE)\r\n" +
                                ";ENDFOLD",
                                this.MPassPointCounter,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number);

                            this.MPassPositiv = this.MPassPositiv + string.Format(
                                ";FOLD ARCON MLDAT (LayerPos) LIN SP{0} Vel={1} m/s CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arconlin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT1; Kuka.VelocityPath={1}; Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas1; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=MLDAT; ArcTech.SeamName=LayerPos\r\n" +
                                ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                                ";Params IgnitionErrorStrategy=1; WeldErrorStrategy=1; SlopeDistance=0\r\n" +
                                ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "FDAT_ACT=FSP{0}\r\n" +
                                "BAS(#CP_PARAMS,{1})\r\n" +
                                "TRIGGER WHEN DISTANCE = 1 DELAY = ATBg_PreDefinitionTime DO Arc_DefineStrikeParams(1, AS_MLDAT) PRIO = -1\r\n" +
                                "Arc_DefineCpPattern(#OffInAdvance, WV_MLDAT, TRUE)\r\n" +
                                "LIN XSP{0}\r\n" +
                                "Arc_On(1, AS_MLDAT, ATBg_StartErrSetField[1], AW_MLDAT, WV_MLDAT, ATBg_WeldErrSetField[1], #StdArcOn, \" \")\r\n" +
                                "Arc_DefineCpPattern(#OnInAdvance, WV_MLDAT, TRUE)\r\n" +
                                ";ENDFOLD\r\n",
                                this.MPassPointCounter.ToString().PadLeft(3,'0'),
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number);

                            this.ArcOnPointTemp = this.PointNumber;

                            this.E6mlDat = this.E6mlDat +
                                string.Format("e6ML_SP0_ACT[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);
                            this.FmlDat = this.FmlDat +
                                string.Format("fML_SP0_TTS[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                            this.MPassWeldPar = this.MPassWeldPar +
                                   string.Format("DECL FDAT FDummy={{TOOL_NO {1},BASE_NO {2},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}" + "\r\n" +
                                                "DECL E6POS XDummy={{{3}{4}}}" + "\r\n" +
                                                "DECL E6POS XP{0}={{{3}{4}}}" + "\r\n" +
                                                "DECL LDAT LCPDAT1={{ACC 100.000,APO_DIST 100.000,APO_FAC 50.0000,AXIS_VEL 100.000,AXIS_ACC 100.000,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0000,GEAR_JERK 50.0000,EXAX_IGN 0}}" + "\r\n" +
                                                "DECL PDAT PPDAT1={{ACC 100.000,APO_DIST 100.000,APO_MODE #CPTP,GEAR_JERK 50.0000}}" + "\r\n",
                                                this.MPassPointCounter,
                                                OperationManager.GetTcpId(operationNode),
                                                OperationManager.GetUserFrame(operationNode).Number,
                                                this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                                this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                        }
                        else
                        {
                            this.Moves.WriteLine(
                            ";FOLD ARCON WDAT{3} LIN P{0} Vel={1} m/s CPDAT{2} Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                            ";FOLD Parameters ;%{{h}}\r\n" +
                            ";Params IlfProvider=kukaroboter.arctech.arconlin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT{2}; Kuka.VelocityPath={1}; Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas{3}; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=WDAT{3}; ArcTech.SeamName=\r\n" +
                            ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                            ";Params IgnitionErrorStrategy=1; WeldErrorStrategy=1; SlopeDistance=0\r\n" +
                            ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                            ";ENDFOLD\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{1})\r\n" +
                            "TRIGGER WHEN DISTANCE = 1 DELAY = ATBg_PreDefinitionTime DO Arc_DefineStrikeParams(1, AS_WDAT1) PRIO = -1\r\n" +
                            "Arc_DefineCpPattern(#OffInAdvance, WV_WDAT{3}, TRUE)\r\n" +
                            "LIN XP{0}\r\n" +
                            "Arc_On(1, AS_WDAT1, ATBg_StartErrSetField[1], AW_WDAT{3}, WV_WDAT{3}, ATBg_WeldErrSetField[1], #StdArcOn, \" \")\r\n" +
                            "Arc_DefineCpPattern(#OnInAdvance, WV_WDAT{3}, TRUE)\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.WeldDataNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number);
                        }
                        

                        this.CurrentProcessStatus = ProcessStatus.ArcSwitch;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcSwitch)
                    {
                        if (MultiPassOutput == true)
                        {
                            this.MPassPointCounter++;
                            this.MultiPassSrcWriter.WriteLine(
                                ";FOLD ARCSWI WDAT1 LIN P{0} CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arcswilin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=True; Kuka.MoveDataName=CPDAT1;  Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas1; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=WDAT1\r\n" +
                                ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                                ";Params SlopeDistance=0\r\n" +
                                ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "LDAT_ACT.APO_Dist = ATBg_APODistanceArcTech\r\n" +
                                "FDAT_ACT=FP{0}\r\n" +
                                "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                                "TRIGGER WHEN DISTANCE = 1 DELAY = 0 DO Arc_Swi(1, AW_WDAT1, WV_WDAT1, ATBg_WeldErrSetField[1]) PRIO = -1\r\n" +
                                "LIN XP{0} {6}\r\n" +
                                "ATB_Definition(AW_WDAT1)\r\n" +
                                "Arc_DefineCpPattern(#OnInAdvance, WV_WDAT1, TRUE)\r\n" +
                                ";ENDFOLD",
                                this.MPassPointCounter,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);

                            this.MPassPositiv = this.MPassPositiv + string.Format(
                                ";FOLD ARCSWI MLDAT  LIN SP{0} CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arcswilin; Kuka.PointName=SP{0}; Kuka.BlendingEnabled=True; Kuka.MoveDataName=CPDAT1;  Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas1; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=MLDAT\r\n" +
                                ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                                ";Params SlopeDistance=0\r\n" +
                                ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "LDAT_ACT.APO_Dist = ATBg_APODistanceArcTech\r\n" +
                                "FDAT_ACT=FSP{0}\r\n" +
                                "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                                "TRIGGER WHEN DISTANCE = 1 DELAY = 0 DO Arc_Swi(1, AW_MLDAT, WV_MLDAT, ATBg_WeldErrSetField[1]) PRIO = -1\r\n" +
                                "LIN XSP{0} {6}\r\n" +
                                "ATB_Definition(AW_MLDAT)\r\n" +
                                "Arc_DefineCpPattern(#OnInAdvance, WV_MLDAT, TRUE)\r\n" +
                                ";ENDFOLD\r\n",
                                this.MPassPointCounter.ToString().PadLeft(3, '0'),
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);


                            this.E6mlDat= this.E6mlDat +
                                string.Format("e6ML_SP0_ACT[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);
                            this.FmlDat = this.FmlDat +
                                string.Format("fML_SP0_TTS[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                            this.MPassWeldPar = this.MPassWeldPar +
                                                string.Format(
                                                              "DECL E6POS XP{0}={{{1}{2}}}" + "\r\n",
                                                    this.MPassPointCounter,
                                                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                        }
                        else
                        {
                        this.Moves.WriteLine(
                            ";FOLD ARCSWI WDAT{3} LIN P{0} CPDAT{2} Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                            ";FOLD Parameters ;%{{h}}\r\n" +
                            ";Params IlfProvider=kukaroboter.arctech.arcswilin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=True; Kuka.MoveDataName=CPDAT{2};  Kuka.MovementParameterFieldEnabled=True; ArcSenseParas=asWeaveParas{3}; ArcSensePS=CSQ[1]; ArcTech.WdatVarName=WDAT{3}\r\n" +
                            ";FOLD Parameters ArcTechAdvanced ;%{{h}}\r\n" +
                            ";Params SlopeDistance=0\r\n" +
                            ";ENDFOLD ArcTechAdvancedFoldEnd\r\n" +
                            ";ENDFOLD\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{2}\r\n" +
                            "LDAT_ACT.APO_Dist = ATBg_APODistanceArcTech\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                            "TRIGGER WHEN DISTANCE = 1 DELAY = 0 DO Arc_Swi(1, AW_WDAT{3}, WV_WDAT{3}, ATBg_WeldErrSetField[1]) PRIO = -1\r\n" +
                            "LIN XP{0} {6}\r\n" +
                            "ATB_Definition(AW_WDAT{3})\r\n" +
                            "Arc_DefineCpPattern(#OnInAdvance, WV_WDAT{3}, TRUE)\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            this.WeldDataNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                        }
                        
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.ArcOff)
                    {
                        if (MultiPassOutput == true)
                        {
                            this.MPassPointCounter++;
                            this.MultiPassSrcWriter.WriteLine(
                                ";FOLD ARCOFF WDAT1 LIN P{0} CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arcofflin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT1;  Kuka.MovementParameterFieldEnabled=True; ArcSense.Enabled=False; ArcTech.WdatVarName=WDAT1\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "FDAT_ACT=FP{0}\r\n" +
                                "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                                "LIN XP{0}\r\n" +
                                "Arc_Off(1, AC_WDAT1)\r\n" +
                                ";ENDFOLD",
                                this.MPassPointCounter,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                this.WeldSeamNumber);

                            this.MPassPositiv = this.MPassPositiv + string.Format(
                                ";FOLD ARCOFF MLDAT LIN SP{0} CPDAT1 Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arcofflin; Kuka.PointName=SP{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT1;  Kuka.MovementParameterFieldEnabled=True; ArcSense.Enabled=False; ArcTech.WdatVarName=MLDAT\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT1\r\n" +
                                "FDAT_ACT=FSP{0}\r\n" +
                                "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                                "LIN XSP{0}\r\n" +
                                "Arc_Off(1, AC_MLDAT)\r\n" +
                                ";ENDFOLD\r\n",
                                this.MPassPointCounter.ToString().PadLeft(3, '0'),
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                this.WeldSeamNumber);

                            this.MultiPassSrcWriter.WriteLine(MultiPassSrcFooter(MultiLayer));

                            this.E6mlDat = this.E6mlDat +
                                string.Format("e6ML_SP0_ACT[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                            this.FmlDat = this.FmlDat +
                                string.Format("fML_SP0_TTS[{0}]={{{1}{2}}}\r\n",
                                            this.MPassPointCounter,
                                            this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                            this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                            this.MPassWeldPar = this.MPassWeldPar +
                                                string.Format(
                                                    "DECL E6POS XP{0}={{{1}{2}}}" + "\r\n",
                                                    this.MPassPointCounter,
                                                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                                                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);

                            for (int PointCounter = 1; PointCounter < 129; PointCounter++)
                            {
                                if (PointCounter <= this.MPassPointCounter)
                                {
                                    this.MPassWeldPar = this.MPassWeldPar +
                                                        string.Format("DECL FDAT FP{0}={{TOOL_NO {1},BASE_NO {2},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}" + "\r\n" +
                                                                    "DECL FDAT FSP{3}={{TOOL_NO {1},BASE_NO {2},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}" + "\r\n",
                                                                    PointCounter,
                                                                    OperationManager.GetTcpId(operationNode),
                                                                    OperationManager.GetUserFrame(operationNode).Number,
                                                                    PointCounter.ToString().PadLeft(3,'0'));
                                }
                                else
                                {
                                    this.E6mlDat = this.E6mlDat + string.Format(
                                        "e6ML_SP0_ACT[{0}]={{X 9999.90,Y 9999.90,Z 9999.90,A 9999.90,B 9999.90,C 9999.90,S 0,T 0,E1 9999.90,E2 9999.90,E3 9999.90,E4 9999.90,E5 9999.90,E6 9999.90}}\r\n",
                                        PointCounter);
                                    this.FmlDat = this.FmlDat + string.Format(
                                        "fML_SP0_TTS[{0}]={{X 9999.90,Y 9999.90,Z 9999.90,A 9999.90,B 9999.90,C 9999.90,S 0,T 0,E1 9999.90,E2 9999.90,E3 9999.90,E4 9999.90,E5 9999.90,E6 9999.90}}\r\n",
                                        PointCounter);
                                }
                            }
                            for (int MultiCounter = 1; MultiCounter <= MultiLayer; MultiCounter++)
                            {
                                this.Moves.WriteLine(
                                        this.CurrentPostFile.Children[1].FileName + "({0})" + "\r\n",
                                        MultiCounter );
                                if (MultiCounter < MultiLayer)
                                {
                                    this.Moves.WriteLine(                                        
                                            ";FOLD PTP P{0}{1} Vel={2} m/s PDAT1 Tool[{4}] Base[{5}];%{{PE}}%R 8.3.43,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:PTP, 2:P{0}, 3:, 5:{2}, 7:PDAT1\r\n" +
                                            "$BWDSTART=FALSE\r\n" +
                                            "PDAT_ACT=PPDAT1\r\n" +
                                            "FDAT_ACT=FP{0}\r\n" +
                                            "BAS(#CP_PARAMS,{2})\r\n" +
                                            "PTP XP{0} C_DIS\r\n" +
                                            ";ENDFOLD",
                                            this.ArcOnPointTemp - 1,
                                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                            this.PdatNumber ,
                                            OperationManager.GetTcpId(operationNode),
                                            OperationManager.GetUserFrame(operationNode).Number);  
                                }
                            }

                            this.E6mlDat = this.E6mlDat + ";ENDFOLD (e6ML_SP0_ACT)\r\n";
                            this.FmlDat = this.FmlDat + ";ENDFOLD (fML_SP0_TTS)\r\n" +
                                                        ";ENDFOLD (MultiLayer)\r\n" +
                                                        ";ENDFOLD (EXTERNAL DECLARATIONS)\r\n" + "\r\n";
                            this.MultiPassDatWriter.WriteLine(this.E6mlDat + this.FmlDat + this.MPassWeldPar);
                        }
                        else
                        {
                            this.Moves.WriteLine(
                                ";FOLD ARCOFF WDAT{3} LIN P{0} CPDAT{2} Tool[{4}] Base[{5}] ;%{{PE}}\r\n" +
                                ";FOLD Parameters ;%{{h}}\r\n" +
                                ";Params IlfProvider=kukaroboter.arctech.arcofflin; Kuka.PointName=P{0}; Kuka.BlendingEnabled=False; Kuka.MoveDataName=CPDAT{2};  Kuka.MovementParameterFieldEnabled=True; ArcSense.Enabled=False; ArcTech.WdatVarName=WDAT{3}\r\n" +
                                ";ENDFOLD\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT{2}\r\n" +
                                "FDAT_ACT=FP{0}\r\n" +
                                "BAS(#CP_PARAMS,ATBg_BAS_VELDefinition)\r\n" +
                                "LIN XP{0}\r\n" +
                                "Arc_Off(1, AC_WDAT1)\r\n" +
                                ";ENDFOLD",
                                this.PointNumber,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                this.WeldSeamNumber);
                        }
                        

                        this.CurrentProcessStatus = ProcessStatus.None;
                    }
                    else if (this.CurrentProcessStatus == ProcessStatus.None)
                    {
                        this.Moves.WriteLine(
                            ";FOLD LIN P{0}{1} Vel={2} m/s CPDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P{0}, 3:{6}, 5:{2}, 7:CPDAT{3}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{3}\r\n" +
                            "FDAT_ACT=FP{0}\r\n" +
                            "BAS(#CP_PARAMS,{2})\r\n" +
                            "LIN XP{0} {6}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS C_DIS" : string.Empty);
                    }

                    break;
            }

            this.PointNumber++;
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (point.IsArcMiddlePoint())
            {
                // Arc middle point move
                this.DatOut.WriteLine(
                    "DECL E6POS XP{0}={{{1}{2}}}",
                    this.PointNumber,
                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);
            }
            else
            {
                // Arc end point
                this.DatOut.WriteLine(
                    "DECL E6POS XP{0}={{{1}{2}}}\r\n" +
                    "DECL FDAT FP{0}={{TOOL_NO {3},BASE_NO {4},IPO_FRAME #BASE,POINT2[] \"P{0}\",TQ_STATE FALSE}}",
                    this.PointNumber,
                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                    OperationManager.GetTcpId(operationNode),
                    OperationManager.GetUserFrame(operationNode).Number);

                string currentLdat = this.FormatLcpdat(operationNode, point);

                if (currentLdat != this.CachedLdat)
                {
                    this.LdatNumber++;
                    this.CachedLdat = this.FormatLcpdat(operationNode, point); // We need to update the CachedLdat after the LdatNumber increment
                    this.DatOut.WriteLine(this.CachedLdat);
                }

                switch (this.KrcVersion)
                {
                    case KukaKrcVersion.Krc2V55:
                        if (this.CurrentProcessStatus == ProcessStatus.ArcOn)
                        {
                            this.Moves.WriteLine(
                                ";FOLD CIRC P{0} P{1} Vel={2} m/s CPDAT{3} ARC_ON PS S Seam{4} Tool[{5}] Base[{6}];%{{PE}}%R 5.5.31,%MKUKATPARC,%CARC_ON,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:, 6:{2}, 8:CPDAT{3}, 11:1, 12:S, 13:Seam{4}\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT{3}\r\n" +
                                "FDAT_ACT=FP{1}\r\n" +
                                "BAS(#CP_PARAMS,{2})\r\n" +
                                "CIRC XP{0},XP{1}\r\n" +
                                "A10(#ARC_STRT)\r\n" +
                                ";ENDFOLD",
                                this.PointNumber - 1,
                                this.PointNumber,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldSeamNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number);

                            this.CurrentProcessStatus = ProcessStatus.ArcSwitch;
                        }
                        else if (this.CurrentProcessStatus == ProcessStatus.ArcSwitch)
                        {
                            this.Moves.WriteLine(
                                ";FOLD CIRC P{0} P{1} CPDAT{3} ARC PS W{4} TRACK T1 Tool[{5}] Base[{6}];%{{PE}}%R 5.5.31,%MKUKATPARC,%CTRACK_SWI,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:{7}, 6:{2}, 8:CPDAT{3}, 11:W{4}, 12:2, 13:T1\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT{3}\r\n" +
                                "FDAT_ACT=FP{1}\r\n" +
                                "BAS(#CP_PARAMS,LDEFAULT.VEL)\r\n" +
                                "PS_TEMP_SET=A10TT1\r\n" +
                                "A10(#PRE_ARC_SWI,A_S_PARA_ACT,A10WW{4},A_E_PARA_ACT,1)\r\n" +
                                "CIRC XP{0},XP{1} {7}\r\n" +
                                ";ENDFOLD",
                                this.PointNumber - 1,
                                this.PointNumber,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                        }
                        else if (this.CurrentProcessStatus == ProcessStatus.ArcOff)
                        {
                            this.Moves.WriteLine(
                                ";FOLD CIRC P{0} P{1} CPDAT{3} ARC_OFF PS W{4} E TRACK T Seam{7} Tool[{5}] Base[{6}];%{{PE}}%R 5.5.31,%MKUKATPARC,%CTRACK_OFF,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:, 6:{2}, 8:CPDAT{3}, 11:1, 12:W{4}, 13:E, 14:2, 15:T, 16:Seam{7}\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT{3}\r\n" +
                                "FDAT_ACT=FP{1}\r\n" +
                                "BAS(#CP_PARAMS,LDEFAULT.VEL)\r\n" +
                                "PS_TEMP_SET=A10TT\r\n" +
                                "A10(#PRE_ARC_OFF,A_S_PARA_ACT,A10WW{4},A10EE,1)\r\n" +
                                "CIRC XP{0},XP{1}\r\n" +
                                "A10(#ARC_OFF)\r\n" +
                                ";ENDFOLD",
                                this.PointNumber - 1,
                                this.PointNumber,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                this.WeldDataNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                this.WeldSeamNumber);

                            this.CurrentProcessStatus = ProcessStatus.None;
                        }
                        else if (this.CurrentProcessStatus == ProcessStatus.None)
                        {
                            this.Moves.WriteLine(
                                ";FOLD CIRC P{0} P{1}{2} Vel={3} m/s CPDAT{4} Tool[{5}] Base[{6}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:{7}, 6:{3}, 8:CPDAT{4}\r\n" +
                                "$BWDSTART=FALSE\r\n" +
                                "LDAT_ACT=LCPDAT{4}\r\n" +
                                "FDAT_ACT=FP{1}\r\n" +
                                "BAS(#CP_PARAMS,{3})\r\n" +
                                "CIRC XP{0},XP{1} {7}\r\n" +
                                ";ENDFOLD",
                                this.PointNumber - 1,
                                this.PointNumber,
                                KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                                point.Feedrate().LinearFeedrate.VelCpFormatted(),
                                this.LdatNumber,
                                OperationManager.GetTcpId(operationNode),
                                OperationManager.GetUserFrame(operationNode).Number,
                                KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                        }

                        break;
                    case KukaKrcVersion.Krc4V83:
                        this.Moves.WriteLine(
                            ";FOLD CIRC P{0} P{1}{2} Vel={3} m/s CPDAT{4} Tool[{5}] Base[{6}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:{7}, 6:{3}, 8:CPDAT{4}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{4}\r\n" +
                            "FDAT_ACT=FP{1}\r\n" +
                            "BAS(#CP_PARAMS,{3})\r\n" +
                            "CIRC XP{0},XP{1} {7}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber - 1,
                            this.PointNumber,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS C_DIS" : string.Empty);
                        break;
                }
            }

            this.PointNumber++;
        }
    }
}
