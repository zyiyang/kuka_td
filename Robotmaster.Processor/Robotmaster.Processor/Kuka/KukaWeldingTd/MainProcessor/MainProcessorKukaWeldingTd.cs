﻿// <copyright file="MainProcessorKukaWeldingTd.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.MainProcessor
{
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Kuka.Default.MainProcessor;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.AuxiliaryMenus;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.ProcessMenus;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.Event;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Kuka Welding Tiandi dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorKukaWeldingTd : MainProcessorKuka
    {
        /// <summary>
        /// Gets or sets the current correction variables count.
        /// </summary>
        internal int CorrectionVariablesCount { get; set; }

        /// <summary>
        /// Gets or sets the cached touch count.
        /// </summary>
        internal int CachedTouchCount { get; set; }

        /// <summary>
        /// Gets or sets the cached touch search correction index.
        /// </summary>
        internal KukaWeldingTdSearchIndex CachedTouchSearchCorrectionIndex { get; set; } = KukaWeldingTdSearchIndex.Start;

        /// <summary>
        /// Gets or sets the current operation correction variables.
        /// </summary>
        internal List<int> OperationCorrectionVariables { get; set; }

        /// <summary>
        /// Gets or sets the complete set of correction variables.
        /// </summary>
        internal List<int> AllCorrectionVariables { get; set; }

        /// <inheritdoc/>
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            base.EditOperationBeforeAllPointEdits(operationNode);

            this.CorrectionVariablesCount = -1;
            this.CachedTouchCount = 0;
            this.OperationCorrectionVariables = new List<int>();
            this.AllCorrectionVariables = new List<int>();

            CbtNode myFirstWeldingOperation = ProgramManager.GetOperationNodes(this.ProgramNode).FirstOrDefault(op =>
                OperationManager.GetApplicationType(op) == ApplicationType.Welding ||
                OperationManager.GetApplicationType(op) == ApplicationType.WeldingWithTouchSensing);

            if (myFirstWeldingOperation != null && operationNode == myFirstWeldingOperation)
            {
                this.EditArcDataPoint(operationNode, OperationManager.GetFirstPoint(operationNode));
            }

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing ||
                OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                this.EditTouchSensingOperation(operationNode);
            }
        }

        /// <inheritdoc/>
        internal override void EditOperationAfterAllPointEdits(CbtNode operationNode)
        {
            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing ||
                OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                this.ConsolidateCorrectionVariables(operationNode);
            }

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding ||
                OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                this.EditWeldingOperation(operationNode);
            }
        }

        /// <inheritdoc/>
        internal override PathNode EditFirstPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            PathNode nextPoint = point.NextPoint(operationNode);

            if (applicationType == ApplicationType.Welding ||
                (applicationType == ApplicationType.WeldingWithTouchSensing && !this.IsTouchPointWeldingWithTouchSensing(operationNode, nextPoint)))
            {
                point = this.EditArcOnPoint(operationNode, point);
            }

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditLastPointOfContact(CbtNode operationNode, PathNode point)
        {
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);
            PathNode previousPoint = point.PreviousPoint(operationNode);

            if (applicationType == ApplicationType.Welding ||
                (applicationType == ApplicationType.WeldingWithTouchSensing && !this.IsTouchPointWeldingWithTouchSensing(operationNode, previousPoint)))
            {
                point = this.EditArcOffPoint(operationNode, point);
            }

            return point;
        }

        /// <inheritdoc/>
        internal override PathNode EditPoint(CbtNode operationNode, PathNode point)
        {
            point = base.EditPoint(operationNode, point);

            if (this.IsTouchPoint(operationNode, point) || this.IsTouchPointWeldingWithTouchSensing(operationNode, point))
            {
                PathNode searchPoint = point.PreviousPoint(operationNode);
                searchPoint = this.EditSearchPoint(operationNode, searchPoint);
            }

            return point;
        }

        /// <summary>
        /// Edit a Touch Sensing operation.
        /// </summary>
        /// <param name="operationNode">Touch sensing operationNode.</param>
        internal virtual void EditTouchSensingOperation(CbtNode operationNode)
        {
            int[] correctionVariables;

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                correctionVariables = new[]
                {
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable1(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable2(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable3(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable4(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable5(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable6(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable1(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable2(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable3(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable4(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable5(operationNode),
                    KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable6(operationNode),
                };
            }
            else //// Welding with Touch Sensing
            {
                correctionVariables = new[]
                {
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable1(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable2(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable3(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable4(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable5(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable6(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable1(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable2(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable3(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable4(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable5(operationNode),
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable6(operationNode),
                };
            }

            this.AllCorrectionVariables = new List<int>(correctionVariables);
            this.OperationCorrectionVariables = new List<int>(correctionVariables).FindAll(var => var > 0);

            if (this.OperationCorrectionVariables.Count != this.OperationCorrectionVariables.Distinct().Count())
            {
                this.NotifyUser($"Warning in {OperationManager.GetOperationName(operationNode)}: one or more Correction Variables are repeated");
            }
        }

        /// <summary>
        /// Consolidates Correction Variables to ensure than the menu values follow the Touch Sensing Logic.
        /// </summary>
        /// <param name="operationNode">The Operation Node.</param>
        internal void ConsolidateCorrectionVariables(CbtNode operationNode)
        {
            int lastUsedVariable = this.CorrectionVariablesCount >= 0 ? this.OperationCorrectionVariables[this.CorrectionVariablesCount] : -1;
            var variableFound = false;

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.TouchSensing)
            {
                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable1(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable2(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetStartCorrectionVariable2(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable3(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetStartCorrectionVariable3(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable4(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetStartCorrectionVariable4(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable5(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetStartCorrectionVariable5(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetStartCorrectionVariable6(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetStartCorrectionVariable6(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable1(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable1(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable2(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable2(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable3(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable3(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable4(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable4(operationNode, 0);
                    }
                }

                if (KukaTdTouchSensingAuxiliaryMenu.GetEndCorrectionVariable5(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable5(operationNode, 0);
                    }
                }

                if (variableFound)
                {
                    KukaTdTouchSensingAuxiliaryMenu.SetEndCorrectionVariable6(operationNode, 0);
                }
            }
            else
            {
                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable1(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable2(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetStartCorrectionVariable2(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable3(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetStartCorrectionVariable3(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable4(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetStartCorrectionVariable4(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable5(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetStartCorrectionVariable5(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetStartCorrectionVariable6(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetStartCorrectionVariable6(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable1(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable1(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable2(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable2(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable3(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable3(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable4(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable4(operationNode, 0);
                    }
                }

                if (KukaTdWeldingWithTouchSensingAuxiliaryMenu.GetEndCorrectionVariable5(operationNode) == lastUsedVariable)
                {
                    variableFound = true;
                }
                else
                {
                    if (variableFound)
                    {
                        KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable5(operationNode, 0);
                    }
                }

                if (variableFound)
                {
                    KukaTdWeldingWithTouchSensingAuxiliaryMenu.SetEndCorrectionVariable6(operationNode, 0);
                }
            }
        }

        /// <summary>
        /// Edit a Welding operation.
        /// </summary>
        /// <param name="operationNode">Welding operationNode.</param>
        internal virtual void EditWeldingOperation(CbtNode operationNode)
        {
            // Get the Touch Sensing Operation Node
            CbtNode touchSensingOperationNode;

            if (OperationManager.GetApplicationType(operationNode) == ApplicationType.Welding && OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot) != null)
            {
                touchSensingOperationNode = OperationManager.GetTouchSensingOperation(operationNode, this.OperationCbtRoot);

                var touchSensingFound = false;

                foreach (CbtNode operation in ProgramManager.GetOperationNodes(this.ProgramNode))
                {
                    if (operation == touchSensingOperationNode)
                    {
                        touchSensingFound = true;
                        break;
                    }

                    if (operation == operationNode)
                    {
                        break;
                    }
                }

                if (!touchSensingFound)
                {
                    this.NotifyUser($"Warning in {OperationManager.GetOperationName(operationNode)}: {OperationManager.GetOperationName(touchSensingOperationNode)} is referenced before being called in {ProgramManager.GetProgramName(this.ProgramNode)}");
                }
            }
            else if (OperationManager.GetApplicationType(operationNode) == ApplicationType.WeldingWithTouchSensing)
            {
                touchSensingOperationNode = operationNode;
            }
            else
            {
                touchSensingOperationNode = null;
            }

            // For the Touch Sensing Operation Node, populate the Touch Search Group List
            var touchSearchGroupsList = new List<TouchSearchGroup>();
            var currentSearchGroupNumber = 0;

            if (touchSensingOperationNode != null)
            {
                for (PathNode point = OperationManager.GetFirstPoint(touchSensingOperationNode); point != null; point = point.NextPoint(touchSensingOperationNode))
                {
                    if (point.Events()?.EventListBefore != null)
                    {
                        foreach (Event eventBefore in point.Events().EventListBefore)
                        {
                            if (eventBefore is TouchSearch myTouchSearch)
                            {
                                if (currentSearchGroupNumber != myTouchSearch.SearchGroup)
                                {
                                    touchSearchGroupsList.Add(new TouchSearchGroup());
                                    currentSearchGroupNumber = myTouchSearch.SearchGroup;
                                }

                                TouchSearchGroup currentSearchGroup = touchSearchGroupsList[touchSearchGroupsList.Count - 1];
                                Vector3 pointInUserFrame = point.PathPointFrameInUserFrame(touchSensingOperationNode, this.SceneCbtRoot).Position;
                                Vector3 nextPointInUserFrame = point.NextPoint(touchSensingOperationNode).PathPointFrameInUserFrame(touchSensingOperationNode, this.SceneCbtRoot).Position;

                                currentSearchGroup.CorrectionVariables.Add(myTouchSearch.TouchSearchVariable);
                                currentSearchGroup.TouchPoints.Add(nextPointInUserFrame);
                                currentSearchGroup.TouchDirections.Add((nextPointInUserFrame - pointInUserFrame).GetNormalized());
                                currentSearchGroup.AveragePoint = ((currentSearchGroup.AveragePoint * (currentSearchGroup.TouchPoints.Count - 1)) + nextPointInUserFrame) / currentSearchGroup.TouchPoints.Count;
                                currentSearchGroup.ReferencePoint = this.GetReferencePoint(currentSearchGroup);
                            }
                        }
                    }
                }
            }

            // Using Touch Search Groups, add Correction Activations
            var addCorrectionOnEvent = false;
            var addCorrectionOffEvent = false;
            var isCorrectionOn = false;
            int currentGroupIndex = -1;

            if (touchSearchGroupsList.Count > 0)
            {
                for (PathNode point = OperationManager.GetFirstPoint(operationNode); point != null; point = point.NextPoint(operationNode))
                {
                    if (point.Events()?.EventListBefore != null)
                    {
                        foreach (Event eventBefore in point.Events().EventListBefore)
                        {
                            switch (eventBefore)
                            {
                                case ArcOn _:
                                    addCorrectionOnEvent = true;
                                    isCorrectionOn = true;
                                    break;
                                case ArcOff _:
                                    addCorrectionOffEvent = true;
                                    isCorrectionOn = false;
                                    break;
                            }
                        }

                        if (addCorrectionOnEvent)
                        {
                            currentGroupIndex = this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList);

                            TouchSearchGroup myTouchSearchGroup = touchSearchGroupsList[currentGroupIndex];

                            point = PointManager.AddEventToPathPoint(
                                new Correction
                                {
                                    CorrectionType = (KukaWeldingTdCorrectionType)myTouchSearchGroup.CorrectionVariables.Count,
                                    CorrectionVariable1 = myTouchSearchGroup.CorrectionVariables.Count >= 1 ? myTouchSearchGroup.CorrectionVariables[0] : 0,
                                    CorrectionVariable2 = myTouchSearchGroup.CorrectionVariables.Count >= 2 ? myTouchSearchGroup.CorrectionVariables[1] : 0,
                                    CorrectionVariable3 = myTouchSearchGroup.CorrectionVariables.Count >= 3 ? myTouchSearchGroup.CorrectionVariables[2] : 0,
                                },
                                EventIndex.Before,
                                point,
                                operationNode);

                            addCorrectionOnEvent = false;
                        }

                        if (addCorrectionOffEvent)
                        {
                            point = PointManager.AddEventToPathPoint(
                                new CorrectionOff
                                {
                                },
                                EventIndex.After,
                                point,
                                operationNode);

                            addCorrectionOffEvent = false;
                        }
                    }

                    if (!point.IsArcMiddlePoint() && isCorrectionOn)
                    {
                        if (currentGroupIndex != this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList))
                        {
                            currentGroupIndex = this.GetClosestSearchGroup(operationNode, point, touchSearchGroupsList);

                            TouchSearchGroup myTouchSearchGroup = touchSearchGroupsList[currentGroupIndex];

                            point = PointManager.AddEventToPathPoint(
                                new Correction
                                {
                                    CorrectionType = (KukaWeldingTdCorrectionType)myTouchSearchGroup.CorrectionVariables.Count,
                                    CorrectionVariable1 = myTouchSearchGroup.CorrectionVariables.Count >= 1 ? myTouchSearchGroup.CorrectionVariables[0] : 0,
                                    CorrectionVariable2 = myTouchSearchGroup.CorrectionVariables.Count >= 2 ? myTouchSearchGroup.CorrectionVariables[1] : 0,
                                    CorrectionVariable3 = myTouchSearchGroup.CorrectionVariables.Count >= 3 ? myTouchSearchGroup.CorrectionVariables[2] : 0,
                                    SwitchedCorrection = true,
                                },
                                EventIndex.Before,
                                point,
                                operationNode);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating the Reference Point for perpendicular 2D and 3D touches.
        /// </summary>
        /// <param name="touchSearchGroup">The Touch Search Group.</param>
        /// <returns>The Reference Point.</returns>
        internal Vector3 GetReferencePoint(TouchSearchGroup touchSearchGroup)
        {
            int touchCount = touchSearchGroup.TouchPoints.Count;

            if (touchCount == 2 || touchCount == 3)
            {
                Vector3 firstTouchDirection, secondTouchDirection, thirdTouchDirection;

                Vector3 firstTouchPoint, secondTouchPoint, thirdTouchPoint;

                if (touchCount == 2)
                {
                    firstTouchDirection = touchSearchGroup.TouchDirections[0];
                    secondTouchDirection = touchSearchGroup.TouchDirections[1];
                    thirdTouchDirection = Vector3.CrossProduct(firstTouchDirection, secondTouchDirection);

                    if (thirdTouchDirection.Length < 0.01)
                    {
                        return touchSearchGroup.AveragePoint;
                    }

                    thirdTouchDirection.Normalize();

                    firstTouchPoint = touchSearchGroup.TouchPoints[0];
                    secondTouchPoint = touchSearchGroup.TouchPoints[1];
                    thirdTouchPoint = touchSearchGroup.TouchPoints[0];
                }
                else
                {
                    firstTouchDirection = touchSearchGroup.TouchDirections[0];
                    secondTouchDirection = touchSearchGroup.TouchDirections[1];
                    thirdTouchDirection = touchSearchGroup.TouchDirections[2];

                    if (Vector3.CrossProduct(firstTouchDirection, secondTouchDirection).Length < 0.01 ||
                        Vector3.CrossProduct(firstTouchDirection, thirdTouchDirection).Length < 0.01 ||
                        Vector3.CrossProduct(secondTouchDirection, thirdTouchDirection).Length < 0.01)
                    {
                        return touchSearchGroup.AveragePoint;
                    }

                    firstTouchPoint = touchSearchGroup.TouchPoints[0];
                    secondTouchPoint = touchSearchGroup.TouchPoints[1];
                    thirdTouchPoint = touchSearchGroup.TouchPoints[2];
                }

                Matrix3X3 mat;
                mat.E11 = firstTouchDirection.X;
                mat.E12 = firstTouchDirection.Y;
                mat.E13 = firstTouchDirection.Z;
                mat.E21 = secondTouchDirection.X;
                mat.E22 = secondTouchDirection.Y;
                mat.E23 = secondTouchDirection.Z;
                mat.E31 = thirdTouchDirection.X;
                mat.E32 = thirdTouchDirection.Y;
                mat.E33 = thirdTouchDirection.Z;
                mat.Inverse();

                var touchVector = new Vector3(Vector3.DotProduct(firstTouchDirection, firstTouchPoint), Vector3.DotProduct(secondTouchDirection, secondTouchPoint), Vector3.DotProduct(thirdTouchDirection, thirdTouchPoint));

                return mat * touchVector;
            }

            // If it's not possible to calculate, return the Average Point
            return touchSearchGroup.AveragePoint;
        }

        /// <summary>
        ///     Gets a value indicating the index of the closest Search Group to the current point.
        /// </summary>
        /// <param name="operationNode">The operation node.</param>
        /// <param name="point">The point.</param>
        /// <param name="touchSearchGroups">The list of touch search groups.</param>
        /// <returns>Index of the closest Search Group.</returns>
        internal int GetClosestSearchGroup(CbtNode operationNode, PathNode point, List<TouchSearchGroup> touchSearchGroups)
        {
            double maxDistance = double.MaxValue;
            int closestIndex = -1;

            for (var i = 0; i < touchSearchGroups.Count; i++)
            {
                double testValue = (point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position - touchSearchGroups[i].ReferencePoint).Length;

                if (testValue < maxDistance)
                {
                    maxDistance = testValue;
                    closestIndex = i;
                }
            }

            return closestIndex;
        }

        /// <summary>
        ///     Gets a value indicating whether <paramref name="point" /> is a touch point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns><c>true</c> if <paramref name="point" /> is a first point of contact; otherwise, <c>false</c>.</returns>
        internal bool IsTouchPointWeldingWithTouchSensing(CbtNode operationNode, PathNode point)
        {
            // Condition is True when previousPoint is first point of contact and nextPoint is last point of contact
            PathNode previousPoint = point.PreviousPoint(operationNode);
            PathNode nextPoint = point.NextPoint(operationNode);

            if (previousPoint != null && nextPoint != null &&
                this.IsFirstPointOfContact(operationNode, previousPoint, previousPoint.PreviousPoint(operationNode), previousPoint.NextPoint(operationNode)) &&
                this.IsLastPointOfContact(operationNode, nextPoint, nextPoint.PreviousPoint(operationNode), nextPoint.NextPoint(operationNode)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///     Edits Arc On Point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal PathNode EditArcOnPoint(CbtNode operationNode, PathNode point)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOn
                {
                    WeldVelocity = KukaWeldingTdWeldData.GetWeldVelocity(operationNode),
                    WeldVoltage = KukaWeldingTdWeldData.GetWeldVoltage(operationNode),
                    WireVelocity = KukaWeldingTdWeldData.GetWireVelocity(operationNode),
                    WeaveStyle = (KukaWeldingTdWeaveStyle)KukaWeldingTdWeldData.GetWeaveStyle(operationNode),
                    WeaveLength = KukaWeldingTdWeldData.GetWeaveLength(operationNode),
                    WeaveAmplitude = KukaWeldingTdWeldData.GetWeaveAmplitude(operationNode),
                    WeavePlane = KukaWeldingTdWeldData.GetWeavePlane(operationNode),
                },
                EventIndex.Before,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Edits Arc Off Point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal PathNode EditArcOffPoint(CbtNode operationNode, PathNode point)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOff
                {
                },
                EventIndex.Before,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Edits Arc Data Point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal PathNode EditArcDataPoint(CbtNode operationNode, PathNode point)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcData
                {
                    StartWeldVoltage = KukaWeldingTdArcData.GetArcOnWeldVoltage(this.ProgramNode),
                    StartWireVelocity = KukaWeldingTdArcData.GetArcOnWireVelocity(this.ProgramNode),
                    StartTime = KukaWeldingTdArcData.GetArcOnStartTime(this.ProgramNode),
                    PreGasTime = KukaWeldingTdArcData.GetArcOnPreGasTime(this.ProgramNode),
                    EndWeldVoltage = KukaWeldingTdArcData.GetArcOffWeldVoltage(this.ProgramNode),
                    EndWireVelocity = KukaWeldingTdArcData.GetArcOffWireVelocity(this.ProgramNode),
                },
                EventIndex.Before,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Edits Search Point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <returns>The edited point.</returns>
        internal PathNode EditSearchPoint(CbtNode operationNode, PathNode point)
        {
            if (point != null && this.CorrectionVariablesCount + 1 <= this.OperationCorrectionVariables.Count - 1)
            {
                this.CorrectionVariablesCount++;
                this.CachedTouchCount++;

                int touchSearchVariable = this.OperationCorrectionVariables[this.CorrectionVariablesCount];
                int touchSearchIndex = this.AllCorrectionVariables.IndexOf(touchSearchVariable);

                KukaWeldingTdSearchIndex touchSearchCorrectionIndex = touchSearchIndex < 6 ? KukaWeldingTdSearchIndex.Start : KukaWeldingTdSearchIndex.End;

                if (touchSearchCorrectionIndex != this.CachedTouchSearchCorrectionIndex)
                {
                    this.CachedTouchSearchCorrectionIndex = touchSearchCorrectionIndex;
                    this.CachedTouchCount = 1;
                }

                point = PointManager.AddEventToPathPoint(
                    new TouchSearch
                    {
                        TouchSearchVariable = touchSearchVariable,
                        SearchGroup = touchSearchCorrectionIndex == KukaWeldingTdSearchIndex.Start ? 1 : 2,
                        TouchCount = this.CachedTouchCount,
                    },
                    EventIndex.Before,
                    point,
                    operationNode);

                if (point.MoveType() != MoveType.Linear)
                {
                    this.NotifyUser($"Warning in {OperationManager.GetOperationName(operationNode)}: Touch Search point must be Linear");
                }
            }

            return point;
        }
    }
}