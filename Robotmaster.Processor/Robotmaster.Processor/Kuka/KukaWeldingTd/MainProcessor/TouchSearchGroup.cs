﻿// <copyright file="TouchSearchGroup.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.MainProcessor
{
    using System.Collections.Generic;
    using Robotmaster.Math.Algebra;

    /// <summary>
    ///     Touch Search Group class.
    /// </summary>
    public class TouchSearchGroup
    {
        /// <summary>
        ///     Gets or sets the list of Correction Variables.
        /// </summary>
        public List<int> CorrectionVariables { get; set; } = new List<int>();

        /// <summary>
        ///     Gets or sets the list of Touch Points.
        /// </summary>
        public List<Vector3> TouchPoints { get; set; } = new List<Vector3>();

        /// <summary>
        ///     Gets or sets the list of Touch Directions.
        /// </summary>
        public List<Vector3> TouchDirections { get; set; } = new List<Vector3>();

        /// <summary>
        ///     Gets or sets the Touch Group Average point.
        /// </summary>
        public Vector3 AveragePoint { get; set; } = Vector3.Zero;

        /// <summary>
        ///     Gets or sets the Touch Group Reference point.
        /// </summary>
        public Vector3 ReferencePoint { get; set; } = Vector3.Zero;
    }
}
