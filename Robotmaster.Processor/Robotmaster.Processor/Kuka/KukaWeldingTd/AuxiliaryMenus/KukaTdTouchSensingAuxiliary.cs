﻿// <copyright file="KukaTdTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.AuxiliaryMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    /// <inheritdoc />
    [DataContract(Name = "KukaTdTouchSensingAuxiliary")]
    public class KukaTdTouchSensingAuxiliary : AuxiliaryMenu, IKukaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaTdTouchSensingAuxiliary"/> class.
        /// </summary>
        public KukaTdTouchSensingAuxiliary()
        {
            this.Name = "Kuka Tiandi Touch Sensing";
            this.ApplicationType = ApplicationType.TouchSensing;
            this.AuxiliaryMenuFileName = "KukaTdTouchSensingAuxiliaryMenu";
        }
    }
}
