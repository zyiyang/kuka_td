﻿// <copyright file="KukaTdWeldingWithTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.AuxiliaryMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    /// <inheritdoc />
    [DataContract(Name = "KukaTdWeldingWithTouchSensingAuxiliary")]
    public class KukaTdWeldingWithTouchSensingAuxiliary : AuxiliaryMenu, IKukaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaTdWeldingWithTouchSensingAuxiliary"/> class.
        /// </summary>
        public KukaTdWeldingWithTouchSensingAuxiliary()
        {
            this.Name = "Kuka Tiandi Welding with Touch Sensing";
            this.ApplicationType = ApplicationType.WeldingWithTouchSensing;
            this.AuxiliaryMenuFileName = "KukaTdWeldingWithTouchSensingAuxiliaryMenu";
        }
    }
}
