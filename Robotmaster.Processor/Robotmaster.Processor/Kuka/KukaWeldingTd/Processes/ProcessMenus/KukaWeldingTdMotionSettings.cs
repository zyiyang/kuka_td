// <copyright file="KukaWeldingTdMotionSettings.cs" company="Hypertherm Robotic Software Inc.">
// Copyright 2002-2020 Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>
// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.ProcessMenus
{
    using System;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Manages the data of the external menu Kuka Inline Motion Settings (Overrides Kuka Motion Settings).
    /// </summary>
    internal static class KukaWeldingTdMotionSettings
    {
        /// <summary>
        ///     The cache for retrieved SMS values.
        /// </summary>
        private static readonly SmsValueCache Cache = new SmsValueCache();

        /// <summary>
        ///     Checks if the external menu is loaded.
        /// </summary>
        /// <param name="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu exists; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsLoaded(object node)
        {
            return ExternalMenuManager.IsExternalMenuLoaded("PM_KukaWeldingTdMotionSettings_CW5MJ9CQ", node);
        }

        /// <summary>
        ///     Checks if the external menu is overridden.
        /// </summary>
        /// <param node="node">
        ///     The node to check.
        /// </param>
        /// <returns>
        ///     <see langword="true"/> if the external menu is overridden; otherwise, <see langword="false"/>.
        /// </returns>
        internal static bool IsOverridden(object node)
        {
            return ExternalMenuManager.IsExternalMenuOverridden("PM_KukaWeldingTdMotionSettings_CW5MJ9CQ", node);
        }

        /// <summary>
        ///     Gets The positioning type for joint motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The positioning type for joint motion.
        /// </returns>
        internal static int GetJointPositioningType(object node, bool useCache = true)
        {
            const string settingUid = "{D1C01BCD-2CB2-4784-A32D-293BB11B488E}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The positioning type for joint motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointPositioningType(object node, string value, bool useCache = true)
        {
            const string settingUid = "{D1C01BCD-2CB2-4784-A32D-293BB11B488E}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The positioning type for linear motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The positioning type for linear motion.
        /// </returns>
        internal static int GetLinearPositioningType(object node, bool useCache = true)
        {
            const string settingUid = "{5399114B-F372-4353-942A-3995362E09A2}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The positioning type for linear motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetLinearPositioningType(object node, string value, bool useCache = true)
        {
            const string settingUid = "{5399114B-F372-4353-942A-3995362E09A2}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The positioning type for circular motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The positioning type for circular motion.
        /// </returns>
        internal static int GetCircularPositioningType(object node, bool useCache = true)
        {
            const string settingUid = "{297E45F0-5C87-4FF0-A1CA-D496570A4D4E}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The positioning type for circular motion.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetCircularPositioningType(object node, string value, bool useCache = true)
        {
            const string settingUid = "{297E45F0-5C87-4FF0-A1CA-D496570A4D4E}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets The orientation control of a CP motion in the advance run.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The orientation control of a CP motion in the advance run.
        /// </returns>
        internal static int GetOrientationControlTypeForLinearAndCircularMove(object node, bool useCache = true)
        {
            const string settingUid = "{7A437E16-F50A-49A0-A0B7-E46D9F40E135}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The orientation control of a CP motion in the advance run.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetOrientationControlTypeForLinearAndCircularMove(object node, string value, bool useCache = true)
        {
            const string settingUid = "{7A437E16-F50A-49A0-A0B7-E46D9F40E135}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value);

            if (useCache)
            {
                int convertedValue = Convert.ToInt32(value);
                Cache.SetValueInCache(settingUid, node, convertedValue);
            }
        }

        /// <summary>
        ///     Gets Approximation distance for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing Approximation distance for joint moves in Cartesian and joint space.
        /// </returns>
        internal static int GetApproximationDistanceForJointMove(object node, bool useCache = true)
        {
            const string settingUid = "{CFAE68C5-4DC6-4D3F-9834-702D5B5DA068}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets Approximation distance for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetApproximationDistanceForJointMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{CFAE68C5-4DC6-4D3F-9834-702D5B5DA068}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis velocity (%) for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis velocity (%) for joint moves in Cartesian and joint space.
        /// </returns>
        internal static int GetJointAxisVelocityForJointMove(object node, bool useCache = true)
        {
            const string settingUid = "{53E73489-17DB-4703-ADD0-9AA3691B78AF}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis velocity (%) for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisVelocityForJointMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{53E73489-17DB-4703-ADD0-9AA3691B78AF}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis acceleration (%) for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis acceleration (%) for joint moves in Cartesian and joint space.
        /// </returns>
        internal static int GetJointAxisAccelerationForJointMove(object node, bool useCache = true)
        {
            const string settingUid = "{D5CFD1BE-CFED-41EE-87E4-3290F78F31E8}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis acceleration (%) for joint moves in Cartesian and joint space.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisAccelerationForJointMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{D5CFD1BE-CFED-41EE-87E4-3290F78F31E8}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets Approximation distance for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing Approximation distance for linear and circular move.
        /// </returns>
        internal static int GetApproximationDistanceForLinearAndCircularMove(object node, bool useCache = true)
        {
            const string settingUid = "{C5A067A9-2777-450A-A88B-06F8D67FD6F0}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets Approximation distance for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetApproximationDistanceForLinearAndCircularMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{C5A067A9-2777-450A-A88B-06F8D67FD6F0}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis velocity (%) for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis velocity (%) for linear and circular move.
        /// </returns>
        internal static int GetJointAxisVelocityForLinearAndCircularMove(object node, bool useCache = true)
        {
            const string settingUid = "{EC483050-7D95-4D30-80B2-B83EACD3BB2C}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis velocity (%) for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisVelocityForLinearAndCircularMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{EC483050-7D95-4D30-80B2-B83EACD3BB2C}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets The joint axis acceleration (%) for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing The joint axis acceleration (%) for linear and circular move.
        /// </returns>
        internal static int GetJointAxisAccelerationForLinearAndCircularMove(object node, bool useCache = true)
        {
            const string settingUid = "{AB9E0D10-08EF-450B-AB2B-7A24D81BE8EA}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets The joint axis acceleration (%) for linear and circular move.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetJointAxisAccelerationForLinearAndCircularMove(object node, int value, bool useCache = true)
        {
            const string settingUid = "{AB9E0D10-08EF-450B-AB2B-7A24D81BE8EA}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }

        /// <summary>
        ///     Gets TCP acceleration.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="useCache">
        ///     Whether or not the SMS value cache should, if possible, be used to get the value.
        /// </param>
        /// <returns>
        ///     A value representing TCP acceleration.
        /// </returns>
        internal static int GetTcpAcceleration(object node, bool useCache = true)
        {
            const string settingUid = "{17DF9C2B-3001-417C-A5F4-1FA0EE8069C2}";

            if (useCache)
            {
                object cachedValue = Cache.GetValueFromCache(settingUid, node);

                if (cachedValue != null)
                {
                    return (int)cachedValue;
                }
                else
                {
                    int result = int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
                    Cache.SetValueInCache(settingUid, node, result);
                    return result;
                }
            }
            else
            {
                return int.Parse(ExternalMenuManager.GetUserValueForProcessor(settingUid, node));
            }
        }

        /// <summary>
        ///     Sets TCP acceleration.
        /// </summary>
        /// <param name="node">
        ///     The node from which to retrieve the data.
        /// </param>
        /// <param name="value">
        ///     The user value to set.
        /// </param>
        internal static void SetTcpAcceleration(object node, int value, bool useCache = true)
        {
            const string settingUid = "{17DF9C2B-3001-417C-A5F4-1FA0EE8069C2}";

            ExternalMenuManager.SetUserValueForProcessor(node, settingUid, value.ToString());

            if (useCache)
            {
                Cache.SetValueInCache(settingUid, node, value);
            }
        }
        /// <summary>
        ///     Clears the SMS value cache.
        /// </summary>
        ///
        internal static void ClearCache()
        {
            Cache.Clear();
        }
    }
}
