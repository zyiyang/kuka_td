﻿// <copyright file="KukaWeldingTdSearchIndex.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Kuka Welding Search Index.
    /// </summary>
    [DataContract(Name = "SearchIndex")]
    public enum KukaWeldingTdSearchIndex
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        [EnumMember(Value = "Undefined")]
        Undefined,

        /// <summary>
        /// Start.
        /// </summary>
        [EnumMember(Value = "Start")]
        Start,

        /// <summary>
        /// End.
        /// </summary>
        [EnumMember(Value = "End")]
        End,
    }
}
