﻿// <copyright file="KukaWeldingTdProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.MainProcessor;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.PostProcessor;
    using Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    /// Defines Kuka welding process for Tiandi.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdProcess")]
    public class KukaWeldingTdProcess : PackageProcess, IKukaProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KukaWeldingTdProcess"/> class.
        /// </summary>
        public KukaWeldingTdProcess()
        {
            this.Name = "Kuka Welding Td Process";
            this.PostProcessorType = typeof(PostProcessorKukaWeldingTd);
            this.MainProcessorType = typeof(MainProcessorKukaWeldingTd);
            this.AddEventToMet(typeof(WeldData));
            this.ProcessMenuFileName = "KukaWeldingTdProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
