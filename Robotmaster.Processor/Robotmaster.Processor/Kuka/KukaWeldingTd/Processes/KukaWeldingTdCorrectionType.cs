﻿// <copyright file="KukaWeldingTdCorrectionType.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Kuka Welding Correction Type.
    /// </summary>
    [DataContract(Name = "CorrectionType")]
    public enum KukaWeldingTdCorrectionType
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        [EnumMember(Value = "Undefined")]
        Undefined = 0,

        /// <summary>
        /// Correction 1D.
        /// </summary>
        [EnumMember(Value = "1D")]
        Correction1D = 1,

        /// <summary>
        /// Correction 2D.
        /// </summary>
        [EnumMember(Value = "2D")]
        Correction2D = 2,

        /// <summary>
        /// Correction 3D.
        /// </summary>
        [EnumMember(Value = "3D")]
        Correction3D = 3,

        /// <summary>
        /// Correction 6D.
        /// </summary>
        [EnumMember(Value = "6D")]
        Correction6D = 6,
    }
}
