﻿// <copyright file="Correction.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Correction event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdCorrection")]
    public class Correction : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Correction"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public Correction()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the correction type.
        /// </summary>
        [DataMember(Name = "KukaWeldingTdCorrectionType")]
        public KukaWeldingTdCorrectionType CorrectionType { get; set; } = KukaWeldingTdCorrectionType.Undefined;

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable1")]
        public int CorrectionVariable1 { get; set; }

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable2")]
        public int CorrectionVariable2 { get; set; }

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable3")]
        public int CorrectionVariable3 { get; set; }

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable4")]
        public int CorrectionVariable4 { get; set; }

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable5")]
        public int CorrectionVariable5 { get; set; }

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "CorrectionVariable6")]
        public int CorrectionVariable6 { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the correction is switched.
        /// </summary>
        [DataMember(Name = "SwitchedCorrection")]
        public bool SwitchedCorrection { get; set; }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return string.Format(
                "; {0}Correction {1}, Var. {2}{3}{4}",
                this.SwitchedCorrection ? "Switched " : string.Empty,
                this.CorrectionType == KukaWeldingTdCorrectionType.Correction1D ? "1D" : this.CorrectionType == KukaWeldingTdCorrectionType.Correction2D ? "2D" : this.CorrectionType == KukaWeldingTdCorrectionType.Correction3D ? "3D" : string.Empty,
                this.CorrectionVariable1,
                this.CorrectionVariable2 > 0 ? "/" + this.CorrectionVariable2 : string.Empty,
                this.CorrectionVariable3 > 0 ? "/" + this.CorrectionVariable3 : string.Empty);
        }
    }
}
