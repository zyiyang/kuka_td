﻿// <copyright file="CorrectionOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Correction Off event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdCorrectionOff")]
    public class CorrectionOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectionOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public CorrectionOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Correction Off";
        }
    }
}
