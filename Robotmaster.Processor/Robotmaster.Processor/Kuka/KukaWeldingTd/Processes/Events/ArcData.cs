﻿// <copyright file="ArcData.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Arc Data event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdArcData")]
    public class ArcData : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArcData"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ArcData()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        /// Gets or sets the Start Weld Voltage.
        /// </summary>
        [DataMember(Name = "StartWeldVoltage")]
        public double StartWeldVoltage { get; set; }

        /// <summary>
        /// Gets or sets the Start Wire Velocity.
        /// </summary>
        [DataMember(Name = "StartWireVelocity")]
        public double StartWireVelocity { get; set; }

        /// <summary>
        /// Gets or sets the Start Time.
        /// </summary>
        [DataMember(Name = "StartTime")]
        public double StartTime { get; set; }

        /// <summary>
        /// Gets or sets the Pre Gas Time.
        /// </summary>
        [DataMember(Name = "PreGasTime")]
        public double PreGasTime { get; set; }

        /// <summary>
        /// Gets or sets the End Weld Voltage.
        /// </summary>
        [DataMember(Name = "EndWeldVoltage")]
        public double EndWeldVoltage { get; set; }

        /// <summary>
        /// Gets or sets the End Wire Velocity.
        /// </summary>
        [DataMember(Name = "EndWireVelocity")]
        public double EndWireVelocity { get; set; }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Arc Data";
        }
    }
}
