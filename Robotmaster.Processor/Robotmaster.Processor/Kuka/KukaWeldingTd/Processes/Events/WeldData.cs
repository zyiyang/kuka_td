﻿// <copyright file="WeldData.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Weld Data event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdWeldData")]
    public class WeldData : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeldData"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public WeldData()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
            this.ValidEventIndexes = EventIndex.Before | EventIndex.After;
        }

        /// <summary>
        /// Gets or sets the Weld Velocity.
        /// </summary>
        [DataMember(Name = "WeldVelocity")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WeldVelocity { get; set; } = 0.23;

        /// <summary>
        /// Gets or sets the Weld Voltage.
        /// </summary>
        [DataMember(Name = "WeldVoltage")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WeldVoltage { get; set; } = 22;

        /// <summary>
        /// Gets or sets the Wire Velocity.
        /// </summary>
        [DataMember(Name = "WireVelocity")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WireVelocity { get; set; } = 320;

        /// <summary>
        /// Gets or sets the Weave Style.
        /// </summary>
        [DataMember(Name = "WeaveStyle")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public KukaWeldingTdWeaveStyle WeaveStyle { get; set; } = KukaWeldingTdWeaveStyle.Off;

        /// <summary>
        /// Gets or sets the Weave Length.
        /// </summary>
        [DataMember(Name = "WeaveLength")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WeaveLength { get; set; } = 3;

        /// <summary>
        /// Gets or sets the Weave Amplitude.
        /// </summary>
        [DataMember(Name = "WeaveAmplitude")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WeaveAmplitude { get; set; } = 2.5;

        /// <summary>
        /// Gets or sets the Weave Plane.
        /// </summary>
        [DataMember(Name = "WeavePlane")]
        [MetEventProperty] // This property can be edited by the user in the Event menu.
        public double WeavePlane { get; set; } = 0;

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Weld Data";
        }
    }
}
