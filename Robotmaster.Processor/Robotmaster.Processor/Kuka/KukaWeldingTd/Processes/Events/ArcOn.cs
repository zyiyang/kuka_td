﻿// <copyright file="ArcOn.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Arc On event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdArcOn")]
    public class ArcOn : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArcOn"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ArcOn()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        /// Gets or sets the Weld Velocity.
        /// </summary>
        [DataMember(Name = "WeldVelocity")]
        public double WeldVelocity { get; set; }

        /// <summary>
        /// Gets or sets the Weld Voltage.
        /// </summary>
        [DataMember(Name = "WeldVoltage")]
        public double WeldVoltage { get; set; }

        /// <summary>
        /// Gets or sets the Wire Velocity.
        /// </summary>
        [DataMember(Name = "WireVelocity")]
        public double WireVelocity { get; set; }

        /// <summary>
        /// Gets or sets the Weave Style.
        /// </summary>
        [DataMember(Name = "WeaveStyle")]
        public KukaWeldingTdWeaveStyle WeaveStyle { get; set; }

        /// <summary>
        /// Gets or sets the Weave Length.
        /// </summary>
        [DataMember(Name = "WeaveLength")]
        public double WeaveLength { get; set; }

        /// <summary>
        /// Gets or sets the Weave Amplitude.
        /// </summary>
        [DataMember(Name = "WeaveAmplitude")]
        public double WeaveAmplitude { get; set; }

        /// <summary>
        /// Gets or sets the Weave Plane.
        /// </summary>
        [DataMember(Name = "WeavePlane")]
        public double WeavePlane { get; set; }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Arc On";
        }
    }
}
