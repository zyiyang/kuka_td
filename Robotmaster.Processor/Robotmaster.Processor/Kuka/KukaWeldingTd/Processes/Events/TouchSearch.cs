﻿// <copyright file="TouchSearch.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Touch Search event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdTouchSearch")]
    public class TouchSearch : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TouchSearch"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public TouchSearch()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        ///     Gets or sets the search index.
        /// </summary>
        [DataMember(Name = "KukaWeldingTdSearchGroup")]
        public int SearchGroup { get; set; } = 0;

        /// <summary>
        ///     Gets or sets the correction variable.
        /// </summary>
        [DataMember(Name = "TouchSearchVariable")]
        public int TouchSearchVariable { get; set; }

        /// <summary>
        ///     Gets or sets the touch count.
        /// </summary>
        [DataMember(Name = "TouchCount")]
        public int TouchCount { get; set; }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return string.Format("; Search Group {0}, Touch {1}, Var. {2}", this.SearchGroup, this.TouchCount, this.TouchSearchVariable);
        }
    }
}
