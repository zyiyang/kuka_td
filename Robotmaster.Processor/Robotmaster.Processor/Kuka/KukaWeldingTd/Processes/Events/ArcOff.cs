﻿// <copyright file="ArcOff.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Arc Off event.
    /// </summary>
    [DataContract(Name = "KukaWeldingTdArcOff")]
    public class ArcOff : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArcOff"/> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public ArcOff()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        /// Display format in point list.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return "; Arc Off";
        }
    }
}
