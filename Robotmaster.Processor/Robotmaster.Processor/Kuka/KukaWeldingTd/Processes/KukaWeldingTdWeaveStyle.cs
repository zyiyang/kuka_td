﻿// <copyright file="KukaWeldingTdWeaveStyle.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaWeldingTd.Processes
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Kuka Welding Weave Style.
    /// </summary>
    [DataContract(Name = "WeaveStyle")]
    public enum KukaWeldingTdWeaveStyle
    {
        /// <summary>
        /// Off.
        /// </summary>
        [EnumMember(Value = "Off")]
        Off = 0,

        /// <summary>
        /// Triangle.
        /// </summary>
        [EnumMember(Value = "Triangle")]
        Triangle = 1,

        /// <summary>
        /// Style 2.
        /// </summary>
        [EnumMember(Value = "Style2")]
        Style2 = 2,

        /// <summary>
        /// Style 3.
        /// </summary>
        [EnumMember(Value = "Style3")]
        Style3 = 3,
    }
}
