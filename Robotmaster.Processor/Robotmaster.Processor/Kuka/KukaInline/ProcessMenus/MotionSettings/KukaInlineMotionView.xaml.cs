﻿// <copyright file="KukaInlineMotionView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaInline.ProcessMenus.MotionSettings
{
    /// <summary>
    /// Interaction logic for Generic Motion.
    /// </summary>
    public partial class KukaInlineMotionView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaInlineMotionView"/> class.
        /// </summary>
        public KukaInlineMotionView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaInlineMotionView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="ViewModel"/> to map to the UI.
        /// </param>
        public KukaInlineMotionView(KukaInlineMotionViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the object linked with the UI.
        /// </summary>
        public KukaInlineMotionViewModel ViewModel { get; }
    }
}
