﻿// <copyright file="KukaInlineMotionViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaInline.ProcessMenus.MotionSettings
{
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     Kuka motion view model.
    /// </summary>
    public class KukaInlineMotionViewModel : ExternalMenuUiHandlerViewModel
    {
        /// <summary>
        ///     Gets the positioning type dictionary.
        /// </summary>
        public static Dictionary<int, string> PositioningTypeSource => new Dictionary<int, string>
        {
            { 0, "Exact" },
            { 1, "Continuous" },
        };

        /// <summary>
        ///     Gets the orientation type dictionary.
        /// </summary>
        public static Dictionary<int, string> OrientationTypeSource => new Dictionary<int, string>
        {
            { 0, "OFF" },
            { 1, "VARIABLE" },
            { 2, "JOINT" },
        };

        /// <summary>
        ///     Gets or sets the joint positioning type.
        /// </summary>
        public int JointPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>("JointPositioningType").SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>("JointPositioningType").SelectedIndex = value;
                this.OnPropertyChanged(nameof(this.JointPositioningType));
            }
        }

        /// <summary>
        ///     Gets or sets the linear positioning type.
        /// </summary>
        public int LinearPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>("LinearPositioningType").SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>("LinearPositioningType").SelectedIndex = value;
                this.OnPropertyChanged(nameof(this.LinearPositioningType));
            }
        }

        /// <summary>
        ///     Gets or sets the circular positioning type.
        /// </summary>
        public int CircularPositioningType
        {
            get => this.GetParameterByPostVariable<ListParameter>("CircularPositioningType").SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>("CircularPositioningType").SelectedIndex = value;
                this.OnPropertyChanged(nameof(this.CircularPositioningType));
            }
        }

        /// <summary>
        ///     Gets or sets the orientation control type for linear and circular move.
        /// </summary>
        public int OrientationControlTypeForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<ListParameter>("OrientationControlTypeForLinearAndCircularMove").SelectedIndex;

            set
            {
                this.GetParameterByPostVariable<ListParameter>("OrientationControlTypeForLinearAndCircularMove").SelectedIndex = value;
                this.OnPropertyChanged(nameof(this.OrientationControlTypeForLinearAndCircularMove));
            }
        }

        /// <summary>
        ///     Gets or sets the approximation distance for joint moves in Cartesian and joint space.
        /// </summary>
        public int ApproximationDistanceForJointMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("ApproximationDistanceForJointMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("ApproximationDistanceForJointMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.ApproximationDistanceForJointMove));
            }
        }

        /// <summary>
        ///     Gets or sets the approximation distance for linear and circular move.
        /// </summary>
        public int ApproximationDistanceForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("ApproximationDistanceForLinearAndCircularMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("ApproximationDistanceForLinearAndCircularMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.ApproximationDistanceForLinearAndCircularMove));
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis velocity (%) for joint moves in Cartesian and joint space.
        /// </summary>
        public int JointAxisVelocityForJointMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("JointAxisVelocityForJointMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("JointAxisVelocityForJointMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.JointAxisVelocityForJointMove));
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis velocity (%) for linear and circular move.
        /// </summary>
        public int JointAxisVelocityForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("JointAxisVelocityForLinearAndCircularMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("JointAxisVelocityForLinearAndCircularMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.JointAxisVelocityForLinearAndCircularMove));
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis acceleration (%) for joint moves in Cartesian and joint space.
        /// </summary>
        public int JointAxisAccelerationForJointMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("JointAxisAccelerationForJointMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("JointAxisAccelerationForJointMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.JointAxisAccelerationForJointMove));
            }
        }

        /// <summary>
        ///     Gets or sets the joint axis acceleration (%) for linear and circular move.
        /// </summary>
        public int JointAxisAccelerationForLinearAndCircularMove
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("JointAxisAccelerationForLinearAndCircularMove").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("JointAxisAccelerationForLinearAndCircularMove").UserValue = value;
                this.OnPropertyChanged(nameof(this.JointAxisAccelerationForLinearAndCircularMove));
            }
        }

        /// <summary>
        ///     Gets or sets the TCP acceleration.
        /// </summary>
        public int TcpAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>("TcpAcceleration").UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>("TcpAcceleration").UserValue = value;
                this.OnPropertyChanged(nameof(this.TcpAcceleration));
            }
        }
    }
}
