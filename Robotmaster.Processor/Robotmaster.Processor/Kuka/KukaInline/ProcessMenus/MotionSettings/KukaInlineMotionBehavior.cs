﻿// <copyright file="KukaInlineMotionBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaInline.ProcessMenus.MotionSettings
{
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;

    /// <summary>
    ///     Defines the behavior for managing ,see cref="KukaMotionView"/>.
    /// </summary>
    internal class KukaInlineMotionBehavior : ExternalMenuUiHandlerBehavior
    {
        /// <summary>
        ///     Gets or sets the view model.
        /// </summary>
        private KukaInlineMotionViewModel ViewModel { get; set; }

        /// <summary>
        ///     Sets up the view model.
        /// </summary>
        protected override void SetupViewModel()
        {
        }
    }
}
