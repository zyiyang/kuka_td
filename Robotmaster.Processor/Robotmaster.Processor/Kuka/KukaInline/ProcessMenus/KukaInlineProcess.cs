﻿// <copyright file="KukaInlineProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaInline.ProcessMenus
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Kuka.KukaInline.PostProcessor;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Kuka inline.
    /// </summary>
    [DataContract(Name = "KukaInlineProcess")]
    public class KukaInlineProcess : PackageProcess, IKukaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaInlineProcess"/> class.
        /// </summary>
        public KukaInlineProcess()
        {
            this.Name = "Kuka Inline Process";
            this.PostProcessorType = typeof(PostProcessorKukaInline);
            this.ProcessMenuFileName = "KukaInlineMotionSettings";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
