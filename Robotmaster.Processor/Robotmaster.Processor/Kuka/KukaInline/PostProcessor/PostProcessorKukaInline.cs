﻿// <copyright file="PostProcessorKukaInline.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaInline.PostProcessor
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Processor.Kuka.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.KukaInline.ProcessMenus.MotionSettings;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    internal class PostProcessorKukaInline : PostProcessorKuka
    {
        /// <summary>
        ///     Defines the type of Kuka KRC controller.
        /// </summary>
        internal enum KukaKrcVersion
        {
            Krc2V55 = 0,
            Krc4V83 = 1,
        }

        /// <summary>
        ///     Gets or sets the Kuka KRC controller version.
        /// </summary>
        internal virtual KukaKrcVersion KrcVersion { get; set; }

        /// <summary>
        ///     Gets or sets the current PDAT number.
        /// </summary>
        internal virtual int PdatNumber { get; set; }

        /// <summary>
        ///     Gets or sets the current LDAT number.
        /// </summary>
        internal virtual int LdatNumber { get; set; }

        /// <summary>
        ///     Gets or sets the current point number.
        /// </summary>
        internal virtual int PointNumber { get; set; } = 1;

        /// <summary>
        ///     Gets or sets the current PDAT sample string.
        /// </summary>
        internal virtual string CachedPdat { get; set; } = string.Empty;

        /// <summary>
        ///     Gets or sets the current LDAT sample string.
        /// </summary>
        internal virtual string CachedLdat { get; set; } = string.Empty;

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            KukaProcessorSwitches.SetDatFileOutput(this.CellSettingsNode, "1");
            base.RunBeforeProgramOutput();
            this.InitializeKrcVersion();
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            //// LC move example with external axes
            //// DAT File:
            ////    DECL E6POS XP1={X 0.9069,Y 0.6666,Z 0.1768,A -108.1324,B -13.4650,C 169.6304,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            ////    DECL FDAT FP1={TOOL_NO 1,BASE_NO 1,IPO_FRAME #BASE,POINT2[] " ",TQ_STATE FALSE}
            this.DatOut.WriteLine(
                "DECL E6POS XP{0}={{{1}{2}}}\r\n" +
                "DECL FDAT FP{0}={{TOOL_NO {3},BASE_NO {4},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}",
                this.PointNumber,
                this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number);

            //// Motion Settings Change
            //// DAT File (KRC2):
            ////    DECL LDAT LCPDAT1={VEL 0.01,ACC 100,APO_DIST 5,APO_FAC 50,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0}
            //// DAT File (KRC4):
            ////    DECL LDAT LCPDAT1={VEL 0.01,ACC 100,APO_DIST 5,APO_FAC 50,AXIS_VEL 35,AXIS_ACC 100,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0,GEAR_JERK 50.0,EXAX_IGN 0}
            if (this.FormatLcpdat(operationNode, point) != this.CachedLdat)
            {
                this.LdatNumber++;
                this.CachedLdat = this.FormatLcpdat(operationNode, point); // We need to update the CachedLdat after the LdatNumber increment
                this.DatOut.WriteLine(this.CachedLdat);
            }

            //// SRC File (KRC2):
            ////    ;FOLD LIN P1 CONT Vel=1.5 m/s CPDAT1 Tool[1] Base[1];%{PE}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P1, 3:C_DIS, 5:1.5, 7:CPDAT1
            ////    $BWDSTART=FALSE
            ////    LDAT_ACT=LCPDAT1
            ////    FDAT_ACT=FP1
            ////    BAS(#CP_PARAMS,1.5)
            ////    LIN XP1 C_DIS
            ////    ;ENDFOLD
            //// SRC File (KRC4):
            ////    ;FOLD LIN P1 CONT Vel=0.01 m/s CPDAT1 Tool[1] Base[1];%{PE}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P1, 3:C_DIS C_DIS, 5:0.01, 7:CPDAT1
            ////    $BWDSTART=FALSE
            ////    LDAT_ACT=LCPDAT1
            ////    FDAT_ACT=FP1
            ////    BAS(#CP_PARAMS,0.01)
            ////    LIN XP1 C_DIS C_DIS
            ////    ;ENDFOLD
            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    this.Moves.WriteLine(
                        ";FOLD LIN P{0}{1} Vel={2} m/s CPDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P{0}, 3:{6}, 5:{2}, 7:CPDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "LDAT_ACT=LCPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#CP_PARAMS,{2})\r\n" +
                        "LIN XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                        point.Feedrate().LinearFeedrate.VelCpFormatted(),
                        this.LdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                    break;
                case KukaKrcVersion.Krc4V83:
                    this.Moves.WriteLine(
                        ";FOLD LIN P{0}{1} Vel={2} m/s CPDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VLIN,%P 1:LIN, 2:P{0}, 3:{6}, 5:{2}, 7:CPDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "LDAT_ACT=LCPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#CP_PARAMS,{2})\r\n" +
                        "LIN XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                        point.Feedrate().LinearFeedrate.VelCpFormatted(),
                        this.LdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS C_DIS" : string.Empty);
                    break;
            }

            this.PointNumber++;
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            //// CC move example with external axes
            //// DAT File:
            ////    DECL E6POS XP6={X -110.3553,Y -85.3553,Z 0.0000,A 45.0000,B 0.0000,C 180.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            ////    DECL E6POS XP7={X -75.0000,Y -100.0000,Z 0.0000,A 45.0000,B 0.0000,C 180.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            ////    DECL FDAT FP7={TOOL_NO 1,BASE_NO 1,IPO_FRAME #BASE,POINT2[] "P7",TQ_STATE FALSE}
            if (point.IsArcMiddlePoint())
            {
                // Arc middle point move
                this.DatOut.WriteLine(
                    "DECL E6POS XP{0}={{{1}{2}}}",
                    this.PointNumber,
                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty);
            }
            else
            {
                // Arc end point
                this.DatOut.WriteLine(
                    "DECL E6POS XP{0}={{{1}{2}}}\r\n" +
                    "DECL FDAT FP{0}={{TOOL_NO {3},BASE_NO {4},IPO_FRAME #BASE,POINT2[] \"P{0}\",TQ_STATE FALSE}}",
                    this.PointNumber,
                    this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                    this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                    OperationManager.GetTcpId(operationNode),
                    OperationManager.GetUserFrame(operationNode).Number);

                //// Motion Settings Change
                //// DAT File (KRC2):
                ////    DECL LDAT LCPDAT1={VEL 0.01,ACC 100,APO_DIST 5,APO_FAC 50,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0}
                //// DAT File (KRC4):
                ////    DECL LDAT LCPDAT1={VEL 0.01,ACC 100,APO_DIST 5,APO_FAC 50,AXIS_VEL 35,AXIS_ACC 100,ORI_TYP #VAR,CIRC_TYP #BASE,JERK_FAC 50.0,GEAR_JERK 50.0,EXAX_IGN 0}
                if (this.FormatLcpdat(operationNode, point) != this.CachedLdat)
                {
                    this.LdatNumber++;
                    this.CachedLdat = this.FormatLcpdat(operationNode, point); // We need to update the CachedLdat after the LdatNumber increment
                    this.DatOut.WriteLine(this.CachedLdat);
                }

                //// SRC File (KRC2):
                ////    ;FOLD CIRC P6 P7 CONT Vel=0.01 m/s CPDAT1 Tool[1] Base[1];%{PE}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P6, 3:P7, 4:C_DIS, 6:0.01, 8:CPDAT1
                ////    $BWDSTART=FALSE
                ////    LDAT_ACT=LCPDAT1
                ////    FDAT_ACT=FP7
                ////    BAS(#CP_PARAMS,0.01)
                ////    CIRC XP6, XP7 C_DIS
                ////    ;ENDFOLD
                //// SRC File (KRC4):
                ////    ;FOLD CIRC P6 P7 CONT Vel=0.01 m/s CPDAT1 Tool[1] Base[1];%{PE}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P6, 3:P7, 4:C_DIS C_DIS, 6:0.01, 8:CPDAT1
                ////    $BWDSTART=FALSE
                ////    LDAT_ACT=LCPDAT1
                ////    FDAT_ACT=FP7
                ////    BAS(#CP_PARAMS,0.01)
                ////    CIRC XP6,XP7 C_DIS C_DIS
                ////    ;ENDFOLD
                switch (this.KrcVersion)
                {
                    case KukaKrcVersion.Krc2V55:
                        this.Moves.WriteLine(
                            ";FOLD CIRC P{0} P{1}{2} Vel={3} m/s CPDAT{4} Tool[{5}] Base[{6}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:{7}, 6:{3}, 8:CPDAT{4}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{4}\r\n" +
                            "FDAT_ACT=FP{1}\r\n" +
                            "BAS(#CP_PARAMS,{3})\r\n" +
                            "CIRC XP{0},XP{1} {7}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber - 1,
                            this.PointNumber,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                        break;
                    case KukaKrcVersion.Krc4V83:
                        this.Moves.WriteLine(
                            ";FOLD CIRC P{0} P{1}{2} Vel={3} m/s CPDAT{4} Tool[{5}] Base[{6}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VCIRC,%P 1:CIRC, 2:P{0}, 3:P{1}, 4:{7}, 6:{3}, 8:CPDAT{4}\r\n" +
                            "$BWDSTART=FALSE\r\n" +
                            "LDAT_ACT=LCPDAT{4}\r\n" +
                            "FDAT_ACT=FP{1}\r\n" +
                            "BAS(#CP_PARAMS,{3})\r\n" +
                            "CIRC XP{0},XP{1} {7}\r\n" +
                            ";ENDFOLD",
                            this.PointNumber - 1,
                            this.PointNumber,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                            point.Feedrate().LinearFeedrate.VelCpFormatted(),
                            this.LdatNumber,
                            OperationManager.GetTcpId(operationNode),
                            OperationManager.GetUserFrame(operationNode).Number,
                            KukaInlineMotionSettings.GetLinearPositioningType(operationNode) == 1 ? "C_DIS C_DIS" : string.Empty);
                        break;
                }
            }

            this.PointNumber++;
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            //// JC move example with external axes
            //// DAT File:
            ////    DECL E6POS XP2={X -125.0000,Y 100.0000,Z 100.0000,A 45.0000,B 0.0000,C 180.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0,S 2,T 10}
            ////    DECL FDAT FP2={TOOL_NO 1,BASE_NO 1,IPO_FRAME #BASE,POINT2[] " ",TQ_STATE FALSE}
            this.DatOut.WriteLine(
                "DECL E6POS XP{0}={{{1}{2}{3}}}\r\n" +
                "DECL FDAT FP{0}={{TOOL_NO {4},BASE_NO {5},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}",
                this.PointNumber,
                this.FormatPosition(point, operationNode) + "," + this.FormatOrientation(point, operationNode),
                this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                this.FormatStatusAndTurnBits(point, operationNode),
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number);

            //// Motion Settings Change
            //// DAT File (KRC2):
            ////    DECL PDAT PPDAT1={VEL 35,ACC 100,APO_DIST 23}
            //// DAT File (KRC4):
            ////    DECL PDAT PPDAT1={VEL 35,ACC 100,APO_DIST 23,APO_MODE #CPTP,GEAR_JERK 50}
            if (this.FormatPpdat(operationNode) != this.CachedPdat)
            {
                this.PdatNumber++;
                this.CachedPdat = this.FormatPpdat(operationNode); // We need to update the CachedPdat after the PdatNumber increment
                this.DatOut.WriteLine(this.CachedPdat);
            }

            //// SRC File (KRC2):
            ////    ;FOLD PTP P2 CONT Vel=35 % PDAT1 Tool[1] Base[1];%{PE}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P2, 3:C_PTP, 5:35, 7:PDAT1
            ////    $BWDSTART=FALSE
            ////    PDAT_ACT=PPDAT1
            ////    FDAT_ACT=FP2
            ////    BAS(#PTP_PARAMS,35)
            ////    PTP XP2 C_PTP
            ////    ;ENDFOLD
            //// SRC File (KRC4):
            ////    ;FOLD PTP P2 CONT Vel=35 % PDAT1 Tool[1] Base[1];%{PE}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P2, 3:C_DIS, 5:35, 7:PDAT1
            ////    $BWDSTART=FALSE
            ////    PDAT_ACT=PPDAT1
            ////    FDAT_ACT=FP2
            ////    BAS(#PTP_PARAMS,35)
            ////    PTP XP2 C_DIS
            ////    ;ENDFOLD
            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    this.Moves.WriteLine(
                        ";FOLD PTP P{0}{1} Vel={2} % PDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P{0}, 3:{6}, 5:{2}, 7:PDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "PDAT_ACT=PPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#PTP_PARAMS,{2})\r\n" +
                        "PTP XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        KukaInlineMotionSettings.GetJointPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        this.PdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        KukaInlineMotionSettings.GetJointPositioningType(operationNode) == 1 ? "C_PTP" : string.Empty);
                    break;
                case KukaKrcVersion.Krc4V83:
                    this.Moves.WriteLine(
                        ";FOLD PTP P{0}{1} Vel={2} % PDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P{0}, 3:{6}, 5:{2}, 7:PDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "PDAT_ACT=PPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#PTP_PARAMS,{2})\r\n" +
                        "PTP XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        KukaInlineMotionSettings.GetJointPositioningType(operationNode) == 1 ? " CONT" : string.Empty,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        this.PdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        KukaInlineMotionSettings.GetJointPositioningType(operationNode) == 1 ? "C_DIS" : string.Empty);
                    break;
            }

            this.PointNumber++;
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            //// JJ move example with external axes
            //// DAT File:
            ////    DECL E6AXIS XP1={A1 0.0000,A2 -90.0000,A3 90.0000,A4 0.0000,A5 0.0000,A6 0.0000,E1 1000.0000,E2 0.0,E3 0.0,E4 0.0,E5 0.0,E6 0.0}
            ////    DECL FDAT FP1={TOOL_NO 1,BASE_NO 1,IPO_FRAME #BASE,POINT2[] " ",TQ_STATE FALSE}
            this.DatOut.WriteLine(
                "DECL E6AXIS XP{0}={{{1}{2}}}\r\n" +
                "DECL FDAT FP{0}={{TOOL_NO {3},BASE_NO {4},IPO_FRAME #BASE,POINT2[] \" \",TQ_STATE FALSE}}",
                this.PointNumber,
                this.FormatJointValues(point),
                this.ExternalAxesJoints.Any() ? "," + this.FormatExternalAxesValues(point) : string.Empty,
                OperationManager.GetTcpId(operationNode),
                OperationManager.GetUserFrame(operationNode).Number);

            //// Motion Settings Change
            //// DAT File (KRC2):
            ////    DECL PDAT PPDAT1={VEL 35,ACC 100,APO_DIST 23}
            //// DAT File (KRC4):
            ////    DECL PDAT PPDAT1={VEL 35,ACC 100,APO_DIST 23,APO_MODE #CPTP,GEAR_JERK 50}
            if (this.FormatPpdat(operationNode) != this.CachedPdat)
            {
                this.PdatNumber++;
                this.CachedPdat = this.FormatPpdat(operationNode); // We need to update the CachedPdat after the PdatNumber increment
                this.DatOut.WriteLine(this.CachedPdat);
            }

            //// SRC File (KRC2):
            ////    ;FOLD PTP P1  Vel=35 % PDAT1 Tool[1] Base[1];%{PE}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P1, 3:, 5:35, 7:PDAT1
            ////    $BWDSTART=FALSE
            ////    PDAT_ACT=PPDAT1
            ////    FDAT_ACT=FP1
            ////    BAS(#PTP_PARAMS,35)
            ////    PTP XP1
            ////    ;ENDFOLD
            //// SRC File (KRC4):
            ////    ;FOLD PTP P1  Vel=35 % PDAT1 Tool[1] Base[1];%{PE}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P1, 3:, 5:35, 7:PDAT1
            ////    $BWDSTART=FALSE
            ////    PDAT_ACT=PPDAT1
            ////    FDAT_ACT=FP1
            ////    BAS(#PTP_PARAMS,35)
            ////    PTP XP1
            ////    ;ENDFOLD
            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    this.Moves.WriteLine(
                        ";FOLD PTP P{0}{1} Vel={2} % PDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 5.5.31,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P{0}, 3:{6}, 5:{2}, 7:PDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "PDAT_ACT=PPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#PTP_PARAMS,{2})\r\n" +
                        "PTP XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        string.Empty,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        this.PdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        string.Empty); // JJ output forced to Exact, like in V6
                    break;
                case KukaKrcVersion.Krc4V83:
                    this.Moves.WriteLine(
                        ";FOLD PTP P{0}{1} Vel={2} % PDAT{3} Tool[{4}] Base[{5}];%{{PE}}%R 8.3.40,%MKUKATPBASIS,%CMOVE,%VPTP,%P 1:PTP, 2:P{0}, 3:{6}, 5:{2}, 7:PDAT{3}\r\n" +
                        "$BWDSTART=FALSE\r\n" +
                        "PDAT_ACT=PPDAT{3}\r\n" +
                        "FDAT_ACT=FP{0}\r\n" +
                        "BAS(#PTP_PARAMS,{2})\r\n" +
                        "PTP XP{0} {6}\r\n" +
                        ";ENDFOLD",
                        this.PointNumber,
                        string.Empty,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        this.PdatNumber,
                        OperationManager.GetTcpId(operationNode),
                        OperationManager.GetUserFrame(operationNode).Number,
                        string.Empty); // JJ output forced to Exact, like in V6
                    break;
            }

            this.PointNumber++;
        }

        /// <inheritdoc />
        internal override void OutputMotionSettingsBlocks(CbtNode operationNode, CbtNode previousOperationNode)
        {
            //// Overridden for Kuka Inline
        }

        /// <summary>
        ///     Formats the LCPDAT output at a given point.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted LCPDAT.</returns>
        internal virtual string FormatLcpdat(CbtNode operationNode, PathNode point)
        {
            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    return string.Format(
                        "DECL LDAT LCPDAT{0}={{VEL {1},ACC {2},APO_DIST {3},APO_FAC {4},ORI_TYP {5},CIRC_TYP {6},JERK_FAC {7}}}",
                        this.LdatNumber,
                        point.Feedrate().LinearFeedrate.VelCpFormatted(),
                        KukaInlineMotionSettings.GetTcpAcceleration(operationNode),
                        KukaInlineMotionSettings.GetApproximationDistanceForLinearAndCircularMove(operationNode),
                        "50",
                        this.GetOrientationControlTypeForLinearAndCircularMove(operationNode),
                        "#BASE", // Hard-coded for now
                        "50.0"); // Hard-coded for now
                case KukaKrcVersion.Krc4V83:
                    return string.Format(
                        "DECL LDAT LCPDAT{0}={{VEL {1},ACC {2},APO_DIST {3},APO_FAC {4},AXIS_VEL {5},AXIS_ACC {6},ORI_TYP {7},CIRC_TYP {8},JERK_FAC {9},GEAR_JERK {10},EXAX_IGN {11}}}",
                        this.LdatNumber,
                        point.Feedrate().LinearFeedrate.VelCpFormatted(),
                        KukaInlineMotionSettings.GetTcpAcceleration(operationNode),
                        KukaInlineMotionSettings.GetApproximationDistanceForLinearAndCircularMove(operationNode),
                        "50", // Hard-coded for now
                        KukaInlineMotionSettings.GetJointAxisVelocityForLinearAndCircularMove(operationNode),
                        KukaInlineMotionSettings.GetJointAxisAccelerationForLinearAndCircularMove(operationNode),
                        this.GetOrientationControlTypeForLinearAndCircularMove(operationNode),
                        "#BASE", // Hard-coded for now
                        "50.0", // Hard-coded for now
                        "50.0", // Hard-coded for now
                        "0"); // Hard-coded for now
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Formats the PPDAT output at a given operation.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted PPDAT.</returns>
        internal virtual string FormatPpdat(CbtNode operationNode)
        {
            switch (this.KrcVersion)
            {
                case KukaKrcVersion.Krc2V55:
                    return string.Format(
                        "DECL PDAT PPDAT{0}={{VEL {1},ACC {2},APO_DIST {3}}}",
                        this.PdatNumber,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        KukaInlineMotionSettings.GetJointAxisAccelerationForJointMove(operationNode),
                        KukaInlineMotionSettings.GetApproximationDistanceForJointMove(operationNode));
                case KukaKrcVersion.Krc4V83:
                    return string.Format(
                        "DECL PDAT PPDAT{0}={{VEL {1},ACC {2},APO_DIST {3},APO_MODE {4},GEAR_JERK {5}}}",
                        this.PdatNumber,
                        KukaInlineMotionSettings.GetJointAxisVelocityForJointMove(operationNode),
                        KukaInlineMotionSettings.GetJointAxisAccelerationForJointMove(operationNode),
                        KukaInlineMotionSettings.GetApproximationDistanceForJointMove(operationNode),
                        "#CPTP", // Hard-coded for now
                        "50"); // Hard-coded for now
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        ///     Initialize the Kuka KRC controller version.
        /// </summary>
        internal virtual void InitializeKrcVersion()
        {
            string configurationName = SetupManager.GetConfiguration(this.SetupNode).Name;

            if (new Regex("_KRC2_V5_5|_KRC2").IsMatch(configurationName))
            {
                // Specific for KSS 5.5
                this.KrcVersion = KukaKrcVersion.Krc2V55;
            }
            else if (new Regex("_KRC4_V8_3|_KRC4").IsMatch(configurationName))
            {
                // Specific for KSS 8.3
                this.KrcVersion = KukaKrcVersion.Krc4V83;
            }
            else
            {
                // Fall-back
                this.KrcVersion = KukaKrcVersion.Krc4V83;
            }
        }

        /// <summary>
        ///     Gets the orientation control of a CP motion in the advance run.
        /// </summary>
        /// <param name="operationNode">Current operation.</param>
        internal virtual string GetOrientationControlTypeForLinearAndCircularMove(CbtNode operationNode)
        {
            switch (KukaInlineMotionSettings.GetOrientationControlTypeForLinearAndCircularMove(operationNode))
            {
                case 1:
                    return "#VAR";
                case 2:
                    return "#JOINT";
                default:
                    return string.Empty;
            }
        }
    }
}