﻿// <copyright file="KukaCamRobProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaCamRob.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Kuka.KukaCamRob.PostProcessor;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Kuka CamRob process.
    /// </summary>
    [DataContract(Name = "KukaCamRobProcess")]
    public class KukaCamRobProcess : PackageProcess, IKukaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="KukaCamRobProcess"/> class.
        /// </summary>
        public KukaCamRobProcess()
        {
            this.Name = "Kuka CamRob Process";
            this.PostProcessorType = typeof(PostProcessorKukaCamRob);
            this.ProcessMenuFileName = "KukaCamRobProcessorSwitches";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
