﻿// <copyright file="PostProcessorKukaCamRob.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Kuka.KukaCamRob.PostProcessor
{
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Kuka.Default.PostProcessor;
    using Robotmaster.Processor.Kuka.KukaCamRob.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Operation.Types;

    internal class PostProcessorKukaCamRob : PostProcessorKuka
    {
        /// <summary>
        ///     Gets or sets Record number.
        /// </summary>
        internal virtual int RecordNumber { get; set; }

        /// <summary>
        ///     Gets or sets CNC line number.
        /// </summary>
        internal virtual int CncLineNumber { get; set; }

        /// <summary>
        ///     Gets or sets the CR_rPARAMS[] array;.
        /// </summary>
        /// <remarks>
        ///     To match Kuka notation CR_rPARAMS[0] will not be used.
        /// </remarks>
        /// <remarks>
        ///     Represents the Kuka CAMRob user defined parameters.
        ///     In addition to the necessary position data, the CAMRob data file can contain up to 26 user parameters.
        ///     On the controller these 26 user parameters are found in the CR_rPARAMS[] array.
        ///     In this way, process parameter settings can be set for each position in the data file.
        ///     These parameters are read together with the position data, and executed when starting the motion to the according position.
        ///     These process parameters are optional and may be omitted.
        /// </remarks>
        internal virtual string[] CrRparams { get; set; } = new string[26 + 1];

        // Determines if the data file was loaded for the specific operation in the SRC
        internal virtual bool IsPointOutputInCsv { get; set; } = false;

        /// <summary>
        ///     Gets directly into the main section of the CSV file.
        /// </summary>
        /// <param name="childNum">Child index number.</param>
        /// <returns>CSV StreamWriter.</returns>
        internal virtual StreamWriter CsvOut(int childNum)
        {
            return this.CurrentPostFile.Children[childNum - 1].FileSection.StreamWriter;
        }

        /// <inheritdoc/>
        internal override void RunBeforeProgramOutput()
        {
            this.VerifyExternalAxesId();

            CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            PathNode firstPoint = OperationManager.GetFirstPoint(firstOperation);

            this.InitializeToolChangeOutputParameters();

            //// no multi-file ouput of .SRC in a single programNode
            //// no sub-programNode exist
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_");
            this.CurrentPostFile.FileExtension = FormatManagerKuka.MainProgramExtension;
            this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);
        }

        /// <inheritdoc/>
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            base.RunBeforeOperationOutput(operationNode);
            this.SetOperationCamRobUserParameters(operationNode); // operation level parameters
        }

        /// <inheritdoc/>
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.SetPointCamRobUserParameters(point); // point level parameters

            if (!this.IsPointOutputInCsv && point.MoveType() == MoveType.Linear)
            {
                string fileStringFormat = KukaCamRobProcessorSwitches.GetFileFormat(this.SetupNode) == 0 ? "CSV" : "BIN";

                // Load CAMRob.ProcessFile file
                this.Moves.WriteLine("CR_PROCESS_FILE(" + "\"" +
                                     this.CurrentPostFile.Children[this.TaskOperationNumber - 1].FileName + "." +
                                     fileStringFormat + "\"" + ", 1, -1, " + KukaCamRobProcessorSwitches.GetChecksum(this.SetupNode) + ")");
                this.IsPointOutputInCsv = true;
            }
        }

        /// <inheritdoc/>
        internal override void RunAfterOperationOutput(CbtNode operationNode)
        {
            this.RecordNumber = 0; // reset record number for next CSV file
            this.CncLineNumber = 0; // reset CNC line number for next CSV file
            this.IsPointOutputInCsv = false; // reset variable for next operation
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formating following <see cref="KukaCamRob"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>).
        ///     Populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <param name="currentPoint">Current point.</param>
        internal override void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode currentPoint)
        {
            base.StartPostFile(postFile, operationNode, currentPoint);

            postFile.FileSection.SubFileSections[0].Header += "; Set the version for the user param source files:" +
                                                              "\r\n" + "CR_iUSER_PARAMS_VERSION = " +
                                                              KukaCamRobProcessorSwitches.GetCrIUserParamsVersion(this.SetupNode) + "\r\n" + "\r\n";

            int numOfTaskOperations = ProgramManager.GetOperationNodes(this.ProgramNode).Count(op => op.GetComponent<OperationType>() == OperationType.Task);

            if (numOfTaskOperations == 1)
            {
                // Create a single .CSV
                var csvFile = new PostFile(); // .CSV
                postFile.AddChild(csvFile); // Add .CSV to .SRC
                csvFile.FileName = postFile.FileName;
                csvFile.FileExtension = ".CSV";
            }
            else
            {
                // Create multiple .CSV file (CSV file per task operation)
                for (var i = 1; i <= numOfTaskOperations; i++)
                {
                    var csvFile = new PostFile(); // .CSV
                    postFile.AddChild(csvFile); // Add .CSV to .SRC
                    csvFile.FileName = postFile.FileName + i;
                    csvFile.FileExtension = ".CSV";
                }
            }
        }

        /// <inheritdoc/>
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            //// Move example in CSV format
            //// RecordNumber;X;Y;Z;A;B;C;P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13;P14;P15;P16;P17;P18;P19;P20;P21;P22;P23;P24;P25;P26
            if (KukaCamRobProcessorSwitches.GetFileFormat(this.SetupNode) == 0)
            {
                // RecordNumber;X;Y;Z;A;B;C;
                this.CsvOut(this.TaskOperationNumber).Write((++this.RecordNumber) + ";" +
                                                            this.FormatPositionCamRobCsv(point, operationNode) +
                                                            this.FormatOrientationCamRobCsv(point, operationNode));

                // P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13;P14;P15;P16;P17;P18;P19;P20;P21;P22;P23;P24;P25;P26
                this.CsvOut(this.TaskOperationNumber).WriteLine(this.FormatCrUserParams());
            }
        }

        /// <inheritdoc/>
        internal override void OutputCircularMove(PathNode currentPoint, CbtNode operationNode)
        {
            // There is no CC move in KukaCAMRob. Must convert CC to LC for posting. Do not post program.
            //// MessageWindow.Show("Kuka CAMRob does not support circular cartesian moves ! Posting will be aborted.", "Abort");
           this.OutputLinearMove(currentPoint, operationNode); // TODO This must be fixed, we need to find a way to convert CC to LC
        }

        /// <summary>
        ///     Formats position output using CAMRob CSV format.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Kuka CAMRob CSV formatted orientation.</returns>
        internal virtual string FormatPositionCamRobCsv(PathNode point, CbtNode operationNode)
        {
            // Gets the XYZ in user frame
            Vector3 pointPositionInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot).Position;

            // X;Y;Z;
            return pointPositionInUserFrame.X.Formatted() + ";" + pointPositionInUserFrame.Y.Formatted() + ";" + pointPositionInUserFrame.Z.Formatted() + ";";
        }

        /// <summary>
        ///     Formats orientation output using CAMRob CSV format.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Kuka CAMRob CSV formatted orientation.</returns>
        internal virtual string FormatOrientationCamRobCsv(PathNode point, CbtNode operationNode)
        {
            // Gets the euler angles in user frame
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            // A;B;C;
            return euler[0].Formatted() + ";" + euler[1].Formatted() + ";" + euler[2].Formatted() + ";";
        }

        /// <summary>
        ///     Updates the user defined process parameters at the operation level.
        /// </summary>
        /// <remarks>
        ///     Initializes user defined parameters that are not present during NC import.
        /// </remarks>
        /// <param name="operationNode">Current operation.</param>
        internal virtual void SetOperationCamRobUserParameters(CbtNode operationNode)
        {
            //// P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13;P14;P15;P16;P17;P18;P19;P20;P21;P22;P23;P24;P25;P26
            switch (KukaCamRobProcessorSwitches.GetCrIUserParamsVersion(this.SetupNode))
            {
                case 0: // KUKA.CAMRob KRC V2.0
                    //// this.CrRparams[1] = // Defined at point level
                    //// this.CrRparams[2] = // Defined at point level
                    this.CrRparams[3] = KukaCamRobProcessorSwitches.GetSpindleState(this.SetupNode).ToString();
                    this.CrRparams[4] = KukaCamRobProcessorSwitches.GetSpindleSpeedInRpm(this.SetupNode).ToString(CultureInfo.InvariantCulture);
                    this.CrRparams[5] = "0"; // Hard coded spindle rotation to CLW
                    this.CrRparams[6] = KukaCamRobProcessorSwitches.GetCoolantState(this.SetupNode).ToString();
                    this.CrRparams[7] = OperationManager.GetTcpId(operationNode).ToString();
                    //// this.CrRparams[8] = // Defined at point level
                    //// this.CrRparams[9] = // Defined at point level
                    this.CrRparams[10] = this.CachedRobotConfig.WristConfiguration == WristConfig.Positive ? "1" : "0"; // TODO verify if this makes sense. Mapping is different. 0 or 2=WristBack-PTP, 1 or 3=WristForward-PTP
                    this.CrRparams[11] = OperationManager.GetUserFrame(operationNode).Number.ToString();
                    this.CrRparams[12] = KukaCamRobProcessorSwitches.GetCustomEndEffectTool(this.SetupNode).ToString();
                    this.CrRparams[13] = OperationManager.GetPointCount(operationNode).ToString();
                    break;
                case 1: // KUKA.CAMRob KRC V2.1, V2.3 or V3.0
                    //// this.CrRparams[1] = // Defined at point level
                    //// this.CrRparams[2] = // Defined at point level
                    this.CrRparams[3] = KukaCamRobProcessorSwitches.GetSpindleState(this.SetupNode).ToString();
                    this.CrRparams[4] = KukaCamRobProcessorSwitches.GetSpindleSpeedInRpm(this.SetupNode).ToString(CultureInfo.InvariantCulture);
                    this.CrRparams[5] = "0"; // Hard coded spindle rotation to CLW
                    this.CrRparams[6] = KukaCamRobProcessorSwitches.GetCoolantState(this.SetupNode).ToString();
                    this.CrRparams[7] = OperationManager.GetTcpId(operationNode).ToString();
                    this.CrRparams[8] = KukaMotionSettings.GetTcpPathAcceleration(operationNode).ToString(CultureInfo.InvariantCulture);
                    //// this.CrRparams[9] = // Defined at point level
                    //// this.CrRparams[10] = // Defined at point level
                    //// this.CrRparams[11] = // Defined at point level
                    //// this.CrRparams[12] = // Defined at point level
                    //// this.CrRparams[13] = // Defined at point level
                    //// this.CrRparams[14] = // Defined at point level
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        ///     Updates the user defined process parameters at the point level.
        /// </summary>
        /// <remarks>
        ///     Initializes user defined parameters that are not present during NC import.
        /// </remarks>
        /// <param name="point">Current Point.</param>
        internal virtual void SetPointCamRobUserParameters(PathNode point)
        {
            //// P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13;P14;P15;P16;P17;P18;P19;P20;P21;P22;P23;P24;P25;P26
            switch (KukaCamRobProcessorSwitches.GetCrIUserParamsVersion(this.SetupNode))
            {
                case 0: // KUKA.CAMRob KRC V2.0
                    this.CrRparams[1] = (++this.CncLineNumber).ToString();
                    this.CrRparams[2] = point.Feedrate().LinearFeedrate.VelCpFormatted();
                    //// this.CrRparams[3] = // Defined at operation level
                    //// this.CrRparams[4] = // Defined at operation level
                    //// this.CrRparams[5] = // Defined at operation level
                    //// this.CrRparams[6] = // Defined at operation level
                    //// this.CrRparams[7] = // Defined at operation level
                    this.CrRparams[8] = !this.JointFormat.TryGetValue("E1", out int e1) ? "0.0" : point.JointValue(e1).Formatted(); // User has to map rail to E1
                    this.CrRparams[9] = !this.JointFormat.TryGetValue("E2", out int e2) ? "0.0" : point.JointValue(e2).Formatted(); // User has to map rotary to E2
                    //// this.CrRparams[10] = // Defined at operation level
                    //// this.CrRparams[11] = // Defined at operation level
                    //// this.CrRparams[12] = // Defined at operation level
                    //// this.CrRparams[13] = // Defined at operation level
                    break;
                case 1: // KUKA.CAMRob KRC V2.1, V2.3 or V3.0
                    this.CrRparams[1] = (++this.CncLineNumber).ToString();
                    this.CrRparams[2] = point.Feedrate().LinearFeedrate.VelCpFormatted();
                    //// this.CrRparams[3] = // Defined at operation level
                    //// this.CrRparams[4] = // Defined at operation level
                    //// this.CrRparams[5] = // Defined at operation level
                    //// this.CrRparams[6] = // Defined at operation level
                    //// this.CrRparams[7] = // Defined at operation level
                    //// this.CrRparams[8] = // Defined at operation level
                    this.CrRparams[9] = !this.JointFormat.TryGetValue("E1", out int e1I) ? "0.0" : point.JointValue(e1I).Formatted();
                    this.CrRparams[10] = !this.JointFormat.TryGetValue("E2", out int e2I) ? "0.0" : point.JointValue(e2I).Formatted();
                    this.CrRparams[11] = !this.JointFormat.TryGetValue("E3", out int e3) ? "0.0" : point.JointValue(e3).Formatted();
                    this.CrRparams[12] = !this.JointFormat.TryGetValue("E4", out int e4) ? "0.0" : point.JointValue(e4).Formatted();
                    this.CrRparams[13] = !this.JointFormat.TryGetValue("E5", out int e5) ? "0.0" : point.JointValue(e5).Formatted();
                    this.CrRparams[14] = !this.JointFormat.TryGetValue("E6", out int e6) ? "0.0" : point.JointValue(e6).Formatted();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        ///     Formats the CrUserParams array containing all the user defined process parameters.
        /// </summary>
        /// <returns>Formatted Kuka CAMRob user defined process parameters.</returns>
        /// <remarks>
        ///     The programs generated by KUKA.CAMRob PC 2.0 make use of the "cr_user_params.src" user routine.
        ///     The CAMRob program uses this version whenever the variable “CR_iUser_Params_Version” is set to 1.
        ///     When adding process parameters P1 to P26, make sure the CR_rPARAMS[] variables in the robot controller are defined (projected) properly.
        /// </remarks>
        /// <remarks>
        ///     The programs generated by KUKA.CAMRob PC 2.1, 2.3 and 3.0 make use of the "cr_user_params_adv.src" and "cr_user_params_trig.src" user routine.
        ///     The CAMRob program uses this version whenever the variable “CR_iUser_Params_Version” is set to 2.
        ///     When adding process parameters P1 to P26, make sure the CR_rPARAMS[] variables in the robot controller are defined (projected) properly.
        /// </remarks>
        internal virtual string FormatCrUserParams()
        {
            switch (KukaCamRobProcessorSwitches.GetCrIUserParamsVersion(this.SetupNode))
            {
                case 0: // KUKA.CAMRob KRC V2.0
                    //// P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13
                    return string.Join(";", this.CrRparams.Skip(1).Take(13));

                case 1: // KUKA.CAMRob KRC V2.1, V2.3 or V3.0
                    if (!this.ExternalAxesJoints.Any())
                    {
                        //// P1;P2;P3;P4;P5;P6;P7;P8
                        return string.Join(";", this.CrRparams.Skip(1).Take(8));
                    }
                    else
                    {
                        //// P1;P2;P3;P4;P5;P6;P7;P8;P9;P10;P11;P12;P13;P14
                        return string.Join(";", this.CrRparams.Skip(1));
                    }

                default:
                    return string.Empty;
            }
        }
    }
}
