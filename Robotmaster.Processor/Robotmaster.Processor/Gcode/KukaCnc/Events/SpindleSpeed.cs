﻿// <copyright file="SpindleSpeed.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Gcode.KukaCnc.Events
{
    using System.Runtime.Serialization;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// Spindle Output event.
    /// </summary>
    [DataContract(Name = "SpindleSpeed")]
    public class SpindleSpeed : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpindleSpeed" /> class.
        /// Default constructor is required for MET Add.
        /// </summary>
        public SpindleSpeed()
        {
            this.EventCalculationType = EventCalculationType.Instantaneous;
        }

        /// <summary>
        /// Gets or sets the Spindle Output command.
        /// </summary>
        [DataMember(Name = "SpindleSpeedCommand")]
        public virtual string SpindleSpeedCommand { get; set; } = string.Empty;

        /// <summary>
        /// Override of GetPointDisplayFormat method.
        /// </summary>
        /// <returns>The information that will be output in the point list.</returns>
        public override string ToString()
        {
            return $"{this.SpindleSpeedCommand}";
        }
    }
}
