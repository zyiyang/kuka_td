﻿// <copyright file="MainProcessorKukaCnc.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Gcode.KukaCnc.MainProcessor
{
    using System.Collections.Generic;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path;
    using Robotmaster.PathEngine;
    using Robotmaster.PathEngine.Core.Helpers;
    using Robotmaster.Processor.Common.Default.MainProcessor;
    using Robotmaster.Processor.Gcode.KukaCnc.Events;
    using Robotmaster.Processor.Gcode.KukaCnc.ProcessorSwitches;
    using Robotmaster.Processor.Kuka.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.Event;

    /// <inheritdoc />
    /// <summary>
    ///     This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    ///     <para>
    ///         Implements the Kuka.Cnc dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorKukaCnc : MainProcessor
    {
        /// <summary>
        ///     Gets or sets a value indicating whether the SpindleSpeed Event has been already Output.
        /// </summary>
        internal virtual bool IsSpindleSpeedOutput { get; set; }

        /// <inheritdoc />
        internal override void EditOperationBeforeAllPointEdits(CbtNode operationNode)
        {
            this.IsSpindleSpeedOutput = false;

            base.EditOperationBeforeAllPointEdits(operationNode);

            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            // TODO - AT-1048 Replace by an API method
            const string tempString = "_SettingObject_PM_SpindleParametersKukaCnc_DCBEF32FS";

            if ((operationNode.HasComponent(tempString) || (previousOperationNode != null && previousOperationNode.HasComponent(tempString))) && !this.IsSpindleSpeedOutput)
            {
                this.AddSpindleSpeedEvent(operationNode, OperationManager.GetFirstPoint(operationNode));
            }
        }

        /// <inheritdoc />
        internal override void EditFirstOperation(CbtNode operationNode)
        {
            KukaProcessorSwitches.SetToolChangeOutputType(this.CellSettingsNode, "2"); // Output Tool Change Macro = Enable

            this.IsSpindleSpeedOutput = true;

            this.AddToolChangeMacroEvent(operationNode, OperationManager.GetFirstPoint(operationNode));
            this.AddSpindleSpeedEvent(operationNode, OperationManager.GetFirstPoint(operationNode));
        }

        /// <inheritdoc />
        internal override void EditFirstOperationAfterToolChange(CbtNode operationNode)
        {
            //// Tool Change
            if (this.IsOperationInMultiToolProgram)
            {
                this.IsSpindleSpeedOutput = true;

                this.AddToolChangeMacroEvent(operationNode, OperationManager.GetFirstPoint(operationNode));
                this.AddSpindleSpeedEvent(operationNode, OperationManager.GetFirstPoint(operationNode));
            }

            //// Tooling activation / deactivation
            //// with tool change
            if (this.ToolingActivationDeactivationCondition == 2)
            {
                CbtNode previousOperation = OperationManager.GetPreviousOperationNodeInProgram(operationNode);
                if (previousOperation != null)
                {
                    //// Get the path engine of an operation.
                    PathEngine pathEngineFromPreviousOperation = OperationManager.GetPathEngine(previousOperation);
                    //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                    //// Get nodes in reversed order to optimize search.
                    string previousOperationLastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngineFromPreviousOperation);
                    IEnumerable<PathNode> operationPointsFromPreviousOperation = pathEngineFromPreviousOperation.GetNodes(previousOperationLastValidExistingStageKey, true).ToIEnumerable();

                    PathNode pointFromPreviousOperation = operationPointsFromPreviousOperation
                        .FirstOrDefault(lastPoint => this.IsToolDeactivationMove(previousOperation, lastPoint));
                    if (pointFromPreviousOperation != null)
                    {
                        this.EditToolingDeactivationPoint(previousOperation, pointFromPreviousOperation);
                    }
                }

                //// Get the path engine of an operation.
                PathEngine pathEngine = OperationManager.GetPathEngine(operationNode);
                //// Get all the points of path engine at the edit stage (same as point list, no interpolated points).
                string lastValidExistingStageKey = PathStageOrder.GetLastValidExistingStageKey(PathStageKeys.Edit, pathEngine);
                IEnumerable<PathNode> operationPoints = pathEngine.GetNodes(lastValidExistingStageKey, false).ToIEnumerable();

                PathNode point = operationPoints
                    .FirstOrDefault(firstPoint => this.IsToolActivationMove(operationNode, firstPoint));

                if (point != null)
                {
                    this.EditToolingActivationPoint(operationNode, point);
                }
            }
        }

        /// <inheritdoc />
        internal override PathNode AddToolChangeMacroEvent(CbtNode operationNode, PathNode point)
        {
            // Tool name
            this.ToolChangeMacro = "T" + OperationManager.GetTool(operationNode).Number + " M6";

            return base.AddToolChangeMacroEvent(operationNode, point);
        }

        /// <summary>
        ///     Adds SpindleSpeed event to the point.
        /// </summary>
        /// <param name="operationNode">The operation node.</param>
        /// <param name="point">The point.</param>
        /// <returns>The point to which the <see cref="SpindleSpeed"/> has been added.</returns>
        internal virtual PathNode AddSpindleSpeedEvent(CbtNode operationNode, PathNode point)
        {
            double spindleValue = KukaCncSpindleParameters.GetSpindleSpeedInRpm(operationNode);
            string output = string.Empty;

            if (KukaCncSpindleParameters.GetSpindleState(operationNode) == 1)
            {
                if (spindleValue > 0)
                {
                    output = $"M3 S{System.Math.Abs(spindleValue)}";
                }
                else if (spindleValue < 0)
                {
                    output = $"M4 S{System.Math.Abs(spindleValue)}";
                }
                else if (System.Math.Abs(spindleValue) < 0.01)
                {
                    output = $"M5";
                }
            }
            else
            {
                output = $"M5";
            }

            point = PointManager.AddEventToPathPoint(new SpindleSpeed { SpindleSpeedCommand = output }, EventIndex.Before, point, operationNode);
            return point;
        }
    }
}
