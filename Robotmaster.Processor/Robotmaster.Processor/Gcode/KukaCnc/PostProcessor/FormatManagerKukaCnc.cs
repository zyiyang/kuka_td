﻿// <copyright file="FormatManagerKukaCnc.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Gcode.KukaCnc.PostProcessor
{
    /// <summary>
    /// Kuka.Cnc Format Manager.
    /// </summary>
    internal static class FormatManagerKukaCnc
    {
        /// <summary>
        /// Gets extension for the posted program.
        /// </summary>
        internal static string MainProgramExtension => ".NC";
    }
}
