﻿// <copyright file="KukaCncProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Gcode.KukaCnc.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Gcode.KukaCnc.MainProcessor;
    using Robotmaster.Processor.Gcode.KukaCnc.PostProcessor;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines the Kuka.Cnc process.
    /// </summary>
    [DataContract(Name = "KukaCncProcess")]
    public class KukaCncProcess : PackageProcess, IKukaProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KukaCncProcess"/> class.
        /// </summary>
        public KukaCncProcess()
        {
            this.Name = "Kuka.CNC Process";
            this.PostProcessorType = typeof(PostProcessorKukaCnc);
            this.MainProcessorType = typeof(MainProcessorKukaCnc);
            this.ProcessMenuFileName = "KukaCncProcessorSwitches";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
