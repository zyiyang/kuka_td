﻿// <copyright file="ProcessorOptionsBehavior.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor
{
    using System.Collections.Generic;
    using Robotmaster.Behavior;
    using Robotmaster.Bootstrap.Option;

    /// <summary>
    /// Behavior responsible for managing the processor package options in Rise.
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public class ProcessorOptionsBehavior : Behavior
    {
        private List<OptionDescription> optionDescriptions;

        /// <inheritdoc/>
        protected override void OnPlug()
        {
            this.optionDescriptions = new List<OptionDescription>();
            this.CreateProcessorSettings();
            this.AddProcessorSettings();
        }

        /// <inheritdoc/>
        protected override void OnUnplug()
        {
            this.RemoveProcessorSettings();
        }

        [MessageHandler("OPTIONS_CHANGED")]
        private void OnOptionsChanged(Behavior source)
        {
            Properties.Settings.Default.Save();
        }

        private void AddProcessorSettings()
        {
            var optionList = this.Get<List<OptionDescription>>("optionList");
            optionList.AddRange(this.optionDescriptions);
        }

        private void RemoveProcessorSettings()
        {
            var optionList = this.Get<List<OptionDescription>>("optionList");

            foreach (var optionDescription in this.optionDescriptions)
            {
                optionList.Remove(optionDescription);
            }
        }

        private void CreateProcessorSettings()
        {
            this.optionDescriptions.Add(new OptionDescription
            {
                Settings = Properties.Settings.Default,
                Key = nameof(Properties.Settings.Default.DestinationFolder),
                Category = ProcessorResources.ProcessorCategory,
                SubCategory = ProcessorResources.PostProcessorTitle,
                Caption = ProcessorResources.DestinationFolderCaption,
            });

            this.optionDescriptions.Add(new OptionDescription
            {
                Settings = Properties.Settings.Default,
                Key = nameof(Properties.Settings.Default.AlwaysAskOutputDestination),
                Category = ProcessorResources.ProcessorCategory,
                SubCategory = ProcessorResources.PostProcessorTitle,
                Caption = ProcessorResources.AlwaysAskOutputDestinationCaption,
            });

            this.optionDescriptions.Add(new OptionDescription
            {
                Settings = Properties.Settings.Default,
                Key = nameof(Properties.Settings.Default.OpenEditor),
                Category = ProcessorResources.ProcessorCategory,
                SubCategory = ProcessorResources.EditorTitle,
                Caption = ProcessorResources.OpenEditorCaption,
            });

            this.optionDescriptions.Add(new OptionDescription
            {
                Settings = Properties.Settings.Default,
                Key = nameof(Properties.Settings.Default.EditorApplication),
                Category = ProcessorResources.ProcessorCategory,
                SubCategory = ProcessorResources.EditorTitle,
                Caption = ProcessorResources.EditorApplicationCaption,
            });

            this.optionDescriptions.Add(new OptionDescription
            {
                Settings = Properties.Settings.Default,
                Key = nameof(Properties.Settings.Default.DisplayWarningOnFileOverride),
                Category = ProcessorResources.ProcessorCategory,
                SubCategory = ProcessorResources.WarningTitle,
                Caption = ProcessorResources.DisplayWarningOnFileOverrideCaption,
            });
        }
    }
}
