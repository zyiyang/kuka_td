﻿// <copyright file="PostProcessorIgm.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Default.PostProcessor
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Igm.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Processor.Igm.Default.DeviceMenus.ProcessorSwitches;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Operation.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     <see cref="PostProcessorIgm"/> inherits from <see cref="PostProcessor"/>.
    ///     <para>
    ///         Implements the Igm dedicated properties and methods of the post-processor in order to output Igm default robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorIgm : PostProcessor
    {
        /// <summary>
        ///     Gets the current File Header <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter FileHeader => this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter;

        /// <summary>
        ///     Gets the current Moves Section <see cref="FileSection.StreamWriter"/>.
        /// </summary>
        internal virtual StreamWriter Moves => this.CurrentPostFile.FileSection.SubFileSections[1].StreamWriter;

        /// <summary>
        ///     Gets the List of Used Coordinate Systems.
        /// </summary>
        internal virtual List<int> UsedCoordinateSystemsList { get; } = new List<int>();

        /// <summary>
        ///     Gets or sets the Additional Point Data string.
        /// </summary>
        internal virtual string AdditionalPointData { get; set; }

        /// <summary>
        ///     Gets or sets the Step Counter.
        /// </summary>
        internal virtual int StepCounter { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the Is Process Loaded condition is used.
        /// </summary>
        internal virtual bool IsProcessLoaded { get; set; } = false;

        /// <summary>
        ///     Gets or sets a value indicating whether the Is Work Step condition is used.
        ///     IGM classifies motion output in two types: JOG Steps and WORK Steps.
        ///     - JOG Steps are used for moving the robot between operations.
        ///     - WORK Steps are used during the welding process. They include motion and process parameters.
        /// </summary>
        internal virtual bool IsWorkStep { get; set; }

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            // Define the Current Post File
            this.CurrentPostFile.FileName = this.ProgramName.Replace(" ", "_").ToUpper();
            this.CurrentPostFile.FileExtension = ".ipa";

            // Define the Post file structure
            this.StartPostFile(this.CurrentPostFile);

            // Reset variables
            this.AdditionalPointData = string.Empty;
            this.StepCounter = 2;
            this.IsWorkStep = false;
            this.UsedCoordinateSystemsList.Clear();
        }

        /// <summary>
        ///     Starts the <paramref name="postFile"/> formatting following <see cref="Igm"/> convention.
        ///     <para>Initializes <paramref name="postFile"/> inner structure (see <see cref="PostFile.FileSection"/>)
        ///     and populates <see cref="FileSection.Header"/> and <see cref="FileSection.Footer"/>.</para>
        /// </summary>
        /// <param name="postFile">The <see cref="PostFile"/> to format.</param>
        internal virtual void StartPostFile(PostFile postFile)
        {
            // Define File Sections
            postFile.FileSection.SubFileSections.Add(new FileSection()); // File Header
            postFile.FileSection.SubFileSections.Add(new FileSection()); // Moves

            // Populate Headers and Footers
            postFile.FileSection.Header = $"; Program Name: \"{postFile.FileName}\"" + "\r\n";
            postFile.FileSection.Header += "; PROGRAMMED USING ROBOTMASTER" + "\r\n";

            this.FileHeader.WriteLine(
                "Start {{\r\n" +
                " Version 1.49\r\n" +
                " Robot RT3xx\r\n" +
                " Global CART\r\n" +
                " Library {0}\r\n" +
                " IsK5Program Yes\r\n" +
                " Station {1}\r\n" +
                " SubStNum {2}\r\n" +
                " TorchNum {3}\r\n" +
                " PwrSrcNum 1\r\n" +
                " MediumNum 255\r\n" +
                " NrGlobal 3\r\n" +
                "}}",
                IgmProcessorSwitches.GetLibraryName(this.CellSettingsNode),
                IgmProcessorSwitches.GetRobotStationName(this.CellSettingsNode),
                IgmProcessorSwitches.GetSubStationNumber(this.CellSettingsNode),
                OperationManager.GetTcpId(ProgramManager.GetOperationNodes(this.ProgramNode).First(op => OperationManager.GetOperationType(op) != OperationType.Home)));
        }

        /// <inheritdoc />
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            if (previousOperationNode == null || OperationManager.GetUserFrame(previousOperationNode).Number != OperationManager.GetUserFrame(operationNode).Number)
            {
                this.Moves.Write(this.FormatCoordinateSystem(operationNode, SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode));
            }
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc />
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(beforePointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputInlineEvent(Event inlineEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(inlineEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(afterPointEvent.ToCode());
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.Moves.WriteLine(this.FormatPointOutput(point, operationNode));
        }

        /// <summary>
        ///     Formats the point output for both JOG and WORK steps.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Igm Point Output.</returns>
        internal virtual string FormatPointOutput(PathNode point, CbtNode operationNode)
        {
            string output = string.Format(
                "#{0}.0.0" + "\r\n" +
                "{1} {{" + "\r\n" +
                " P {2}{3}" + "\r\n" +
                " V {4:0.00}" + "\r\n" + // Linear speed units are [m/min] for JOG steps and [cm/min] for WORK steps!!
                "{5}" +
                "{6}" +
                " COO {{" + "\r\n" +
                "  WC {7}" + "\r\n" +
                "  XYZ {8}" + "\r\n" +
                "{9}" + "\r\n" +
                "{10}" +
                " }}" + "\r\n" +
                "}}",
                this.StepCounter,
                this.IsWorkStep ? "WORK" : "JOG",
                point.MoveType() == MoveType.Rapid ? "NL" : (point.MoveType() == MoveType.Circular ? "A" : "L"),
                this.GetPasspoint(point, operationNode) != 0 ? (point.MoveType() == MoveType.Rapid ? "P" : "PP") : string.Empty,
                point.MoveType() == MoveType.Rapid ? IgmMotionSettings.GetNonLinearMaximumSpeed(operationNode) : (this.IsWorkStep ? point.Feedrate().LinearFeedrate : point.Feedrate().LinearFeedrate / 100),
                this.GetPasspoint(point, operationNode) != 0 ? $" PP {this.GetPasspoint(point, operationNode):0.00}\r\n" : string.Empty,
                this.AdditionalPointData,
                OperationManager.GetUserFrame(operationNode).Number,
                this.FormatCartesianPointValues(point, operationNode),
                this.FormatRobotArmFlags(point, operationNode),
                this.FormatExternalAxes(point));

            this.StepCounter++;

            return output;
        }

        /// <summary>
        ///     Gets the Passpoint values for Joint, Linear and Circular moves.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Passpoint Value.</returns>
        internal virtual int GetPasspoint(PathNode point, CbtNode operationNode)
        {
            if (this.IsWorkStep)
            {
                return 0;
            }

            if (point.MoveType() == MoveType.Rapid)
            {
                if (IgmMotionSettings.GetNonLinearUsePasspoint(operationNode) == 1)
                {
                    int passpointValue = IgmMotionSettings.GetNonLinearPasspointValue(operationNode);

                    // Trim passpoint value between 0 and 50 (maximum value)
                    return passpointValue > 50 ? 50 : passpointValue < 0 ? 0 : passpointValue;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                if (IgmMotionSettings.GetLinearCircularUsePasspoint(operationNode) == 1)
                {
                    int passpointValue = IgmMotionSettings.GetLinearCircularPasspointValue(operationNode);

                    // Trim passpoint value between 0 and 50 (maximum value)
                    return passpointValue > 50 ? 50 : passpointValue < 0 ? 0 : passpointValue;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        ///     Formats the Cartesian point values for Joint, Linear and Circular moves.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Igm Cartesian Point Values.</returns>
        internal virtual string FormatCartesianPointValues(PathNode point, CbtNode operationNode)
        {
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 pointPosition = pointFrameInUserFrame.Position;
            Vector3 eulerAngles = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            return string.Format(
                "{0:0.000} {1:0.000} {2:0.000} {3:0.000} {4:0.000} {5:0.000}",
                pointPosition.X,
                pointPosition.Y,
                pointPosition.Z,
                eulerAngles.X,
                eulerAngles.Y,
                eulerAngles.Z);
        }

        /// <summary>
        ///     Formats the External Axes values for Joint, Linear and Circular moves.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Igm External Axes Output.</returns>
        internal virtual string FormatExternalAxes(PathNode point)
        {
            string output = string.Empty;

            for (var axisIndex = 1; axisIndex <= 5; axisIndex++)
            {
                output += this.JointFormat.TryGetValue($"A{axisIndex}", out int i)
                    ? $"  A{axisIndex} {point.JointValue(i):0.000}" + "\r\n"
                    : string.Empty;
            }

            return output;
        }

        /// <summary>
        ///     Formats the Robot Arm Configuration Flags for Joint, Linear and Circular moves.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operation.</param>
        /// <returns>Formatted Igm Arm Configuration Flags.</returns>
        internal virtual string FormatRobotArmFlags(PathNode point, CbtNode operationNode)
        {
            double j1Value = point.JointValue(this.JointFormat["J1"]);
            double j4Value = point.JointValue(this.JointFormat["J4"]);
            double j5Value = point.JointValue(this.JointFormat["J5"]);
            double j6Value = point.JointValue(this.JointFormat["J6"]);

            string output = string.Format(
                "  FL {0} {1} {2} {3} {4} {5} {6}",
                this.CachedRobotConfig.BaseConfiguration == BaseConfig.Front ? "F" : "B",
                this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up ? "U" : "D",
                this.CachedRobotConfig.WristConfiguration == WristConfig.Negative ? "U" : "D",
                j1Value >= 180 ? "+" : j1Value < -180 ? "-" : "0",
                j4Value >= 180 ? "+" : j4Value < -180 ? "-" : "0",
                j5Value >= 180 ? "+" : j5Value < -180 ? "-" : "0",
                j6Value >= 180 ? "+" : j6Value < -180 ? "-" : "0");

            return output;
        }

        /// <summary>
        ///     Formats the Igm Coordinate System Output.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="referenceFrameNode">The reference frame in which the user frame values are given.</param>
        /// <returns>Formatted Igm Coordinate System Output.</returns>
        internal virtual string FormatCoordinateSystem(CbtNode operationNode, CbtNode referenceFrameNode)
        {
            UserFrame userFrame = OperationManager.GetUserFrame(operationNode);

            if (this.UsedCoordinateSystemsList.FindIndex(a => a == userFrame.Number) == -1)
            {
                Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrameNode);
                Vector3 eulerAngles = this.RobotFormatter.MatrixToEuler(userFrameMatrix);

                return
                    $"#1.{userFrame.Number}.0" + "\r\n" +
                    "WRKCOO {" + "\r\n" +
                    $" Name WRKCOO{userFrame.Number}" + "\r\n" +
                    " Type 16" + "\r\n" +
                    " ConnTo -1" + "\r\n" +
                    $" X {userFrameMatrix.Position.X:0.000}" + "\r\n" +
                    $" Y {userFrameMatrix.Position.Y:0.000}" + "\r\n" +
                    $" Z {userFrameMatrix.Position.Z:0.000}" + "\r\n" +
                    $" PHI {eulerAngles.X:0.000000}" + "\r\n" +
                    $" Theta {eulerAngles.Y:0.000000}" + "\r\n" +
                    $" PSI {eulerAngles.Z:0.000000}" + "\r\n" +
                    "}" + "\r\n";
            }

            return string.Empty;
        }

        /// <inheritdoc />
        internal override bool IsProgramInputValid()
        {
            // Verifies if the device has the "label" for the rail and rotaries axes properly defined in the ROBX file.
            if (!this.IsExternalAxesNameValid())
            {
                this.NotifyUser("ERROR: The rail/rotary axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            // Verifies if the program contains tool changes
            if (this.IsMultiToolProgram)
            {
                this.NotifyUser("ERROR: Igm does not support tool changes inside a program. Posting will be stopped.");
                return false;
            }

            foreach (CbtNode operationNode in ProgramManager.GetOperationNodes(this.ProgramNode))
            {
                // Verifies if any operation calls a dynamic User Frame
                if (UserFrameManager.IsDynamicUserFrame(OperationManager.GetUserFrame(operationNode), this.OperationCbtRoot))
                {
                    this.NotifyUser("ERROR: Igm does not support dynamic User Frames. Please set the User Frame to Cell Fixed instead. Posting will be stopped.");
                    return false;
                }

                // If no process is loaded, check for Circular moves. Igm only supports Arc moves in WORK (welding) steps.
                if (!this.IsProcessLoaded)
                {
                    for (PathNode point = OperationManager.GetFirstPoint(operationNode); point != null; point = point.NextPoint(operationNode))
                    {
                        if (point.MoveType() == MoveType.Circular)
                        {
                            this.NotifyUser("ERROR: Igm only supports Arc moves in WORK (welding) steps. Posting will be stopped.");
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the rail and rotary axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the rail and rotary axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsExternalAxesNameValid()
        {
            var externalAxes = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            return !(this.Rails.Any() && this.Rails.Any(x => !externalAxes.Contains(x.Name))) &&
                   !(this.Rotaries.Any() && this.Rotaries.Any(x => !externalAxes.Contains(x.Name)));
        }
    }
}
