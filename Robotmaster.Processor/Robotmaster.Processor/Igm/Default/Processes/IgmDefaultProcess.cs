﻿// <copyright file="IgmDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Igm default process.
    /// </summary>
    [DataContract(Name = "IgmDefaultProcess")]
    public class IgmDefaultProcess : PackageProcess, IIgmProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IgmDefaultProcess"/> class.
        /// </summary>
        public IgmDefaultProcess()
        {
            this.Name = "IGM Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
