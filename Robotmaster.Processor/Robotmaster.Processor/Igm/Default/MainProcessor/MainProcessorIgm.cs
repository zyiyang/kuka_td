﻿// <copyright file="MainProcessorIgm.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Default.MainProcessor
{
    using Robotmaster.Processor.Common.Default.MainProcessor;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Igm dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorIgm : MainProcessor
    {
    }
}
