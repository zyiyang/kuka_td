﻿// <copyright file="IgmWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Igm.Welding.MainProcessor;
    using Robotmaster.Processor.Igm.Welding.PostProcessor;
    using Robotmaster.Processor.Igm.Welding.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Igm welding process.
    /// </summary>
    [DataContract(Name = "IgmWeldingProcess")]
    public class IgmWeldingProcess : PackageProcess, IIgmProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IgmWeldingProcess"/> class.
        /// </summary>
        public IgmWeldingProcess()
        {
            this.Name = "Igm Welding Process";
            this.PostProcessorType = typeof(PostProcessorIgmWelding);
            this.MainProcessorType = typeof(MainProcessorIgmWelding);
            this.AddEventToMet(typeof(ArcOn));
            this.AddEventToMet(typeof(ArcOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
