﻿// <copyright file="IgmWeldingWithTouchSensingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Welding.AuxiliaryMenus.IgmWeldingWithTouchSensing
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "IgmWeldingWithTouchSensingAuxiliary")]
    public class IgmWeldingWithTouchSensingAuxiliary : AuxiliaryMenu, IIgmProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IgmWeldingWithTouchSensingAuxiliary"/> class.
        /// </summary>
        public IgmWeldingWithTouchSensingAuxiliary()
        {
            this.Name = "Igm Welding with Touch Sensing";
            this.ApplicationType = ApplicationType.WeldingWithTouchSensing;
            this.AuxiliaryMenuFileName = "IgmWeldingWithTouchSensingAuxiliaryMenu";
        }
    }
}
