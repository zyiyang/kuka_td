﻿// <copyright file="IgmWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Welding.AuxiliaryMenus.IgmWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "IgmWeldingAuxiliary")]
    public class IgmWeldingAuxiliary : AuxiliaryMenu, IIgmProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IgmWeldingAuxiliary"/> class.
        /// </summary>
        public IgmWeldingAuxiliary()
        {
            this.Name = "Igm Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "IgmWeldingAuxiliaryMenu";
        }
    }
}
