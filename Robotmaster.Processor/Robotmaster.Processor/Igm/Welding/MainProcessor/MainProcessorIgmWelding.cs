﻿// <copyright file="MainProcessorIgmWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Welding.MainProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Igm.Default.MainProcessor;
    using Robotmaster.Processor.Igm.Welding.AuxiliaryMenus.IgmWelding;
    using Robotmaster.Processor.Igm.Welding.AuxiliaryMenus.IgmWeldingWithTouchSensing;
    using Robotmaster.Processor.Igm.Welding.Processes.Events;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    internal class MainProcessorIgmWelding : MainProcessorIgm
    {
        /// <inheritdoc />
        internal override PathNode EditPlungeMove(CbtNode operationNode, PathNode point)
        {
            // Due to the JOG and WORK step limitations, we assume tool activation will be always done after the plunge
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                string weldingParameter = string.Empty;

                if (weldingOperation)
                {
                    weldingParameter = IgmWeldingAuxiliaryMenu.GetWeldingParameter(operationNode);
                }

                if (weldingWithTouchSensingOperation)
                {
                    weldingParameter = IgmWeldingWithTouchSensingAuxiliaryMenu.GetWeldingParameter(operationNode);
                }

                point = this.AddArcOnEvent(operationNode, point, EventIndex.After, weldingParameter);
            }

            return point;
        }

        /// <inheritdoc />
        internal override PathNode EditRetractMove(CbtNode operationNode, PathNode point)
        {
            // Due to the JOG and WORK step limitations, we assume tool deactivation will be always done before the retract
            ApplicationType applicationType = OperationManager.GetApplicationType(operationNode);

            bool weldingOperation = applicationType == ApplicationType.Welding;
            bool weldingWithTouchSensingOperation = applicationType == ApplicationType.WeldingWithTouchSensing && ((IWeldingWithTouchSensing)point)?.IsTouchSensingPoint == false;

            if (weldingOperation || weldingWithTouchSensingOperation)
            {
                point = this.AddArcOffEvent(operationNode, point, EventIndex.Before);
            }

            return point;
        }

        /// <summary>
        ///     Adds ArcOn Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <param name="weldingParameter">The welding parameter.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddArcOnEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex, string weldingParameter)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOn()
                {
                    WeldingParameter = weldingParameter,
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }

        /// <summary>
        ///     Adds ArcOff Event at given point.
        /// </summary>
        /// <param name="operationNode">The operation.</param>
        /// <param name="point">The point.</param>
        /// <param name="eventIndex">The event index.</param>
        /// <returns>Point.</returns>
        internal virtual PathNode AddArcOffEvent(CbtNode operationNode, PathNode point, EventIndex eventIndex)
        {
            point = PointManager.AddEventToPathPoint(
                new ArcOff()
                {
                },
                eventIndex,
                point,
                operationNode);

            return point;
        }
    }
}
