﻿// <copyright file="PostProcessorIgmWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Igm.Welding.PostProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Igm.Default.PostProcessor;
    using Robotmaster.Processor.Igm.Welding.Processes.Events;
    using Robotmaster.Rise.Event;

    internal class PostProcessorIgmWelding : PostProcessorIgm
    {
        /// <inheritdoc />
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            base.OutputBeforePointEvent(beforePointEvent, point, operationNode);

            if (beforePointEvent is ArcOn myArcOn)
            {
                // After arc activation, subsequent points will use WORK steps
                // WORK steps require additional point data
                this.IsWorkStep = true;
                this.SetAdditionalPointData(myArcOn.WeldingParameter);
            }
            else if (beforePointEvent is ArcOff)
            {
                // After arc deactivation, subsequent points will use JOG steps
                // JOG steps do not require additional point data
                this.IsWorkStep = false;
                this.AdditionalPointData = string.Empty;
            }
        }

        /// <inheritdoc />
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            base.OutputAfterPointEvent(afterPointEvent, point, operationNode);

            if (afterPointEvent is ArcOn myArcOn)
            {
                // After arc activation, subsequent points will use WORK steps
                // WORK steps require additional point data
                this.IsWorkStep = true;
                this.SetAdditionalPointData(myArcOn.WeldingParameter);
            }
            else if (afterPointEvent is ArcOff)
            {
                // After arc deactivation, subsequent points will use JOG steps
                // JOG steps do not require additional point data
                this.IsWorkStep = false;
                this.AdditionalPointData = string.Empty;
            }
        }

        /// <summary>
        ///     Sets the additional point data for the welding process.
        /// </summary>
        /// <param name="weldingParameter">The welding parameter.</param>
        internal virtual void SetAdditionalPointData(string weldingParameter)
        {
            this.AdditionalPointData = !this.IsWorkStep
                ? string.Empty
                : " P0 0.00" + "\r\n" +
                  " P1 0.00" + "\r\n" +
                  " P2 0.00" + "\r\n" +
                  " P3 0.00" + "\r\n" +
                  " P4 0.00" + "\r\n" +
                  " P5 0.00" + "\r\n" +
                  " P6 0.00" + "\r\n" +
                  " P7 0.00" + "\r\n" +
                  $" WSC {{ N {weldingParameter} F 0 T 999 }}" + "\r\n";
        }

        /// <inheritdoc />
        internal override void Run(CbtNode programNode)
        {
            // If the process is loaded, additional point data will be allowed
            this.IsProcessLoaded = true;

            base.Run(programNode);
        }
    }
}
