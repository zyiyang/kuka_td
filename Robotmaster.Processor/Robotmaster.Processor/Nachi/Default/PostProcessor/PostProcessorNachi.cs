﻿// <copyright file="PostProcessorNachi.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Default.PostProcessor
{
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Kinematics;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.PostProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;

    internal class PostProcessorNachi : PostProcessorOtcDaihen
    {
        /// <inheritdoc />
        internal override string FormatOrientation(PathNode point, CbtNode operationNode)
        {
            // TODO Review if Nachi convention defined in RISE is correct, currently it does not match OTC's
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 euler = ConvertRotationRepresentation.RotationMatrixToEuler(
                pointFrameInUserFrame.Orientation,
                EulerConvention.Rzyx,
                FrameConvention.BodyFixed,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);

            return euler[0].Formatted() + "," + euler[1].Formatted() + "," + euler[2].Formatted();
        }

        /// <inheritdoc />
        internal override string FormatConfigBits(PathNode point)
        {
            // CONF = ijkl
            //
            // i -> 0:FLIP (J5 < 0) / 1:NONFLIP (J5 > 0)
            // j -> 0:ABOVE (UP) / 1:BELOW (DOWN)
            // k -> 0:LEFTY (FRONT) / 1:RIGHTY (BACK)
            // l -> Flange axis (J6) rotation direction specification:
            //      0: -180 to +180
            //      1: 0 to +360 according to manual. We will assume +180 to +360
            //      2: -360 to 0 according to manual. We will assume -360 to -180
            return (this.CachedRobotConfig.WristConfiguration == WristConfig.Negative ? "0" : "1") +
                   (this.CachedRobotConfig.ElbowConfiguration == ElbowConfig.Up ? "0" : "1") +
                   (this.CachedRobotConfig.BaseConfiguration == BaseConfig.Front ? "0" : "1") +
                   (point.JointValue(this.JointFormat["J6"]) < -180 ? "2" :
                       point.JointValue(this.JointFormat["J6"]) > 180 ? "1" : "0");
        }
    }
}
