﻿// <copyright file="MainProcessorNachi.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Default.MainProcessor
{
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Default.MainProcessor;

    /// <summary>
    /// Nachi base class.
    /// Contains the Nachi methods executed when Calculated is clicked such as event creation automation.
    /// </summary>
    internal class MainProcessorNachi : MainProcessorOtcDaihen
    {
    }
}
