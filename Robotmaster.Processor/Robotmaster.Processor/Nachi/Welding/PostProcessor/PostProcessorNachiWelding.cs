﻿// <copyright file="PostProcessorNachiWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Welding.PostProcessor
{
    using Robotmaster.Processor.Nachi.Default.PostProcessor;

    /// <summary>
    ///     This class inherits from <see cref="PostProcessorNachi"/>.
    ///     <para>
    ///         Implements the Nachi welding dedicated properties and methods of the post-processor in order to output Nachi welding robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorNachiWelding : PostProcessorNachi
    {
    }
}
