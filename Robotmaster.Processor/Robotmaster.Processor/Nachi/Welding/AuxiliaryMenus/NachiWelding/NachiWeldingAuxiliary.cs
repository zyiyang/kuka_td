﻿// <copyright file="NachiWeldingAuxiliary.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Welding.AuxiliaryMenus.NachiWelding
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.BaseClasses.Application.Types;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Auxiliary menu definition.
    /// </summary>
    [DataContract(Name = "NachiWeldingAuxiliary")]
    public class NachiWeldingAuxiliary : AuxiliaryMenu, INachiProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="NachiWeldingAuxiliary"/> class.
        /// </summary>
        public NachiWeldingAuxiliary()
        {
            this.Name = "Nachi Welding";
            this.ApplicationType = ApplicationType.Welding;
            this.AuxiliaryMenuFileName = "OtcDaihenWeldingAuxiliaryMenu";
        }
    }
}
