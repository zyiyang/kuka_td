﻿// <copyright file="NachiWeldingProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Welding.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Nachi.Welding.MainProcessor;
    using Robotmaster.Processor.Nachi.Welding.PostProcessor;
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.Processes.Events;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Nachi welding process.
    /// </summary>
    [DataContract(Name = "NachiWeldingProcess")]
    public class NachiWeldingProcess : PackageProcess, INachiProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="NachiWeldingProcess"/> class.
        /// </summary>
        public NachiWeldingProcess()
        {
            this.Name = "Nachi Welding Process";
            this.PostProcessorType = typeof(PostProcessorNachiWelding);
            this.MainProcessorType = typeof(MainProcessorNachiWelding);
            this.AddEventToMet(typeof(WeldOn));
            this.AddEventToMet(typeof(WeldOff));
            this.AddEventToMet(typeof(WeaveOn));
            this.AddEventToMet(typeof(WeaveOff));
            this.ProcessMenuFileName = "OtcDaihenWeldingProcessMenu";
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
