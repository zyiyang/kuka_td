﻿// <copyright file="MainProcessorNachiWelding.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Nachi.Welding.MainProcessor
{
    using Robotmaster.Processor.OsakaTransformerCompanyDaihen.Welding.MainProcessor;

    /// <summary>
    ///     This class inherits from <see cref="MainProcessorOtcDaihenWelding" />.
    ///     <para>
    ///         Implements the Nachi Welding dedicated properties and methods of the main processor.
    ///     </para>
    /// </summary>
    internal class MainProcessorNachiWelding : MainProcessorOtcDaihenWelding
    {
    }
}
