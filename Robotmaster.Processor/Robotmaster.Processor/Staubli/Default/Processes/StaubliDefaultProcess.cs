﻿// <copyright file="StaubliDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Staubli default process.
    /// </summary>
    [DataContract(Name = "StaubliDefaultProcess")]
    public class StaubliDefaultProcess : PackageProcess, IStaubliProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="StaubliDefaultProcess"/> class.
        /// </summary>
        public StaubliDefaultProcess()
        {
            this.Name = "Staubli Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
