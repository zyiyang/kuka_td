// <copyright file="StaubliMotionSettingsViewModel.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.DeviceMenus.MotionSettings
{
    using System.Collections.Generic;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu;
    using Robotmaster.Rise.ApplicationLayer.ExternalMenu.Model;

    /// <summary>
    ///     StaubliMotionSettingsViewModel View Model.
    /// </summary>
    public class StaubliMotionSettingsViewModel : ExternalMenuUiHandlerViewModel
    {
        private static bool expandable1;
        private static bool expandable2;
        private static bool expandable3;
        private static bool expandable4;

        /// <summary>
        ///     Gets or sets the joint motion leave distance value.
        ///     Leave is the distance from the arrival point at which the nominal trajectory is left (start of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double JointMotionLeaveDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionLeaveDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionLeaveDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the joint motion reach distance value.
        ///     Reach is the distance from the arrival point at which the nominal trajectory is rejoined (end of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double JointMotionReachDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionReachDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.JointMotionReachDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint speed as a % of the nominal speed of the robot.
        /// </summary>
        public int JointMotionMaximumSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionMaximumSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionMaximumSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint acceleration as a % of the nominal acceleration of the robot.
        /// </summary>
        public int JointMotionAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint deceleration as a % of the nominal deceleration of the robot.
        /// </summary>
        public int JointMotionDeceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionDeceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.JointMotionDeceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear motion leave distance value.
        ///     Leave is the distance from the arrival point at which the nominal trajectory is left (start of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double LinearMotionLeaveDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinearMotionLeaveDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinearMotionLeaveDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the linear motion reach distance value.
        ///     Reach is the distance from the arrival point at which the nominal trajectory is rejoined (end of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double LinearMotionReachDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinearMotionReachDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.LinearMotionReachDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint speed as a % of the nominal speed of the robot.
        /// </summary>
        public int LinearMotionMaximumJointSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionMaximumJointSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionMaximumJointSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint acceleration as a % of the nominal acceleration of the robot.
        /// </summary>
        public int LinearMotionJointAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionJointAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionJointAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint deceleration as a % of the nominal deceleration of the robot.
        /// </summary>
        public int LinearMotionJointDeceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionJointDeceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.LinearMotionJointDeceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the circular motion leave distance value.
        ///     Leave is the distance from the arrival point at which the nominal trajectory is left (start of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double CircularMotionLeaveDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.CircularMotionLeaveDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.CircularMotionLeaveDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the circular motion reach distance value.
        ///     Reach is the distance from the arrival point at which the nominal trajectory is rejoined (end of blending).
        ///     Units can be in mm or inches, depending on the unit of length of the application.
        /// </summary>
        public double CircularMotionReachDistance
        {
            get => this.GetParameterByPostVariable<DoubleParameter>(nameof(this.CircularMotionReachDistance)).UserValue;

            set
            {
                this.GetParameterByPostVariable<DoubleParameter>(nameof(this.CircularMotionReachDistance)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint speed as a % of the nominal speed of the robot.
        /// </summary>
        public int CircularMotionJointMaximumSpeed
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointMaximumSpeed)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointMaximumSpeed)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint acceleration as a % of the nominal acceleration of the robot.
        /// </summary>
        public int CircularMotionJointAcceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointAcceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointAcceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum permitted joint deceleration as a % of the nominal deceleration of the robot.
        /// </summary>
        public int CircularMotionJointDeceleration
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointDeceleration)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.CircularMotionJointDeceleration)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum translational velocity of the tool center point.
        ///     Units can be in mm/s or inch/s depending on the unit of length of the application.
        /// </summary>
        public int MaximumTranslationalVelocityOfTcp
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumTranslationalVelocityOfTcp)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumTranslationalVelocityOfTcp)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum rotational velocity of the tool in deg/s.
        /// </summary>
        public int MaximumRotationalVelocityOfTcp
        {
            get => this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumRotationalVelocityOfTcp)).UserValue;

            set
            {
                this.GetParameterByPostVariable<IntegerParameter>(nameof(this.MaximumRotationalVelocityOfTcp)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enable output of joint motion blending (leave and reach distance).
        /// </summary>
        public bool IsJointMotionBlendingEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointMotionBlendingEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsJointMotionBlendingEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enable output of linear motion blending (leave and reach distance).
        /// </summary>
        public bool IsLinearMotionBlendingEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearMotionBlendingEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsLinearMotionBlendingEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether enable output of linear motion blending (leave and reach distance).
        /// </summary>
        public bool IsCircularMotionBlendingEnabled
        {
            get => this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularMotionBlendingEnabled)).UserValue;

            set
            {
                this.GetParameterByPostVariable<BooleanParameter>(nameof(this.IsCircularMotionBlendingEnabled)).UserValue = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable1"/> is checked.
        /// </summary>
        public bool IsExpandable1Checked
        {
            get => expandable1;

            set
            {
                expandable1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable2"/> is checked.
        /// </summary>
        public bool IsExpandable2Checked
        {
            get => expandable2;

            set
            {
                expandable2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable3"/> is checked.
        /// </summary>
        public bool IsExpandable3Checked
        {
            get => expandable3;

            set
            {
                expandable3 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the <see cref="expandable4"/> is checked.
        /// </summary>
        public bool IsExpandable4Checked
        {
            get => expandable4;

            set
            {
                expandable4 = value;
                this.OnPropertyChanged();
            }
        }
    }
}