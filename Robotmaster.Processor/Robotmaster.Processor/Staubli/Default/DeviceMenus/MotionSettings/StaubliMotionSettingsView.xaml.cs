// <copyright file="StaubliMotionSettingsView.xaml.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.DeviceMenus.MotionSettings
{
    /// <summary>
    ///     Interaction logic for StaubliMotionSettings.
    /// </summary>
    public partial class StaubliMotionSettingsView
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="StaubliMotionSettingsView"/> class.
        /// </summary>
        public StaubliMotionSettingsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StaubliMotionSettingsView"/> class.
        /// </summary>
        /// <param name="viewModel">
        ///     The <see cref="StaubliMotionSettingsViewModel"/> to map to the UI.
        /// </param>
        public StaubliMotionSettingsView(StaubliMotionSettingsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets the object linked with the UI.
        /// </summary>
        public StaubliMotionSettingsViewModel ViewModel { get; }
    }
}