﻿// <copyright file="MainProcessorStaubli.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.MainProcessor
{
    using Robotmaster.Processor.Common.Default.MainProcessor;

    /// <inheritdoc />
    /// <summary>
    /// This class inherits from <see cref="T:Robotmaster.Processor.Common.Default.MainProcessor.MainProcessor" />.
    /// <para>Implements the Staubli dedicated properties and methods of the main processor.</para>
    /// </summary>
    internal class MainProcessorStaubli : MainProcessor
    {
    }
}