﻿// <copyright file="MovementParameters.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.PostProcessor
{
    /// <summary>
    ///     Staubli Movement Parameters.
    /// </summary>
    internal class MovementParameters
    {
        /// <summary>
        ///     Gets or sets the maximum permitted revolute acceleration as a % of the nominal acceleration of the robot.
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: accel.
        /// </remarks>
        internal double Acceleration { get; set; }

        /// <summary>
        ///     Gets or sets the maximum permitted revolute speed as a % of the nominal speed of the robot.
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: vel.
        /// </remarks>
        internal double Velocity { get; set; }

        /// <summary>
        ///     Gets or sets the maximum permitted revolute deceleration as a % of the nominal deceleration of the robot.
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: decel.
        /// </remarks>
        internal double Deceleration { get; set; }

        /// <summary>
        ///     Gets or sets the maximum permitted translational speed of the tool center point.
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: tvel.
        /// </remarks>
        internal double MaximumTranslationalSpeedOfTcp { get; set; }

        /// <summary>
        ///     Gets or sets the maximum permitted rotational speed of the tool.
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: rvel.
        /// </remarks>
        internal double MaximumRotationalSpeedOfTool { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the blend mode is joint (blending).
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: blend.
        /// </remarks>
        internal bool IsJointBlending { get; set; }

        /// <summary>
        ///     Gets or sets the distance between the target point where blending starts and the next point.
        /// <para>
        ///     Used when <see cref="IsJointBlending"/> is in joint mode.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: leave.
        /// </remarks>
        internal double Leave { get; set; }

        /// <summary>
        ///     Gets or sets the distance between the target point where blending stops and the next point.
        /// <para>
        ///     Used when <see cref="IsJointBlending"/> is in joint mode.
        /// </para>
        /// </summary>
        /// <remarks>
        ///     STAUBLI VAL3 component name: reach.
        /// </remarks>
        internal double Reach { get; set; }
    }
}
