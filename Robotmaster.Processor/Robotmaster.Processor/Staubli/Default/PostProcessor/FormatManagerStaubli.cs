﻿// <copyright file="FormatManagerStaubli.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.PostProcessor
{
    using Robotmaster.Kinematics;
    using Robotmaster.Math;
    using Robotmaster.Math.Algebra;

    /// <summary>
    ///     Staubli Format Manager.
    /// </summary>
    public static class FormatManagerStaubli
    {
        /// <summary>
        ///     Gets the main project extension.
        /// </summary>
        public const string ProjectExtension = ".pjx";

        /// <summary>
        ///     Gets the data extension.
        /// </summary>
        public const string DataExtension = ".dtx";

        /// <summary>
        ///     Gets the program section extension.
        /// </summary>
        public const string ProgramExtension = ".pgx";

        /// <summary>
        ///     Gets the feed format.
        /// </summary>
        private const string FeedFormat = "{0:0.0}";

        /// <summary>
        ///     Gets the generic format.
        /// </summary>
        private const string GenericFormat = "{0:0.000}";

        /// <summary>
        ///     Gets point number format.
        /// </summary>
        private const string PointNumberFormat = "{0:0.#}";

        /// <summary>
        ///     Formats the point number with leading zeros.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>String with leading zeros.</returns>
        public static string PointNumberFormatted(this int value)
        {
            return string.Format(PointNumberFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this int value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this double value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted value.</returns>
        public static string Formatted(this float value)
        {
            return string.Format(GenericFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any int.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this int value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any double.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this double value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic numeric to string formatting.
        /// </summary>
        /// <param name="value">Any float.</param>
        /// <returns>Formatted Speed value.</returns>
        public static string FeedFormatted(this float value)
        {
            return string.Format(FeedFormat, value);
        }

        /// <summary>
        ///     Generic Base Config output string formatting.
        /// </summary>
        /// <param name="baseConfig">Base Configuration Robotmaster Type.</param>
        /// <returns>Formatted Base Configuration String.</returns>
        public static string BaseConfigFormatted(BaseConfig baseConfig)
        {
            return baseConfig == BaseConfig.Front ? "lefty" : "righty";
        }

        /// <summary>
        ///     Generic Elbow Config output string formatting.
        /// </summary>
        /// <param name="elbowConfig">Elbow Configuration Robotmaster Type.</param>
        /// <returns>Formatted Elbow Configuration String.</returns>
        public static string ElbowConfigFormatted(ElbowConfig elbowConfig)
        {
            return elbowConfig == ElbowConfig.Up ? "epositive" : "enegative";
        }

        /// <summary>
        ///     Generic Wrist Config output string formatting.
        /// </summary>
        /// <param name="wristConfig">Wrist Configuration Robotmaster Type.</param>
        /// <returns>Formatted Wrist Configuration String.</returns>
        public static string WristConfigFormatted(WristConfig wristConfig)
        {
            return wristConfig == WristConfig.Positive ? "wpositive" : "wnegative";
        }

        /// <summary>
        ///     Formats matrix 3x3 to Euler angles Staubli convention.
        /// </summary>
        /// <param name="orientationMatrix">Orientation Matrix3X3.</param>
        /// <returns>Formatted Staubli convention euler rotation Vector3.</returns>
        public static Vector3 ToEuler(Matrix3X3 orientationMatrix)
        {
            return ConvertRotationRepresentation.RotationMatrixToEuler(
                orientationMatrix,
                EulerConvention.Rxyz,
                FrameConvention.BodyFixed,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);
        }

        /// <summary>
        ///     Formats matrix 4x4 to Euler angles Staubli convention.
        /// </summary>
        /// <param name="frameMatrix">Frame Matrix4X4.</param>
        /// <returns>Formatted Staubli convention euler rotation Vector3.</returns>
        public static Vector3 ToEuler(Matrix4X4 frameMatrix)
        {
            return ConvertRotationRepresentation.RotationMatrixToEuler(
                frameMatrix.Orientation,
                EulerConvention.Rxyz,
                FrameConvention.BodyFixed,
                SecondAngleConvention.RightHalfOrPositive,
                SingularityOption.First,
                0);
        }
    }
}