﻿// <copyright file="PostProcessorStaubli.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Device.Component;
    using Robotmaster.Device.Path.Types;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Staubli.Default.DeviceMenus.MotionSettings;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Robotmaster.Rise.BaseClasses.Operation.Types;
    using Robotmaster.Rise.BaseClasses.Settings;
    using Robotmaster.Rise.Event;

    /// <summary>
    ///     <see cref="PostProcessorStaubli"/> inherits from <see cref="PostProcessor"/>.
    ///     <para>
    ///         Implements the Staubli dedicated properties and methods of the post-processor in order to output Staubli default robotic code.
    ///     </para>
    /// </summary>
    internal class PostProcessorStaubli : PostProcessor
    {
        /// <summary>
        ///     Gets or sets the schema instance namespace (XSI) used to associate XML schemas with XML documents.
        /// </summary>
        internal virtual XNamespace NameSpaceXsi { get; set; } = "http://www.w3.org/2001/XMLSchema-instance";

        /// <summary>
        ///     Gets or sets the XML namespace of DTX database.
        /// </summary>
        internal virtual XNamespace NameSpaceDtx { get; set; } = "http://www.staubli.com/robotics/VAL3/Data/2";

        /// <summary>
        ///     Gets or sets the XML namespace of PGX program/function file.
        /// </summary>
        internal virtual XNamespace NameSpacePgx { get; set; } = "http://www.staubli.com/robotics/VAL3/Program/2";

        /// <summary>
        ///     Gets or sets the XML namespace of PJX project file.
        /// </summary>
        internal virtual XNamespace NameSpacePjx { get; set; } = "http://www.staubli.com/robotics/VAL3/Project/3";

        /// <summary>
        ///     Gets or sets the XML namespace of PGX parameters.
        /// </summary>
        internal virtual XNamespace NameSpacePgxParameters { get; set; } = "http://www.staubli.com/robotics/VAL3/Param/1";

        /// <summary>
        ///     Gets or sets the <see cref="XDocument"/> used to create the .dtx file.
        /// </summary>
        internal virtual XDocument DtxXDocument { get; set; } = new XDocument();

        /// <summary>
        ///     Gets or sets the <see cref="XDocument"/> used to create the .pjx file.
        /// </summary>
        internal virtual XDocument PjxXDocument { get; set; } = new XDocument();

        /// <summary>
        ///     Gets or sets the <see cref="XDocument"/> used to create the start.pgx file.
        /// </summary>
        internal virtual XDocument StartXDocument { get; set; } = new XDocument();

        /// <summary>
        ///     Gets or sets the <see cref="XDocument"/> used to create the stop.pgx file.
        /// </summary>
        internal virtual XDocument StopXDocument { get; set; } = new XDocument();

        /// <summary>
        ///     Gets or sets the <see cref="XDocument"/> used to create the setLink.pgx file.
        /// </summary>
        internal virtual XDocument SetLinkXDocument { get; set; } = new XDocument();

        /// <summary>
        ///     Gets or sets the <see cref="PostFile"/> used to create the Lasman .txt file.
        /// </summary>
        internal virtual PostFile LasmanPostFile { get; set; }

        /// <summary>
        ///     Gets the <see cref="StreamWriter"/> used to create the Lasman .txt file.
        /// </summary>
        internal virtual StreamWriter LasmanStreamWriter => this.LasmanPostFile.FileSection.StreamWriter;

        /// <summary>
        ///     Gets or sets the CData string builder.
        /// </summary>
        internal virtual StringBuilder StartXcDataStringBuilder { get; set; } = new StringBuilder();

        /// <summary>
        ///     Gets or sets the list of used user frame(s).
        /// </summary>
        internal virtual List<int> ListOfUsedUserFrames { get; set; } = new List<int>();

        /// <summary>
        ///     Gets or sets the list of used tool frame(s).
        /// </summary>
        internal virtual List<int> ListOfUsedToolFrames { get; set; } = new List<int>();

        /// <summary>
        ///     Gets or sets the list of used Cartesian movement parameters.
        /// </summary>
        internal virtual List<MovementParameters> UsedCartesianMovementParameters { get; set; } = new List<MovementParameters>();

        /// <summary>
        ///     Gets or sets the list of used joint movement parameters.
        /// </summary>
        internal virtual List<MovementParameters> UsedJointMovementParameters { get; set; } = new List<MovementParameters>();

        /// <summary>
        ///     Gets or sets the current nTarget number (external axes output).
        /// </summary>
        internal virtual int CurrentNTarget { get; set; }

        /// <summary>
        ///     Gets or sets the current nTargetJoint number (external axes output).
        /// </summary>
        internal virtual int CurrentNTargetJoint { get; set; }

        /// <summary>
        ///     Gets or sets the Part Frame name.
        /// </summary>
        internal virtual string PartFrameName { get; set; }

        /// <summary>
        ///     Gets or sets the Tool Frame name.
        /// </summary>
        internal virtual string ToolFrameName { get; set; }

        /// <summary>
        ///     Gets or sets the Routing Path name.
        /// </summary>
        internal virtual string RoutingPathName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the program is RTCP.
        /// </summary>
        internal virtual bool IsRtcp { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the Lasman output should be generated.
        /// </summary>
        internal virtual bool LasmanOutput { get; set; } = false;

        /// <summary>
        ///     Gets or sets a value indicating whether the tool is active (Lasman output).
        /// </summary>
        internal virtual bool LasmanToolOnFlag { get; set; }

        /// <summary>
        ///     Gets or sets the first task node (Lasman output).
        /// </summary>
        internal virtual CbtNode FirstTaskNode { get; set; }

        /// <summary>
        ///     Gets or sets the operation counter (Lasman output).
        /// </summary>
        internal virtual int LasmanOperationCounter { get; set; }

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            //// Initiates the dtx document.
            //// Example:
            ////
            ////      <?xml version="1.0" encoding="utf-8" ?>
            ////      <Database xmlns: xsi = "http://www.w3.org/2001/XMLSchema-instance" xmlns = "http://www.staubli.com/robotics/VAL3/Data/2">
            ////         <Datas>
            ////           <Data name="fUserFrame" access="private" xsi:type="array" type="frame" size="0" />
            ////           <Data name="mNomSpeed" access="private" xsi:type="array" type="mdesc" size="0"/>
            ////           <Data name="mdJointBlend" access="private" xsi:type="array" type="mdesc" size="0"/>
            ////           <Data name="mdLineBlend" access="private" xsi:type="array" type="mdesc" size="0"/
            ////           <Data name="jtPark" access="private" xsi:type="array" type="jointRx" size="0"/>
            ////           <Data name="ptRoutingPath" access="private" xsi:type="array" type="pointRx" size="0"/>
            ////           <Data name="tTool" access="private" xsi:type="array" type="tool" size="0"/>
            ////         </Datas>
            ////      </Database>

            this.IsRtcp = SetupManager.IsRtcp(this.SetupNode);
            this.PartFrameName = this.IsRtcp ? "toolOrigin" : "fUserFrame";
            this.ToolFrameName = this.IsRtcp ? "ptTool" : "tTool";
            this.RoutingPathName = this.IsRtcp ? "toolRoutingPath" : "ptRoutingPath";

            // Lasman output is triggered from the configuration name
            this.LasmanOutput = SetupManager.GetConfiguration(this.SetupNode).Name.IndexOf("_LASMAN", StringComparison.OrdinalIgnoreCase) != -1;

            this.CurrentNTarget = 0;
            this.CurrentNTargetJoint = 0;

            var docDeclaration = new XDeclaration("1.0", "UTF-8", null);

            // Adds <?xml version="1.0" encoding="utf-8" ?>
            this.DtxXDocument.Declaration = docDeclaration;

            // Adds <Database xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Data/2">
            var databaseDtxXElement = new XElement(this.NameSpaceDtx + "Database");
            databaseDtxXElement.Add(new XAttribute(XNamespace.Xmlns + "xsi", this.NameSpaceXsi));
            this.DtxXDocument.Add(databaseDtxXElement);

            // Adds <Datas>
            var datasXElement = new XElement(this.NameSpaceDtx + "Datas");
            databaseDtxXElement.Add(datasXElement);

            // Adds all the <Data>
            var dataXmlNames = new string[] { this.PartFrameName, "mNomSpeed", "mdJointBlend", "mdLineBlend", "jtPoints", this.RoutingPathName, this.ToolFrameName };
            var dataXmlTypes = new string[] { "frame", "mdesc", "mdesc", "mdesc", "jointRx", "pointRx", "tool" };
            var dataXmlTypesRtcp = new string[] { "tool", "mdesc", "mdesc", "mdesc", "jointRx", "tool", "pointRx" };

            for (var i = 0; i < dataXmlNames.Length; i++)
            {
                var dataElement = new XElement(this.NameSpaceDtx + "Data");
                dataElement.SetAttributeValue("name", dataXmlNames[i]);
                dataElement.SetAttributeValue("access", "private");
                dataElement.SetAttributeValue(this.NameSpaceXsi + "type", "array");
                dataElement.SetAttributeValue("type", this.IsRtcp ? dataXmlTypesRtcp[i] : dataXmlTypes[i]);
                dataElement.SetAttributeValue("size", "0");

                datasXElement.Add(dataElement);
            }

            // TODO: (AT-494) Hard coded to be removed
            // Adds <Value key="0" accel="100" vel="100" decel="100" tmax="99999" rmax="99999" blend="off" leave="50" reach="50" />
            var mNomSpeedXElement = new XElement(this.NameSpaceDtx + "Value");
            mNomSpeedXElement.SetAttributeValue("key", "0");
            mNomSpeedXElement.SetAttributeValue("accel", "100");
            mNomSpeedXElement.SetAttributeValue("vel", "100");
            mNomSpeedXElement.SetAttributeValue("decel", "100");
            mNomSpeedXElement.SetAttributeValue("tmax", "99999");
            mNomSpeedXElement.SetAttributeValue("rmax", "99999");
            mNomSpeedXElement.SetAttributeValue("blend", "off");
            mNomSpeedXElement.SetAttributeValue("leave", "50");
            mNomSpeedXElement.SetAttributeValue("reach", "50");

            this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", "mNomSpeed", mNomSpeedXElement);

            //// Initiates the pjx document.
            //// Example:
            ////
            ////       <?xml version="1.0" encoding="utf-8" ?>
            ////       <Project xmlns="http://www.staubli.com/robotics/VAL3/Project/3">
            ////         <Parameters version="s7.3.1" stackSize="5000" millimeterUnit="true" />
            ////         <Programs>
            ////           <Program file="start.pgx" />
            ////           <Program file="stop.pgx" />
            ////         </Programs>
            ////         <Database>
            ////           <Data file="PROGRAM_NAME.dtx" />
            ////         </Database>
            ////         <Libraries/>
            ////       </Project>

            // Adds <?xml version="1.0" encoding="utf-8" ?>
            this.PjxXDocument.Declaration = docDeclaration;

            // Adds <Project xmlns="http://www.staubli.com/robotics/VAL3/Project/3">
            var projectXElement = new XElement(this.NameSpacePjx + "Project");

            // Adds <Parameters version="s7.3.1" stackSize="5000" millimeterUnit="true" />
            var parametersXElement = new XElement(this.NameSpacePjx + "Parameters");
            parametersXElement.SetAttributeValue("version", "s7.3.1");
            parametersXElement.SetAttributeValue("stackSize", "5000");
            parametersXElement.SetAttributeValue("millimeterUnit", "true");
            projectXElement.Add(parametersXElement);

            // Adds <Programs/>
            var programsPjxXElement = new XElement(this.NameSpacePjx + "Programs");
            projectXElement.Add(programsPjxXElement);

            if (this.ExternalAxesJoints.Any())
            {
                // Adds <Program file="setLink.pgx" />
                var programSetLinkFileXElement = new XElement(this.NameSpacePjx + "Program");
                programSetLinkFileXElement.SetAttributeValue("file", "setLink.pgx");
                programsPjxXElement.Add(programSetLinkFileXElement);
            }

            // Adds <Program file="start.pgx" />
            var programStartFileXElement = new XElement(this.NameSpacePjx + "Program");
            programStartFileXElement.SetAttributeValue("file", "start.pgx");
            programsPjxXElement.Add(programStartFileXElement);

            // Adds <Program file="stop.pgx" />
            var programStopFileXElement = new XElement(this.NameSpacePjx + "Program");
            programStopFileXElement.SetAttributeValue("file", "stop.pgx");
            programsPjxXElement.Add(programStopFileXElement);

            // Adds <Database/>
            var databasePjxXElement = new XElement(this.NameSpacePjx + "Database");
            projectXElement.Add(databasePjxXElement);

            // Adds <Data file="PROGRAM_NAME.dtx" />
            var dataXElement = new XElement(this.NameSpacePjx + "Data");
            dataXElement.SetAttributeValue("file", this.ProgramName + ".dtx");
            databasePjxXElement.Add(dataXElement);

            // Adds <Libraries/>
            projectXElement.Add(new XElement(this.NameSpacePjx + "Libraries"));
            this.PjxXDocument.Add(projectXElement);

            //// Initiates the start.pgx Document.
            //// Example:
            ////
            ////        <?xml version="1.0" encoding="utf-8" ?>
            ////        <Programs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Program/2">
            ////          <Program name="start" access="private">
            ////            <Code><![CDATA[
            ////           begin
            ////                 ]]></Code>
            ////           </Program>
            ////        </Programs>

            // Adds <?xml version="1.0" encoding="utf-8" ?>
            this.StartXDocument.Declaration = docDeclaration;

            // Adds <Programs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Program/2">
            var programsStartXElement = new XElement(this.NameSpacePgx + "Programs");
            programsStartXElement.SetAttributeValue(XNamespace.Xmlns + "xsi", this.NameSpaceXsi);
            this.StartXDocument.Add(programsStartXElement);

            // Adds <Program name="start" access="private">
            var programStartXElement = new XElement(this.NameSpacePgx + "Program");
            programStartXElement.SetAttributeValue("name", "start");
            programStartXElement.SetAttributeValue("access", "private");
            programsStartXElement.Add(programStartXElement);

            // Adds <Code/>
            programStartXElement.Add(new XElement(this.NameSpacePgx + "Code"));

            // Starts CDATA string builder
            this.StartXcDataStringBuilder.AppendLine("\r\n      begin");

            //// Initiates the stop.pgx Document.
            //// Example:
            ////
            ////        <?xml version="1.0" encoding="utf-8" ?>
            ////        <Programs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Program/2">
            ////            <Program name="stop" access="private">
            ////            <Code/>
            ////            </Program>
            ////        </Programs>

            // Adds <?xml version="1.0" encoding="utf-8" ?>
            this.StopXDocument.Declaration = docDeclaration;

            // Adds <Programs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Program/2">
            var programsStopXElement = new XElement(this.NameSpacePgx + "Programs");
            programsStopXElement.SetAttributeValue(XNamespace.Xmlns + "xsi", this.NameSpaceXsi);
            this.StopXDocument.Add(programsStopXElement);

            // Adds <Program name="stop" access="private">
            var programStopXElement = new XElement(this.NameSpacePgx + "Program");
            programStopXElement.SetAttributeValue("name", "stop");
            programStopXElement.SetAttributeValue("access", "private");
            programsStopXElement.Add(programStopXElement);

            // Adds <Code/>
            programStopXElement.Add(new XElement(this.NameSpacePgx + "Code"));

            // SetLink Output
            if (this.ExternalAxesJoints.Any())
            {
                // Adds <?xml version="1.0" encoding="utf-8" ?>
                this.SetLinkXDocument.Declaration = docDeclaration;

                // Adds <Programs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.staubli.com/robotics/VAL3/Program/2">
                var programsSetLinkXElement = new XElement(this.NameSpacePgx + "Programs");
                programsSetLinkXElement.SetAttributeValue(XNamespace.Xmlns + "xsi", this.NameSpaceXsi);
                this.SetLinkXDocument.Add(programsSetLinkXElement);

                // Adds <Program name="stop" access="private">
                var programSetLinkXElement = new XElement(this.NameSpacePgx + "Program");
                programSetLinkXElement.SetAttributeValue("name", "setLink");
                programSetLinkXElement.SetAttributeValue("access", "public");
                programsSetLinkXElement.Add(programSetLinkXElement);

                // Adds <Parameters/>
                var parametersSetLinkXElement = new XElement(this.NameSpacePgxParameters + "Parameters");
                var parameterSetLinkXElement = new XElement(this.NameSpacePgxParameters + "Parameter");
                parameterSetLinkXElement.SetAttributeValue("name", "x_fCellule");
                parameterSetLinkXElement.SetAttributeValue("type", "frame");
                parameterSetLinkXElement.SetAttributeValue(this.NameSpaceXsi + "type", "element");
                parameterSetLinkXElement.SetAttributeValue("use", "reference");

                parametersSetLinkXElement.Add(parameterSetLinkXElement);
                programSetLinkXElement.Add(parametersSetLinkXElement);

                // Adds <Code/>
                programSetLinkXElement.Add(new XElement(this.NameSpacePgx + "Code"));
            }

            // Lasman Output
            if (this.LasmanOutput)
            {
                CbtNode firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
                this.FirstTaskNode = ProgramManager.GetOperationNodes(this.ProgramNode).FirstOrDefault(operation => OperationManager.GetOperationType(operation) == OperationType.Task);

                this.LasmanPostFile = new PostFile();
                this.LasmanPostFile.FileName = this.ProgramName.ToUpper();
                this.LasmanPostFile.FileExtension = ".txt";

                //// Initiates the Lasman .txt Document.
                //// Example:
                ////
                //// PRODUCT=PROGRAM 1
                //// TOOL = 226.117,0.000,263.388,0.000,59.998,-180.000
                //// HOME = 0.000,0.000,90.000,0.000,0.000,0.000
                //// TOTALELEMENTS = 1
                //// FRAME = 500.000,0.000,0.000,0.000,0.000,0.000
                //// OFFSET = 0,0,0,0,0,0
                //// PRESET = StaubliPreset

                this.LasmanStreamWriter.WriteLine("PRODUCT=" + this.LasmanPostFile.FileName);
                this.LasmanStreamWriter.WriteLine(this.FormatToolFrameLasman(firstOperation));
                this.LasmanStreamWriter.WriteLine(this.FormatHomePositionLasman(OperationManager.GetFirstPoint(firstOperation)));
                this.LasmanStreamWriter.WriteLine("TOTALELEMENTS=" + ProgramManager.GetOperationNodes(this.ProgramNode).Count(operation => OperationManager.GetOperationType(operation) == OperationType.Task));
                this.LasmanStreamWriter.WriteLine("FRAME=0,0,0,0,0,0");
                this.LasmanStreamWriter.WriteLine("OFFSET=0,0,0,0,0,0");
                this.LasmanStreamWriter.WriteLine("PRESET=StaubliPreset");

                this.LasmanOperationCounter = 0;
                this.LasmanToolOnFlag = false;
            }
        }

        /// <inheritdoc />
        internal override void RunAfterProgramOutput()
        {
            // Process Start.pgx XDocument
            // Ends start CDATA string builder
            this.StartXcDataStringBuilder.AppendLine("      end");
            string startXcDataString;

            if (this.IsRtcp)
            {
                // RTCP requires setting the blending to Cartesian Mode TODO: To be confirmed
                List<string> startXcDataList = this.StartXcDataStringBuilder.ToString().Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();

                int index = startXcDataList.IndexOf("      begin");

                if (index != -1)
                {
                    startXcDataList[index] = startXcDataList[index] + "\r\n" + "\r\n" + "      // Set the blending to Cartesian Mode";

                    for (var i = 0; i < this.UsedJointMovementParameters.Count; i++)
                    {
                        startXcDataList[index] = startXcDataList[index] + "\r\n" + $"      mdJointBlend[{i}].blend=Cartesian";
                    }

                    for (var i = 0; i < this.UsedCartesianMovementParameters.Count; i++)
                    {
                        startXcDataList[index] = startXcDataList[index] + "\r\n" + $"      mdLineBlend[{i}].blend=Cartesian";
                    }
                }

                startXcDataString = string.Join("\r\n", startXcDataList);
            }
            else
            {
                startXcDataString = this.StartXcDataStringBuilder.ToString();
            }

            var startXcData = new XCData(startXcDataString);

            // Adds CDATA to Start.pgx XDocument
            this.StartXDocument.Element(this.NameSpacePgx + "Programs")?
                               .Element(this.NameSpacePgx + "Program")?
                               .Element(this.NameSpacePgx + "Code")?
                               .Add(startXcData);

            // Process Stop.pgx XDocument
            // Ends stop CDATA string builder
            const string stopXcDataString = "\r\n      " + "begin\r\n" +
                                            "        " + "resetMotion()\r\n" +
                                            "      " + "end\r\n";
            var stopXcData = new XCData(stopXcDataString);

            // Adds CDATA to Stop.pgx XDocument
            this.StopXDocument.Element(this.NameSpacePgx + "Programs")?
                              .Element(this.NameSpacePgx + "Program")?
                              .Element(this.NameSpacePgx + "Code")?
                              .Add(stopXcData);

            if (this.ExternalAxesJoints.Any())
            {
                // Process setLink.pgx XDocument
                // Ends stop CDATA string builder
                string setLinkXcDataString = "\r\n      " + "begin\r\n" +
                                             "        " + "link(" + this.PartFrameName + ",x_fCellule)\r\n" +
                                             "      " + "end\r\n";

                var setLinkXcData = new XCData(setLinkXcDataString);

                // Adds CDATA to setLink.pgx XDocument
                this.SetLinkXDocument.Element(this.NameSpacePgx + "Programs")?
                                     .Element(this.NameSpacePgx + "Program")?
                                     .Element(this.NameSpacePgx + "Code")?
                                     .Add(setLinkXcData);
            }
        }

        /// <inheritdoc />
        internal override void GenerateFiles()
        {
            //// dtx output
            this.DtxXDocument.Save(Path.Combine(this.ProgramFolderPath, this.ProgramName + ".dtx"), SaveOptions.None);

            this.CurrentPostFile.FileName = this.ProgramName;
            this.CurrentPostFile.FileExtension = ".dtx";

            //// pjx output
            this.PjxXDocument.Save(Path.Combine(this.ProgramFolderPath, this.ProgramName + ".pjx"), SaveOptions.None);

            //// Start program output
            this.StartXDocument.Save(Path.Combine(this.ProgramFolderPath, "start.pgx"), SaveOptions.None);

            //// Stop program output
            this.StopXDocument.Save(Path.Combine(this.ProgramFolderPath, "stop.pgx"), SaveOptions.None);

            if (this.ExternalAxesJoints.Any())
            {
                //// SetLink program output
                this.SetLinkXDocument.Save(Path.Combine(this.ProgramFolderPath, "setLink.pgx"), SaveOptions.None);
            }

            //// Lasman output
            if (this.LasmanOutput)
            {
                this.LasmanPostFile.GenerateFiles(this.ProgramFolderPath);
            }
        }

        /// <inheritdoc />
        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            CbtNode previousOperationNode = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            if (this.LasmanOutput && (previousOperationNode == null || (operationNode != this.FirstTaskNode && OperationManager.GetOperationType(operationNode) == OperationType.Task)))
            {
                this.LasmanOperationCounter++;
                this.FormatBeforeOperationLasman(operationNode);
            }
        }

        /// <inheritdoc />
        internal override void RunAfterOperationOutput(CbtNode operationNode)
        {
            CbtNode nextOperationNode = OperationManager.GetNextOperationNodeInProgram(operationNode);

            if (this.LasmanOutput && (nextOperationNode == null || (nextOperationNode != this.FirstTaskNode && OperationManager.GetOperationType(nextOperationNode) == OperationType.Task)))
            {
                this.FormatAfterOperationLasman();
            }
        }

        /// <inheritdoc />
        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            this.ProcessDtxCartesian(point, operationNode, out int pathKey, out int toolKey, out int blendKey);

            this.StartXcDataStringBuilder.AppendLine(string.Format(
                "      " + "{0}movel{1}({2}[{3}]{4},{5}[{6}],mdLineBlend[{7}])",
                this.ExternalAxesJoints.Any() ? "$X" : this.IsRtcp ? "$" : string.Empty,
                this.IsRtcp ? "p" : string.Empty,
                this.RoutingPathName,
                pathKey,
                this.ExternalAxesJoints.Any() ? this.FormatExternalAxesValues(point) : string.Empty,
                this.ToolFrameName,
                toolKey,
                blendKey));

            if (this.LasmanOutput)
            {
                this.LasmanStreamWriter.WriteLine(this.FormatLinearMoveLasman(point, operationNode));
            }
        }

        /// <inheritdoc />
        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            this.ProcessDtxCartesian(point, operationNode, out int pathKey, out int toolKey, out int blendKey);

            if (point.IsArcMiddlePoint())
            {
                this.StartXcDataStringBuilder.AppendLine(string.Format(
                    "      " + "{0}movec{1}({2}[{3}],{2}[{4}]{5},{6}[{7}],mdLineBlend[{8}])",
                    this.ExternalAxesJoints.Any() ? "$X" : this.IsRtcp ? "$" : string.Empty,
                    this.IsRtcp ? "p" : string.Empty,
                    this.RoutingPathName,
                    pathKey,
                    pathKey + 1,
                    this.ExternalAxesJoints.Any() ? this.FormatExternalAxesValues(point) : string.Empty,
                    this.ToolFrameName,
                    toolKey,
                    blendKey));
            }

            if (this.LasmanOutput)
            {
                this.LasmanStreamWriter.Write(this.FormatCircularMoveLasman(point, operationNode));
            }
        }

        /// <inheritdoc />
        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            this.ProcessDtxCartesian(point, operationNode, out int pathKey, out int toolKey, out int blendKey);

            this.StartXcDataStringBuilder.AppendLine(string.Format(
                "      " + "{0}movej{1}({2}[{3}]{4},{5}[{6}],mdJointBlend[{7}])",
                this.ExternalAxesJoints.Any() ? "$X" : this.IsRtcp ? "$" : string.Empty,
                this.IsRtcp ? "p" : string.Empty,
                this.RoutingPathName,
                pathKey,
                this.ExternalAxesJoints.Any() ? this.FormatExternalAxesValues(point) : string.Empty,
                this.ToolFrameName,
                toolKey,
                blendKey));

            if (this.LasmanOutput)
            {
                this.LasmanStreamWriter.WriteLine(this.FormatJointMoveLasman(point, operationNode));
            }
        }

        /// <inheritdoc />
        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.ProcessDtxJoint(point, operationNode, out int pathKey, out int toolKey, out int blendKey);

            if (OperationManager.GetOperationType(operationNode) == OperationType.Home)
            {
                this.StartXcDataStringBuilder.AppendLine("      " + "\r\n" + "      " + "// move home");
                this.StartXcDataStringBuilder.AppendLine(string.Format(
                    "      " + "{0}movej{1}(jtPoints[{2}]{3},{4}[{5}],mdJointBlend[{6}])",
                    this.ExternalAxesJoints.Any() ? "$X" : this.IsRtcp ? "$" : string.Empty,
                    this.IsRtcp ? "p" : string.Empty,
                    pathKey,
                    this.ExternalAxesJoints.Any() ? this.FormatExternalAxesValues(point) : string.Empty,
                    this.IsRtcp ? "flange" : "tTool",
                    toolKey,
                    blendKey));
                this.StartXcDataStringBuilder.AppendLine("      " + "waitEndMove()" + "\r\n" + "      ");
            }
            else
            {
                this.StartXcDataStringBuilder.AppendLine(string.Format(
                    "      " + "{0}movej{1}(jtPoints[{2}]{3},{4}[{5}],mdJointBlend[{6}])",
                    this.ExternalAxesJoints.Any() ? "$X" : this.IsRtcp ? "$" : string.Empty,
                    this.IsRtcp ? "p" : string.Empty,
                    pathKey,
                    this.ExternalAxesJoints.Any() ? this.FormatExternalAxesValues(point) : string.Empty,
                    this.IsRtcp ? "flange" : "tTool",
                    toolKey,
                    blendKey));
            }

            if (this.LasmanOutput)
            {
                this.LasmanStreamWriter.WriteLine(this.FormatJointMoveLasman(point, operationNode));
            }
        }

        /// <summary>
        ///     Generic Staubli method to add an XElement to a parent XElement and update the "size" of this parent XElement.
        /// <para>
        ///     The parent XElement is the first <paramref name="descendantXName"/> XElement of <paramref name="document"/> with <paramref name="attribute"/> equal to <paramref name="value"/>.
        /// </para>
        /// </summary>
        /// <param name="document">Document containing the parent XElement.</param>
        /// <param name="descendantXName">Parent XElement XName.</param>
        /// <param name="attribute">Attribute the parent XElement must contain.</param>
        /// <param name="value">Value the <paramref name="attribute"/> must be equal to.</param>
        /// <param name="elementToAdd">XElement to add to parent XElement.</param>
        internal virtual void AddXElementWithSizeUpdate(XDocument document, XName descendantXName, string attribute, string value, XElement elementToAdd)
        {
            XElement myParentNode = document.Descendants(descendantXName)
                                            .FirstOrDefault(node => node.Attribute(attribute)?.Value == value);

            myParentNode?.Add(elementToAdd);
            myParentNode.Attribute("size").Value = (int.Parse(myParentNode.Attribute("size")?.Value) + 1).ToString();
        }

        /// <summary>
        ///     Generic Staubli method to get the "size" an queried XElement which is the first <paramref name="descendantXName"/> XElement of <paramref name="document"/> with <paramref name="attribute"/> equal to <paramref name="value"/>.
        /// </summary>
        /// <param name="document">Document containing the queried XElement.</param>
        /// <param name="descendantXName">Queried XElement XName.</param>
        /// <param name="attribute">Attribute the queried XElement must contain.</param>
        /// <param name="value">Value the <paramref name="attribute"/> must be equal to.</param>
        /// <returns>The size of the queried XElement.</returns>
        internal virtual int GetXElementSize(XDocument document, XName descendantXName, string attribute, string value) => document.Descendants(descendantXName).FirstOrDefault(node => node.Attribute(attribute)?.Value == value).Descendants().Count();

        /// <summary>
        ///     Generic Staubli method to add a Cartesian point to the <see cref="DtxXDocument"/> database.
        ///     Adds the new user frame, tool, frame, motion settings if necessary.
        ///     Keys are passed by reference to be used in the <see cref="StartXcDataStringBuilder"/> once calculated.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="pathKey">Routing Path key.</param>
        /// <param name="toolKey">Tool Frame key.</param>
        /// <param name="blendKey">mdLineBlend key.</param>
        internal virtual void ProcessDtxCartesian(PathNode point, CbtNode operationNode, out int pathKey, out int toolKey, out int blendKey)
        {
            // Process Tool Frame
            toolKey = this.ListOfUsedToolFrames.FindIndex(a => a == OperationManager.GetTcpId(operationNode));
            this.AddToolFrameXElement(operationNode, ref toolKey);

            // Process User Frame
            int userFrameKey = this.ListOfUsedUserFrames.FindIndex(a => a == OperationManager.GetUserFrame(operationNode).Number);
            this.AddUserFrameXElement(operationNode, ref userFrameKey);

            // Process Path Point Frame
            this.AddPathPointFrameXElement(point, operationNode, userFrameKey, out pathKey);

            //// Process Movement Parameters
            blendKey = -1;
            this.ProcessMovementParameters(point, operationNode, ref blendKey);
        }

        /// <summary>
        ///     Generic Staubli method to add a joint point to the <see cref="DtxXDocument"/> database.
        ///     Adds the new tool frame and motion settings if necessary.
        ///     Keys are passed by reference to be used in the <see cref="StartXcDataStringBuilder"/> once calculated.
        /// </summary>
        /// <param name="currentPoint">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="pathKey">jtPoints key.</param>
        /// <param name="toolKey">tTool key.</param>
        /// <param name="blendKey">mdJointBlend key.</param>
        internal virtual void ProcessDtxJoint(PathNode currentPoint, CbtNode operationNode, out int pathKey, out int toolKey, out int blendKey)
        {
            // Process Tool Frame
            toolKey = this.ListOfUsedToolFrames.FindIndex(a => a == OperationManager.GetTcpId(operationNode));
            this.AddToolFrameXElement(operationNode, ref toolKey);

            //// Process JJ Output
            this.AddJointMovesInJointSpaceXElement(currentPoint, out pathKey);

            //// Process Movement Parameters
            blendKey = -1;
            this.ProcessMovementParameters(currentPoint, operationNode, ref blendKey);
        }

        /// <summary>
        ///     Processes joint moves in joint space by adding it to the XElement.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="pathKey">ptRoutingPath key.</param>
        internal virtual void AddJointMovesInJointSpaceXElement(PathNode point, out int pathKey)
        {
            pathKey = this.GetXElementSize(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", "jtPoints");

            var pointOutput = new XElement(this.NameSpaceDtx + "Value");
            pointOutput.Add(new XAttribute("key", pathKey));
            pointOutput.Add(new XAttribute("j1", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J1")).Name]).Formatted()));
            pointOutput.Add(new XAttribute("j2", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J2")).Name]).Formatted()));
            pointOutput.Add(new XAttribute("j3", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J3")).Name]).Formatted()));
            pointOutput.Add(new XAttribute("j4", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J4")).Name]).Formatted()));
            pointOutput.Add(new XAttribute("j5", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J5")).Name]).Formatted()));
            pointOutput.Add(new XAttribute("j6", point.JointValue(this.JointFormat[this.RobotJoints.Single(j => j.Name.Contains("J6")).Name]).Formatted()));

            this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", "jtPoints", pointOutput);
        }

        /// <summary>
        ///     Processes the path node frame by adding it to the XElement.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="userFrameKey">fUserFrame key.</param>
        /// <param name="pathKey">ptRoutingPath key.</param>
        internal virtual void AddPathPointFrameXElement(PathNode point, CbtNode operationNode, int userFrameKey, out int pathKey)
        {
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 pointPositionInUserFrame = pointFrameInUserFrame.Position;
            Vector3 tcpOrientationInUserFrame = FormatManagerStaubli.ToEuler(pointFrameInUserFrame.Orientation);

            pathKey = this.GetXElementSize(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", this.RoutingPathName);

            var pointOutput = new XElement(this.NameSpaceDtx + "Value");
            pointOutput.Add(new XAttribute("key", pathKey));
            pointOutput.Add(new XAttribute("x", pointPositionInUserFrame.X.Formatted()));
            pointOutput.Add(new XAttribute("y", pointPositionInUserFrame.Y.Formatted()));
            pointOutput.Add(new XAttribute("z", pointPositionInUserFrame.Z.Formatted()));
            pointOutput.Add(new XAttribute("rx", tcpOrientationInUserFrame.X.Formatted()));
            pointOutput.Add(new XAttribute("ry", tcpOrientationInUserFrame.Y.Formatted()));
            pointOutput.Add(new XAttribute("rz", tcpOrientationInUserFrame.Z.Formatted()));

            if (!this.IsRtcp)
            {
                pointOutput.Add(new XAttribute("shoulder", FormatManagerStaubli.BaseConfigFormatted(this.CachedRobotConfig.BaseConfiguration)));
                pointOutput.Add(new XAttribute("elbow", FormatManagerStaubli.ElbowConfigFormatted(this.CachedRobotConfig.ElbowConfiguration)));
                pointOutput.Add(new XAttribute("wrist", FormatManagerStaubli.WristConfigFormatted(this.CachedRobotConfig.WristConfiguration)));
            }

            pointOutput.Add(new XAttribute("fatherId", $@"{this.PartFrameName}[{userFrameKey}]"));

            if (this.IsRtcp)
            {
                pointOutput.Add(new XAttribute("ioLink", "valve1"));
            }

            this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", this.RoutingPathName, pointOutput);
        }

        /// <summary>
        ///     Processes the tool frame by adding it to the XElement.
        /// </summary>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="toolKey">tTool key.</param>
        internal virtual void AddToolFrameXElement(CbtNode operationNode, ref int toolKey)
        {
            //// Process tTool
            if (toolKey == -1)
            {
                Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

                if (this.EnableToolFrameExtraRotation)
                {
                    toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
                }

                Vector3 toolFramePosition = toolFrameMatrix.Position;
                Vector3 toolFrameOrientation = FormatManagerStaubli.ToEuler(toolFrameMatrix.Orientation);

                this.ListOfUsedToolFrames.Add(OperationManager.GetTcpId(operationNode));
                toolKey = this.ListOfUsedToolFrames.Count - 1;

                var toolFrameOutput = new XElement(this.NameSpaceDtx + "Value");
                toolFrameOutput.Add(new XAttribute("key", toolKey));
                toolFrameOutput.Add(new XAttribute("x", toolFramePosition.X.Formatted()));
                toolFrameOutput.Add(new XAttribute("y", toolFramePosition.Y.Formatted()));
                toolFrameOutput.Add(new XAttribute("z", toolFramePosition.Z.Formatted()));
                toolFrameOutput.Add(new XAttribute("rx", toolFrameOrientation.X.Formatted()));
                toolFrameOutput.Add(new XAttribute("ry", toolFrameOrientation.Y.Formatted()));
                toolFrameOutput.Add(new XAttribute("rz", toolFrameOrientation.Z.Formatted()));

                if (this.IsRtcp)
                {
                    toolFrameOutput.Add(new XAttribute("shoulder", FormatManagerStaubli.BaseConfigFormatted(this.CachedRobotConfig.BaseConfiguration)));
                    toolFrameOutput.Add(new XAttribute("elbow", FormatManagerStaubli.ElbowConfigFormatted(this.CachedRobotConfig.ElbowConfiguration)));
                    toolFrameOutput.Add(new XAttribute("wrist", FormatManagerStaubli.WristConfigFormatted(this.CachedRobotConfig.WristConfiguration)));
                }

                toolFrameOutput.Add(new XAttribute("fatherId", this.IsRtcp ? "world[0]" : "flange[0]"));

                if (!this.IsRtcp)
                {
                    toolFrameOutput.Add(new XAttribute("ioLink", "valve1"));
                }

                this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", this.ToolFrameName, toolFrameOutput);
            }
        }

        /// <summary>
        ///     Processes the user frame by adding it to the XElement.
        /// </summary>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="userFrameKey">fUserFrame key.</param>
        internal virtual void AddUserFrameXElement(CbtNode operationNode, ref int userFrameKey)
        {
            //// Process fUserFrame
            if (userFrameKey == -1)
            {
                UserFrame userFrame = OperationManager.GetUserFrame(operationNode);
                CbtNode referenceFrame = SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode;
                Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrame);
                Vector3 userFramePosition = userFrameMatrix.Position;
                Vector3 userFrameOrientation = FormatManagerStaubli.ToEuler(userFrameMatrix.Orientation);

                this.ListOfUsedUserFrames.Add(OperationManager.GetUserFrame(operationNode).Number);
                userFrameKey = this.ListOfUsedUserFrames.Count - 1;

                var fUserFrameOutput = new XElement(this.NameSpaceDtx + "Value");
                fUserFrameOutput.Add(new XAttribute("key", userFrameKey));
                fUserFrameOutput.Add(new XAttribute("x", userFramePosition.X.Formatted()));
                fUserFrameOutput.Add(new XAttribute("y", userFramePosition.Y.Formatted()));
                fUserFrameOutput.Add(new XAttribute("z", userFramePosition.Z.Formatted()));
                fUserFrameOutput.Add(new XAttribute("rx", userFrameOrientation.X.Formatted()));
                fUserFrameOutput.Add(new XAttribute("ry", userFrameOrientation.Y.Formatted()));
                fUserFrameOutput.Add(new XAttribute("rz", userFrameOrientation.Z.Formatted()));
                fUserFrameOutput.Add(new XAttribute("fatherId", this.IsRtcp ? "flange[0]" : "world[0]"));

                if (this.IsRtcp)
                {
                    fUserFrameOutput.Add(new XAttribute("ioLink", "valve1"));
                }

                this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + "Data", "name", this.PartFrameName, fUserFrameOutput);
            }
        }

        /// <summary>
        ///     Add the movement XElement.
        /// </summary>
        /// <param name="mvtParameters">The <see cref="MovementParameters"/>.</param>
        /// <param name="movementType">The movement type.</param>
        /// <param name="matchedIndex">The matched index.</param>
        /// <param name="descendantName">The descendant name.</param>
        /// <param name="attributeMember">The attribute member.</param>
        /// <param name="value">The value.</param>
        internal virtual void AddMovementXElement(MovementParameters mvtParameters, MoveType movementType, int matchedIndex, string descendantName, string attributeMember, string value)
        {
            if (matchedIndex != -1)
            {
                return;
            }

            int blendKey = this.GetXElementSize(this.DtxXDocument, this.NameSpaceDtx + descendantName, attributeMember, value);
            XElement blendOutput = this.CreateMovementParametersXElement(mvtParameters, blendKey);
            this.AddXElementWithSizeUpdate(this.DtxXDocument, this.NameSpaceDtx + descendantName, attributeMember, value, blendOutput);

            if (movementType == MoveType.Rapid)
            {
                this.UsedJointMovementParameters.Add(mvtParameters);
            }
            else
            {
                this.UsedCartesianMovementParameters.Add(mvtParameters);
            }
        }

        /// <summary>
        ///     Generic Staubli method to add a mdBlend element to the <see cref="DtxXDocument"/> database.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <param name="blendKey">mdBlend key.</param>
        internal virtual void ProcessMovementParameters(PathNode point, CbtNode operationNode, ref int blendKey)
        {
            const string descendantName = "Data";
            const string attributeMember = "name";
            const string cartesianValue = "mdLineBlend";
            const string jointValue = "mdJointBlend";

            MoveType moveType = point.MoveType();

            switch (moveType)
            {
                case MoveType.Linear:
                    {
                        var testBlend = new MovementParameters
                        {
                            Acceleration = StaubliMotionSettings.GetLinearMotionJointAcceleration(operationNode),
                            Velocity = StaubliMotionSettings.GetLinearMotionMaximumJointSpeed(operationNode),
                            Deceleration = StaubliMotionSettings.GetLinearMotionJointDeceleration(operationNode),
                            MaximumTranslationalSpeedOfTcp = point.Feedrate().LinearFeedrate,
                            MaximumRotationalSpeedOfTool = 9999,
                            IsJointBlending = StaubliMotionSettings.GetIsLinearMotionBlendingEnabled(operationNode),
                            Leave = StaubliMotionSettings.GetLinearMotionLeaveDistance(operationNode),
                            Reach = StaubliMotionSettings.GetLinearMotionReachDistance(operationNode),
                        };

                        int matchedCartesianMovementIndex = this.UsedCartesianMovementParameters.FindIndex(linMovement => this.AreMovementParametersEqual(linMovement, testBlend));
                        blendKey = matchedCartesianMovementIndex == -1 ? this.UsedCartesianMovementParameters.Count : matchedCartesianMovementIndex; // If index is not found, next index will be the list count
                        this.AddMovementXElement(testBlend, moveType, matchedCartesianMovementIndex, descendantName, attributeMember, cartesianValue);
                        break;
                    }

                case MoveType.Circular when point.IsArcMiddlePoint():
                    {
                        var testBlend = new MovementParameters
                        {
                            Acceleration = StaubliMotionSettings.GetCircularMotionJointAcceleration(operationNode),
                            Velocity = StaubliMotionSettings.GetCircularMotionJointMaximumSpeed(operationNode),
                            Deceleration = StaubliMotionSettings.GetCircularMotionJointDeceleration(operationNode),
                            MaximumTranslationalSpeedOfTcp = point.Feedrate().LinearFeedrate,
                            MaximumRotationalSpeedOfTool = 9999,
                            IsJointBlending = StaubliMotionSettings.GetIsCircularMotionBlendingEnabled(operationNode),
                            Leave = StaubliMotionSettings.GetCircularMotionLeaveDistance(operationNode),
                            Reach = StaubliMotionSettings.GetCircularMotionReachDistance(operationNode),
                        };

                        int matchedCartesianMovementIndex = this.UsedCartesianMovementParameters.FindIndex(circMovement => this.AreMovementParametersEqual(circMovement, testBlend));
                        blendKey = matchedCartesianMovementIndex == -1 ? this.UsedCartesianMovementParameters.Count : matchedCartesianMovementIndex; // If index is not found, next index will be the list count
                        this.AddMovementXElement(testBlend, moveType, matchedCartesianMovementIndex, descendantName, attributeMember, cartesianValue);
                        break;
                    }

                case MoveType.Rapid:
                    {
                        var testBlend = new MovementParameters
                        {
                            Acceleration = StaubliMotionSettings.GetJointMotionAcceleration(operationNode),
                            Velocity = StaubliMotionSettings.GetJointMotionMaximumSpeed(operationNode),
                            Deceleration = StaubliMotionSettings.GetJointMotionDeceleration(operationNode),
                            MaximumTranslationalSpeedOfTcp = StaubliMotionSettings.GetMaximumTranslationalVelocityOfTcp(operationNode),
                            MaximumRotationalSpeedOfTool = StaubliMotionSettings.GetMaximumRotationalVelocityOfTcp(operationNode),
                            IsJointBlending = StaubliMotionSettings.GetIsJointMotionBlendingEnabled(operationNode),
                            Leave = StaubliMotionSettings.GetJointMotionLeaveDistance(operationNode),
                            Reach = StaubliMotionSettings.GetJointMotionReachDistance(operationNode),
                        };

                        int matchedJointMovementIndex = this.UsedJointMovementParameters.FindIndex(jointMovement => this.AreMovementParametersEqual(jointMovement, testBlend));
                        blendKey = matchedJointMovementIndex == -1 ? this.UsedJointMovementParameters.Count : matchedJointMovementIndex; // If index is not found, next index will be the list count
                        this.AddMovementXElement(testBlend, moveType, matchedJointMovementIndex, descendantName, attributeMember, jointValue);
                        break;
                    }
            }
        }

        /// <summary>
        ///     Creates a <see cref="MovementParameters"/> XElement from a <see cref="MovementParameters"/> object.
        /// </summary>
        /// <param name="blend"><see cref="MovementParameters"/> object.</param>
        /// <param name="key">Key of the XElement.</param>
        /// <returns>Resulting XElement.</returns>
        internal virtual XElement CreateMovementParametersXElement(MovementParameters blend, int key)
        {
            var blendOutput = new XElement(this.NameSpaceDtx + "Value");
            blendOutput.Add(new XAttribute("key", key));
            blendOutput.Add(new XAttribute("accel", $"{blend.Acceleration:0.###}"));
            blendOutput.Add(new XAttribute("vel", $"{blend.Velocity:0.###}"));
            blendOutput.Add(new XAttribute("decel", $"{blend.Deceleration:0.###}"));
            blendOutput.Add(new XAttribute("tmax", $"{blend.MaximumTranslationalSpeedOfTcp:0.###}"));
            blendOutput.Add(new XAttribute("rmax", $"{blend.MaximumRotationalSpeedOfTool:0.###}"));
            blendOutput.Add(new XAttribute("blend", blend.IsJointBlending ? "joint" : "off"));
            blendOutput.Add(new XAttribute("leave", $"{blend.Leave:0.###}"));
            blendOutput.Add(new XAttribute("reach", $"{blend.Reach:0.###}"));

            return blendOutput;
        }

        /// <inheritdoc />
        internal override bool IsProgramInputValid()
        {
            // Verifies if the device has the "label" for the rail and rotaries axes properly defined in the ROBX file.
            if (!this.IsExternalAxesNameValid())
            {
                this.NotifyUser("ERROR: The rail/rotary axes \"label\" tag in the ROBX file is not valid. Posting will be stopped.", true, false);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Gets a value indicating whether the "label" tag(s) found in the ROBX for the rail and rotary axes are valid.
        /// </summary>
        /// <returns><c>true</c> if the "label" tag(s) for the rail and rotary axes are valid; otherwise, <c>false</c>.</returns>
        internal virtual bool IsExternalAxesNameValid()
        {
            var externalAxes = new List<string>() { "R1", "R2", "E1", "E2", "E3" };
            return !(this.Rails.Any() && this.Rails.Any(x => !externalAxes.Contains(x.Name))) &&
                   !(this.Rotaries.Any() && this.Rotaries.Any(x => !externalAxes.Contains(x.Name)));
        }

        /// <summary>
        ///     Verifies if the <see cref="MovementParameters"/> are equal.
        /// </summary>
        /// <param name="blend1">First <see cref="MovementParameters"/>.</param>
        /// <param name="blend2">Second <see cref="MovementParameters"/>.</param>
        /// <returns><c>true</c> if the <see cref="MovementParameters"/> are equal; otherwise, <c>false</c>.</returns>
        internal virtual bool AreMovementParametersEqual(MovementParameters blend1, MovementParameters blend2)
        {
            return blend1.IsJointBlending == blend2.IsJointBlending &&
                   Math.Abs(blend1.Acceleration - blend2.Acceleration) < 0.01 &&
                   Math.Abs(blend1.Deceleration - blend2.Deceleration) < 0.01 &&
                   Math.Abs(blend1.Leave - blend2.Leave) < 0.01 &&
                   Math.Abs(blend1.Reach - blend2.Reach) < 0.01 &&
                   Math.Abs(blend1.MaximumRotationalSpeedOfTool - blend2.MaximumRotationalSpeedOfTool) < 0.01 &&
                   Math.Abs(blend1.MaximumTranslationalSpeedOfTcp - blend2.MaximumTranslationalSpeedOfTcp) < 0.01 &&
                   Math.Abs(blend1.Velocity - blend2.Velocity) < 0.01;
        }

        /// <summary>
        ///     Formats external axes values output at a given point.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Staubli External Axes.</returns>
        internal virtual string FormatExternalAxesValues(PathNode point)
        {
            XElement dataXElement;

            if (point.MoveType() == MoveType.Linear ||
                point.MoveType() == MoveType.Circular ||
                (point.MoveType() == MoveType.Rapid && point.PathSetting().MotionType != DeviceMotionType.JointSpaceMove))
            {
                // Find Last nTarget
                IEnumerable<XElement> myNodeIEnumerable =
                    from e in this.DtxXDocument.Descendants(this.NameSpaceDtx + "Datas").Descendants()
                    where e.Attribute("name") != null && e.Attribute("name").Value.StartsWith("nTarget") && !e.Attribute("name").Value.StartsWith("nTargetJoint")
                    select e;

                if (!myNodeIEnumerable.Any())
                {
                    // Find Last nTargetJoint
                    myNodeIEnumerable =
                        from e in this.DtxXDocument.Descendants(this.NameSpaceDtx + "Datas").Descendants()
                        where e.Attribute("name") != null && e.Attribute("name").Value.StartsWith("nTargetJoint")
                        select e;

                    if (!myNodeIEnumerable.Any())
                    {
                        // Find Tool Frame
                        myNodeIEnumerable =
                            from e in this.DtxXDocument.Descendants(this.NameSpaceDtx + "Datas").Descendants()
                            where e.Attribute("name") != null && e.Attribute("name").Value.StartsWith(this.ToolFrameName)
                            select e;
                    }
                }

                XElement previousNode = myNodeIEnumerable.LastOrDefault();

                if (point.MoveType() == MoveType.Circular)
                {
                    this.CurrentNTarget++;
                }

                dataXElement = new XElement(this.NameSpaceDtx + "Data");
                dataXElement.SetAttributeValue("name", "nTarget" + this.CurrentNTarget);
                dataXElement.SetAttributeValue("access", "private");
                dataXElement.SetAttributeValue(this.NameSpaceXsi + "type", "array");
                dataXElement.SetAttributeValue("type", "num");
                dataXElement.SetAttributeValue("size", 0);

                previousNode.AddAfterSelf(dataXElement);

                this.CurrentNTarget++;
            }
            else
            {
                // Find Last nTargetJoint
                IEnumerable<XElement> myNodeIEnumerable =
                    from e in this.DtxXDocument.Descendants(this.NameSpaceDtx + "Datas").Descendants()
                    where e.Attribute("name") != null && e.Attribute("name").Value.StartsWith("nTargetJoint")
                    select e;

                if (!myNodeIEnumerable.Any())
                {
                    // Find Tool Frame
                    myNodeIEnumerable =
                        from e in this.DtxXDocument.Descendants(this.NameSpaceDtx + "Datas").Descendants()
                        where e.Attribute("name") != null && e.Attribute("name").Value.StartsWith(this.ToolFrameName)
                        select e;
                }

                XElement previousNode = myNodeIEnumerable.LastOrDefault();

                dataXElement = new XElement(this.NameSpaceDtx + "Data");
                dataXElement.SetAttributeValue("name", "nTargetJoint" + this.CurrentNTargetJoint);
                dataXElement.SetAttributeValue("access", "private");
                dataXElement.SetAttributeValue(this.NameSpaceXsi + "type", "array");
                dataXElement.SetAttributeValue("type", "num");
                dataXElement.SetAttributeValue("size", 0);

                previousNode.AddAfterSelf(dataXElement);

                this.CurrentNTargetJoint++;
            }

            string output = string.Empty;

            var count = 0;

            var rails = new[] { "E1", "E2", "E3" };

            var rotaries = new[] { "R1", "R2" };

            var railIndex = 0;
            var rotaryIndex = 0;

            foreach (Joint rail in this.Rails)
            {
                var valueXElement = new XElement(this.NameSpaceDtx + "Value");
                valueXElement.SetAttributeValue("key", count);
                valueXElement.SetAttributeValue("value", $"{point.JointValue(this.JointFormat[rails[railIndex]]):0.000}");

                dataXElement.Add(valueXElement);

                output += $",{dataXElement.Attribute("name").Value}[{count}]";
                count++;
                railIndex++;
            }

            foreach (Joint rotary in this.Rotaries)
            {
                var valueXElement = new XElement(this.NameSpaceDtx + "Value");
                valueXElement.SetAttributeValue("key", count);
                valueXElement.SetAttributeValue("value", $"{point.JointValue(this.JointFormat[rotaries[rotaryIndex]]):0.000}");

                dataXElement.Add(valueXElement);

                output += $",{dataXElement.Attribute("name").Value}[{count}]";
                count++;
                rotaryIndex++;
            }

            dataXElement.Attribute("size").Value = count.ToString();

            return output;
        }

        /// <inheritdoc />
        internal override void RunBeforePointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.Before);
        }

        /// <inheritdoc />
        internal override void RunAfterPointOutput(PathNode point, CbtNode operationNode)
        {
            this.OutputAllSelectedEvents(point, operationNode, EventIndex.After);
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.StartXcDataStringBuilder.AppendLine("      " + beforePointEvent.ToCode());

            // Lasman logic
            if (this.LasmanOutput)
            {
                if (beforePointEvent is ToolOn)
                {
                    this.LasmanToolOnFlag = true;
                }
                else if (beforePointEvent is ToolOff)
                {
                    this.LasmanToolOnFlag = false;
                }

                this.LasmanStreamWriter.WriteLine(beforePointEvent.ToCode());
            }
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.StartXcDataStringBuilder.AppendLine("      " + afterPointEvent.ToCode());

            // Lasman logic
            if (this.LasmanOutput)
            {
                if (afterPointEvent is ToolOn)
                {
                    this.LasmanToolOnFlag = true;
                }
                else if (afterPointEvent is ToolOff)
                {
                    this.LasmanToolOnFlag = false;
                }

                this.LasmanStreamWriter.WriteLine(afterPointEvent.ToCode());
            }
        }

        /// <summary>
        ///     Formats the Operation Header for the Lasman output.
        /// </summary>
        /// <param name="operationNode">Current operationNode.</param>
        internal virtual void FormatBeforeOperationLasman(CbtNode operationNode)
        {
            //// BEGIN=PROGRAM 1_1
            //// BYPASS = FALSE
            //// CUTTYPE = 10
            //// ARRAY = 0,0,0,0,0,0
            //// TOOL = 226.117,0.000,263.388,0.000,59.998,-180.000
            //// FRAME = 500.000,0.000,0.000,0.000,0.000,0.000
            //// OFFSET = 0,0,0,0,0,0
            //// DOWNLEAD = 0,0,0,0,0,0
            //// WORKPLACE = 0,0,0,0,0,0

            this.LasmanStreamWriter.WriteLine(string.Empty);
            this.LasmanStreamWriter.WriteLine("BEGIN=" + this.LasmanPostFile.FileName + "_" + this.LasmanOperationCounter);
            this.LasmanStreamWriter.WriteLine("BYPASS=FALSE");
            this.LasmanStreamWriter.WriteLine(this.ExternalAxesJoints.Any() ? "CUTTYPE=20" : "CUTTYPE=10");
            this.LasmanStreamWriter.WriteLine("ARRAY=0,0,0,0,0,0");
            this.LasmanStreamWriter.WriteLine(this.FormatToolFrameLasman(operationNode));
            this.LasmanStreamWriter.WriteLine(this.FormatUserFrameLasman(operationNode));
            this.LasmanStreamWriter.WriteLine("OFFSET=0,0,0,0,0,0");
            this.LasmanStreamWriter.WriteLine("DOWNLEAD=0,0,0,0,0,0");
            this.LasmanStreamWriter.WriteLine("WORKPLACE=0,0,0,0,0,0");
        }

        /// <summary>
        ///     Formats the Operation Footer for the Lasman output.
        /// </summary>
        internal virtual void FormatAfterOperationLasman()
        {
            //// END=PROGRAM 1_1

            this.LasmanStreamWriter.WriteLine("END=" + this.LasmanPostFile.FileName + "_" + this.LasmanOperationCounter);
        }

        /// <summary>
        ///     Formats the Tool Frame for the Lasman output.
        /// </summary>
        /// <param name="operationNode">Current operationNode.</param>
        /// <returns>Formatted Tool Frame for Lasman output.</returns>
        internal virtual string FormatToolFrameLasman(CbtNode operationNode)
        {
            Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operationNode);

            if (this.EnableToolFrameExtraRotation)
            {
                toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 toolFramePosition = toolFrameMatrix.Position;
            Vector3 toolFrameOrientation = FormatManagerStaubli.ToEuler(toolFrameMatrix.Orientation);

            return string.Format(
                "TOOL={0},{1},{2},{3},{4},{5}",
                toolFramePosition.X.Formatted(),
                toolFramePosition.Y.Formatted(),
                toolFramePosition.Z.Formatted(),
                toolFrameOrientation.X.Formatted(),
                toolFrameOrientation.Y.Formatted(),
                toolFrameOrientation.Z.Formatted());
        }

        /// <summary>
        ///     Formats the User Frame for the Lasman output.
        /// </summary>
        /// <param name="operationNode">Current operationNode.</param>
        /// <returns>Formatted User Frame for Lasman output.</returns>
        internal virtual string FormatUserFrameLasman(CbtNode operationNode)
        {
            UserFrame userFrame = OperationManager.GetUserFrame(operationNode);
            CbtNode referenceFrame = SetupManager.GetConfiguration(this.SetupNode).UserFrameReferenceNode;
            Matrix4X4 userFrameMatrix = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot, referenceFrame);
            Vector3 userFramePosition = userFrameMatrix.Position;
            Vector3 userFrameOrientation = FormatManagerStaubli.ToEuler(userFrameMatrix.Orientation);

            return string.Format(
                "FRAME={0},{1},{2},{3},{4},{5}",
                userFramePosition.X.Formatted(),
                userFramePosition.Y.Formatted(),
                userFramePosition.Z.Formatted(),
                userFrameOrientation.X.Formatted(),
                userFrameOrientation.Y.Formatted(),
                userFrameOrientation.Z.Formatted());
        }

        /// <summary>
        ///     Formats the joint moves for the Lasman output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <returns>Formatted joint move for Lasman output.</returns>
        internal virtual string FormatJointMoveLasman(PathNode point, CbtNode operationNode)
        {
            //// MOVEJ=-5.656,11.072,76.005,4.169,55.629,-6.849/30,100

            return string.Format(
                "MOVEJ={0},{1},{2},{3},{4},{5}/{6:0},{7:0}{8}",
                point.JointValue(this.JointFormat["J1"]).Formatted(),
                point.JointValue(this.JointFormat["J2"]).Formatted(),
                point.JointValue(this.JointFormat["J3"]).Formatted(),
                point.JointValue(this.JointFormat["J4"]).Formatted(),
                point.JointValue(this.JointFormat["J5"]).Formatted(),
                point.JointValue(this.JointFormat["J6"]).Formatted(),
                StaubliMotionSettings.GetJointMotionMaximumSpeed(operationNode),
                StaubliMotionSettings.GetJointMotionAcceleration(operationNode),
                this.ExternalAxesJoints.Any() ? "/" + this.FormatExternalAxesLasman(point) : string.Empty);
        }

        /// <summary>
        ///     Formats the Home position for the Lasman output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted Home position for Lasman output.</returns>
        internal virtual string FormatHomePositionLasman(PathNode point)
        {
            //// HOME=0.000,0.000,90.000,0.000,0.000,0.000

            return string.Format(
                "HOME={0},{1},{2},{3},{4},{5}",
                point.JointValue(this.JointFormat["J1"]).Formatted(),
                point.JointValue(this.JointFormat["J2"]).Formatted(),
                point.JointValue(this.JointFormat["J3"]).Formatted(),
                point.JointValue(this.JointFormat["J4"]).Formatted(),
                point.JointValue(this.JointFormat["J5"]).Formatted(),
                point.JointValue(this.JointFormat["J6"]).Formatted());
        }

        /// <summary>
        ///     Formats the linear moves for the Lasman output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <returns>Formatted linear move for Lasman output.</returns>
        internal virtual string FormatLinearMoveLasman(PathNode point, CbtNode operationNode)
        {
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 pointPositionInUserFrame = pointFrameInUserFrame.Position;
            Vector3 tcpOrientationInUserFrame = FormatManagerStaubli.ToEuler(pointFrameInUserFrame.Orientation);

            //// MOVEL=85.468,3.800,-35.271,178.950,-22.424,-0.208/20,100

            return string.Format(
                "MOVEL={0},{1},{2},{3},{4},{5}{6}/{7:0},{8:0}{9}{10}",
                pointPositionInUserFrame.X.Formatted(),
                pointPositionInUserFrame.Y.Formatted(),
                pointPositionInUserFrame.Z.Formatted(),
                tcpOrientationInUserFrame.X.Formatted(),
                tcpOrientationInUserFrame.Y.Formatted(),
                tcpOrientationInUserFrame.Z.Formatted(),
                this.LasmanToolOnFlag ? "/1,0" : string.Empty,
                point.Feedrate().LinearFeedrate,
                StaubliMotionSettings.GetLinearMotionJointAcceleration(operationNode),
                this.LasmanToolOnFlag ? "," + (StaubliMotionSettings.GetIsLinearMotionBlendingEnabled(operationNode) ? StaubliMotionSettings.GetLinearMotionLeaveDistance(operationNode) : 0) : string.Empty,
                this.ExternalAxesJoints.Any() ? "/" + this.FormatExternalAxesLasman(point) : string.Empty);
        }

        /// <summary>
        ///     Formats the circular moves for the Lasman output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <param name="operationNode">Current operationNode.</param>
        /// <returns>Formatted circular move for Lasman output.</returns>
        internal virtual string FormatCircularMoveLasman(PathNode point, CbtNode operationNode)
        {
            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 pointPositionInUserFrame = pointFrameInUserFrame.Position;
            Vector3 tcpOrientationInUserFrame = FormatManagerStaubli.ToEuler(pointFrameInUserFrame.Orientation);

            //// MOVEC=127.929,-41.071,0.000,180.000,0.000,-45.000/125.000,-34.000,0.000,180.000,0.000,-45.000/1,0/12,90,0/0.000,0.000

            if (point.IsArcMiddlePoint())
            {
                return string.Format(
                    "MOVEC={0},{1},{2},{3},{4},{5}",
                    pointPositionInUserFrame.X.Formatted(),
                    pointPositionInUserFrame.Y.Formatted(),
                    pointPositionInUserFrame.Z.Formatted(),
                    tcpOrientationInUserFrame.X.Formatted(),
                    tcpOrientationInUserFrame.Y.Formatted(),
                    tcpOrientationInUserFrame.Z.Formatted());
            }
            else
            {
                return string.Format(
                    "/{0},{1},{2},{3},{4},{5}{6}/{7:0},{8:0}{9}{10}\r\n",
                    pointPositionInUserFrame.X.Formatted(),
                    pointPositionInUserFrame.Y.Formatted(),
                    pointPositionInUserFrame.Z.Formatted(),
                    tcpOrientationInUserFrame.X.Formatted(),
                    tcpOrientationInUserFrame.Y.Formatted(),
                    tcpOrientationInUserFrame.Z.Formatted(),
                    this.LasmanToolOnFlag ? "/1,0" : string.Empty,
                    point.Feedrate().LinearFeedrate,
                    StaubliMotionSettings.GetCircularMotionJointAcceleration(operationNode),
                    this.LasmanToolOnFlag ? "," + (StaubliMotionSettings.GetIsCircularMotionBlendingEnabled(operationNode) ? StaubliMotionSettings.GetCircularMotionLeaveDistance(operationNode) : 0) : string.Empty,
                    this.ExternalAxesJoints.Any() ? "/" + this.FormatExternalAxesLasman(point) : string.Empty);
            }
        }

        /// <summary>
        ///     Formats the external axes for the Lasman output.
        /// </summary>
        /// <param name="point">Current point.</param>
        /// <returns>Formatted external axes for Lasman output.</returns>
        internal virtual string FormatExternalAxesLasman(PathNode point)
        {
            string output = string.Empty;

            var rails = new[] { "E1", "E2", "E3" };
            var rotaries = new[] { "R1", "R2" };

            var railIndex = 0;
            var rotaryIndex = 0;

            foreach (Joint rail in this.Rails)
            {
                output += string.Format("{0}{1}", railIndex > 0 ? "," : string.Empty, this.JointFormat.TryGetValue(rails[railIndex], out int i) ? point.JointValue(i).Formatted() : string.Empty);

                railIndex++;
            }

            foreach (Joint rotary in this.Rotaries)
            {
                output += string.Format("{0}{1}", (railIndex > 0 | rotaryIndex > 0) ? "," : string.Empty, this.JointFormat.TryGetValue(rotaries[rotaryIndex], out int i) ? point.JointValue(i).Formatted() : string.Empty);

                rotaryIndex++;
            }

            return output;
        }
    }
}
