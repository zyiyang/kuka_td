﻿// <copyright file="StaubliValLaserProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.ValLaser.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Processor.Staubli.Default.MainProcessor;
    using Robotmaster.Processor.Staubli.ValLaser.PostProcessor;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    /// Defines Staubli ValLaser process.
    /// </summary>
    [DataContract(Name = "StaubliValLaserProcess")]
    public class StaubliValLaserProcess : PackageProcess, IStaubliProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaubliValLaserProcess"/> class.
        /// </summary>
        public StaubliValLaserProcess()
        {
            this.Name = "Staubli ValLaser Process";
            this.PostProcessorType = typeof(PostProcessorStaubliValLaser);
            this.MainProcessorType = typeof(MainProcessorStaubli);
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
