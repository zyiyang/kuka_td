﻿// <copyright file="PostProcessorStaubliValLaser.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Staubli.ValLaser.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Staubli.Default.PostProcessor;
    using Robotmaster.Rise.Event;

    /// <summary>
    /// This class inherits from <see cref="PostProcessorStaubli"/>.
    /// <para>Implements the Staubli ValLaser dedicated properties and methods of the post-processor in order to output Staubli ValLaser robot codes.</para>
    /// </summary>
    internal class PostProcessorStaubliValLaser : PostProcessorStaubli
    {
        /// <summary>
        ///     Gets or sets the XML namespace of PJX project file.
        /// </summary>
        internal override XNamespace NameSpacePjx { get; set; } = "http://www.staubli.com/robotics/VAL3/Project/3";

        /// <summary>
        ///     Gets or sets the list of called macros in the PJX file.
        /// </summary>
        internal virtual List<string> ListOfCalledPjxMacros { get; set; }

        /// <inheritdoc />
        internal override void RunBeforeProgramOutput()
        {
            base.RunBeforeProgramOutput();

            this.ListOfCalledPjxMacros = new List<string>();

            this.PjxXDocument.Root?.Element(this.NameSpacePjx + "Libraries")?.Add(new XElement(
                this.NameSpacePjx + "Library",
                new XAttribute("alias", "io"),
                new XAttribute("path", "Disk://io/io.pjx"),
                new XAttribute("autoload", "true"),
                new XAttribute("password", string.Empty)));
        }

        /// <inheritdoc/>
        internal override void OutputBeforePointEvent(Event beforePointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.StartXcDataStringBuilder.AppendLine("      " + "waitEndMove()");
            this.StartXcDataStringBuilder.AppendLine("      " + "call " + beforePointEvent + "()");

            // Call Macro in PJX
            this.CallMacroInPjx(beforePointEvent);

            // Lasman logic
            if (this.LasmanOutput)
            {
                if (beforePointEvent is ToolOn)
                {
                    this.LasmanToolOnFlag = true;
                }
                else if (beforePointEvent is ToolOff)
                {
                    this.LasmanToolOnFlag = false;
                }

                this.LasmanStreamWriter.WriteLine(beforePointEvent);
            }
        }

        /// <inheritdoc/>
        internal override void OutputAfterPointEvent(Event afterPointEvent, PathNode point, CbtNode operationNode)
        {
            // Output event
            this.StartXcDataStringBuilder.AppendLine("      " + "waitEndMove()");
            this.StartXcDataStringBuilder.AppendLine("      " + "call " + afterPointEvent.ToCode() + "()");

            // Call Macro in PJX
            this.CallMacroInPjx(afterPointEvent);

            // Lasman logic
            if (this.LasmanOutput)
            {
                if (afterPointEvent is ToolOn)
                {
                    this.LasmanToolOnFlag = true;
                }
                else if (afterPointEvent is ToolOff)
                {
                    this.LasmanToolOnFlag = false;
                }

                this.LasmanStreamWriter.WriteLine(afterPointEvent.ToCode());
            }
        }

        /// <summary>
        ///     Calls the tooling activation and deactivation macros in the PJX file.
        /// </summary>
        /// <param name="macroEvent">Tooling activation or deactivation macro.</param>
        internal virtual void CallMacroInPjx(Event macroEvent)
        {
            if (!this.ListOfCalledPjxMacros.Contains(macroEvent.ToString(), StringComparer.OrdinalIgnoreCase))
            {
                this.ListOfCalledPjxMacros.Add(macroEvent.ToString());
                this.PjxXDocument.Root?.Element(this.NameSpacePjx + "Programs")?.Add(new XElement(this.NameSpacePjx + "Program", new XAttribute("file", macroEvent + ".pgx")));
            }
        }
    }
}
