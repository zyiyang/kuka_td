﻿// <copyright file="ToshibaDefaultProcess.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Toshiba.Default.Processes
{
    using System.Runtime.Serialization;
    using Robotmaster.Processor.Common.Default.Processes;
    using Robotmaster.Processor.Common.Default.Processes.Events;
    using Robotmaster.Processor.Common.Default.Processes.IBrandProcess;
    using Robotmaster.Rise.ProcessorPackage;

    /// <summary>
    ///     Defines Toshiba default process.
    /// </summary>
    [DataContract(Name = "ToshibaDefaultProcess")]
    public class ToshibaDefaultProcess : PackageProcess, IToshibaProcess
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ToshibaDefaultProcess"/> class.
        /// </summary>
        public ToshibaDefaultProcess()
        {
            this.Name = "Toshiba Default Process";
            this.AddEventToMet(typeof(ToolOn));
            this.AddEventToMet(typeof(ToolOff));
            this.ProcessingMode = ProcessingMode.Decoupled;
        }
    }
}
