﻿// <copyright file="PostProcessorToshiba.cs" company="Hypertherm Robotic Software Inc.">
// Copyright (c) Hypertherm Robotic Software Inc. All rights reserved.
// </copyright>

namespace Robotmaster.Processor.Toshiba.Default.PostProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Robotmaster.ComponentBasedTree;
    using Robotmaster.Kinematics;
    using Robotmaster.Math.Algebra;
    using Robotmaster.PathEngine;
    using Robotmaster.Processor.Common.Default.PostProcessor;
    using Robotmaster.Rise.ApplicationLayer.Managers;
    using Math = System.Math;

    internal class PostProcessorToshiba : PostProcessor
    {
        internal virtual FileSection MovesSection => this.CurrentPostFile.FileSection.SubFileSections[1];

        internal virtual FileSection DataSection => this.CurrentPostFile.FileSection.SubFileSections[2];

        internal override void RunBeforeProgramOutput()
        {
            var firstOperation = ProgramManager.GetOperationNodes(this.ProgramNode).First();
            var firstPoint = OperationManager.GetFirstPoint(firstOperation);
            this.StartPostFile(this.CurrentPostFile, firstOperation, firstPoint);

            this.OutputAllTcpFrames();
            this.OutputAllUserFrames();
        }

        internal virtual void OutputAllTcpFrames()
        {
            var uniqueTcpFrames = new Dictionary<int, Matrix4X4>(); // Dictionary: key: Tcp Id , Value : Tcp Frame

            // For each operation of the program
            foreach (var operation in ProgramManager.GetOperationNodes(this.ProgramNode))
            {
                int tcpId = OperationManager.GetTcpId(operation);

                // If the Tcp Id is not already in the dictionary
                if (!uniqueTcpFrames.ContainsKey(tcpId))
                {
                    // Add the Tcp Id and Tcp Frame pair to the dictionary
                    Matrix4X4 toolFrameMatrix = OperationManager.GetTcpFrameValue(operation);

                    if (this.EnableToolFrameExtraRotation)
                    {
                        toolFrameMatrix.Orientation = toolFrameMatrix.Orientation * this.ToolFrameExtraRotation;
                    }

                    uniqueTcpFrames.Add(tcpId, toolFrameMatrix);
                }
            }

            // Output the tool frame
            foreach (var tcpFrame in uniqueTcpFrames)
            {
                //// Data tool output
                //// Example:
                //// TRANS TL1                    =   100.000,     0.000,   -50.000,     0.000,     0.000,     0.000

                Matrix4X4 tcpFrameValue = tcpFrame.Value;
                Vector3 tcpEulerAngles = this.RobotFormatter.MatrixToEuler(tcpFrameValue);

                var dataLine = string.Format(
                    "{0,-28:TRANS TL0}={1,10:0.000},{2,10:0.000},{3,10:0.000},{4,10:0.000},{5,10:0.000},{6,10:0.000}",
                    tcpFrame.Key,
                    tcpFrameValue.Position.X,
                    tcpFrameValue.Position.Y,
                    tcpFrameValue.Position.Z,
                    tcpEulerAngles.X,
                    tcpEulerAngles.Y,
                    tcpEulerAngles.Z);
                this.DataSection.StreamWriter.WriteLine(dataLine);
            }
        }

        internal virtual void OutputAllUserFrames()
        {
            var uniqueUserFrames = ProgramManager.GetOperationNodes(this.ProgramNode).Select(op => OperationManager.GetUserFrame(op)).Distinct();

            // Output the work frame
            foreach (var userFrame in uniqueUserFrames)
            {
                //// Data tool output
                //// Example:
                //// TRANS WK1                    =   100.000,     0.000,   -50.000,     0.000,     0.000,     0.000

                Matrix4X4 userFrameValue = UserFrameManager.GetInitialValuesInReferenceFrame(userFrame, this.SceneCbtRoot, this.OperationCbtRoot);
                Vector3 userFrameEulerAngles = this.RobotFormatter.MatrixToEuler(userFrameValue);

                var dataLine = string.Format(
                    @"{0,-28:TRANS WK0}={1,10:0.000},{2,10:0.000},{3,10:0.000},{4,10:0.000},{5,10:0.000},{6,10:0.000}",
                    userFrame.Number,
                    userFrameValue.Position.X,
                    userFrameValue.Position.Y,
                    userFrameValue.Position.Z,
                    userFrameEulerAngles.X,
                    userFrameEulerAngles.Y,
                    userFrameEulerAngles.Z);
                this.DataSection.StreamWriter.WriteLine(dataLine);
            }
        }

        internal virtual void StartPostFile(PostFile postFile, CbtNode operationNode, PathNode pointNode)
        {
            postFile.FileName = this.ProgramName;

            FileSection globalSection = new FileSection()
            {
                Header = "GLOBAL" + "\r\n",
                Footer = "END" + "\r\n",
            };
            postFile.FileSection.SubFileSections.Add(globalSection);

            FileSection movesSection = new FileSection()
            {
                Header = "PROGRAM MAIN" + "\r\n",
                Footer = "END" + "\r\n",
            };
            postFile.FileSection.SubFileSections.Add(movesSection);

            FileSection dataSection = new FileSection()
            {
                Header = "DATA" + "\r\n",
                Footer = "END" + "\r\n",
            };
            postFile.FileSection.SubFileSections.Add(dataSection);
        }

        internal override void RunBeforeOperationOutput(CbtNode operationNode)
        {
            var previousOperation = OperationManager.GetPreviousOperationNodeInProgram(operationNode);

            int tcpId = OperationManager.GetTcpId(operationNode);
            if (previousOperation == null || tcpId != OperationManager.GetTcpId(previousOperation))
            {
                //// Move output
                //// Example:
                //// TOOL = TL1
                this.MovesSection.StreamWriter.WriteLine("TOOL = TL" + tcpId);
            }
        }

        internal override void OutputLinearMove(PathNode point, CbtNode operationNode)
        {
            //// Move output
            //// Example:
            //// MOVES P[5] WITH WORK = WK1
            this.MovesSection.StreamWriter.WriteLine("MOVES P[" + this.PointNumberInProgram + "] WITH WORK = WK" + OperationManager.GetUserFrame(operationNode).Number);

            //// Data point output
            //// Example:
            //// POINT P(5)                  =    73.500,  -257.736,     6.500,     0.000,     0.000,     0.000,     0.000,     0.000 / -------

            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 eulerAnglesInUserFrame = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            var dataLine = string.Format(
                "{0,-28:POINT P(0)}={1,10:0.000},{2,10:0.000},{3,10:0.000},{4,10:0.000},{5,10:0.000},{6,10:0.000},{7,10:0.000},{8,10:0.000} / {9}",
                this.PointNumberInProgram,
                pointFrameInUserFrame.Position.X,
                pointFrameInUserFrame.Position.Y,
                pointFrameInUserFrame.Position.Z,
                eulerAnglesInUserFrame.X,
                eulerAnglesInUserFrame.Y,
                eulerAnglesInUserFrame.Z,
                0, // point.JointValues(JointFormat["R1"])
                0, // point.JointValues(JointFormat["R2"])
                this.FormatRobotConfiguration(point, operationNode));
            this.DataSection.StreamWriter.WriteLine(dataLine);
        }

        internal override void RunAfterProgramOutput()
        {
            //// Global output
            //// Example:
            ////   DIM P(500) AS POINT
            ////   PNUM = 500

            this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter.WriteLine("DIM P(" + this.PointNumberInProgram + ") AS POINT");
            this.CurrentPostFile.FileSection.SubFileSections[0].StreamWriter.WriteLine("PNUM = " + this.PointNumberInProgram);
        }

        internal override void OutputCircularMove(PathNode point, CbtNode operationNode)
        {
            if (point.IsArcMiddlePoint())
            {
                //// Arc mid point output
                //// Example:
                //// MOVEC P[6]
                this.MovesSection.StreamWriter.Write("MOVEC P[" + this.PointNumberInProgram + "]");

                return;
            }
            else
            {
                //// Arc end point output
                //// Example:
                //// P[7] WITH WORK = WK1
                this.MovesSection.StreamWriter.WriteLine(" P[" + this.PointNumberInProgram + "] WITH WORK = WK" + OperationManager.GetUserFrame(operationNode).Number);
            }

            //// Data point output
            //// Example:
            //// POINT P(1)                  =    73.500,  -257.736,     6.500,     0.000,     0.000,     0.000,     0.000,     0.000 / -------

            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 eulerAnglesInUserFrame = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            var dataLine = string.Format(
                @"{0,-28:POINT P(0)}={1,10:0.000},{2,10:0.000},{3,10:0.000},{4,10:0.000},{5,10:0.000},{6,10:0.000},{7,10:0.000},{8,10:0.000} / {9}",
                this.PointNumberInProgram,
                pointFrameInUserFrame.Position.X,
                pointFrameInUserFrame.Position.Y,
                pointFrameInUserFrame.Position.Z,
                eulerAnglesInUserFrame.X,
                eulerAnglesInUserFrame.Y,
                eulerAnglesInUserFrame.Z,
                0, // point.JointValues(JointFormat["R1"])
                0, // point.JointValues(JointFormat["R2"])
                this.FormatRobotConfiguration(point, operationNode));
            this.DataSection.StreamWriter.WriteLine(dataLine);
        }

        internal override void OutputJointMove(PathNode point, CbtNode operationNode)
        {
            //// Move output
            //// Example:
            //// MOVE P[1] WITH WORK = WK1
            this.MovesSection.StreamWriter.WriteLine("MOVE P[" + this.PointNumberInProgram + "] WITH WORK = WK" + OperationManager.GetUserFrame(operationNode).Number);

            //// Data point output
            //// Example:
            //// POINT P(1)                  =    73.500,  -257.736,     6.500,     0.000,     0.000,     0.000,     0.000,     0.000 / -------

            Matrix4X4 pointFrameInUserFrame = point.PathPointFrameInUserFrame(operationNode, this.SceneCbtRoot);

            if (this.EnableToolFrameExtraRotation)
            {
                pointFrameInUserFrame.Orientation = pointFrameInUserFrame.Orientation * this.ToolFrameExtraRotation;
            }

            Vector3 eulerAnglesInUserFrame = this.RobotFormatter.MatrixToEuler(pointFrameInUserFrame);

            var dataLine = string.Format(
                "{0,-28:POINT P(0)}={1,10:0.000},{2,10:0.000},{3,10:0.000},{4,10:0.000},{5,10:0.000},{6,10:0.000},{7,10:0.000},{8,10:0.000} / {9}",
                this.PointNumberInProgram,
                pointFrameInUserFrame.Position.X,
                pointFrameInUserFrame.Position.Y,
                pointFrameInUserFrame.Position.Z,
                eulerAnglesInUserFrame.X,
                eulerAnglesInUserFrame.Y,
                eulerAnglesInUserFrame.Z,
                0, // point.JointValues(JointFormat["R1"])
                0, // point.JointValues(JointFormat["R2"])
                this.FormatRobotConfiguration(point, operationNode));
            this.DataSection.StreamWriter.WriteLine(dataLine);
        }

        internal virtual string FormatRobotConfiguration(PathNode point, CbtNode operationNode)
        {
            var robotConfigurationCommand = string.Empty;

            switch (OperationManager.GetRobotConfiguration(operationNode).BaseConfiguration)
            {
                case BaseConfig.Back:
                    robotConfigurationCommand += "R"; // RIGHTY
                    break;
                case BaseConfig.Front:
                    robotConfigurationCommand += "L"; // LEFTY
                    break;
                default:
                    robotConfigurationCommand += "-"; // SAME
                    break;
            }

            switch (OperationManager.GetRobotConfiguration(operationNode).ElbowConfiguration)
            {
                case ElbowConfig.Up:
                    robotConfigurationCommand += "A"; // ABOVE
                    break;
                case ElbowConfig.Down:
                    robotConfigurationCommand += "B"; // BELOW
                    break;
                default:
                    robotConfigurationCommand += "-"; // SAME
                    break;
            }

            switch (OperationManager.GetRobotConfiguration(operationNode).WristConfiguration)
            {
                case WristConfig.Negative:
                    robotConfigurationCommand += "F"; // FLIP
                    break;
                case WristConfig.Positive:
                    robotConfigurationCommand += "N"; // NFLIP
                    break;
                default:
                    robotConfigurationCommand += "-"; // SAME
                    break;
            }

            if (Math.Abs(point.JointValue(this.JointFormat["J4"])) < 180)
            {
                robotConfigurationCommand += "S"; // SINGLE4
            }
            else
            {
                robotConfigurationCommand += "D"; // DOUBLE4
            }

            if (Math.Abs(point.JointValue(this.JointFormat["J6"])) < 180)
            {
                robotConfigurationCommand += "S"; // SINGLE6
            }
            else
            {
                robotConfigurationCommand += "D"; // DOUBLE6
            }

            return robotConfigurationCommand;
        }

        internal override void OutputJointSpaceMove(PathNode point, CbtNode operationNode)
        {
            this.MovesSection.StreamWriter.WriteLine("P[" + this.PointNumberInProgram + "] JJ MOVES NOT SUPPORTED");
            this.DataSection.StreamWriter.WriteLine("P[" + this.PointNumberInProgram + "] JJ MOVES NOT SUPPORTED");
        }
    }
}
